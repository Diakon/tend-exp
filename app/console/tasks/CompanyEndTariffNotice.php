<?php
namespace console\tasks;

use common\components\SendMail;
use common\modules\company\models\common\Company;
use common\modules\company\models\common\CompanyUsers;
use common\modules\tariff\models\common\TariffPlans;
use Yii;
use modules\cron\IConsoleTask;

/**
 * Крон-задача: Уведомляет о скором окончании оплаченого тарифа.
 */
class CompanyEndTariffNotice implements IConsoleTask
{
    const DB_TABLE_SCHEDULED = '{{%scheduled}}';

    /**
     * Запуск действия.
     *
     * @return boolean
     */
    public function run()
    {
        // Получаю тарифный план "минимальный"
        $minTariff = TariffPlans::find()
            ->where(['type' => TariffPlans::CONST_TYPE_MIN])
            ->asArray()
            ->one();
        // Получаю компании, у которыю тариф не менимальный, а срок окончания - 15 дней
        $companies = Company::find()
            ->where(['!=', 'tariff_plan_id', $minTariff['id']])
            ->andWhere(['<', 'tariff_plan_end', time() - 15 * 86400]);
        foreach ($companies->each(100) as $company) {
            $userCompany = $company->getUsers()->where(['type_role' => CompanyUsers::TYPE_ROLE_ADMIN])->one();
            if (!empty($userCompany->user->email)) {
                $user = $userCompany->user;
                $paramView = [];
                $paramView['userName'] = $user->username;
                $paramView['email'] = $userCompany->user->email;
                $paramView['companyUrl'] = $company->url;
                $paramView['companyName'] = $company->title;
                $paramView['tariffPlan'] = TariffPlans::$typesList[$company->tariff->type] ?? '';
                $paramView['tariffPlanEndData'] = Yii::$app->formatter->asDate($company->tariff_plan_end, 'php:d.m.Y');
                SendMail::send(
                    'cron-end-tariff-notice',
                    $paramView,
                    Yii::$app->params['infoEmailFrom'],
                    [$userCompany->user->email => $user->username],
                    'TENDEROS. Уведомление об окончании оплаченого тарифного плана.'
                );
            }
        }

        return true;
    }

}