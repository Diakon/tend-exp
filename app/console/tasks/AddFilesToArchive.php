<?php
namespace console\tasks;

use common\modules\company\models\common\Company;
use common\modules\tariff\models\common\TariffPlans;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use modules\cron\IConsoleTask;

/**
 * Крон-задача: Создает архивы файлов тендера для скачивания
 */
class AddFilesToArchive implements IConsoleTask
{
    const DB_TABLE_SCHEDULED = '{{%scheduled}}';

    /**
     * Запуск действия.
     *
     * @return boolean
     */
    public function run()
    {
        // Полую тендеры, которые еще не завершены
        $tenders = Tenders::find()->where(['!=', 'status',  Tenders::STATUS_CLOSE]);
        foreach ($tenders->each(100) as $tender) {
            $patch = Yii::getAlias('@root') . '/upload/tenders/files/' . $tender->id;
            $zipFile = $patch . '/' . $tender->id . '.zip';
            // Если есть файл архива созданый ранее - удаляю (могли добавить / убрать файлы в редактировании тендера с последнего времени создания архива)
            if (file_exists($zipFile)) {
                unlink($zipFile);
            }

            $zip = new \ZipArchive();
            if ($zip->open($zipFile, \ZipArchive::CREATE) !== true) {
                $zip->close();
                continue;
            }
            // Если есть файлы - создаю архив
            if (!empty($tender->files)) {
                foreach ($tender->files as $file) {
                    $archiveFile = $patch . '/' . $file->fileName;
                    if (file_exists($archiveFile)) {
                        $zip->addFile($archiveFile, $file->fileName);
                    }
                }
            }

            $zip->close();
        }

        return true;
    }

}