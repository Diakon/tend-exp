<?php
namespace console\tasks;

use common\models\UserIdentity;
use common\modules\company\models\common\Company;
use common\modules\company\models\common\CompanyUsers;
use common\modules\tariff\models\common\TariffPlans;
use Yii;
use modules\cron\IConsoleTask;
use yii\helpers\ArrayHelper;

/**
 * Крон-задача: Проверяет, окончилось ли оплаченое время действия тарифа.
 * Если окончился снимаем тариф (назначается минимальный)
 */
class CompanyTariffPlan implements IConsoleTask
{
    const DB_TABLE_SCHEDULED = '{{%scheduled}}';

    /**
     * Запуск действия.
     *
     * @return boolean
     */
    public function run()
    {
        // Получаю тарифный план "минимальный", который устанавливаем, если оплаченая подписка окончена
        $minTariff = TariffPlans::find()
            ->where(['type' => TariffPlans::CONST_TYPE_MIN])
            ->asArray()
            ->one();
        // Получаем компании у которых окончилась оплаченная подписка
        $companies = Company::find()
            ->where(['!=', 'tariff_plan_id', $minTariff['id']])
            ->andWhere(['<', 'tariff_plan_end', time()]);
        foreach ($companies->each(100) as $company) {
            $company->tariff_plan_id = $minTariff['id'];
            $company->tariff_plan_start = time();
            $company->tariff_plan_end = null; //Минимальный тариф не оканчивается до покупки другого
            $company->save(false);
            // Удаляю всех пользователей в этой компании кроме админа
            $usersIds = ArrayHelper::map($company->getUsers()->where(['!=', 'type_role', CompanyUsers::TYPE_ROLE_ADMIN])->asArray()->all(), 'user_id', 'user_id');
            if (!empty($usersIds)) {
                UserIdentity::deleteAll(['in', 'id', $usersIds]);
            }
        }

        return true;
    }

}