<?php
namespace console\tasks;

use common\modules\tender\models\common\Objects;
use common\modules\tender\models\common\Tenders;
use modules\cron\IConsoleTask;

/**
 * Крон-задача: Проверяет, окончилось ли время проведения тендера. Если да и тендер не завершен - завершает его (без выбора победителя).
 * Так же проверяет, истек ли срок выполнения объектов, если истек - закрывает его
 */
class TendersStatusCheck implements IConsoleTask
{
    const DB_TABLE_SCHEDULED = '{{%scheduled}}';

    /**
     * Запуск действия.
     *
     * @return boolean
     */
    public function run()
    {
        // Получаю тендеры, которые заканчиваются сегодня и у которыз статус != Tenders::STATUS_CLOSE(Завершен)
        $tenders = Tenders::find()
            ->where(['!=', 'status',  Tenders::STATUS_CLOSE])
            ->andWhere(['<=', 'date_end', date('Y-m-d H:i:s', time())]);
        foreach ($tenders->each(100) as $tender) {
            $tender->status =  Tenders::STATUS_CLOSE;
            $tender->save(false);
        }

        // Прохожу по всем объектам, если в этих объектах время объекта истекло - закрываю объект
        $objects = Objects::find()
            ->where(['<=', 'date_end', date('Y-m-d H:i:s', time())])
            ->andWhere(['status_complete_id' => Objects::STATUS_NON_COMPLETE]);
        foreach ($objects->each(100) as $object) {
            $object->status =  Objects::STATUS_COMPLETE;
            $object->save(false);
        }

        return true;
    }

}