<?php

use yii\db\Migration;

/**
 * Class m181015_091453_offers_elements_add_sum_sum_discount_fields
 */
class m181015_091453_offers_elements_add_sum_sum_discount_fields extends Migration
{
    const DB_TABLE = '{{%offers_elements}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'sum', "DECIMAL(10,2) NULL DEFAULT '0.00' COMMENT 'Сумма элемнта предложения'");
        $this->addColumn(self::DB_TABLE, 'sum_discount', "DECIMAL(10,2) NULL DEFAULT '0.00' COMMENT 'Сумма элемнта предложения с учетом запроса скидки'");

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'sum');
        $this->dropColumn(self::DB_TABLE, 'sum_discount');

        Yii::$app->db->schema->refresh();
    }
}
