<?php

use yii\db\Migration;

/**
 * Class m180515_142256_create_table_company_tender_estimates
 */
class m180515_142256_create_table_company_tender_estimates extends Migration
{
    const TABLE_NAME = '{{%estimates_rubrics}}';
    const TABLE_TENDER = '{{%tenders}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'parent_id' => $this->integer(10)->comment('Родительская запись'),
                'lft' => $this->integer()->notNull()->comment('Левый ключ'),
                'rgt' => $this->integer()->notNull()->comment('Правый ключ'),
                'depth' => $this->integer()->notNull()->comment('Уровень'),
                'title' => $this->string(255)->notNull()->comment('Название сметы'),
                'tender_id' => $this->integer(10)->unsigned()->comment('Тендер'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_tender_estimates_rubrics_lft', self::TABLE_NAME, ['lft', 'rgt']);
            $this->createIndex('IND_company_tender_estimates_rubrics_rgt', self::TABLE_NAME, ['rgt']);
            $this->createIndex('IND_company_tender_estimates_rubrics_parent_id', self::TABLE_NAME, 'parent_id');
            $this->createIndex('IND_company_tender_estimates_rubrics_depth', self::TABLE_NAME, 'depth');
            $this->createIndex('IND_company_tender_estimates_rubrics_tender_id', self::TABLE_NAME, 'tender_id');

            $this->addForeignKey('FK_company_tender_estimates_rubrics_tender_id', self::TABLE_NAME, 'tender_id', self::TABLE_TENDER, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tender_estimates_rubrics_tender_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
