<?php

use yii\db\Migration;

/**
 * Class m190807_105049_create_table_tender_tenders_chars_works
 */
class m190807_105049_create_table_tender_tenders_chars_works extends Migration
{
    const TABLE_NAME = '{{%tenders_chars_works}}';
    const TABLE_TENDER = '{{%tenders}}';
    const TABLE_DIR = '{{%directories}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'tender_id' => $this->integer(10)->unsigned()->notNull()->comment('Тендер'),
                'main_work_id' => $this->integer(10)->unsigned()->notNull()->comment('Направления работ'),
                'types_work_id' => $this->integer(10)->unsigned()->notNull()->comment('Группы работ'),
                'works_id' => $this->integer(10)->unsigned()->notNull()->comment('Работы'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
            $this->addForeignKey('FK_tenders_chars_works_tender_id', self::TABLE_NAME, 'tender_id', self::TABLE_TENDER, 'id', 'CASCADE');
            $this->addForeignKey('FK_tenders_chars_works_main_work_id', self::TABLE_NAME, 'main_work_id', self::TABLE_DIR, 'id', 'CASCADE');
            $this->addForeignKey('FK_tenders_chars_works_types_work_id', self::TABLE_NAME, 'types_work_id', self::TABLE_DIR, 'id', 'CASCADE');
            $this->addForeignKey('FK_tenders_chars_works_works_id', self::TABLE_NAME, 'works_id', self::TABLE_DIR, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_tenders_chars_works_tender_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_tenders_chars_works_main_work_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_tenders_chars_works_types_work_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_tenders_chars_works_works_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
