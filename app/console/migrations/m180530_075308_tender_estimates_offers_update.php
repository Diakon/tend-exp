<?php

use yii\db\Migration;

/**
 * Class m180530_075308_tender_estimates_offers_update
 */
class m180530_075308_tender_estimates_offers_update extends Migration
{
    const TABLE_NAME = '{{%offers_elements}}';
    const TABLE_OFFER = '{{%offers}}';
    const TABLE_ELEMENT = '{{%estimates_elements}}';
    const TABLE_TENDER = '{{%tenders}}';

    public function safeUp()
    {
        // Убираю поля, которые не нужны
        $this->dropColumn(self::TABLE_ELEMENT, 'type');
        $this->dropForeignKey('FK_company_tender_estimates_offers_element_id', self::TABLE_OFFER);
        $this->dropColumn(self::TABLE_OFFER, 'element_id');

        // Добавляю тендер в таблицу предложения
        $this->addColumn(self::TABLE_OFFER, 'tender_id', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Тендер'");
        $this->createIndex('IND_company_tender_estimates_offers_tender_id', self::TABLE_OFFER, 'tender_id');
        $this->addForeignKey('FK_company_tender_estimates_offers_tender_id', self::TABLE_OFFER, 'tender_id', self::TABLE_TENDER, 'id', 'CASCADE');

        // Создаю таблицу доп. предложений к тендеру от заказчика
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'offer_id' => $this->integer(10)->unsigned()->notNull()->comment('Предложение'),
                'title' => $this->string(555)->notNull()->comment('Работа'),
                'count' => $this->integer(11)->unsigned()->notNull()->comment('Количество'),
                'unit_id' => $this->smallInteger(2)->unsigned()->comment('Единица измерения'),
                'price_work' => $this->decimal(10, 2)->comment('Стоимость работы за 1 единицу'),
                'price_material' => $this->decimal(10, 2)->comment('Стоимость материалов за 1 единицу'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_tender_estimates_elements_offer_id', self::TABLE_NAME, 'offer_id');
            $this->addForeignKey('FK_company_tender_estimates_elements_offer_id', self::TABLE_NAME, 'offer_id', self::TABLE_OFFER, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tender_estimates_elements_offer_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);

        $this->dropForeignKey('FK_company_tender_estimates_offers_tender_id', self::TABLE_OFFER);
        $this->dropColumn(self::TABLE_OFFER, 'tender_id');

        $this->addColumn(self::TABLE_ELEMENT, 'type', "TINYINT(1)");
        $this->addColumn(self::TABLE_OFFER, 'element_id', "INT(10) UNSIGNED NULL DEFAULT NULL");
        $this->createIndex('IND_company_tender_estimates_offers_element_id', self::TABLE_OFFER, 'element_id');
        $this->addForeignKey('FK_company_tender_estimates_offers_element_id', self::TABLE_OFFER, 'element_id', self::TABLE_ELEMENT, 'id', 'CASCADE');


        Yii::$app->db->schema->refresh();
    }
}
