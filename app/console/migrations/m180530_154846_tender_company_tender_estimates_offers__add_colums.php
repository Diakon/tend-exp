<?php

use yii\db\Migration;

/**
 * Class m180530_154846_tender_company_tender_estimates_offers__add_colums
 */
class m180530_154846_tender_company_tender_estimates_offers__add_colums extends Migration
{
    const TABLE_NAME = '{{%offers}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'time_work', 'TINYINT(3) UNSIGNED NOT NULL COMMENT "Срок работы (мес.)"');
        $this->addColumn(self::TABLE_NAME, 'time_warranty', 'TINYINT(3) UNSIGNED NOT NULL COMMENT "Срок гарантии (мес.)"');
        $this->addColumn(self::TABLE_NAME, 'bank_prepayment_warranty', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "0" COMMENT "Банковская гарантия для аванса"');
        $this->addColumn(self::TABLE_NAME, 'bank_period_warranty', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "0" COMMENT "Банковская гарантия для аванса"');
        $this->addColumn(self::TABLE_NAME, 'bank_obligation_warranty', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "0" COMMENT "Банковская гарантия для выполнения обязательства"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'time_work');
        $this->dropColumn(self::TABLE_NAME, 'time_warranty');
        $this->dropColumn(self::TABLE_NAME, 'bank_prepayment_warranty');
        $this->dropColumn(self::TABLE_NAME, 'bank_period_warranty');
        $this->dropColumn(self::TABLE_NAME, 'bank_obligation_warranty');
    }
}
