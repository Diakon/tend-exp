<?php

use yii\db\Migration;

/**
 * Class m181017_131647_tender_comment_chane_offerId_type_field
 */
class m181017_131647_tender_comment_chane_offerId_type_field extends Migration
{
    const DB_TABLE = '{{%comment}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(self::DB_TABLE, 'offerId', 'VARCHAR(255) NULL DEFAULT NULL COMMENT "ID Предложения для которого оставлен коментарий"');

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn(self::DB_TABLE, 'offerId', 'INT(10) UNSIGNED NULL DEFAULT NULL COMMENT "ID Предложения для которого оставлен коментарий"');

        Yii::$app->db->getSchema()->refresh();
    }
}
