<?php

use yii\db\Migration;

/**
 * Class m180607_213319_add_count_tenders_id__to_tender_tariff_plans
 */
class m180607_213319_add_count_tenders_id__to_tender_tariff_plans extends Migration
{
    const TABLE_NAME = '{{%tariff_plans}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'count_tenders_id', "SMALLINT(2) NULL DEFAULT NULL COMMENT 'Количество тендеров'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'count_tenders_id');
    }
}
