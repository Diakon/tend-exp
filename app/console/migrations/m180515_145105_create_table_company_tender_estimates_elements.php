<?php

use yii\db\Migration;

/**
 * Class m180515_145105_create_table_company_tender_estimates_elements
 */
class m180515_145105_create_table_company_tender_estimates_elements extends Migration
{
    const TABLE_NAME = '{{%estimates_elements}}';
    const TABLE_RUBRIC = '{{%estimates_rubrics}}';
    const TABLE_TENDER = '{{%tenders}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'rubric_id' => $this->integer(10)->unsigned()->comment('Раздел сметы'),
                'tender_id' => $this->integer(10)->unsigned()->notNull()->comment('Тендер'),
                'title' => $this->string(555)->notNull()->comment('Работа'),
                'unit_id' => $this->smallInteger(2)->unsigned()->comment('Единица измерения'),
                'count' => $this->integer(11)->unsigned()->notNull()->comment('Количество'),
                'price_work' => $this->integer(11)->comment('Стоимость работы за 1 единицу'),
                'price_material' => $this->integer(11)->comment('Стоимость материалов за 1 единицу'),
                'type' => $this->smallInteger(1)->unsigned()->defaultValue(1)->comment('Тип эллемента 1-работа, 2-доп. работы'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_tender_estimates_elements_rubric_id', self::TABLE_NAME, 'rubric_id');
            $this->createIndex('IND_company_tender_estimates_elements_tender_id', self::TABLE_NAME, 'tender_id');
            $this->createIndex('IND_company_tender_estimates_elements_type', self::TABLE_NAME, 'type');

            $this->addForeignKey('FK_company_tender_estimates_elements_rubric_id', self::TABLE_NAME, 'rubric_id', self::TABLE_RUBRIC, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_tender_estimates_elements_tender_id', self::TABLE_NAME, 'tender_id', self::TABLE_TENDER, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tender_estimates_elements_rubric_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_tender_estimates_elements_tender_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
