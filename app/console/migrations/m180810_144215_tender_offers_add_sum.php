<?php

use yii\db\Migration;

/**
 * Class m180810_144215_tender_offers_add_sum
 */
class m180810_144215_tender_offers_add_sum extends Migration
{
    const DB_TABLE = '{{%offers}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'sum', "DECIMAL(10,2) NULL DEFAULT '0' COMMENT 'Сумма предложения для тендера'");

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'sum');

        Yii::$app->db->getSchema()->refresh();
    }
}
