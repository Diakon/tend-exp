<?php

use yii\db\Migration;

/**
 * Class m180507_150456_company_users
 */
class m180507_150456_company_users extends Migration
{
    const TABLE_NAME = '{{%company_users}}';
    const TABLE_COMPANY = '{{%company}}';
    const TABLE_USER = '{{%user}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'user_id' => $this->integer(11)->comment('Пользователь'),
                'type_role' => $this->smallInteger(2)->comment('Роль в организации'),
                'status' => $this->smallInteger(2)->comment('Статус'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_users_company_id', self::TABLE_NAME, 'company_id');
            $this->createIndex('IND_company_users_user_id', self::TABLE_NAME, 'user_id');
            $this->createIndex('IND_company_users_status', self::TABLE_NAME, 'status');

            $this->addForeignKey('FK_company_users_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id');
            $this->addForeignKey('FK_company_users_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_users_company_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_users_user_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
