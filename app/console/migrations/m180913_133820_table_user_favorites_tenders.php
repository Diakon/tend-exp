<?php

use yii\db\Migration;

/**
 * Class m180913_133820_table_user_favorites_tenders
 */
class m180913_133820_table_user_favorites_tenders extends Migration
{
    const TABLE_NAME = '{{%user_favorites_tenders}}';
    const TABLE_TENDER = '{{%tenders}}';
    const TABLE_USER = '{{%user}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'user_id' => $this->integer(11)->comment('Пользователь'),
                'tender_id' => $this->integer(10)->unsigned()->comment('Тендер'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_user_favorites_tenders_tender_id', self::TABLE_NAME, 'tender_id');
            $this->createIndex('IND_user_favorites_tenders_user_id', self::TABLE_NAME, 'user_id');

            $this->addForeignKey('FK_user_favorites_tenders_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id', 'CASCADE');
            $this->addForeignKey('FK_user_favorites_tenders_tender_id', self::TABLE_NAME, 'tender_id', self::TABLE_TENDER, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_user_favorites_tenders_user_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_user_favorites_tenders_tender_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
