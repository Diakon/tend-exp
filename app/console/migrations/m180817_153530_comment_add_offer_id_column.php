<?php

use yii\db\Migration;

/**
 * Class m180817_153530_comment_add_offer_id_column
 */
class m180817_153530_comment_add_offer_id_column extends Migration
{
    const TABLE_NAME = '{{%comment}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(self::TABLE_NAME, 'companyId');
        $this->addColumn(self::TABLE_NAME, 'offerId', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'ID Предложения для которого оставлен коментарий'");

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(self::TABLE_NAME, 'companyId', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Компания'");
        $this->dropColumn(self::TABLE_NAME, 'offerId');

        Yii::$app->db->schema->refresh();
    }
}
