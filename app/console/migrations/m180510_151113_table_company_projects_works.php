<?php

use yii\db\Migration;

/**
 * Class m180510_151113_table_company_projects_works
 */
class m180510_151113_table_company_projects_works extends Migration
{
    const TABLE_NAME = '{{%project_works}}';
    const TABLE_PROJECT = '{{%project_works}}';
    const TABLE_DIR = '{{%directories}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'project_id' => $this->integer(10)->unsigned()->comment('Проект'),
                'work_id' => $this->integer(10)->unsigned()->comment('Вид работы'),
                'area' => $this->integer(10)->unsigned()->comment('Площадь в м3'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_project_works_project_id', self::TABLE_NAME, 'project_id');
            $this->createIndex('IND_company_project_works_work_id', self::TABLE_NAME, 'work_id');

            $this->addForeignKey('FK_company_project_works_project_id', self::TABLE_NAME, 'project_id', self::TABLE_PROJECT, 'id');
            $this->addForeignKey('FK_company_project_works_work_id', self::TABLE_NAME, 'work_id', self::TABLE_DIR, 'id');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_project_works_project_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_project_works_work_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
