<?php

use yii\db\Migration;

/**
 * Class m190109_180000_alter_table_objects
 */
class m190109_180000_alter_table_objects extends Migration
{
    const DB_TABLE = '{{%objects}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(self::DB_TABLE, 'date_end',' timestamp NULL DEFAULT \'0000-00-00 00:00:00\' COMMENT \'Дата окончания реализации\' AFTER `date_start`');
        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn(self::DB_TABLE, 'date_end','TIMESTAMP DEFAULT 0 COMMENT "Дата окончания реализации"');

        Yii::$app->db->getSchema()->refresh();
    }
}
