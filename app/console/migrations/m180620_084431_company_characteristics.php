<?php

use yii\db\Migration;

/**
 * Class m180620_084431_company_characteristics
 */
class m180620_084431_company_characteristics extends Migration
{
    const TABLE_CHAR_REGIONS = '{{%company_chars_regions}}';
    const TABLE_CHAR_FUNCTIONS = '{{%company_chars_functions}}';
    const TABLE_CHAR_BUILDS = '{{%company_chars_builds}}';
    const TABLE_CHAR_WORKS = '{{%company_chars_works}}';

    const TABLE_ACTIVITYS = '{{%company_activitys}}';
    const TABLE_ADDRESS = '{{%company_address}}';
    const TABLE_DIRECTORIES = '{{%directories}}';
    const TABLE_COMPANY = '{{%company}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable(self::TABLE_ACTIVITYS);

        if (Yii::$app->db->schema->getTableSchema(self::TABLE_CHAR_REGIONS, true) === null) {
            $this->createTable(self::TABLE_CHAR_REGIONS, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'country_id' => $this->integer(10)->unsigned()->comment('Страна'),
                'region_id' => $this->integer(10)->unsigned()->comment('Регион'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_chars_regions_company_id', self::TABLE_CHAR_REGIONS, 'company_id');
            $this->createIndex('IND_company_chars_regions_country_id', self::TABLE_CHAR_REGIONS, 'country_id');
            $this->createIndex('IND_company_chars_regions_region_id', self::TABLE_CHAR_REGIONS, 'region_id');
            $this->addForeignKey('FK_company_chars_regions_company_id', self::TABLE_CHAR_REGIONS, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
        }

        if (Yii::$app->db->schema->getTableSchema(self::TABLE_CHAR_FUNCTIONS, true) === null) {
            $this->createTable(self::TABLE_CHAR_FUNCTIONS, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'function_id' => $this->integer(10)->unsigned()->comment('Запись в справочнике функций компании'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_chars_functions_company_id', self::TABLE_CHAR_FUNCTIONS, 'company_id');
            $this->createIndex('IND_company_chars_functions_function_id', self::TABLE_CHAR_FUNCTIONS, 'function_id');
            $this->addForeignKey('FK_company_chars_functions_company_id', self::TABLE_CHAR_FUNCTIONS, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_chars_functions_function_id', self::TABLE_CHAR_FUNCTIONS, 'function_id', self::TABLE_DIRECTORIES, 'id', 'CASCADE');
        }

        if (Yii::$app->db->schema->getTableSchema(self::TABLE_CHAR_BUILDS, true) === null) {
            $this->createTable(self::TABLE_CHAR_BUILDS, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'main_work_id' => $this->integer(10)->unsigned()->comment('Запись в справочнике направлений деятельности'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_chars_builds_company_id', self::TABLE_CHAR_BUILDS, 'company_id');
            $this->createIndex('IND_company_chars_builds_main_work_id', self::TABLE_CHAR_BUILDS, 'main_work_id');
            $this->addForeignKey('FK_company_chars_builds_company_id', self::TABLE_CHAR_BUILDS, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_chars_builds_main_work_id', self::TABLE_CHAR_BUILDS, 'main_work_id', self::TABLE_DIRECTORIES, 'id', 'CASCADE');
        }

        if (Yii::$app->db->schema->getTableSchema(self::TABLE_CHAR_WORKS, true) === null) {
            $this->createTable(self::TABLE_CHAR_WORKS, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'type_work_id' => $this->integer(10)->unsigned()->comment('Запись в справочнике направлений деятельности'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_chars_works_company_id', self::TABLE_CHAR_WORKS, 'company_id');
            $this->createIndex('IND_company_chars_works_type_work_id', self::TABLE_CHAR_WORKS, 'type_work_id');
            $this->addForeignKey('FK_company_chars_works_company_id', self::TABLE_CHAR_WORKS, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_chars_works_type_work_id', self::TABLE_CHAR_WORKS, 'type_work_id', self::TABLE_DIRECTORIES, 'id', 'CASCADE');
        }
        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_CHAR_REGIONS);
        $this->dropTable(self::TABLE_CHAR_FUNCTIONS);
        $this->dropTable(self::TABLE_CHAR_BUILDS);
        $this->dropTable(self::TABLE_CHAR_WORKS);

        if (Yii::$app->db->schema->getTableSchema(self::TABLE_ACTIVITYS, true) === null) {
            $this->createTable(self::TABLE_ACTIVITYS, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'address_id' => $this->integer(10)->unsigned()->comment('Адресс'),
                'function_id' => $this->integer(10)->unsigned()->comment('Функции компании'),
                'main_work_id' => $this->integer(10)->unsigned()->comment('Направление деятельности'),
                'types_work_id' => $this->integer(10)->unsigned()->comment('Виды работ'),
                'employees_now' => $this->smallInteger(5)->comment('Количество сотрудников сейчас'),
                'employees_max' => $this->smallInteger(5)->comment('Количество сотрудников максимуми'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_activitys_company_id', self::TABLE_ACTIVITYS, 'company_id');
            $this->createIndex('IND_company_activitys_address_id', self::TABLE_ACTIVITYS, 'address_id');
            $this->createIndex('IND_company_activitys_function_id', self::TABLE_ACTIVITYS, 'function_id');
            $this->createIndex('IND_company_activitys_main_work_id', self::TABLE_ACTIVITYS, 'main_work_id');
            $this->createIndex('IND_company_activitys_types_work_id', self::TABLE_ACTIVITYS, 'types_work_id');

            $this->addForeignKey('FK_company_activitys_company_id', self::TABLE_ACTIVITYS, 'company_id', self::TABLE_COMPANY, 'id');
            $this->addForeignKey('FK_company_activitys_address_id', self::TABLE_ACTIVITYS, 'address_id', self::TABLE_ADDRESS, 'id');
            $this->addForeignKey('FK_company_activitys_function_id', self::TABLE_ACTIVITYS, 'function_id', self::TABLE_DIRECTORIES, 'id');
            $this->addForeignKey('FK_company_activitys_main_work_id', self::TABLE_ACTIVITYS, 'main_work_id', self::TABLE_DIRECTORIES, 'id');
            $this->addForeignKey('FK_company_activitys_types_work_id', self::TABLE_ACTIVITYS, 'types_work_id', self::TABLE_DIRECTORIES, 'id');
        }

        Yii::$app->db->schema->refresh();
    }
}
