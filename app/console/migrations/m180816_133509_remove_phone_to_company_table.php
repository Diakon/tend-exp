<?php

use yii\db\Migration;

/**
 * Class m180816_133509_remove_phone_to_company_table
 */
class m180816_133509_remove_phone_to_company_table extends Migration
{
    const TABLE_COMPANY = '{{%company}}';
    const TABLE_PHONES = '{{%company_phones}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_COMPANY, 'phone', "VARCHAR(255) NULL DEFAULT NULL COMMENT 'номер телефона'");
        $this->dropForeignKey('FK_company_phones_company_id', self::TABLE_PHONES);
        $this->dropTable(self::TABLE_PHONES);

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_COMPANY, 'phone');
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_PHONES, true) === null) {
            $this->createTable(self::TABLE_PHONES, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компания'),
                'phone' => $this->string(20)->comment('Телефон'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),

            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_phones_company_id', self::TABLE_PHONES, 'company_id');
            $this->addForeignKey('FK_company_phones_company_id', self::TABLE_PHONES, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }
}
