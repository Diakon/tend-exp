<?php

use yii\db\Migration;

/**
 * Class m190717_151744_offers__add___sum_discount_request
 */
class m190717_151744_offers__add___sum_discount_request extends Migration
{
    const DB_TABLE = '{{%offers}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'sum_discount_request',"FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Сумма с учетом не подтвержденного автором предложения запроса скидки'");

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'sum_discount_request');

        Yii::$app->db->getSchema()->refresh();
    }
}
