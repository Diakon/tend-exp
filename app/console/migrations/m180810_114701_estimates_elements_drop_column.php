<?php

use yii\db\Migration;

/**
 * Class m180810_114701_estimates_elements_drop_column
 */
class m180810_114701_estimates_elements_drop_column extends Migration
{
    const TABLE_RUBRIC = '{{%estimates_rubrics}}';
    const TABLE_ELEMENT = '{{%estimates_elements}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(self::TABLE_RUBRIC, 'discount');
        $this->dropColumn(self::TABLE_ELEMENT, 'price_work');
        $this->dropColumn(self::TABLE_ELEMENT, 'price_material');
        $this->dropColumn(self::TABLE_ELEMENT, 'discount');

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(self::TABLE_RUBRIC, 'discount', "FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Запрос скидки'");
        $this->addColumn(self::TABLE_ELEMENT, 'price_work', "INT(11) NULL DEFAULT NULL COMMENT 'Стоимость работы за 1 единицу'");
        $this->addColumn(self::TABLE_ELEMENT, 'discount', "FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Запрос скидки'");
        $this->addColumn(self::TABLE_ELEMENT, 'price_material', "INT(11) NULL DEFAULT NULL COMMENT 'Стоимость материалов за 1 единицу'");

        Yii::$app->db->schema->refresh();
    }
}
