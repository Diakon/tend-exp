<?php

use yii\db\Migration;

/**
 * Class m180504_110708_table_company_address
 */
class m180504_110708_table_company_address extends Migration
{
    const TABLE_NAME = '{{%company_address}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'country_id' => $this->integer(10)->unsigned()->comment('Страна'),
                'region_id' => $this->integer(10)->unsigned()->comment('Регион'),
                'city_id' => $this->integer(11)->unsigned()->comment('Город'),
                'street' => $this->string(255)->comment('Улица'),
                'house' => $this->string(255)->comment('Дом'),
                'structure' => $this->string(20)->comment('Корпус / строение'),
                'index' => $this->string(20)->comment('Почтовый индекс'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),

            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_address_country_id', self::TABLE_NAME, 'country_id');
            $this->createIndex('IND_company_address_region_id', self::TABLE_NAME, 'region_id');
            $this->createIndex('IND_company_address_city_id', self::TABLE_NAME, 'city_id');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
