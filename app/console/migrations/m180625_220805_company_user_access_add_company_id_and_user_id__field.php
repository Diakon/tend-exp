<?php

use yii\db\Migration;

/**
 * Class m180625_220805_company_user_access_add_company_id_and_user_id__field
 */
class m180625_220805_company_user_access_add_company_id_and_user_id__field extends Migration
{
    const TABLE_NAME = '{{%company_user_access}}';
    const TABLE_USER = '{{%user}}';
    const TABLE_COMPANY = '{{%company_users}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('FK_company_user_access_company_user_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'company_user_id');

        $this->addColumn(self::TABLE_NAME, 'user_id', 'INT(11) NULL DEFAULT NULL COMMENT "Пользователь"');
        $this->createIndex('IND_company_user_access_user_id', self::TABLE_NAME, 'user_id');
        $this->addForeignKey('FK_company_user_access_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id');
        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_company_user_access_user_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'user_id');

        $this->addColumn(self::TABLE_NAME, 'company_user_id', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Пользователь в компании'");
        $this->createIndex('IND_company_user_access_company_user_id', self::TABLE_NAME, 'company_user_id');
        $this->addForeignKey('FK_company_user_access_company_user_id', self::TABLE_NAME, 'company_user_id', self::TABLE_COMPANY, 'id');
        Yii::$app->db->schema->refresh();
    }
}
