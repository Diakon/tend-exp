<?php

use yii\db\Migration;

/**
 * Class m180906_150000_create_entity_file
 */
class m180906_150000_create_entity_file extends Migration
{
    const TABLE_NAME = 'tender_entity_file';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->execute('CREATE TABLE `' . self::TABLE_NAME . '` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор\',
  `guid` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `status` smallint(2) NOT NULL DEFAULT \'0\' COMMENT \'Статус\',
  `title` varchar(512) CHARACTER SET utf8 NOT NULL COMMENT \'Заголовок\',
  `modelName` varchar(512) CHARACTER SET utf8 NOT NULL COMMENT \'Наименование модели\',
  `modelId` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор модели\',
  `modelAttribute` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT \'0\' COMMENT \'Атрибут модели\',
  `fileName` varchar(512) CHARACTER SET utf8 NOT NULL COMMENT \'Наименование файла\',
  `fileAlt` varchar(512) CHARACTER SET utf8 DEFAULT NULL COMMENT \'Описание файла\',
  `fileSize` int(11) NOT NULL DEFAULT \'0\' COMMENT \'Размер файла\',
  `fileExt` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT \'Расширение файла\',
  `order` int(11) NOT NULL DEFAULT \'0\' COMMENT \'Сортировка\',
  `createdAt` timestamp NOT NULL DEFAULT NOW() COMMENT \'Дата создания\',
  `updatedAt` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\' COMMENT \'Дата обновления\',
  PRIMARY KEY (`id`),
  KEY `modelId` (`modelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT=\'Файлы тендеров и объектов\';');

            Yii::$app->db->schema->refresh();

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }

}
