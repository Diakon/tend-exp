<?php

use yii\db\Migration;

/**
 * Class m181210_091411_add_company_count_objects_add_field
 */
class m181210_091411_add_company_count_objects_add_field extends Migration
{
    const DB_TABLE = '{{%company}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'count_objects', 'INT(10) UNSIGNED NULL DEFAULT "0" COMMENT "Количество объектов"');

        Yii::$app->db->getSchema()->refresh();

        foreach (\common\modules\company\models\common\Company::find()->all() as $company) {
            $company->count_objects = $company->getObjects()->count();
            $company->save(false);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME, 'count_objects');

        Yii::$app->db->getSchema()->refresh();
    }
}
