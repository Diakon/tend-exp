<?php

use yii\db\Migration;

/**
 * Class m180913_093902_tenderos_fk_change
 */
class m180913_093902_tenderos_fk_change extends Migration
{
    const TABLE_COMPANY = '{{%company}}';
    const TABLE_COMPANY_USERS = '{{%company_users}}';
    const TABLE_ADDRESS = '{{%company_address}}';
    const TABLE_USERS = '{{%user}}';
    const TABLE_OBJECT = '{{%objects}}';
    const TABLE_TENDERS = '{{%tenders}}';
    const TABLE_OFFERS = '{{%offers}}';
    const TABLE_USERS_ACCESS = '{{%company_user_access}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('FK_company_users_company_id', self::TABLE_COMPANY_USERS);
        $this->addForeignKey('FK_company_users_company_id', self::TABLE_COMPANY_USERS, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');

        $this->dropForeignKey('FK_company_users_user_id', self::TABLE_COMPANY_USERS);
        $this->addForeignKey('FK_company_users_user_id', self::TABLE_COMPANY_USERS, 'user_id', self::TABLE_USERS, 'id', 'CASCADE');

        $this->dropForeignKey('FK_company_projects_company_id', self::TABLE_OBJECT);
        $this->addForeignKey('FK_company_projects_company_id', self::TABLE_OBJECT, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');

        $this->dropForeignKey('FK_company_projects_user_id', self::TABLE_OBJECT);
        $this->addForeignKey('FK_company_projects_user_id', self::TABLE_OBJECT, 'user_id', self::TABLE_USERS, 'id', 'SET NULL');

        $this->dropForeignKey('FK_company_projects_address_id', self::TABLE_OBJECT);
        $this->addForeignKey('FK_company_projects_address_id', self::TABLE_OBJECT, 'address_id', self::TABLE_ADDRESS, 'id', 'SET NULL');

        $this->dropForeignKey('FK_company_tenders_offer_id', self::TABLE_TENDERS);
        $this->addForeignKey('FK_company_tenders_offer_id', self::TABLE_TENDERS, 'offer_id', self::TABLE_OFFERS, 'id', 'SET NULL');

        $this->dropForeignKey('FK_company_tenders_user_id', self::TABLE_TENDERS);
        $this->addForeignKey('FK_company_tenders_user_id', self::TABLE_TENDERS, 'user_id', self::TABLE_USERS, 'id', 'SET NULL');

        $this->dropForeignKey('FK_tenders_object_id', self::TABLE_TENDERS);
        $this->addForeignKey('FK_tenders_object_id', self::TABLE_TENDERS, 'object_id', self::TABLE_OBJECT, 'id', 'CASCADE');

        $this->dropForeignKey('FK_company_actual_address_id', self::TABLE_COMPANY);
        $this->addForeignKey('FK_company_actual_address_id', self::TABLE_COMPANY, 'actual_address_id', self::TABLE_ADDRESS, 'id', 'SET NULL');

        $this->dropForeignKey('FK_company_legal_address_id', self::TABLE_COMPANY);
        $this->addForeignKey('FK_company_legal_address_id', self::TABLE_COMPANY, 'legal_address_id', self::TABLE_ADDRESS, 'id', 'SET NULL');

        $this->dropForeignKey('FK_company_user_access_user_id', self::TABLE_USERS_ACCESS);
        $this->addForeignKey('FK_company_user_access_user_id', self::TABLE_USERS_ACCESS, 'user_id', self::TABLE_USERS, 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_company_users_company_id', self::TABLE_COMPANY_USERS);
        $this->addForeignKey('FK_company_users_company_id', self::TABLE_COMPANY_USERS, 'company_id', self::TABLE_COMPANY, 'id');

        $this->dropForeignKey('FK_company_users_user_id', self::TABLE_COMPANY_USERS);
        $this->addForeignKey('FK_company_users_user_id', self::TABLE_COMPANY_USERS, 'user_id', self::TABLE_USERS, 'id');

        $this->dropForeignKey('FK_company_projects_company_id', self::TABLE_OBJECT);
        $this->addForeignKey('FK_company_projects_company_id', self::TABLE_OBJECT, 'company_id', self::TABLE_COMPANY, 'id');

        $this->dropForeignKey('FK_company_projects_user_id', self::TABLE_OBJECT);
        $this->addForeignKey('FK_company_projects_user_id', self::TABLE_OBJECT, 'user_id', self::TABLE_USERS, 'id');

        $this->dropForeignKey('FK_company_projects_address_id', self::TABLE_OBJECT);
        $this->addForeignKey('FK_company_projects_address_id', self::TABLE_OBJECT, 'address_id', self::TABLE_ADDRESS, 'id');

        $this->dropForeignKey('FK_company_tenders_offer_id', self::TABLE_TENDERS);
        $this->addForeignKey('FK_company_tenders_offer_id', self::TABLE_TENDERS, 'offer_id', self::TABLE_OFFERS, 'id');

        $this->dropForeignKey('FK_company_tenders_user_id', self::TABLE_TENDERS);
        $this->addForeignKey('FK_company_tenders_user_id', self::TABLE_TENDERS, 'user_id', self::TABLE_USERS, 'id');

        $this->dropForeignKey('FK_tenders_object_id', self::TABLE_TENDERS);
        $this->addForeignKey('FK_tenders_object_id', self::TABLE_TENDERS, 'object_id', self::TABLE_OBJECT, 'id');

        $this->dropForeignKey('FK_company_actual_address_id', self::TABLE_COMPANY);
        $this->addForeignKey('FK_company_actual_address_id', self::TABLE_COMPANY, 'actual_address_id', self::TABLE_ADDRESS, 'id', 'CASCADE');

        $this->dropForeignKey('FK_company_legal_address_id', self::TABLE_COMPANY);
        $this->addForeignKey('FK_company_legal_address_id', self::TABLE_COMPANY, 'legal_address_id', self::TABLE_ADDRESS, 'id', 'CASCADE');

        $this->dropForeignKey('FK_company_user_access_user_id', self::TABLE_USERS_ACCESS);
        $this->addForeignKey('FK_company_user_access_user_id', self::TABLE_USERS_ACCESS, 'user_id', self::TABLE_USERS, 'id');
    }
}
