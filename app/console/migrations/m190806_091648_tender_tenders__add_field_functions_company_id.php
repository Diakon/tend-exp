<?php

use yii\db\Migration;

/**
 * Class m190806_091648_tender_tenders__add_field_functions_company_id
 */
class m190806_091648_tender_tenders__add_field_functions_company_id extends Migration
{
    const DB_TABLE = '{{%tenders}}';
    const DB_DIR = '{{%directories}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'function_contractor_id',"INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Функция подрядчика'");
        $this->addForeignKey('FK_tenders_function_contractor_id', self::DB_TABLE, 'function_contractor_id', self::DB_DIR, 'id', 'SET NULL');

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_tenders_function_contractor_id', self::DB_TABLE);
        $this->dropColumn(self::DB_TABLE, 'function_contractor_id');

        Yii::$app->db->getSchema()->refresh();
    }
}
