<?php

use yii\db\Migration;

/**
 * Class m180925_180000_alter_table_pages
 */
class m180925_180000_alter_table_pages extends Migration
{
    const DB_TABLE = '{{%pages}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'type_templates', $this->smallInteger()->defaultValue(1)->comment('тип шаблона страницы'));

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'type_templates');

        Yii::$app->db->getSchema()->refresh();
    }
}
