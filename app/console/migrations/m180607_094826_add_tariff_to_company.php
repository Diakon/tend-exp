<?php

use yii\db\Migration;

/**
 * Class m180607_094826_add_tariff_to_company
 */
class m180607_094826_add_tariff_to_company extends Migration
{
    const TABLE_NAME = '{{%company}}';
    const TABLE_TARIFF = '{{%tariff_plans}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'tariff_plan_id', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Тарифный план'");
        $this->addColumn(self::TABLE_NAME, 'tariff_plan_start', "INT(11) NULL DEFAULT NULL COMMENT 'Дана начала плана'");
        $this->addColumn(self::TABLE_NAME, 'tariff_plan_end', "INT(11) NULL DEFAULT NULL COMMENT 'Дана окончания плана'");
        $this->createIndex('IND_company_tariff_plan_id', self::TABLE_NAME, 'tariff_plan_id');
        $this->addForeignKey('FK_company_tariff_plan_id', self::TABLE_NAME, 'tariff_plan_id', self::TABLE_TARIFF, 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tariff_plan_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'tariff_plan_id');
        $this->dropColumn(self::TABLE_NAME, 'tariff_plan_start');
        $this->dropColumn(self::TABLE_NAME, 'tariff_plan_end');
    }
}
