<?php

use yii\db\Migration;

/**
 * Class m180727_120000_alter_table_object
 */
class m180727_120000_alter_table_object extends Migration
{
    const DB_TABLE = '{{%objects}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'status_complete_id', "SMALLINT(1) DEFAULT '1' COMMENT 'Статус готовности объекта'");

        $this->alterColumn(self::DB_TABLE, 'date_start', 'TIMESTAMP DEFAULT 0 COMMENT "Дата начала реализации"');
        $this->alterColumn(self::DB_TABLE, 'date_end','TIMESTAMP DEFAULT 0 COMMENT "Дата окончания реализации"');
        $this->alterColumn(self::DB_TABLE, 'created_at', 'TIMESTAMP DEFAULT NOW() COMMENT "Дата создания"');
        $this->alterColumn(self::DB_TABLE, 'updated_at','TIMESTAMP DEFAULT 0 COMMENT "Дата изменения"');

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'status_complete_id');

        Yii::$app->db->getSchema()->refresh();
    }
}
