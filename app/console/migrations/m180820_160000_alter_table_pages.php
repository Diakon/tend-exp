<?php

use yii\db\Migration;

/**
 * Class m180820_160000_alter_table_pages
 */
class m180820_160000_alter_table_pages extends Migration
{
    const DB_TABLE = '{{%pages}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'is_important', $this->smallInteger()->defaultValue(0)->comment('Флаг важной новости'));

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'is_important');

        Yii::$app->db->getSchema()->refresh();
    }
}
