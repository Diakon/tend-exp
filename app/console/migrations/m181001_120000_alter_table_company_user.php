<?php

use yii\db\Migration;

/**
 * Class m181001_120000_alter_table_company_user
 */
class m181001_120000_alter_table_company_user extends Migration
{
    const DB_TABLE = '{{%company_users}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'verification', $this->smallInteger()->defaultValue(0)->comment('флаг подтверждения аккаунта'));

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'verification');

        Yii::$app->db->getSchema()->refresh();
    }
}
