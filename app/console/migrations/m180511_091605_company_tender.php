<?php

use yii\db\Migration;

/**
 * Class m180511_091605_company_tender
 */
class m180511_091605_company_tender extends Migration
{
    const TABLE_NAME = '{{%tenders}}';
    const TABLE_PROJECT = '{{%projects}}';
    const TABLE_DIR = '{{%directories}}';
    const TABLE_USER = '{{%user}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'project_id' => $this->integer(10)->unsigned()->comment('Проект'),
                'user_id' => $this->integer(11)->comment('Пользователь'),
                'title' => $this->string(255)->comment('Название тендера'),
                'description' => $this->text()->defaultValue('')->comment('Краткое описание работ'),
                'type' => $this->smallInteger(2)->comment('Тип тендера'),
                'mode' => $this->smallInteger(2)->comment('Вид тендера'),
                'stage' => $this->smallInteger(2)->comment('Этапность тендера'),
                'currency_id' => $this->integer(10)->unsigned()->comment('Валюта'),
                'status' => $this->smallInteger(2)->comment('Статус'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_tenders_project_id', self::TABLE_NAME, 'project_id');
            $this->createIndex('IND_company_tenders_user_id', self::TABLE_NAME, 'user_id');
            $this->createIndex('IND_company_tenders_currency_id', self::TABLE_NAME, 'currency_id');
            $this->createIndex('IND_company_tenders_type', self::TABLE_NAME, 'type');
            $this->createIndex('IND_company_tenders_mode', self::TABLE_NAME, 'mode');
            $this->createIndex('IND_company_tenders_stage', self::TABLE_NAME, 'stage');
            $this->createIndex('IND_company_tenders_status', self::TABLE_NAME, 'status');


            $this->addForeignKey('FK_company_tenders_project_id', self::TABLE_NAME, 'project_id', self::TABLE_PROJECT, 'id');
            $this->addForeignKey('FK_company_tenders_currency_id', self::TABLE_NAME, 'currency_id', self::TABLE_DIR, 'id');
            $this->addForeignKey('FK_company_tenders_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tenders_project_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_tenders_currency_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_tenders_user_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
