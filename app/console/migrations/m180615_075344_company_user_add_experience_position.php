<?php

use yii\db\Migration;

/**
 * Class m180615_075344_company_user_add_experience_position
 */
class m180615_075344_company_user_add_experience_position extends Migration
{
    const TABLE_NAME = '{{%company_users}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'position', 'VARCHAR(255) NULL DEFAULT NULL COMMENT "Должнвость"');
        $this->addColumn(self::TABLE_NAME, 'experience', 'SMALLINT(2) NULL DEFAULT NULL COMMENT "Работаете в компании"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'position');
        $this->dropColumn(self::TABLE_NAME, 'experience');
    }
}
