<?php

use yii\db\Migration;

/**
 * Class m180723_135907_offer_change_table
 */
class m180723_135907_offer_change_table extends Migration
{
    const DB_TABLE_OFFERS = '{{%offers}}';
    const DB_TABLE_OFFERS_ELEMENTS = '{{%offers_elements}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE_OFFERS, 'is_archive', "SMALLINT(1) NULL DEFAULT '0' COMMENT 'Предложение перенесено в архив'");
        $this->dropColumn(self::DB_TABLE_OFFERS, 'price');

        $this->addColumn(self::DB_TABLE_OFFERS_ELEMENTS, 'class_name', "VARCHAR(255) NOT NULL COMMENT 'Модель сметы'");
        $this->addColumn(self::DB_TABLE_OFFERS_ELEMENTS, 'class_id', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'ID модели сметы'");
        $this->addColumn(self::DB_TABLE_OFFERS_ELEMENTS, 'discount', "FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Запрос скидки'");
        $this->createIndex('IND_offers_elements_class_name_class_id', self::DB_TABLE_OFFERS_ELEMENTS, 'class_name, class_id');
        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE_OFFERS, 'is_archive');
        $this->addColumn(self::DB_TABLE_OFFERS, 'price', "DECIMAL(10,2) NULL DEFAULT NULL COMMENT 'Цена'");

        $this->dropColumn(self::DB_TABLE_OFFERS_ELEMENTS, 'class_name');
        $this->dropColumn(self::DB_TABLE_OFFERS_ELEMENTS, 'class_id');
        $this->dropColumn(self::DB_TABLE_OFFERS_ELEMENTS, 'discount');
        Yii::$app->db->getSchema()->refresh();
    }
}
