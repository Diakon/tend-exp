<?php

use yii\db\Migration;

/**
 * Class m190716_143012_tender_offers_elements__add__discount_request__sum_discount_request
 */
class m190716_143012_tender_offers_elements__add__discount_request__sum_discount_request extends Migration
{
    const DB_TABLE = '{{%offers_elements}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'discount_request',"FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Не подтвержденный автором предложения запрос скидки от автора тендера'");
        $this->addColumn(self::DB_TABLE, 'sum_discount_request',"FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Сумма элемнта предложения для не подтвержденного автором предложения запроса скидки'");

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'discount_request');
        $this->dropColumn(self::DB_TABLE, 'sum_discount_request');

        Yii::$app->db->getSchema()->refresh();
    }
}
