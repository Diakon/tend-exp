<?php

use yii\db\Migration;

/**
 * Class m180820_155420_table_offer_add_offer_status
 */
class m180820_155420_table_offer_add_offer_status extends Migration
{
    const TABLE_OFFERS = '{{%offers}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_OFFERS, 'status', "TINYINT(2) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Статус предложения'");

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_OFFERS, 'status');

        Yii::$app->db->schema->refresh();
    }
}
