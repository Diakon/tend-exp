<?php

use yii\db\Migration;

/**
 * Class m180718_091934_add_count_and_unit_id_to_objects_completed_works_table
 */
class m180718_091934_add_count_and_unit_id_to_objects_completed_works_table extends Migration
{
    const TABLE_NAME = '{{%objects_completed_works}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(self::TABLE_NAME, 'area');
        $this->addColumn(self::TABLE_NAME, 'count', "INT(11) UNSIGNED NOT NULL COMMENT 'Количество'");
        $this->addColumn(self::TABLE_NAME, 'unit_id', "SMALLINT(2) UNSIGNED NULL DEFAULT NULL COMMENT 'Единица измерения'");

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'count');
        $this->dropColumn(self::TABLE_NAME, 'unit_id');
        $this->addColumn(self::TABLE_NAME, 'area', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Площадь в м3'");

        Yii::$app->db->schema->refresh();
    }

}
