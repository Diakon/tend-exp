<?php

use yii\db\Migration;

/**
 * Class m180816_153410_company_user_access_add_company_id
 */
class m180816_153410_company_user_access_add_company_id extends Migration
{
    const TABLE_ACCESS = '{{%company_user_access}}';
    const TABLE_COMPANY = '{{%company}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_ACCESS, 'company_id', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Компания'");
        $this->createIndex('IND_company_user_access_company_id', self::TABLE_ACCESS, 'company_id');
        $this->addForeignKey('FK_company_user_access_company_id', self::TABLE_ACCESS, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_company_user_access_company_id', self::TABLE_ACCESS);
        $this->dropColumn(self::TABLE_ACCESS, 'company_id');

        Yii::$app->db->schema->refresh();
    }
}
