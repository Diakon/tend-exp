<?php

use yii\db\Migration;

/**
 * Class m180911_170000_create_comment_company_table
 */
class m180911_170000_create_comment_company_table extends Migration
{
    const TABLE_NAME = '{{%company_comment}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('id компании'),
                'object_id' => $this->integer(10)->unsigned()->comment('id объекта'),
                'comment' => $this->text()->comment('комментарий'),

                'created_by' => $this->integer(10)->unsigned()->comment('id создателя коммента'),
                'updated_by' => $this->integer(10)->unsigned()->comment('id пользователя последнего обновления'),
                'created_at' => 'TIMESTAMP DEFAULT NOW() COMMENT "Дата создания"',
                'updated_at' => 'TIMESTAMP DEFAULT 0 COMMENT "Дата изменения"',
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Отзывы компании"');


            $this->createIndex('company_id', self::TABLE_NAME, 'company_id');
            $this->createIndex('created_by', self::TABLE_NAME, 'created_by');
            $this->createIndex('created_at', self::TABLE_NAME, 'created_at');

        }

        Yii::$app->db->schema->refresh();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }

}
