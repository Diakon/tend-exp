<?php

use yii\db\Migration;

/**
 * Class m180503_121821_geo_table_mun
 */
class m180503_121821_geo_table_mun extends Migration
{
    const TABLE_NAME = '{{%geo_mun}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $geo = file_get_contents(Yii::getAlias('@console') . '/migrations/dump/mun.sql');
        if ($geo) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT=1';
            }

            $this->createTable(self::TABLE_NAME, [
                'mun_id' => "int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'",
                'name' => "varchar(255) NOT NULL COMMENT 'Название'",
                'region_id' => "int(10) unsigned NOT NULL COMMENT 'Id региона'",
                'popul_all' => "int(11) NOT NULL DEFAULT '100000' COMMENT 'Начеление всего'",
                'popul_municipal' => "int(11) NOT NULL DEFAULT '100000' COMMENT 'Муниципальное начеление'",
                'popul_regional' => "int(11) NOT NULL DEFAULT '100000' COMMENT 'Начеление ркгиона'",
                'status' => "TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Статус'",
                'PRIMARY KEY (mun_id)',
            ], $tableOptions);

            $this->execute($geo);
            $this->renameColumn(self::TABLE_NAME, 'mun_id', 'id');
            Yii::$app->db->getSchema()->refresh();
        } else {
            echo 'File not found';
            Yii::$app->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
