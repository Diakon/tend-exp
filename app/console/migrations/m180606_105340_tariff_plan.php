<?php

use yii\db\Migration;

/**
 * Class m180606_105340_tariff_plan
 */
class m180606_105340_tariff_plan extends Migration
{
    const TABLE_NAME = '{{%tariff_plans}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'title' => $this->string(255)->comment('Название тарифа'),
                'description' => $this->text()->defaultValue('')->comment('Краткое описание тарифа'),
                'count_users_id' => $this->smallInteger(2)->comment('Количество пользователей'),
                'count_offers_id' => $this->smallInteger(2)->comment('Количество предложений'),
                'price' => $this->decimal(10, 2)->comment('Цена'),
                'status' => $this->smallInteger(2)->comment('Статус'),
                'sort' => $this->integer(11)->defaultValue(1)->comment('Сортировка'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_tariff_plans_title', self::TABLE_NAME, 'title', true);
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
