<?php

use yii\db\Migration;

/**
 * Class m181114_103530_objects_links_table
 */
class m181114_103530_objects_links_table extends Migration
{
    const TABLE_NAME = '{{%objects_links}}';
    const TABLE_OBJECT = '{{%objects}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'object_id' => $this->integer(10)->unsigned()->notNull()->comment('Объект'),
                'link' => $this->string(255)->notNull()->comment('Ссылка'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB COMMENT="Комментарии к заявка"');

            $this->createIndex('IND_objects_links_object_id', self::TABLE_NAME, 'object_id');

            $this->addForeignKey('FK_objects_links_object_id', self::TABLE_NAME, 'object_id', self::TABLE_OBJECT, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_objects_links_object_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
