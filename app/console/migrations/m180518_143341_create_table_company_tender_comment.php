<?php

use yii\db\Migration;

/**
 * Class m180518_143341_create_table_company_tender_comment
 */
class m180518_143341_create_table_company_tender_comment extends Migration
{
    const TABLE_NAME = '{{%comment}}';
    const TABLE_USER = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'userId' => $this->integer(11)->comment('Пользователь'),
                'isRead' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->comment('Прочитанно или нет'),
                'className' => $this->string(255)->notNull()->comment('Название класса сущности'),
                'entityId' => $this->bigInteger()->unsigned()->notNull()->comment('Id сущности к которой прикреплен коммент'),
                'text' => $this->text()->comment('текст комментария'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB COMMENT="Комментарии к заявка"');

            $this->createIndex('IND_comment_userId', self::TABLE_NAME, 'userId');
            $this->createIndex('IND_comment_isRead', self::TABLE_NAME, 'isRead');
            $this->createIndex('IND_comment_class', self::TABLE_NAME, 'className, entityId');

            $this->addForeignKey('FK_comment_user_id', self::TABLE_NAME, 'userId', self::TABLE_USER, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_comment_user_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
