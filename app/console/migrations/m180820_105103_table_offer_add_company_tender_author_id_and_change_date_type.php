<?php

use yii\db\Migration;

/**
 * Class m180820_105103_table_offer_add_company_tender_author_id_and_change_date_type
 */
class m180820_105103_table_offer_add_company_tender_author_id_and_change_date_type extends Migration
{
    const TABLE_OFFERS = '{{%offers}}';
    const TABLE_OFFERS_ELEMENTS = '{{%offers_elements}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_OFFERS, 'company_author_id', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Компания - автор тендера'");
        $this->alterColumn(self::TABLE_OFFERS, 'created_at', "TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата создания предложения'");
        $this->alterColumn(self::TABLE_OFFERS, 'updated_at', "TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата редактирования предложения'");

        $this->alterColumn(self::TABLE_OFFERS_ELEMENTS, 'created_at', "TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата создания предложения'");
        $this->alterColumn(self::TABLE_OFFERS_ELEMENTS, 'updated_at', "TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата редактирования предложения'");

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_OFFERS, 'company_author_id');
        $this->alterColumn(self::TABLE_OFFERS, 'created_at', "INT(11) NOT NULL COMMENT 'Дата создания предложения'");
        $this->alterColumn(self::TABLE_OFFERS, 'updated_at', "INT(11) NOT NULL COMMENT 'Дата редактирования предложения'");

        $this->alterColumn(self::TABLE_OFFERS_ELEMENTS, 'created_at', "INT(11) NOT NULL COMMENT 'Дата создания предложения'");
        $this->alterColumn(self::TABLE_OFFERS_ELEMENTS, 'updated_at', "INT(11) NOT NULL COMMENT 'Дата редактирования предложения'");

        Yii::$app->db->schema->refresh();
    }
}
