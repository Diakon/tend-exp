<?php
use yii\db\Migration;

/**
 * Class m180717_170000_alter_table_tenders
 */
class m180717_170000_alter_table_tenders extends Migration
{
    const TABLE_NAME = '{{%tenders}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'company_id', $this->integer()->notNull()->comment('ID компании'));
        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'company_id');
        Yii::$app->db->getSchema()->refresh();
    }
}
