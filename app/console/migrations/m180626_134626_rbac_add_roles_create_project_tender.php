<?php

use common\components\AuthDbManager;
use yii\db\Migration;

/**
 * Class m180626_134626_rbac_add_roles_create_project_tender
 */
class m180626_134626_rbac_add_roles_create_project_tender extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rule = new frontend\rbac\AccessCompanyRule();
        $userOrg = Yii::$app->authManager->getRole(AuthDbManager::ROLE_CLIENT);

        $permit = Yii::$app->authManager->createPermission('addProject');
        $permit->description = 'Право создавать новый проект';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        $permit = Yii::$app->authManager->createPermission('addTender');
        $permit->description = 'Право создавать новый тендер';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $client = Yii::$app->authManager->getRole('client');
        Yii::$app->authManager->remove($client);

        Yii::$app->db->getSchema()->refresh();
    }
}
