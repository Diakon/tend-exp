<?php

use yii\db\Migration;

/**
 * Class m180727_170000_alter_table_object
 */
class m180727_170000_alter_table_object extends Migration
{
    const DB_TABLE = '{{%objects}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'employerName', $this->string(255)->defaultValue('')->comment('Заказчик'));
        $this->addColumn(self::DB_TABLE, 'employer_id', $this->string(255)->defaultValue('')->comment('Id Заказчика если существует'));

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'employerName');
        $this->dropColumn(self::DB_TABLE, 'employer_id');

        Yii::$app->db->getSchema()->refresh();
    }
}
