<?php

use yii\db\Migration;

/**
 * Class m180717_095739_projects_to_objects
 */
class m180717_095739_projects_to_objects extends Migration
{
    const TABLE_OLD_NAME = '{{%projects}}';
    const TABLE_OLD_NAME_WORKS = '{{%project_works}}';

    const TABLE_NAME = '{{%objects}}';
    const TABLE_NAME_ACTIVITIES = '{{%objects_activities}}';
    const TABLE_NAME_TYPE = '{{%objects_type}}';
    const TABLE_NAME_FUNCTIONS = '{{%objects_functions}}';
    const TABLE_NAME_WORKS = '{{%objects_works}}';
    const TABLE_NAME_COMPLETED_WORKS = '{{%objects_completed_works}}';

    const TABLE_COMPANY = '{{%company}}';
    const TABLE_ADDRESS = '{{%company_address}}';
    const TABLE_DIR = '{{%directories}}';
    const TABLE_USER = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('FK_company_projects_type_id', self::TABLE_OLD_NAME);
        $this->dropForeignKey('FK_company_project_works_project_id', self::TABLE_OLD_NAME_WORKS);
        $this->dropForeignKey('FK_company_project_works_work_id', self::TABLE_OLD_NAME_WORKS);

        $this->renameTable(self::TABLE_OLD_NAME_WORKS, self::TABLE_NAME_COMPLETED_WORKS);
        $this->renameTable(self::TABLE_OLD_NAME, self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'type_id');
        $this->addColumn(self::TABLE_NAME, 'description', "TEXT NULL COMMENT 'Описание работ'");

        $this->renameColumn(self::TABLE_NAME_COMPLETED_WORKS, 'project_id', 'object_id');
        $this->createIndex('IND_objects_completed_works_object_id', self::TABLE_NAME_COMPLETED_WORKS, 'object_id');
        $this->addForeignKey('FK_objects_completed_works_object_id', self::TABLE_NAME_COMPLETED_WORKS, 'object_id', self::TABLE_NAME, 'id', 'CASCADE');
        $this->createIndex('IND_objects_completed_works_work_id', self::TABLE_NAME_COMPLETED_WORKS, 'work_id');
        $this->addForeignKey('FK_objects_completed_works_work_id', self::TABLE_NAME_COMPLETED_WORKS, 'work_id', self::TABLE_DIR, 'id', 'CASCADE');


        // Создаю связаные таблицы
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME_ACTIVITIES, true) === null) {
            $this->createTable(self::TABLE_NAME_ACTIVITIES, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'object_id' => $this->integer(10)->unsigned()->comment('Объект'),
                'activity_id' => $this->integer(10)->unsigned()->comment('Направление сторительства'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_objects_activities_object_id', self::TABLE_NAME_ACTIVITIES, 'object_id');
            $this->createIndex('IND_objects_activities_activity_id', self::TABLE_NAME_ACTIVITIES, 'activity_id');
            $this->addForeignKey('FK_objects_activities_object_id', self::TABLE_NAME_ACTIVITIES, 'object_id', self::TABLE_NAME, 'id', 'CASCADE');
            $this->addForeignKey('FK_objects_activities_activity_id', self::TABLE_NAME_ACTIVITIES, 'activity_id', self::TABLE_DIR, 'id', 'CASCADE');
        }
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME_TYPE, true) === null) {
            $this->createTable(self::TABLE_NAME_TYPE, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'object_id' => $this->integer(10)->unsigned()->comment('Объект'),
                'type_id' => $this->integer(10)->unsigned()->comment('Тип'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_objects_type_object_id', self::TABLE_NAME_TYPE, 'object_id');
            $this->createIndex('IND_objects_type_type_id', self::TABLE_NAME_TYPE, 'type_id');
        }
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME_FUNCTIONS, true) === null) {
            $this->createTable(self::TABLE_NAME_FUNCTIONS, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'object_id' => $this->integer(10)->unsigned()->comment('Объект'),
                'function_id' => $this->integer(10)->unsigned()->comment('Функции'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_objects_functions_object_id', self::TABLE_NAME_FUNCTIONS, 'object_id');
            $this->createIndex('IND_objects_functions_function_id', self::TABLE_NAME_FUNCTIONS, 'function_id');
        }
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME_WORKS, true) === null) {
            $this->createTable(self::TABLE_NAME_WORKS, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'object_id' => $this->integer(10)->unsigned()->comment('Объект'),
                'work_id' => $this->integer(10)->unsigned()->comment('Вид работы'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_objects_works_object_id', self::TABLE_NAME_WORKS, 'object_id');
            $this->createIndex('IND_objects_works_work_id', self::TABLE_NAME_WORKS, 'work_id');
            $this->addForeignKey('FK_objects_works_object_id', self::TABLE_NAME_WORKS, 'object_id', self::TABLE_NAME, 'id', 'CASCADE');
            $this->addForeignKey('FK_objects_works_work_id', self::TABLE_NAME_WORKS, 'work_id', self::TABLE_DIR, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME_ACTIVITIES);
        $this->dropTable(self::TABLE_NAME_TYPE);
        $this->dropTable(self::TABLE_NAME_FUNCTIONS);
        $this->dropTable(self::TABLE_NAME_WORKS);
        $this->renameTable(self::TABLE_NAME_COMPLETED_WORKS, self::TABLE_OLD_NAME_WORKS);
        $this->renameTable(self::TABLE_NAME, self::TABLE_OLD_NAME);

        $this->createIndex('IND_company_projects_company_id', self::TABLE_NAME, 'company_id');
        $this->createIndex('IND_company_projects_user_id', self::TABLE_NAME, 'user_id');
        $this->createIndex('IND_company_projects_type_id', self::TABLE_NAME, 'type_id');
        $this->createIndex('IND_company_projects_address_id', self::TABLE_NAME, 'address_id');
        $this->createIndex('IND_company_projects_status', self::TABLE_NAME, 'status');

        $this->addForeignKey('FK_company_projects_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id');
        $this->addForeignKey('FK_company_projects_type_id', self::TABLE_NAME, 'type_id', self::TABLE_DIR, 'id');
        $this->addForeignKey('FK_company_projects_address_id', self::TABLE_NAME, 'address_id', self::TABLE_ADDRESS, 'id');
        $this->addForeignKey('FK_company_projects_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id');


        Yii::$app->db->schema->refresh();
    }
}
