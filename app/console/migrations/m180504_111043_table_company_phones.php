<?php

use yii\db\Migration;

/**
 * Class m180504_111043_table_company_phones
 */
class m180504_111043_table_company_phones extends Migration
{
    const TABLE_NAME = '{{%company_phones}}';
    const TABLE_COMPANY = '{{%company}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компания'),
                'phone' => $this->string(20)->comment('Телефон'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),

            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_phones_company_id', self::TABLE_NAME, 'company_id');
            $this->addForeignKey('FK_company_phones_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_phones_company_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
