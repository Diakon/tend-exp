<?php

use yii\db\Migration;

/**
 * Class m180607_085350_update_tariff_plan
 */
class m180607_085350_update_tariff_plan extends Migration
{
    const TABLE_NAME = '{{%tariff_plans}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'type', "SMALLINT(1) UNSIGNED NOT NULL COMMENT 'Тип тарифа'");
        $this->addColumn(self::TABLE_NAME, 'count_month', "SMALLINT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Срок (в месяцах)'");
        $this->createIndex('IND_tariff_plans_type', self::TABLE_NAME, 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'type');
        $this->dropColumn(self::TABLE_NAME, 'count_month');
    }
}
