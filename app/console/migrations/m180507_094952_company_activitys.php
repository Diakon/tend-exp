<?php

use yii\db\Migration;

/**
 * Class m180507_094952_company_activitys
 */
class m180507_094952_company_activitys extends Migration
{
    const TABLE_NAME = '{{%company_activitys}}';
    const TABLE_ADDRESS = '{{%company_address}}';
    const TABLE_DIRECTORIES = '{{%directories}}';
    const TABLE_COMPANY = '{{%company}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'address_id' => $this->integer(10)->unsigned()->comment('Адресс'),
                'function_id' => $this->integer(10)->unsigned()->comment('Функции компании'),
                'main_work_id' => $this->integer(10)->unsigned()->comment('Направление деятельности'),
                'types_work_id' => $this->integer(10)->unsigned()->comment('Виды работ'),
                'employees_now' => $this->smallInteger(5)->comment('Количество сотрудников сейчас'),
                'employees_max' => $this->smallInteger(5)->comment('Количество сотрудников максимуми'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_activitys_company_id', self::TABLE_NAME, 'company_id');
            $this->createIndex('IND_company_activitys_address_id', self::TABLE_NAME, 'address_id');
            $this->createIndex('IND_company_activitys_function_id', self::TABLE_NAME, 'function_id');
            $this->createIndex('IND_company_activitys_main_work_id', self::TABLE_NAME, 'main_work_id');
            $this->createIndex('IND_company_activitys_types_work_id', self::TABLE_NAME, 'types_work_id');

            $this->addForeignKey('FK_company_activitys_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id');
            $this->addForeignKey('FK_company_activitys_address_id', self::TABLE_NAME, 'address_id', self::TABLE_ADDRESS, 'id');
            $this->addForeignKey('FK_company_activitys_function_id', self::TABLE_NAME, 'function_id', self::TABLE_DIRECTORIES, 'id');
            $this->addForeignKey('FK_company_activitys_main_work_id', self::TABLE_NAME, 'main_work_id', self::TABLE_DIRECTORIES, 'id');
            $this->addForeignKey('FK_company_activitys_types_work_id', self::TABLE_NAME, 'types_work_id', self::TABLE_DIRECTORIES, 'id');

        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_activitys_company_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_activitys_address_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_activitys_function_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_activitys_main_work_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_activitys_types_work_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
