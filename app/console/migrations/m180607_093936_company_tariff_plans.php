<?php

use yii\db\Migration;

/**
 * Class m180607_093936_company_tariff_plans
 */
class m180607_093936_company_tariff_plans extends Migration
{
    const TABLE_NAME = '{{%tariff_history}}';
    const TABLE_COMPANY = '{{%company}}';
    const TABLE_TARIFF = '{{%tariff_plans}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компания'),
                'tariff_plan_id' => $this->integer(10)->unsigned()->comment('Тарифный план'),
                'date_start' => $this->integer(11)->notNull()->comment('Дата начала тарифного плана'),
                'date_end' => $this->integer(11)->comment('Дата окончания тарифного плана'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_tariff_history_company_id', self::TABLE_NAME, 'company_id');
            $this->createIndex('IND_company_tariff_history_tariff_plan_id', self::TABLE_NAME, 'tariff_plan_id');
            $this->addForeignKey('FK_company_tariff_history_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_tariff_history_tariff_plan_id', self::TABLE_NAME, 'tariff_plan_id', self::TABLE_TARIFF, 'id');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tariff_history_company_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_tariff_history_tariff_plan_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
