<?php

use yii\db\Migration;
use \common\components\AuthDbManager;

/**
 * Class m180507_142550_add_rbac_role
 */
class m180507_142550_add_rbac_role extends Migration
{
    public function up()
    {
        $clientRoleName = AuthDbManager::ROLE_CLIENT;
        // Роль Клиент
        $client = Yii::$app->authManager->createRole($clientRoleName);
        $client->description = 'Роль клиента';
        Yii::$app->authManager->add($client);

        // Создаю пермишены
        $rule = new frontend\rbac\AccessCompanyRule();
        Yii::$app->authManager->add($rule);
        $userOrg = Yii::$app->authManager->getRole(AuthDbManager::ROLE_CLIENT);

        $permit = Yii::$app->authManager->createPermission($clientRoleName . 'editCompany');
        $permit->description = 'Право редактировать данные о компании';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        $permit = Yii::$app->authManager->createPermission('addClientCompany');
        $permit->description = 'Право добавлять нового клиента к компании';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);


        $permit = Yii::$app->authManager->createPermission('editClientCompany');
        $permit->description = 'Право редактировать клиента компании';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        $permit = Yii::$app->authManager->createPermission('deleteClientCompany');
        $permit->description = 'Право удалить клиента в компании';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        $permit = Yii::$app->authManager->createPermission('accessProject');
        $permit->description = 'Право доступа к проектам компании';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        $permit = Yii::$app->authManager->createPermission('accessTender');
        $permit->description = 'Право доступа к тендерам компании';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);

        $permit = Yii::$app->authManager->createPermission('accessPay');
        $permit->description = 'Право доступа к платежам компании';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        Yii::$app->db->getSchema()->refresh();

    }

    public function down()
    {
        $client = Yii::$app->authManager->getRole('client');
        Yii::$app->authManager->remove($client);

        Yii::$app->db->getSchema()->refresh();
    }
}