<?php

use yii\db\Migration;

/**
 * Добавление таблицы иникальных достижений
 */
class m180705_170000_add_table_unique_features extends Migration
{
    const DB_TABLE= '{{%unique_features}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema(self::DB_TABLE, true) === null) {
            $this->createTable(self::DB_TABLE, [
                'id' => 'int(11) NOT NULL AUTO_INCREMENT COMMENT "Идентификатор"',
                'title' => 'varchar(255) NOT NULL COMMENT "Заголовок"',
                'titleSecond' => 'varchar(255) NOT NULL COMMENT "Заголовок 2"',
                'image' => 'varchar(255) NOT NULL COMMENT "Фотография"',
                'url' => 'varchar(255) NOT NULL COMMENT "Сслыка"',
                'text' => 'text NOT NULL COMMENT "Текст слайдера"',
                'publish' => 'tinyint(1) NOT NULL DEFAULT "0" COMMENT "Опубликовано"',
                'sort' => 'int(11) NOT NULL DEFAULT "0" COMMENT "Порядковый номер "',
                'created_at' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT "Дата создания"',
                'updated_at' => 'TIMESTAMP DEFAULT 0 COMMENT "Дата изменения"',
                'PRIMARY KEY (id)',
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB COMMENT="унакальные достижения (на главной)"');

            Yii::$app->db->getSchema()->refresh();
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::DB_TABLE);
        Yii::$app->db->getSchema()->refresh();
    }
}

