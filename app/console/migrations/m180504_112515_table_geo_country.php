<?php

use yii\db\Migration;
use common\models\GeoRegion;

/**
 * Class m180504_112515_table_geo_country
 */
class m180504_112515_table_geo_country extends Migration
{
    const TABLE_NAME = '{{%geo_country}}';
    const TABLE_REGION = '{{%geo_region}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'name' => $this->string(255)->comment('Название'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_geo_country_name', self::TABLE_NAME, 'name', true);
        }

        $countryDump = <<<SQL
INSERT INTO `tender_geo_country` VALUES (1, 'Российская Федерация');
SQL;
        Yii::$app->db->createCommand($countryDump)->execute();

        // Добавляю поле страна в таблицу регионов и проставялю страну - Россиия
        $this->addColumn(self::TABLE_REGION, 'country_id', 'INT(11) NOT NULL');
        $this->createIndex('IND_geo_region_country_id', self::TABLE_REGION, 'country_id');

        GeoRegion::updateAll([
            'country_id' => 1,
        ]);

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
