<?php

use yii\db\Migration;

/**
 * Class m180504_101932_table_company
 */
class m180504_101932_table_company extends Migration
{
    const TABLE_NAME = '{{%company}}';

    public function safeUp()
    {

        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'url' => $this->string(255)->comment('ссылка на страницу'),
                'title' => $this->string(255)->comment('Название'),
                'ogrn' => $this->string(20)->comment('ОГРН'),
                'inn' => $this->string(20)->comment('ИНН'),
                'kpp' => $this->string(20)->comment('КПП'),
                'year' => $this->integer(11)->comment('год основания'),
                'general_manage' => $this->string(255)->comment('Генеральный директор'),
                'employees_now' => $this->smallInteger(5)->comment('Количество сотрудников сейчас'),
                'employees_max' => $this->smallInteger(5)->comment('Количество сотрудников максимуми'),
                'legal_address_id' => $this->integer(10)->unsigned()->comment('Юридический адрес'),
                'actual_address_id' => $this->integer(10)->unsigned()->comment('Фактический адрес'),
                'address_same' => $this->smallInteger(1)->comment('Совпадает с юридическим'),
                'fax' => $this->string(20)->comment('Факс'),
                'status' => $this->smallInteger(2)->comment('Статус'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),

            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_url', self::TABLE_NAME, 'url', true);
            $this->createIndex('IND_company_status', self::TABLE_NAME, 'status');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
