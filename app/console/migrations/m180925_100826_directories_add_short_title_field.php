<?php

use yii\db\Migration;

/**
 * Class m180925_100826_directories_add_short_title_field
 */
class m180925_100826_directories_add_short_title_field extends Migration
{
    const TABLE_OFFERS = '{{%directories}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_OFFERS, 'title_short', "VARCHAR(256) NULL DEFAULT NULL COMMENT 'Краткое наименование'");


        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_OFFERS, 'title_short');

        
        Yii::$app->db->schema->refresh();
    }
}
