<?php

use yii\db\Migration;

/**
 * Class m180503_120357_geo_table
 */
class m180503_120357_geo_table_area extends Migration
{
    const TABLE_NAME = '{{%geo_area}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $city = file_get_contents(Yii::getAlias('@console') . '/migrations/dump/area.sql');
        if ($city) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT=1';
            }

            $this->createTable(self::TABLE_NAME, [
                'id' => "int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'",
                'name' => "varchar(255) NOT NULL COMMENT 'Название'",
                'abbr' => "varchar(10) NOT NULL COMMENT 'Абивиатура'",
                'PRIMARY KEY (id)',
            ], $tableOptions);

            $this->execute($city);

            Yii::$app->db->getSchema()->refresh();
        } else {
            echo 'File not found';
            Yii::$app->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable(self::TABLE_NAME);
    }
}
