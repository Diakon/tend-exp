<?php

use yii\db\Migration;
use common\modules\company\models\common\Company;

/**
 * Class m190703_122730_company_publish_status
 */
class m190703_122730_company_publish_status extends Migration
{
    const TABLE_NAME = '{{%company}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'publish_catalog', "SMALLINT(2) NULL DEFAULT NULL COMMENT 'Статус публикации в каталоге компаний'");
        // Проставляю всем уже созданым компаниям статус публикации в каталоге компаний
        Company::updateAll(['publish_catalog' => Company::STATUS_PUBLISH_CATALOG_YES]);

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'publish_catalog');


        Yii::$app->db->schema->refresh();
    }
}
