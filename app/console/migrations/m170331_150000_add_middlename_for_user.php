<?php
use yii\db\Migration;

/**
 *
 * Class m170331_150000_add_middlename_for_user
 */
class m170331_150000_add_middlename_for_user extends Migration
{
    const TABLE_NAME = '{{%user}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'middlename', "varchar(255) NULL COMMENT 'Отчество' AFTER lastname");
        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'middlename');
        Yii::$app->db->getSchema()->refresh();
    }
}
