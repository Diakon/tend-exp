<?php

use yii\db\Migration;

/**
 * Class m180503_121827_geo_table_region
 */
class m180503_121827_geo_table_region extends Migration
{
    const TABLE_NAME = '{{%geo_region}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $geo = file_get_contents(Yii::getAlias('@console') . '/migrations/dump/region.sql');
        if ($geo) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT=1';
            }

            $this->createTable(self::TABLE_NAME, [
                'region_id' => "int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'",
                'area_id' => "int(10) unsigned NOT NULL COMMENT 'ID из geo_area'",
                'name' => "varchar(255) NOT NULL COMMENT 'Наименование'",
                'coord_lon' => "decimal(9,6) NOT NULL COMMENT 'долгота центра'",
                'coord_lat' => "decimal(9,6) NOT NULL COMMENT 'широта центра'",
                'popul_all' => "int(10) unsigned NOT NULL COMMENT 'Население всего'",
                'popul_regional' => "int(10) unsigned NOT NULL DEFAULT '99999' COMMENT 'Население региона'",
                'PRIMARY KEY (region_id)',
            ], $tableOptions);

            $this->execute($geo);
            $this->createIndex('IND_geo_region_area_id', self::TABLE_NAME, 'area_id');

            $this->renameColumn(self::TABLE_NAME, 'region_id', 'id');

            Yii::$app->db->getSchema()->refresh();
        } else {
            echo 'File not found';
            Yii::$app->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
