<?php

use yii\db\Migration;

/**
 * Class m180522_082225_create_table_company_tender_estimates_template
 */
class m180522_082225_create_table_company_tender_estimates_template extends Migration
{
    const TABLE_NAME = '{{%estimates_template}}';
    const TABLE_USER = '{{%user}}';
    const TABLE_COMPANY = '{{%company}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'user_id' => $this->integer(11)->comment('Автор щаблона'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компания'),
                'title' => $this->string(255)->notNull()->comment('Название шаблона'),
                'estimates_data' => $this->text()->comment('Содержимое щаблона'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_tender_estimates_template_user_id', self::TABLE_NAME, 'user_id');
            $this->createIndex('IND_company_tender_estimates_template_company_id', self::TABLE_NAME, 'company_id');

            $this->addForeignKey('FK_company_tender_estimates_template_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_tender_estimates_template_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tender_estimates_template_user_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_tender_estimates_template_company_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
