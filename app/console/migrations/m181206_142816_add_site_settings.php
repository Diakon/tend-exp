<?php

use yii\db\Expression;
use yii\db\Migration;
use common\modules\settings\models\Setting;

/**
 * Class m181206_142816_add_site_settings
 */
class m181206_142816_add_site_settings extends Migration
{
    private $addRows = [
        'inn' => 'ИНН',
        'ogrn' => 'ОГРН',
        'terms_use' => 'Ссылка на пользовательское соглашение',
    ];

    /**
     * {@inheritdoc}
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        foreach ($this->addRows as $key => $title) {
            Yii::$app->db->createCommand()
                ->insert(Setting::tableName(), [
                    'key' => $key,
                    'title' => $title,
                    'status' => 1,
                    'type' => $key == 'string',
                    'section' => 'site',
                    'createdAt' => new Expression('NOW()'),
                    'updatedAt' => new Expression('NOW()'),
                ])
                ->execute();
        }
        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        foreach ($this->addRows as $key => $title) {
            Yii::$app->db->createCommand()
                ->delete(Setting::tableName(), ['key' => $key])
                ->execute();
        }
        Yii::$app->db->schema->refresh();
    }

}
