<?php

use yii\db\Migration;

/**
 * Class m180718_150520_change_project_id_to_object_id_fields
 */
class m180718_150520_change_project_id_to_object_id_fields extends Migration
{
    const DB_TABLE_TENDERS = '{{%tenders}}';
    const DB_TABLE_OBJECTS = '{{%objects}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('FK_company_tenders_project_id', self::DB_TABLE_TENDERS);
        $this->renameColumn(self::DB_TABLE_TENDERS, 'project_id', 'object_id');
        $this->addForeignKey('FK_tenders_object_id', self::DB_TABLE_TENDERS, 'object_id', self::DB_TABLE_OBJECTS, 'id');
        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_tenders_object_id', self::DB_TABLE_TENDERS);
        $this->renameColumn(self::DB_TABLE_TENDERS, 'object_id', 'project_id');
        $this->addForeignKey('FK_tenders_project_id', self::DB_TABLE_TENDERS, 'project_id', self::DB_TABLE_OBJECTS, 'id');
        Yii::$app->db->getSchema()->refresh();
    }
}
