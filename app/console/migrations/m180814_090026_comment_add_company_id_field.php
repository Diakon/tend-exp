<?php

use yii\db\Migration;

/**
 * Class m180814_090026_comment_add_company_id_field
 */
class m180814_090026_comment_add_company_id_field extends Migration
{
    const DB_TABLE = '{{%comment}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'companyId', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Компания, к которой относится автор отзыва'");
        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'companyId');
        Yii::$app->db->getSchema()->refresh();
    }
}
