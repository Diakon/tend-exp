<?php

use yii\db\Migration;

/**
 * Class m180503_121833_geo_table_city
 */
class m180503_121833_geo_table_city extends Migration
{
    const TABLE_NAME = '{{%geo_city}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $geo = file_get_contents(Yii::getAlias('@console') . '/migrations/dump/city.sql');
        if ($geo) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT=1';
            }

            $this->createTable(self::TABLE_NAME, [
                'city_id' => "int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор'",
                'name' => "varchar(255) NOT NULL COMMENT 'Наименование'",
                'region_id' => "int(11) NOT NULL COMMENT 'Регион'",
                'popul_all' => "int(11) NOT NULL DEFAULT '100000' COMMENT 'население'",
                'popul_municipal' => "int(11) NOT NULL DEFAULT '100000' COMMENT 'Муниципальное население'",
                'popul_regional' => "int(11) NOT NULL DEFAULT '100000' COMMENT 'Население региона'",
                'PRIMARY KEY (city_id)',
            ], $tableOptions);

            $this->execute($geo);
            $this->createIndex('IND_geo_city_region_id', self::TABLE_NAME, 'region_id');
            $this->renameColumn(self::TABLE_NAME, 'city_id', 'id');

            Yii::$app->db->getSchema()->refresh();

        } else {
            echo 'File not found';
            Yii::$app->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}

