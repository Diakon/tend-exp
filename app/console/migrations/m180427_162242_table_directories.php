<?php

use yii\db\Migration;

/**
 * Class m180427_162242_table_directories
 */
class m180427_162242_table_directories extends Migration
{
    const DB_TABLE = '{{%directories}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::DB_TABLE, [
            'id' => $this->primaryKey()->unsigned()->comment('Идентификатор'),
            'parent_id' => $this->integer(11)->unsigned()->comment('ID родительской записи'),
            'type' => $this->string(55)->notNull()->comment('Тип справочника'),
            'title' => $this->string(256)->comment('Наименование справочника'),
            'unit' => $this->smallInteger(2)->comment('Тип единицы измерения'),
            'status' => $this->smallInteger(2)->comment('Статус'),
            'sort' => $this->integer(11)->comment('Сортировка')
        ]);

        $this->createIndex('IND_directories_type', self::DB_TABLE, 'type');
        $this->addForeignKey('FK_directories_parent_id_to_directories_id', self::DB_TABLE, 'parent_id', self::DB_TABLE, 'id', 'SET NULL');

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_directories_parent_id_to_directories_id', self::DB_TABLE);
        $this->dropTable(self::DB_TABLE);

        Yii::$app->db->getSchema()->refresh();
    }
}
