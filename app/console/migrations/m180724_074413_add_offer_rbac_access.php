<?php

use yii\db\Migration;
use \common\components\AuthDbManager;

/**
 * Class m180724_074413_add_offer_rbac_access
 */
class m180724_074413_add_offer_rbac_access extends Migration
{
    public function safeUp()
    {
        $rule = new frontend\rbac\AccessCompanyRule();
        $userOrg = Yii::$app->authManager->getRole(AuthDbManager::ROLE_CLIENT);

        $permit = Yii::$app->authManager->createPermission('addOffer');
        $permit->description = 'Право добавить предложение для тендера';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $client = Yii::$app->authManager->getRole('client');
        Yii::$app->authManager->remove($client);

        Yii::$app->db->getSchema()->refresh();
    }
}
