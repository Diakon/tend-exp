<?php

use yii\db\Migration;

/**
 * Class m180525_084208_company_tender_offer
 */
class m180525_084208_company_tender_estimates_offer extends Migration
{
    const TABLE_NAME = '{{%offers}}';
    const TABLE_USER = '{{%user}}';
    const TABLE_COMPANY = '{{%company}}';
    const TABLE_ELEMENT = '{{%estimates_elements}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'element_id' => $this->integer(10)->unsigned()->notNull()->comment('Работа сметы'),
                'user_id' => $this->integer(11)->notNull()->comment('Пользователь'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компания'),
                'text' => $this->text()->comment('Текст комментария'),
                'price' => $this->decimal(10, 2)->comment('Цена'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_tender_estimates_offers_user_id', self::TABLE_NAME, 'user_id');
            $this->createIndex('IND_company_tender_estimates_offers_company_id', self::TABLE_NAME, 'company_id');
            $this->createIndex('IND_company_tender_estimates_offers_element_id', self::TABLE_NAME, 'element_id');

            $this->addForeignKey('FK_company_tender_estimates_offers_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_tender_estimates_offers_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id', 'CASCADE');
            $this->addForeignKey('FK_company_tender_estimates_offers_element_id', self::TABLE_NAME, 'element_id', self::TABLE_ELEMENT, 'id', 'CASCADE');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_tender_estimates_offers_user_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_tender_estimates_offers_company_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_tender_estimates_offers_element_id', self::TABLE_NAME);


        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
