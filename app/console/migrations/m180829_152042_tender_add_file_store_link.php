<?php

use yii\db\Migration;

/**
 * Class m180829_152042_tender_add_file_store_link
 */
class m180829_152042_tender_add_file_store_link extends Migration
{
    const DB_TABLE = '{{%tenders}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'files_store_link', $this->string(255)->comment('Ссылка на внешнее файловое хранилище'));

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'files_store_link');

        Yii::$app->db->getSchema()->refresh();
    }
}
