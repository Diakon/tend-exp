<?php

use common\components\AuthDbManager;
use yii\db\Migration;

/**
 * Class m180731_125653_rbac_add_child_for_role_access_tender
 */
class m180731_125653_rbac_add_child_for_role_access_tender extends Migration
{
    public function up()
    {
        $userOrg = Yii::$app->authManager->getRole(AuthDbManager::ROLE_CLIENT);
        $permit = Yii::$app->authManager->getPermission('accessTender');
        Yii::$app->authManager->addChild($userOrg, $permit);

        Yii::$app->db->getSchema()->refresh();

    }

    public function down()
    {
        $client = Yii::$app->authManager->getPermission('accessTender');

        Yii::$app->db->getSchema()->refresh();
    }
}
