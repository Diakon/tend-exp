<?php

use yii\db\Migration;

/**
 * Class m180821_073525_table_offer_add_sum_discount
 */
class m180821_073525_table_offer_add_sum_discount extends Migration
{
    const TABLE_OFFERS = '{{%offers}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_OFFERS, 'sum_discount', "DECIMAL(10,2) NULL DEFAULT '0.00' COMMENT 'Сумма тендера с учетом запроса скидки'");

        Yii::$app->db->schema->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_OFFERS, 'sum_discount');

        Yii::$app->db->schema->refresh();
    }
}
