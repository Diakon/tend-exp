<?php

use yii\db\Migration;

/**
 * Class m180510_141928_table_company_projects
 */
class m180510_141928_table_company_projects extends Migration
{
    const TABLE_NAME = '{{%projects}}';
    const TABLE_COMPANY = '{{%company}}';
    const TABLE_ADDRESS = '{{%company_address}}';
    const TABLE_DIR = '{{%directories}}';
    const TABLE_USER = '{{%user}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('Компании'),
                'user_id' => $this->integer(11)->comment('Пользователь'),
                'type_id' => $this->integer(10)->unsigned()->comment('Тип проекта'),
                'address_id' => $this->integer(10)->unsigned()->comment('Адрес объекта'),
                'title' => $this->string(255)->comment('Название'),
                'date_start' => $this->integer(11)->notNull()->comment('Дата начала реализации'),
                'date_end' => $this->integer(11)->notNull()->comment('Дата окончания реализации'),
                'status' => $this->smallInteger(2)->comment('Статус'),
                'show_portfolio' => $this->smallInteger(2)->comment('Показывать в портфолио'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_projects_company_id', self::TABLE_NAME, 'company_id');
            $this->createIndex('IND_company_projects_user_id', self::TABLE_NAME, 'user_id');
            $this->createIndex('IND_company_projects_type_id', self::TABLE_NAME, 'type_id');
            $this->createIndex('IND_company_projects_address_id', self::TABLE_NAME, 'address_id');
            $this->createIndex('IND_company_projects_status', self::TABLE_NAME, 'status');
            $this->createIndex('IND_company_projects_show_portfolio', self::TABLE_NAME, 'show_portfolio');

            $this->addForeignKey('FK_company_projects_company_id', self::TABLE_NAME, 'company_id', self::TABLE_COMPANY, 'id');
            $this->addForeignKey('FK_company_projects_type_id', self::TABLE_NAME, 'type_id', self::TABLE_DIR, 'id');
            $this->addForeignKey('FK_company_projects_address_id', self::TABLE_NAME, 'address_id', self::TABLE_ADDRESS, 'id');
            $this->addForeignKey('FK_company_projects_user_id', self::TABLE_NAME, 'user_id', self::TABLE_USER, 'id');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_projects_company_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_projects_type_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_projects_address_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_projects_user_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
