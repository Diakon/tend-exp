<?php

use yii\db\Migration;

/**
 * Class m180904_150000_create_company_completed_works_table
 */
class m180904_150000_create_company_completed_works_table extends Migration
{
    const TABLE_NAME = '{{%company_completed_works}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_id' => $this->integer(10)->unsigned()->comment('id компании'),
                'work_id' => $this->integer(10)->unsigned()->comment('Вид работы'),
                'created_at' => 'TIMESTAMP DEFAULT NOW() COMMENT "Дата создания"',
                'updated_at' => 'TIMESTAMP DEFAULT 0 COMMENT "Дата изменения"',
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Выполненные работы компании"');

            $this->addColumn(self::TABLE_NAME, 'count', "INT(11) UNSIGNED NOT NULL COMMENT 'Количество'");
            $this->addColumn(self::TABLE_NAME, 'unit_id', "SMALLINT(2) UNSIGNED NULL DEFAULT NULL COMMENT 'Единица измерения'");

            $this->createIndex('company_id', self::TABLE_NAME, 'company_id');

        }

        Yii::$app->db->schema->refresh();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }

}
