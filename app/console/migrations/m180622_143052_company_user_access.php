<?php

use yii\db\Migration;

/**
 * Class m180622_143052_company_user_access
 */
class m180622_143052_company_user_access extends Migration
{
    const TABLE_NAME = '{{%company_user_access}}';
    const TABLE_COMPANY = '{{%company_users}}';

    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE_NAME, true) === null) {
            $this->createTable(self::TABLE_NAME, [
                'id' => $this->primaryKey()->unsigned()->comment('Id'),
                'company_user_id' => $this->integer(10)->unsigned()->comment('Пользователь в компании'),
                'class_name' => $this->string(255)->notNull()->comment('Модель'),
                'class_id' => $this->integer(10)->unsigned()->comment('ID модели'),
                'type_access' => $this->smallInteger(2)->comment('Тип доступа'),
                'created_at' => $this->integer(11)->notNull()->comment('Дата создания'),
                'updated_at' => $this->integer(11)->notNull()->comment('Updated at'),
            ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

            $this->createIndex('IND_company_user_access_company_user_id', self::TABLE_NAME, 'company_user_id');
            $this->createIndex('IND_company_user_access_class_name_class_id', self::TABLE_NAME, 'class_name, class_id');
            $this->createIndex('IND_company_user_access_type_access', self::TABLE_NAME, 'type_access');

            $this->addForeignKey('FK_company_user_access_company_user_id', self::TABLE_NAME, 'company_user_id', self::TABLE_COMPANY, 'id');
        }

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_user_access_company_user_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->schema->refresh();
    }
}
