<?php

use yii\db\Migration;

/**
 * Class m180531_103547_company_tender_status_date
 */
class m180531_103547_company_tender_status_date extends Migration
{
    const TABLE_TENDER = '{{%tenders}}';
    const TABLE_OFFER = '{{%offers}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Срок начала - завершения тендера
        $this->addColumn(self::TABLE_TENDER, 'date_start', "TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата начала тендера'");
        $this->addColumn(self::TABLE_TENDER, 'date_end', "TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата окончания тендера'");
        // Предложение к тендеру, которое выбрал заказчик в качестве победителя
        $this->addColumn(self::TABLE_TENDER, 'offer_id', "INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Предложение подрядчика (победитель тендера)'");
        $this->createIndex('IND_company_tenders_offer_id', self::TABLE_TENDER, 'offer_id');
        $this->addForeignKey('FK_company_tenders_offer_id', self::TABLE_TENDER, 'offer_id', self::TABLE_OFFER, 'id');
        // Дата выбора победителя (может быть до срока окончания тендера)
        $this->addColumn(self::TABLE_TENDER, 'date_offer', "TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата выбора победителя'");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_TENDER, 'date_start');
        $this->dropColumn(self::TABLE_TENDER, 'date_end');
        $this->dropForeignKey('FK_company_tenders_offer_id', self::TABLE_TENDER);
        $this->dropColumn(self::TABLE_TENDER, 'FK_company_tenders_offer_id');
        $this->dropColumn(self::TABLE_TENDER, 'date_offer');
    }
}
