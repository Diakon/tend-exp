<?php

use yii\db\Migration;

/**
 * Class m180801_142259_offers_add_is_read_column
 */
class m180801_142259_offers_add_is_read_column extends Migration
{
    const DB_TABLE = '{{%offers}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::DB_TABLE, 'is_read', $this->smallInteger(1)->defaultValue(0)->comment('Прочитанно'));

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::DB_TABLE, 'is_read');

        Yii::$app->db->getSchema()->refresh();
    }
}
