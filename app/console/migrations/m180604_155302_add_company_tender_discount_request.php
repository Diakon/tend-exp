<?php

use yii\db\Migration;

/**
 * Class m180604_155302_add_company_tender_discount_request
 */
class m180604_155302_add_company_tender_discount_request extends Migration
{
    const TABLE_RUBRIC = '{{%estimates_rubrics}}';
    const TABLE_ELEMENT = '{{%estimates_elements}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_RUBRIC, 'discount', 'FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT "Запрос скидки"');
        $this->addColumn(self::TABLE_ELEMENT, 'discount', 'FLOAT(10,2) UNSIGNED NULL DEFAULT NULL COMMENT "Запрос скидки"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_RUBRIC, 'discount');
        $this->dropColumn(self::TABLE_ELEMENT, 'discount');

    }
}
