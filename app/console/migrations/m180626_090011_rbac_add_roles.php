<?php

use yii\db\Migration;
use \common\components\AuthDbManager;

/**
 * Class m180626_090011_rbac_add_roles
 */
class m180626_090011_rbac_add_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rule = new frontend\rbac\AccessCompanyRule();
        $userOrg = Yii::$app->authManager->getRole(AuthDbManager::ROLE_CLIENT);

        $permit = Yii::$app->authManager->createPermission('editProject');
        $permit->description = 'Право редактировать существующий проект';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        $permit = Yii::$app->authManager->createPermission('editTender');
        $permit->description = 'Право редактировать существующий тендер';
        $permit->ruleName = $rule->name;
        Yii::$app->authManager->add($permit);
        Yii::$app->authManager->addChild($userOrg, $permit);

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $client = Yii::$app->authManager->getRole('client');
        Yii::$app->authManager->remove($client);

        Yii::$app->db->getSchema()->refresh();
    }
}
