<?php

use yii\db\Migration;

/**
 * Class m180504_115848_add_fk_to_company
 */
class m180504_115848_add_fk_to_company extends Migration
{
    const TABLE_NAME = '{{%company}}';
    const TABLE_ADDRESS = '{{%company_address}}';
    const TABLE_COUNTRY = '{{%geo_country}}';
    const TABLE_CITY = '{{%geo_city}}';
    const TABLE_REGION = '{{%geo_region}}';

    public function safeUp()
    {
        $this->addForeignKey('FK_company_legal_address_id', self::TABLE_NAME, 'legal_address_id', self::TABLE_ADDRESS, 'id', 'CASCADE');
        $this->addForeignKey('FK_company_actual_address_id', self::TABLE_NAME, 'actual_address_id', self::TABLE_ADDRESS, 'id', 'CASCADE');
        $this->createIndex('IND_company_legal_address_id', self::TABLE_NAME, 'legal_address_id');
        $this->createIndex('IND_company_actual_address_id', self::TABLE_NAME, 'actual_address_id');
        $this->createIndex('IND_company_address_same', self::TABLE_NAME, 'address_same');

        $this->addForeignKey('FK_company_address_country_id', self::TABLE_ADDRESS, 'country_id', self::TABLE_COUNTRY, 'id');
        $this->addForeignKey('FK_company_address_city_id', self::TABLE_ADDRESS, 'city_id', self::TABLE_CITY, 'id');
        $this->addForeignKey('FK_company_address_region_id', self::TABLE_ADDRESS, 'region_id', self::TABLE_REGION, 'id');

        Yii::$app->db->schema->refresh();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_legal_address_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_company_actual_address_id', self::TABLE_NAME);

        Yii::$app->db->schema->refresh();
    }
}
