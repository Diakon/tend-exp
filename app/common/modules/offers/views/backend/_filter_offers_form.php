<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?php
            $form = \components\ActiveForm::begin(
                [
                    'action' => \yii\helpers\Url::to(['/offers/offers/offers_list/']),
                    'method' => 'get',
                    'options' => [
                        'class' => 'filter form-inline',
                        'role' => 'form',
                    ],
                    'fieldConfig' => function ($model, $attribute) {
                        return [
                            'options' => [
                                'class' => '',
                            ],
                            'labelOptions' => ['class' => ' '],
                            'template' => "{label}<br>\n{input}\n{hint}\n{error}",
                        ];
                    },
                ]
            ) ?>
            <div class="row">
                <div class="col-xs-3">
                    <?= $form->field($model, 'filterCompanyId')->dropDownList(\common\modules\company\models\common\Company::getCompanyList(),
                        [
                            'class' => 'js-dropdown-box js-trigger-submit-change form-control select_one',
                            'prompt' => 'Все компании',
                        ]
                    ) ?>
                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'filterTenderId')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\modules\tender\models\backend\Tenders::find()->asArray()->all(), 'id', 'title'),
                        [
                            'class' => 'js-dropdown-box js-trigger-submit-change form-control select_one',
                            'prompt' => 'Все тендеры',
                        ]
                    ) ?>
                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'filterUserId')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\models\UserIdentity::find()->asArray()->all(), 'id', 'email'),
                        [
                            'class' => 'js-dropdown-box js-trigger-submit-change form-control select_one',
                            'prompt' => 'Все пользователи',
                        ]
                    ) ?>
                </div>
                <div class="col-xs-2">
                    <div class="btn-container float-right">
                        <br>
                        <button type="submit" class="btn btn-success"><?= Yii::t('cms', 'Apply') ?></button>
                        <a class="btn btn-danger"
                           href="<?= \yii\helpers\Url::to(['/offers/offers/offers_list/']) ?>"><?= Yii::t('cms', 'Clear') ?></a>
                    </div>
                </div>
            </div>
            <?php \components\ActiveForm::end() ?>
        </div>
    </div>
</div>
