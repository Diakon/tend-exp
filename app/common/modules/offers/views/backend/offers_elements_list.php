<?php
/**
 * @var View                                                                 $this
 * @var \common\modules\company\models\backend\CompanyTenderEstimatesElements $model
 * @var ActiveDataProvider                                                   $provider
 */

use common\modules\offers\models\backend\OffersElements;
use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>
<?= $this->render('_filter_offers_elements_form', ['model' => $model]) ?>
<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'title',
                        [
                            'label' => 'Единица измерения',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return OffersElements::getUnits($data->unit_id) ?? '';
                            }
                        ],
                        'count',
                        'offer.id',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
