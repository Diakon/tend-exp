<?php
/**
 * @var View                                                                 $this
 * @var ActiveDataProvider                                                   $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;
?>
<?= $this->render('_filter_offers_form', ['model' => $model]) ?>
<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'id',
                        'tender.title',
                        'user.email',
                        'company.title',
                        'sum',
                        'text',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
