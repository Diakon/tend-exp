<?php
namespace common\modules\offers\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use common\models\UserIdentity;
use yii\helpers\ArrayHelper;

/**
 * Конфигурация формы предложения для работы списка тендеров компаний
 *
 * Class Offers
 *
 * @package common\modules\offers\models\backend\forms
 */
class Offers extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Предложение для работы ' . $model->isNewRecord ? '' : $model->element->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['tender_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(\common\modules\tender\models\backend\Tenders::find()->orderBy(['title' => SORT_ASC])->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['user_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' =>  ArrayHelper::map(UserIdentity::find()->asArray()->all(), 'id', 'email'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['company_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' =>  ArrayHelper::map(\common\modules\company\models\backend\Company::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];

        $data['text'] = ['type' => self::TYPE_TEXT];
        $data['time_work'] = ['type' => self::TYPE_TEXT];
        $data['time_warranty'] = ['type' => self::TYPE_TEXT];
        $data['bank_prepayment_warranty'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data['bank_period_warranty'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data['bank_obligation_warranty'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';

        return $data;
    }
}
