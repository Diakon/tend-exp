<?php
namespace common\modules\offers\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use common\modules\tender\models\backend\EstimatesRubrics;


/**
 * Конфигурация формы списка доп. работ сметы тендера
 *
 * Class OffersElements
 *
 * @package common\modules\offers\models\backend\forms
 */
class OffersElements extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Доп. работа предложения ' . $model->isNewRecord ? '' : $model->offer->id . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['title'] = ['type' => self::TYPE_TEXT];
        $data['offer_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' =>  ArrayHelper::map(\common\modules\offers\models\backend\Offers::find()->asArray()->all(), 'id', 'id'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['unit_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' =>  \common\modules\offers\models\backend\OffersElements::getUnits(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];

        $data['count'] = ['type' => self::TYPE_TEXT];
        $data['price_work'] = ['type' => self::TYPE_TEXT];
        $data['price_material'] = ['type' => self::TYPE_TEXT];
        $data[] = '</fieldset>';
        $data[] = '</div>';

        return $data;
    }
}
