<?php
namespace common\modules\offers\models\backend;

use common\models\UserIdentity;
use common\modules\company\models\backend\Company;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Offers
 */
class Offers extends \common\modules\offers\models\common\Offers
{

    /**
     * Компания
     *
     * @var integer
     */
    public $filterCompanyId;

    /**
     * Тендер
     *
     * @var integer
     */
    public $filterTenderId;

    /**
     * Пользователь
     *
     * @var integer
     */
    public $filterUserId;

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['filterCompanyId', 'filterTenderId', 'filterUserId'], 'integer']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'filterCompanyId' => Yii::t('app', 'Компания'),
            'filterTenderId' => Yii::t('app', 'Тендер'),
            'filterUserId' => Yii::t('app', 'Пользователь'),
        ]);
    }

    /**
     * Возвращает dataProvider для списка телефонов
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['id' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню предлжений.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Offers::class,
                    'title' => Yii::t('app', 'Предложения для тендера компании'),
                ],
                [
                    'class' => OffersElements::class,
                    'title' => Yii::t('app', 'Доп. работы предложения для тендера'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/offers/offers/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/offers/offers/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/offers/offers/' . $item['class']::classNameShort('id') . '_edit'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();
        $query->andFilterWhere(
            [
                'and',
                ['=', 'tender_id', $this->filterTenderId],
                ['=', 'company_id', $this->filterCompanyId],
                ['=', 'user_id', $this->filterUserId],
            ]
        );

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Предложения')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Предложения'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать предложение') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
