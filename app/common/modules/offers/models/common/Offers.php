<?php
namespace common\modules\offers\models\common;

use modules\crud\models\ActiveRecord;
use modules\upload\behaviors\UploadBehavior;
use common\components\ClassMap;
use common\components\User;
use common\modules\company\models\common\Company;
use common\modules\company\models\common\CompanyUsers;
use common\modules\tender\models\common\Tenders;
use frontend\modules\uwm\helpers\CompareListInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\UserIdentity;
use common\modules\offers\traits\OffersTrait;

/**
 * Предложения к работам тендера компани.
 *
 * @property integer $id                        Иднтификатор
 * @property integer $tender_id                 Тендер
 * @property integer $user_id                   Пользователь
 * @property integer $company_id                Компания
 * @property string $text                       Текст комментария
 * @property integer $created_at                Дата создания записи.
 * @property integer $updated_at                Дата изменения записи.
 * @property integer $time_work                 Срок работы (мес.)
 * @property integer $time_warranty             Срок гарантии (мес.)
 * @property integer $bank_prepayment_warranty  Банковская гарантия для аванса
 * @property integer $bank_period_warranty      Банковская гарантия для аванса
 * @property integer $bank_obligation_warranty  Банковская гарантия для выполнения обязательства
 * @property integer $is_archive                Предложение перенесено в архив
 * @property integer $is_read                   Предложение было прочитано заказчиком
 * @property float $sum                         Сумма предложения к тендеру
 * @property float $sum_discount                Сумма предложения к тендеру с учетом запроса скидки
 * @property float $sum_discount_request        Сумма с учетом не подтвержденного автором предложения запроса скидки
 * @property integer $company_author_id         Компания - автор тендера для которого оставлено это предложение
 * @property integer $status                    Статус предложения
 *
 * @property Company $company
 * @property User $user
 */
class Offers extends ActiveRecord implements CompareListInterface
{
    use OffersTrait;

    /**
     * Тип записи в архиве
     */
    const IS_ARCHIVE_TRUE = 1;

    /**
     * Тип записи в не в архиве
     */
    const IS_ARCHIVE_NO = 0;

    /**
     * Есть гарантия
     */
    const TYPE_WARRANTY_YES = 1;

    /**
     * Нет гарантии
     */
    const TYPE_WARRANTY_NO = 0;

    /**
     * Не прочитано
     */
    const IS_READ_NO = 0;

    /**
     * Прочитано
     */
    const IS_READ_YES = 1;

    /**
     * Статус предложения
     */
    const STATUS_SEND = 1;
    const STATUS_APPROVED_YES = 2;
    const STATUS_APPROVED_NO = 3;
    public static $statusList = [
        self::STATUS_SEND => 'Отправлено',
        self::STATUS_APPROVED_YES => 'Одобрено',
        self::STATUS_APPROVED_NO => 'Не одобрено'
    ];

    /**
     * Возвращает массив или имя статуса предложения
     *
     * @param null|integer $statusId
     * @return array|string
     */
    public static function getStatusList($statusId = null)
    {
        return !empty($statusId) ? Yii::t('app', self::$statusList[$statusId]) : self::$statusList;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['tender_id', 'user_id', 'company_id', 'time_work', 'time_warranty', 'bank_prepayment_warranty', 'bank_period_warranty', 'bank_obligation_warranty', 'status'], 'required'],
            [['tender_id', 'user_id', 'company_id', 'time_work', 'time_warranty', 'bank_prepayment_warranty', 'bank_period_warranty', 'bank_obligation_warranty', 'is_archive', 'is_read', 'company_author_id'], 'integer'],
            [['text'], 'string'],
            ['user_id', 'validateUserCompany'],
            [['sum', 'sum_discount', 'sum_discount_request'], 'number'],
            [['time_work'], 'integer', 'min' => 1],
            [['time_warranty'], 'integer', 'min' => 1],
            [['created_at', 'updated_at'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['status'], 'in', 'range' => array_keys(self::$statusList)],
        ]);
    }

    /**
     * Проверяет, что пользователь относится к указанной компании
     *
     * @param $attribute
     * @param $params
     * @param $validator
     * @return bool
     */
    public function validateUserCompany($attribute, $params, $validator)
    {
        $chkUser = CompanyUsers::find()
            ->where(['user_id' => $this->user_id])
            ->andWhere(['company_id' => $this->company_id])
            ->count();
        if (empty($chkUser)) {
            $this->addError($attribute, 'Пользователь не закреплен за компанией');
            return false;
        }
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'documents' => [
                'class' => UploadBehavior::class,
                'attributes' => [
                    'documents' => [
                        'singleFile' => false,
                        'rules' => [
                            'file' => [
                                'extensions' => 'doc,docx,xls,xlsx,pdf,ppt',
                                'maxSize' => 256 * 1024 * 1024
                            ],
                        ],
                    ],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tender_id' => Yii::t('app', 'Тендер'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'company_id' => Yii::t('app', 'Компания'),
            'text' => Yii::t('app', 'Текст комментария'),
            'is_archive' => Yii::t('app', 'Предложение перенесено в архив'),
            'time_work' => Yii::t('app', 'Срок работы (мес.)'),
            'time_warranty' => Yii::t('app', 'Срок гарантии (мес.)'),
            'bank_prepayment_warranty' => Yii::t('app', 'Банковская гарантия для аванса'),
            'bank_period_warranty' => Yii::t('app', 'Банковская гарантия для аванса'),
            'bank_obligation_warranty' => Yii::t('app', 'Банковская гарантия для выполнения обязательства'),
            'is_read' => Yii::t('app', 'Прочитано'),
            'sum' => Yii::t('app', 'Сумма предложения к тендеру'),
            'sum_discount' => Yii::t('app', 'Сумма предложения к тендеру с учетом запроса скидки'),
            'sum_discount_request' => Yii::t('app', 'Сумма с учетом не подтвержденного автором предложения запроса скидки'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
            'company_author_id' => Yii::t('app', 'Компания - автор тендера'),
            'status' => Yii::t('app', 'Статус предложения'),
        ];
    }

    /**
     * Связь с тендером
     *
     * @return ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tenders::class, ['id' => 'tender_id']);
    }

    /**
     * Связь с пользователем
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'user_id']);
    }

    /**
     * Связь с компанией
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с работами
     *
     * @return ActiveQuery
     */
    public function getElements()
    {
        return $this->hasMany(OffersElements::class, ['offer_id' => 'id']);
    }

    /**
     *  Возвращает сумму предложения по всему тендеру (или для конкретной позиции) с дисконтом и без
     * @return array
     */
    public function setSumPrice()
    {
        $sum = $this->getOfferTotalSumPrice();
        $this->sum = $sum['noDiscount'];
        $this->sum_discount = $sum['withDiscount'];
        $this->sum_discount_request = $sum['withRequestDiscount'];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);

        // Пересчитываем стоимость всего предложения
        $this->setSumPrice();

        return $result;
    }

    /**
     * Данные, которые будут храниться в сессии
     *
     * @return array
     */
    public function getCompareWidgetListData()
    {
        return ['id' => $this->id];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        $this->calcDiscountOffer($this);

    }
}
