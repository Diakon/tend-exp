<?php
namespace common\modules\offers\models\common;

use modules\crud\models\ActiveRecord;
use common\modules\directories\models\common\Works;
use common\modules\offers\traits\OffersTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Доп. работы тендера
 *
 * @property integer $id                 Иднтификатор
 * @property integer $offer_id           Предложение
 * @property string $title               Работа
 * @property integer $unit_id            Единица измерения
 * @property string $class_name          Модель сметы
 * @property integer $class_id           ID модели сметы
 * @property float $discount             Запрос скидки
 * @property integer $count              Количество
 * @property float $price_work           Стоимость работы за 1 единицу
 * @property float $price_material       Стоимость материалов за 1 единицу
 * @property integer $created_at         Дата создания записи.
 * @property integer $updated_at         Дата изменения записи.
 * @property integer $is_archive         Флаг, что предложение является архивным
 * @property float $sum                  Сумма элемнта предложения
 * @property integer $sum_discount       Сумма элемнта предложения с учетом запроса скидки
 * @property float $discount_request     Не подтвержденный автором предложения запрос скидки от автора тендера
 * @property float $sum_discount_request Сумма элемнта предложения для не подтвержденного автором предложения запроса скидки
 *
 */
class OffersElements extends ActiveRecord
{
    use OffersTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offers_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['unit_id', 'count', 'price_work', 'price_material', 'class_name'], 'required'],
            [['class_id', 'offer_id', 'count', 'unit_id', 'class_name'], 'integer'],
            [['price_work', 'price_material'], 'number'],
            [[ 'title'], 'string', 'max' => 255],
            [['discount', 'discount_request'], 'number', 'min' => 0, 'max' => 100],
            [['sum', 'sum_discount', 'sum_discount_request'], 'number'],
            [['created_at', 'updated_at'], 'date', 'format' => 'php:Y-m-d H:i:s']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'offer_id' => Yii::t('app', 'Предложение'),
            'title' => Yii::t('app', 'Работа'),
            'class_name' => Yii::t('app', 'Модель сметы'),
            'class_id' => Yii::t('app', 'ID модели сметы'),
            'discount' => Yii::t('app', 'Запрос скидки'),
            'discount_request' => Yii::t('app', 'Не подтвержденный автором предложения запрос скидки от автора тендера'),
            'unit_id' => Yii::t('app', 'Единица измерения'),
            'count' => Yii::t('app', 'Количество'),
            'price_work' => Yii::t('app', 'Стоимость работы за 1 единицу'),
            'price_material' => Yii::t('app', 'Стоимость материалов за 1 единицу'),
            'sum' =>Yii::t('app', 'Сумма элемнта предложения'),
            'sum_discount' =>Yii::t('app', 'Сумма элемнта предложения с учетом запроса скидки'),
            'sum_discount_request' => Yii::t('app', 'Сумма элемнта предложения для не подтвержденного автором предложения запроса скидки'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Связь с предложением для тендера
     *
     * @return ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(Offers::class, ['id' => 'offer_id']);
    }

    /**
     * Возвращает список единиц измерения, или значение выбранной единицы измерения
     *
     * @param null $unitId
     * @return array
     */
    public static function getUnits($unitId = null)
    {
        $units = Works::$units;
        return isset($units[$unitId]) ? $units[$unitId] : $units;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        // Если есть запрос скидки - обрабатываем
        $this->updateDiscount($this);

    }
}
