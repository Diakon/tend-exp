<?php
namespace common\modules\offers\models\frontend;

use common\components\ClassMap;
use yii\helpers\ArrayHelper;

/**
 * OffersElements
 */
class OffersElements extends \common\modules\offers\models\common\OffersElements
{
    /**
     * сценарий создания записи предложения для убрики сметы
     */
    const SCENARIO_CREATE_RUBRIC = 'create-offer-rubric';

    /**
     * сценарий редактирования записи предложения для убрики сметы
     */
    const SCENARIO_UPDATE_RUBRIC = 'create-offer-rubric';

    /**
     * сценарий создания записи предложения для эллемента сметы
     */
    const SCENARIO_CREATE_ELEMENT = 'create-offer-element';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return parent::rules();
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(
            parent::scenarios(),
            [
                self::SCENARIO_CREATE_RUBRIC => ['offer_id', 'discount', 'class_name', 'class_id', 'discount_request'],
            ],
            [
                self::SCENARIO_UPDATE_RUBRIC => ['offer_id', 'discount', 'class_name', 'class_id', 'discount_request'],
            ],
            [
                self::SCENARIO_CREATE_ELEMENT => ['class_id', 'offer_id', 'count', 'unit_id', 'price_work', 'price_material', 'class_name', 'title', 'discount', 'discount_request'],
            ]
            );
    }
}
