<?php
namespace common\modules\offers\models\frontend;

use common\components\ClassMap;
use common\modules\company\models\common\Company;
use frontend\models\EntityComment;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use frontend\components\CheckTHashTrait;
use yii\helpers\Url;
use common\models\EntityFile;
use frontend\modules\upload\components\FileUploadInterface;
use frontend\modules\uwm\Module as Uwm;

/**
 * Offers
 * @property Company $company
 */
class Offers extends \common\modules\offers\models\common\Offers implements FileUploadInterface
{
    use CheckTHashTrait;

    const MODEL_ATTRIBUTE_FILES = 'files';

    /**
     * @var string $tHash Свойство для хранения временного идентификатора модели
     */
    private $tHash;

    /**
     * @param string $tHash
     */
    public function setTHash(string $tHash)
    {
        $this->tHash = $tHash;
    }

    /**
     * @return string
     */
    public function getTHash(): string
    {
        if (!empty($this->tHash)) {
            return $this->tHash;
        }

        $this->setTHash($this->isNewRecord ? uniqid('thash_' . substr(md5(static::class), 0,
                6) . '_') : (string)$this->id);

        return $this->tHash;
    }

    /**
     * Получаем ajax путь удаления файла
     *
     * @return string
     */
    public function getAjaxDeleteFileUrl(): string
    {
        return Url::to(['/upload/base/delete']);
    }

    /**
     * Получаем ajax путь загрузки файла
     *
     * @return string
     */
    public function getAjaxUploadFileUrl(): string
    {
        return Url::to(['/upload/base/upload']);
    }

    /**
     * Подготовка модели к редактированию
     *
     * @param $tHash
     *
     * @throws \Exception
     */
    public function prepareModelAction($tHash)
    {
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'processTHashEntityFile'], ['tHash' => $tHash]);
        $this->on(self::EVENT_AFTER_MAIN_ACTION_COMMIT, [$this, 'flushTHash'], ['tHash' => $tHash]);
    }

    /**
     * Получаем реляцию модели
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getFiles()
    {
        return $this->hasMany(EntityFile::class, ['modelId' => 'thash'])->onCondition([
            'modelName' => ClassMap::getId(self::class),
            'modelAttribute' => 'files'
        ]);
    }


    /**
     * Событие после коммита в RequestMainAction
     *
     * @see \frontend\modules\request\controllers\actions\RequestMainAction::run
     */
    const EVENT_AFTER_MAIN_ACTION_COMMIT = 'event_after_main_action_commit';

    /**
     * сценарий создания предложения
     */
    const SCENARIO_CREATE = 'create-offer';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(
            parent::scenarios(),
            [
                self::SCENARIO_CREATE => [
                    'tender_id',
                    'user_id',
                    'company_id',
                    'created_at',
                    'updated_at',
                    'time_work',
                    'time_warranty',
                    'bank_prepayment_warranty',
                    'bank_period_warranty',
                    'bank_obligation_warranty',
                    'is_archive',
                    'is_read',
                    'text',
                    'user_id',
                    'sum',
                    'sum_discount',
                    'time_work',
                    'time_warranty',
                    'company_author_id',
                    'status'
                ],
            ]
        );
    }


    /**
     * Возвращает сисок предложений для тендера
     *
     * @param null $request Параметры фильтрации.
     * @param integer $tenderId ID тендера
     * @return ActiveDataProvider
     */
    public function geOffersTender($request = null, $tenderId)
    {
        /** @var \yii\db\ActiveQuery $query */

        // Сперва получаем ID записей, с группировкой по компаниям,

        $query = self::find();
        $query->where(['tender_id' => $tenderId]);
        $query->andWhere(['is_archive' => self::IS_ARCHIVE_NO]);

        $query->orderBy(['sum' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' => 10,
                'pageSizeParam' => false,
            ],
        ]);
    }

    /**
     * Ссылка на просмотре предложения для тендера.
     * @param array $params Массив параметров.
     *
     * @return string
     */
    public function getOfferViewLink(array $params = [])
    {
        return $this->getViewLink('tender_offer_view', $params);
    }

    /**
     * Ссылка на добавление предложения для тендера.
     * @param array $params Массив параметров.
     *
     * @return string
     */
    public function getOfferAddLink(array $params = [])
    {
        return $this->getAddLink('offer_add', $params);
    }

    /**
     * Возвращает предложения для тендера от компании в виде массива и сортировки для отбова предложений
     *
     * @return array
     */
    public function getOffersTender()
    {
        $returnOffersTender = [];

        $offersTender = self::find()->where(['company_id' => $this->company_id])
            ->andWhere(['tender_id' => $this->tender_id])
            ->orderBy(['updated_at' => SORT_ASC])
            ->asArray()
            ->all();

        foreach ($offersTender as $key => $offer) {
            /**
             * @var Offers $offer
             */
            $returnOffersTender[$key]['id'] = $offer['id'];
            $returnOffersTender[$key]['sum'] = $offer['sum'];
            $returnOffersTender[$key]['sumDiscount'] = $offer['sum_discount'];
            $returnOffersTender[$key]['date'] = $offer['created_at'];
        }

        return $returnOffersTender;
    }

    /**
     * Связь с коментариями
     *
     * @return \modules\crud\models\ActiveQuery|\yii\db\ActiveQuery
     */
    public function getComments()
    {
        return EntityComment::find()->where(['offerId' => $this->id]);
    }

    /**
     * Возвращает ID предложений, которые отобраны для сравнения.
     * Если указать $tenderId - вернет только те ID предложений, которые оставлены для этого тендера
     *
     * @param null|integer $tenderId
     * @return array
     */
    public static function compareIds($tenderId = null)
    {
        $returnIds = [];
        $compare = Uwm::getList(Uwm::TYPE_COMPARE);
        if (!empty($compare[self::classNameShort()])) {
            $returnIds = array_keys($compare[self::classNameShort()]);
            if (!empty($tenderId)) {
                $offerIds = ArrayHelper::map(self::find()->where(['tender_id' => $tenderId])->asArray()->all(), 'id', 'id');
                foreach ($returnIds as $key => $id) {
                    if (!in_array($id, $offerIds)) {
                        unset($returnIds[$key]);
                    }
                }
            }
        }

        return $returnIds;
    }

    /**
     * Возращает лучшее предложение для тендера $tenderId
     * 
     * @param $tenderId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getBestOfferTender($tenderId)
    {
        return Offers::find()
            ->where(['tender_id' => $tenderId])
            ->andWhere(['is_archive' => self::IS_ARCHIVE_NO])
            ->orderBy(['sum' => SORT_ASC])
            ->one();
    }

    /**
     * Возращает последнее активное предложение для тендера от компани
     *
     * @param int $companyId
     * @param int $tenderId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getLastActiveOffer(int $companyId, int $tenderId)
    {
        return self::find()
            ->where(['company_id' => $companyId])
            ->andWhere(['tender_id' => $tenderId])
            ->andWhere(['is_archive' => self::IS_ARCHIVE_NO])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    /**
     * Расчитывает и возращает сумму необходимой корреткировки предложения
     *
     * @return float|mixed
     */
    public function getCalcSumAdjustmentDiscountRequest()
    {
        // Получаю стоимость работ со скидкой без учета доп. работ
        $sum = $this->getElements()->where(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])->andWhere(['not', ['class_id' => null]])->sum('sum');

        // Вычитаю из стоимости работ сумму запроса скидка
        return $sum - $this->sum_discount_request;
    }
}
