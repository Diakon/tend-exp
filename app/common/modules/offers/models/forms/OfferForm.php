<?php
namespace common\modules\offers\models\forms;

use common\components\ClassMap;
use common\components\SendMail;
use common\models\EntityFile;
use common\modules\offers\models\frontend\Offers;
use common\modules\offers\models\frontend\OffersElements;
use common\modules\tender\models\common\Tenders;
use common\modules\tender\models\frontend\EstimatesElements;
use common\modules\tender\models\frontend\EstimatesRubrics;
use frontend\models\EntityComment;
use yii\base\Model;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Форма добавления нового / редактирования существующего предложения
 *
 * OfferForm form
 */
class OfferForm extends Model
{
    /**
     * Модель предложения
     * @var Offers
     */
    public $offer;

    /**
     * Модель тендера для которого составляется предложение
     * @var Tenders
     */
    public $tender;

    /**
     * ID предложения
     * @var integer
     */
    public $offerId;

    /**
     * tHash предложения
     * @var integer
     */
    public $offerTHash;

    /**
     * ID класса сметы
     * @var integer
     */
    public $classId;


    /**
     * Имя класса сметы
     * @var string
     */
    public $className;

    /**
     * Запись о рубриках тендера
     * @var array
     */
    public $tenderRubrics = [];

    /**
     * Дополнительные работы
     * @var array
     */
    public $addWorks = [];

    /**
     * Предложение по работам тендера
     * @var array
     */
    public $tenderWorks = [];

    /**
     * Запрос сеидки от автора тендера
     *
     * @var array
     */
    public $offerDiscountRequest = [];

    /**
     * ID компании от имени которой создается предложение к тендеру
     * @var integer
     */
    public $companyId;

    /**
     * Время работы в месяцах
     * @var integer
     */
    public $timeWork = 0;

    /**
     * Время гарантии в месяцах
     * @var integer
     */
    public $timeWarranty = 0;

    /**
     * Банковская гарантия для аванса
     * @var integer
     */
    public $bankPrepaymentWarranty;

    /**
     * Банковская гарантия для гарантийного периода
     * @var integer
     */
    public $bankPeriodWarranty;

    /**
     * Банковская гарантия для выполнения обязательства
     * @var integer
     */
    public $bankObligationWarranty;

    /**
     * Дополнительное описание
     * @var string
     */
    public $description;

    /**
     * Запрос скидки
     * @var float
     */
    public $discount;

    /**
     * @var array
     */
    public $offerPrices = [];

    /**
     * Коментарий к  тендеру
     * @var array
     */
    public $tenderComments = [];

    const SCENARIO_CREATE = 'create-offer-form';
    const SCENARIO_UPDATE = 'update-offer-form';
    const ADD_WORK_HIDDEN_KEY = 'keyCloneId';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timeWork', 'timeWarranty', 'tenderWorks', 'bankPrepaymentWarranty', 'bankPeriodWarranty', 'bankObligationWarranty'], 'required'],
            [['classId', 'offerId', 'companyId', 'timeWork', 'timeWarranty', 'bankPrepaymentWarranty', 'bankPeriodWarranty', 'bankObligationWarranty', 'className'], 'integer'],
            [['timeWork', 'timeWarranty'], 'validateTenderTimeWorks'],
            ['tenderWorks', 'validateTenderWorks'],
            ['addWorks', 'validateAddWorks'],
            [['description'], 'string'],
            [['tenderComments'], 'each', 'rule' => ['integer']],
            [['discount'], 'number', 'min' => 0, 'max' => 100],
            [['tenderRubrics'], 'each', 'rule' => ['integer']],
            ['offerDiscountRequest', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(
            parent::scenarios(),
            [
                self::SCENARIO_CREATE => ['classId', 'offerId', 'companyId', 'timeWork', 'timeWarranty', 'bankPrepaymentWarranty', 'bankPeriodWarranty', 'bankObligationWarranty',
                    'className', 'tenderWorks', 'addWorks', 'description', 'tenderComments', 'discount', 'offerDiscountRequest'],
                self::SCENARIO_UPDATE => ['classId', 'tenderWorks', 'offerId', 'companyId', 'className', 'addWorks', 'description', 'tenderComments', 'discount', 'offerDiscountRequest'],
            ]
        );
    }

    /**
     * Проверяет введеные основные работы
     *
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateTenderWorks($attribute, $params)
    {
        // Проверяем что указана или стоимость работ или стоимость материалов (или оба) для каждой строки сметы работ
        foreach ($this->{$attribute} as $work) {
            if (empty($work['price_work']) && empty($work['price_material'])) {
                $errorText = Yii::t('app', 'Вы должны указать стоимость работ и/или материалов для всех элементов сметы');
            }
        }

        if (!empty($errorText)) {
            $errorText = Yii::$app->session->getFlash('error') . '<br>' . $errorText;
            Yii::$app->session->setFlash('error', $errorText, false);
        }

        return !empty($errorText) ? false : true;
    }

    /**
     * Проверяет введенное время работы и гарантии
     *
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateTenderTimeWorks($attribute, $params)
    {
        // Проверяем что указан срок работы и гарантии
        if (empty($this->{$attribute}) || !is_numeric($this->{$attribute})) {
            $errorText = Yii::t('app', "Вы не указали ") . ($attribute == "timeWork" ? "срок работы (мес.)" : "срок гарантии (мес.)");
            $errorText = Yii::$app->session->getFlash('error') . '<br>' . $errorText;
            Yii::$app->session->setFlash('error', $errorText, false);
        }

        return !empty($errorText) ? false : true;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->timeWork = !empty($this->timeWork) ? $this->timeWork : 0;
        $this->timeWarranty = !empty($this->timeWarranty) ? $this->timeWarranty : 0;

        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    /**
     * Проверяет введеные доп. работы
     *
     * @param $attribute
     * @param $params
     *
     * @return bool
     */
    public function validateAddWorks($attribute, $params)
    {
        $errorText = null;
        // Проверяем наличие всех обязательных полей и данные в них
        foreach ($this->{$attribute} as $idWork => $work) {
            if ($idWork == self::ADD_WORK_HIDDEN_KEY) {
                continue;
            }
            $units = OffersElements::getUnits();
            if (empty($work['title'])) {
                $errorText = Yii::t('app', 'Вы не указали название для дополнительных работ');
                Yii::$app->session->setFlash('error', Yii::t('app', $errorText), false);
            }
            if (empty((int)$work['count'])) {
                $errorText = Yii::t('app', 'Вы не указали количество для дополнительных работ');
                Yii::$app->session->setFlash('error', Yii::t('app', $errorText), false);
            }
            if (empty($units[(int)$work['unit_id']])) {
                $errorText = Yii::t('app', 'Вы не указали ед. измерения для дополнительных работ');
                Yii::$app->session->setFlash('error', Yii::t('app', $errorText), false);
            }
            if (empty($work['price_work']) && empty($work['price_material'])) {
                $errorText = Yii::t('app', 'Вы не указали стоимость работ и/или стоимость материалов для дополнительных работ');
                Yii::$app->session->setFlash('error', Yii::t('app', $errorText), false);;
            }
        }

        return !empty($errorText) ? false : true;
    }

    /**
     * Связь с элементами предложения к тендеру
     * @return object
     */
    public function getOfferElements()
    {
        return OffersElements::find()->where(['offer_id' => $this->offerId])->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID]);
    }

    /**
     * Связь с предложением к тендеру
     * @return object
     */
    public function getOffer()
    {
        return !empty($this->offer) ? $this->offer : Offers::find()->where(['id' => $this->offerId])->one();
    }


    /**
     * Устанавливает значения предложения
     */
    public function setOfferData()
    {
        if (!empty($this->offerId)) {
            $this->setOffer();
            $this->setAddWorks();
            $this->setTenderWorks();
        }
    }

    /**
     * Устанавливает основые условия предложения к тендеру
     */
    private function setOffer()
    {
        $offer = $this->getOffer();
        $this->description = $offer->text;
        $this->timeWork = $offer->time_work;
        $this->timeWarranty = $offer->time_warranty;
        $this->bankPrepaymentWarranty = $offer->bank_prepayment_warranty;
        $this->bankPeriodWarranty = $offer->bank_period_warranty;
        $this->bankObligationWarranty = $offer->bank_obligation_warranty;

        // Устанавливаю суммы по рубрикам и эллементам сметы
        $this->offerPrices['rubrics'] = [];
        $this->offerPrices['elements'] = [];
        $this->offerPrices['addWorks'] = [];
        $this->offerPrices['totalOfferSum']['noDiscount'] = 0; // Сумма предложения без скидок
        $this->offerPrices['totalOfferSum']['withDiscount'] = 0; // Сумма предложения с одобреной скидкой
        $this->offerPrices['totalOfferSum']['withRequestDiscount'] = 0; // Сумма прелодения с учетом запрошеных, но не одоьбреных скидок
        $this->offerPrices['totalOfferSum']['noDiscountAndNoAddWork'] = 0; // Сумма предлжения без учета доп. работ
        unset($this->addWorks[self::ADD_WORK_HIDDEN_KEY]);
        foreach ($offer->getElements()->asArray()->all() as $offerElement) {
            if ($offerElement['class_name'] == ClassMap::CLASS_ESTIMATES_RUBRICS_ID) {
                $id = $offerElement['class_id'];
                $this->offerPrices['rubrics'][$id]['noDiscount'] = $offerElement['sum'];
                $this->offerPrices['rubrics'][$id]['withDiscount'] = $offerElement['sum_discount'];
                $this->offerPrices['rubrics'][$id]['discount'] = $offerElement['discount'];

                $this->offerPrices['rubrics'][$id]['withRequestDiscount'] = $offerElement['sum_discount_request'];
                $this->offerPrices['rubrics'][$id]['discountRequest'] = $offerElement['discount_request'];
            } else {
                $key = !empty($offerElement['class_id']) ? 'elements' : 'addWorks'; // Если у работу указан ID записи в смете тендера - это работа тендера, если нет - это доп. работа
                $id = $key == 'elements' ? $offerElement['class_id'] : $offerElement['id'];
                $this->offerPrices[$key][$id]['noDiscount'] = $offerElement['sum'];
                $this->offerPrices[$key][$id]['withDiscount'] = $offerElement['sum_discount'];
                $this->offerPrices[$key][$id]['discount'] = $offerElement['discount'];
                $this->offerPrices[$key][$id]['withRequestDiscount'] = $offerElement['sum_discount_request'];
                $this->offerPrices[$key][$id]['discountRequest'] = $offerElement['discount_request'];


                $this->offerPrices['totalOfferSum']['noDiscount'] += $offerElement['sum'];
                if ($offerElement['sum_discount'] > 0) {
                    $this->offerPrices['totalOfferSum']['withDiscount'] += $offerElement['sum'] - $offerElement['sum_discount'];
                }
                if ($offerElement['sum_discount_request'] > 0 || $offerElement['sum_discount'] > 0) {
                    $this->offerPrices['totalOfferSum']['withRequestDiscount'] += $offerElement['sum'] - ($offerElement['sum_discount_request'] > 0 ? $offerElement['sum_discount_request'] : $offerElement['sum_discount']);
                }
                if ($key == 'elements') {
                    $this->offerPrices['totalOfferSum']['noDiscountAndNoAddWork'] += $offerElement['sum'];
                }
            }
        }
        $this->offerPrices['totalOfferSum']['totalSum'] = $offer->sum; // Итоговая стоимость без учета одобреной скидки
        $this->offerPrices['totalOfferSum']['totalSumWithDiscount'] = $offer->sum; // Итоговая стоимость с учетом одобреной скидки
    }

    /**
     * Устанавливает значения выставленные в предложении для работ сметы
     */
    private function setTenderWorks()
    {
        $this->tenderWorks = [];
        $offers = $this->getOfferElements()
            ->andWhere(['not', ['class_id' => null]])
            ->asArray()
            ->all();

        foreach ($offers as $offer) {
            foreach (['price_work', 'price_material', 'discount'] as $fieldName) {
                $this->tenderWorks[$fieldName] = $offer[$fieldName];
            }
        }
    }

    /**
     * Устанавливает значения выставленные в предложении для доп. работ
     */
    private function setAddWorks()
    {
        $this->addWorks = $this->getOfferElements()
            ->andWhere(['class_id' => null])
            ->asArray()
            ->all();
    }

    /**
     * Добавление запроса скидки для предложения
     *
     * @return bool
     */
    public function addOfferDiscount()
    {
        $offerElement = OffersElements::find()->where(['offer_id' => $this->offerId])
            ->andWhere(['class_name' => $this->className])
            ->andWhere(['class_id' => $this->classId])
            ->one();
        // Если поступи запрос скидки к рубрике - то создаем в предложении запись о рубрике
        if (empty($offerElement) && $this->className == EstimatesRubrics::classNameShort()) {
            $offerElement = new OffersElements();
            $offerElement->title = Yii::t('app', 'Рубрика сметы');
            $offerElement->offer_id = $this->offerId;
            $offerElement->class_name = $this->className;
            $offerElement->class_id = $this->classId;
        }
        if (!empty($offerElement)) {
            $offerElement->setScenario($this->className == EstimatesRubrics::classNameShort() ? OffersElements::SCENARIO_CREATE_RUBRIC : OffersElements::SCENARIO_DEFAULT);
            $offerElement->discount = $this->discount;

            $transactionOffer = OffersElements::getDb()->beginTransaction();
            try {
                if ($offerElement->save()) {
                    $transactionOffer->commit();

                    return true;
                }

                $transactionOffer->rollBack();
                return false;
            } catch(\Throwable $e) {
                $transactionOffer->rollBack();
                Yii::error('Ошибка при добавления запроса скидки к предложению с ID= ' . $this->offerId . '.
                Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderOffers');
                Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения данных'), false);

                return false;
            }
        }

        return false;
    }

    /**
     * Обновляет предложение, записывая в него коректировки от автора тендера
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    public function correctOfferAuthorTender()
    {
        $needRollBack = false;
        $this->setScenario(self::SCENARIO_CREATE);
        $offer = $this->getOffer();
        $transactionOffer = Offers::getDb()->beginTransaction();

        try {
            foreach ($this->tenderRubrics as $idRubric => $rubricValue) {
                // Получаю элемент предложения (рубрику сметы), для которой сдежует разместить дисконт
                $offerElement = OffersElements::find()->where(['offer_id' => $offer->id])->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID])->andWhere(['class_id' => $idRubric])->one();
                if (empty($offerElement)) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения запроса скидки'), false);
                    $needRollBack = true;
                }
                $offerElement->scenario = OffersElements::SCENARIO_UPDATE_RUBRIC;
                if (!empty($rubricValue['discount'])) {
                    $offerElement->discount_request = $rubricValue['discount'] ?? null;
                }

                if (!$offerElement->save()) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения запроса скидки'), false);
                    $needRollBack = true;
                }
            }

            // Обновляю стоимость всего предложения
            if (!$offer->save()) {
                $needRollBack = true;
            }

            if ($needRollBack) {
                $transactionOffer->rollBack();
            } else {
                // Все сохранено успешно - отправляю уведомление автору предложения
                $this->sendEmailCorrectOffer($offer);
                // Сохраняю коментарий от автора тендера
                $this->saveComments($offer);

                $transactionOffer->commit();
                return true;
            }

            return false;
        } catch(\Throwable $e) {
            $transactionOffer->rollBack();
            Yii::error('Ошибка при коррекции предложения автором тендера ID=' . ($this->tender->id ?? "") . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderOffers');
            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения данных'), false);

            return false;
        }

    }

    /**
     *
     */
    public function addCommentOffer()
    {
        $this->setScenario(self::SCENARIO_CREATE);
        $this->saveComments($this->getOffer());
    }

    /**
     * Возращает значения по запросу скидки автора тендера в нужный вид
     *
     * @return array
     */
    private function getOfferDiscountRequestData()
    {
        $requestDiscount = [];
        foreach ($this->offerDiscountRequest as $classId => $discountParams) {
            foreach ($discountParams as $discountParam) {
                $id = $discountParam['id'];
                $type = $discountParam['type'];
                // Если в type пусто (не 0 или 1) - значит пока решение не принято отновсительно запроса суидки - не добавляю в массив
                if ($type == '') {
                    continue;
                }
                $requestDiscount[$classId][$id]['id'] = $id;
                $requestDiscount[$classId][$id]['comment'] = $discountParam['comment'];
                $requestDiscount[$classId][$id]['type'] = !empty($type);
            }
        }

        return $requestDiscount;
    }

    /**
     * Сохраняет новое предложение к тендеру
     *
     * @return bool|int
     * @throws \yii\db\Exception
     */
    public function createOffer()
    {
        // Подготавливаю массив цен сметы к обработке
        foreach ($this->tenderWorks as $key => $pricesWork) {
            foreach ($pricesWork as $priceKey => $price) {
                $this->tenderWorks[$key][$priceKey] = !empty($price) ? $price : 0;
            }
        }
        foreach ($this->addWorks as $key => $pricesAddWorks) {

            foreach (['price_work', 'price_material'] as $work) {
                $price = preg_replace("/[^,.0-9]/", '', $this->addWorks[$key][$work]);
                $this->addWorks[$key][$work] = floatval(!empty($price) ? $price : 0);
            }
        }

        $this->setScenario(self::SCENARIO_CREATE);
        $transactionOffer = Offers::getDb()->beginTransaction();

        try {
            $needRollBack = false;

            // Если offerId указан, то помечаем это предложение как архивное
            $discounts = [];
            $discountsRequests = [];
            $oldOffer = $this->getOffer();

            // Если пришло одобение/не одобрение запроса скидки - обновляю получаю новые данные по сккидкам
            $discountsRequestData = $this->getOfferDiscountRequestData();

            if (!empty($oldOffer)) {
                $oldOffer->setScenario(Offers::SCENARIO_CREATE);
                $oldOffer->is_archive = Offers::IS_ARCHIVE_TRUE;
                $oldOffer->save();

                // Получаю из архтвного предложения дисконты для работ и доп. работ сметы
                foreach ($oldOffer->getElements()->asArray()->all() as $val) {
                    $type = 'rubrics';
                    $className = $val['class_name'];
                    if ($className == ClassMap::CLASS_ESTIMATES_ELEMENTS_ID) {
                        $type = !empty($val['class_id']) ? 'elements' : 'addWorks';
                    }
                    $key = !empty($val['class_id']) ? $val['class_id'] : $val['id'];
                    $discount = $val['discount'];
                    $discountRequest = $val['discount_request'];

                    // Смотрю, есть ли решение автора предложения по запросам скидки от автора тендера
                    if (!empty($val['class_id']) && !empty($discountsRequestData[$className][$key])) {
                        // Если автор предложения одобрил запрос скидки от автора тендера - меняю значение дисконта
                        if (!empty($discountsRequestData[$className][$key]['type'])) {
                            $discount = $discountRequest;
                        }
                        $discountRequest = null;
                    }

                    $discounts[$type][$key] = $discount;
                    $discountsRequests[$type][$key] = $discountRequest;
                    // Добавляем значения скидки в цены с формы (работы и доп. работы)
                    if ($type == 'elements') {
                        if (!empty($val['class_id']) && isset($this->tenderWorks[$key])) {
                            $this->tenderWorks[$key]['discount'] = $discount;
                            $this->tenderWorks[$key]['discount_request'] = $discountRequest;
                        }
                        if (empty($val['class_id']) && isset($this->addWorks[$key])) {
                            $this->addWorks[$key]['discount'] = $discount;
                        }
                    }
                }
            }

            // Создаем новое предложение к тендеру
            $modelOffer = new Offers();
            $modelOffer->setScenario(Offers::SCENARIO_CREATE);
            $modelOffer->user_id = Yii::$app->user->id;
            $modelOffer->company_id = $this->companyId;
            $modelOffer->tender_id = $this->tender->id;
            $modelOffer->time_work = $this->timeWork;
            $modelOffer->time_warranty = $this->timeWarranty;
            $modelOffer->bank_prepayment_warranty = $this->bankPrepaymentWarranty;
            $modelOffer->bank_period_warranty = $this->bankPeriodWarranty;
            $modelOffer->bank_obligation_warranty = $this->bankObligationWarranty;
            $modelOffer->is_archive = Offers::IS_ARCHIVE_NO;
            $modelOffer->text = $this->description ?? '';
            $modelOffer->is_read = Offers::IS_READ_NO;
            $modelOffer->status = Offers::STATUS_SEND;
            $modelOffer->company_author_id = $this->tender->object->company_id;
            if ($modelOffer->save()) {
                unset($this->addWorks[self::ADD_WORK_HIDDEN_KEY]);
                // Высчитываю стоимость работ
                $sumPriceData = $modelOffer->getSumOfferData($this->tenderWorks, $this->addWorks);

                // Сохраняем элементы предложения (рубрики сметы тендера)
                foreach ($this->tender->estimatesRubrics as $rubric) {

                    if (!$elemet = $this->addOfferElement([
                        'offer_id' => $modelOffer->id,
                        'class_name' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID,
                        'class_id' => $rubric->id,
                        'title' => $rubric->title,
                        'sum' => $sumPriceData['rubrics'][$rubric->id]['noDiscount'] ?? 0,
                        'sum_discount' => $sumPriceData['rubrics'][$rubric->id]['withDiscount'] ?? 0,
                        'discount' => $discounts['rubrics'][$rubric->id] ?? null,
                        'sum_discount_request' => $sumPriceData['rubrics'][$rubric->id]['withDiscountRequest'] ?? 0,
                        'discount_request' => $discountsRequests['rubrics'][$rubric->id] ?? null,
                    ], OffersElements::SCENARIO_CREATE_RUBRIC)) {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения данных'), false);
                        $needRollBack = true;
                    }

                    // Сохраняем элементы предложения (работы по смете тендера)
                    if (!empty($rubric->elements)) {
                        foreach ($rubric->elements as $element) {
                            $workValue = $this->tenderWorks[$element->id];
                            if (!$this->addOfferElement([
                                'offer_id' => $modelOffer->id,
                                'class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID,
                                'class_id' => $element->id,
                                'title' => $element->title,
                                'count' => $element->count,
                                'unit_id' => $element->unit_id,
                                'price_work' => $workValue['price_work'],
                                'price_material' => $workValue['price_material'],
                                'sum' => $sumPriceData['elements'][$element->id]['noDiscount'] ?? 0,
                                'sum_discount' => $sumPriceData['elements'][$element->id]['withDiscount'] ?? 0,
                                'discount' => $discounts['elements'][$element->id] ?? null
                            ])) {
                                Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения данных'), false);
                                $needRollBack = true;
                            }
                        }
                    }
                }


                // Сохраняем элементы предложения (доп. работы по смете тендера)
                foreach ($this->addWorks as $keyWork => $workValue) {
                    $idWork = $workValue['id'];

                    if ($addWork = $this->addOfferElement([
                        'offer_id' => $modelOffer->id,
                        'class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID,
                        'title' => $workValue['title'],
                        'count' => $workValue['count'],
                        'unit_id' => $workValue['unit_id'],
                        'price_work' => $workValue['price_work'],
                        'price_material' => $workValue['price_material'],
                        'sum' => $sumPriceData['addWorks'][$idWork]['noDiscount'] ?? 0,
                        'sum_discount' => $sumPriceData['addWorks'][$idWork]['withDiscount'] ?? 0
                    ])) {
                        $this->addWorks[$keyWork]['id'] = $addWork->id;
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения данных'), false);
                        $needRollBack = true;
                    }
                }

                if (!$needRollBack) {
                    // Сораняем еще раз запись предложения что бы обновить сумму тендера, т.к. были созданы работы и требуется пересчитать
                    $modelOffer->setScenario(Offers::SCENARIO_CREATE);
                    if ($modelOffer->save(false)) {
                        // Сохраняю коментарии к рубрикам сметы оставленные застройщиком
                        $this->saveComments($modelOffer);
                        // Переназначаю файлы с архивного предложения, на текущее
                        EntityFile::updateAll(
                            ['modelId' => $modelOffer->id],
                            ['modelName' => ClassMap::CLASS_OFFER, 'modelId' => $oldOffer->id ?? 0]
                        );

                        $transactionOffer->commit();
                        // Отправляю письмо автору тендера о новом предложении
                        $this->sendEmailAboutOffer($modelOffer);
                        return $modelOffer->id;
                    }
                }
            }

            $transactionOffer->rollBack();
            return false;
        } catch(\Throwable $e) {
            $transactionOffer->rollBack();
            Yii::error('Ошибка при добавления предложения для тендера с  ID=' . ($this->tender->id ?? "") . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderOffers');
            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения данных'), false);

            return false;
        }
    }

    /**
     * Сохраняет коментарии оставленные к предложению (элементы рубрики сметы), а так же к доп. работам
     *
     * @param bool $isAddWorks
     */
    private function saveComments($offer)
    {
        // Сохраняю коментарии к рубрикам сметы
        if (!empty($this->tenderComments['EstimatesRubrics'])) {
            foreach ($this->tenderComments['EstimatesRubrics'] as $rubricId => $comment) {
                $rubric = EstimatesRubrics::find()->where(['id' => $rubricId])->one();
                if (empty($comment) || empty($rubric)) {
                    continue;
                }
                if (EntityComment::addComment($rubric, $comment, $offer->id)) {
                    // Отпраляю письмо автору тендера
                    $this->sendEmailComment($offer);
                }
            }
        }
        // Сохраняю коментарии к доп. работам
        if (!empty($this->addWorks)) {
            foreach ($this->addWorks as $addWork) {
                $comment = trim($addWork['comment'] ?? '');
                if (empty($comment)) {
                    continue;
                }
                $model = OffersElements::find()->where(['id' => $addWork['id']])->one();
                if (!empty($model)) {
                    EntityComment::addComment($model, $comment, $offer->id);
                }
            }
        }
        // Сохраняю коментарий для отклоненных/одобреных запросов скидки
        foreach ($this->getOfferDiscountRequestData() as $className => $params) {
            foreach ($params as $param) {
                $comment = trim($param['comment']);
                if (empty($param['comment'])) {
                    continue;
                }
                $query = $className == ClassMap::CLASS_ESTIMATES_RUBRICS_ID ? EstimatesRubrics::find() : EstimatesElements::find();
                $model = $query->where(['id' => $param['id']])->one();
                if (empty($model)) {
                    continue;
                }
                EntityComment::addComment($model, $comment, $offer->id);
            }
        }

    }

    /**
     * Создает запись в списке эллементов предложения
     *
     * @param array $param
     * @param string $scenario
     *
     * @return bool|OffersElements|ActiveRecord
     */
    private function addOfferElement($param = [], $scenario = OffersElements::SCENARIO_CREATE_ELEMENT)
    {
        if (!empty($param['id'])) {
            return OffersElements::find()->where(['id' => $param['id']])->one();
        }

        $model = new OffersElements();
        $model->setScenario($scenario);
        foreach ($param as $key => $value) {
            $model->{$key} = $value;
        }


        return $model->save() ? $model : false;
    }

    /**
     * Возвращает список вариантов для гарантии
     * @return array
     */
    public static function getWarrantyList()
    {
        return [
            Offers::TYPE_WARRANTY_YES => Yii::t('app', 'Да'),
            Offers::TYPE_WARRANTY_NO => Yii::t('app', 'Нет'),
        ];
    }

    /**
     * Отправка письма автору предложения о том, что оно было скоректировано автором тендера
     *
     * @param object $offer
     * @return bool
     */
    public function sendEmailCorrectOffer($offer)
    {
        if (!$user = $offer->user) {
            return false;
        }
        $viewEmail = 'tender-correct-offer';
        $subjectEmail = 'TENDEROS. Поступил запрос на корректирование Вашего предложение для тендера № ' . $this->tender->id;
        $paramView = [];
        $paramView['userName'] = $user->username;
        $paramView['email'] = $user->email;
        $paramView['numberTender'] = $this->tender->id;
        $paramView['nameTender'] = $this->tender->title;
        $paramView['companyUrl'] = $this->tender->object->company->url;
        $paramView['companyName'] = $this->tender->object->company->title;
        $paramView['offerLink'] = Yii::$app->urlManager->createAbsoluteUrl([
            'dashboard/offer_add',
            'offerId' => $offer->id,
            'tenderId' => $this->tender->id
        ]);

        return SendMail::send(
            $viewEmail,
            $paramView,
            Yii::$app->params['infoEmailFrom'],
            [$user->email => $user->username],
            $subjectEmail
        );
    }

    /**
     * Отправка письма при создании / корректировке предложения по тендеру.
     *
     * @param object $offer
     * @return bool
     */
    private function sendEmailAboutOffer($offer)
    {
        if (!$user = $this->tender->object->user) {
            return false;
        }
        // Проверяю, первое ли это предложение для этого тендера от компании
        $checkFirstOfferCompany = Offers::find()->where(['tender_id' => $offer->tender_id])->andWhere(['company_id' => $offer->company_id])->andWhere(['!=', 'id', $offer->id])->count();
        $viewEmail = empty($checkFirstOfferCompany) ? 'tender-estimates-offer' : 'tender-estimates-edit-offer';
        $subjectEmail = (empty($checkFirstOfferCompany) ? 'TENDEROS. Новое предложение для Вашего тендера ' : 'TENDEROS. Скорректировано предложение для Вашего тендера ') . '№ ' . $this->tender->id;
        $paramView = [];
        $paramView['userName'] = $user->username;
        $paramView['email'] = $user->email;
        $paramView['numberTender'] = $this->tender->id;
        $paramView['nameTender'] = $this->tender->title;
        $paramView['companyUrl'] = $this->tender->object->company->url;
        $paramView['companyName'] = $this->tender->object->company->title;
        $paramView['companyOfferName'] = $offer->company->title;
        $paramView['companyOfferUrl'] = $offer->company->url;
        $paramView['offerLink'] = Yii::$app->urlManager->createAbsoluteUrl([
            'dashboard/tender_offer_view',
            'id' => $offer->id,
            'companyUrl' => $this->tender->object->company->url,
            'tenderId' => $this->tender->id
        ]);

        return SendMail::send(
            $viewEmail,
            $paramView,
            Yii::$app->params['infoEmailFrom'],
            [$user->email => $user->username],
            $subjectEmail
        );
    }

    /**
     * Отправка письма при добавлении коментария. Если флаг !$isOfferAuthor - значит коментарий оставил автор тендера для предложения,
     * в противном случае - это коментарий от автора предложения для авторов тендера
     *
     * @param object $offer
     * @param bool $isOfferAuthor
     * @return bool
     */
    public function sendEmailComment($offer, $isOfferAuthor = true)
    {
        $user = $isOfferAuthor ? $offer->tender->object->user : $offer->user;
        if (!$user) {
            return false;
        }
        $viewEmail = 'comment-customer-offer';
        $subjectEmail = ($isOfferAuthor ? 'TENDEROS. Новый коментарий для Вашего тендера' : 'TENDEROS. Новый коментарий для вашего предложения от автора тендера') . ' № ' . $offer->tender->id;
        $paramView = [];
        $paramView['userName'] = $user->username;
        $paramView['email'] = $user->email;
        $paramView['numberTender'] = $offer->tender->id;
        $paramView['nameTender'] = $offer->tender->title;
        $paramView['companyUrl'] = $offer->tender->object->company->url;
        $paramView['companyName'] = $offer->tender->object->company->title;
        $paramView['isOfferAuthor'] = $isOfferAuthor;
        $paramView['offerLink'] = Yii::$app->urlManager->createAbsoluteUrl([
            'dashboard/tender_offer_view',
            'id' => $offer->id,
            'companyUrl' => $offer->tender->object->company->url,
            'tenderId' => $offer->tender->id,
        ]);

        return SendMail::send(
            $viewEmail,
            $paramView,
            Yii::$app->params['infoEmailFrom'],
            [$user->email => $user->username],
            $subjectEmail
        );
    }

    /**
     * Возращает стоимость работы элемента сметы
     *
     * @param EstimatesElements $element
     * @return float
     */
    public function getOfferElementPriceWork($element)
    {
        return $this->tenderWorks[$element->id]['price_work'] ?? $element->getOfferPriceWork($this->offerId);
    }

    /**
     * Возращает стоимость материала элемента сметы
     *
     * @param EstimatesElements $element
     * @return float
     */
    public function getOfferElementPriceMaterial($element)
    {
        return $this->tenderWorks[$element->id]['price_material'] ?? $element->getOfferPriceMaterial($this->offerId);
    }

    /**
     * Возращает форму предложения по модели преложения
     *
     * @param Offers $offer
     * @return OfferForm
     */
    public static function setOfferFormByOffer(Offers $offer)
    {
        $model = new self();
        $model->tender = $offer->tender_id;
        $model->companyId = $offer->company_id;
        $model->offer = $offer;
        $model->offerId = $offer->id;
        $model->setOfferData();

        return $model;
    }

    /**
     * Возращает расчитанную сумму коректировки для запроса скидки
     *
     * @return mixed
     */
    public function getCalcSumDiscountRequest()
    {
        return $this->offerPrices['totalOfferSum']['withRequestDiscount'];
    }

    /**
     * Возращает расчитанную сумму коректировки для утвержденной скидки
     *
     * @return mixed
     */
    public function getCalcSumWithDiscount()
    {
        return $this->offerPrices['totalOfferSum']['withDiscount'];
    }

    /**
     * Возращает итоговую стоимость предложения
     *
     * @return mixed
     */
    public function getTotalSum()
    {
        if (empty($this->offerPrices)) {
            return null;
        }
        return !empty($this->offerPrices['totalOfferSum']['totalSumWithDiscount'])
            ? $this->offerPrices['totalOfferSum']['totalSumWithDiscount']
            : $this->offerPrices['totalOfferSum']['totalSum'];
    }

    /**
     * Возращает начальную стоимость предложения. Начальная стоимость - это стомость самого первого предложения
     *
     * @return mixed
     */
    public function getSumInitialPrice()
    {

        $offer = $this->getOffer();
        $firstOffer = Offers::find()
            ->where(['company_id' => $offer->company_id])
            ->andWhere(['tender_id' => $offer->tender_id])
            ->orderBy(['id' => SORT_ASC])
            ->asArray()
            ->one();

        return $firstOffer['sum'] ?? $this->offerPrices['totalOfferSum']['noDiscount'];
    }
}
