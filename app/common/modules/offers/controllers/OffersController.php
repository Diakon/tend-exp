<?php
namespace common\modules\offers\controllers;

use modules\crud\actions\DeleteAction;
use modules\crud\actions\ListAction;
use backend\controllers\BackendController;
use modules\crud\actions\CreateAction;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\Json;
use common\modules\offers\models\backend\Offers;
use common\modules\offers\models\backend\OffersElements;


/**
 * Основной контроллер.
 */
class OffersController extends BackendController
{
    /**
     * @var string|boolean the name of the layout to be applied to this controller's views.
     */
    public $layout = '@root/themes/backend/views/layouts/main';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'offers_list',
                            'offers_update',
                            'offers_elements_list',
                            'offers_elements_update',
                            'offers_delete',
                            'offers_elements_delete',
                        ],
                        'roles' => ['administrator'],
                    ],

                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'offers_list' => [
                'class' => ListAction::class,
                'template' => '/backend/offers_list',
                'model' => Offers::class,
            ],
            'offers_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Offers::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Offers::classNameShort('id') . '_edit',
                ],
            ],
            'offers_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Offers::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Offers::classNameShort('id') . '_create',
                ],
            ],
            'offers_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => Offers::class,
                    'returnUrl' => ['backend/offers_list'],
                ],
            ],
            'offers_elements_list' => [
                'class' => ListAction::class,
                'template' => '/backend/offers_elements_list',
                'model' => OffersElements::class,
            ],
            'offers_elements_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => OffersElements::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . OffersElements::classNameShort('id') . '_edit',
                ],
            ],
            'offers_elements_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => OffersElements::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . OffersElements::classNameShort('id') . '_create',
                ],
            ],
            'offers_elements_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => OffersElements::class,
                    'returnUrl' => ['backend/offers_elements_list'],
                ],
            ],
        ];
    }

    /**
     * Обработка системных ошибок.
     *
     * @return string
     */
    public function actionError()
    {
        $this->layout = '@cms/themes/backend/views/layouts/base';
        /** @var \yii\base\Exception $exception */
        $exception = Yii::$app->errorHandler->exception;
        $handler = Yii::$app->errorHandler;

        $data = [];
        $data['name'] = $exception->getName();
        $data['message'] = $exception->getMessage();
        $data['line'] = $exception->getLine();
        $data['file'] = $exception->getFile();
        $data['trace'] = $exception->getTraceAsString();
        $data['code'] = $exception->getCode();

        $code = $exception instanceof \yii\web\HttpException ? $exception->statusCode : $exception->getCode();
        $message = $exception->getMessage();


        if (Yii::$app->request->isAjax) {
            return print_r($data, true);
        } else {
            $this->view->title = $code . ' ' . $exception->getMessage();
            $params = compact('exception', 'code', 'message','handler');
            Yii::error(Json::encode($data), 'error');

            return $this->render('@cms/themes/backend/views/backend/error', $params);
        }
    }
}
