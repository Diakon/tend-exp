<?php
namespace common\modules\offers\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\ClassMap;
use common\modules\offers\models\forms\OfferForm;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\Tenders;
use frontend\models\EntityComment;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class OfferAddAction
 * @package common\modules\offers\controllers\actions
 */
class OfferAddAction extends BaseAction
{
    /**
     * Добавление предложения для тендера
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        $this->controller->layout = $this->layout;
        $this->controller->view->params['isLongView'] = true;

        $tenderId = Yii::$app->request->get('tenderId', 0);
        $tender = Tenders::find()->where(['id' => (int)$tenderId])->andWhere(['status' => Tenders::STATUS_ACTIVE])->one();
        $company = Yii::$app->user->getCompany()->one();

        if (empty($tender) || empty($company)) {
            throw new NotFoundHttpException('Tender not found');
        }

        $offerId = Yii::$app->request->get('offerId', null);
        $offer = !empty($offerId) ? Offers::find()->where(['id' => $offerId])->one() : new Offers();

        $model = new OfferForm();
        $model->tender = $tender;
        $model->companyId = $company->id;
        $model->offer = $offer;
        $model->offerId = $offer->id;
        $model->className = ClassMap::CLASS_ESTIMATES_ELEMENTS_ID;

        $tHash = Yii::$app->request->get('tHash');
        $offer->tHash = $offer->isNewRecord && !empty($tHash) && empty($offer->id) ? $tHash : $offer->id;

        $model->offerTHash = $offer->tHash;

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            // Комантарий для рубрик смет
            if (!empty(Yii::$app->request->post('EstimatesRubricsComment'))) {
                $commentParams = Yii::$app->request->post('EstimatesRubricsComment');
                $commentParams['companyId'] = $company->id;
                $result = $this->commentRubrics($commentParams, $offer->tHash);
                Yii::$app->response->statusCode = $statusCode = !empty($result) ? 200 : 500;
                echo Json::encode(['status' => $statusCode, 'result' => $result ?? []]);
            }

            // Если операции на форме (например, пересчет)
            if (Yii::$app->request->get('type')) {
                $typeAction = Yii::$app->request->get('type');
                switch ($typeAction) {
                    case 'recount':
                        $sumOfferData = $this->getRecountOfferData($offer, Yii::$app->request->post('OfferForm'));

                        echo Json::encode(['sum' => $sumOfferData]);
                        break;
                }
            }
            exit();
        }

        $model->setOfferData();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $offerId = $model->createOffer();
            if (!empty($offerId)) {
                // Все коментарии, которые были оставлены в этом или в предыдущих предложениях для этого тендера обновляем для последнего предложения
                $oldOfferIds = ArrayHelper::map(Offers::find()->where(['tender_id' => $tender->id])->andWhere(['company_id' => $offer->company_id])->asArray()->all(), 'id', 'id');
                if (empty($oldOfferIds[$offer->tHash])) {
                    $oldOfferIds = ArrayHelper::merge($oldOfferIds ?? [], [$offer->tHash]);
                }
                EntityComment::updateAll(
                    ['offerId' => $offerId],
                    [
                        'AND',
                        ['in', 'offerId', $oldOfferIds]
                    ]);

                Yii::$app->response->redirect(['/dashboard/offer_add', 'tenderId' => $tenderId, 'offerId' => $offerId]);
                Yii::$app->end();
            }
        }

        $offer->prepareModelAction($tHash);
        unset($model->addWorks[OfferForm::ADD_WORK_HIDDEN_KEY]);

        return $this->renderAction($this->params['template'], [
            'model' => $model,
            'offer' => $offer,
            'tHash' => $tHash
        ]);
    }

    /**
     * Возвращает стоимость работ на форме
     *
     * @param Offers $offer
     * @param array $paramForm
     * @return array
     */
    private function getRecountOfferData($offer, $paramForm)
    {
        unset($paramForm['addWorks'][OfferForm::ADD_WORK_HIDDEN_KEY]);

        if (!empty($paramForm['addWorks'])) {
            foreach ($paramForm['addWorks'] as $key => $values) {
                $priceWork = preg_replace("/[^,.0-9]/", '', $values['price_work']);
                $priceMaterial = preg_replace("/[^,.0-9]/", '', $values['price_material']);
                $paramForm['addWorks'][$key]['price_work'] = floatval(!empty($priceWork) ? $priceWork : 0);
                $paramForm['addWorks'][$key]['price_material'] = floatval(!empty($priceMaterial) ? $priceMaterial : 0);
            }
        }

        $offerSum = $offer->getSumOfferData($paramForm['tenderWorks'], $paramForm['addWorks'] ?? []);

        // Получаю сумму всего по предложению
        $totalOfferSum = [];
        $totalOfferSum['noDiscount'] = 0;
        $totalOfferSum['withDiscount'] = 0;
        $totalOfferSum['discount'] = null;
        foreach (['elements', 'addWorks'] as $key) {
            if (empty($offerSum[$key])) {
                continue;
            }
            foreach ($offerSum[$key] as $id => $prices) {
                $totalOfferSum['noDiscount'] += $prices['noDiscount'];
                $totalOfferSum['withDiscount'] += $prices['withDiscount'];
            }
        }
        // Высчитываю общую скидку
        $totalDiscount = $totalOfferSum['withDiscount'] > 0 ? $totalOfferSum['withDiscount'] * 100 / $totalOfferSum['noDiscount'] : null;
        $totalDiscount = !empty($totalDiscount) ? (100 - $totalDiscount) : $totalDiscount;
        if (!empty($totalDiscount)) {
            $totalOfferSum['discount'] = round($totalDiscount);
            $totalOfferSum['discountSum'] = Yii::$app->formatter->asMoney($totalOfferSum['noDiscount'] - $totalOfferSum['withDiscount']);
        }

        $totalOfferSum['noDiscount'] = Yii::$app->formatter->asMoney($totalOfferSum['noDiscount']);
        $totalOfferSum['withDiscount'] = Yii::$app->formatter->asMoney($totalOfferSum['withDiscount']);

        // Проходим по всем ценам и приводим к формалищованому виду
        foreach ($offerSum as $key => $value) {
            foreach ($value as $id => $prices) {
                $offerSum[$key][$id]['formatterNoDiscount'] = !empty($prices['noDiscount']) && $prices['noDiscount'] > 0 ? Yii::$app->formatter->asMoney($prices['noDiscount']) : $prices['noDiscount'];
                $offerSum[$key][$id]['formatterWithDiscount'] = !empty($prices['withDiscount']) && $prices['withDiscount'] > 0 ? Yii::$app->formatter->asMoney($prices['withDiscount']) : $prices['noDiscount'];
            }
        }


        $offerSum = ArrayHelper::merge($offerSum, ['totalOfferSum' => $totalOfferSum]);

        return $offerSum;
    }

    /**
     * Создает коментарий от автора предложения к смете тендера
     *
     * @param array $commentParams
     * @param string|integer $offerId
     * @return bool
     */
    private function commentRubrics($commentParams, $offerId)
    {
        $model = EstimatesRubrics::find()->where(['id' => $commentParams['id']])->one();
        return EntityComment::addComment($model, $commentParams['comment'], $offerId);
    }
}