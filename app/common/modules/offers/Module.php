<?php
namespace common\modules\offers;

use common\modules\offers\controllers\OffersController;

/**
 * Class Module.
 * @package common\modules\offers
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\offers\controllers';

    public $controllerMap = [
        'company' => OffersController::class
    ];
}
