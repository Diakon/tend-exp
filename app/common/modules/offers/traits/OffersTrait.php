<?php
namespace common\modules\offers\traits;

use common\components\ClassMap;
use common\modules\offers\models\common\OffersElements;
use common\modules\offers\models\forms\OfferForm;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\frontend\EstimatesElements;
use common\modules\tender\models\frontend\EstimatesRubrics;
use yii\helpers\ArrayHelper;

/**
 * Трейт для обработки данных по предложения к тендеру
 *
 * Trait OffersTrait
 * @package common\modules\offers\traits
 */
trait OffersTrait
{
    /**
     * Пересчитывает все запросы скидки от автора тендера и установленные скидки в предложении
     *
     * @param \common\modules\offers\models\common\Offers $offer
     */
    public function calcDiscountOffer(\common\modules\offers\models\common\Offers $offer)
    {
        // Произвожу расчет запроса скидки и утвержденной ранее скидки
        foreach (['discount', 'discount_request'] as $field) {
            // Получаю ID рубрик и значение установленной/запрашиваемой скиодк у рубрик, для которых они были установленны
            $ids = ArrayHelper::map($offer->getElements()->where(['class_name' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID])->andWhere(['>', $field, 0])->asArray()->all(), 'class_id', $field);
            if (empty($ids)) {
                // Нет запроса скидки / утвержденной скидки - обнуляю суммы в sum_discount и sum_discount_request
                OffersElements::updateAll([$field => null, 'sum_' . $field => 0],
                    [
                        'AND',
                        ['=', 'offer_id', $offer->id],
                    ]);
                continue;
            }
            foreach ($ids as $id => $valueDiscount) {
                $rubricsIds = [];
                $rubric = EstimatesRubrics::find()->where(['id' => $id])->one();
                if (empty($rubric)) {
                    continue;
                }
                $rubricsIds[] = $rubric->id;
                // Получаю ID рубрик, которые являются дочерними для нее
                $rubricsIds = ArrayHelper::merge($rubricsIds, ArrayHelper::map($rubric->children()->asArray()->all(), 'id', 'id'));
                // Получаю ID работ, которые относятся к этим рубрикам
                $worksIds = ArrayHelper::map(EstimatesElements::find()->where(['in', 'rubric_id', $rubricsIds])->asArray()->all(), 'id', 'id');
                // Произвожу расчет стоимости работ
                foreach ($offer->getElements()->where(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])->andWhere(['in', 'class_id', $worksIds])->all() as $element)
                {
                    $sum = $this->calcSumElement($element->count, $element->price_work, $element->price_material);
                    $sumDiscount = $this->calcSumElementWithDiscount($sum, $valueDiscount);
                    $key = 'sum_' . $field;
                    $element->sum = $sum;
                    $element->{$key} = $sumDiscount;
                    $element->save(false);
                }
            }
        }
        // Обновляю все суммы стоимости работ для категорий
        foreach ($offer->getElements()->where(['class_name' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID])->all() as $element)
        {
            $worksIds = ArrayHelper::map(EstimatesElements::find()->where(['rubric_id' => $element->class_id])->asArray()->all(), 'id', 'id');
            $element->sum = $offer->getElements()->where(['in', 'class_id', $worksIds])->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])->sum('sum');
            $element->sum_discount = $offer->getElements()->where(['in', 'class_id', $worksIds])->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])->sum('sum_discount');
            $element->sum_discount_request = $offer->getElements()->where(['in', 'class_id', $worksIds])->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])->sum('sum_discount_request');

            $element->save(false);
        }
    }


    /**
     * Обнуляет дискон и запрос дисконта у родительских разделов и товаров, если указан дисконт для этой записи
     *
     * @param OffersElements $offerElement
     */
    public function updateDiscount(OffersElements $offerElement)
    {
        foreach (['discount', 'discount_request'] as $field) {
            // Есть дисконт / запрос дисконта от автора тендера - обнуляю дисконт для рубрик / работ сметы предложений, в зависимости от $model
            if (!empty($offerElement->{$field})) {
                $class = ClassMap::getClassName($offerElement->class_name);
                $model = $class::find()->where(['id' => (int)$offerElement->class_id])->one();
                if (!empty($model)) {
                    switch ($offerElement->class_name) {
                        case ClassMap::CLASS_ESTIMATES_ELEMENTS_ID:
                            // Если дисконт для работы - убираю дисконт для всех разделов в которые входит рубрика товара
                            // Получаю ID родительского раздела и его родителей
                            $rubric = EstimatesRubrics::find()->where(['id' => (int)$model->rubric_id])->one();

                            if (!empty($rubric)) {
                                $rubricsIds = ArrayHelper::map($rubric->parents()->andWhere(['>', 'depth', 0])->all(), 'id', 'id');
                                $rubricsIds = ArrayHelper::merge($rubricsIds, [$rubric->id]);
                                $this->updateOfferElement($offerElement->offer_id, ClassMap::CLASS_ESTIMATES_RUBRICS_ID, $rubricsIds, [$field => null]);
                            }

                            break;

                        case ClassMap::CLASS_ESTIMATES_RUBRICS_ID:
                            // Если дисконт для рубрики - убираю дисконт для всех дочерних разделов рубрики и для ее товаров
                            // Получаю ID дочерних разделов что бы убрать дисконт с этих разделов
                            $rubricsIds = ArrayHelper::map($model->children()->asArray()->all(), 'id', 'id');
                            if (!empty($rubricsIds)) {
                                $this->updateOfferElement($offerElement->offer_id, ClassMap::CLASS_ESTIMATES_RUBRICS_ID, $rubricsIds, [$field => null]);
                            }
                            // Убираю дисконт у работ этого и всех дочерних разделов
                            $rubricsIds = ArrayHelper::merge($rubricsIds, [$model->id]);
                            $elementsIds = ArrayHelper::map(EstimatesElements::find()->where(['in', 'rubric_id', $rubricsIds])->all(), 'id', 'id');
                            if (!empty($elementsIds)) {
                                $this->updateOfferElement($offerElement->offer_id, ClassMap::CLASS_ESTIMATES_ELEMENTS_ID, $elementsIds, [$field => null]);
                            }
                            break;
                    }
                }
            }
        }
    }

    /**
     * Возвращает сумму предложения с учетом и без учета запроса скидки
     *
     * @return array
     */
    public function getOfferTotalSumPrice()
    {
        $returnSum = [];
        $returnSum['noDiscount'] = $this->getElements()->where(['in', 'class_name', [ClassMap::CLASS_ESTIMATES_ELEMENTS_ID]])->sum('sum');
        $returnSum['withDiscount'] = $this->getElements()->where(['in', 'class_name', [ClassMap::CLASS_ESTIMATES_ELEMENTS_ID]])->sum('sum_discount');
        $returnSum['withRequestDiscount'] = $this->getElements()->where(['in', 'class_name', [ClassMap::CLASS_ESTIMATES_ELEMENTS_ID]])->sum('sum_discount_request');

        return $returnSum;
    }

    /**
     * Возврвщвет стоимость работ и их рубрик, а так же доп. работ
     *
     * @param array $works
     * @return array
     */
    public function getSumOfferData($works = [], $addWorks = [])
    {
        $sumDataArray = [];
        $sumDataArray['rubrics'] = [];
        $sumDataArray['elements'] = [];
        $sumDataArray['addWorks'] = [];
        // Расчет стоимости работ тендера и категорий работ
        foreach ($works as $workId => $workPrices) {
            if (empty($workId)) {
                continue;
            }
            $element = EstimatesElements::find()->where(['id' => $workId])->one();
            // Получаю категории к которым относится работа
            $model = $element->rubric;
            $rubricsIds = ArrayHelper::map($model->parents()->andWhere(['>', 'depth', 0])->asArray()->all(), 'id', 'id');
            $rubricsIds = ArrayHelper::merge($rubricsIds, [$model->id]); // Добавляю ID родительской категории

            // Получаю значение скидки
            $discount = $workPrices['discount'] ?? null;

            // Получаю значение запроса скидки
            $discountRequest = $workPrices['discount_request'] ?? null;

            // Расчитываю стоимость работы с учетом скидки и без
            $sum = $this->calcSumElement($element->count, $workPrices['price_work'], $workPrices['price_material']);
            $sumWithDiscount = !empty($discount) ? $this->calcSumElementWithDiscount($sum, $discount) : null;
            $sumWithDiscountRequest = !empty($discountRequest) ? $this->calcSumElementWithDiscount($sum, $discountRequest) : null;

            foreach ($rubricsIds as $rubricsId) {
                if (!isset($sumDataArray['rubrics'][$rubricsId])) {
                    $sumDataArray['rubrics'][$rubricsId] = [];
                    $sumDataArray['rubrics'][$rubricsId]['noDiscount'] = 0;
                    $sumDataArray['rubrics'][$rubricsId]['withDiscount'] = 0;
                    $sumDataArray['rubrics'][$rubricsId]['withDiscountRequest'] = 0;
                }

                $sumDataArray['rubrics'][$rubricsId]['noDiscount'] += $sum;
                $sumDataArray['rubrics'][$rubricsId]['withDiscount'] += $sumWithDiscount;
                $sumDataArray['rubrics'][$rubricsId]['withDiscountRequest'] += $sumWithDiscountRequest;
            }

            $sumDataArray['elements'][$workId]['noDiscount'] = $sum;
            $sumDataArray['elements'][$workId]['withDiscount'] = $sumWithDiscount;
            $sumDataArray['elements'][$workId]['withDiscountRequest'] = $sumWithDiscountRequest;
        }

        // Расчет стоимости доп. работ
        foreach ($addWorks as $workPrices) {
            $element = \common\modules\offers\models\frontend\OffersElements::find()->where(['id' => $workPrices['id'] ?? 0])->asArray()->one();
            $discount = $element['discount'] ?? null;
            $discountRequest = $workPrices['discount_request'] ?? null;
            $sum = $this->calcSumElement($workPrices['count'], $workPrices['price_work'], $workPrices['price_material']);
            $sumWithDiscount = !empty($discount) ? $this->calcSumElementWithDiscount($sum, $discount) : null;
            $sumWithDiscountRequest = !empty($discountRequest) ? $this->calcSumElementWithDiscount($sum, $discountRequest) : null;
            $idWork = $element['id'] ?? $workPrices['id'];
            $sumDataArray['addWorks'][$idWork]['noDiscount'] = $sum;
            $sumDataArray['addWorks'][$idWork]['withDiscount'] = $sumWithDiscount;
            $sumDataArray['addWorks'][$idWork]['withDiscountRequest'] = $sumWithDiscountRequest;
        }

        return $sumDataArray;
    }


    /**
     * Расчитывает стоимость работы
     *
     * @param integer $count
     * @param float $priceWork
     * @param float $priceMaterial
     * @return float
     */
    private function calcSumElement($count, $priceWork, $priceMaterial)
    {
        return ($count * $priceWork) + ($count * $priceMaterial);
    }

    /**
     * Возвращает сумму с учетом скидки
     *
     * @param integer $sum
     * @param float $discount
     * @return float
     */
    private function calcSumElementWithDiscount($sum, $discount)
    {
        return $sum - ($sum * ($discount / 100));
    }

    /**
     * Обновляет записи в OffersElements
     *
     * @param integer $offerId
     * @param string $className
     * @param array $classIds
     * @param array $attributes
     */
    private function updateOfferElement($offerId, $className, $classIds = [], $attributes = [])
    {
        OffersElements::updateAll($attributes,
            [
                'AND',
                ['class_id' => $classIds],
                ['=', 'offer_id', $offerId],
                ['=', 'class_name', $className]
            ]);
    }

}