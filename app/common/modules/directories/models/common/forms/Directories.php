<?php
namespace common\modules\directories\models\common\forms;

use modules\crud\widgets\FormView;
use common\modules\directories\models\common\Activities;
use common\modules\directories\models\common\MainWork;
use common\modules\directories\models\common\Projects;
use common\modules\directories\models\common\TypesWork;
use common\modules\directories\models\common\Works;
use Yii;

/**
 * Конфигурация формы добавления/редактирования справочника.
 */
class Directories extends \modules\crud\behaviors\FormConfigBehavior
{

    /**
     * Конфигурация кнопок формы
     *
     * @return array
     */
    public function getFormButtonsConfig()
    {
        return [
            'apply' => [
                '<i class="fa fa-pencil"></i> ' . Yii::t('cms/crud', 'Apply'), ['type' => 'submit', 'name' => 'action', 'value' => FormView::ACTION_APPLY, 'class' => 'btn btn-success', 'id' => 'apply-button'],
            ],
            'submit' => [
                '<i class="fa fa-check"></i> ' . Yii::t('cms/crud', 'Save and exit'), ['type' => 'submit', 'name' => 'action', 'value' => FormView::ACTION_SAVE, 'class' => 'btn btn-primary'],
            ],
            'cancel' => [
                '<i class="fa fa-reply-all"></i> ' . Yii::t('cms/crud', 'Cancel'), ['class' => 'btn btn-default'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return $this->owner->isNewRecord ? 'Создание новой записи' : $this->owner->title;
    }

    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $result[] = '<div class="panel-body">';
        $result[] = '<fieldset class="form-horizontal">';
        $result['title'] = ['type' => 'text'];
        $result['title_short'] = ['type' => 'text'];

        switch ($this->owner->className()) {
            case Projects::class:
                $result['parent_id'] = [
                    'type' => self::TYPE_DROP_DOWN_LIST,
                    'items' => Activities::findActiveList(),
                    'attributes' => [
                        'class' => 'select_one form-control',
                    ],
                ];

                break;
            case TypesWork::class:
                $result['parent_id'] = [
                    'type' => self::TYPE_DROP_DOWN_LIST,
                    'items' => MainWork::findActiveList(),
                    'attributes' => [
                        'class' => 'select_one form-control',
                    ],
                ];

                break;
            case Works::class:
                $result['parent_id'] = [
                    'type' => self::TYPE_DROP_DOWN_LIST,
                    'items' => TypesWork::findActiveList(),
                    'attributes' => [
                        'class' => 'select_one form-control',
                    ],
                ];
                $result['unit'] = [
                    'type' => self::TYPE_DROP_DOWN_LIST,
                    'items' => \common\modules\directories\models\common\Directories::$units,
                    'attributes' => [
                        'class' => 'select_one form-control',
                    ],
                ];

                break;
        }


        $result['sort'] = ['type' => 'text'];
        $result['status'] = [
            'type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];




        return $result;
    }
}
