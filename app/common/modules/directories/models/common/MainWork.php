<?php
namespace common\modules\directories\models\common;

use modules\crud\models\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Направления работ.
 */
class MainWork extends Directories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['status', 'validateStatus'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validateStatus($attribute, $params)
    {
        if ($this->$attribute == ActiveRecord::STATUS_INACTIVE && !$this->isNewRecord) {
            $count = TypesWork::find()->where(['parent_id' => $this->id])->count();
            if (!empty($count)) {
                $this->addError($attribute, 'Эта запись не может быть отключена, т.к. используется в справочнике "Группы работ"');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = new TypeQuery(get_called_class());
        $query->alias(self::classNameShort('id'));
        $query->andWhere([self::classNameShort('id') . '.type' => self::classNameShort('id')]);

        return $query;
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Направления работ')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Направления работ'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать направление') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

}
