<?php
namespace common\modules\directories\models\common;

/**
 * Валюта проектов.
 */
class Currency extends Directories
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = new TypeQuery(get_called_class());
        $query->alias(self::classNameShort('id'));
        $query->andWhere([self::classNameShort('id') . '.type' => self::classNameShort('id')]);

        return $query;
    }

    /**
     * Возвращает активные валюты с сортировкой
     *
     * @return object
     */
    public static function getActive()
    {
        return self::find()->andWhere(['status' => self::STATUS_ACTIVE])->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Валюта')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Валюта'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать валюту') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * Возращает массив списка валют
     *
     * @param null $id
     * @return array|mixed
     */
    public static function getCurrencyList($currencyId = null)
    {
        $list = [];
        foreach (self::find()->asArray()->all() as $data) {
            $id = $data['id'];
            $name = !empty($data['title_short']) ? $data['title_short'] : $data['title'];
            $list[$id] = $name;
        }

        return !empty($currencyId) ? $list[$currencyId] : $list;
    }
}
