<?php
namespace common\modules\directories\models\common;

use modules\crud\models\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Группы работ.
 */
class TypesWork extends Directories
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['parent_id',], 'required'],
            ['status', 'validateStatus'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validateStatus($attribute, $params)
    {
        if ($this->$attribute == ActiveRecord::STATUS_INACTIVE && !$this->isNewRecord) {
            $count = Works::find()->where(['parent_id' => $this->id])->count();
            if (!empty($count)) {
                $this->addError($attribute, 'Эта запись не может быть отключена, т.к. используется в справочнике "Работы"');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'parent_id' => Yii::t('app', 'Основное направление работ'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = new TypeQuery(get_called_class());
        $query->alias(self::classNameShort('id'));
        $query->andWhere([self::classNameShort('id') . '.type' => self::classNameShort('id')]);

        return $query;
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Группы работ')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Группы работ'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать новую группу работ') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

}
