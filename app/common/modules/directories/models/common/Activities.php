<?php
namespace common\modules\directories\models\common;

use modules\crud\models\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Направления строительства.
 * Переименован в соответствии с заданием https://basecamp.com/1795438/projects/15055171/todos/359090287
 */
class Activities extends Directories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['status', 'validateStatus'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validateStatus($attribute, $params)
    {
        if ($this->$attribute == ActiveRecord::STATUS_INACTIVE && !$this->isNewRecord) {
            $count = Projects::find()->where(['parent_id' => $this->id])->count();
            if (!empty($count)) {
                $this->addError($attribute, 'Эта запись не может быть отключена, т.к. используется в справочнике "Типы объекта"');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = new TypeQuery(get_called_class());
        $query->alias(self::classNameShort('id'));
        $query->andWhere([self::classNameShort('id') . '.type' => self::classNameShort('id')]);

        return $query;
    }


    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Направления строительства')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Направления строительства'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать направление') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
