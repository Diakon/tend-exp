<?php
namespace common\modules\directories\models\common;

/**
 *  Максимальное количество сотрудников
 */
class EmployeesMax extends Directories
{

    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = new TypeQuery(get_called_class());
        $query->alias(self::classNameShort('id'));
        $query->andWhere([self::classNameShort('id') . '.type' => self::classNameShort('id')]);

        return $query;
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Максимальное количество сотрудников')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Максимальное количество сотрудников'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать диапазон') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

}
