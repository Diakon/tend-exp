<?php
namespace common\modules\directories\models\common;

use modules\crud\behaviors\FormConfigBehavior;
use Yii;
use yii\base\InvalidCallException;
use modules\crud\models\ActiveQuery;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use modules\crud\actions\CreateAction;
use modules\crud\actions\DeleteAction;

/**
 * Справочники.
 *
 * @property integer $id          Идентификатор.
 * @property string  $type        Тип справочника.
 * @property string  $title       Наименование.
 * @property string  $title_short Краткое наименование.
 * @property integer $parent_id   ID родительской записи.
 * @property integer $unit        Тип единицы измерения.
 * @property integer $status      Статус.
 * @property integer $sort        Сортировка.
 * @property integer $created_at  Дата создания записи.
 * @property integer $updated_at  Дата изменения записи.
 */
class Directories extends ActiveRecord
{
    /**
     * Единицы измерения
     */
    const UNIT_M3 = 1;
    const UNIT_M2 = 2;
    const UNIT_MH = 3;
    const UNIT_PIECES = 4;
    const UNIT_PM = 5;
    const UNIT_T = 6;

    public static $units = [
        self::UNIT_M3 => 'м3',
        self::UNIT_M2 => 'м2',
        self::UNIT_MH => 'чел./час',
        self::UNIT_PIECES => 'шт.',
        self::UNIT_PM => 'п.м.',
        self::UNIT_T => 'тн.',
    ];


    public static function getUnitData($unitId = null, $column = 'title')
    {
        $units = [
            self::UNIT_M3 => ['id' => self::UNIT_M3, 'title' => 'м<sup>3</sup>'],
            self::UNIT_M2 => ['id' => self::UNIT_M3, 'title' => 'м<sup>3</sup>'],
            self::UNIT_MH => ['id' => self::UNIT_M3, 'title' => 'чел./час'],
            self::UNIT_PIECES => ['id' => self::UNIT_M3, 'title' => 'шт.'],
            self::UNIT_PM => ['id' => self::UNIT_M3, 'title' => 'п.м.'],
            self::UNIT_T => ['id' => self::UNIT_M3, 'title' => 'тн.'],
        ];
        return $units[$unitId] ? $units[$unitId][$column] : $units;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%directories}}';
    }

    /**
     * @inheritdoc
     */
    public $pageSize = 20;


    /**
     * @inheritdoc
     */
    public function getListLink($action = '_list', array $params = [])
    {
        $action = static::classNameShort('id') . $action;

        return $this->generateUrl($action, $params);
    }

    /**
     * @inheritdoc
     */
    public function getAddLink($action = '_add', array $params = [])
    {
        $action = static::classNameShort('id') . $action;

        return $this->generateUrl($action, $params, [static::$paramIdent => $this->{static::$paramColumn}]);
    }

    /**
     * @inheritdoc
     */
    public function getUpdateLink($action = '_edit', array $params = [])
    {
        $action = static::classNameShort('id') . $action;

        return $this->generateUrl($action, $params, [static::$paramIdent => $this->{static::$paramColumn}]);
    }

    /**
     * @inheritdoc
     */
    public function getDeleteLink($action = '_delete', array $params = [])
    {
        $action = static::classNameShort('id') . $action;

        return $this->generateUrl($action, $params, [static::$paramIdent => $this->{static::$paramColumn}]);
    }


    /**
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->orderBy('title')->andWhere([self::tableName().'.type' => self::classNameShort('id')]);
    }

    /**
     * Список активных записей
     *
     * @param string $attr1
     * @param string $attr2
     * @param null $parentId
     * @return array
     */
    public static function findActiveList($attr1 = 'id', $attr2 = 'title', $parentId = null)
    {
        $query = self::findActive();
        if (!empty($parentId)) {
            $query->andWhere(['parent_id' => $parentId]);
        }
        return ArrayHelper::map($query->orderBy(['title' => SORT_ASC])->asArray()->all(), $attr1, $attr2);
    }

    /**
     * Активные параметры
     *
     * @param bool $orderTitle
     * @return ActiveQuery
     */
    public static function findActive($orderTitle = false)
    {
        $query = static::find()->andWhere('status =:status', [':status' => ActiveRecord::STATUS_ACTIVE]);
        if ($orderTitle) {
            $query->orderBy(['title' => SORT_ASC]);
        }
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);
        $this->type = self::classNameShort('id');
        $this->sort = !empty($this->sort) ? $this->sort : 1;

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find()->andFilterWhere(['LIKE', 'title', $this->title]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC],
                'attributes' => [ 'id', 'sort'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * @var array[] Список справочников.
     */
    private static $list = [];

    /**
     * Возвращает список справочников.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Activities::class,
                    'title' => Yii::t('app', 'Направления строительства'),
                ],
                [
                    'class' => Projects::class,
                    'title' => Yii::t('app', 'Типы объекта'),
                ],
                [
                    'class' => MainWork::class,
                    'title' => Yii::t('app', 'Направления работ'),
                ],
                [
                    'class' => TypesWork::class,
                    'title' => Yii::t('app', 'Группы работ'),
                ],
                [
                    'class' => Works::class,
                    'title' => Yii::t('app', 'Виды работ'),
                ],
                [
                    'class' => FunctionsCompany::class,
                    'title' => Yii::t('app', 'Функции компании'),
                ],
                [
                    'class' => Currency::class,
                    'title' => Yii::t('app', 'Валюта'),
                ],
                [
                    'class' => EmployeesNow::class,
                    'title' => Yii::t('app', 'Количество сотрудников'),
                ],
                [
                    'class' => EmployeesMax::class,
                    'title' => Yii::t('app', 'Максимальное количество сотрудников'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/directories/directories/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Directories'),
                        'url' => ['/directories/directories/' . $item['class']::classNameShort('id') . '_add'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Directories'),
                        'url' => ['/directories/directories/' . $item['class']::classNameShort('id') . '_edit'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * Возвращает массив, где имя - класс модели справочника, а значение - его название.
     * Нужно для вывода имени выбранного справочника в таблице в админке.
     *
     * @return array
     */
    public static function getDirectoriesName()
    {
        $directoriesNames = [];
        foreach (self::getList() as $list) {
            $key = $list['class']::classNameShort();
            $directoriesNames[$key] = $list['title'];
        }

        return $directoriesNames;
    }

    /**
     * Метод возвращает данные для заполнения формы.
     *
     * @param boolean $returnTitle Возвращать заголовок формы или набор полей
     * @param string  $namespace   Пространство имен в котором нужно выполнять поиск пользовательской конфигурации
     *
     * @return array|string
     * @throws InvalidCallException
     */
    public function getFormsFields($returnTitle = false, $namespace = 'forms')
    {
        $object = new \ReflectionClass($this->owner);

        $sortName = 'Directories';

        $formConfigClass = ($object->getNamespaceName() . '\\' . $namespace . '\\' . $sortName);

        if (class_exists($formConfigClass)) {

            /** @var FormConfigBehavior $obj */
            $obj = Yii::createObject($formConfigClass);

            if (!($obj instanceof FormConfigBehavior)) {
                throw new InvalidCallException('Form config class must extends sb\modules\crud\behaviors\FormConfigBehavior');
            }

            $obj->owner = $this->owner;
        } else {
            Yii::warning('Default form configuration is used. Please, extend sb\modules\crud\behaviors\FormConfigBehavior instead');

            $obj = $this;
        }

        if ($returnTitle) {
            return $obj->getTitle($this->owner);
        } else {
            $config['elements'] = $obj->getConfig();

            $config['buttons'] = $obj->getFormButtonsConfig();

            return $config;
        }
    }

    /**
     * Возвращает список действий.
     *
     * @return array
     */
    public static function getActions()
    {
        $actions = [];
        foreach (self::getList() as $item) {
            $actions[$item['class']::classNameShort('id') . '_list'] = [
                'class' => \common\modules\directories\controllers\actions\DirectoriesList::class,
                'directories' => $item['class'],
            ];

            $actions[$item['class']::classNameShort('id') . '_edit'] = [
                'class' => CreateAction::class,
                'params' => [
                    'model' => $item['class'],
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => 'control/' . $item['class']::classNameShort('id') . '_edit',
                ],
            ];

            $actions[$item['class']::classNameShort('id') . '_add'] = [
                'class' => CreateAction::class,
                'params' => [
                    'model' => $item['class'],
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => 'control/' . $item['class']::classNameShort('id') . '_edit',
                ],
            ];

            $actions[$item['class']::classNameShort('id') . '_delete'] = [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => $item['class'],
                ],
            ];
        }

        return $actions;
    }

    /**
     * @inheritdoc
     *
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        $result[] = ['label' => Yii::t('app', 'Справочники')];
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => self::getDirectoriesTitle()];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => self::getDirectoriesTitle(), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? 'Добавление новой записи справочника' : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * Вызвращаем заголовок справочника
     *
     * @return string
     */
    public static function getDirectoriesTitle()
    {
        $list = ArrayHelper::map(self::getList(), 'class', 'title');

        return !empty($list[self::class]) ? $list[self::class] : self::classNameShort();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['title', 'status'], 'required'],
            [['title'], 'trim'],
            [['title', 'title_short'], 'string', 'max' => 255],
            [['status', 'parent_id', 'unit', 'sort'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Категория справочника'),
            'title' => Yii::t('app', 'Название'),
            'title_short' => Yii::t('app', 'Краткое название'),
            'parent_id' => Yii::t('app', 'Родительская запись'),
            'unit' => Yii::t('app', 'Единица измерения'),
            'status' => Yii::t('app', 'Статус'),
            'sort' => Yii::t('app', 'Сортировка'),
        ];
    }

    /**
     * Свзяь с родительской записью
     *
     * @return $this
     */
    public function getParent()
    {
        return self::find()->where(['id' => $this->parent_id]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent = parent::behaviors();

        unset($parent['timestamp']);

        return $parent;
    }

    /**
     * Возвращает активные записи
     *
     * @return object
     */
    public static function getActive()
    {
        return self::find()->andWhere(['status' => self::STATUS_ACTIVE])->orderBy(['title' => SORT_ASC]);
    }
}
