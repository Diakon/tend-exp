<?php
namespace common\modules\directories\models\common;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Типы объекта.
 * Переименован в меню в соотсветствии с заданием https://basecamp.com/1795438/projects/15055171/todos/359090287
 */
class Projects extends Directories
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['parent_id',], 'required'],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'parent_id' => Yii::t('app', 'Вид деятельности'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = new TypeQuery(get_called_class());
        $query->alias(self::classNameShort('id'));
        $query->andWhere([self::classNameShort('id') . '.type' => self::classNameShort('id')]);

        return $query;
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Типы объекта')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Типы объекта'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать новый тип объекта') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

}
