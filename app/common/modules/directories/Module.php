<?php
namespace common\modules\directories;
use common\modules\directories\controllers\DirectoriesController;


/**
 * Class Module.
 * @package common\modules\directories
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\directories\controllers';

    public $controllerMap = [
        'directories' => DirectoriesController::class
    ];

}
