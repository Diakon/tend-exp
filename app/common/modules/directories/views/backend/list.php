<?php
/**
 * @var yii\web\View                 $this
 * @var \yii\data\ActiveDataProvider $provider
 */
?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= \common\components\GridView::widget([
                'dataProvider' => $provider,
                'layoutControl' => '{edit}',
                'options' => ['class' => 'grid-view property-list disable-modal'],
                'tableOptions' => ['class' => 'table table-bordered table-hover'],
                'columns' =>  [
                    'id',
                    [
                        'attribute' => 'title',
                        'format' => 'html',
                        'value' => function ($data) {
                            $html = $data->title;
                            return $html;
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'status',
                        'options' => ['class' => 'column-status'],
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'parent_id',
                        'format' => 'html',
                        'value' => function ($data) {
                            return !empty($data->parent_id) ? $data->parent->title : '';
                        }
                    ],
                    [
                        'attribute' => 'sort',
                        'format' => 'html',
                        'value' => function ($data) {
                            $html = $data->sort;
                            return $html;
                        },
                        'filter' => false,
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
