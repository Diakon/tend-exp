<?php
namespace common\modules\directories\controllers;

use backend\controllers\BackendController;
use modules\crud\actions\CreateAction;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\modules\directories\models\common\Directories;

/**
 * Основной контроллер.
 */
class DirectoriesController extends BackendController
{
    /**
     * @var string|boolean the name of the layout to be applied to this controller's views.
     */
    public $layout = '@root/themes/backend/views/layouts/main';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'activities_list',
                            'activities_edit',
                            'projects_list',
                            'projects_edit',
                            'main-work_list',
                            'main-work_edit',
                            'types-work_list',
                            'types-work_edit',
                            'works_list',
                            'works_edit',
                            'functions-company_list',
                            'functions-company_edit',
                            'currency_list',
                            'currency_edit',
                        ],
                        'roles' => ['administrator'],
                    ],

                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return Directories::getActions();
    }

    /**
     * Обработка системных ошибок.
     *
     * @return string
     */
    public function actionError()
    {
        $this->layout = '@cms/themes/backend/views/layouts/base';
        /** @var \yii\base\Exception $exception */
        $exception = Yii::$app->errorHandler->exception;
        $handler = Yii::$app->errorHandler;

        $data = [];
        $data['name'] = $exception->getName();
        $data['message'] = $exception->getMessage();
        $data['line'] = $exception->getLine();
        $data['file'] = $exception->getFile();
        $data['trace'] = $exception->getTraceAsString();
        $data['code'] = $exception->getCode();

        $code = $exception instanceof \yii\web\HttpException ? $exception->statusCode : $exception->getCode();
        $message = $exception->getMessage();


        if (Yii::$app->request->isAjax) {
            return print_r($data, true);
        } else {
            $this->view->title = $code . ' ' . $exception->getMessage();
            $params = compact('exception', 'code', 'message','handler');
            Yii::error(Json::encode($data), 'error');

            return $this->render('@cms/themes/backend/views/backend/error', $params);
        }
    }
}
