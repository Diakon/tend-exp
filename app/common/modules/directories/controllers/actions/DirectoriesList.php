<?php
namespace common\modules\directories\controllers\actions;

use modules\crud\actions\ListAction;
use common\modules\directories\models\common\Directories;
use Yii;

/**
 * Class DirectoriesList.
 * @package common\modules\directories\controllers\actions
 */
class DirectoriesList extends ListAction
{
    /**
     * @var string Класс свойства.
     */
    public $directories;

    /**
     * Запуск действия.
     *
     * @return string
     */
    public function run()
    {
        /** @var Directories $model */
        $model = new $this->directories;

        $this->applyFilter($model);

        $view = $this->controller->getView();

        // Устанавливаем добавочный класс для wrapper container
        $view->params['wrapperContentAdditionalClass'] = 'animated fadeInRight';
        // Устанавливаем добавочный класс для main container
        $view->params['mainContainerClass'] = ' ';

        // Устанавливаем breadcrumbs
        $view->params['breadcrumbs'] = $model->getAdminBreadcrumbsData($model::BREADCRUMBS_TYPE_LIST);

        $view->params['breadcrumbs_buttons'] = [
            ['url' => $model->addLink, 'title' => '<i class="glyphicon glyphicon-plus-sign"></i> ' . Yii::t('cms/crud', 'Add new'), 'options' => ['class' => 'btn btn-w-m btn-success']],
        ];

        return $this->controller->render('/backend/list', [
            'model' => $model,
            'provider' => $model->search(),
        ]);
    }
}
