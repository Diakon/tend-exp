<?php
/**
 * @var yii\web\View                 $this
 * @var \yii\data\ActiveDataProvider $provider
 */

use common\modules\tariff\models\backend\TariffPlans;
?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= \common\components\GridView::widget([
                'dataProvider' => $provider,
                'layoutControl' => '{edit}',
                'options' => ['class' => 'grid-view property-list disable-modal'],
                'tableOptions' => ['class' => 'table table-bordered table-hover'],
                'columns' =>  [
                    'id',
                    [
                        'attribute' => 'type',
                        'format' => 'html',
                        'value' => function ($data) {
                            return TariffPlans::$typesList[$data->type] ?? '';
                        }
                    ],
                    [
                        'attribute' => 'title',
                        'format' => 'html',
                        'value' => function ($data) {
                            $html = $data->title;
                            return $html;
                        }
                    ],
                    'count_month',
                    [
                        'attribute' => 'count_users_id',
                        'format' => 'html',
                        'value' => function ($data) {
                            return TariffPlans::$typeCountUsers[$data->count_users_id] ?? '';
                        }
                    ],
                    [
                        'attribute' => 'count_offers_id',
                        'format' => 'html',
                        'value' => function ($data) {
                            return TariffPlans::$typeCountOffers[$data->count_offers_id] ?? '';
                        }
                    ],
                    [
                        'attribute' => 'count_tenders_id',
                        'format' => 'html',
                        'value' => function ($data) {
                            return TariffPlans::$typeCountTenders[$data->count_tenders_id] ?? '';
                        }
                    ],
                    'price',
                    'sort',
                    'status:status'
                ],
            ]) ?>
        </div>
    </div>
</div>
