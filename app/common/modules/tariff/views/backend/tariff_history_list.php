<?php
/**
 * @var yii\web\View                 $this
 * @var \yii\data\ActiveDataProvider $provider
 */
?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= \common\components\GridView::widget([
                'dataProvider' => $provider,
                'layoutControl' => '{edit}',
                'options' => ['class' => 'grid-view property-list disable-modal'],
                'tableOptions' => ['class' => 'table table-bordered table-hover'],
                'columns' =>  [
                    'id',
                    'company.title',
                    'tariff.title',
                    [
                        'attribute' => 'date_start',
                        'format' => 'html',
                        'value' => function ($data) {
                            return !empty($data->date_start)
                                ? Yii::$app->formatter->asDate($data->date_start, 'php:d-m-Y')
                                : '';
                        }
                    ],
                    [
                        'attribute' => 'date_end',
                        'format' => 'html',
                        'value' => function ($data) {
                            return !empty($data->date_end)
                                ? Yii::$app->formatter->asDate($data->date_end, 'php:d-m-Y')
                                : '';
                        }
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
