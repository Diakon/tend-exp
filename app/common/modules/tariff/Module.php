<?php
namespace common\modules\tariff;
use common\modules\tariff\controllers\TariffController;


/**
 * Class Module.
 * @package common\modules\tariff
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\tariff\controllers';

    public $controllerMap = [
        'tariff' => TariffController::class
    ];

}
