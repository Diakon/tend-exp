<?php
namespace common\modules\tariff\controllers\actions;

use modules\crud\actions\ListAction;
use common\modules\tariff\models\backend\TariffPlans;
use Yii;

/**
 * Class TariffList
 *
 * @package common\modules\tariff\controllers\actions
 */
class TariffList extends ListAction
{
    /**
     * @var string Модель.
     */
    public $model;

    /**
     * Запуск действия.
     *
     * @return string
     */
    public function run()
    {
        /** @var TariffPlans $model */
        $model = new $this->model;

        $this->applyFilter($model);

        $view = $this->controller->getView();

        // Устанавливаем добавочный класс для wrapper container
        $view->params['wrapperContentAdditionalClass'] = 'animated fadeInRight';
        // Устанавливаем добавочный класс для main container
        $view->params['mainContainerClass'] = ' ';

        // Устанавливаем breadcrumbs
        $view->params['breadcrumbs'] = $model->getAdminBreadcrumbsData($model::BREADCRUMBS_TYPE_LIST);

        $view->params['breadcrumbs_buttons'] = [
            ['url' => $model->addLink, 'title' => '<i class="glyphicon glyphicon-plus-sign"></i> ' . Yii::t('cms/crud', 'Add new'), 'options' => ['class' => 'btn btn-w-m btn-success']],
        ];

        return $this->controller->render('list', [
            'model' => $model,
            'provider' => $model->search(),
        ]);
    }
}
