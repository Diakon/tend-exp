<?php
namespace common\modules\tariff\controllers;

use modules\crud\actions\ListAction;
use backend\controllers\BackendController;
use modules\crud\actions\CreateAction;
use common\modules\tariff\models\backend\TariffHistory;
use common\modules\tariff\models\backend\TariffPlans;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\Json;


/**
 * Основной контроллер.
 */
class TariffController extends BackendController
{
    /**
     * @var string|boolean the name of the layout to be applied to this controller's views.
     */
    public $layout = '@root/themes/backend/views/layouts/main';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'tariff_plans_create',
                            'tariff_plans_list',
                            'tariff_plans_update',
                            'tariff_history_create',
                            'tariff_history_list',
                            'tariff_history_update',
                        ],
                        'roles' => ['administrator'],
                    ],

                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'tariff_plans_list' => [
                'class' => ListAction::class,
                'template' => '/backend/tariff_plans_list',
                'model' => TariffPlans::class,
            ],
            'tariff_plans_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => TariffPlans::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . TariffPlans::classNameShort('id') . '_edit',
                ],
            ],
            'tariff_plans_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => TariffPlans::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . TariffPlans::classNameShort('id') . '_create',
                ],
            ],
            'tariff_history_list' => [
                'class' => ListAction::class,
                'template' => '/backend/tariff_history_list',
                'model' => TariffHistory::class,
            ],
            'tariff_history_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => TariffHistory::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . TariffHistory::classNameShort('id') . '_edit',
                ],
            ],
            'tariff_history_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => TariffHistory::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . TariffHistory::classNameShort('id') . '_create',
                ],
            ],
        ];
    }

    /**
     * Обработка системных ошибок.
     *
     * @return string
     */
    public function actionError()
    {
        $this->layout = '@cms/themes/backend/views/layouts/base';
        /** @var \yii\base\Exception $exception */
        $exception = Yii::$app->errorHandler->exception;
        $handler = Yii::$app->errorHandler;

        $data = [];
        $data['name'] = $exception->getName();
        $data['message'] = $exception->getMessage();
        $data['line'] = $exception->getLine();
        $data['file'] = $exception->getFile();
        $data['trace'] = $exception->getTraceAsString();
        $data['code'] = $exception->getCode();

        $code = $exception instanceof \yii\web\HttpException ? $exception->statusCode : $exception->getCode();
        $message = $exception->getMessage();


        if (Yii::$app->request->isAjax) {
            return print_r($data, true);
        } else {
            $this->view->title = $code . ' ' . $exception->getMessage();
            $params = compact('exception', 'code', 'message','handler');
            Yii::error(Json::encode($data), 'error');

            return $this->render('@cms/themes/backend/views/backend/error', $params);
        }
    }
}
