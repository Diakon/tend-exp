<?php
namespace common\modules\tariff\models\frontend;

use Yii;

/**
 * Тарифные планы.
 *
 */
class TariffPlans extends \common\modules\tariff\models\common\TariffPlans
{
    /**
     * Возвращает описание тарифа в соответствии с его типом
     *
     * @param integer $typeId
     * @return array
     */
    public static function getTypesDescription($typeId)
    {
        $description = [];
        $description[] = Yii::t('app', 'Поиск подрядчиков по основным критериям');
        $description[] = Yii::t('app', 'Возможность просмотра основной информации в профиле Компании');
        $description[] = Yii::t('app', 'Регистрация на сайте и создание своей Компании');

        switch ($typeId) {
            case self::CONST_TYPE_BASE:
                $description[] = Yii::t('app', 'Возможность расширенного поиска подрядчиков');
                $description[] = Yii::t('app', 'Доступ ко всей информации в профиле Компании ');
                $description[] = Yii::t('app', 'Возможность опубликовать профиль Компании');
                break;
            case self::CONST_TYPE_PROF:
                $description[] = Yii::t('app', 'Возможность расширенного поиска подрядчиков');
                $description[] = Yii::t('app', 'Доступ ко всей информации в профиле Компании ');
                $description[] = Yii::t('app', 'Возможность опубликовать профиль Компании');
                $description[] = Yii::t('app', 'Поиск и участие в тендерах');
                break;
            case self::CONST_TYPE_CORP:
                $description[] = Yii::t('app', 'Возможность расширенного поиска подрядчиков');
                $description[] = Yii::t('app', 'Доступ ко всей информации в профиле Компании ');
                $description[] = Yii::t('app', 'Возможность опубликовать профиль Компании');
                $description[] = Yii::t('app', 'Поиск и участие в тендерах');
                $description[] = Yii::t('app', 'Создание и проведение тендеров');
                break;
        }

        return $description;
    }


}