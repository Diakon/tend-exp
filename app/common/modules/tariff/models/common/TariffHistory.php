<?php
namespace common\modules\tariff\models\common;

use Yii;
use common\modules\company\models\common\Company;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;

/**
 * История смены тарифных планов компанией.
 *
 * @property integer $id             Идентификатор.
 * @property integer $company_id     Компания.
 * @property integer $tariff_plan_id Тарифный план.
 * @property integer $date_start     Дата начала тарифного плана.
 * @property integer $date_end       Дата окончания тарифного плана.
 * @property integer $created_at     Дата создания записи.
 * @property integer $updated_at     Дата изменения записи.
 */
class TariffHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tariff_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'tariff_plan_id', 'date_start'], 'required'],
            [['id', 'company_id', 'tariff_plan_id', 'date_start', 'date_end', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'tariff_plan_id' => Yii::t('app', 'Тарифный план'),
            'date_start' => Yii::t('app', 'Дата начала тарифного плана'),
            'date_end' => Yii::t('app', 'Дата окончания тарифного плана'),
        ];
    }

    /**
     * Связь с компаниями
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с тарфиным планом
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TariffPlans::class, ['id' => 'tariff_plan_id']);
    }

    /**
     * Созадант запись в историю о смене тарифного плана в компании
     *
     * @param Company $company
     */
    public static function addToHistory(Company $company)
    {
        $model = new self();
        $model->company_id = $company->id;
        $model->tariff_plan_id = $company->tariff_plan_id;
        $model->date_start = $company->tariff_plan_start;
        $model->date_end = $company->tariff_plan_end;
        $model->save(false);
    }
}
