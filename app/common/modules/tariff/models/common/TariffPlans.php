<?php
namespace common\modules\tariff\models\common;

use Yii;
use common\modules\company\models\common\Company;
use modules\crud\models\ActiveQuery;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;

/**
 * Тарифные планы.
 *
 * @property integer $id               Идентификатор.
 * @property string  $title            Наименование.
 * @property string $description       Описание.
 * @property integer $count_users_id   Количество пользователей.
 * @property integer $count_offers_id  Количество предложений.
 * @property integer $count_tenders_id Количество тендеров.
 * @property integer $count_month      Срок (в месяцах).
 * @property float $price              Цена.
 * @property integer $status           Статус.
 * @property integer $sort             Сортировка.
 * @property integer $type             Тип тарифа.
 * @property integer $created_at       Дата создания записи.
 * @property integer $updated_at       Дата изменения записи.
 */
class TariffPlans extends ActiveRecord
{
    const CONST_TYPE_MIN = 1;
    const CONST_TYPE_BASE = 2;
    const CONST_TYPE_PROF = 3;
    const CONST_TYPE_CORP = 4;
    public static $typesList = [
        self::CONST_TYPE_MIN => 'Минимальный',
        self::CONST_TYPE_BASE => 'Основной',
        self::CONST_TYPE_PROF => 'Профессиональный',
        self::CONST_TYPE_CORP => 'Корпоративный'
    ];

    /**
     * Количество пользователей
     */
    const TYPE_COUNT_USER_1 = 1;
    const TYPE_COUNT_USER_2 = 2;
    const TYPE_COUNT_USER_3 = 3;
    public static $typeCountUsers = [
        self::TYPE_COUNT_USER_1 => 1,
        self::TYPE_COUNT_USER_2 => 5,
        self::TYPE_COUNT_USER_3 => 10
    ];

    /**
     * Количество предложений
     */
    const TYPE_COUNT_NO_OFFER = 0;
    const TYPE_COUNT_OFFER_1 = 1;
    const TYPE_COUNT_OFFER_2 = 2;
    const TYPE_COUNT_OFFER_3 = 3;
    const TYPE_COUNT_OFFER_NO_LIMITS = 4;
    public static $typeCountOffers = [
        self::TYPE_COUNT_NO_OFFER => 0,
        self::TYPE_COUNT_OFFER_1 => 1,
        self::TYPE_COUNT_OFFER_2 => 5,
        self::TYPE_COUNT_OFFER_3 => 10,
        self::TYPE_COUNT_OFFER_NO_LIMITS => 'Безлимитное количество',
    ];

    /**
     * Количество тендеров
     */
    const TYPE_COUNT_NO_TENDER = 0;
    const TYPE_COUNT_TENDER_1 = 1;
    const TYPE_COUNT_TENDER_2 = 2;
    const TYPE_COUNT_TENDER_3 = 3;
    const TYPE_COUNT_TENDER_NO_LIMITS = 4;
    public static $typeCountTenders = [
        self::TYPE_COUNT_NO_TENDER => 0,
        self::TYPE_COUNT_TENDER_1 => 1,
        self::TYPE_COUNT_TENDER_2 => 5,
        self::TYPE_COUNT_TENDER_3 => 10,
        self::TYPE_COUNT_TENDER_NO_LIMITS => 'Безлимитное количество',
    ];

    /**
     * Количество месяцев
     */
    const COUNT_MONTH_1 = 1;
    const COUNT_MONTH_3 = 3;
    const COUNT_MONTH_6 = 6;
    const COUNT_MONTH_12 = 12;
    public static $countMonthList = [
        self::COUNT_MONTH_1 => 1,
        self::COUNT_MONTH_3 => 3,
        self::COUNT_MONTH_6 => 6,
        self::COUNT_MONTH_12 => 12,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tariff_plans}}';
    }


    /**
     * Активные тарифы
     *
     * @return ActiveQuery
     */
    public static function findActive()
    {
        return static::find()->andWhere('status =:status', [':status' => ActiveRecord::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);
        $this->sort = !empty($this->sort) ? $this->sort : 1;

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['title'], 'unique'],
            [['title', 'status', 'type', 'count_offers_id', 'count_users_id', 'count_tenders_id'], 'required'],
            [['title'], 'trim'],
            [['title'], 'string', 'max' => 255],
            [['price'], 'number'],
            [['description'], 'safe'],
            [['id', 'type', 'count_month', 'status', 'count_offers_id', 'count_users_id', 'sort', 'count_tenders_id', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название тарифа'),
            'description' => Yii::t('app', 'Краткое описание тарифа'),
            'count_users_id' => Yii::t('app', 'Количество пользователей'),
            'count_offers_id' => Yii::t('app', 'Количество предложений'),
            'count_tenders_id' => Yii::t('app', 'Количество тендеров'),
            'price' => Yii::t('app', 'Цена'),
            'status' => Yii::t('app', 'Статус'),
            'sort' => Yii::t('app', 'Сортировка'),
            'type' => Yii::t('app', 'Тип тарифа'),
            'count_month' => Yii::t('app', 'Срок (в месяцах)'),
        ];
    }

    /**
     * Устанавливает данные для нового тарифного плана
     * и пишет данные а историю смены тарифного плана в компании
     *
     * @param Company $company
     * @return Company
     */
    public static function setNewTariffPlan(Company $company)
    {
        $tariff = self::find()->where(['id' => (int)$company->tariff_plan_id])->one();
        $company->tariff_plan_start = time();
        if (!empty($tariff)) {
            $company->tariff_plan_id = $tariff->id;
            if (!empty($tariff->count_month)) {
                $company->tariff_plan_end = strtotime(date('Y-m-d H:i:s', time()) . ' +' . $tariff->count_month . ' month');
            }
        }

        return $company;
    }

    /**
     * Продевает текущий тарифный план
     *
     * @param Company $company
     * @return Company
     */
    public static function extendCurrentTariffPlan(Company $company)
    {
        if (!empty($company->tariff_plan_id)) {
            $tariff = self::find()->where(['id' => (int)$company->tariff_plan_id])->andWhere(['status' => self::STATUS_ACTIVE])->one();
            if (!empty($tariff->count_month)) {
                $dateEnd = $company->tariff_plan_end < time() ? time() : $company->tariff_plan_end;
                $company->tariff_plan_end = strtotime(date('Y-m-d H:i:s', $dateEnd) . ' +' . $tariff->count_month . ' month');
            }
        }

        return $company;
    }
}
