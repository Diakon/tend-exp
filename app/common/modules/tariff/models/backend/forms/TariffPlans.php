<?php
namespace common\modules\tariff\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * Конфигурация формы списка тарифного плана
 *
 * Class TariffPlans
 *
 * @package common\modules\tariff\models\backend\forms
 */
class TariffPlans extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Тарифный план ' . $model->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['title'] = ['type' => self::TYPE_TEXT];
        $data['description'] = ['type' => self::TYPE_TEXT_AREA];
        $data['type'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tariff\models\backend\TariffPlans::$typesList,
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['count_month'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tariff\models\backend\TariffPlans::$countMonthList,
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['count_users_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tariff\models\backend\TariffPlans::$typeCountUsers,
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['count_offers_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tariff\models\backend\TariffPlans::$typeCountOffers,
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['count_tenders_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tariff\models\backend\TariffPlans::$typeCountTenders,
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['price'] = ['type' => self::TYPE_TEXT];
        $data['sort'] = ['type' => self::TYPE_TEXT];
        $data['status'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';


        return $data;
    }

    private function getCountMonth()
    {
        $month = [];
        for ($i = 1; $i <= 12; ++$i) {
            $month[$i] = $i;
        }

        return $month;
    }

}
