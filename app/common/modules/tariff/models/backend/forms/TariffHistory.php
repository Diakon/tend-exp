<?php
namespace common\modules\tariff\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use yii\helpers\ArrayHelper;


/**
 * Конфигурация формы изменений тарифного плана в компании
 *
 * Class TariffHistory
 *
 * @package common\modules\tariff\models\backend\forms
 */
class TariffHistory extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Смена тарифного план в компании' . $model->isNewRecord ? '' : $model->company->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['company_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\company\models\common\Company::getCompanyList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['tariff_plan_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(\common\modules\tariff\models\backend\TariffPlans::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';


        return $data;
    }
}
