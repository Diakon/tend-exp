<?php

namespace common\modules\settings\models;

use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Class Setting
 * @package frontend\modules\settings\models
 */
class Setting extends BaseSetting
{
    /**
     * @param bool $forDropDown if false - return array or validators, true - key=>value for dropDown
     * @return array
     */
    public function getTypes($forDropDown = true)
    {
        $values = [
            'string' => ['value', 'string'],
            'integer' => ['value', 'integer'],
            'boolean' => ['value', 'boolean', 'trueValue' => "1", 'falseValue' => "0", 'strict' => true],
            'float' => ['value', 'number'],
            'email' => ['value', 'email'],
            'ip' => ['value', 'ip'],
            'url' => ['value', 'url'],
            'object' => [
                'value',
                function ($attribute, $params) {
                    $object = null;
                    try {
                        Json::decode($this->$attribute);
                    } catch (InvalidParamException $e) {
                        $this->addError($attribute, Yii::t('settings', '"{attribute}" must be a valid JSON object', [
                            'attribute' => $attribute,
                        ]));
                    }
                }
            ],
        ];

        if (!$forDropDown) {
            return $values;
        }

        $return = [];
        foreach ($values as $key => $value) {
            $return[$key] = Yii::t('settings', $key);
        }

        return $return;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section', 'key'], 'required'],
            [['value'], 'string'],
            [['section', 'key'], 'match', 'pattern' => '/^[a-zA-Z_-]+$/', 'message' => Yii::t('settings', '{attribute} "{value}" should only contain {pattern}', ['pattern' => '[a-zA-Z_]'])],
            [['section', 'key','title'], 'string', 'max' => 255],
            [
                ['key'],
                'unique',
                'targetAttribute' => ['section', 'key'],
                'message' =>
                    Yii::t('settings', '{attribute} "{value}" already exists for this section.')
            ],
            ['type', 'in', 'range' => array_keys($this->getTypes(false))],
            [['type', 'createdAt', 'updatedAt'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    public function beforeSave($insert)
    {
        $validators = $this->getTypes(false);
        if (!array_key_exists($this->type, $validators)) {
            $this->addError('type', Yii::t('settings', 'Please select correct type'));
            return false;
        }

        $model = DynamicModel::validateData([
            'value' => $this->value
        ], [
            $validators[$this->type],
        ]);

        if ($model->hasErrors()) {
            $this->addError('value', $model->getFirstError('value'));
            return false;
        }

        if ($this->hasErrors()) {
            return false;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('settings', 'ID'),
            'type' => Yii::t('settings', 'Type'),
            'section' => Yii::t('settings', 'Section'),
            'title' => Yii::t('settings', 'Title'),
            'key' => Yii::t('settings', 'Key'),
            'value' => Yii::t('settings', 'Value'),
            'status' => Yii::t('settings', 'Status'),
            'createdAt' => Yii::t('settings', 'Created'),
            'updatedAt' => Yii::t('settings', 'Modified'),
        ];
    }
}
