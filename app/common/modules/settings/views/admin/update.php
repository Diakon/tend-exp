<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\settings\models\Setting $model
 */

$this->title = Yii::t(
        'settings',
        'Update {modelClass}: ',
        [
            'modelClass' => Yii::t('settings', 'Setting'),
        ]
    ) . ' ' . $model->section. '.' . $model->key;

$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('settings', 'Update');

?>
<div class="setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render(
        '_form',
        [
            'model' => $model,
        ]
    ) ?>

</div>
