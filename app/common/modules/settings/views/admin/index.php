<?php

use common\modules\settings\models\Setting;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\settings\models\SettingSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('settings', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]);?>

    <p>
        <?php
        if(\helpers\Dev::check()) {
           echo  Html::a(Yii::t('settings','Create settings'),['create'],['class' => 'btn btn-success']);
        }
       ?>
    </p>
    <?php Pjax::begin(); ?>
    <?=
    GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'title',
//                [
//                    'attribute' => 'section',
//                    'filter' => ArrayHelper::map(
//                        Setting::find()->select('section')->distinct()->where(['<>', 'section', ''])->all(),
//                        'section',
//                        'section'
//                    ),
//                ],
//                'key',
                'value:ntext',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => [],
                    'template' => '<div class="pull-right btn-group btn-group-sm">{view}{update}{permissions}{delete}</div>',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            if (Yii::$app->user->can('userView', ['user' => $model])) {
                                $options = [
                                    'title' => Yii::t('yii', 'View'),
                                    'aria-label' => Yii::t('yii', 'View'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-default',
                                ];

                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                            }
                        },
                        'update' => function ($url, $model, $key) {
                            if (!Yii::$app->user->can('userUpdate', ['user' => $model])) {
                                return '';
                            }
                            $options = [
                                'title' => Yii::t('yii', 'Update'),
                                'aria-label' => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-default',
                            ];

                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                        },

                        'delete' => function ($url, $model, $key) {
                            if (!\helpers\Dev::check()) {
                                return '';
                            }
                            $options = [
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'class' => 'btn btn-default btn-confirm-open',
                                'data' => [
                                    'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                ],
                            ];

                            return Html::a('<span class="glyphicon glyphicon-remove text-danger"></span>', $url, $options);
                        },
                    ],
                ],
            ],
        ]
    ); ?>
    <?php Pjax::end(); ?>
</div>
