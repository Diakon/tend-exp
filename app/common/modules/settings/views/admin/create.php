<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\settings\models\Setting $model
 */

$this->title = Yii::t('settings','Create settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render(
        '_form',
        [
            'model' => $model,
        ]
    ) ?>

</div>
