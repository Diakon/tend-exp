<?php
namespace common\modules\settings;

use Yii;
use const DIRECTORY_SEPARATOR;

/**
 * Модуль настроек
 * @package frontend\modules\settings
 */
class Module extends \yii\base\Module
{

    /**
     * @var null|array Роли которым есть доступ к контроллеру AdminController, eg. ['admin']. Если устаовлено `null`, то accessFilter не будет применен
     */
    public $accessRoles = null;

    /**
     *
     * @var string Установка языка
     */
    public $sourceLanguage = 'ru';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }
    /**
     * Регистрация языковых файлов
     */
    protected function registerTranslations()
    {
        Yii::$app->i18n->translations['settings'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => $this->sourceLanguage,
            'basePath' => __DIR__.DIRECTORY_SEPARATOR.'messages',
            'fileMap' => [
                'settings' => 'settings.php',
            ],
        ];
    }
}
