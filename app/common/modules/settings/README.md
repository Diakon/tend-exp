
Установка
------------


Subsequently, run

```php
./yii migrate/up --migrationPath=@vendor/pheme/yii2-settings/migrations
```

in order to create the settings table in your database.


Использование
-----

Настройка модуля

```php
'modules' => [
    'settings' => [
        'class' => 'pheme\settings\Module',
    ],
    ...
],
```

Настройка компонента

```php
'components' => [
    'settings' => [
        'class' => 'pheme\settings\components\Settings'
    ],
    ...
]
```

Использование компонента

```php

$settings = Yii::$app->settings;

$value = $settings->get('section.key');

$value = $settings->get('key', 'section');

$settings->set('section.key', 'value');

$settings->set('section.key', 'value', null, 'string');

$settings->set('key', 'value', 'section', 'integer');

// Automatically called on set();
$settings->clearCache();
