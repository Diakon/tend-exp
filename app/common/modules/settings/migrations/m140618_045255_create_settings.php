<?php
namespace common\modules\settings\migrations;

use yii\db\Expression;

class m140618_045255_create_settings extends \yii\db\Migration
{
    const DB_TABLE = '{{%settings}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB COMMENT="Настройки"';
        }
        $this->createTable(
            self::DB_TABLE,
            [
                'id' => $this->primaryKey(),
                'status' => $this->smallInteger(2)->comment('Статус'),
                'title' => $this->string(255)->comment('Заголовок'),
                'type' => $this->string(255)->notNull(),
                'section' => $this->string(255)->notNull(),
                'key' => $this->string(255)->notNull(),
                'value' => $this->text(),
                'createdAt' => $this->timestamp()->defaultValue(0)->comment('Дата создания'),
                'updatedAt' => $this->timestamp()->defaultValue(0)->comment('Дата обновления'),
            ],
            $tableOptions
        );

        $this->createIndex('settings_unique_key_section', self::DB_TABLE, ['section', 'key'], true);

        $this->insert(self::DB_TABLE,[
            'status'=>1,
            'title'=>'Наименование сайта',
            'type'=>'string',
            'section'=>'site',
            'key'=>'name',
            'value'=>'Наименование сайта',
            'createdAt'=>new Expression('NOW()'),
            'updatedAt'=>new Expression('NOW()'),
        ]);
    }

    public function down()
    {
        $this->dropIndex('settings_unique_key_section', self::DB_TABLE);

        $this->dropTable(self::DB_TABLE);
    }
}
