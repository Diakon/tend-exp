<?php
namespace common\modules\dashboard;

/**
 * Class Module.
 * @package common\modules\dashboard
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\dashboard\controllers';

    public $controllerMap = [
        'dashboard' => 'common\modules\dashboard\controllers\DashboardController',
    ];

}
