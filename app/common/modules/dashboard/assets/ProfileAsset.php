<?php

namespace common\modules\dashboard\assets;

use yii\web\AssetBundle;

/**
 * Ресурсы приложения.
 */
class ProfileAsset extends AssetBundle
{
    /**
     * @var string the Web-accessible directory that contains the asset files in this bundle.
     */
    public $basePath = '@webroot/frontend/web/themes/frontend/html/dist/static';

    /**
     * @var string the base URL that will be prefixed to the asset files for them to be accessed via Web server.
     */
    public $baseUrl = '@tenderosAsset';

    /**
     * @var array list of CSS files that this bundle contains.
     */
    public $css = [
        'css/main.css',
    ];

    /**
     * @var array list of JavaScript files that this bundle contains.
     */
    public $js = [
        'js/script.js',
    ];

    /**
     * @var array list of JavaScript files that this bundle contains.
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];

    /**
     * @var array the options to be passed to [[AssetManager::publish()]] when the asset bundle is being published.
     */
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
