<?php
/**
 * @var \common\modules\tender\models\frontend\Objects $model
 * @var string                                         $companyUrl
 * @var \yii\data\ActiveDataProvider                   $dataProvider
 */
use  \common\modules\tender\models\frontend\Objects;
$myObjects = \yii\helpers\Url::current(['/dashboard/dashboard/objects','companyUrl' => Yii::$app->user->getActiveCompanyData()['url']]);
$objects = \yii\helpers\Url::current(['/dashboard/dashboard/objects_employee','companyUrl' => Yii::$app->user->getActiveCompanyData()['url']]);
$this->title = Yii::t('app', 'Объекты');

$sort = $dataProvider->sort;
$statusASC = Yii::t('app', 'Завершены');
$statusDESC = Yii::t('app', 'Строятся');

$currentStatus = $sort->getAttributeOrder('status_complete_id') == SORT_ASC ? $statusASC : $statusDESC;
$currentStatusPortfolio = $model::portfolioStatusList($portfolio);

?>

<div class="tabs-block tabs-block--lk">
    <div class="tabs-block__inner">
        <div class="tabs-block__head">
            <div class="tabs-block__list">
                <a href="<?= $myObjects ?>"
                   class="tabs-block__btn <?= $myObjects == Yii::$app->request->url ? 'active' : '' ?>"> <?= Yii::t('app', 'МОИ ОБЪЕКТЫ')?></a>
                <a href="<?=  $objects ?>"
                   class="tabs-block__btn <?= $objects == Yii::$app->request->url ? 'active' : '' ?>"><?= Yii::t('app', 'ОБЪЕКТЫ ЗАКАЗЧИКА')?></a>
            </div>
        </div>
    </div>
</div>

<div class="filter filter--lk">
    <div class="filter__inner">



        <div class="filter__head">
            <div class="filter__item">
                <div class="filter__label"><?= Yii::t('app', 'Сортировать по:') ?></div>
                <div class="filter__select">
                    <div class="drop-list js-sort-list">

                        <div class="drop-list__header js-sort-list-open">
                            <span class="drop-list__arrow"></span>
                            <span class="drop-list__text"><?= $currentStatus ?></span>
                        </div>

                        <div class="drop-list__box">
                            <ul class="drop-list__list">
                                <li>
                                    <?= \yii\helpers\Html::a($currentStatus == $statusASC ? $statusDESC : $statusASC, $sort->createUrl('status_complete_id', true)) ?>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter__item">
                <div class="filter__label"><?= Yii::t('app', 'Создан для:') ?></div>
                <div class="filter__select">
                    <div class="drop-list js-sort-list">

                        <div class="drop-list__header js-sort-list-open">
                            <span class="drop-list__arrow"></span>
                            <span class="drop-list__text"><?= $currentStatusPortfolio ?></span>
                        </div>

                        <div class="drop-list__box">
                            <ul class="drop-list__list">
                                <li>
                                    <?= \yii\helpers\Html::a($model::portfolioStatusList(Objects::ALL_OBJECTS),['/dashboard/dashboard/objects',
                                        'companyUrl' => Yii::$app->user->getActiveCompanyData()['url'],  'portfolio' => Objects::ALL_OBJECTS]) ?>
                                </li>
                                <li>
                                    <?= \yii\helpers\Html::a($model::portfolioStatusList(Objects::STATUS_ACTIVE),['/dashboard/dashboard/objects','companyUrl' => Yii::$app->user->getActiveCompanyData()['url'],
                                        'portfolio' => Objects::STATUS_ACTIVE]) ?>
                                </li>
                                <li>
                                    <?= \yii\helpers\Html::a($model::portfolioStatusList(Objects::STATUS_INACTIVE),['/dashboard/dashboard/objects','companyUrl' => Yii::$app->user->getActiveCompanyData()['url'],
                                    'portfolio' => Objects::STATUS_INACTIVE] ) ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="filter__body">
            <div class="filter__action">
                <?php if (Yii::$app->user->can('addProject', ['companyUrl' => $companyUrl])) { ?>
                    <a href="<?= $model->getCreateLink($companyUrl) ?>" class="btn btn-primary"><?= Yii::t('app', 'ДОБАВИТЬ НОВЫЙ ОБЪЕКТ')  ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<?=\modules\crud\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'tender-item',
    ],
    'viewParams' => [
            'companyUrl' => $companyUrl
    ],
    'itemView' => '_objects_list_item',
    'emptyText' => 'У вас еще нет объектов этой категории',
    'pager' => false,
]) ?>

<br>

<?= $this->render('_pagination', ['dataProvider' => $dataProvider]) ?>




