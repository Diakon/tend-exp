<?php echo \modules\crud\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '<div class="catalog"><div class="container"><div class="catalog__inner">{items}</div></div></div>
                            <div class="pagination"><div class="container"><div class="pagination__inner">{pager}</div></div></div>',
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'catalog__item',
    ],
    'itemView' => '_company_list_item',
    'emptyText' => '<div class="container"><div class="empty"><div class="filter-result__empty_filter"><div class="_title">По вашем запросу ничего не найдено</div></div></div></div>',
    'pager' => [
        'class' => \yii\widgets\LinkPager::class,
        'options' => [
            'class' => 'pagination__list',
        ],
        'linkOptions' => [
            'class' => '_link',
        ],
        'nextPageLabel' => '',
        'prevPageLabel' => '',
        'prevPageCssClass' => 'pagination__arrow pagination__arrow--prev _link',
        'nextPageCssClass' => 'pagination__arrow pagination__arrow _link',
    ],
]) ?>
