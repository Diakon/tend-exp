<?php

use yii\helpers\Html;

/**
 * @var \common\modules\tender\models\common\ObjectsCompletedWorks $completedWorks
 * @var \common\modules\tender\models\frontend\Objects $model
 */
?>
<?php foreach ($completedWorks as $key => $data) { ?>


    <div class="col-6">
        <div class="form-group   ">
            <label for="" class="control-label ">
                <?= Yii::t('app', 'Вид работы') ?></label>
            <?= Html::activeDropDownList($model, 'completedWorkWorkIds[' . $key . ']', \common\modules\directories\models\common\Works::findActiveList(), [
                'class' => $data->isNewRecord ? 'js-drop-down-param-class' : 'js-dropdown-box',
                'value' => $data->workId,
            ]) ?>
        </div>
    </div>
    <div class="col-3">
        <div class="form-group   ">
            <label for="" class="control-label ">
                <?= Yii::t('app', 'Количество') ?></label>
            <?= Html::activeInput('text', $model, 'completedWorkCount[' . $key . ']', [
                'class' => 'form-control',
                'value' => (int)$data->count,
            ]) ?>
        </div>
    </div>

        <div class="col-3">
            <div class="form-group">
                <label for="" class="control-label"><?= Yii::t('app', 'Ед. измерения') ?></label>

                <div class="row">
                    <div class="col-6">
                        <?= Html::activeDropDownList($model, 'completedWorkUnitIds[' . $key . ']', \common\modules\tender\models\common\ObjectsCompletedWorks::getUnits(), [
                            'class' => $data->isNewRecord ? 'js-drop-down-param-class' : 'js-dropdown-box',
                            'value' => $data->unitId,
                        ]) ?>
                    </div>
                </div>

            </div>
        </div>

<?php } ?>
