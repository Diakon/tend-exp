<?php

use yii\bootstrap\ActiveForm;

\frontend\widgets\FlashMessagesWidget::widget();
$this->title = Yii::t('app', 'Настройки профиля');
/**
 * @var $avatar \frontend\modules\upload\models\Upload
 */
$avatar = Yii::$app->user->identity->avatar;
use \common\components\Thumbnail;
?>

<div class="default">
    <div class="default__inner">
        <div class="page-title">
            <div class="container">
                <div class="page-title__inner">
                    <h1 class="page-title__title"><?= $this->title ?></h1>
                </div>
            </div>
        </div>

        <div class="set-profile">

            <div class="container">

                <?php $form = ActiveForm::begin([
                    'validateOnBlur' => false,
                    'options' =>  ['enctype'=>'multipart/form-data'],
                    'fieldConfig' => [
                        'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
                    ],
                ]); ?>

                <div class="set-profile__inner">
                    <div class="set-profile__body">
                        <div class="row">
                            <div class="col-lg-4">
                                <?= $form->field($model, 'lastname' )->textInput()->error(['tag' => 'div']) ?>
                            </div>
                            <div class="col-lg-4">
                                <?= $form->field($model, 'username' )->textInput()->error(['tag' => 'div']) ?>
                            </div>
                            <div class="col-lg-4">
                                <?= $form->field($model, 'middlename' )->textInput()->error(['tag' => 'div']) ?>
                            </div>
                            <div class="col-lg-4">
                                <?= $form->field($model, 'position' )->textInput()->error(['tag' => 'div']) ?>
                            </div>
                            <div class="col-lg-4">
                                <?= $form->field($model, 'phone' )->widget(\yii\widgets\MaskedInput::class, ['mask' => '+7 999 999-99-99',])
                                    ->error(['tag' => 'div']) ?>
                            </div>
                            <div class="col-lg-4">
                                <?= $form->field($model, 'email' )->textInput()->error(['tag' => 'div']) ?>
                            </div>

                            <div class="col-lg-2"></div>
                            <div class="col-lg-6">
                                <div class="edit-logo edit-logo--adaptive">
                                    <div class="edit-logo__inner">
                                        <div class="edit-logo__head">
                                            <div class="edit-logo__title">Фотография профиля</div>
                                        </div>
                                        <div class="edit-logo__main">
                                            <div class="edit-logo__img">
                                                <div class="edit-logo__img-inner">

                                                    <?php if ($avatar) { ?>
                                                        <img style="border-radius: 70px" class="_img js-avatar" src="<?=Thumbnail::thumbnailFileUrl($avatar->getFile(true, Yii::getAlias('@static') . '/../content/images/helment.svg')); ?>">
                                                    <?php } else { ?>
                                                        <img  src="<?= Yii::getAlias('@static') ?>/../content/images/helment.svg" alt="" class="_img ">
                                                    <?php } ?>
                                                </div>
                                                <span class="_btn btn-remove js-btn-remove-avatar" data-url="<?= \yii\helpers\Url::to(['/dashboard/dashboard/delete_avatar']) ?>">
                                                    <svg class="icon icon-remove ">
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#remove"/>
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="edit-logo__body">
                                                <div class="edit-logo__action">

                                                    <input type="file" name="<?= \yii\helpers\Html::getInputName(Yii::$app->user->identity, 'avatar') ?>" id="edit-logo" class="_input js-file-name">
                                                    <label for="edit-logo" class="_label btn btn-primary">ЗАГРУЗИТЬ ЛОГОТИП</label>
                                                    <br>
                                                    <span class="js-file-name-text file-name-text"></span>
                                                    <span class="js-file-delete-text file-name-text" style="display: none">
                                                        <div>
                                                            <?= Yii::t('app', 'Логотип будет удалён после сохранения изменений') ?>
                                                        </div>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="set-profile__foot">
                        <div class="row">
                            <div class="col-lg-4">
                                <?= $form->field($model, 'password' )->passwordInput()->error(['tag' => 'div']) ?>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="" class="control-label">Повторите пароль</label>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $form->field($model, 'password_repeat')->passwordInput()->label(false)->error(['tag' => 'div']) ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="set-profile__note">
                                                <?= Yii::t('app', 'Оставьте поля пустыми, если не хотите менять пароль') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="set-profile__action">
                            <button class="btn btn-primary" type="submit"><?= Yii::t('app', 'СОХРАНИТЬ') ?></button>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
