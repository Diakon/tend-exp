<?php
use common\modules\company\models\frontend\CompanyUsers;
/**
 * @var $company  \common\modules\company\models\frontend\Company
 * @var $teamUsers \common\modules\company\models\frontend\CompanyUsers[]
 * @var boolean $canEditTeam
 */
$this->title = Yii::t('app', 'Настройки команды');

$canEdit = Yii::$app->user->identity->companyUser ? Yii::$app->user->identity->companyUser->canEditMyCompany() : false;
\frontend\widgets\FlashMessagesWidget::widget();
?>

<?php if(!Yii::$app->user->canShowAdditionalFilter()) { ?>
    <div class="improve-block showed">
        <div class="improve-block__inner">
            <button class="improve-block__close js-close-improve" type="button"></button>
            <div class="improve-block__head">
                <div class="improve-block__title"><?= Yii::t('app', 'Улучшите аккаунт для публикации изменений')?></div>
            </div>
            <div class="improve-block__body">
                <div class="improve-block__desc">
                    Ваш текущий уровень аккаунта не позволяет публиковать дополнительные изменения в профиле организации.
                    Улучшите аккаунт и наслаждайтесь всеми возможностями сервиса TENDEROS, как сделали уже 2105
                    пользователей.
                </div>
                <div class="improve-block__action">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/tariff_edit', 'url' => $company->url]) ?>" class="btn btn-primary btn-small"><?= Yii::t('app', 'УЛУЧШИТЬ АККАУНТ')?></a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="tabs-block tabs-block--link">
    <div class="tabs-block__inner">
        <div class="tabs-block__head">

            <?= $this->render('_tab_menu_company', ['company' => $company]) ?>

            <?php if ($canEdit) { ?>
                <div class="tabs-block__action">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/company_edit', 'url' => $company->url]) ?>" class="btn-edit">
                        <svg class="icon icon-edit ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= \Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#edit"/>
                        </svg>
                        <span><?= Yii::t('app', 'РЕДАКТИРОВАТЬ ОРГАНИЗАЦИЮ') ?></span>
                    </a>
                </div>
            <?php } ?>
        </div>
        <?php $form = \yii\bootstrap\ActiveForm::begin([
                'validateOnBlur' => false,
                //  'enableClientValidation' => false,
                'action' => \yii\helpers\Url::current(),
                'options' => [
                    'method' => 'POST',
                ],
                'fieldConfig' => [
                    'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
                ],
            ]); ?>
        <div class="tabs-block__body">
            <?php foreach ($teamUsers as $teamUser) { ?>
                <?php if ($teamUser->userId != Yii::$app->user->id) { ?>
                    <div class="team-item">
                        <div class="team-item__inner">
                            <div class="team-item__main">
                                <div class="team-item__title"><?= $teamUser->user->getFullName() ?></div>
                                <div class="team-item__mail">
                                    <?php if ($teamUser->verification == \common\modules\company\models\common\CompanyUsers::STATUS_INACTIVE) { ?>
                                        <span class="_icon _icon--orange"></span>
                                    <?php } else { ?>
                                        <span class="_icon "></span>
                                    <?php } ?>
                                    <a href="mailto:<?= $teamUser->user->email ?>" class="_link"><?= $teamUser->user->email ?></a>
                                </div>
                            </div>
                            <?php if ($teamUser->verification == \common\modules\company\models\common\CompanyUsers::STATUS_ACTIVE && $canEditTeam) { ?>
                                <div class="team-item__field">


                                    <div class="form-group form-group--autocomplete js-multi-select" data-placeholder="<?= Yii::t('app', 'Укажите объекты, к которым должен получить доступ пользователь') ?>">
                                        <select id="companyusers-objectsids" class=" "
                                                name=" CompanyUsers[objectsIds][<?= $teamUser->userId ?>][]"
                                                multiple="multiple" size="4"
                                                data-result-hidden-input-name="result-input"
                                                data-empty-text="<?= Yii::t('app', 'Ничего не найдено') ?>"
                                                placeholder="Выбрать">


                                            <?php

                                            $options = \yii\helpers\ArrayHelper::map($company->objects, 'id', 'title');

                                            $checked = $objectsIds[$teamUser->userId] ?? $teamUser->getCompanyUserAccess()->select('class_id')
                                                    ->where(['class_name' => \common\components\ClassMap::getId(\common\modules\tender\models\frontend\Objects::class)])
                                                    ->asArray()->column();

                                            foreach ($options as $id => $title) { ?>
                                                <option value="<?= $id ?>" <?= in_array($id, $checked) ? 'selected' : '' ?>><?= $title ?></option>
                                            <?php } ?>


                                        </select>
                                    </div>


                                    <!--                            <div class="form-group form-group--autocomplete">-->
                                    <!--                                <label for="" class="control-label ">Autocomplete multiselect</label>-->
                                    <!---->
                                    <!--                                <select class="_select js-autocomplete-select-multi "-->
                                    <!--                                        name="select"-->
                                    <!--                                        data-result-hidden-input-name="result-input"-->
                                    <!--                                        data-empty-text="Ничего не найдено"-->
                                    <!--                                        multiple-->
                                    <!--                                        placeholder="Choose any one">-->
                                    <!---->
                                    <!--                                    <option value="0" selected>Example text 1</option>-->
                                    <!--                                    <option value="1">Example text 2</option>-->
                                    <!--                                    <option value="2">Example text 3</option>-->
                                    <!--                                    <option value="3" selected>Example text 4</option>-->
                                    <!--                                    <option value="4">Example text 5</option>-->
                                    <!--                                </select>-->
                                    <!--                            </div>-->


                                </div>
                            <?php } ?>
                            <?php if ($teamUser->verification == \common\modules\company\models\common\CompanyUsers::STATUS_INACTIVE && $canEditTeam) { ?>
                                <div class="team-item__field">
                                    <a class="btn btn-outline-secondary btn-small"
                                       href="<?= \yii\helpers\Url::current(['user_id' => $teamUser->id]) ?>"><?= Yii::t('app', 'ДОБАВИТЬ В КОМАНДУ') ?></a>
                                </div>
                            <?php } ?>
                            <?php if ($teamUser->verification == \common\modules\company\models\common\CompanyUsers::STATUS_ACTIVE && $canEditTeam) { ?>
                                <div class="team-item__action">
                                    <a href="<?= \yii\helpers\Url::current(['delete_user_id' => $teamUser->userId]) ?>" class="btn-remove">
                                        <svg class="icon icon-remove ">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#remove"/>
                                        </svg>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>


<!--            <div class="team-item">-->
<!--                <div class="team-item__inner">-->
<!--                    <div class="team-item__main">-->
<!--                        <div class="team-item__title">Константинопольский Константин Константинович</div>-->
<!--                        <div class="team-item__mail">-->
<!--                            <span class="_icon "></span>-->
<!--                            <a href="mailto:konstantin@mail.ru" class="_link">konstantin@mail.ru</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="team-item__field">-->
<!---->
<!--                        <div class="team-item__wait">-->
<!--                            ОЖИДАЕТ ПОДТВЕРЖДЕНИЯ-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="team-item__action">-->
<!--                        <button class="btn-remove">-->
<!--                            <svg class="icon icon-remove ">-->
<!--                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../static/images/svg/spriteInline.svg#remove"/>-->
<!--                            </svg>-->
<!--                        </button>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->

            <?php if ($canEditTeam) { ?>
                <div class="section-lk__action">
                    <button type="submit" class="btn btn-primary"><?= Yii::t('app', 'СОХРАНИТЬ ИЗМЕНЕНИЯ')?></button>
                    <button data-popup="add-user" class="btn btn-outline-secondary"><?= Yii::t('app', 'ПРИГЛАСИТЬ В КОМАНДУ')?></button>
                </div>
            <?php } ?>
        </div>

        <?php \yii\bootstrap\ActiveForm::end(); ?>
    </div>
</div>
<div class="popup popup--user" data-popup-id="add-user" id="add-user" style="display: none;">
    <div class="popup__box">
        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'validateOnBlur' => false,
            'action' => \yii\helpers\Url::current(),
            'options' => [
                'method' => 'POST',
            ],
            'fieldConfig' => [
                'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
            ],
        ]); ?>
            <div class="popup__box-inner">
                <div class="popup__title"><?= Yii::t('app', 'Добавить участников к команде') ?></div>
                <div class="popup__body">
                    <div class="popup-user">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group js-add-user-popup-block">
                                    <?= \yii\helpers\Html::hiddenInput('addUsersInTeamCount', 0, ['class' => 'js-add-users-in-team-hidden-input']) ?>
                                    <div class="js-add-users-in-team-input-block" style="display: none;">
                                        <?= \yii\helpers\Html::input('text', 'addUsersInTeam[emails][count-input]', '', ['class' => 'form-control', 'style' => 'margin-top:10px;']) ?>
                                    </div>
                                    <label for="" class="control-label "><?= Yii::t('app', 'Адрес электронной почты') ?></label>
                                    <?= \yii\helpers\Html::input('text', 'addUsersInTeam[emails][0]', '', ['class' => 'form-control']) ?>
                                </div>
                                <button class="popup-user__btn js-add-new-user-in-team"><?= Yii::t('app', 'ДОБАВИТЬ УЧАСТНИКА') ?></button>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="" class="control-label "><?= Yii::t('app', 'Сообщение') ?></label>
                                    <textarea class="form-control"
                                              name="addUsersInTeam[text]"
                                              placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="popup__footer">
                    <div class="popup__action">
                        <button class="btn btn-primary" type="submit"><?= Yii::t('app', 'ОТПРАВИТЬ') ?></button>
                        <button class="btn btn-outline-secondary js-close-wnd" type="reset"><?= Yii::t('app', 'ОТМЕНИТЬ') ?></button>
                    </div>
                </div>
            </div>
        <?php \yii\bootstrap\ActiveForm::end(); ?>
    </div>
</div>
