<?php
use yii\bootstrap\ActiveForm;
use common\modules\tariff\models\frontend\TariffPlans;
/**
 * @var $company  \common\modules\company\models\frontend\Company
 * @var $tariffPlans array
 * @var $minTariffPlan TariffPlans
 * @var array $extendPaymentData
 */
$this->title = Yii::t('app', 'Настройки команды');

$canEdit = Yii::$app->user->identity->companyUser ? Yii::$app->user->identity->companyUser->canEditMyCompany() : false;
\frontend\widgets\FlashMessagesWidget::widget();

?>

<?php if(!Yii::$app->user->canShowAdditionalFilter()) { ?>
    <div class="improve-block showed">
        <div class="improve-block__inner">
            <button class="improve-block__close js-close-improve" type="button"></button>
            <div class="improve-block__head">
                <div class="improve-block__title"><?= Yii::t('app', 'Улучшите аккаунт для публикации изменений')?></div>
            </div>
            <div class="improve-block__body">
                <div class="improve-block__desc">
                    Ваш текущий уровень аккаунта не позволяет публиковать дополнительные изменения в профиле организации.
                    Улучшите аккаунт и наслаждайтесь всеми возможностями сервиса TENDEROS, как сделали уже 2105
                    пользователей.
                </div>
                <div class="improve-block__action">
                    <a href="#" class="btn btn-primary btn-small"><?= Yii::t('app', 'УЛУЧШИТЬ АККАУНТ')?></a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<div class="tabs-block tabs-block--link">
    <div class="tabs-block__inner">
        <div class="tabs-block__head">

            <?= $this->render('_tab_menu_company', ['company' => $company]) ?>

            <?php if ($canEdit) { ?>
                <div class="tabs-block__action">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/company_edit', 'url' => $company->url]) ?>" class="btn-edit">
                        <svg class="icon icon-edit ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= \Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#edit"/>
                        </svg>
                        <span><?= Yii::t('app', 'РЕДАКТИРОВАТЬ ОРГАНИЗАЦИЮ') ?></span>
                    </a>
                </div>
            <?php } ?>
        </div>

        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'validateOnBlur' => false,
            //  'enableClientValidation' => false,
            'action' => \yii\helpers\Url::current(),
            'options' => [
                'method' => 'POST',
            ],
            'fieldConfig' => [
                'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
            ],
        ]); ?>
        <div class="tabs-block__body">
            <div class="section-lk">
                <div class="section-lk__body">
                    <ul class="info-list info-list--small">
                        <li class="info-list__item">
                            <div class="info-list__label">Тариф</div>
                            <div class="info-list__value"><?= TariffPlans::$typesList[$company->tariff->type] ?? '' ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label">Действует до</div>
                            <div class="info-list__value"><?= Yii::$app->formatter->asDatetime($company->tariff_plan_end)?></div>
                        </li>
                    </ul>
                </div>

                <!--
                <div class="section-lk__action section-lk__action--margin-t-big">
                    <button class="btn btn-primary btn-lk">ПРОДЛИТЬ ТАРИФ</button>
                    <button class="btn btn-outline-secondary btn-lk">ИЗМЕНИТЬ ТАРИФ</button>
                </div>
                -->
            </div>

            <?php if ($company->tariff_plan_id != $minTariffPlan->id && !empty($extendPaymentData)) { ?>
                <div class="section-lk">
                    <div class="section-lk__head">
                        <h1 class="section-lk__title"><?= Yii::t('app', 'Продлить тариф') ?></h1>
                    </div>
                    <div class="section-lk__body">
                        <div class="extend-form">
                            <?php $form = ActiveForm::begin([
                                'validateOnBlur' => false,
                                'action' => \yii\helpers\Url::current(),
                                'options' => [
                                    'class' => 'js-form-upload',
                                    'method' => 'POST'
                                ],
                                'fieldConfig' => [
                                    'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
                                ],
                            ]); ?>
                            <div class="extend-form__inner">
                                <div class="extend-form__head form-group">
                                    <label for="" class="control-label"><?= Yii::t('app', 'Период продления') ?></label>
                                </div>
                                <div class="extend-form__form">
                                    <div class="extend-form__input">
                                        <div class="form-group">

                                            <select class="js-dropdown-box" name="extendPayment">
                                                <?php foreach ($extendPaymentData as $id => $countMonth) { ?>
                                                    <option value="<?= $id ?>">
                                                        <?= Yii::t('app', 'Продлить на ') ?>
                                                        <?= $countMonth ?>
                                                        <?= \helpers\Text::plural($countMonth, ['месяц', 'месяца', 'месяцев']) ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="extend-form__action">
                                        <button class="btn btn-primary btn-lk" type="submit"><?= Yii::t('app', 'ПЕРЕЙТИ К ОПЛАТЕ') ?></button>
                                        <!--<button class="btn btn-outline-secondary" type="button"><?= Yii::t('app', 'ОТМЕНА') ?></button>-->
                                    </div>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="change-tariff">
                <div class="change-tariff__inner">
                    <div class="change-tariff__head">
                        <div class="change-tariff__title">Изменить тариф</div>
                    </div>
                    <div class="change-tariff__body">
                        <div class="change-tariff__row">
                            <?php foreach ($tariffPlans as $tariffPlan) { ?>
                                <?php
                                // Для списка тарифов и их цен вывожу только те тарифы, которые указаны для 1 месяца
                                $tariffPlan = $tariffPlan[0];
                                ?>
                                <div class="change-tariff__col">
                                    <div class="tariff-item <?= $company->tariff->type == $tariffPlan['type'] ? 'active' : '' ?>">
                                        <div class="tariff-item__inner">
                                            <div class="tariff-item__head">
                                                <div class="tariff-item__title"><?= Yii::t('app', TariffPlans::$typesList[$tariffPlan['type']]) ?></div>
                                            </div>
                                            <div class="tariff-item__body">
                                                <ul class="tariff-item__list">
                                                    <?php foreach (TariffPlans::getTypesDescription($tariffPlan['type']) as $description) { ?>
                                                        <li><?= $description ?></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                            <div class="tariff-item__foot">
                                                <div class="tariff-item__price">
                                                    <?= empty($tariffPlan['price']) ? Yii::t('app', 'БЕСПЛАТНО') : (Yii::$app->formatter->asMoney($tariffPlan['price']) . ' РУБ./МЕСЯЦ') ?>
                                                </div>
                                                <?php if ($tariffPlan['type'] != TariffPlans::CONST_TYPE_MIN) { ?>
                                                    <div class="tariff-item__action">
                                                        <button
                                                                class="<?= $company->tariff->type == $tariffPlan['type'] ? '_btn' : 'btn-primary' ?> btn btn-w100 js-show-tariff-bar-btn"
                                                                data-popup="selected-tariff-<?= $tariffPlan['type'] ?>"
                                                                data-show-sidebar="true"
                                                        >
                                                            <?= $company->tariff->type == $tariffPlan['type'] ? Yii::t('app', 'Ваш текущий тариф') : Yii::t('app', 'ВЫБРАТЬ') ?>
                                                        </button>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php \yii\bootstrap\ActiveForm::end(); ?>
    </div>
</div>

<!-- Блок с параметрами отображающими цены тарифов -->
<?php foreach ($tariffPlans as $tariffPlan) { ?>
    <?php foreach ($tariffPlan as $key => $value) { ?>
        <div
                class="js-tariff-plan-price-<?=$value['type'] ?>-<?=$value['count_users_id'] ?>-<?= $value['count_month'] ?>"
                data-price="<?= Yii::$app->formatter->asMoney($value['price'] * $value['count_month']) ?>">
        </div>
    <?php } ?>
<?php } ?>

<!-- Блок тарифов -->
<?php foreach ($tariffPlans as $idTariff => $tariffPlan) { ?>
    <div class="popup popup--fill selected-tariff-<?= $idTariff ?> selected-tariff js-tariff-plan-block-<?= $idTariff ?>" id="selected-tariff-<?= $idTariff ?>" style="display: none">
        <?php $form = ActiveForm::begin([
            'validateOnBlur' => false,
            //  'enableClientValidation' => false,
            'action' => \yii\helpers\Url::current(),
            'options' => [
                'id' => 'js-tariff-plan-form-'. $idTariff,
                'enctype' => 'multipart/form-data',
                'class' => 'js-form-upload',
                'method' => 'POST'
            ],
            'fieldConfig' => [
                'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
            ],
        ]); ?>
        <?= $form->field($model, 'type')->hiddenInput(['value' => $idTariff]) ?>
        <div class="popup__box-inner selected-tariff__inner">
            <div class="selected-tariff__head">
                <div class="selected-tariff__close">
                    <button class="_btn js-close-wnd" type="button">
                        <svg class="icon icon-arrow-pag-prev ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias("@static") ?>/images/svg/spriteInline.svg#arrow-pag-prev"/>
                        </svg>
                    </button>
                </div>
                <div class="selected-tariff__title"><?= Yii::t('app', TariffPlans::$typesList[$idTariff]) ?></div>
            </div>
            <div class="selected-tariff__body">
                <div class="selected-tariff__main">
                    <div class="selected-tariff__sub-title"><?= Yii::t('app', 'Дополнительные функции')?></div>
                    <div class="selected-tariff__field">
                        <div class="selected-tariff__label"> <?= Yii::t('app', 'Срок действия тарифа')?></div>
                        <div class="selected-tariff__row js-select-tariff-plan-month">
                            <?php foreach (TariffPlans::$countMonthList as $month) { ?>
                                <div class="selected-tariff__col">
                                    <input  class="_input js-tariff-plan-month-input"
                                            data-type="<?= $idTariff ?>"
                                            type="radio"
                                            value="<?= $month ?>"
                                            id="tar-month<?= $idTariff ?>-<?= $month ?>"
                                            name="<?= \yii\helpers\Html::getInputName($model, 'count_month') ?>"
                                            <?= $month == TariffPlans::COUNT_MONTH_1 ? 'checked' : ''?>
                                    >
                                    <label for="tar-month<?= $idTariff ?>-<?= $month ?>" class="_label"><?= $month ?> <?= \helpers\Text::plural($month, ['месяц', 'месяца', 'месяцев']) ?></label>
                                    <?php if ($month > TariffPlans::COUNT_MONTH_1) { ?>
                                        <?php
                                            $styleShift = 0;
                                            switch ($month) {
                                                case TariffPlans::COUNT_MONTH_3:
                                                    $styleShift = 180;
                                                    break;
                                                case TariffPlans::COUNT_MONTH_6:
                                                    $styleShift = 340;
                                                    break;
                                                case TariffPlans::COUNT_MONTH_12:
                                                    $styleShift = 550;
                                                    break;
                                            }
                                        ?>
                                        <div class="_line" style="width: <?= $styleShift ?>px"></div>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <div class="selected-tariff__field">
                        <div class="selected-tariff__label"><?= Yii::t('app', 'Количество пользователей')?></div>
                        <div class="selected-tariff__row">
                            <?php foreach (TariffPlans::$typeCountUsers as $userId => $user) { ?>
                                <?php
                                $issetTariffForUser = false;
                                // Проверяю, есть ли в тарифе данные для этого кол-ва пользователей
                                foreach ($tariffPlans[$idTariff] as $value) {
                                    if ($value['count_users_id'] == $userId) {
                                        $issetTariffForUser = true;
                                        break;
                                    }
                                }
                                ?>
                                <?php if ($issetTariffForUser) { ?>
                                    <div class="selected-tariff__col">
                                        <input  class="_input js-tariff-plan-user-input"
                                                data-type="<?= $idTariff ?>"
                                                type="radio"
                                                value="<?= $userId ?>"
                                                id="tar-user<?= $idTariff ?>-<?= $userId ?>"
                                                name="<?= \yii\helpers\Html::getInputName($model, 'count_users_id') ?>"
                                            <?= $userId == TariffPlans::TYPE_COUNT_USER_1 ? 'checked' : ''?>
                                        >
                                        <label for="tar-user<?= $idTariff ?>-<?= $userId ?>" class="_label"><?= $user ?> <?= \helpers\Text::plural($user, ['пользователь', 'пользователей']) ?></label>
                                        <?php if ($user > TariffPlans::TYPE_COUNT_USER_1) { ?>
                                            <?php
                                            $styleShift = 0;
                                            switch ($userId) {
                                                case TariffPlans::TYPE_COUNT_USER_2:
                                                    $styleShift = 266;
                                                    break;
                                                case TariffPlans::TYPE_COUNT_USER_3:
                                                    $styleShift = 550;
                                                    break;
                                            }
                                            ?>
                                            <div class="_line" style="width: <?= $styleShift ?>px"></div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>


                    <div class="selected-tariff__field">
                        <div class="selected-tariff__label">
                            <?= Yii::t('app', 'Количество предложений:')?>&nbsp;
                            <?= in_array($idTariff, [TariffPlans::CONST_TYPE_PROF, TariffPlans::CONST_TYPE_CORP]) ? Yii::t('app', 'безлимитное количество') : 0 ?>
                        </div>
                    </div>

                    <div class="selected-tariff__field">
                        <div class="selected-tariff__label">
                            <?= Yii::t('app', 'Количество тендеров:')?>&nbsp;
                            <?= in_array($idTariff, [TariffPlans::CONST_TYPE_PROF, TariffPlans::CONST_TYPE_CORP]) ? Yii::t('app', 'безлимитное количество') : 0 ?>
                        </div>
                    </div>

                </div>

                <div class="selected-tariff__aside">
                    <div class="selected-tariff__price">
                        <span class="js-selected-tariff-price-<?= $idTariff ?>">
                            <?= !empty($tariffPlan[0]) ? Yii::$app->formatter->asMoney($tariffPlan[0]['price']) : Yii::t('app', 'БЕСПЛАТНО') ?>
                        </span>
                        <?= Yii::t('app', 'РУБ.') ?>
                    </div>
                    <button type="submit" class="btn btn-primary"><?= Yii::t('app', 'ПЕРЕЙТИ К ОПЛАТЕ') ?></button>
                </div>
            </div>
        </div>


        <?php ActiveForm::end(); ?>
    </div>
<?php } ?>
