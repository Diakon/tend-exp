<?php
/**
 * @var integer $isAjax
 * @var string $typeTenderList
 * @var array $tenderFavoritesId
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \common\modules\company\models\frontend\Company $company
 * @var \common\modules\tender\models\frontend\Tenders $model
 * @var array $currency
 */
$addObject = new \common\modules\tender\models\frontend\Objects();
$sort = $dataProvider->sort;

$sortASC = Yii::t('app', 'Дата создания тендера (по возрастанию)');
$sortDESC = Yii::t('app', 'Дата создания тендера (по убыванию)');
$selectedSort = $sort->getAttributeOrder('date_start') == SORT_ASC ? $sortASC : $sortDESC;
?>

<div class="tabs-block tabs-block--lk">
    <div class="tabs-block__inner">
        <div class="tabs-block__head">
            <div class="tabs-block__list">
                <a href="<?= \yii\helpers\Url::to(['/dashboard/tenders']) ?>" class="tabs-block__btn <?= empty($typeTenderList) ? 'active' : '' ?>">
                    <?= Yii::t('app', 'МОИ ТЕНДЕРЫ') ?></a>
                <a href="<?= \yii\helpers\Url::to(['/dashboard/tenders', 'type' => 'partners']) ?>" class="tabs-block__btn <?= $typeTenderList == 'partners' ? 'active' : '' ?>">
                    <?= Yii::t('app', 'УЧАСТВУЮ') ?>
                </a>
                <a href="<?= \yii\helpers\Url::to(['/dashboard/tenders', 'type' => 'completed']) ?>" class="tabs-block__btn <?= $typeTenderList == 'completed' ? 'active' : '' ?>">
                    <?= Yii::t('app', 'УЧАСТВОВАЛ') ?>
                </a>
                <?php if (!empty($tenderFavoritesId)) { ?>
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/tenders', 'type' => 'favorites']) ?>" class="tabs-block__btn <?= $typeTenderList == 'favorites' ? 'active' : '' ?>">
                        <?= Yii::t('app', 'ИЗБРАННОЕ') ?>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="filter filter--lk">
    <div class="filter__inner">
        <div class="filter__head">
            <div class="filter__item">
                <div class="filter__label"><?= Yii::t('app', 'Сортировать по:') ?></div>
                <div class="filter__select">
                    <div class="drop-list js-sort-list">
                        <div class="drop-list__header js-sort-list-open">
                            <span class="drop-list__arrow"></span>
                            <span class="drop-list__text"><?= $selectedSort ?></span>
                        </div>

                        <div class="drop-list__box">
                            <ul class="drop-list__list">
                                <li>
                                    <?= \yii\helpers\Html::a($selectedSort == $sortASC ? $sortDESC : $sortASC, $sort->createUrl('date_start', true)) ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="filter__body">
            <div class="filter__action">
                <button class="btn btn-outline-secondary js-advanced-search-btn" data-popup="advanced-search">
                    <svg class="icon icon-filter ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#filter"/>
                    </svg>
                    <span><?= Yii::t('app', 'ФИЛЬТРЫ')  ?></span>
                </button>
                <?php if (!empty($typeTenderList)) { ?>
                    <a href="<?= \yii\helpers\Url::to(['/tender/tenders/tender_list']) ?>" class="btn btn-primary">
                        <?= Yii::t('app', 'ПОИСК ТЕНДЕРОВ В КАТАЛОГЕ')  ?>
                    </a>
                <?php } else if (Yii::$app->user->can('addTender', ['companyUrl' => Yii::$app->request->get('companyUrl')])) { ?>
                    <a href="<?= $model->getCreateLink($company->url) ?>" class="btn btn-primary">
                        <?= Yii::t('app', 'СОЗДАТЬ НОВЫЙ ТЕНДЕР')  ?>
                    </a>
                <?php }
                ?>
            </div>
        </div>
    </div>
</div>

<?= $this->render('_tenders_list_inner', ['dataProvider' => $dataProvider, 'isAjax' => $isAjax, 'company' => $company, 'typeTenderList' => $typeTenderList, 'currency' => $currency]) ?>

<div class="pagination">
    <div class="pagination__inner">
        <?= $this->render('_tenders_pagination', ['dataProvider' => $dataProvider]) ?>
    </div>
</div>


<?= $this->render('_tenders_list_filter', ['model' => $model, 'address' => $address]) ?>