<?php
/**
 * @var \common\modules\offers\models\frontend\Offers $offers
 */
?>
<?php foreach ($offers as $offer) { ?>
    <?php
    // Предложения по тендеру от компании
    $offerTender = $offer->getOffersTender();
    $keyLasOffer = end($offerTender);
    ?>
    <b><?= Yii::t('app', 'Компания') ?>: <?= $offer->company->title ?></b>
    <?php if (!empty($offerTender[0])) { ?>
        <p>
            <?= Yii::t('app', 'Первое предложение') . '(' . Yii::$app->formatter->asDate($offerTender[0]['date'], 'php:d.m.Y') . ')'  ?>:
            <?= Yii::$app->formatter->asMoney($offerTender[0]['sum']) ?>
        </p>
    <?php } ?>
    <?php if (!empty($offerTender[1])) { ?>
        <p>
            <?= Yii::t('app', 'Второе предложение') . '(' . Yii::$app->formatter->asDate($offerTender[1]['date'], 'php:d.m.Y') . ')'  ?>:
            <?= Yii::$app->formatter->asMoney($offerTender[1]['sum']) ?>
        </p>
    <?php } ?>
    <?php if (!empty($keyLasOffer)) { ?>
        <p>
            <?= Yii::t('app', 'Последнее предложение') . '(' . Yii::$app->formatter->asDate($keyLasOffer['date'], 'php:d.m.Y') . ')'  ?>:
            <?= Yii::$app->formatter->asMoney($keyLasOffer['sum']) ?>
        </p>
    <?php } ?>
<?php } ?>