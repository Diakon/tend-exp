<?php
/**
 * $var $model \common\modules\company\models\frontend\CompanyTenderEstimatesRubrics
 */
$comments = $model->getComments()->orderBy(['created_at' => SORT_DESC])->all();
$countComments = count($comments);
?>
<?php if (!empty($countComments)) { ?>
    <div class="acco-work__comment">
        <button class="btn-comment" type="button" data-popup="comment-list-<?= $id ?>">
            <svg class="icon icon-comment ">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#comment"/>
            </svg>
            <span class="_qty"><?= $countComments ?></span>
        </button>
    </div>
<?php } ?>



<div class="popup popup--comment js-popup-comment-block-id-<?= $id ?>" data-popup-id="comment-list" id="comment-list-<?= $id ?>" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <button class="popup__close js-close-wnd" type="button"></button>
            <div class="popup__title"><?= Yii::t('app', 'Комментарии') ?></div>
            <div class="popup__body">
                <div class="popup-comment">
                    <ul class="popup-comment__list js-popup-scroll">
                        <?php foreach ($comments as $comment) { ?>
                            <li class="popup-comment__item popup-comment__item--blue">
                                <div class="popup-comment__item-logo">
                                    <?php if ($comment->user->avatar) { ?>
                                        <img src="<?php echo \common\components\Thumbnail::thumbnailFile($comment->user->avatar->getFile(true));?>" alt="" class="_img">
                                    <?php } else { ?>
                                        <img src="<?= Yii::getAlias('@static') ?>/../content/images/helment-small.svg" alt="" class="_img">
                                    <?php } ?>
                                </div>
                                <div class="popup-comment__item-content">
                                    <p>
                                        <?= $comment->text ?>
                                    </p>
                                </div>
                            </li>

                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
