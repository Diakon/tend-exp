<?php

use yii\helpers\Html;

/**
 * @var \common\modules\tender\models\common\TendersCharsWorks $charsWorks
 * @var \common\modules\tender\models\frontend\Tenders $model
 */

?>
<?php foreach ($charsWorks as $key => $data) { ?>
<div class="row js-tender-chars-works-block-<?= $key ?>" style="margin-top: 10px;">
    <div class="col-3 depdrop js-tender-chars-works-main-work-block">
        <div class="form-group form-group--autocomplete">
            <div class="form-group">
                <label for="" class="control-label "><?= Yii::t('app', 'Направление работ') ?></label>
                <?= Html::dropDownList('mainWorkId[' . $key . ']', $data->main_work_id, \common\modules\directories\models\common\MainWork::findActiveList(), [
                    'class' => '_select js-tender-chars-works-main-work-input js-tender-chars-works-main-work-input-' . $key,
                    'data-key' => $key,
                    'prompt' => '',
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-3 depdrop js-tender-chars-works-type-work-block js-tender-chars-works-type-work-block-<?= $key ?>" style="<?= !is_int($key) ? 'display:none;' : '' ?>">
        <div class="form-group form-group--autocomplete">
            <div class="form-group">
                <label for="" class="control-label "><?= Yii::t('app', 'Группы работ') ?></label>
                <?= Html::dropDownList('typeWorkId[' . $key . ']', $data->types_work_id, \common\modules\directories\models\common\TypesWork::findActiveList(), [
                    'class' => '_select js-tender-works-type-work-input js-tender-chars-works-type-work-input-' . $key,
                    'data-key' => $key,
                    'prompt' => '',
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-3 depdrop js-tender-chars-works-work-block js-tender-chars-works-work-block-<?= $key ?>" style="<?= !is_int($key) ? 'display:none;' : '' ?>">
        <div class="form-group form-group--autocomplete">
            <div class="form-group">
                <label for="" class="control-label "><?= Yii::t('app', 'Работа') ?></label>
                <?= Html::activeDropDownList($model, 'charsWorksIds[' . $key . ']', \common\modules\directories\models\common\Works::findActiveList(), [
                    'class' => '_select js-tender-chars-works-work-input js-tender-chars-works-work-input-' . $key,
                    'data-key' => $key,
                    'prompt' => '',
                    'value' => $data->works_id
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-3">
        <div class="form-group">
            <div class="form-group">
                <button class="btn btn-w100 btn-outline-secondary btn-plus js-tender-chars-works-delete-btn" style="width: 50px; margin-top: 30px" data-key="<?= $key ?>" type="button">x</button>
            </div>
        </div>
    </div>
</div>
<?php } ?>
