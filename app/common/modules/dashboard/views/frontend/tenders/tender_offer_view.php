<?php
use components\ActiveForm;
use common\modules\tender\models\frontend\Tenders;
use yii\helpers\ArrayHelper;
use frontend\widgets\EstimatesList;
use common\modules\offers\models\frontend\Offers;
/**
 * @var \common\modules\offers\models\forms\OfferForm $model
 * @var \common\modules\offers\models\frontend\Offers $offer
 * @var array $comments
 * @var string $companyUrl
 */
$errors = Yii::$app->session->getAllFlashes();
$files = $offer->files;
$fileSumSize = 0;
foreach ($files as $file) {
    $fileSumSize += $file->fileSize;
}
\frontend\widgets\FlashMessagesWidget::widget();
?>
<?php $form = ActiveForm::begin([
    'validateOnBlur' => false,
    'errorCssClass' => 'form-error',
    'options' => [
        'id' => 'js-edit-tender-form',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => '<div class="form-row">{label}{input}{hint}{error}</div>',
    ],
]); ?>
<main class="main">
    <div class="main__head">
        <h1 class="main__title"><?= Yii::t('app', 'Просмотр предложения') ?></h1>
    </div>

    <div class="main__body">
        <div class="section-lk">
            <div class="section-lk__head">
                <div class="main__label main__label--small"><?= Tenders::getModes($model->tender->mode) . ' ' . Yii::t('app', 'тендер') ?> № <?= $model->tender->id ?>. <?= Tenders::getTypes($model->tender->type) ?></div>
                <h1 class="section-lk__title"><?= $model->tender->title ?></h1>
            </div>

            <div class="section-lk__body">
                <div class="participate-block">
                    <div class="participate-block__inner">
                        <div class="participate-block__left">
                            <ul class="info-list">
                                <li class="info-list__item">
                                    <div class="info-list__label"><?= Yii::t('app', 'Тип объекта') ?></div>
                                    <div class="info-list__value"><?= implode(', ', ArrayHelper::map($model->tender->object->types, 'id', 'title')) ?></div>
                                </li>
                                <li class="info-list__item">
                                    <div class="info-list__label"><?= Yii::t('app', 'Начало работ') ?></div>
                                    <div class="info-list__value"><?= Yii::$app->formatter->asDate($model->tender->date_start, 'php:d.m.Y') ?></div>
                                </li>
                                <li class="info-list__item">
                                    <div class="info-list__label"><?= Yii::t('app', 'Вид работ') ?></div>
                                    <div class="info-list__value"><?= implode(', ', ArrayHelper::map($model->tender->object->works, 'id', 'title')) ?></div>
                                </li>
                                <!--
                                <li class="info-list__item">
                                    <div class="info-list__label"><?= Yii::t('app', 'Краткое описание') ?></div>
                                    <div class="info-list__value">
                                        <?= $model->tender->description ?>
                                    </div>
                                </li>
                                -->
                            </ul>
                        </div>
                        <!--
                        <div class="participate-block__right">
                            <div class="participate-block__action">
                                <label class="btn btn-primary">
                                    <input type="file" hidden>
                                    <span>ЗАГРУЗИТЬ РАСЧЕТ</span>
                                </label><br>
                                <a href="#" download="" class="btn btn-outline-secondary">СКАЧАТЬ СМЕТУ</a>
                            </div>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>

        <?= EstimatesList::widget([
            'typeEstimates' => EstimatesList::TYPE_CORRECTION_OFFER,
            'offerForm' => $model,
            'tender' => $model->tender,
        ]); ?>


        <div class="section-lk__acco">
            <div class="section-lk__acco-head js-acco-work js-offer-conditions-title">
                <div class="section-lk__acco-title"><?= Yii::t('app', 'Условия') ?></div>
            </div>
            <div class="section-lk__acco-body">
                <div class="section-lk__head">
                    <div class="section-lk__label">
                        <?= Yii::t('app', 'Обозначьте условия предоставления вами выше указанных услуг')?>.
                    </div>
                </div>
                <div class="section-lk__body">
                    <div class="conditions-block conditions-block--edit">
                        <div class="conditions-block__inner">
                            <div class="conditions-block__left">
                                <ul class="info-list">
                                    <li class="info-list__item">
                                        <div class="info-list__label">
                                            <?= Yii::t('app', 'Срок работы (мес.)') ?>
                                        </div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= $offer->time_work ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label">
                                            <?= Yii::t('app', 'Срок гарантии (мес.)') ?>
                                        </div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= $offer->time_warranty ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label">
                                            <?= Yii::t('app', 'Банковская гарантия для аванса') ?>
                                        </div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Yii::t('app', Offers::TYPE_WARRANTY_YES == $offer->bank_prepayment_warranty ? 'Да' : 'Нет') ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label">
                                            <?= Yii::t('app', 'Банковская гарантия для гарантийного периода') ?>
                                        </div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Yii::t('app', Offers::TYPE_WARRANTY_YES == $offer->bank_period_warranty ? 'Да' : 'Нет') ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label">
                                            <?= Yii::t('app', 'Банковская гарантия для выполнения обязательств') ?>
                                        </div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Yii::t('app', Offers::TYPE_WARRANTY_YES == $offer->bank_obligation_warranty ? 'Да' : 'Нет') ?>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="conditions-block__right">
                                <div class="conditions-block__note">
                                    <div class="_title"><?= Yii::t('app', 'Порядок оплаты') ?></div>
                                    <div class="form-group">
                                        <?= $offer->text ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-lk">
            <div class="section-lk__head">
                <h1 class="section-lk__title">Документы</h1>
                <div class="section-lk__label">
                    <span class="_item">Общий объем файлов <?= number_format($fileSumSize / 1000000, 2, '.', ''); ?> мб (максимум 1 гб)</span>
                    <span class="_item">
                                <!--
                                <button class="btn-upload">
                                    <svg class="icon icon-download ">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#download"/>
                                    </svg>
                                    <span><?= Yii::t('app', 'Скачать все файлы') ?></span>
                                </button>
                                -->
                            </span>
                </div>
            </div>
            <div class="section-lk__body">
                <div class="doc-load">
                    <div class="doc-load__inner">

                        <div class="doc-load__body">
                            <div class="doc-load__row js-files-container">
                                <?php
                                if (!empty($files)) {
                                    /** @var \common\models\EntityFile[] $files */
                                    foreach ($files as $file) { ?>
                                        <div class="col-6 js-delete-file-url js-delete-file-block-<?= $file->id ?>">
                                            <div class="doc-item">
                                                <div class="doc-item__inner">
                                                    <div class="doc-item__title">
                                                        <a href="/upload/tenders/files/<?= $file->modelId ?>/<?= $file->fileName ?>" class="doc-item__link"><?= $file->title ?>.<?= $file->fileExt ?></a>
                                                    </div>
                                                    <div class="doc-item__size"><?=  number_format($file->fileSize / 1000000, 2, '.', ''); ?> мб</div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($comments)) { ?>
            <div class="section-lk__acco">
                <div class="section-lk__acco-head js-acco-work">
                    <div class="section-lk__acco-title">Комментарии заказчика</div>
                </div>
                <div class="section-lk__acco-body">
                    <?php foreach ($comments as $comment) { ?>
                        <p><?= $comment['text'] ?></p><br>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <div class="section-lk__action section-lk__action--flex">
            <button class="btn btn-primary" type="submit">СКОРРЕКТИРОВАТЬ ПРЕДЛОЖЕНИЕ</button>
            <!--<button class="btn btn-outline-secondary btn-white-bg" type="submit">СОХРАНИТЬ ЧЕРНОВИК</button>-->
            <a href="<?= \yii\helpers\Url::to(['/dashboard/tender_offers_list', 'companyUrl' => $companyUrl, 'id' => $model->tender->id]) ?>" class="btn btn-outline-secondary btn-white-bg btn-ml">ОТМЕНИТЬ</a>
        </div>
    </div>
</main>
<?php ActiveForm::end(); ?>


<?php
/*
use components\ActiveForm;
use yii\helpers\Html;
use frontend\widgets\EstimatesList;

?>

<?php $form = ActiveForm::begin([
    'validateOnBlur' => false,
    'errorCssClass' => 'form-error',
    'options' => [
        'id' => 'js-edit-tender-form',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'template' => '<div class="form-row">{label}{input}{hint}{error}</div>',
    ],
]); ?>

<?php if ($isBestOffer) { ?>
    <b><?= Yii::t('app', 'Лучшее предожение') ?></b>
<?php } ?>

<?php if (empty($model->tender->offer_id)) { ?>
    <?= Html::a(Yii::t('app', 'Выбрать'), '#', [
        'class' => 'js-select-tender-offer-winner'
    ]) ?>
<?php } else { ?>
    <?= Yii::t('app', 'Победитель тендера выбран') ?>
<?php } ?>

<ul>
    <li>
        <?= Yii::t('app', 'Компания') ?>:<?= $offer->company->title ?>
    </li>
    <li>
        <?= Yii::t('app', 'опубиковано') ?>:<?= Yii::$app->formatter->asDate($offer->created_at, 'php:d.m.Y') ?>
    </li>
    <li>
        <?= $offer->user->fullName ?>
    </li>
    <li>
        <?= $offer->user->email ?>
    </li>
    <li>
        <?= $offer->user->phone ?>
    </li>
</ul>

<br>Коментарий:<textarea class="js-comment-block"></textarea>

<?= EstimatesList::widget([
    'typeEstimates' => EstimatesList::TYPE_VIEW_OFFER,
    'offerForm' => $model,
    'tender' => $model->tender,
    'view' => 'estimates_list/list',
]);
?>

<br>
<?= Yii::t('app', 'Дополнительные работы') ?>
<br>
<?= Yii::t('app', 'Если вы считаете необходимым проведение дополнительных работ, не указанных в смете Заказчика, укажите их ниже') ?>:
<div class="js-offer-add-work-main-block">
    <?= $this->render('_tender_offer_additional_work', ['model' => $model->addWorks, 'offer' => $offer]);?>
</div>

<?php if (!empty($documents)) { ?>
    <?= Yii::t('app', 'Документы') ?>:
    <?php foreach ($documents as $document) { ?>
        <a href="<?= $document['url'] ?>"><?= $document['name'] ?></a>
    <?php } ?>
<?php } ?>

    <button type="submit"><span>Сохранить</span></button>
<?php ActiveForm::end(); ?>
<?php */ ?>
