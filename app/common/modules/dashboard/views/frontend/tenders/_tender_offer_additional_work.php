<?php
/**
 * @var \common\modules\offers\models\forms\OfferForm $model
 * @var \common\modules\offers\models\frontend\Offers $offer
 */
use common\modules\offers\models\common\OffersElements;
?>
<?php foreach ($model as $key => $data) { ?>
    <?php
    $sum = $offer->getSumPrice($data['id']);
    ?>
    <ul>
        <li>
            Работа: <?= $data['title'] ?>
        </li>
        <li>
            Количество: <?= $data['count'] ?>
        </li>
        <li>
            Ед. измерения: <?= OffersElements::getUnits($data['unit_id']) ?>
        </li>
        <li>
            Цена работы: <?= $data['price_work'] ?>
        </li>
        <li>
            Цена материала: <?= $data['price_material'] ?>
        </li>
        <li>С скидкой: <?= Yii::$app->formatter->asMoney($sum['withDiscount']) ?></li>
        <li>Без скидки: <?= Yii::$app->formatter->asMoney($sum['noDiscount']) ?></li>
    </ul>
<?php } ?>
