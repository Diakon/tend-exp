<?php
use frontend\widgets\EstimatesList;
use yii\bootstrap\ActiveForm;
use common\modules\tender\models\frontend\Objects;
use yii\helpers\ArrayHelper;
use common\modules\tender\models\frontend\Tenders;
use common\modules\directories\models\common\FunctionsCompany;

/**
 * @var \common\modules\company\models\frontend\Company $company
 * @var \common\modules\company\models\frontend\CompanyUsers $user
 * @var \common\modules\tender\models\frontend\Tenders $model
 * @var array $objects
 * @var boolean $isNewRecord
 * @var string $tHash
 * @var boolean $canEditTender
 * @var array $typesWorkList
 * @var array $workList
 */
$canAddObject = Yii::$app->user->can('addProject');
$this->title = Yii::t('app', (!empty($model->id) ? 'Редактирование' : 'Добавление') . ' тендера');
$success =  Yii::$app->getSession()->getFlash('success-tender');
$error =   Yii::$app->getSession()->getFlash('error-tender');
\frontend\widgets\FlashMessagesWidget::widget();
?>

    <div class="main__body">
        <?php $form = ActiveForm::begin([
            'action' => \yii\helpers\Url::current(),
            'validateOnBlur' => true,
            'options' => [
                'id' => 'js-edit-tender-form',
                'enctype' => 'multipart/form-data',
                'method' => 'POST'
            ],
            'fieldConfig' => [
                'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
            ],
        ]); ?>

            <div class="section-lk">
                <div class="section-lk__head">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Информация об объекте') ?></h1>
                    <div class="section-lk__label">
                        <?= Yii::t('app', 'Укажите основные характеристики объекта, на котором будут проводиться работы.') ?>
                    </div>
                </div>
                <div class="section-lk__body">
                    <div class="form-group">
                        <label for="" class="control-label"><?= Yii::t('app', $canEditTender ? 'Выберите объект' : 'Объект') ?></label>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?php if ($canEditTender) { ?>
                                        <?= $form->field($model, 'object_id', ['template' => '{input}{error}'])
                                            ->dropDownList(
                                                $objects,
                                                [
                                                    'class' => 'js-dropdown-box',
                                                    'prompt' => Yii::t('app', 'Выберите объект'),
                                                ]
                                            )->label(null)
                                            ->error(['tag' => 'div']) ?>
                                    <?php } else { ?>
                                        <?= $model->object->title ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php if ($canAddObject && $canEditTender) { ?>
                                <div class="col-6">
                                    <div class="create-action">
                                        <div class="create-action__label js-create-object-in-tender-label" style="<?= !empty($model->isCreateObject) ? 'display: none;' : '' ?>">
                                            <?= Yii::t('app', 'Или создайте новый') ?>
                                        </div>
                                        <button class="btn btn-outline-secondary js-create-object-in-tender js-object-in-tender-btn" data-type="1" style="<?= !empty($model->isCreateObject) ? 'display: none;' : '' ?>">
                                            <?= Yii::t('app', 'СОЗДАТЬ НОВЫЙ ОБЪЕКТ' ) ?>
                                        </button>
                                        <button class="btn btn-outline-secondary js-cancel-create-object-in-tender js-object-in-tender-btn" data-type="0" style="<?= empty($model->isCreateObject) ? 'display: none;' : '' ?>">
                                            <?= Yii::t('app', 'ОТМЕНИТЬ СОЗДАНИЕ ОБЪЕКТА') ?>
                                        </button>
                                        <?= $form->field($model, 'isCreateObject')->hiddenInput(['class' => 'js-create-object-in-tender-hidden-input'])->label(false) ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>


                <?php if ($canAddObject) { ?>
                    <div class="js-create-object-in-tender-block" style="<?= empty($model->isCreateObject) ? 'display: none;' : '' ?>">
                        <div class="section-lk__head">
                            <h1 class="section-lk__title"><?= Yii::t('app', 'Новый объект') ?></h1>
                        </div>
                        <div class="section-lk__body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group   ">
                                        <?= $form->field($model, 'createObjectTitle')
                                            ->textInput(['class' => 'form-control', 'placeholder' => ''])
                                            ->label(Yii::t('app', 'Название'), ['class' => 'control-label'])
                                            ->error(['tag' => 'div'])
                                        ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group   ">
                                        <?= $form->field($model, 'createObjectStatus', ['template' => '{label}{input}{error}'])
                                            ->dropDownList(
                                                ArrayHelper::map(Objects::getStatusComplete(), 'id', 'title'),
                                                [
                                                    'class' => 'js-dropdown-box',
                                                    'data-placeholder' => Objects::getStatusComplete($model->createObjectStatus)
                                                ]
                                            )->label(Yii::t('app', 'Статус'), ['class' => 'control-label'])
                                            ->error(['tag' => 'div'])
                                        ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group   ">
                                        <?= $form->field($model, 'createObjectType', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}</div>{error}'])
                                            ->dropDownList(
                                                \common\modules\directories\models\common\Projects::findActiveList(),
                                                [
                                                    'multiple' => 'multiple',
                                                    'size' => 1,
                                                    'class' => ''
                                                ]
                                            )->label(Yii::t('app', 'Тип'), ['class' => 'control-label'])
                                            ->error(['tag' => 'div']) ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group   ">
                                        <?= $form->field($model, 'createObjectFunction', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}</div>{error}'])
                                            ->dropDownList(
                                                FunctionsCompany::findActiveList(),
                                                [
                                                    'multiple' => 'multiple',
                                                    'size' => 1,
                                                    'class' => ''
                                                ]
                                            )->label(Yii::t('app', 'Функция'), ['class' => 'control-label'])
                                            ->error(['tag' => 'div']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="section-lk">
                <div class="section-lk__head">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Информация о тендере') ?></h1>
                    <div class="section-lk__label">
                        <?= Yii::t('app', 'Укажите основные настройки проведения тендера.') ?>
                    </div>
                </div>
                <div class="section-lk__body">
                    <div class="row">
                        <div class="col-3 form-group   ">
                            <div>
                                <?php if ($canEditTender) { ?>
                                    <?= $form->field($model, 'title', ['template' => '{label}{input}{error}'])
                                        ->textInput(['class' => 'form-control', 'placeholder' => '',  'disabled' => $canEditTender ? false : true])
                                        ->label(Yii::t('app', 'Название'), ['class' => 'control-label'])
                                        ->error(['tag' => 'div'])
                                    ?>
                                <?php } else { ?>
                                    <?= \yii\helpers\Html::label(Yii::t('app', 'Название'), null, ['class' => 'control-label']); ?>
                                    <br>
                                    <?= $model->title ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group   ">
                                <?php if ($canEditTender) { ?>
                                    <?= $form->field($model, 'type', ['template' => '{label}{input}{error}'])
                                        ->dropDownList(
                                            \common\modules\tender\models\frontend\Tenders::getTypes(),
                                            [
                                                'class' => 'js-dropdown-box',
                                                'data-placeholder' => ''
                                            ]
                                        )->label(Yii::t('app', 'Тип'), ['class' => 'control-label'])
                                        ->error(['tag' => 'div']) ?>
                                <?php } else { ?>
                                    <?= \yii\helpers\Html::label(Yii::t('app', 'Тип'), null, ['class' => 'control-label']); ?>
                                    <br>
                                    <?= \common\modules\tender\models\frontend\Tenders::getTypes()[$model->type] ?? '' ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group   ">
                                <?php if ($canEditTender) { ?>
                                    <?= $form->field($model, 'type', ['template' => '{label}{input}{error}'])
                                        ->dropDownList(
                                            \common\modules\tender\models\frontend\Tenders::getModes(),
                                            [
                                                'class' => 'js-dropdown-box',
                                                'data-placeholder' => ''
                                            ]
                                        )->label(Yii::t('app', 'Вид'), ['class' => 'control-label'])
                                        ->error(['tag' => 'div']) ?>
                                <?php } else { ?>
                                    <?= \yii\helpers\Html::label(Yii::t('app', 'Вид'), null, ['class' => 'control-label']); ?>
                                    <br>
                                    <?= \common\modules\tender\models\frontend\Tenders::getModes()[$model->type] ?? '' ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group form-group--date">
                                <?= $form->field($model, 'date_end', ['template' => '<div class="form-row">{label}{input}{hint}{error}</div>'])
                                    ->textInput([
                                        'class' => 'form-control js-datepicker',
                                        'placeholder' => Yii::t('app', 'Выберите дату'),
                                        'autocomplete' => 'off',
                                        'data-date-start-date' => 'today'
                                    ])->label(Yii::t('app', 'Срок завершения тендера'), ['class' => 'control-label'])
                                    ->error(['tag' => 'div']) ?>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group   ">
                                <?php if ($canEditTender) { ?>
                                    <?= $form->field($model, 'currency_id', ['template' => '<div class="form-row">{label}{input}{hint}{error}</div>'])
                                        ->dropDownList(ArrayHelper::map(\common\modules\directories\models\common\Currency::getActive()->asArray()->all(), 'id', 'title'),
                                            ['class' => 'js-dropdown-box'])
                                        ->label(null, ['class' => 'control-label'])
                                        ->error(['tag' => 'div']) ?>
                                <?php } else { ?>
                                    <?= \yii\helpers\Html::label(Yii::t('app', 'Валюта'), null, ['class' => 'control-label']); ?>
                                    <br>
                                    <?= $model->currency->title ?? '' ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <?php if ($canEditTender) { ?>
                                    <?= $form->field($model, 'function_contractor_id', ['template' => '<div class="form-row">{label}{input}{hint}{error}</div>'])
                                        ->dropDownList(
                                            FunctionsCompany::findActiveList(),
                                            [
                                                'class' => 'js-dropdown-box'
                                            ]
                                        )->label(null, ['class' => 'control-label'])
                                        ->error(['tag' => 'div']) ?>
                                <?php } else { ?>
                                    <?= \yii\helpers\Html::label(Yii::t('app', 'Функция подрядчика'), null, ['class' => 'control-label']); ?>
                                    <br>
                                    <?= $model->functionContractor->title ?? '' ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if ($canEditTender) { ?>
                        <!-- Блок с селектами выбора -->
                        <div class="js-tendet-chars-works-selects-block-hidden-block" style="display:none;">
                            <?php
                            // Создаю записи с справочниками для использования в селектах
                            foreach ($typesWorkList as $workData) {
                                echo \yii\helpers\Html::tag('div', '', [
                                    'class' => 'js-tender-chars-select-data-type-work-' . $workData['parent_id'],
                                    'data-id' => $workData['id'],
                                    'data-title' => $workData['title']]
                                );
                            }
                            foreach ($workList as $workData) {
                                echo \yii\helpers\Html::tag('div', '', [
                                        'class' => 'js-tender-chars-select-data-work-' . $workData['parent_id'],
                                        'data-id' => $workData['id'],
                                        'data-title' => $workData['title']]
                                );
                            }
                            ?>
                        </div>

                        <!-- Скрытый блок для JS клонирования -->
                        <div class="js-chars-works-block-hidden-block" style="display:none;">
                            <?= $this->render('_chars_works', [
                                'model' => $model,
                                'charsWorks' => ['keyCloneId' => new \common\modules\tender\models\common\TendersCharsWorks()],
                            ]); ?>
                        </div>

                        <div class="row js-chars-works-block">
                            <?= $this->render('_chars_works', [
                                'model' => $model,
                                'charsWorks' => $model->charsWorks,
                            ]); ?>
                        </div>
                        <?= \yii\helpers\Html::hiddenInput('countCompletedWorks', count($model->charsWorks), [
                            'class' => 'js-count-chars-works-hidden-input',
                        ]) ?>
                        <div class="col-1" style="margin-top: 50px;">
                            <div class="row">
                                <button class="btn btn-w100 btn-outline-secondary btn-plus js-chars-works-add-btn" type="button">+</button>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <?= \yii\helpers\Html::label(Yii::t('app', 'Работы'), null,
                                        ['class' => 'control-label']); ?>
                                    <br>
                                    <?php foreach ($model->charsWorks as $dataWorks) { ?>
                                        <?= $dataWorks->mainWork->title ?? '' ?>, <?= $dataWorks->typesWork->title ?? '' ?>, <?= $dataWorks->work->title ?? '' ?>
                                        <br>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <?= EstimatesList::widget([
                'typeEstimates' => EstimatesList::TYPE_ADD_TENDER,
                'offerForm' => null,
                'tender' => $model,
                'canEdit' => $canEditTender,
            ]); ?>

            <?=  \frontend\modules\upload\widgets\UploadFileFormWidget::widget([
                'model' => $model,
                'modelId' => $model->isNewRecord ? $tHash : $model->id,
                'canEdit' => $canEditTender,
                'type' => \frontend\modules\upload\widgets\UploadFileFormWidget::TYPE_DOCUMENTS
            ]); ?>

            <?php if ($canEditTender) { ?>
                <div class="section-lk">
                    <div class="section-lk__head">
                        <h1 class="section-lk__title"><?= Yii::t('app', 'Внешнее хранилище')?></h1>
                        <div class="section-lk__label">
                        <span>
                            <?= Yii::t('app', 'Вы можете указать ссылку на внешнее хранилище файлов (yandex Диск, dropbox и т.п.)')?>
                        </span>
                        </div>
                    </div>
                    <div class="section-lk__body">
                        <div class="doc-load">
                            <div class="doc-load__inner">
                                <?= $form->field($model, 'files_store_link', ['template' => '<div class="form-row">{label}{input}{hint}{error}</div>'])
                                    ->textInput(['class' => 'form-control', 'placeholder' => Yii::t('app', 'Ссылка на внешнее хранилище файлов')])
                                    ->label(false, ['class' => 'control-label'])
                                    ->error(['tag' => 'div']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>


        <?php if (!$model->isNewRecord) { ?>
            <?php if ($canEditTender) { ?>
                <div class="section-lk__action">
                    <?= $form->field($model, 'status',  ['horizontalCssClasses' => ['wrapper' => '']])
                        ->checkbox(['tag' => false,])
                        ->label('<span class="check-label">' . Yii::t('app', 'Статус публикации тендера') . '</span>', ['class' => 'check-box']);
                    ?>
                </div>
            <?php } else {  ?>
                <div class="section-lk__action">
                    <?= \yii\helpers\Html::label(Yii::t('app', 'Статус публикации тендера'), null, ['class' => 'control-label']); ?>:
                    <?= Tenders::getStatuses($model->status) ?>
                </div>
            <?php } ?>
        <?php } ?>

        <div class="section-lk__action section-lk__action--flex">
            <button class="btn btn-primary js-add-tender-submit-btn" type="submit"><?= Yii::t('app', 'СОХРАНИТЬ') ?></button>
            <a class="btn btn-outline-secondary" href="<?= \yii\helpers\Url::to(['/dashboard/tenders']) ?>"><?= Yii::t('app', 'ОТМЕНИТЬ')?></a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

