<?php
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\EstimatesElements;
use yii\helpers\Html;

/**
 * @var $rubric EstimatesRubrics
 * @var $element EstimatesElements
 */
?>
<?= Html::activeHiddenInput($element, 'id', [
    'class' => 'js-estimates-elements-id'
]) ?>
<div class="acco-work__form-text">
    <div class="form-group   ">
        <?= Html::activeInput('text', $element, 'title', [
            'class' => 'js-edit-estimates-element js-estimates-elements-title form-control',
            'placeholder' => Yii::t('app', 'Название работы'),
            'data-id' => $element->id,
            'data-rubric' => $element->rubric_id,
            'value' => $element->isNewRecord ? EstimatesElements::DEFAULT_NAME : $element->title
        ]) ?>
    </div>
</div>

<div class="acco-work__form-qty">
    <div class="form-group   ">
        <?= Html::activeInput('text', $element, 'count', [
            'class' => 'js-edit-estimates-element js-estimates-elements-count form-control',
            'data-id' => $element->id,
            'data-rubric' => $element->rubric_id,
            'placeholder' => Yii::t('app', 'Количество'),
            'value' => (int)$element->count ])
        ?>
    </div>
</div>

<div class="acco-work__form-metric">
    <div class="form-group   ">
        <?= Html::activeDropDownList($element, 'unit_id', EstimatesElements::getUnits(), [
            'class' => 'js-edit-estimates-element js-estimates-elements-unit_id js-dropdown-box',
            'data-id' => $element->id,
            'data-rubric' => $element->rubric_id,
            'placeholder' => Yii::t('app', 'Единица измерения')
        ]) ?>
    </div>
</div>

<?= Html::a('x', '#', [
    'class' => 'js-delete-estimates-element',
    'data-id' => $element->id,
    'data-rubric' => $element->rubric_id
]) ?>
