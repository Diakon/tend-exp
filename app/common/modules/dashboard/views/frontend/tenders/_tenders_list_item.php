<?php
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\frontend\Tenders;
/**
 * @var array $currency
 * @var null|string $typeTenderList
 * @var common\modules\tender\models\frontend\Tenders $model
 * @var \common\modules\company\models\frontend\Company $company
 */

$companyAuthorTender = $model->object->company;
if (in_array($typeTenderList, ['partners', 'completed'])) {
    // Получаю последнего предложения для этого тендера
    $tenderOffer = Offers::getLastActiveOffer($company->id, $model->id);
    $statusOffer = !empty($tenderOffer->status)  ? Offers::getStatusList($tenderOffer->status)  : '';
    $offerForm = \common\modules\offers\models\forms\OfferForm::setOfferFormByOffer($tenderOffer);
}
if (empty($typeTenderList)) {
    // Получаю предложения для этого тендера - 1ое, мой ответ и последнее
    $offersCount = $model->getOffersCount();
    $offers = !empty($offersCount) ? $model->getTendersListOffers() : null;
}
?>

<div class="tender-item js-tender-acco">
    <div class="tender-item__inner">
        <div class="tender-item__head">
            <div class="tender-item__options">
                <div class="tender-item__option"><?= Yii::t('app', Tenders::getModes($model->mode) . ' тендер') ?> № <?= $model->id ?></div>
                <div class="tender-item__option"><?= Yii::t('app', 'Тип') ?>: <span class="_inline"><?= Yii::t('app', Tenders::getTypes($model->type)) ?></span></div>
                <div class="tender-item__option"><?= Yii::t('app', 'Дата завершения тендера') ?>: <span class="_inline"><?= Yii::$app->formatter->asDate($model->date_end, 'php:d.m.Y') ?></span>
                </div>
            </div>

            <div class="tender-item__status">
                <div class="status-block <?= $model->status == Tenders::STATUS_DRAFT ? 'status-block--red' : '' ?>">
                    <div class="status-block__text">
                        <?= Tenders::getStatuses($model->status) ?>
                    </div>
                </div>
            </div>

            <div class="tender-item__head-action hidden-md hidden-lg">
                <button class="btn-add">
                    <svg class="icon icon-favorite ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#favorite"></use>
                    </svg>
                </button>
            </div>
        </div>

        <div class="tender-item__body">
            <div class="tender-item__body-left">
                <h2 class="tender-item__title">
                    <?php
                    $url = $model->getViewLink('tender_edit', ['companyUrl' => Yii::$app->request->get('companyUrl')]);
                    if (!empty($typeTenderList)) {
                        switch ($typeTenderList) {
                            case "partners":
                                $offer = Offers::getLastActiveOffer($company->id, $model->id);
                                $url = !empty($offer)
                                    ? \yii\helpers\Url::to(['/dashboard/offer_add', 'tenderId' => $model->id, 'offerId' => $offer->id])
                                    : \yii\helpers\Url::to(['/dashboard/tender_offers_info', 'tenderId' => $model->id]);
                                break;
                            default:
                                $url = \yii\helpers\Url::to(['/dashboard/tender_offers_info', 'tenderId' => $model->id]);
                        }
                    }
                    ?>
                    <a href="<?= $url ?>">
                        <?= $model->title ?>
                    </a>
                </h2>
                <div class="tender-item__desc"><?= $model->object->address->fullAddress ?? '' ?></div>
            </div>
            <div class="tender-item__body-right">
                <?php if ($typeTenderList == 'partners') { ?>
                    <div class="tender-item__condition">
                        <?= Yii::t('app', 'Статус предложения по тендеру')?>: <b><?= !empty($statusOffer) ? $statusOffer : Yii::t('app', 'Нет статуса') ?></b>
                    </div>
                <?php } ?>
                <?php if ($typeTenderList == 'completed') { ?>
                    <div class="tender-item__condition">
                        <?= Yii::t('app', 'Статус предложения по тендеру')?>: <b><?= !empty($statusOffer) ? $statusOffer : Yii::t('app', 'Нет статуса') ?></b>
                    </div>
                <?php } ?>
                <?php if (empty($typeTenderList)) { ?>
                    <div class="tender-item__offer">
                        <span class="_qty">
                            <a href="<?= !empty($offersCount['totalCount']) ? $model->getOffersListLink(['companyUrl' => $companyAuthorTender->url, 'id' => $model->id]) : '#' ?>"><?= Yii::t('app', 'Предложения')?>: <?= $offersCount['totalCount'] ?></a></span>
                        <a href="<?= !empty($offersCount['totalCount']) ? $model->getOffersListLink(['companyUrl' => $companyAuthorTender->url, 'id' => $model->id]) : '#' ?>" class="_new"><?= $offersCount['newCount'] ?> <?= Yii::t('app', 'новых') ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="tender-item__foot">
            <div class="tender-item__options">
                <div class="tender-item__option hidden-lg">
                    <div class="_label"><?= Yii::t('app','Заказчик') ?>:</div>
                    <div class="_value"><?= $companyAuthorTender->title ?? '' ?></div>
                </div>

                <div class="tender-item__option">
                    <div class="_label"><?= Yii::t('app','Вид деятельности') ?>:</div>
                    <div class="_value"><?= implode('; ', \yii\helpers\ArrayHelper::map($companyAuthorTender->getBuilds()->asArray()->all(), 'title', 'title')) ?></div>
                </div>

                <!--
                <div class="tender-item__option">
                    <div class="_label"><?= Yii::t('app','Краткое описание работ') ?>:</div>
                    <div class="_value"><?= Yii::t('app', $model->description) ?></div>
                </div>
                -->

                <?php if (empty($typeTenderList)) { ?>
                    <div class="tender-item__option">
                        <div class="_label"><?= Yii::t('app','Название проекта') ?>:</div>
                        <div class="_value"><?= Yii::t('app', $model->object->title) ?></div>
                    </div>
                <?php } ?>
            </div>

            <?php if (empty($typeTenderList) && !empty($offers)) { ?>
                <div class="tender-item__action">
                    <button class="tender-item__acco js-tender-acco-btn" data-text-open="<?= Yii::t('app','Подробнее о тендере') ?>" data-text-close="<?= Yii::t('app','Скрыть подробности') ?>"></button>
                </div>
            <?php } ?>

            <?php if ($typeTenderList == 'favorites') { ?>
                <div class="tender-item__action">
                    <button class="btn-add btn-add--active js-add-tender-to-favorites" data-url="<?= \yii\helpers\Url::to() ?>" data-tender="<?= $model->id ?>">
                        <img src="/html/app/content/images/icons/favorite.png">
                    </button>
                </div>
            <?php } ?>

            <?php if ($typeTenderList == 'partners' && !empty($tenderOffer)) { ?>
                <div class="tender-item__action">
                    <div class="tender-item__option">
                        <div class="_label"><?= Yii::t('app','Ваше предложение') ?></div>
                        <div class="_value">
                            <b>
                                <?= $tenderOffer->sum > 0 ? (Yii::$app->formatter->asMoney($tenderOffer->sum) . ' ' . $currency[$model->currency_id]) : '' ?>
                            </b>
                        </div>
                    </div>

                    <?php if ($tenderOffer->sum_discount_request > 0) { ?>
                        <div class="tender-item__option">
                            <div class="_label"><?= Yii::t('app','Запрош. скидка') ?></div>
                            <div class="_value">
                                <b class="text-orange">
                                    <?= Yii::$app->formatter->asMoney($offerForm->calcSumDiscountRequest) . ' ' . ($currency[$model->currency_id] ?? '') ?>
                                </b>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>

        <?php if (!empty($offers)) { ?>
            <div class="tender-item__content js-tender-acco-content">
                <!--<div class="tender-item__content-title">По приглашениям</div>-->
                <div class="tender-item__table">
                    <div class="tender-item__table-head">
                        <div class="tender-item__table-row">
                            <div class="tender-item__table-col _col-1"><?= Yii::t('app','Название компании') ?></div>
                            <div class="tender-item__table-col _col-2"><?= Yii::t('app','Первое предложение') ?></div>
                            <div class="tender-item__table-col _col-3"><?= Yii::t('app','Ваш ответ') ?></div>
                            <div class="tender-item__table-col _col-4"><?= Yii::t('app','Последнее предложение') ?></div>
                            <div class="tender-item__table-col _col-5"></div>
                        </div>
                    </div>

                    <div class="tender-item__table-body">
                        <?php foreach ($offers as $offer) { ?>
                            <div class="tender-item__table-row">
                                <?php $companyName = $offer['companyName']; ?>
                                <div class="tender-item__table-col _col-1"><?= $companyName ?></div>
                                <div class="tender-item__table-col _col-2"><?= Yii::$app->formatter->asMoney($offer['firstOffer']['sum']) ?> руб.</div>
                                <div class="tender-item__table-col _col-3"><?= $offer['answerOffer']['sumDiscount'] > 0 ? (Yii::$app->formatter->asMoney($offer['answerOffer']['sumDiscount']) . ' руб.') : '' ?></div>
                                <div class="tender-item__table-col _col-4"><?= !empty($offer['lastOffer']) ? (Yii::$app->formatter->asMoney($offer['lastOffer']['sum']) . ' руб.') : '' ?></div>
                                <!--
                                <div class="tender-item__table-col _col-5">
                                    <button class="btn-comment">
                                        <svg class="icon icon-comment ">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#comment"/>
                                        </svg>
                                        <span class="_qty">7</span>
                                    </button>
                                </div>
                                -->
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>