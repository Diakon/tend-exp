<?php
use yii\helpers\Html;
use common\modules\tender\models\frontend\EstimatesElements;

/**
 * @var $company \common\modules\company\models\frontend\Company
 * @var $model \common\modules\tender\models\frontend\EstimatesElements
 * @var boolean $canEditTender
 */

$estimatesRubrics = $model->estimatesRubrics ?? null;
?>


<div id="js-estimates-elements-hidden-block" style="display:none;">
    <?= $this->render('_tender_estimates_elements', ['element' => new EstimatesElements()]);?>
</div>


<div class="section-lk">
    <div class="section-lk__head">
        <h1 class="section-lk__title"><?= Yii::t('app', 'Смета работ') ?></h1>
        <div class="section-lk__info">
            <div class="section-lk__label">
                <p>
                    <?= Yii::t('app', '
                    Перечислите работы, которые должны быть проведены подрядчиком.
                    Воспользуйтесь группировкой для структурирования сметы.  
                    ')?>
                </p>
            </div>
        </div>
    </div>
    <div class="section-lk__body">
        <div class="acco-work" id="js-estimates-block-list">
            <?php if (!empty($estimatesRubrics)) { ?>
                <div class="acco-work__main">
                    <?php $numLine = 0; ?>
                    <?php foreach ($estimatesRubrics as $estimatesRubric) { ?>
                        <?php if ($estimatesRubric->depth == 1) { ?>
                            <?php
                                // Если новый родительский узел - закрываю предыдущиего узла теги и обнуляю счетчик строк
                                for ($i = 0; $i < ($numLine-1); ++$i) {
                                    echo '</div></div>';
                                }
                                if ($numLine > 0) {
                                    echo '</div>';
                                }
                                $numLine = 0;
                            ?>
                            <div class="acco-work__item" id="js-estimates-rubric-block-item-<?= $estimatesRubric->id ?>">
                                <div class="acco-work__head js-acco-work">
                                    <div class="js-estimates-rubric-title-<?= $estimatesRubric->id ?>"><?= $estimatesRubric->title ?></div>
                                    <div class="acco-work__title acco-work__form">
                                        <input style="display: none" type="text" class="js-estimates-rubric-edit js-estimates-rubric-edit-title-<?= $estimatesRubric->id ?> form-control" data-id="<?= $estimatesRubric->id ?>" value="<?= $estimatesRubric->title ?>">
                                    </div>
                                    <?= $this->render('_tender_estimates_comments', ['model' => $estimatesRubric, 'id' => $estimatesRubric->id]);?>
                                    <div class="acco-work__action">
                                        <button class="_btn _move" type="button">
                                            <svg class="icon icon-move ">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#move"/>
                                            </svg>
                                        </button>
                                        <div class="option-menu js-option">
                                            <div class="option-menu__head">
                                                <button class="option-menu__action js-option-open" type="button">
                                                    <svg class="icon icon-more-set ">
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#more-set"/>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div class="option-menu__body">
                                                <?= $this->render('_tender_estimates_rubric_action', ['estimatesRubric' => $estimatesRubric]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <?php } ?>

                        <?php if ($estimatesRubric->depth > 1) { ?>
                                <div class="acco-work__body">
                                    <div class="acco-work__item">
                                        <div class="acco-work__head js-acco-work">
                                            <div class="js-estimates-rubric-title-<?= $estimatesRubric->id ?>"><?= $estimatesRubric->title ?></div>
                                            <div class="acco-work__title acco-work__form">
                                                <input style="display: none" type="text" class="js-estimates-rubric-edit js-estimates-rubric-edit-title-<?= $estimatesRubric->id ?> form-control" data-id="<?= $estimatesRubric->id ?>" value="<?= $estimatesRubric->title ?>">
                                            </div>
                                            <?= $this->render('_tender_estimates_comments', ['model' => $estimatesRubric, 'id' => $estimatesRubric->id]);?>
                                            <div class="acco-work__action">
                                                <div class="option-menu js-option">
                                                    <div class="option-menu__head">
                                                        <button class="option-menu__action js-option-open" type="button">
                                                            <svg class="icon icon-more-set ">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#more-set"/>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="option-menu__body">
                                                        <?= $this->render('_tender_estimates_rubric_action', ['estimatesRubric' => $estimatesRubric]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="acco-work__body">
                                            <div class="acco-work__form" id="js-estimates-elements-block-rubrics-<?= $estimatesRubric->id ?>">
                                                <?php if (!empty($estimatesRubric->elements)) { ?>
                                                    <?php foreach ($estimatesRubric->elements as $element) { ?>
                                                        <div class="acco-work__form-item" id="js-estimates-elements-block-id-<?= $element->id ?>">
                                                            <?= $this->render('_tender_estimates_elements', ['element' => $element]);?>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                        <?php } ?>
                        <?php ++$numLine; ?>
                    <?php } ?>
                    <?php
                        // Закрываю теги для последнего выведеного узла
                        for ($i = 0; $i < $numLine-1; ++$i) {
                            echo '</div></div>';
                        }
                        echo '</div>';
                    ?>
                </div>
            <?php } ?>


            <?php if ($canEditTender) { ?>
                <div class="acco-work__footer">
                    <div class="acco-work__footer-add">
                        <button type="button" class="btn btn-primary js-edit-estimates-rubric" data-is-new-tender="<?= $model->isNewRecord ? 'yes' : 'no' ?>" data-action="addParent">
                            <?= Yii::t('app', 'ДОБАВИТЬ НАЗВАНИЕ РАБОТ') ?>
                        </button>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="popup popup--comment js-tender-estimates-add-comment-block" data-popup-id="add-comment" id="add-comment" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title"><?= Yii::t('app', 'Оставить комментарий') ?></div>
            <div class="popup__body">
                <div class="popup-comment">
                    <div class="popup-comment__message">
                        <div class="form-group   ">
                            <textarea class="form-control js-estimates-comment-input"></textarea>
                        </div>
                        <?= Html::hiddenInput('commentRubricId', null, ['class' => '_hidden-input-id js-comment-rubric-id-hidden-input']) ?>
                    </div>
                </div>
            </div>

            <div class="popup__footer">
                <div class="popup__action">
                    <button class="btn btn-primary js-add-estimates-comment-btn" type="submit" data-url="<?= \yii\helpers\Url::to() ?>"><?= Yii::t('app', 'ОТПРАВИТЬ') ?></button>
                    <button class="btn btn-outline-secondary js-add-estimates-comment-close-btn" type="reset"><?= Yii::t('app', 'ОТМЕНИТЬ') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
