<?php
use common\modules\tender\models\frontend\EstimatesRubrics;

/**
 * @var EstimatesRubrics $estimatesRubric
 */
?>
<ul class="option-menu__list">
    <li class="option-menu__item">
        <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="down" data-id="<?= $estimatesRubric->id ?>">
            <?= Yii::t('app', 'Добавить пункт снизу') ?>
        </button>
    </li>
    <li class="option-menu__item">
        <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="up" data-id="<?= $estimatesRubric->id ?>">
            <?= Yii::t('app', 'Добавить пункт сверху') ?>
        </button>
    </li>
    <?php if ($estimatesRubric->depth < EstimatesRubrics::LEVEL_MAX) { ?>
        <li class="option-menu__item">
            <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="addChildren" data-id="<?= $estimatesRubric->id ?>">
                <?= Yii::t('app', 'Добавить дочерний пункт') ?>
            </button>
        </li>
    <?php } ?>
    <?php if ($estimatesRubric->depth > 1) { ?>
        <li class="option-menu__item">
            <button class="option-menu__link js-add-new-estimates-element" type="button" data-action="addWork" data-rubric="<?= $estimatesRubric->id ?>">
                <?= Yii::t('app', 'Добавить работу') ?>
            </button>
        </li>
    <?php } ?>
    <li class="option-menu__item">
        <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="edit" data-id="<?= $estimatesRubric->id ?>">
            <?= Yii::t('app', 'Изменить') ?>
        </button>
    </li>
    <li class="option-menu__item">
        <button class="option-menu__link" type="button" data-popup="add-comment" data-id="<?= $estimatesRubric->id ?>">
            <?= Yii::t('app', 'Добавить комментарий') ?>
        </button>
    </li>
    <li class="option-menu__item">
        <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="delete" data-id="<?= $estimatesRubric->id ?>">
            <?= Yii::t('app', 'Удалить') ?>
        </button>
    </li>
</ul>