<?php

use common\modules\tender\models\common\ObjectsCompletedWorks;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/**
 * @var \common\modules\tender\models\frontend\Objects $model
 * @var string                                         $tHash
 */

$completedWorks = $model->completedWorks;

$this->title = Yii::t('app', ($model->isNewRecord ? 'Создание' : 'Редактирование') . ' объекта');
\frontend\widgets\FlashMessagesWidget::widget();
?>

<?php $form = ActiveForm::begin([
    'validateOnBlur' => false,
    //  'enableClientValidation' => false,
    'action' => \yii\helpers\Url::current(),
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'js-form-upload',
        'method' => 'POST'
    ],
    'fieldConfig' => [
        'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
    ],
]); ?>
<div class="section-lk">
    <div class="section-lk__head">
        <h1 class="section-lk__title"><?= Yii::t('app', 'Основная информация') ?></h1>
    </div>
    <div class="section-lk__body">
        <div class="row">
            <div class="col-12">
                <?= $form->field($model, 'title')->textInput()->label(null)->error(['tag' => 'div']) ?>
            </div>
            <div class="col-6">

                <?= $form->field($model, 'activitiesIds',
                    [
                        'template' => '{label}<div class="js-multi-select" 
                            data-placeholder="Выберите значение">{input}</div><div class="help-block__head">{error}</div>'])
                    ->dropDownList(\common\modules\directories\models\common\Activities::findActiveList(),
                        [
                            'multiple' => 'multiple',
                        ])
                ?>

            </div>
            <div class="col-6">
                <?= $form->field($model, 'status_complete_id')
                    ->dropDownList(\yii\helpers\ArrayHelper::map(\common\modules\tender\models\frontend\Objects::getStatusComplete(), 'id', 'title'),
                        ['class' => 'js-dropdown-box js-object-form-status-complete']
                    ) ?>
            </div>
            <div class="col-6">


                <?= $form->field($model, 'typesIds',
                    [
                        'template' => '{label}<div class="js-multi-select" 
                            data-placeholder="Выберите значение">{input}</div><div class="help-block__head">{error}</div>'])
                    ->dropDownList(\common\modules\directories\models\common\Projects::findActiveList(),
                        [
                            'multiple' => 'multiple',
                        ])
                ?>

            </div>
            <div class="col-6">

                <?= $form->field($model, 'functionsIds', [
                        'template' => '{label}<div class="js-multi-select" 
                            data-placeholder="Выберите значение">{input}</div><div class="help-block__head">{error}</div>'])
                    ->dropDownList(\common\modules\directories\models\common\FunctionsCompany::findActiveList(),
                        [
                            'multiple' => 'multiple',
                        ])
                ?>

            </div>
            <div class="col-6">


                <?= $form->field($model, 'worksIds', [
                    'template' => '{label}<div class="js-multi-select" 
                            data-placeholder="Выберите значение">{input}</div><div class="help-block__head">{error}</div>'])
                    ->dropDownList(\common\modules\directories\models\common\Works::findActiveList(),
                        [
                            'multiple' => 'multiple',
                        ])
                ?>
            </div>
            <div class="col-3">

                <?= $form->field($model, 'date_start')->textInput([ 'autocomplete' => 'off', 'class' => 'form-control js-datepicker'])->error(['tag' => 'span']) ?>

            </div>
            <div class="col-3">
                <?= $form->field($model, 'date_end')->textInput(['autocomplete' => 'off','class' => 'form-control js-datepicker js-object-form-date-end'])->error(['tag' => 'span']) ?>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="" class="control-label">Адрес</label>
                    <div class="row">
                        <div class="col-2">
                            <?= $form->field($model, 'addressIndex')
                                ->textInput(['placeholder' => Yii::t('app', 'Индекс')])
                                ->label(null)
                                ->label(false)
                                ->error(['tag' => 'div']) ?>

                        </div>
                        <div class="col-2">
                            <?= $form->field($model, 'addressCountryId')
                                ->dropDownList(\common\models\GeoCountry::getCountryList(), ['class' => 'js-dropdown-box'])
                                ->label(false)
                                ->error(['tag' => 'div']) ?>

                        </div>
                        <div class="col-2">

                            <?= $form->field($model, 'addressCityId', ['options' => ['class' => 'form-group form-group--autocomplete']])
                                ->dropDownList(\common\models\GeoCity::getCityList(), [
                                        'class' => '_select js-autocomplete-select',
                                        'data-empty-text' => 'Ничего не найдено',
                                        'placeholder' => 'Выберите город',

                                ])
                                ->label(false)
                                ->error(['tag' => 'div']) ?>

                        </div>


                        <div class="col-2">
                        <?= $form->field($model, 'addressStreet')->textInput(['placeholder' => Yii::t('app', 'Улица')])->label(false)->error(['tag' => 'div']) ?>
                        </div>
                        <div class="col-3">
                            <div class="row">
                                <div class="col-6">
                                    <?= $form->field($model, 'addressHouse')->textInput(['placeholder' => Yii::t('app', 'Дом')])->label(false)->error(['tag' => 'div']) ?>
                                </div>
                                <div class="col-6">

                                    <?= $form->field($model, 'addressStructure')->textInput(['placeholder' => Yii::t('app', 'Строение')])->label(false)->error(['tag' => 'div']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <?= $form->field($model, 'employer_id')
                                ->dropDownList(['Выбрать'] + \common\modules\company\models\frontend\Company::getCompanyList(),
                                    ['class' => 'js-dropdown-box']
                                )->label( Yii::t('app', 'Выберите заказчика')) ?>

                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'employerName')->textInput()->error(['tag' => 'div'])->label(Yii::t('app', 'Или создайте нового заказчика')) ?>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-12">
                <p>Выполненные работы</p>

                        <!-- Скрытый блок для JS клонирования -->
                        <div class="js-completed-works-block-hidden-block" style="display:none;">
                            <?= $this->render('_object_edit_completed_works', [
                                'model' => $model,
                                'completedWorks' => ['keyCloneId' => new ObjectsCompletedWorks()],
                            ]); ?>
                        </div>

                        <div class="row js-completed-works-block">
                            <?= $this->render('_object_edit_completed_works', [
                                'model' => $model,
                                'completedWorks' => $completedWorks,
                            ]); ?>
                        </div>
                        <?= Html::hiddenInput('countCompletedWorks', count($completedWorks), [
                            'class' => 'js-count-completed-works-hidden-input',
                        ]) ?>

            </div>
            <div class="col-1">
            <div class="row">

                    <button class="btn btn-w100 btn-outline-secondary btn-plus js-completed-works-add-btn" type="button">+</button>
                </div>
            </div>
            <div class="col-12">

                <?= $form->field($model, 'description')
                    ->textarea(['class' => 'form-control'])
                    ->error(['tag' => 'div']) ?>

            </div>
        </div>
    </div>
</div>


<?=  \frontend\modules\upload\widgets\UploadFileFormWidget::widget([
    'model' => $model,
    'modelId' => $model->isNewRecord ? $tHash : $model->id,
]); ?>

<div class="section-lk">
    <div class="section-lk__head">
        <h1 class="section-lk__title">Видео</h1>
    </div>
    <div class="section-lk__body">
        <div class="form-group">
            <label for="" class="control-label">Укажите ссылку на видео</label>
            <div class="row">
                <div class="col-6 js-links-block">
                    <?php $i = 0; ?>
                    <?php foreach ($model->links as $data) { ?>
                        <div class="form-group" style="margin-top: 10px;">
                            <?= $form->field($model, 'videoLinks[' . $i . ']', ['template' => '{input}'])->textInput([
                                'value' => $data->link,
                                'class' => 'form-control js-object-links-' . $i
                            ])->label(false); ?>
                            <?php ++$i ?>
                        </div>
                    <?php } ?>
                    <div class="form-group" style="margin-top: 10px;">
                        <?= $form->field($model, 'videoLinks[' . $i . ']', ['template' => '{input}'])->textInput(['class' => 'form-control js-object-links-' . $i])->label(false) ?>
                    </div>
                </div>
                <div class="col">
                    <a href="#" class="btn btn-w114 btn-outline-secondary btn-plus js-object-links-bnt-add">+</a>
                </div>
                <div class="js-object-links-count" style="display: none;">
                    <div class="js-object-links-input">
                        <div class="form-group" style="margin-top: 10px;">
                            <?= Html::input('text', $model::classNameShort() . '[videoLinks][param-links-input]', '', ['class' => 'form-control']) ?>
                        </div>
                    </div>
                    <?= Html::hiddenInput('objectLinksCount', $i, ['class' => 'js-object-links-count-input']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-lk__action">

    <?= $form->field($model, 'show_portfolio')->checkbox()->label(' <span class="check-label">Добавить объект в портфолио</span>', ['class' => 'check-box']) ?>
    <?= $form->field($model, 'status')->checkbox()->label(' <span class="check-label">Опубликовать</span>', ['class' => 'check-box']) ?>

</div>

<div class="section-lk__action section-lk__action--flex">
    <button class="btn btn-primary" type="submit"><?= Yii::t('app', $model->isNewRecord ? 'ДОБАВИТЬ ОБЪЕКТ' : 'СОХРАНИТЬ')?></button>
    <a class="btn btn-outline-secondary btn-white-bg "
       href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/objects', 'companyUrl' => Yii::$app->user->getActiveCompanyData()['url']])?>"
      >ОТМЕНИТЬ</a>
</div>



<?php ActiveForm::end(); ?>




