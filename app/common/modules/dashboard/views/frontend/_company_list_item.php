<?php
use yii\helpers\Html;
use common\modules\company\models\frontend\Company;
use common\components\Thumbnail;

/**
 * @var Company $model
 */


$noPhoto = Yii::$app->params['noPhotoUrl'];
?>
<p><b><?= Html::a($model->title, ['/dashboard/dashboard/company_info',  'url' => $model->url]) ?></b></p>
<?= Thumbnail::img([
    'image' => $model->photo,
    'height' => 200,
    'noImage' => $noPhoto,
    'options' => ['alt' => $model->title]
]); ?>
<p><?= Yii::t('app', 'Юридический адрес') ?>: <?= $model->actualAddress->fullAddress ?? '' ?></p>
<?php if ($model->address_same != Company::ADDRESS_SAME_YES) { ?>
    <p><?= Yii::t('app', 'Фактический адрес') ?>: <?= $model->legalAddress->fullAddress ?? '' ?></p>
<?php } ?>

<?php if (Yii::$app->user->can('clienteditCompany', ['companyUrl' => $model->url])) { ?>
    <?= Html::a('Редактировать', ['/dashboard/dashboard/company_edit',  'companyUrl' => $model->url]) ?>
<?php } ?>
