<?php
use common\modules\offers\models\forms\OfferForm;
use yii\helpers\ArrayHelper;
use frontend\widgets\EstimatesList;
use common\modules\tender\models\frontend\Tenders;
use components\ActiveForm;
use yii\helpers\Html;

/**
 * @var \common\modules\offers\models\forms\OfferForm $model
 * @var string $tHash
 * @var $offer \common\modules\offers\models\frontend\Offers
 */
\frontend\widgets\FlashMessagesWidget::widget();
?>
<main class="main">
    <div class="main__head">
        <h1 class="main__title"><?= empty($model->offerId) ? Yii::t('app', 'Добавление предложения') : Yii::t('app', 'Редактирование предложения') ?></h1>
    </div>
    <div class="main__body">
        <?php ActiveForm::begin([
            'validateOnBlur' => false,
            'errorCssClass' => 'form-error',
            'options' => [
                'id' => 'js-add-offer-form'
            ]
        ]); ?>

            <div class="section-lk">
                <div class="section-lk__head section-lk__head--edit">
                    <div class="main__label main__label--small"><?= Tenders::getModes($model->tender->mode) . ' ' . Yii::t('app', 'тендер') ?> № <?= $model->tender->id ?>. <?= Tenders::getTypes($model->tender->type) ?></div>
                    <h1 class="section-lk__title"><?= $model->tender->title ?></h1>
                </div>

                <div class="section-lk__body">
                    <div class="participate-block">
                        <div class="participate-block__inner">
                            <div class="participate-block__left">
                                <ul class="info-list">
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Тип объекта') ?></div>
                                        <div class="info-list__value"><?= implode(', ', ArrayHelper::map($model->tender->object->types, 'id', 'title')) ?></div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Начало работ') ?></div>
                                        <div class="info-list__value"><?= Yii::$app->formatter->asDate($model->tender->date_start, 'php:d.m.Y') ?></div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Вид работ') ?></div>
                                        <div class="info-list__value"><?= implode(', ', ArrayHelper::map($model->tender->object->works, 'id', 'title')) ?></div>
                                    </li>
                                    <!--
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Краткое описание') ?></div>
                                        <div class="info-list__value">
                                            <?= $model->tender->description ?>
                                        </div>
                                    </li>
                                    -->
                                </ul>
                            </div>
                            <!--
                            <div class="participate-block__right">
                                <div class="participate-block__action">
                                    <label class="btn btn-primary">
                                        <input type="file" hidden>
                                        <span>ЗАГРУЗИТЬ РАСЧЕТ</span>
                                    </label><br>
                                    <a href="#" download="" class="btn btn-outline-secondary">СКАЧАТЬ СМЕТУ</a>
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
            </div>

            <?= EstimatesList::widget([
                'typeEstimates' => EstimatesList::TYPE_ADD_OFFER,
                'offerForm' => $model,
                'tender' => $model->tender,
            ]); ?>

            <div class="section-lk">
                <div class="section-lk__head section-lk__head--edit">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Условия') ?></h1>
                    <div class="section-lk__label"><?= Yii::t('app', 'Обозначьте условия предоставления вами выше указанных услуг.') ?></div>
                </div>
                <div class="section-lk__body">
                    <div class="conditions-block conditions-block--edit">
                        <div class="conditions-block__inner">
                            <div class="conditions-block__left">
                                <ul class="info-list">
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Срок работы (мес.)') ?></div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Html::activeTextInput($model, 'timeWork', ['class' => 'form-control']) ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Срок гарантии (мес.)') ?></div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Html::activeTextInput($model, 'timeWarranty', ['class' => 'form-control']) ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Банковская гарантия для аванса') ?></div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Html::activeDropDownList($model, 'bankPrepaymentWarranty', $model::getWarrantyList(), [
                                                    'class' => 'js-dropdown-box'
                                                ]) ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Банковская гарантия для гарантийного периода') ?></div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Html::activeDropDownList($model, 'bankPeriodWarranty', $model::getWarrantyList(), [
                                                    'class' => 'js-dropdown-box'
                                                ]) ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="info-list__item">
                                        <div class="info-list__label"><?= Yii::t('app', 'Банковская гарантия для выполнения обязательств') ?></div>
                                        <div class="info-list__value">
                                            <div class="form-group">
                                                <?= Html::activeDropDownList($model, 'bankObligationWarranty', $model::getWarrantyList(), [
                                                    'class' => 'js-dropdown-box'
                                                ]) ?>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="conditions-block__right">
                                <div class="conditions-block__note">
                                    <div class="_title"><?= Yii::t('app', 'Порядок оплаты') ?></div>
                                    <div class="form-group">
                                        <?= Html::activeTextarea($model, 'description', ['class' => 'form-control', 'style' => 'width: 80%']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?=  \frontend\modules\upload\widgets\UploadFileFormWidget::widget([
                'model' => $offer,
                'modelId' => $offer->isNewRecord ? $tHash : $offer->id,
                'type' => \frontend\modules\upload\widgets\UploadFileFormWidget::TYPE_DOCUMENTS
            ]); ?>

            <div class="section-lk__action section-lk__action--flex">
                <button class="btn btn-primary" type="submit">
                    <?= empty($model->offerId) ? Yii::t('app', 'ДОБАВИТЬ ПРЕДЛОЖЕНИЕ') : Yii::t('app', 'СКОРРЕКТИРОВАТЬ ПРЕДЛОЖЕНИЕ') ?>
                </button>
                <!--<button class="btn btn-outline-secondary btn-white-bg" type="submit">СОХРАНИТЬ ЧЕРНОВИК</button>-->
                <a class="btn btn-outline-secondary btn-white-bg btn-ml" href="<?= \yii\helpers\Url::to(['/dashboard/tenders', 'type' => 'partners']) ?>">
                    <?= Yii::t('app', 'ОТМЕНИТЬ') ?>
                </a>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</main>