<?php
use frontend\widgets\EstimatesList;
use yii\helpers\ArrayHelper;
use common\modules\tender\models\frontend\Tenders;
/**
 * @var Tenders $tender
 * @var string $companyUrl
 * @var $offer \common\modules\offers\models\frontend\Offers
 * @var \common\modules\company\models\common\Company $companyUser
 */
$types = ArrayHelper::map($tender->object->types, 'id', 'title');
$works = ArrayHelper::map($tender->object->works, 'id', 'title');
$files = $tender->files;
$fileSumSize = 0;
foreach ($files as $file) {
    $fileSumSize += $file->fileSize;
}
// Проверяю наличие архива
$zipFile = file_exists(Yii::getAlias('@root') . $tender->zipFileArchiveLink) ? $tender->zipFileArchiveLink : null;
?>
<div class="main__head">
    <div class="main__info">
        <div class="main__label"><?= Tenders::getModes($tender->mode) ?> <?= Yii::t('app', 'тендер') ?> № <?= $tender->id ?></div>
        <h1 class="main__title"><?= $tender->title ?></h1>
    </div>
    <div class="main__table">
        <ul class="info-list">
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Тип') ?></div>
                <div class="info-list__value"><?= Tenders::getTypes($tender->type) ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Вид') ?></div>
                <div class="info-list__value"><?= Tenders::getModes($tender->mode) ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Этапность') ?></div>
                <div class="info-list__value"><?= Tenders::getStages($tender->stage) ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Срок завершения') ?></div>
                <div class="info-list__value"><?= Yii::$app->formatter->asDate($tender->date_end, 'php:d.m.Y') ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Валюта') ?></div>
                <div class="info-list__value"><?= $tender->currency->title ?></div>
            </li>
        </ul>
    </div>
</div>

<div class="main__body">
    <div class="tabs-block tabs-block--link">
        <div class="tabs-block__inner">
            <div class="tabs-block__head">
                <div class="tabs-block__list">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/tender_offers_info', 'tenderId' => $tender->id, 'companyUrl' => $companyUrl]) ?>" class="tabs-block__btn active">
                        <?= Yii::t('app', 'УСЛОВИЯ') ?>
                    </a>
                    <?php if (!empty($companyUser->id) && $tender->company->id == $companyUser->id) { ?>
                        <a href="<?= \yii\helpers\Url::to(['/dashboard/tender_offers_list', 'tenderId' => $tender->id, 'companyUrl' => $companyUrl]) ?>" class="tabs-block__btn">
                            <?= Yii::t('app', 'ПРЕДЛОЖЕНИЯ') ?>
                        </a>
                    <?php } ?>
                </div>

                <?php if (!empty($companyUser->id) && $tender->company->id == $companyUser->id) { ?>
                    <div class="tabs-block__action">
                        <a class="btn-edit" href="<?= $tender->getViewLink('tender_edit', ['companyUrl' => $companyUrl]) ?>">
                            <svg class="icon icon-edit ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#edit"/>
                            </svg>
                            <span><?= Yii::t('app', 'РЕДАКТИРОВАТЬ УСЛОВИЯ') ?></span></a>
                    </div>
                <?php } ?>

            </div>

            <div class="tabs-block__body">
                <div class="section-lk">
                    <div class="section-lk__head">
                        <h1 class="section-lk__title"><?= Yii::t('app', 'Информация об объекте') ?></h1>
                    </div>

                    <div class="section-lk__body">
                        <div class="participate-block">
                            <div class="participate-block__inner">
                                <div class="participate-block__left">
                                    <ul class="info-list">
                                        <li class="info-list__item">
                                            <div class="info-list__label"><?= Yii::t('app', 'Название объекта') ?></div>
                                            <div class="info-list__value">
                                                <?= $tender->object->title ?>
                                            </div>
                                        </li>
                                        <li class="info-list__item">
                                            <div class="info-list__label"><?= Yii::t('app', 'Тип объекта') ?></div>
                                            <div class="info-list__value">
                                                <?= implode(", ", $types) ?>
                                            </div>
                                        </li>
                                        <li class="info-list__item">
                                            <div class="info-list__label"><?= Yii::t('app', 'Адрес') ?></div>
                                            <div class="info-list__value">
                                                <?= $tender->object->address->fullAddress ?? '' ?>
                                            </div>
                                        </li>
                                        <li class="info-list__item">
                                            <div class="info-list__label"><?= Yii::t('app', 'Вид работ')?></div>
                                            <div class="info-list__value">
                                                <?= implode(", ", $works) ?>
                                            </div>
                                        </li>
                                        <li class="info-list__item">
                                            <div class="info-list__label"><?= Yii::t('app', 'Начало работ')?></div>
                                            <div class="info-list__value"><?= Yii::$app->formatter->asDate($tender->date_start, 'php:d.m.Y') ?></div>
                                        </li>
                                        <li class="info-list__item">
                                            <div class="info-list__label"><?= Yii::t('app', 'Краткое описание') ?></div>
                                            <div class="info-list__value">
                                                <?= $tender->description ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="participate-block__right">
                                    <div class="participate-block__action">
                                        <?php if (Yii::$app->user->canGetTender($tender)) { ?>
                                            <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/offer_add',  'tenderId' => $tender->id, 'companyUrl' => $companyUser->url, 'tHash' => $offer->getTHash()]) ?>"
                                               class="btn btn-primary">УЧАСТВОВАТЬ В ТЕНДЕРЕ</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?= EstimatesList::widget([
                    'typeEstimates' => EstimatesList::TYPE_VIEW_OFFER,
                    'offerForm' => null,
                    'tender' => $tender,
                ]); ?>

                <div class="section-lk">
                    <div class="section-lk__head">
                        <h1 class="section-lk__title">Документы</h1>
                        <div class="section-lk__label">
                            <span class="_item">Общий объем файлов <?= number_format($fileSumSize / 1000000, 2, '.', ''); ?> мб (максимум 1 гб)</span>
                            <span class="_item">

                                <?php if (!empty($zipFile)) { ?>
                                    <a class="btn-upload" href="<?= $zipFile ?>">
                                    <svg class="icon icon-download ">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#download"/>
                                    </svg>
                                    <span><?= Yii::t('app', 'Скачать все файлы') ?></span>
                                </a>
                                <?php } ?>

                            </span>
                        </div>
                    </div>
                    <div class="section-lk__body">
                        <div class="doc-load">
                            <div class="doc-load__inner">

                                <div class="doc-load__body">
                                    <div class="doc-load__row js-files-container">
                                        <?php
                                        if (!empty($files)) {
                                            /** @var \common\models\EntityFile[] $files */
                                            foreach ($files as $file) { ?>
                                                <div class="col-6 js-delete-file-url js-delete-file-block-<?= $file->id ?>">
                                                    <div class="doc-item">
                                                        <div class="doc-item__inner">
                                                            <div class="doc-item__title">
                                                                <a href="/upload/tenders/files/<?= $file->modelId ?>/<?= $file->fileName ?>" class="doc-item__link"><?= $file->title ?>.<?= $file->fileExt ?></a>
                                                            </div>
                                                            <div class="doc-item__size"><?=  number_format($file->fileSize / 1000000, 2, '.', ''); ?> мб</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>