<?php

use common\components\ClassMap;
use common\modules\tender\models\frontend\Tenders;
use yii\helpers\ArrayHelper;
use common\modules\offers\models\frontend\Offers;
use yii\helpers\Url;

/**
 * @var \common\modules\offers\models\frontend\Offers $model
 * @var Tenders $tender
 * @var $offersData Offers
 * @var $issetAddWorks boolean
 * @var $estimatesOfferData array
 * @var $currency array
 * @var $bestOfferId integer
 * @var $companyUrl string
 */
$types = ArrayHelper::map($tender->object->types, 'id', 'title');
$works = ArrayHelper::map($tender->object->works, 'id', 'title');
// Проверяю наличие архива
$zipFile = file_exists(Yii::getAlias('@root') . $tender->zipFileArchiveLink) ? $tender->zipFileArchiveLink : null;
\frontend\widgets\FlashMessagesWidget::widget();
?>
<main class="main compare compare--big js-compare-page">
    <div class="main__head">
        <div class="main__info">
            <div class="main__label"><?= Tenders::getModes($tender->mode) ?> <?= Yii::t('app', 'тендер') ?>
                № <?= $tender->id ?>. <?= Tenders::getStatuses($tender->status) ?></div>
            <h1 class="main__title"><?= $tender->title ?></h1>

            <div class="info-list info-list--vertical js-info-list">
                <button class="info-list__more-btn js-info-more-btn" data-open="<?= Yii::t('app', 'Развернуть подробности о тендере') ?>" data-close="<?= Yii::t('app', 'Скрыть подробности о тендере') ?>"></button>
                <ul class="info-list__more-box js-info-more-box">
                    <li class="info-list__item">
                        <div class="info-list__label"><?= Yii::t('app', 'Тип') ?></div>
                        <div class="info-list__value">
                            <div class="info-list__desc">
                                <p><?= Tenders::getTypes($tender->type) ?></p>
                            </div>
                        </div>
                    </li>
                    <li class="info-list__item">
                        <div class="info-list__label"><?= Yii::t('app', 'Вид работ') ?></div>
                        <div class="info-list__value"><?= Tenders::getModes($tender->mode) ?></div>
                    </li>

                    <li class="info-list__item">
                        <div class="info-list__label"><?= Yii::t('app', 'Старт работ') ?></div>
                        <div class="info-list__value"><?= Yii::$app->formatter->asDate($tender->date_start) ?></div>
                    </li>

                    <li class="info-list__item">
                        <div class="info-list__label"><?= Yii::t('app', 'Краткое описание') ?></div>
                        <div class="info-list__value">
                            <p><?= $tender->description ?></p>
                        </div>
                    </li>
                </ul>
            </div>

            <?php /*
            <?php if (!empty($zipFile)) { ?>
                <a href="<?= $zipFile ?>" download="#" class="btn btn-download">
                    <svg class="icon icon-download-arrow ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink"  xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#download-arrow"/>
                    </svg>
                    <?= Yii::t('app', 'СКАЧАТЬ УСЛОВИЯ ТЕНДЕРА') ?>
                </a>
            <? } ?>
            <?php */ ?>
        </div>

        <div class="compare__offers">
            <div class="compare-slider">
                <div class="compare-slider__common">
                    <div class="compare-slider__main">
                        <ul class="compare-slider__list js-compare-main-slider">
                            <?php foreach ($offersData as $key => $offerData)  { ?>
                                <?php
                                $compareOptions['data-id'] = $offerData->id;
                                $compareOptions['data-tender'] = $offerData->tender_id;
                                $compareOptions['data-url'] = Url::to(['/uwm/compare/index', 'id' => $offerData->id]);
                                $compareOptions['data-model'] = $offerData::classNameShort();
                                ?>
                                <li class="compare-slider__item" data-compare-item-id="<?= $key + 1 ?>">
                                    <div class="offer-item-compare ">
                                        <div class="<?= $bestOfferId == $offerData->id ? 'offer-item--best' : '' ?> offer-item-compare__inner">
                                            <h2 class="offer-item-compare__title">
                                                <a href="<?= \yii\helpers\Url::to(['/company/' . $offerData->company->url]) ?>"><?= $offerData->company->title ?></a>
                                            </h2>

                                            <?php if ($bestOfferId == $offerData->id) { ?>
                                                <div class="offer-item__label">
                                                    <span class="_text"><?= Yii::t('app', 'ЛУЧШЕЕ ПРЕДЛОЖЕНИЕ') ?></span>
                                                </div>
                                            <?php } ?>

                                            <div class="offer-item-compare__link">
                                                <a class="_link js-compare-contact-info-btn" href="#"><?= Yii::t('app', 'Контактная информация') ?></a>
                                                <ul class="js-compare-contact-info-block" style="display: none">
                                                    <li><?= Yii::t('app', 'Телефон') ?>: <?= $offerData->company->phone ?></li>
                                                    <li><?= Yii::t('app', 'Адрес') ?>: <?= $offerData->company->actualAddress->fullAddress ?></li>
                                                </ul>
                                            </div>

                                            <div class="offer-item-compare__body">
                                                <?php /*
                                                <div class="offer-item-compare__body-left">
                                                    <div class="offer-item-compare__review">
                                                        <div class="offer-item-compare__rating">
                                                            <span class="_value">5.0</span>
                                                            <span class="_qty">по профилю</span>
                                                        </div>

                                                        <div class="offer-item-compare__rating offer-item-compare__rating--orange">
                                                            <span class="_value">4.0</span>
                                                            <span class="_qty">по отзывам</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                */ ?>
                                                <div class="offer-item-compare__body-right">
                                                    <button class="btn btn-reset js-compare-item-hide">
                                                        <svg class="icon icon-hide ">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                 xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#hide"/>
                                                        </svg>
                                                        <?= Yii::t('app', 'ВРЕМЕННО СКРЫТЬ') ?>
                                                    </button>
                                                    <button class="btn btn-reset js-compare-item-remove js-compare"
                                                            data-id="<?= $compareOptions['data-id'] ?>"
                                                            data-url="<?= $compareOptions['data-url'] ?>"
                                                            data-model="<?= $compareOptions['data-model'] ?>"
                                                            data-tender="<?= $compareOptions['data-tender'] ?>"

                                                    >
                                                        <svg class="icon icon-remove ">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                 xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#remove"/>
                                                        </svg>
                                                        <?= Yii::t('app', 'УДАЛИТЬ ИЗ СРАВНЕНИЯ') ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>              <div class="compare-params">
                                        <div class="compare-params__body">
                                            <div class="compare-params__option">
                                                <div class="compare-params__option-item">
                                                    <div class="_label"><span><?= Yii::t('app', 'Итоговая стоимость') ?></span></div>
                                                    <div class="_value"><span class="_bold"><?= Yii::$app->formatter->asMoney($offerData->sum) ?> <?= $currency[$tender->currency_id] ?? '' ?></span></div>
                                                </div>
                                                <div class="compare-params__option-item">
                                                    <div class="_label"><span><?= Yii::t('app', 'Срок работы (месяцев)') ?></span></div>
                                                    <div class="_value"><?= $offerData->time_work ?></div>
                                                </div>

                                                <button class="_btn" data-popup="all-params"><?= Yii::t('app', 'ПОКАЗАТЬ ВСЕ ПАРАМЕТРЫ') ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="compare-slider__action">
                                        <button
                                                onclick="$('.js-offer-id-hidden-input').val(<?= $offerData->id ?>); $('.js-offer-company-name').empty().append('<?= $offerData->company->title ?>');"
                                                class="btn btn-download"
                                                data-popup="winner">
                                            <?= Yii::t('app', 'ВЫБРАТЬ') ?>
                                        </button>
                                    </div>
                                </li>

                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main__body">
        <div class="compare-table">
            <div class="compare-table__head">
                <h2 class="compare-table__title"><?= Yii::t('app', 'Смета работ') ?></h2>
            </div>
            <div class="compare-table__body js-acco-work">
                <div class="compare-table__common">
                    <div class="table-work">
                        <div class="table-work__inner">
                            <div class="table-work__head">
                                <div class="table-work__row">
                                    <div class="table-work__col _col-1"><?= Yii::t('app', 'Наименование работ') ?></div>
                                    <div class="table-work__col _col-2"><?= Yii::t('app', 'Кол-во') ?></div>
                                    <div class="table-work__col _col-3"><?= Yii::t('app', 'Ед. изм.') ?></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="compare-table__result">
                        <div class="acco-work acco-work--compare">
                            <div class="acco-work__main">
                                <?php foreach ($tender->estimatesRubrics as $rubrics) { ?>
                                    <?php $commentsCount = $rubrics->getComments()->count(); ?>
                                    <div class="acco-work__item js-acco-work-item" data-id="<?= $rubrics->id ?>" data-parent-id="<?= $rubrics->parent_id ?>" data-depth="<?= $rubrics->depth ?>">
                                        <div class="acco-work__head">
                                            <div class="acco-work__title"><?= $rubrics->title ?></div>
                                            <?php if (!empty($commentsCount)) { ?>
                                                <div class="acco-work__comment">
                                                    <button class="btn-comment" type="button" data-popup="comment-list-<?= $rubrics->id ?>">
                                                        <svg class="icon icon-comment ">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#comment"/>
                                                        </svg>
                                                        <span class="_qty"><?= $commentsCount ?></span>
                                                    </button>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php if (!empty($rubrics->elements)) { ?>
                                            <div class="acco-work__body">
                                                <div class="acco-work__form">
                                                    <div class="table-work">
                                                        <div class="table-work__inner">
                                                            <div class="table-work__body">
                                                                <?php foreach ($rubrics->elements as $element) { ?>
                                                                    <div class="table-work__row" data-work-row="work-<?= $element->id ?>e">

                                                                        <div class="table-work__col _col-1"><?= $element->title ?></div>
                                                                        <div class="table-work__col _col-2"><?= $element->count ?></div>
                                                                        <div class="table-work__col _col-3"><?= $units[$element->unit_id] ?? '' ?></div>


                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                <!-- доп. работы -->
                                <?php if ($issetAddWorks) { ?>
                                    <div class="acco-work__item js-acco-work-item " data-id="1adw" data-parent-id="0" data-depth="1">
                                        <div class="acco-work__head">
                                            <div class="acco-work__title"><?= Yii::t('app', 'Дополнительные работы') ?></div>
                                        </div>
                                        <div class="acco-work__body">
                                            <div class="acco-work__form">
                                                <div class="table-work">
                                                    <div class="table-work__inner">
                                                        <div class="table-work__body">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="compare-table__main">
                    <ul class="compare-table__slider js-compare-main-table">
                        <?php foreach ($offersData as $key => $offerData)  { ?>
                            <li class="compare-table__item" data-compare-item-id="<?= $key + 1 ?>">
                                <div class="compare-table__item-wrap">
                                    <div class="table-work">
                                        <div class="table-work__inner">
                                            <div class="table-work__head">
                                                <div class="table-work__row">
                                                    <div class="table-work__col _col-4"><?= Yii::t('app', 'Стоимость работ за ед.') ?>, руб.</div>
                                                    <div class="table-work__col _col-5"><?= Yii::t('app', 'Стоимость материала за ед.') ?>, руб.</div>
                                                    <div class="table-work__col _col-6"><?= Yii::t('app', 'Общая стоимость за ед.') ?>, руб.</div>
                                                    <div class="table-work__col _col-7"><?= Yii::t('app', 'Сумма, без НДС,') ?> руб.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="compare-table__result">


                                        <div class="acco-work acco-work--compare acco-work--body">
                                            <div class="acco-work__main">
                                                <?php foreach ($tender->estimatesRubrics as $rubrics) { ?>

                                                    <div class="acco-work__item js-acco-work-item" data-id="<?= $rubrics->id ?>" data-depth="<?= $rubrics->depth ?>" data-parent-id="<?= $rubrics->parent_id ?>">
                                                        <div class="acco-work__head">
                                                            <?php if ($rubrics->depth == 1) { ?>
                                                                <span class="js-fvtooltip-switch" tabindex="0"></span>
                                                            <?php } ?>
                                                            <div class="acco-work__title"><?= $rubrics->title ?></div>
                                                            <div class="acco-work__action"></div>
                                                            <div class="acco-work__sale">
                                                                <span class="_current-price js-offer-compare-sum-estimates-block-btn"
                                                                >
                                                                    <?= !empty($estimatesOfferData[$offerData->id]['rubrics'][$rubrics->id]['noDiscount'])
                                                                        ? (Yii::$app->formatter->asMoney($estimatesOfferData[$offerData->id]['rubrics'][$rubrics->id]['noDiscount']) . ' ' . $currency[$tender->currency_id] ?? '')
                                                                        : ''
                                                                    ?>
                                                                </span>

                                                            </div>
                                                        </div>
                                                        <?php if (!empty($rubrics->elements)) { ?>
                                                            <div class="acco-work__body">
                                                                <div class="acco-work__form">
                                                                    <div class="table-work">
                                                                        <div class="table-work__inner">
                                                                            <div class="table-work__body">
                                                                                <?php foreach ($rubrics->elements as $element) { ?>
                                                                                    <div class="table-work__row" data-work-row="work-<?= $element->id ?>e">
                                                                                        <div class="table-work__col _col-4">
                                                                                            <?= !empty($estimatesOfferData[$offerData->id]['elements'][$element->id]['priceWork'])
                                                                                                ? Yii::$app->formatter->asMoney($estimatesOfferData[$offerData->id]['elements'][$element->id]['priceWork'])
                                                                                                : ''
                                                                                            ?>
                                                                                        </div>
                                                                                        <div class="table-work__col _col-5">
                                                                                            <?= !empty($estimatesOfferData[$offerData->id]['elements'][$element->id]['priceMaterial'])
                                                                                                ? Yii::$app->formatter->asMoney($estimatesOfferData[$offerData->id]['elements'][$element->id]['priceMaterial'])
                                                                                                : ''
                                                                                            ?>
                                                                                        </div>
                                                                                        <div class="table-work__col _col-6">
                                                                                            <?= !empty($estimatesOfferData[$offerData->id]['elements'][$element->id]['sumPrice'])
                                                                                                ? Yii::$app->formatter->asMoney($estimatesOfferData[$offerData->id]['elements'][$element->id]['sumPrice'])
                                                                                                : ''
                                                                                            ?>
                                                                                        </div>
                                                                                        <div class="table-work__col _col-7">
                                                                                            <div class="table-work__sale">
                                                                                                <div class="_current-price">
                                                                                                    <?= !empty($estimatesOfferData[$offerData->id]['elements'][$element->id]['noDiscount'])
                                                                                                        ? Yii::$app->formatter->asMoney($estimatesOfferData[$offerData->id]['elements'][$element->id]['noDiscount'])
                                                                                                        : ''
                                                                                                    ?>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>

                                                <?php } ?>
                                                <!-- доп. работы -->
                                                <?php if ($issetAddWorks) { ?>

                                                    <div class="acco-work__item js-acco-work-item" data-id="1adw" data-depth="1" data-parent-id="0">
                                                        <div class="acco-work__head">
                                                            <div class="acco-work__title"><?= Yii::t('app', 'Дополнительные работы') ?></div>
                                                            <div class="acco-work__action"></div>
                                                            <div class="acco-work__sale">
                                                                <span class="_current-price">
                                                                    <?php $sum = 0; ?>
                                                                    <?php foreach ($estimatesOfferData as $offerId => $estimatesData) { ?>
                                                                        <?php if (!empty($estimatesData['addWorks']) && $offerId == $offerData->id) {
                                                                            foreach ($estimatesData['addWorks'] as $addWorks) {
                                                                                $sum += $addWorks['noDiscount'];
                                                                            }
                                                                        } ?>
                                                                    <?php } ?>
                                                                    <?= (!empty($sum) ? Yii::$app->formatter->asMoney($sum) : '-') . ' ' . $currency[$tender->currency_id] ?? '' ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="acco-work__body">
                                                            <div class="acco-work__form">
                                                                <div class="table-work">
                                                                    <div class="table-work__inner">
                                                                        <div class="table-work__body">
                                                                            <?php foreach ($estimatesOfferData as $offerId => $estimatesData) { ?>
                                                                                <?php if (!empty($estimatesData['addWorks'])) { ?>
                                                                                    <?php foreach ($estimatesData['addWorks'] as $addWorks) { ?>
                                                                                        <?php if ($offerId != $offerData->id) {
                                                                                            continue;
                                                                                        } ?>
                                                                                        <div class="table-work__row">
                                                                                            <div class="table-work__col _col-1">
                                                                                                <?= $addWorks['title'] ?>, <?= $addWorks['count'] ?> <?= $units[$addWorks['unit_id']] ?? '' ?>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="table-work__row" data-work-row="add-work-<?= $addWorks['id'] ?>e">
                                                                                            <div class="table-work__col _col-4">
                                                                                                <?= !empty($addWorks['priceWork']) ? Yii::$app->formatter->asMoney($addWorks['priceWork']) : '' ?>
                                                                                            </div>
                                                                                            <div class="table-work__col _col-5">
                                                                                                <?= !empty($addWorks['priceMaterial']) ? Yii::$app->formatter->asMoney($addWorks['priceMaterial']) : '' ?>
                                                                                            </div>
                                                                                            <div class="table-work__col _col-6">
                                                                                                <?= !empty($addWorks['sumPrice']) ? Yii::$app->formatter->asMoney($addWorks['sumPrice']) : '' ?>
                                                                                            </div>
                                                                                            <div class="table-work__col _col-7">
                                                                                                <div class="table-work__sale">
                                                                                                    <div class="_current-price">
                                                                                                        <?= !empty($addWorks['noDiscount']) ? Yii::$app->formatter->asMoney($addWorks['noDiscount']) : '' ?>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="popup popup--compare" data-popup-id="all-params" id="all-params" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <button class="popup__close js-close-wnd" type="button"></button>
            <div class="popup__title"><?= Yii::t('app', 'Все параметры') ?></div>
            <div class="popup__body">
                <div class="compare-slider">
                    <div class="compare-slider__common">
                        <div class="compare-slider__aside-hidden js-compare-popup-aside-hidden"></div>
                        <div class="compare-slider__hidden js-compare-popup-main-hidden"></div>

                        <div class="compare-slider__main">
                            <ul class="compare-slider__list js-compare-popup-main-slider">
                                <?php foreach ($offersData as $key => $offerData)  { ?>
                                    <?php
                                    $compareOptions['data-id'] = $offerData->id;
                                    $compareOptions['data-tender'] = $offerData->tender_id;
                                    $compareOptions['data-url'] = Url::to(['/uwm/compare/index', 'id' => $offerData->id]);
                                    $compareOptions['data-model'] = $offerData::classNameShort();
                                    ?>
                                    <li class="compare-slider__item js-offer-compare-data-<?= $offerData->id ?>" data-compare-item-id="<?= $key + 1 ?>">
                                        <div class="offer-item-compare ">
                                            <div class="offer-item-compare__inner">
                                                <h2 class="offer-item-compare__title">
                                                    <a href="<?= \yii\helpers\Url::to(['/company/' . $offerData->company->url]) ?>">
                                                        <?= $offerData->company->title ?>
                                                    </a>
                                                </h2>

                                                <div class="offer-item-compare__link">
                                                    <a class="_link js-compare-contact-info-btn" href="#"><?= Yii::t('app', 'Контактная информация') ?></a>
                                                    <ul class="js-compare-contact-info-block" style="display: none">
                                                        <li><?= Yii::t('app', 'Телефон') ?>: <?= $offerData->company->phone ?></li>
                                                        <li><?= Yii::t('app', 'Адрес') ?>: <?= $offerData->company->actualAddress->fullAddress ?></li>
                                                    </ul>
                                                </div>

                                                <div class="offer-item-compare__body">
                                                    <!--
                                                    <div class="offer-item-compare__body-left">
                                                        <div class="offer-item-compare__review">
                                                            <div class="offer-item-compare__rating">
                                                                <span class="_value">5.0</span>
                                                                <span class="_qty">по профилю</span>
                                                            </div>

                                                            <div class="offer-item-compare__rating offer-item-compare__rating--orange">
                                                                <span class="_value">4.0</span>
                                                                <span class="_qty">по отзывам</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    -->

                                                    <div class="offer-item-compare__body-right">
                                                        <button class="btn btn-reset js-compare-item-hide">
                                                            <svg class="icon icon-hide ">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#hide"/>
                                                                <?= Yii::t('app', 'ВРЕМЕННО СКРЫТЬ') ?>
                                                            </svg>
                                                        </button>
                                                        <button class="btn btn-reset js-compare-item-remove js-compare"
                                                                data-id="<?= $compareOptions['data-id'] ?>"
                                                                data-url="<?= $compareOptions['data-url'] ?>"
                                                                data-model="<?= $compareOptions['data-model'] ?>"
                                                                data-tender="<?= $compareOptions['data-tender'] ?>"
                                                        >
                                                            <svg class="icon icon-remove ">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#remove"/>
                                                                <?= Yii::t('app', 'УДАЛИТЬ ИЗ СРАВНЕНИЯ') ?>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="compare-params">
                                            <div class="compare-params__body">
                                                <div class="compare-params__option">
                                                    <div class="compare-params__option-title"><?= Yii::t('app', 'Условия') ?></div>
                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span><?= Yii::t('app', 'Срок работы (мес.)') ?></span></div>
                                                        <div class="_value"><span><?= $offerData->time_work ?></span></div>
                                                    </div>
                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span><?= Yii::t('app', 'Срок гарантии (мес.)') ?></span></div>
                                                        <div class="_value"><span><?= $offerData->time_warranty ?></span></div>
                                                    </div>

                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span><?= Yii::t('app', 'Банковская гарантия для аванса') ?></span></div>
                                                        <div class="_value">
                                                        <span>
                                                            <?= Yii::t('app', $offerData->bank_prepayment_warranty == Offers::TYPE_WARRANTY_YES ? 'Да' : 'Нет') ?>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="compare-params__option-item">
                                                        <div class="_label">
                                                        <span>
                                                            <?= Yii::t('app', 'Банковская гарантия для гарантийного периода') ?>
                                                        </span>
                                                        </div>
                                                        <div class="_value">
                                                        <span>
                                                            <?= Yii::t('app', $offerData->bank_period_warranty == Offers::TYPE_WARRANTY_YES ? 'Да' : 'Нет') ?>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="compare-params__option-item">
                                                        <div class="_label">
                                                        <span>
                                                            <?= Yii::t('app', 'Банковская гарантия для выполнения обязательств') ?>
                                                        </span>
                                                        </div>
                                                        <div class="_value">
                                                        <span>
                                                            <?= Yii::t('app', $offerData->bank_obligation_warranty == Offers::TYPE_WARRANTY_YES ? 'Да' : 'Нет') ?>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span><?= Yii::t('app', 'Порядок оплаты') ?></span></div>
                                                        <div class="_value"><?= $offerData->text ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="compare-params">
                                            <div class="compare-params__body">
                                                <div class="compare-params__option">
                                                    <div class="compare-params__option-title"><?= Yii::t('app', 'Итого') ?></div>
                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span><?= Yii::t('app', 'Итоговая стоимость') ?></span></div>
                                                        <div class="_value"><span class="_total"><?= Yii::$app->formatter->asMoney($offerData->sum) ?> <?= $currency[$tender->currency_id] ?? '' ?></span></div>
                                                    </div>
                                                    <!--
                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span>Включено в стоимость</span></div>
                                                        <div class="_value"><span>Текст</span></div>
                                                    </div>
                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span>Не включено в стоимость</span></div>
                                                        <div class="_value"><span>Текст</span></div>
                                                    </div>
                                                    -->
                                                    <div class="compare-params__option-item">
                                                        <div class="_label"><span><?= Yii::t('app', 'Комментарии') ?></span></div>
                                                        <div class="_value"><span><?= $offerData->text ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="compare__btn-wrapper">
                                            <button
                                                    class="btn btn-download"
                                                    onclick="$('.js-offer-id-hidden-input').val(<?= $offerData->id ?>); $('.js-offer-company-name').empty().append('<?= $offerData->company->title ?>');"
                                                    data-popup="winner">
                                                <?= Yii::t('app', 'ВЫБРАТЬ') ?>
                                            </button>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>      </div>
        </div>
    </div>
</div>
<?php foreach ($tender->estimatesRubrics as $rubrics) { ?>
    <?php if ($rubrics->getComments()->count() == 0) {
        continue;
    } ?>
    <div class="popup popup--comment-<?= $rubrics->id ?>" data-popup-id="comment-list" id="comment-list-<?= $rubrics->id ?>" style="display: none;">
        <div class="popup__box">
            <form action="">
                <div class="popup__box-inner">
                    <button class="popup__close js-close-wnd" type="button"></button>
                    <div class="popup__title"><?= Yii::t('app', 'Комментарии') ?></div>
                    <div class="popup__body">
                        <div class="popup-comment">
                            <ul class="popup-comment__list js-popup-scroll">
                                <?php foreach ($rubrics->getComments()->asArray()->all() ?? [] as $comment) { ?>
                                    <li class="popup-comment__item popup-comment__item--blue">
                                        <div class="popup-comment__item-logo">
                                            <img src="<?= Yii::getAlias('@static') ?>/../content/images/helment-small.svg" alt="" class="_img">
                                        </div>
                                        <div class="popup-comment__item-content">
                                            <p><?= $comment['text'] ?></p>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php } ?>
<div class="popup popup--winner" data-popup-id="winner" id="winner" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title popup__title--margin-b-small"><?= Yii::t('app', 'Выбрать победителя тендера') ?></div>
            <div class="popup__body">
                <p>
                    Вы действительно хотите выбрать компанию <span class="js-offer-company-name"></span> победителем тендера?
                </p>
            </div>
            <?= \yii\helpers\Html::hiddenInput('offerId', null, ['class' => 'js-offer-id-hidden-input']) ?>
            <div class="popup__footer">
                <div class="popup__action">
                    <button
                            data-url="<?= Url::to(['/dashboard/tender_offers_list/', 'companyUrl' => $companyUrl, 'id' => $tender->id]) ?>"
                            type="button" class="btn btn-primary js-select-tender-offer-winner"><?= Yii::t('app', 'Да') ?></button>
                    <button type="button" class="btn btn-outline-secondary js-close-wnd"><?= Yii::t('app', 'Отменить') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
