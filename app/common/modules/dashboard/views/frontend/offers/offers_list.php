<?php
use common\modules\tender\models\frontend\Tenders;
use yii\helpers\Url;

/**
 * @var Tenders $tender
 * @var null|string $typeList
 * @var  integer $bestOfferId
 * @var string $companyUrl
 * @var integer $countCompare
 * @var string $compareModel
 * @var array $comments
 */

?>
<div class="js-flash-message-widget-block">
    <?php
    \frontend\widgets\FlashMessagesWidget::widget();
    ?>
</div>
<div class="main__head">
    <div class="main__info">
        <div class="main__label"><?= Tenders::getModes($tender->mode) ?> <?= Yii::t('app', 'тендер') ?> № <?= $tender->id ?></div>
        <h1 class="main__title"><?= $tender->title ?></h1>
    </div>
    <div class="main__table">
        <ul class="info-list">
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Тип') ?></div>
                <div class="info-list__value"><?= Tenders::getTypes($tender->type) ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Вид') ?></div>
                <div class="info-list__value"><?= Tenders::getModes($tender->mode) ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Этапность') ?></div>
                <div class="info-list__value"><?= Tenders::getStages($tender->stage) ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Срок завершения') ?></div>
                <div class="info-list__value"><?= Yii::$app->formatter->asDate($tender->date_end, 'php:d.m.Y') ?></div>
            </li>
            <li class="info-list__item">
                <div class="info-list__label"><?= Yii::t('app', 'Валюта') ?></div>
                <div class="info-list__value"><?= $tender->currency->title ?></div>
            </li>
        </ul>
    </div>
</div>

<div class="main__body">
    <div class="tabs-block tabs-block--link">
        <div class="tabs-block__inner">
            <div class="tabs-block__head">
                <div class="tabs-block__list">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/tender_offers_info', 'tenderId' => $tender->id, 'companyUrl' => $companyUrl]) ?>" class="tabs-block__btn">
                        <?= Yii::t('app', 'УСЛОВИЯ') ?>
                    </a>
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/tender_offers_list', 'tenderId' => $tender->id, 'companyUrl' => $companyUrl]) ?>" class="tabs-block__btn active">
                        <?= Yii::t('app', 'ПРЕДЛОЖЕНИЯ') ?>
                    </a>
                </div>
                <div class="tabs-block__action js-count-compare-block" style="<?= $countCompare > 0 ? '' : 'display: none;' ?>">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/offers_compare', 'companyUrl' => $companyUrl, 'tenderId' => $tender->id]) ?>" class="btn btn-compare btn-outline-secondary">
                        <span><?= Yii::t('app', 'ПЕРЕЙТИ К СРАВНЕНИЮ') ?></span>
                        <span class="_qty js-count-compare-label"><?= $countCompare ?></span>
                        <?= \yii\helpers\Html::hiddenInput('countCompare', $countCompare, ['class' => 'js-count-compare-offers-input']) ?>
                    </a>
                </div>
            </div>

            <?=\modules\crud\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items}',
                'itemOptions' => [
                    'tag' => 'div',
                    'class' => '_offers_list_item',
                    'id' => 'offers-list',
                ],
                'viewParams' => [
                    'companyUrl' => $companyUrl,
                    'bestOfferId' => $bestOfferId,
                    'compareModel' => $compareModel
                ],
                'itemView' => '_offers_list_item',
                'emptyText' => 'У вас еще нет объектов этой категории',
                'pager' => false,
            ]) ?>

            <br>

            <?= $this->render('_offers_list_pagination', ['dataProvider' => $dataProvider]) ?>
        </div>
    </div>
</div>

<div class="popup popup--winner" data-popup-id="winner" id="winner" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title popup__title--margin-b-small"><?= Yii::t('app', 'Выбрать победителя тендера')?></div>
            <div class="popup__body">
                <p><?= Yii::t('app', 'Вы действительно хотите выбрать компанию') ?> <span class="js-offer-company-name"></span> <?= Yii::t('app', 'победителем тендера?') ?></p>
            </div>
            <div class="popup__footer">
                <div class="popup__action">
                    <?= \yii\helpers\Html::hiddenInput('offerId', null, ['class' => 'js-offer-id-hidden-input']) ?>
                    <button type="button" data-url="<?= Url::to() ?>" class="btn btn-primary js-select-tender-offer-winner"><?= Yii::t('app', 'Да') ?></button>
                    <button type="button" class="btn btn-outline-secondary js-close-wnd"><?= Yii::t('app', 'Отменить') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($comments as $offerId => $comment) { ?>
    <div class="popup popup--comment-<?= $offerId ?>" data-popup-id="comment-list" id="comment-list-<?= $offerId ?>" style="display: none;">
        <div class="popup__box">
            <form action="">
                <div class="popup__box-inner">
                    <button class="popup__close js-close-wnd" type="button"></button>
                    <div class="popup__title"><?= Yii::t('app', 'Комментарии') ?></div>
                    <div class="popup__body">
                        <div class="popup-comment">
                            <ul class="popup-comment__list js-popup-scroll">
                                <?php foreach ($comment as $val) { ?>
                                    <li class="popup-comment__item popup-comment__item--blue">
                                        <div class="popup-comment__item-logo">
                                            <img src="<?= Yii::getAlias('@static') ?>/../content/images/helment-small.svg" alt="" class="_img">
                                        </div>
                                        <div class="popup-comment__item-content">
                                            <p><?= $val['text'] ?></p>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php } ?>