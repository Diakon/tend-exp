<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */

?>


<?= \yii\widgets\LinkPager::widget([
    'pagination' => $dataProvider->pagination,
    'options' => [
        'class' => 'pagination__list',
    ],
    'linkContainerOptions' => [
        'class' => 'pagination__item',
    ],
    'linkOptions' => [
        'class' => 'pagination__link',
    ],
    'maxButtonCount' => 7,
    'disableCurrentPageButton' => true,
    'activePageCssClass' => 'pagination__item--current',
    'disabledPageCssClass' => 'pagination__item--disabled',
    'disabledListItemSubTagOptions' => ['tag' => 'span', 'class' => 'pagination__link'],
    'nextPageLabel' => '<svg class="icon icon-arrow-pag-next ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'
        . Yii::getAlias("@static") . '/../static/images/svg/spriteInline.svg#arrow-pag-next"/></svg>',
    'prevPageLabel' => '
<svg class="icon icon-arrow-pag-prev ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . Yii::getAlias("@static") . '/../static/images/svg/spriteInline.svg#arrow-pag-prev"/>
                            </svg>',
    'prevPageCssClass' => 'pagination__item pagination__item--prev',
    'nextPageCssClass' => 'pagination__item pagination__item--next',
]) ?>