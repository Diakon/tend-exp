<?php
use frontend\modules\uwm\Module as Uwm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var integer $bestOfferId
 * @var string $companyUrl
 * @var common\modules\offers\models\frontend\Offers $model
 * @var string $compareModel
 */
// Получаю функции компании
$functionsCompany = ArrayHelper::map($model->company->functionsCompany, 'id', 'title');
// Предложения по тендеру от компании
$offers = $model->getOffersTender();
$keyLasOffer = end($offers);

$compareSelected = Uwm::isSelected($model->id, Uwm::TYPE_COMPARE, $compareModel);
$compareOptions['class'] = empty($compareSelected) ? 'js-compare' : 'btn-add--active js-compare';
$compareOptions['data-id'] = $model->id;
$compareOptions['data-tender'] = $model->tender_id;
$compareOptions['data-url'] = Url::to(['/uwm/compare/index', 'id' => $model->id]);
$compareOptions['data-model'] = $model::classNameShort();
$commentsCount = $model->getComments()->count();
$diff = date('Y', time()) - $model->company->year;

?>
<div class="tabs-block__body">
    <div class="offer-item <?= $bestOfferId == $model->id ? 'offer-item--best' : '' ?>">
        <div class="offer-item__inner">
            <div class="offer-item__body">
                <div class="offer-item__body-left">
                    <?php if ($model->is_read == \common\modules\offers\models\frontend\Offers::IS_READ_NO || $bestOfferId == $model->id) { ?>
                        <div class="offer-item__label">
                            <span class="_text"><?= $bestOfferId == $model->id ? Yii::t('app', 'ЛУЧШЕЕ ПРЕДЛОЖЕНИЕ') : Yii::t('app', 'НОВОЕ ПРЕДЛОЖЕНИЕ') ?></span>
                        </div>
                    <?php } ?>

                    <h2 class="offer-item__title">
                        <?= \yii\helpers\Html::a($model->company->title, $model->getOfferViewLink(['companyUrl' => $companyUrl, 'tenderId' => $model->tender_id])) ?>
                    </h2>
                    <div class="offer-item__desc"><?= $model->company->legalAddress->fullAddress ?? '' ?></div>
                    <div class="offer-item__options">
                        <div class="offer-item__option">
                            <div class="_label"><?= Yii::t('app', 'Генеральный проектировщик') ?></div>
                            <div class="_value"><?= $diff ?> <?= \helpers\Text::plural($diff, ['год', 'года', 'лет']) ?> <?= Yii::t('app', 'опыта')?></div>
                        </div>
                        <?php if (!empty($offers[0])) { ?>
                            <div class="offer-item__option">
                                <div class="_label"><?= Yii::t('app', 'Первое предложение') ?> (<?= Yii::$app->formatter->asDate($offers[0]['date'], 'php:d.m.Y') ?>)</div>
                                <div class="_value"><?= Yii::$app->formatter->asMoney($offers[0]['sumDiscount'] > 0 ? $offers[0]['sumDiscount'] : $offers[0]['sum']) ?> <?= $model->tender->currency->title ?>.</div>
                            </div>
                        <?php } ?>
                        <?php if (!empty($offers[1])) { ?>
                            <div class="offer-item__option">
                                <div class="_label"><?= Yii::t('app', 'Второе предложение') ?> (<?= Yii::$app->formatter->asDate($offers[1]['date'], 'php:d.m.Y') ?>)</div>
                                <div class="_value"><?= Yii::$app->formatter->asMoney($offers[1]['sumDiscount'] > 0 ? $offers[1]['sumDiscount'] : $offers[1]['sum']) ?> <?= $model->tender->currency->title ?>.</div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="offer-item__body-right">
                    <div class="offer-item__action">
                        <!--
                        <div class="offer-item__rating  ">
                            <span class="_value">5.0</span>
                            <span class="_qty">83 отзыва</span>
                        </div>
                        -->
                        <div class="offer-item__btn">
                            <?php if (empty($model->tender->offer_id)) { ?>
                                <button class="_btn btn-icon js-winner-btn-icon"
                                        data-popup="winner"
                                        onclick="$('.js-offer-id-hidden-input').val(<?= $model->id ?>); $('.js-offer-company-name').empty().append('<?= $model->company->title ?>');">
                                    <svg class="icon icon-cup ">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#cup"/>
                                    </svg>
                                </button>
                            <?php } ?>
                            <?php if (!empty($commentsCount)) { ?>
                                <button class="_btn btn-icon btn-count" data-popup="comment-list-<?= $model->id ?>">
                                    <svg class="icon icon-comment ">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#comment"/>
                                    </svg>
                                    <span class="_btn-qty"><?= $commentsCount ?></span>
                                </button>
                            <?php } ?>
                            <button class="_btn btn-add <?= $compareOptions['class'] ?>"
                                    data-id="<?= $compareOptions['data-id'] ?>"
                                    data-url="<?= $compareOptions['data-url'] ?>"
                                    data-model="<?= $compareOptions['data-model'] ?>"
                                    data-tender="<?= $compareOptions['data-tender'] ?>"
                            >
                                <svg class="icon icon-add ">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#add"/>
                                </svg>
                            </button>
                        </div>
                    </div>

                    <?php if (!empty($keyLasOffer)) { ?>
                        <div class="offer-item__options">
                            <div class="offer-item__option">
                                <div class="_label"><?= Yii::t('app', 'Последнее предложение') ?> (<?= Yii::$app->formatter->asDate($keyLasOffer['date'], 'php:d.m.Y') ?>)</div>
                                <div class="_value"><?= Yii::$app->formatter->asMoney($keyLasOffer['sumDiscount'] > 0 ?  $keyLasOffer['sumDiscount'] : $keyLasOffer['sum']) ?> <?= $model->tender->currency->title ?>.</div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>