<?php
/**
 * @var $data \common\modules\company\models\frontend\Company
 */
$this->title = Yii::t('app', 'Настройки организации');

$canEdit = Yii::$app->user->identity->companyUser ? Yii::$app->user->identity->companyUser->canEditMyCompany() : false;


$builds = $data->builds;
$functions = $data->functionsCompany;
$employeesNowLabel = \common\modules\directories\models\common\EmployeesNow::findActiveList();
$employeesMaxLabel = \common\modules\directories\models\common\EmployeesMax::findActiveList();
?>

<?php if(!Yii::$app->user->canShowAdditionalFilter()) { ?>
    <div class="improve-block showed">
        <div class="improve-block__inner">
            <button class="improve-block__close js-close-improve" type="button"></button>
            <div class="improve-block__head">
                <div class="improve-block__title"><?= Yii::t('app', 'Улучшите аккаунт для публикации изменений')?></div>
            </div>
            <div class="improve-block__body">
                <div class="improve-block__desc">
                    Ваш текущий уровень аккаунта не позволяет публиковать дополнительные изменения в профиле организации.
                    Улучшите аккаунт и наслаждайтесь всеми возможностями сервиса TENDEROS, как сделали уже 2105
                    пользователей.
                </div>
                <div class="improve-block__action">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/tariff_edit', 'url' => $data->url]) ?>" class="btn btn-primary btn-small"><?= Yii::t('app', 'УЛУЧШИТЬ АККАУНТ')?></a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="tabs-block tabs-block--link">
    <div class="tabs-block__inner">
        <div class="tabs-block__head">

            <?= $this->render('_tab_menu_company', ['company' => $data]) ?>

            <?php if ($canEdit) { ?>
                <div class="tabs-block__action">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/company_edit', 'url' => $data->url]) ?>" class="btn-edit">
                        <svg class="icon icon-edit ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= \Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#edit"/>
                        </svg>
                        <span><?= Yii::t('app', 'РЕДАКТИРОВАТЬ ОРГАНИЗАЦИЮ') ?></span>
                    </a>
                </div>
            <?php } ?>

        </div>

        <div class="tabs-block__body">
            <div class="section-lk">
                <div class="logo-block logo-block--abs">
                    <div class="logo-block__inner">

                        <?php if ($data->avatar) { ?>
                            <img class="_img" src="<?php echo \common\components\Thumbnail::thumbnailFile($data->avatar->getFile(true)); ?>">
                        <?php } else { ?>
                            <img src="<?= \Yii::getAlias('@static') ?>/../content/images/helment.svg" alt="" class="_img">
                        <?php } ?>

                    </div>
                </div>
                <div class="section-lk__head">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Реквизиты')?></h1>
                </div>
                <div class="section-lk__body">
                    <ul class="info-list">
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Название компании') ?></div>
                            <div class="info-list__value"><?= $data->title ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'ОГРН')?></div>
                            <div class="info-list__value"><?= $data->ogrn ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'ИНН')?></div>
                            <div class="info-list__value"><?= $data->inn ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'КПП')?></div>
                            <div class="info-list__value"><?= $data->kpp ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Компания основана')?></div>
                            <div class="info-list__value"><?= $data->year ?> <?= Yii::t('app', 'год')?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Генеральный директор')?></div>
                            <div class="info-list__value"><?= $data->generalManage ?></div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="section-lk">
                <div class="section-lk__head">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Контактная информация')?></h1>
                </div>
                <div class="section-lk__body">
                    <ul class="info-list">
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Телефон')?></div>
                            <div class="info-list__value"><?= $data->phone ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Факс')?></div>
                            <div class="info-list__value"><?= !empty($data->fax) ? $data->fax : '-----' ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Юридический адрес') ?></div>
                            <div class="info-list__value"><?=  !empty($data->legalAddress) ? $data->legalAddress->getFullAddress() : '----' ?>
                            </div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Фактический адрес') ?></div>
                            <div class="info-list__value">
                                <?php if (!empty($data->address_same)) {
                                    echo $data->legalAddress ? $data->legalAddress->getFullAddress() : '----';
                                } else {
                                    echo $data->actualAddress ? $data->actualAddress->getFullAddress() :  '----';
                                }?>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="section-lk">
                <div class="section-lk__head">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Деятельность') ?></h1>
                </div>
                <div class="section-lk__body">
                    <ul class="info-list">
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Регион деятельности') ?></div>
                            <div class="info-list__value">
                                <?php foreach ($data->charsCountryIds as $countryId) {
                                    $country = \common\models\GeoCountry::find()->where(['id' => $countryId])->one();
                                    $regions = \yii\helpers\ArrayHelper::map(\common\models\GeoRegion::find()->where(['in', 'id', $data->charsRegionIds])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
                                    echo (!empty($country->name) ? ($country->name . ': ') : '') . implode(", ", $regions);
                                } ?>
                            </div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Функции') ?></div>
                            <div class="info-list__value"><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($functions, 'title')) ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Направления строительства') ?></div>
                            <div class="info-list__value"><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($builds, 'title')) ?></div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Сотрудников в штате')?></div>
                            <div class="_value"><?= $employeesNowLabel[$data->employeesNow] ?? 0  ?> человек</div>
                        </li>
                        <li class="info-list__item">
                            <div class="info-list__label"><?= Yii::t('app', 'Сотрудников максимум') ?></div>
                            <div class="_value"><?= $employeesMaxLabel[$data->employeesMax] ?? 0  ?> человек</div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="section-lk">
                <div class="section-lk__head">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Виды работ')?></h1>
                </div>
                <div class="section-lk__body">
                    <ul class="info-list">
                        <?php foreach ($data->completedWorks as $work ) { ?>
                            <li class="company-common__item">
                                <div class="_label"><?= $work->type->title ?></div>
                                <div class="_value"><?= $work->count ?>
                                    <?= \common\modules\directories\models\common\Directories::getUnitData($work->unitId)?></div>
                            </li>
                        <?php  } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
