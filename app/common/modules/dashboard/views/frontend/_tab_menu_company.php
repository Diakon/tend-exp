<?php
/**
 * @var \common\modules\company\models\frontend\Company $company
 */

?>

<?= \yii\widgets\Menu::widget([
  //  'encodeLabels' => false,
    'items' => [
        [
            'label' => Yii::t('app', 'ОБЩЕЕ'),
            'url' => ['/dashboard/dashboard/company_info'],
        ],
        [
            'label' => Yii::t('app', 'КОМАНДА'),
            'url' => ['/dashboard/dashboard/team_edit', 'url' => $company->url],
        ],
        [
            'label' => Yii::t('app', 'НАСТРОЙКИ'),
            'url' => ['/dashboard/dashboard/tariff_edit', 'url' => $company->url],
        ],

    ],
    'options' => ['class' => 'tabs-block__list', 'tag' => 'div'],
    'itemOptions' => ['class' => 'tabs-block__btn'],
]);
?>
