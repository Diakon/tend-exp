<?php
/**
 * @var $model \common\modules\company\models\frontend\Company
 */

use yii\bootstrap\ActiveForm;
use common\models\GeoCountry;
use common\models\GeoRegion;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\directories\models\common\TypesWork;
use \common\components\Thumbnail;

$this->title = Yii::t('app', 'Настройки организации');
$noPhoto = '';

$completedWorks = $model->completedWorks;
$error = Yii::$app->session->getFlash('error-address');

/**
 * @var $avatar \frontend\modules\upload\models\Upload
 */
$avatar = $model->avatar;
?>

<?php $form = ActiveForm::begin([
    'validateOnBlur' => false,
    'enableClientValidation' => false,
    'action' => \yii\helpers\Url::current(),
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
    'fieldConfig' => [
        'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
    ],
]); ?>
<div class="tabs-block tabs-block--lk js-tabs">

    <div class="tabs-block__body">
        <div class="tabs-block__content">

            <?php if ($error || !empty($model->errors)) { ?>
                <div class="_item alert-error" >
                    <?= $form->errorSummary($model) ?>
                    <?= implode(', ', $error ?? []) ?>
                </div>
            <?php } ?>

            <div class="section-lk">
                <div class="section-lk__head">
                    <h1 class="section-lk__title"><?= Yii::t('app', 'Реквизиты') ?></h1>
                </div>
                <div class="section-lk__body">
                    <div class="row">

                        <div class="col-6">
                            <?= $form->field($model, 'title')->textInput()->label(null)->error(['tag' => 'div']) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'year')->textInput()->label(null)->error(['tag' => 'div']) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'ogrn')->textInput()->label(null)->error(['tag' => 'div']) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'inn')->textInput()->label(null)->error(['tag' => 'div']) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'kpp')->textInput()->label(null)->error(['tag' => 'div']) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'general_manage')->textInput()->label(null)->error(['tag' => 'div']) ?>
                        </div>
                    </div>
                </div>

                <div class="section-lk__body">
                    <div class="edit-logo">
                        <div class="edit-logo__inner">
                            <div class="edit-logo__head">
                                <div class="edit-logo__title"><?= Yii::t('app', 'Логотип компании') ?></div>
                            </div>
                            <div class="edit-logo__main">
                                <div class="edit-logo__img">
                                    <div class="edit-logo__img-inner">

                                        <?php if ($avatar) { ?>
                                            <img style="border-radius: 70px" class="_img js-avatar"
                                                 src="<?= Thumbnail::thumbnailFileUrl($avatar->getFile(true, Yii::getAlias('@static') . '/../content/images/helment.svg')); ?>">
                                        <?php } else { ?>
                                            <img  src="<?= Yii::getAlias('@static') ?>/../content/images/helment.svg" alt="" class="_img ">
                                        <?php } ?>

                                    </div>
                                    <span class="_btn btn-remove js-btn-remove-avatar" data-url="<?= \yii\helpers\Url::to(['/dashboard/dashboard/delete_avatar_company']) ?>">
                                        <svg class="icon icon-remove ">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#remove"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="edit-logo__body">
                                    <div class="edit-logo__action">
                                        <input type="file" name="<?= \yii\helpers\Html::getInputName($model, 'avatar') ?>" id="edit-logo" class="_input js-file-name">
                                        <label for="edit-logo" class="_label btn btn-primary"><?= Yii::t('app', 'ЗАГРУЗИТЬ ЛОГОТИП') ?></label>
                                        <br>
                                        <span class="js-file-name-text file-name-text"></span>
                                        <span class="js-file-delete-text file-name-text" style="display: none">
                                            <div>
                                                <?= Yii::t('app', 'Логотип будет удалён после сохранения изменений') ?>
                                            </div>
                                        </span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section-lk__head">
                    <h1 class="section-lk__title">Профильные характеристики</h1>
                </div>
                <div class="section-lk__body">
                    <div class="row">

                        <?php foreach ($model->charsCountryIds  as $countryId) { ?>
                            <div class="col-6">
                                <?= $form->field($model, 'charsCountryIds[' . $countryId . ']')
                                    ->dropDownList(GeoCountry::getCountryList(),
                                        [
                                            'class' => 'js-dropdown-box',
                                        ]
                                    )->label(Yii::t('app', 'Страна деятельности')) ?>
                            </div>
                            <div class="col-6">
                                <?= $form->field($model, 'charsRegionIds', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                                    ->dropDownList(GeoRegion::getRegionsList($countryId),
                                        [
                                            'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать']
                                    ) ?>
                            </div>
                        <?php } ?>

                        <div class="col-6">
                            <?= $form->field($model, 'charsFunctionsIds', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                                ->dropDownList(FunctionsCompany::findActiveList(),
                                    [
                                        //'value' => $model->charsFunctionsIds,
                                        'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать', 'placeholder' => 'Выбрать']
                                )->label(null)->error(['tag' => 'div']) ?>

                        </div>
                        <div class="col-6">

                            <?= $form->field($model, 'charsBuildsIds', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                                ->dropDownList(\common\modules\directories\models\common\Activities::findActiveList(),
                                    [
                                           // 'value' => $model->charsBuildsIds,
                                        'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden']
                                ) ?>

                        </div>
                        <div class="col-6">

                            <?= $form->field($model, 'charsWorksIds', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                                ->dropDownList(TypesWork::findActiveList(),
                                    [
                                            //'value' => $model->charsWorksIds,
                                'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать']
                                ) ?>
                            </div>
                                <div class="col-6">
                                    <?= $form->field($model, 'employees_now')
                                        ->dropDownList(\common\modules\directories\models\common\EmployeesNow::findActiveList(), ['class' => 'js-dropdown-box'])
                                        ->error(['tag' => 'div']) ?>
                                </div>
                                <div class="col-6">
                                    <?= $form->field($model, 'employees_max')
                                        ->dropDownList(\common\modules\directories\models\common\EmployeesMax::findActiveList(), ['class' => 'js-dropdown-box'])
                                        ->error(['tag' => 'div']) ?>
                                </div>

                    </div>
                    <div class="section-lk__head">
                        <h1 class="section-lk__title">Контактные данные</h1>
                    </div>

                    <div class="section-lk__body">
                        <div class="form-group">
                            <label for="" class="control-label">Юридический адрес</label>
                            <br>
                            <div class="address-field">

                                <div class="address-field__row">
                                    <div class="address-field__col _col-1">
                                        <div class="form-group ">
                                            <?= $form->field($model, 'legalAddressIndex')
                                                ->textInput(['placeholder' => Yii::t('app', 'Индекс')])
                                                ->label(false)
                                                ->error(['tag' => 'div']) ?>
                                        </div>
                                    </div>
                                    <div class="address-field__col _col-2">
                                        <?= $form->field($model, 'legalAddressCountryId')
                                            ->dropDownList(\common\models\GeoCountry::getCountryList(), ['class' => 'js-dropdown-box'])
                                            ->label(false)->error(['tag' => 'div']) ?>
                                    </div>
                                    <div class="address-field__col _col-3">
                                        <?= $form->field($model, 'legalAddressCityId', ['options' => ['class' => 'form-group form-group--autocomplete']])
                                            ->dropDownList(\common\models\GeoCity::getCityList(), [
                                                'class' => '_select js-autocomplete-select',
                                                'data-empty-text' => 'Ничего не найдено',
                                                'placeholder' => 'Выберите город',

                                            ])
                                            ->error(['tag' => 'div'])->label(false) ?>
                                    </div>

                                    <div class="address-field__col _col-4">
                                        <?= $form->field($model, 'legalAddressStreet')
                                            ->textInput(['placeholder' => Yii::t('app', 'Улица')])->error(['tag' => 'div'])->label(false) ?>

                                    </div>

                                    <div class="address-field__col _col-5">
                                        <?= $form->field($model, 'legalAddressHouse')
                                            ->textInput(['placeholder' => Yii::t('app', 'Дом')])->error(['tag' => 'div'])->label(false) ?>
                                    </div>

                                    <div class="address-field__col _col-6">
                                        <?= $form->field($model, 'legalAddressStructure')
                                            ->textInput(['placeholder' => Yii::t('app', 'Строение')])->error(['tag' => 'div'])->label(false) ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="section-lk__body ">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="" class="control-label">Фактический адрес</label>
                                    <div class="check-box address-check">

                                        <?= $form->field($model, 'address_same')->checkbox(['class' => 'js-address_same _input'])
                                            ->label('<span class="check-label">Совпадает с юридическим</span>', ['class' => 'check-box']) ?>
<!--                                        <input value="0" id="address-check" class="_input" type="checkbox" checked>-->
<!--                                        <label for="address-check" class="check-label">Совпадает с юридическим</label>-->

                                        <div class="address-field js-show-actual-address">
                                            <div class="address-field__row">
                                                <div class="address-field__col _col-1">
                                                    <?= $form->field($model, 'actualAddressIndex')
                                                        ->textInput(['placeholder' => Yii::t('app', 'Индекс')])
                                                        ->label(false)
                                                        ->error(['tag' => 'div']) ?>
                                                </div>
                                                <div class="address-field__col _col-2">
                                                    <?= $form->field($model, 'actualAddressCountryId')
                                                        ->dropDownList(\common\models\GeoCountry::getCountryList(), ['class' => 'js-dropdown-box', 'placeholder' => 'Выберите город',])
                                                        ->label(false)
                                                        ->error(['tag' => 'div']) ?>
                                                </div>
                                                <div class="address-field__col _col-3">
                                                    <?= $form->field($model, 'actualAddressCityId', ['options' => ['class' => 'form-group form-group--autocomplete']])
                                                        ->dropDownList(\common\models\GeoCity::getCityList(), ['class' => '_select js-autocomplete-select', 'data-empty-text'=>"Ничего не найдено", 'placeholder'=>"Выберите город"])
                                                        ->label(false)
                                                        ->error(['tag' => 'div']) ?>
                                                </div>
                                                <div class="address-field__col _col-4">
                                                    <?= $form->field($model, 'actualAddressStreet')
                                                        ->textInput(['placeholder' => Yii::t('app', 'Улица')])
                                                        ->label(false)
                                                        ->error(['tag' => 'div']) ?>
                                                </div>
                                                <div class="address-field__col _col-5">
                                                    <?= $form->field($model, 'actualAddressHouse')
                                                        ->textInput(['placeholder' => Yii::t('app', 'Дом')])
                                                        ->label(false)
                                                        ->error(['tag' => 'div']) ?>
                                                </div>
                                                <div class="address-field__col _col-6">
                                                    <?= $form->field($model, 'actualAddressStructure')
                                                        ->textInput(['placeholder' => Yii::t('app', 'Строение')])
                                                        ->label(false)
                                                        ->error(['tag' => 'div']) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-lk__body">
                            <div class="row">
                                <div class="col-4">
                                    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, ['mask' => '+7 999 999-99-99',])
                                        ->label(null)->error(['tag' => 'div']) ?>
                                </div>

                                <div class="col-4">
                                    <?= $form->field($model, 'fax')->widget(\yii\widgets\MaskedInput::class, ['mask' => '+7 999 999-99-99',])
                                        ->label(null)->error(['tag' => 'div']) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-lk__body">
                        <div class="row">
                            <div class="col-12">
                                <p>Выполненные работы</p>

                                <!-- Скрытый блок для JS клонирования -->
                                <div class="js-completed-works-block-hidden-block" style="display:none;">
                                    <?= $this->render('_object_edit_completed_works', [
                                        'model' => $model,
                                        'completedWorks' => ['keyCloneId' => new \common\modules\company\models\common\CompanyCompletedWorks()],
                                    ]); ?>
                                </div>

                                <div class="row js-completed-works-block">
                                    <?= $this->render('_object_edit_completed_works', [
                                        'model' => $model,
                                        'completedWorks' => $completedWorks,
                                    ]); ?>
                                </div>
                                <?= \yii\helpers\Html::hiddenInput('countCompletedWorks', count($completedWorks), [
                                    'class' => 'js-count-completed-works-hidden-input',
                                ]) ?>

                            </div>
                            <div class="col-1">
                                <div class="row">

                                    <button style="margin-left:20px;" class="btn btn-w100 btn-outline-secondary btn-plus js-completed-works-add-btn" type="button">+</button>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="section-lk__body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <?= $form->field($model, 'publish_catalog')->checkbox(['class' => ''])
                                    ->label('<span class="check-label">' . Yii::t('app', 'Публиковать в каталоге компаний') . '</span>', ['class' => 'check-box']) ?>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="section-lk__action">
                        <button type="submit" class="btn btn-primary"><?= Yii::t('app', 'СОХРАНИТЬ ИЗМЕНЕНИЯ') ?></button>
                        <?= \yii\helpers\Html::a(Yii::t('app', 'Отменить'), \yii\helpers\Url::to(['/dashboard/dashboard/company_info']), ['class' => 'btn btn-outline-secondary']) ?>
                    </div>
                </div>


            </div>
        </div>

        <?php ActiveForm::end(); ?>
