<?php
/**
 * @var \common\modules\tender\models\frontend\Objects $model
 */

$countTenders = count($model->tenders);
$functions = $model->functions ?? [];
// Направление строительства
$activities = $model->activities ?? [];
$types = $model->types ?? [];
?>


<div class="tender-item__inner">
    <div class="tender-item__head">
        <div class="tender-item__options">
            <div class="tender-item__option"><?= Yii::t('app', 'Создан') ?>: <?= Yii::$app->formatter->asDate($model->created_at) ?></div>
            <div class="tender-item__option"><?= Yii::t('app', 'Автор') ?>: <?= $model->user ? $model->user->getFullName() : '-----' ?></div>
        </div>

        <div class="tender-item__status">
            <div class="status-block <?= $model->statusCompleteId != \common\modules\tender\models\common\Objects::STATUS_COMPLETE ? 'status-block--red' : '' ?>">
                <div class="status-block__text"><?= $model->getStatusComplete($model->statusCompleteId) ?></div>
            </div>
        </div>
    </div>
    <div class="tender-item__body">
        <div class="tender-item__body-left">


            <?php if (Yii::$app->user->can('editProject', ['companyUrl' => $companyUrl])) { ?>

                <a href="<?= $model->getUpdateLink('object_edit', ['companyUrl' => $companyUrl, 'objectId' => $model->id]) ?>" class="tender-item__edit">
                    <svg class="icon icon-edit ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= \Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#edit"/>
                    </svg>
                    <span><?= Yii::t('app', 'РЕДАКТИРОВАТЬ') ?></span>
                </a>
            <?php } ?>

            <h2 class="tender-item__title">
                <a href="<?= $model->getUpdateLink('object_edit', ['companyUrl' => $companyUrl, 'objectId' => $model->id]) ?>"><?= $model->title ?></a></h2>
            <div class="tender-item__desc"><?= $model->address->fullAddress ?? '' ?></div>
        </div>
    </div>
    <div class="tender-item__foot">
        <div class="tender-item__options">
            <div class="tender-item__option"><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($functions, 'title')) ?></div>
            <div class="tender-item__option"><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($activities, 'title')) ?>
                 <?php if (!empty($types)) {
                    echo ':' . implode(', ', \yii\helpers\ArrayHelper::getColumn($types, 'title'));
                }  ?></div>
        </div>
        <div class="tender-item__action">
            <div class="tender-item__qty"><?= $countTenders ?>  <?= \helpers\Text::plural($countTenders, ['тендер', 'тендера', 'тендеров']) ?></div>
        </div>
    </div>
</div>



