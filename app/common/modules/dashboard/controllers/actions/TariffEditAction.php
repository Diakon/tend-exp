<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\ClassMap;
use common\modules\company\models\common\CompanyUsers;
use common\modules\company\models\frontend\Company;
use common\modules\company\models\frontend\CompanyUserAccess;
use common\modules\tariff\models\common\TariffHistory;
use common\modules\tariff\models\frontend\TariffPlans;
use common\modules\tender\models\frontend\Objects;
use http\Url;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class TariffEditAction. Управление тарифом
 * @package common\modules\dashboard\controllers\actions
 */
class TariffEditAction extends BaseAction
{
    /**
     * Редактирование тарифа.
     *
     * @return string
     */
    public function run()
    {
        $this->controller->layout = $this->layout;

        /**
         * @var Company $model
         */
        $company = Company::findOne(['id'=> Yii::$app->user->getActiveCompanyData()['id']]);

        if (empty($company)) {
            throw new NotFoundHttpException();
        }
        $minTariffPlan = TariffPlans::find()->where(['type' => TariffPlans::CONST_TYPE_MIN])->one();

        // Если не указан тарифный план компании - считаем его минимальным
        if (is_null($company->tariff_plan_id)) {
            $company->tariff_plan_id = $minTariffPlan->id ?? null;
        }

        // Запрос пробления текущего тарифного плана
        if (!empty(Yii::$app->request->post('extendPayment'))) {
            $idExtendPayment = Yii::$app->request->post('extendPayment');
            $tariff = TariffPlans::find()
                ->where(['status' => TariffPlans::STATUS_ACTIVE])
                ->andWhere(['id' => (int)$idExtendPayment])
                ->one();
            if (empty($tariff)) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Тариф по указанным параметрам не найден.'));
            }
        }

        $model = new TariffPlans();
        if ($model->load(Yii::$app->request->post())) {
            // Получаю тариф по параметрам
            $query = TariffPlans::find()->where(['status' => TariffPlans::STATUS_ACTIVE]);
            foreach ($model->attributes as $key => $value) {
                if (is_null($value)) {
                    continue;
                }
                $query->andWhere([$key => $value]);
            }
            $tariff = $query->one();
            if (empty($tariff)) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Тариф по указанным параметрам не найден.'));
            }
        }

        // Если выбран какой то тариф - перехожу к оплате
        if (!empty($tariff)) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Ваш тариф успешно оплачен!'));
            // Продление текущего тарифного плана
            if (!empty($company->tariff)
                && $company->tariff->type != TariffPlans::CONST_TYPE_MIN
                && $company->tariff->type == $tariff->type
                && $company->tariff->count_users_id == $tariff->count_users_id
            ) {
                $company->tariff_plan_id = $tariff->id;
                $company = TariffPlans::extendCurrentTariffPlan($company);
            } else {
                // Новый тариф. план
                $company->tariff_plan_id = $tariff->id;
                $company = TariffPlans::setNewTariffPlan($company);
            }
            $company->save(false);
            TariffHistory::addToHistory($company);
        }

        // Получаю все тарифные планы
        $tariffPlans = [];
        foreach (TariffPlans::$typesList as $idType => $nameType) {
            $tariffPlan = TariffPlans::find()->where(['type' => $idType])->andWhere(['status' => TariffPlans::STATUS_ACTIVE])->orderBy(['sort' => SORT_ASC])->asArray()->all();
            if (!empty($tariffPlan)) {
                $tariffPlans[$idType] = $tariffPlan;
            }
        }

        //Формирую сассив в котором значение - срок  продения тарифного плана, а ключ - ID тарифного плана
        $extendPaymentData = [];
        foreach ($tariffPlans[$company->tariff->type ?? 0] ?? [] as $tariff) {
            // Для продления текущего тарифа берем только те тарифы, количество пользователей в которых соответствует выбранному (текущему) тарифу
            if ($tariff['count_users_id'] != $company->tariff->count_users_id ?? null) {
                continue;
            }
            $id = $tariff['id'];
            $extendPaymentData[$id] = $tariff['count_month'];
        }
        asort($extendPaymentData);


        return $this->controller->render($this->params['template'], [
            'model' => $model,
            'company' => $company,
            'tariffPlans' => $tariffPlans,
            'minTariffPlan' => $minTariffPlan ?? new TariffPlans(),
            'extendPaymentData' => $extendPaymentData
        ]);
    }


}
