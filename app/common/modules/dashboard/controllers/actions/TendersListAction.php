<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\modules\company\models\frontend\CompanyUsers;
use common\modules\directories\models\common\Currency;
use common\modules\tender\models\frontend\Tenders;
use common\modules\tender\models\frontend\UserFavoritesTenders;
use frontend\widgets\CompanyTendersList;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;

/**
 * Class TendersListAction
 * @package common\modules\tender\controllers\actions
 */
class TendersListAction extends BaseAction
{
    /**
     * Список проектов доступных для просмотра пользователю.
     *
     * @return string
     */
    public function run()
    {
        $this->controller->layout = $this->layout;
        $company = Yii::$app->user->getCompany($this->params['companyUrl'])->one();
        /**
         * @var $companyUser CompanyUsers
         */
        $companyUser = $company->companyUser;

        $typeTenderList = Yii::$app->request->get('type');
        // Получаю ID тендеров, которые были добавлены в избранное пользователем
        $favoritesTenders = new UserFavoritesTenders();
        $tenderFavoritesId = ArrayHelper::map($favoritesTenders->getFavoritesTenders()->asArray()->all(), 'tender_id', 'tender_id');

        $currency = Currency::getCurrencyList();

        $model = new Tenders();
        $dataProvider = $model->searchItems(Yii::$app->request, false, $company->id, $typeTenderList ?? 'myTenders', $companyUser);

        $isAjax = Yii::$app->request->isAjax;
        $page = intval(Yii::$app->request->post('loadPage', null));
        if ($isAjax) {
            // Если пришел запрос добавления тендера в избранное - добавляем и выходим
            if (!empty(Yii::$app->request->post('tenderFavorites')) && !Yii::$app->user->isGuest) {
                $this->tenderFavorites(Yii::$app->request->post('tenderFavorites'));
                return Json::encode([
                    'status' => 200
                ]);
            }

            //Если пришел аякс запрос получения информации о прдложении к тендеру - возвращаю ответ
            if (!empty(Yii::$app->request->post('OffersTenderInfo'))) {
                $tender = Yii::$app->request->post('OffersTenderInfo');
                $result = $this->getOffersTender((int)$tender['tenderId']);
                Yii::$app->response->statusCode = $statusCode = !empty($result) ? 200 : 500;
                return Json::encode(['status' => $statusCode, 'result' => $result ?? '']);
            }

            $dataProvider->setPagination(['pageSize' => YII_ENV == 'dev' ? 10 : 10, 'pageSizeLimit' => [1, $dataProvider->totalCount], 'totalCount' => $dataProvider->totalCount]);
            $dataProvider->pagination->setPage($page);

            if ($page >= $dataProvider->pagination->pageCount) { // Если нет данных
                $dataProvider->pagination->setPage($page-1);
                return Json::encode(['data' => '',
                    'pagination' => $this->controller->renderAjax('/frontend/tenders/_tenders_pagination', ['dataProvider' => $dataProvider ]),
                    'page' => $page,]);
            }
        }

        $title = Yii::t('app', 'Тендеры');
        $this->controller->view->params['breadcrumbs'][] = ['encode' => false, 'label' => '<span class="breadcrumbs__label">' . $title . '</span>'];
        $this->controller->view->title = $title;

        $view = $isAjax ? '/frontend/tenders/_tenders_list_inner' : $this->params['template'];

        if ($isAjax) {
            return Json::encode([
                'data' => Yii::$app->controller->renderAjax($view,
                    [
                        'model' => $model,
                        'isAjax' => $isAjax,
                        'dataProvider' => $dataProvider,
                        'page' => $page,
                        'address' => $model->addressFilter,
                        'company' => $company,
                        'typeTenderList' => $typeTenderList,
                        'tenderFavoritesId' => $tenderFavoritesId,
                        'currency' => $currency
                    ]),
                'pageCount' => $dataProvider->pagination->page+1,
                'pagination' => Yii::$app->controller->renderAjax('/frontend/tenders/_tenders_pagination',
                    ['dataProvider' => $dataProvider,]),
            ]);

        }

        return $this->controller->render(
            $view, [
                'model' => $model,
                'isAjax' => $isAjax,
                'dataProvider' => $dataProvider,
                'address' => $model->addressFilter,
                'company' => $company,
                'typeTenderList' => $typeTenderList,
                'tenderFavoritesId' => $tenderFavoritesId,
                'currency' => $currency
            ]
        );
    }


    /**
     * Возвращает даныые о предложениях к тендеру
     *
     * @param integer $tenderId
     * @return string
     */
    private function getOffersTender($tenderId)
    {
        $returnHtml = '';
        $model = Tenders::find()->where(['id' => $tenderId])->one();

        if (!empty($model)) {
            $offers = $model->getOffersList()->groupBy(['company_id'])->orderBy(['updated_at' => SORT_ASC])->all();
            $returnHtml = $this->controller->renderPartial('/frontend/tenders/_offers_tender_info_list', [
                'offers' => $offers
            ]);
        }

        return $returnHtml;
    }

    /**
     * Добавляет / убирает тендер из избранного
     *
     * @param $tenderId
     */
    private function tenderFavorites($tenderId)
    {
        $model = UserFavoritesTenders::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['tender_id' => $tenderId])->one();
        // Тендера нет в избраном - добавляю
        if (empty($model)) {
            $model = new UserFavoritesTenders();
            $model->user_id = Yii::$app->user->id;
            $model->tender_id = $tenderId;
            $model->save();
        } else {
            // Тендер есть в избранном - убираю, т.к. повторно кликнули на кнопку "добавить в избранное"
            $model->delete();
        }
    }
}