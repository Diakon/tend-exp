<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\ClassMap;
use common\modules\company\models\frontend\Company;
use common\modules\offers\models\forms\OfferForm;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\frontend\EstimatesElements;
use common\modules\tender\models\frontend\Tenders;
use common\modules\tender\models\frontend\EstimatesRubrics;
use frontend\models\EntityComment;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class TenderOfferViewAction
 * @package common\modules\dashboard\controllers\actions
 */
class TenderOfferViewAction extends BaseAction
{
    /**
     * Просмотр предложения к тендеру
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        $this->controller->layout = $this->layout;
        $this->controller->view->params['isLongView'] = true;

        $tenderId = $this->params['tenderId'];
        $offerId = $this->params['offerId'];
        $companyUrl = $this->params['companyUrl'];

        /**
         * @var Company $company
         * @var Tenders $tender
         * @var Offers $offer
         */
        $tender = Tenders::find()->where(['id' => (int)$tenderId])->one();
        $offer = Offers::find()->where(['id' => (int)$offerId])->andWhere(['tender_id' => (int)$tenderId])->one();
        // Если в архиве - получаю послднее не архивное предложение
        if ($offer->is_archive == Offers::IS_ARCHIVE_TRUE) {
            $offer = Offers::find()
                ->where(['tender_id' => (int)$tenderId])
                ->andWhere(['is_archive' => Offers::IS_ARCHIVE_NO])
                ->andWhere(['company_id' => $offer->company_id])
                ->one();
        }
        $company = Yii::$app->user->getCompany($companyUrl)->one();

        if (empty($tender) || empty($company) || empty($offer)) {
            throw new NotFoundHttpException('Offer not found');
        }
        $offerId = $offer->id;
        
        // Ставлю флаг, что предложение просмотренно
        if ($offer->is_read != Offers::IS_READ_YES) {
            $offer->is_read = Offers::IS_READ_YES;
            $offer->save(false);
        }

        $model = new OfferForm();
        $model->tender = $tender;
        $model->companyId = $company->id;
        $model->offer = $offer;
        $model->offerId = $offer->id;
        $model->className = ClassMap::CLASS_ESTIMATES_ELEMENTS_ID;

        //Если пришел аякс запрос - обрабатываю
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            // Это победитель тендера
            if (!empty(Yii::$app->request->post('isTenderWinner'))) {
                $result = false;
                if (empty($tender->offer_id)) {
                    $tender->offer_id = $offer->id;
                    $result = $tender->save();
                }
                Yii::$app->response->statusCode = $statusCode = !empty($result) ? 200 : 500;
                return Json::encode(['status' => $statusCode, 'result' => $result ?? '']);
            }
            // Коментарий для рубрик смет
            if (!empty(Yii::$app->request->post('EstimatesRubricsComment'))) {
                $commentParams = Yii::$app->request->post('EstimatesRubricsComment');
                $commentParams['companyId'] = $company->id;
                $result = $this->commentRubrics($commentParams, $offer->tHash);
                Yii::$app->response->statusCode = $statusCode = !empty($result) ? 200 : 500;
                if ($statusCode == 200) {
                    // Отправляю письмо автору предложения, что автор тендера оставил коментарий
                    $model->sendEmailComment($offer, true);
                }
                return Json::encode(['status' => $statusCode, 'result' => $result ?? []]);
            }
        }

        $model->setOfferData();

        if ($model->load(Yii::$app->request->post()) && $model->correctOfferAuthorTender())
        {
            $this->controller->redirect(['/dashboard/tender_offer_view', 'companyUrl' => $company->url, 'id' => $offer->id, 'tenderId' => $tender->id]);
            Yii::$app->end();
        }


        // Проверяю - лучшее ли это предложение
        $bestOffer = Offers::getBestOfferTender($tender->id);
        $isBestOffer = !empty($bestOffer) && $bestOffer->id == $offer->id ? true : false;

        // Получаю коментарии заказчика для этого тендера
        $comments = EntityComment::find()
            ->where(['className' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID])
            ->andWhere(['entityId' => $tender->id])
            ->andWhere(['offerId' => null])
            ->orderBy(['created_at' => SORT_ASC])
            ->asArray()
            ->all();

        return $this->renderAction($this->params['template'], [
            'model' => $model,
            'offer' => $offer,
            'isBestOffer' => $isBestOffer,
            'comments' => $comments,
            'companyUrl' => $company->url
        ]);
    }

    /**
     * Создает коментарий от автора тендера для предложения
     *
     * @param array $commentParams
     * @param string|integer $offerId
     * @return bool
     */
    private function commentRubrics($commentParams, $offerId)
    {
        $model = EstimatesRubrics::find()->where(['id' => $commentParams['id']])->one();
        return EntityComment::addComment($model, $commentParams['comment'], $offerId);
    }
}