<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class CompanyDetailAction
 * @package common\modules\company\controllers\actions
 */
class CompanyInfo extends BaseAction
{
    /**
     * Вывод простой карточки элемента.
     *
     * @param $url
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $model = is_object($this->params['model']) ? $this->params['model'] : new $this->params['model'];

        if ($model instanceof \modules\crud\models\ActiveRecord && !$model->isNewRecord) {
            $data = $model;
        } else {
            $key = !empty($this->params['id']) ? $model::primaryKey()[0] : $model::$paramColumn;

            $data = $model->findOne([$key => Yii::$app->user->getActiveCompanyData()['url']]);

        }

        if (is_null($data)) {
            throw new NotFoundHttpException();
        }

        $data->setCharsParam();

        return $this->renderAction($this->params['template'], ['data' => $data, 'model' => $model,]);

    }
}
