<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\ListAction;
use common\modules\company\models\frontend\Company;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class MyCompanyList.
 * @package common\modules\dashboard\controllers\actions
 */
class MyCompanyList extends ListAction
{
    /**
     * @var string
     */
    public $layout = '@root/themes/frontend/views/layouts/main';

    /**
     * @inheritdoc
     *
     * @throws NotFoundHttpException Not found http exception
     */
    public function run()
    {
        $model = new Company();
        $this->controller->layout = $this->layout;
        $dataProvider = $model->getUserCompanies(Yii::$app->request);

        return $this->controller->render(
            $this->params['template'],
            [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
