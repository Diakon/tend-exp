<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\modules\tender\models\frontend\Objects;
use frontend\widgets\DetailObjectEdit;
use Yii;
use yii\base\Event;

/**
 * Class ObjectEditAction
 * @package common\modules\tender\controllers\actions
 */
class ObjectEditAction extends BaseAction
{
    /**
     * Редактирование проекта.
     *
     * @return string
     */
    public function run($companyUrl, $objectId = null, $tHash = null)
    {

        $this->controller->layout = $this->layout;

        $company = Yii::$app->user->getCompany($companyUrl)->one();

        /**
         * @var Objects $model
         */
        $model = new $this->params['model'];
        if (!empty($objectId)) {
            $model = $model::find()->where(['id' => (int)$objectId])->one();
        }

        if (empty($model) || empty($company)) {
            throw new NotFoundHttpException();
        }
        $model->prepareModelAction($tHash);

        // Установка временного хеша модели
        $model->tHash = $model->isNewRecord && !empty($tHash) && empty($model->id) ? $tHash : $model->id;
        $isNewRecord = $model->isNewRecord;

        $model->setAddressParam();
        $model->setScenario(Objects::SCENARIO_CREATE_OBJECT);
        if ($model->isNewRecord) {
            $model->status = $model::STATUS_ACTIVE;
            $model->company_id = $company->id;
            $model->user_id = Yii::$app->user->id;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->updateObjectChildRecords();
            if ($model->save()) {


                // Событие после сохранения модели
                $event = new Event();
                $event->sender = $model;
                $model->trigger($model::EVENT_AFTER_MAIN_ACTION_COMMIT, $event);
                Yii::$app->getResponse()->redirect(['/dashboard/dashboard/objects', 'companyUrl' => Yii::$app->user->getActiveCompanyData()['url']]);
            }
        }

        return $this->controller->render($this->params['template'], [
            'model' => $model,
            'tHash' => $tHash,
            'isNewRecord' => $isNewRecord,
        ]);

    }
}
