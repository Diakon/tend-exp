<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\ClassMap;
use common\components\SendMail;
use common\components\User;
use common\models\UserIdentity;
use common\modules\company\models\common\CompanyUsers;
use common\modules\company\models\frontend\Company;
use common\modules\company\models\frontend\CompanyUserAccess;
use common\modules\tender\models\frontend\Objects;
use common\modules\users\models\forms\SignupForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class TeamEditAction.
 * @package common\modules\dashboard\controllers\actions
 */
class TeamEditAction extends BaseAction
{
    /**
     * Редактирование команды.
     *
     * @return string
     */
    public function run()
    {
        $this->controller->layout = $this->layout;

        /**
         * @var Company $model
         */
        $company = Company::findOne(['id'=> Yii::$app->user->getActiveCompanyData()['id']]);

        if (empty($company)) {
            throw new NotFoundHttpException();
        }

        $userId = Yii::$app->request->get('user_id');
        $deleteUserId = Yii::$app->request->get('delete_user_id');
        $addUsers = Yii::$app->request->post('addUsersInTeam');
        $authUser = $company->companyUser;
        $canEditTeam = $authUser->type_role == CompanyUsers::TYPE_ROLE_ADMIN ? true : false;

        // Если пришел запрос на добавление пользователя в команду
        if ($addUsers['emails'] && $canEditTeam) {
            $errors = [];
            foreach ($addUsers['emails'] as $email) {
                $email = trim($email);
                if (empty($email)) {
                    continue;
                }
                // Создаем пользователя, генерируем пароль и отправляю письмо с приглашением
                $password = Yii::$app->security->generateRandomString(rand(6,10));
                $model = new SignupForm();
                $model->scenario = SignupForm::SCENARIO_ADD_IN_TEAM_USER;
                $model->username = $email;
                $model->email = $email;
                $model->password = $model->password_repeat = $password;
                $model->agreement = 1;
                $model->companyId = $company->id;
                $model->validate();
                if (empty($model->error)) {
                    $transaction = Yii::$app->db->beginTransaction();

                    $signUp = $model->signup(true);
                    $user = $signUp['user'] ?? null;

                    if ($user) {
                       // $this->sendAddUserTeamEmail($model, $company, $password,$addUsers['text'] ?? '');

                        /**
                         * @var $user UserIdentity
                         */
                        $user = $signUp['user'] ?? null;
                        $company = $signUp['company'] ?? null;

                        if (!$user->createEmailConfirmToken()) {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка создания токена проверки почтового адреса'));
                            $transaction->rollBack();
                        }
                        if (!$user->sendEmailConfirmationEmail('confirmNewEmail', 'new_email', $company, true, ['adminText' => $addUsers['text'] ?? ''])) {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка при отправке почтового сообшения'));
                            $transaction->rollBack();
                        }

                        $transaction->commit();

                    } else {
                        $errors[] = Yii::t('app', 'Ошибка добавления пользователя ' . $email );
                        $transaction->rollBack();
                    }
                }
            }
            $status = !empty($errors) ? 'errors' : 'success';
            $textStatus = !empty($errors) ? implode("<BR>", $errors) : Yii::t('app', 'Приглашения отправлены.');
            Yii::$app->session->setFlash($status, $textStatus);
        }

        // Список команды компании.
        $teamUsers = $company->users;

        // Если пришел запрос на удаление пользователя из команды
        if (!empty($deleteUserId) && in_array($deleteUserId, ArrayHelper::getColumn($teamUsers, 'user_id')) && $canEditTeam) {
            foreach ($teamUsers as $teamUser) {

                if ($teamUser->user_id == $deleteUserId) {

                    $this->deleteObjectsAccess($company->id, $deleteUserId);
                    $teamUser->verification = CompanyUsers::STATUS_INACTIVE;
                    $teamUser->typeRole = $teamUser::TYPE_ROLE_USER_INVIT_USER;
                    $teamUser->save();

                    Yii::$app->response->redirect(\yii\helpers\Url::current(['delete_user_id' => null]));
                }
            }
        }

        // Если пришел запрос на добавление пользователя к команде
        if (!empty($userId) && in_array($userId, ArrayHelper::getColumn($teamUsers, 'id')) && $canEditTeam) {

           foreach ($teamUsers as $teamUser) {

               if($teamUser->id == $userId) {
                   $teamUser->verification = CompanyUsers::STATUS_ACTIVE;
                   $teamUser->typeRole =  $teamUser::TYPE_ROLE_USER_INVIT_USER;
                   $teamUser->save();
                   Yii::$app->response->redirect(\yii\helpers\Url::current(['user_id' => null]));
               }
           }
           // $company->populateRelation('users', $teamUsers);
        }

        $objectsIds = @Yii::$app->request->post('CompanyUsers')['objectsIds'];
        if($objectsIds && $canEditTeam) {

            $userIds = [];
            $oldUserObjects = [];
            $currentUserObjects = [];
            // Получаем старые объекты, к которым был выдан доступ пользователям
            foreach ($objectsIds as $companyUserId => $objectsId) {
                $userIds[$companyUserId] = $companyUserId;
            }
            foreach ($userIds as $userId) {
                $oldUserObjects[$userId] = ArrayHelper::map(CompanyUserAccess::find()
                    ->where(['user_id' => $userId])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['class_name' => ClassMap::getId(Objects::class)])
                    ->andWhere(['type_access' => CompanyUserAccess::TYPE_ACCESS_ALLOW])->asArray()->all(),
                    'class_id', 'class_id');
            }

            foreach ( $objectsIds as $companyUserId => $objectsId) {
                $this->deleteObjectsAccess($company->id, $companyUserId);


                if (!empty($objectsId)) {
                    foreach ($objectsId as $id) {
                        $access = new CompanyUserAccess();

                        $access->class_id = $id;
                        $access->class_name = ClassMap::getId(Objects::class);;
                        $access->type_access = CompanyUserAccess::TYPE_ACCESS_ALLOW;
                        $access->user_id = $companyUserId;
                        $access->company_id = $company->id;
                        $access->save();
                        $currentUserObjects[$companyUserId][] = $id;
                    }
                }
            }

            // Если достьуп к объектам для пользователей менялся - отправляю письмо с уведомлением об изменении доступа к объектам
            foreach ($userIds as $userId) {
                $user = UserIdentity::find()->where(['id' => $userId])->one();
                // Вычисляю расхождение между новыми и старыми значениями
                $objectsDiffIds = ArrayHelper::merge(
                    (array_diff($currentUserObjects[$userId], $oldUserObjects[$userId])),
                    (array_diff($oldUserObjects[$userId], $currentUserObjects[$userId]))
                );

                if (!empty($objectsDiffIds) && !empty($user->email)) {
                   // Доступ к объектам изменен - отправляю письмо пользователю
                    $this->changeAccessObjectForUserEmail($user, $company, !empty($currentUserObjects[$userId]) ? $currentUserObjects[$userId] : []);
                }
            }


        }


        return $this->controller->render($this->params['template'], [
            'company' => $company,
            'teamUsers' => $teamUsers,
            'objectsIds' => $objectsIds,
            'canEditTeam' => $canEditTeam
        ]);
    }

    /**
     * Удаление прав по объектам
     *
     * @param $companyId
     * @param $companyUserId
     *
     * @return void
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    private function deleteObjectsAccess($companyId, $companyUserId) {
        $className = ClassMap::getId(Objects::class);
        $command = Yii::$app->db->createCommand();
        // Удаляем старые права по объектам
        return $command->delete(CompanyUserAccess::tableName(), 'company_id=:company_id AND user_id=:user_id AND class_name=:class_name',
            [':company_id' => $companyId, ':user_id' => $companyUserId, ':class_name' => $className])->execute();
    }


    /**
     * Отправка письма после изменения доступа к объектам для пользователя
     *
     * @param object $user
     * @param object $company
     * @param array $objectsIds
     * @return bool
     */
    private function changeAccessObjectForUserEmail($user, $company, $objectsIds = [])
    {
        $viewEmail = 'add_access_to_objects';
        $subjectEmail = 'TENDEROS. Предоставлен доступ к объектам компании.';
        $email = $user->email;
        $userName = $user->username;
        $paramView['email'] = $email;
        $paramView['objects'] = !empty($objectsIds) ? ArrayHelper::map(Objects::find()->where(['in', 'id', $objectsIds])->asArray()->all(), 'id', 'title') : [];
        $paramView['userName'] = $userName;
        $paramView['companyName'] = $company->title;


        return SendMail::send(
            $viewEmail,
            $paramView,
            Yii::$app->params['infoEmailFrom'],
            [$email => $userName],
            $subjectEmail
        );
    }
}
