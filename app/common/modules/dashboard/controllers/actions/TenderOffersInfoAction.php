<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class TenderOffersInfoAction
 * @package common\modules\dashboard\controllers\actions
 */
class TenderOffersInfoAction extends BaseAction
{
    /**
     * Вывод информации по тендеру
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function run()
    {

        $this->controller->layout = $this->layout;
        $companyUrl = $this->params['companyUrl'];
        $tenderId = $this->params['tenderId'] ?? Yii::$app->request->get('tenderId');
        $tender = Tenders::find()->where(['id' => $tenderId])->one();

        $companyUser = Yii::$app->user->getCompany()->one();

        if(!$tender) {
            throw new NotFoundHttpException('Данного тендера не существует');
        }
        return $this->controller->render(
            $this->params['template'],
            [
                'tender' => $tender,
                'companyUrl' => $companyUrl,
                'companyUser' => $companyUser,
                'offer' => new Offers()
            ]
        );
    }
}