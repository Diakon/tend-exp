<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use frontend\widgets\UserProfileEdit;
use Yii;

/**
 * Class ProfileEditAction
 * @package common\modules\dashboard\controllers\actions
 */
class ProfileEditAction extends BaseAction
{
    /**
     * Редактирование пользователя.
     *
     * @return string
     */
    public function run()
    {
        $this->controller->layout = $this->layout;
        $data = UserProfileEdit::widget([
            'userId' => Yii::$app->user->id,
            'params' => $this->params,
        ]);

        return $this->renderAction('//content', ['content' => $data]);
    }
}
