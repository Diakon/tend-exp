<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\models\GeoCountry;
use common\modules\company\models\frontend\Company;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class CompanyDetailAction
 * @package common\modules\company\controllers\actions
 */
class CompanyEditAction extends BaseAction
{
    /**
     * Вывод простой карточки элемента.
     *
     * @param $url
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function run($url)
    {
        $this->controller->layout = $this->layout;

        /**
         * @var Company $model
         */
        $model = Company::findOne(['id'=> Yii::$app->user->getActiveCompanyData()['id']]);

        if (empty($model)) {
            throw new NotFoundHttpException();
        }
        // Назначаем параметры адресов в переменные (для редактирования формы компании)
        $model->setAddressParam();
        $model->setCharsParam();
        $model->setScenario($model::SCENARIO_EDIT_COMPANY);

        // Если для страны не выбрана страна, то ставлю страну первую в списке (РФ)
        if (empty($model->charsCountryIds)) {
            $model->charsCountryIds[] = GeoCountry::find()->one()->id;
        }

        // $model->updateCompanyAddress();
        $model->load(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->updateCompanyChildRecords() && $model->save()) {
            //Делаем редирект, т.к. может измениться url компании, если меняют название
            $this->controller->redirect(['/dashboard/dashboard/company_info']);
        }

        return $this->controller->render($this->params['template'], [
            'model' => $model
        ]);

    }
}
