<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\Response;
use frontend\modules\upload\models\Upload;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class DeleteAvatar.
 * @package common\modules\dashboard\controllers\actions
 */
class DeleteAvatar extends BaseAction
{
    /**
     * Удаление авы
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function run()
    {
        if (!Yii::$app->user->isGuest && YII::$app->request->isAjax) {

            if (Yii::$app->request->post('avatarRemove') == true) {
                /** @var Upload $avatar */
                $avatarModel = Yii::$app->user->identity->avatar;
                $avatar = $avatarModel ?? new Upload();

                if (!$avatar->isNewRecord && $avatar->delete()) {
                    $avatar->deleteFiles();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    echo Json::encode(['data' => 'true', 'src' => Yii::getAlias('@static'). '/../content/images/helment.svg']);
                    Yii::$app->end();
                }
            }
        }

        echo Json::encode(['data' => 'false']);
        Yii::$app->end();
    }
}
