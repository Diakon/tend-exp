<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\frontend\Tenders;
use frontend\models\EntityComment;
use Yii;
use yii\data\ActiveDataProvider;
use frontend\modules\uwm\Module as Uwm;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class TenderOffersListAction
 * @package common\modules\dashboard\controllers\actions
 */
class TenderOffersListAction extends BaseAction
{
    /**
     * Список предложений по тендеру
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $this->controller->layout = $this->layout;
        $companyUrl = $this->params['companyUrl'];
        $tenderId = $this->params['tenderId'] ?? Yii::$app->request->get('tenderId');
        $tender = Tenders::find()->where(['id' => $tenderId])->one();
        $typeList = Yii::$app->request->get('type');

        if(!$tender) {
            throw new NotFoundHttpException('Данной компании не существует');
        }

        //Если пришел аякс запрос - обрабатываю
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            // Это победитель тендера
            if (!empty(Yii::$app->request->post('winnerOfferId'))) {
                $result = $tender->setWinnerTender(Yii::$app->request->post('winnerOfferId', 0));
                Yii::$app->response->statusCode = 200;
                $msg = 'Ошибка выбора победителя тендера ' . $tender->title . ' №' . $tender->id;
                if (!empty($result)) {
                    $offer = Offers::find()->where(['id' => Yii::$app->request->post('winnerOfferId')])->one();
                    $msg = 'Компания ' . $offer->company->title . ' выбрана в качестве победителя тендера ' . $tender->title . ' №' . $tender->id;
                }
                echo Json::encode([
                    'status' => !empty($result) ? 'success' : 'error',
                    'msg' => $msg,
                ]);
            }

            exit();
        }

        $model = new Offers();
        $compareModel = $model::classNameShort();
        $offersCompareId = $model::compareIds($tender->id);
        $countCompare = count($offersCompareId);

        $dataProvider = $model->geOffersTender(Yii::$app->request, $tender->id);
        // Получаю ID лучшего предложения
        $bestOfferId = Offers::getBestOfferTender($tender->id)->id ?? null;

        $offerIds = ArrayHelper::map(Offers::find()->where(['tender_id' => $tender->id])->asArray()->all(), 'id', 'id');
        $comments = [];
        foreach (EntityComment::find()->where(['in', 'offerId', $offerIds])->asArray()->all() as $comment) {
            $id = $comment['offerId'];
            $comments[$id][] = $comment;
        }

        return $this->controller->render(
            $this->params['template'],
            [
                'model' => $model,
                'companyUrl' => $companyUrl,
                'tender' => $tender,
                'dataProvider' => $dataProvider ?? new ActiveDataProvider(),
                'typeList' => $typeList,
                'bestOfferId' => $bestOfferId,
                'countCompare' => $countCompare,
                'compareModel' => $compareModel,
                'comments' => $comments
            ]
        );
    }
}