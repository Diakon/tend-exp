<?php

namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\AuthDbManager;
use common\modules\company\models\frontend\Company;
use common\modules\company\models\frontend\CompanyUsers;
use common\modules\tender\models\frontend\Objects;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\web\NotFoundHttpException;

/**
 * Class ObjectsListAction
 * @package common\modules\tender\controllers\actions
 */
class ObjectsListAction extends BaseAction
{

    public $objectsEmployee = false;

    /**
     * Список проектов доступных для просмотра пользователю.
     *
     * @param $companyUrl
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function run($companyUrl)
    {
        /**
         * @var Company $company
         */
        $company = Yii::$app->user->getCompany($companyUrl)->one();

        if (!$company) {
            throw new NotFoundHttpException('Данной компании не существует');
        }
        /**
         * @var $companyUser CompanyUsers
         */
        $companyUser = $company->companyUser;

//        Dev::dump($company->users);
//       // Dev::dump($companyUser->attributes);
//        die();

        $model = new Objects();
        $isPortfolio = intval(Yii::$app->request->get('portfolio', Objects::ALL_OBJECTS));
        $this->controller->layout = $this->layout;
        $user = Yii::$app->user;

        $with = ['activities', 'types', 'functions', 'tenders', 'address', 'company', 'user'];

        $sort = new Sort([
            'attributes' => [
                'status_complete_id' => [
                    'asc' => [
                        $model::tableName() . '.status_complete_id' => SORT_ASC,
                    ],
                    'desc' => [
                        $model::tableName() . '.status_complete_id' => SORT_DESC,
                    ],

                ],

            ],
            'defaultOrder' => [
                'status_complete_id' => SORT_ASC,
            ],
        ]);

        // Если админ, то возвращаем все объекта компании, если обычнвый клиент, то те к которым пользователь имеет доступ.
        if ($user->can(AuthDbManager::ROLE_ADMIN)) {
            $query = $company->getObjects()->with($with)
                ->where('employerName IS NOT NULL OR employerName != ""');

            if ($isPortfolio != Objects::ALL_OBJECTS) {
                $query->andFilterWhere(['and', ['=', Objects::tableName() . '.show_portfolio', $isPortfolio]]);
            }

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'totalCount' => $query->count(),
                    'pageSize' => YII_ENV == 'dev' ? 1 : 10,
                    'pageSizeParam' => false,
                ],
                'sort' => $sort,
            ]);
        } else {

            // Если показывать только свои объекты
            if ($this->objectsEmployee == false) {

                // $dataProvider = $model->getUserProjects($companyUser, ['!=', 'employerName' => '']);
                $query = Objects::getObjectsUser($companyUser)->with($with)->andWhere('employerName IS NOT NULL OR employerName !=""');
                $query->andWhere([Objects::tableName() . '.company_id' => $company->id]);
            } else {
                $query = Objects::getObjectsUserEmployee($companyUser)->with($with)->andWhere('employerName IS NULL OR employerName = ""');
            }

            if ($isPortfolio != Objects::ALL_OBJECTS) {
                $query->andFilterWhere(['and', ['=', Objects::tableName() . '.show_portfolio', $isPortfolio]]);
            }
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'totalCount' => $query->count(),
                    'pageSize' => YII_ENV == 'dev' ? 3 : 10,
                    'pageSizeParam' => false,
                ],
                'sort' => $sort,
            ]);
        }

        return $this->controller->render(
            $this->params['template'],
            [
                'model' => $model,
                'companyUrl' => $companyUrl,
                'portfolio' => $isPortfolio,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
