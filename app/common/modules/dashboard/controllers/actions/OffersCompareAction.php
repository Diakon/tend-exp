<?php
namespace common\modules\dashboard\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\ClassMap;
use common\modules\directories\models\common\Currency;
use common\modules\offers\models\frontend\Offers;
use common\modules\offers\models\frontend\OffersElements;
use common\modules\tender\models\frontend\EstimatesElements;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use frontend\modules\uwm\Module as Uwm;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class OffersCompareAction
 * @package common\modules\dashboard\controllers\actions
 */
class OffersCompareAction extends BaseAction
{
    /**
     * Сравнение предложений по тендеру
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $this->controller->layout = $this->layout;
        $this->controller->view->params['isLongView'] = true;

        $companyUrl = $this->params['companyUrl'];
        $tenderId = $this->params['tenderId'] ?? Yii::$app->request->get('tenderId');
        $tender = Tenders::find()->where(['id' => $tenderId])->one();

        if(!$tender) {
            throw new NotFoundHttpException('Тендер не существует');
        }

        $model = new Offers();
        $offersCompareId = Offers::compareIds($tender->id);

        if(!$offersCompareId) {
            throw new NotFoundHttpException('Вы не выбрали предложения для сравнения');
        }

        // Получаю ID всех предложений для этого тендера
        $offersIds = ArrayHelper::map(Offers::find()->where(['tender_id' => $tender->id])->andWhere(['is_archive' => Offers::IS_ARCHIVE_NO])->asArray()->all(), 'id', 'id');

        // Получаю предложения, которые были выбраны ждя сравнения
        $offersData = $model::find()->where(['in', 'id', $offersCompareId])->andWhere(['tender_id' => $tender->id])->andWhere(['is_archive' => Offers::IS_ARCHIVE_NO])->orderBy(['sum' => SORT_ASC])->all();



        // Собираю данные по пунктам сметы
        $issetAddWorks = false;
        $otherOfferEstimatesData = [];
        $estimatesOfferData = [];
        foreach ($offersData as $offerData) {
            foreach ($offerData->getElements()->asArray()->all() as $offerElement) {
                if ($offerElement['class_name'] == ClassMap::CLASS_ESTIMATES_RUBRICS_ID) {
                    $id = $offerElement['class_id'];
                    $estimatesOfferData[$offerData->id]['rubrics'][$id]['noDiscount'] = $offerElement['sum'];
                    $estimatesOfferData[$offerData->id]['rubrics'][$id]['withDiscounPriceOne'] = $offerElement['count'] > 0 ? round($offerElement['sum_discount'] / $offerElement['count']) : 0;
                    $estimatesOfferData[$offerData->id]['rubrics'][$id]['withDiscount'] = $offerElement['sum_discount'];
                    $estimatesOfferData[$offerData->id]['rubrics'][$id]['discount'] = $offerElement['discount'];
                    //Получаю данные по другим предложениям для этой рубрики
                    foreach (OffersElements::find()
                                 ->where(['in', 'offer_id', $offersIds])
                                 ->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID])
                                 ->andWhere(['class_id' => $id])
                                 ->orderBy(['sum' => SORT_ASC])
                                 ->all() as $dataElement)
                    {
                        if (empty($dataElement->offer->company)) {
                            continue;
                        }
                        $companyId = $dataElement->offer->company->id;
                        $otherOfferEstimatesData[ClassMap::CLASS_ESTIMATES_RUBRICS_ID][$id][$companyId]['company'] = $dataElement->offer->company->title;
                        $otherOfferEstimatesData[ClassMap::CLASS_ESTIMATES_RUBRICS_ID][$id][$companyId]['sun'] = $dataElement->sum;
                    }
                } else {
                    $key = !empty($offerElement['class_id']) ? 'elements' : 'addWorks'; // Если у работу указан ID записи в смете тендера - это работа тендера, если нет - это доп. работа
                    $id = $key == 'elements' ? $offerElement['class_id'] : $offerElement['id'];
                    $estimatesOfferData[$offerData->id][$key][$id]['priceWork'] = $offerElement['price_work'];
                    $estimatesOfferData[$offerData->id][$key][$id]['priceMaterial'] = $offerElement['price_material'];
                    $estimatesOfferData[$offerData->id][$key][$id]['sumPrice'] = $offerElement['price_material'] + $offerElement['price_work'];
                    $estimatesOfferData[$offerData->id][$key][$id]['noDiscount'] = $offerElement['sum'];
                    $estimatesOfferData[$offerData->id][$key][$id]['withDiscount'] = $offerElement['sum_discount'];
                    $estimatesOfferData[$offerData->id][$key][$id]['discount'] = $offerElement['discount'];
                    if ($key == 'addWorks') {
                        $estimatesOfferData[$offerData->id][$key][$id]['id'] = $offerElement['id'];
                        $estimatesOfferData[$offerData->id][$key][$id]['title'] = $offerElement['title'];
                        $estimatesOfferData[$offerData->id][$key][$id]['count'] = $offerElement['count'];
                        $estimatesOfferData[$offerData->id][$key][$id]['unit_id'] = $offerElement['unit_id'];
                        $issetAddWorks = true;
                    }
                }
            }
        }

        // Получаю ID лучшего предложения
        $bestOfferId = Offers::getBestOfferTender($tender->id)->id ?? null;

        return $this->controller->render(
            $this->params['template'],
            [
                'model' => $model,
                'companyUrl' => $companyUrl,
                'tender' => $tender,
                'offersData' => $offersData,
                'currency' => Currency::getCurrencyList(),
                'units' => EstimatesElements::getUnits(),
                'estimatesOfferData' => $estimatesOfferData,
                'otherOfferEstimatesData' => $otherOfferEstimatesData,
                'issetAddWorks' => $issetAddWorks,
                'bestOfferId' => $bestOfferId
            ]
        );
    }
}