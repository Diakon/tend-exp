<?php
namespace common\modules\dashboard\controllers;

use modules\crud\actions\DetailAction;
use common\components\AuthDbManager;
use common\modules\company\controllers\actions\DeleteCompanyAvatar;
use common\modules\dashboard\controllers\actions\CompanyInfo;
use common\modules\dashboard\controllers\actions\DeleteAvatar;
use common\modules\dashboard\controllers\actions\MyCompanyList;
use common\modules\dashboard\controllers\actions\ObjectsListAction;
use common\modules\dashboard\controllers\actions\OffersCompareAction;
use common\modules\dashboard\controllers\actions\TariffEditAction;
use common\modules\dashboard\controllers\actions\TeamEditAction;
use common\modules\dashboard\controllers\actions\TenderOffersInfoAction;
use common\modules\dashboard\controllers\actions\TenderOfferViewAction;
use common\modules\offers\controllers\actions\OfferAddAction;
use common\modules\dashboard\controllers\actions\ObjectEditAction;
use common\modules\tender\controllers\actions\TenderEditAction;
use common\modules\dashboard\controllers\actions\ProfileEditAction;
use common\modules\dashboard\controllers\actions\TendersListAction;
use common\modules\dashboard\controllers\actions\TenderOffersListAction;
use common\modules\company\models\frontend\Company;
use common\modules\tender\models\frontend\Objects;
use common\modules\tender\models\frontend\Tenders;
use frontend\controllers\FrontendController;
use frontend\modules\upload\models\Upload;
use frontend\widgets\CompanyTendersList;
use yii\filters\AccessControl;
use common\modules\dashboard\controllers\actions\CompanyEditAction;
use Yii;

/**
 * Основной контроллер для действий авторизованого пользователя.
 */
class DashboardController extends FrontendController
{
    /**
     * @var string|boolean the name of the layout to be applied to this controller's views.
     */
    public $layout = '';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['companies', 'delete_avatar', 'delete_avatar_company'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['company_edit', 'team_edit', 'tariff_edit'],
                        'roles' => ['clienteditCompany'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('url'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['company_info'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['profile_edit'],
                        'roles' => [AuthDbManager::ROLE_CLIENT],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['objects', 'objects_employee'],
                        'roles' => ['accessProject', AuthDbManager::ROLE_ADMIN],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['object_edit'],
                        'roles' => ['editProject', AuthDbManager::ROLE_ADMIN],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['object_add'],
                        'roles' => ['addProject', AuthDbManager::ROLE_ADMIN],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tenders'],
                        'roles' => ['accessTender'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tenders_partners'],
                        'roles' => ['accessTender'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tender_add'],
                        'roles' => ['addTender'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tender_edit'],
                        'roles' => ['editTender'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                                'id' => Yii::$app->request->get('id'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['offer_add'],
                        'roles' => ['addOffer'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tender_offers_list'],
                        'roles' => ['accessTender'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                                'tenderId' => Yii::$app->request->get('tenderId'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tender_offers_info'],
                        'roles' => ['accessTender'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                                'tenderId' => Yii::$app->request->get('tenderId'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['offers_compare'],
                        'roles' => ['accessTender'],
                        'roleParams' => function () {
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                                'tenderId' => Yii::$app->request->get('tenderId'),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tender_offer_view'],
                        'roles' => ['editTender'],
                        'roleParams' => function(){
                            return [
                                'companyUrl' => Yii::$app->request->get('companyUrl'),
                                'offerId' => Yii::$app->request->get('id'),
                                'tenderId' => Yii::$app->request->get('tenderId')
                            ];
                        }
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            // Список компаний
            'companies' => [
                'class' => MyCompanyList::class,
                'params' => [
                    'template' => '/frontend/companies_list'
                    ]
            ],
            'delete_avatar' => [
                'class' => DeleteAvatar::class,
            ],
            'delete_avatar_company' => [
                'class' => DeleteCompanyAvatar::class,
            ],
            // Информация о компании
            'company_info' => [
                'class' => CompanyInfo::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Company::class,
                    'template' => '/frontend/company_info',
                ],
            ],
            // Редактировать информация о компании
            'company_edit' => [
                'class' => CompanyEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Company::class,
                    'template' => '/frontend/company_edit',
                ],
            ],
            'team_edit' => [
                'class' => TeamEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Company::class,
                    'template' => '/frontend/team_edit',
                ],
            ],
            'tariff_edit' => [
                'class' => TariffEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Company::class,
                    'template' => '/frontend/tariff_edit',
                ],
            ],
            // Редактировать информация о пользователе
            'profile_edit' => [
                'class' => ProfileEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/inner',
                'params' => [
                    'template' => '/frontend/profile_edit',
                ],
            ],
            // Просмотр списка объектов компании
            'objects' => [
                'class' => ObjectsListAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'template' => '/frontend/objects_list',
                    'companyUrl' => Yii::$app->request->get('companyUrl')
                ],
            ],

            'objects_employee' => [
                'class' => ObjectsListAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'objectsEmployee' => true, // Показываем объекты заказчика, в которых мы учавствовали
                'params' => [
                    'template' => '/frontend/objects_list',
                    'companyUrl' => Yii::$app->request->get('companyUrl'),
                ],
            ],
            // Создать новый объект
            'object_add' => [
                'class' => ObjectEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Objects::class,
                    'template' => '/frontend/object_edit',
                ],
            ],
            // Редактировать объект
            'object_edit' => [
                'class' => ObjectEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Objects::class,
                    'template' => '/frontend/object_edit',
                ],
            ],

            // Просмотр списка тендеров компании
            'tenders' => [
                'class' => TendersListAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'template' => '/frontend/tenders/tenders_list',
                    'companyUrl' => Yii::$app->request->get('companyUrl'),
                ],
            ],
            // Просмотр списка тендеров в которых учатсвует моя компания (участвую)
            'tenders_partners' => [
                'class' => TendersListAction::class,
                'params' => [
                    'template' => '/frontend/tenders/tenders_list',
                    'companyUrl' => Yii::$app->request->get('companyUrl'),
                    'typeList' => CompanyTendersList::TYPE_TENDERS_LIST_PARTNER
                ],
            ],
            // Создать тендер
            'tender_add' => [
                'class' => TenderEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Tenders::class,
                    'template' => '/frontend/tenders/tender_edit',
                ],
            ],
            // Редактировать тендер
            'tender_edit' => [
                'class' => TenderEditAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'model' => Tenders::class,
                    'template' => '/frontend/tenders/tender_edit',
                ],
            ],
            // Оставить предложение к тендеру
            'offer_add' => [
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'class' => OfferAddAction::class,
                'params' => [
                    'tenderId' => Yii::$app->request->get('tenderId'),
                    'template' => '/frontend/offers/offer_add',
                ],
            ],
            // Просмотр списка предложений для тендера (со стороны автора тендера)
            'tender_offers_list' => [
                'class' => TenderOffersListAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'template' => '/frontend/offers/offers_list',
                    'companyUrl' => Yii::$app->request->get('companyUrl'),
                    'tenderId' => Yii::$app->request->get('id')
                ],
            ],

            // Просмотр информации о тендере в списке предложений
            'tender_offers_info' => [
                'class' => TenderOffersInfoAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'template' => '/frontend/offers/tender_info',
                    'companyUrl' => Yii::$app->request->get('companyUrl'),
                    'tenderId' => Yii::$app->request->get('id')
                ],
            ],
            // Просмотр предложений добавленых в сравнение
            'offers_compare' => [
                'class' => OffersCompareAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'template' => '/frontend/offers/compare',
                    'companyUrl' => Yii::$app->request->get('companyUrl'),
                    'tenderId' => Yii::$app->request->get('id')
                ],
            ],
            // Просмотр предложения для тендера (со стороны автора тендера)
            'tender_offer_view' => [
                'class' => TenderOfferViewAction::class,
                'layout' => '@root/themes/frontend/views/layouts/lk',
                'params' => [
                    'template' => '/frontend/tenders/tender_offer_view',
                    'companyUrl' => Yii::$app->request->get('companyUrl'),
                    'offerId' => Yii::$app->request->get('id'),
                    'tenderId' => Yii::$app->request->get('tenderId')
                ],
            ],
        ];
    }


}
