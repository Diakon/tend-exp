<?php
namespace common\modules\company\models\backend;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * CompanyAddress
 */
class CompanyAddress extends \common\modules\company\models\common\CompanyAddress
{

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * Возвращает dataProvider для списка орегионов
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['title' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Company::class,
                    'title' => Yii::t('app', 'Адреса компании'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_update'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * Возвращает массив из адресов
     */
    public static function getCompanyAddressList()
    {
        $returnAddress = [];
        foreach (CompanyAddress::find()->each(100) as $address) {
            $returnAddress[$address->id] = self::getAddress($address);
        }

        return $returnAddress;
    }

    /**
     * Возвращает адрес
     *
     * $model CompanyAddress
     */
    public static function getAddress($model)
    {
        $address = trim($model->country->name . ', г. ' . $model->city->name . ', ул. ' . $model->street . ', дом ' . $model->house . ' ' . $model->structure)
            . '. Индекс ' . $model->index;
        return $address;
    }


    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => Yii::t('app', 'Адреса компаний')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => Yii::t('app', 'Адреса компаний'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? Yii::t('app', 'Создать адрес') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getDeleteLink($action = 'company_address_delete', array $params = [])
    {
        return $this->generateUrl($action, $params, [static::$paramIdent => $this->{static::$paramColumn}]);
    }
}
