<?php
namespace common\modules\company\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use common\models\UserIdentity;
use yii\helpers\ArrayHelper;


/**
 * Конфигурация формы списка пользователей компаний
 *
 * Class CompanyUsers
 *
 * @package common\modules\company\models\backend\forms
 */
class CompanyUsers extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Пользователи компании</h2>';
        $data[] = '<div class="panel-body">';
        $data['company_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\company\models\common\Company::getCompanyList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['user_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(UserIdentity::find()->asArray()->all(), 'id', 'email'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['type_role'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\company\models\backend\CompanyUsers::$typeRoleList,
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['experience'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\company\models\backend\CompanyUsers::getExperienceList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['position'] = ['type' => self::TYPE_TEXT];
        $data['status'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';


        return $data;
    }

}
