<?php
namespace common\modules\company\models\backend\forms;

use backend\models\GeoCity;
use backend\models\GeoCountry;
use modules\crud\behaviors\FormConfigBehavior;
use modules\upload\widgets\UploadStaticWidget;
use common\models\GeoRegion;
use common\modules\company\models\backend\CompanyAddress;
use common\modules\directories\models\common\EmployeesMax;
use common\modules\directories\models\common\EmployeesNow;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\directories\models\common\MainWork;
use common\modules\directories\models\common\TypesWork;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * Конфигурация формы списка компаний
 *
 * Class Company
 *
 * @package common\modules\company\models\backend\forms
 */
class Company extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;
        $model->setAddressParam();
        
        $data = [];
        // Основные
        $data[] = '<h2>Компания ' . $model->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['url'] = ['type' => self::TYPE_TEXT];
        $data['title'] = ['type' => self::TYPE_TEXT];
        $data['inn'] = ['type' => self::TYPE_TEXT];
        $data['ogrn'] = ['type' => self::TYPE_TEXT];
        $data['kpp'] = ['type' => self::TYPE_TEXT];
        $data['general_manage'] = ['type' => self::TYPE_TEXT];
        $data['year'] = ['type' => self::TYPE_TEXT];
        $data['status'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data['tariff_plan_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(\common\modules\tariff\models\backend\TariffPlans::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['employees_now'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(EmployeesNow::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['employees_max'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(EmployeesMax::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];

        $data[] = '<b>Логотип</b>';
        $data[] = UploadStaticWidget::widget([
            'model' => $this->owner,
            'attribute' => 'avatar',
        ]);
        $data[] = '</fieldset>';
        $data[] = '</div>';

        // Контактная информация
        $data[] = ['type' => 'tab', 'label' => Yii::t('app', 'Контактная информация')];
        $data[] = '<div class="panel-body">';
        $data[] = '<fieldset class="form-horizontal">';
        $data['fax'] = ['type' => self::TYPE_TEXT];
        $data['phone'] = ['type' => self::TYPE_TEXT];



        $data[] = '<p><b>Юридический адрес:</b></p>';
        $data['legalAddressCountryId'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoCountry::getCountryList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['legalAddressRegionId'] = array(
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoRegion::getRegionsList(),
            'attributes' => array(
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ),
        );
        $data['legalAddressCityId'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoCity::getCityList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['legalAddressStreet'] = ['type' => self::TYPE_TEXT];
        $data['legalAddressHouse'] = ['type' => self::TYPE_TEXT];
        $data['legalAddressStructure'] = ['type' => self::TYPE_TEXT];
        $data['legalAddressIndex'] = ['type' => self::TYPE_TEXT];

        $data[] = '<p><b>Фактический адрес:</b></p>';
        $data['actualAddressCountryId'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoCountry::getCountryList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['actualAddressRegionId'] = array(
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoRegion::getRegionsList(),
            'attributes' => array(
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ),
        );
        $data['actualAddressCityId'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoCity::getCityList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['actualAddressStreet'] = ['type' => self::TYPE_TEXT];
        $data['actualAddressHouse'] = ['type' => self::TYPE_TEXT];
        $data['actualAddressStructure'] = ['type' => self::TYPE_TEXT];
        $data['actualAddressIndex'] = ['type' => self::TYPE_TEXT];

        $data['address_same'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ]
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';


        // Характеристики
        $data[] = ['type' => 'tab', 'label' => Yii::t('app', 'Характеристики')];
        $data[] = '<div class="panel-body">';
        $data[] = '<fieldset class="form-horizontal">';
        $data['charsRegionsIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoRegion::getRegionsList(),
            'attributes' => [
                'multiple' => 'multiple',
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['charsRegionsIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoRegion::getRegionsList(),
            'attributes' => [
                'multiple' => 'multiple',
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['charsFunctionsIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => FunctionsCompany::findActiveList(),
            'attributes' => [
                'multiple' => 'multiple',
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['charsBuildsIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => MainWork::findActiveList(),
            'attributes' => [
                'multiple' => 'multiple',
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['charsWorksIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => TypesWork::findActiveList(),
            'attributes' => [
                'multiple' => 'multiple',
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';




        return $data;
    }
}
