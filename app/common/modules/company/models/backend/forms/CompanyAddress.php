<?php
namespace common\modules\company\models\backend\forms;

use backend\models\GeoCountry;
use backend\models\GeoRegion;
use modules\crud\behaviors\FormConfigBehavior;
use common\models\GeoCity;



/**
 * Конфигурация формы списка адресов компаний
 *
 * Class Company
 *
 * @package common\modules\company\models\backend\forms
 */
class CompanyAddress extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Адреса компании ' . $model->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['country_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoCountry::getCountryList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['region_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoRegion::getRegionsList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['city_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => GeoCity::getCityList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['street'] = ['type' => self::TYPE_TEXT];
        $data['house'] = ['type' => self::TYPE_TEXT];
        $data['structure'] = ['type' => self::TYPE_TEXT];
        $data['index'] = ['type' => self::TYPE_TEXT];


        $data[] = '</fieldset>';
        $data[] = '</div>';


        return $data;
    }

}
