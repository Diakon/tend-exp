<?php
namespace common\modules\company\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use yii\helpers\ArrayHelper;
use common\models\UserIdentity;


/**
 * Конфигурация формы списка прав доступа к проектам для пользователей компаний
 *
 * Class CompanyUserAccess
 *
 * @package common\modules\company\models\backend\forms
 */
class CompanyUserAccess extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Права доступа к проектам для пользователя компании</h2>';
        $data[] = '<div class="panel-body">';
        $data['company_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\company\models\common\Company::getCompanyList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['user_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(UserIdentity::find()->asArray()->all(), 'id', 'email'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['objectModel'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(\common\modules\tender\models\backend\Objects::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['type_access'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\company\models\backend\CompanyUserAccess::getTypeAccessList(),
            'attributes' => [
                'class' => 'select_one form-control',
               // 'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';


        return $data;
    }

}
