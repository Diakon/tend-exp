<?php
namespace common\modules\company\models\backend;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CompanyUsers
 */
class CompanyUsers extends \common\modules\company\models\common\CompanyUsers
{

    /**
     * Фильтр компаний
     *
     * @var integer
     */
    public $filterCompanyId;

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['filterCompanyId'], 'integer'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'filterCompanyId' => 'Компания',
            ]
        );
    }

    /**
     * Возвращает dataProvider для списка пользователей организации
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['id' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Company::class,
                    'title' => Yii::t('app', 'Пользователи компании'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_update'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();

        $query->andFilterWhere(
            [
                'and',
                ['=', 'company_id', $this->filterCompanyId],
            ]
        );

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * Возвращает массив пользователей компаний
     *
     * @param null $companyId
     * @return array
     */
    public static function getUsers($companyId = null)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE]);
        if (!empty($companyId)) {
            $query->andWhere(['company_id' => $companyId]);
        }
        $users = [];

        foreach ($query->all() as $data) {
            $users[$data->id] = $data->user->getFullName() . ' (' . $data->user->email . ')';
        }

        return $users;
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => Yii::t('app', 'Пользователи компаний')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => Yii::t('app', 'Пользователи компаний'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? Yii::t('app', 'Создать пользователя') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getDeleteLink($action = 'company_users_delete', array $params = [])
    {
        return $this->generateUrl($action, $params, [static::$paramIdent => $this->{static::$paramColumn}]);
    }
}

