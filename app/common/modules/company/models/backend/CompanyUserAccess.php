<?php
namespace common\modules\company\models\backend;

use common\components\ClassMap;
use common\modules\tender\models\frontend\Objects;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CompanyUserAccess
 */
class CompanyUserAccess extends \common\modules\company\models\common\CompanyUserAccess
{
    /**
     * Объект / тендер для которого назначается доступ
     *
     * @var integer
     */
    public $objectModel;

    /**
     * Фильтр по компании
     *
     * @var integer
     */
    public $filterCompanyId;

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['objectModel'], 'required'],
                [['objectModel', 'filterCompanyId'], 'integer'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'objectModel' => 'Объект',
                'filterCompanyId' => 'Компания',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $model = $this->getModel()->one();
        $this->objectModel = !empty($model) ? $model->id : null;
    }


    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->objectModel)) {
            $this->class_name = ClassMap::getId(Objects::class);
            $this->class_id = $this->objectModel;
        }

        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает dataProvider для списка пользователей организации
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['id' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Company::class,
                    'title' => Yii::t('app', 'Пользователи компании'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_update'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();
        $query->andFilterWhere(
            [
                'and',
                ['=', 'company_id', $this->filterCompanyId],
            ]
        );

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * Возвращает массив пользователей компаний
     *
     * @return array
     */
    public static function getCompanyUserList()
    {
        $returnListData = [];
        foreach (CompanyUsers::find()->where(['status' => CompanyUsers::STATUS_ACTIVE])->all() as $data) {
            $returnListData[$data->id] = $data->user->fullName . ' в компании ' . $data->company->title;
        }

        return $returnListData;
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => Yii::t('app', 'Права доступа к проектам')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => Yii::t('app', 'Права доступа к проектам'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? Yii::t('app', 'Создать новые права доступа') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
