<?php
namespace common\modules\company\models\backend;
use backend\models\GeoRegion;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii2tech\ar\linkmany\LinkManyBehavior;

/**
 * Class Company
 * @package backend\models
 */
class Company extends \common\modules\company\models\common\Company
{
    /**
     * Юридический адрес - страна
     * @var integer
     */
    public $legalAddressCountryId;

    /**
     * Юридический адрес - регион
     * @var integer
     */
    public $legalAddressRegionId;

    /**
     * Юридический адрес - город
     * @var integer
     */
    public $legalAddressCityId;

    /**
     * Юридический адрес - улица
     * @var string
     */
    public $legalAddressStreet;

    /**
     * Юридический адрес - дом
     * @var string
     */
    public $legalAddressHouse;

    /**
     * Юридический адрес - корпус
     * @var string
     */
    public $legalAddressStructure;

    /**
     * Юридический адрес - индекс
     * @var string
     */
    public $legalAddressIndex;

    /**
     * Фактический адрес - страна
     * @var integer
     */
    public $actualAddressCountryId;

    /**
     * Фактический адрес - регион
     * @var integer
     */
    public $actualAddressRegionId;

    /**
     * Фактический адрес - город
     * @var integer
     */
    public $actualAddressCityId;

    /**
     * Фактический адрес - улица
     * @var string
     */
    public $actualAddressStreet;

    /**
     * Фактический адрес - дом
     * @var string
     */
    public $actualAddressHouse;

    /**
     * Фактический адрес - корпус
     * @var string
     */
    public $actualAddressStructure;

    /**
     * Фактический адрес - индекс
     * @var string
     */
    public $actualAddressIndex;

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;


    /**
     * Возвращает dataProvider для списка орегионов
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['title' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['fax'], 'safe'],
                [['general_manage'], 'string'],
                [['ogrn'], 'string', 'min' => 13, 'max' => 13],
                [['inn'], 'string', 'min' => 10, 'max' => 12],
                [['kpp'], 'string', 'min' => 9, 'max' => 9],
                [['legalAddressCountryId', 'legalAddressRegionId', 'legalAddressCityId'], 'integer'],
                [['actualAddressCountryId', 'actualAddressRegionId', 'actualAddressCityId'], 'integer'],
                [['legalAddressStreet', 'legalAddressHouse', 'actualAddressStreet', 'actualAddressHouse'], 'string', 'max' => 255],
                [['legalAddressStructure', 'legalAddressIndex', 'actualAddressStructure', 'actualAddressIndex'], 'string', 'max' => 20],

            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'legalAddressCountryId' => Yii::t('app', 'Страна (юридический адрес)'),
                'legalAddressRegionId' => Yii::t('app', 'Регион (юридический адрес)'),
                'legalAddressCityId' => Yii::t('app', 'Город (юридический адрес)'),
                'legalAddressStreet' => Yii::t('app', 'Улица (юридический адрес)'),
                'legalAddressHouse' => Yii::t('app', 'Дом (юридический адрес)'),
                'legalAddressStructure' => Yii::t('app', 'Корпус / строение (юридический адрес)'),
                'legalAddressIndex' => Yii::t('app', 'Индекс (юридический адрес)'),
                'actualAddressCountryId' => Yii::t('app', 'Страна (фактический адрес)'),
                'actualAddressRegionId' => Yii::t('app', 'Регион (фактический адрес)'),
                'actualAddressCityId' => Yii::t('app', 'Город (фактический адрес)'),
                'actualAddressStreet' => Yii::t('app', 'Улица (фактический адрес)'),
                'actualAddressHouse' => Yii::t('app', 'Дом (фактический адрес)'),
                'actualAddressStructure' => Yii::t('app', 'Корпус / строение (фактический адрес)'),
                'actualAddressIndex' => Yii::t('app', 'Индекс (фактический адрес)'),
            ]
        );
    }

    /**
     * Получает актуальный и физический адреса компании и назначает значения в параметры модели
     */
    public function setAddressParam()
    {
        $addressTypes = [
            'actual',
            'legal'
        ];
        $addressCompanyFields = [
            'AddressCountryId' => 'country_id',
            'AddressRegionId' => 'region_id',
            'AddressCityId' => 'city_id',
            'AddressStreet' => 'street',
            'AddressHouse' => 'house',
            'AddressStructure' => 'structure',
            'AddressIndex' => 'index'
        ];
        foreach ($addressTypes as $addressType) {
            $address = $addressType . 'Address';
            $address = $this->{$address};
            if (empty($address)) {
                continue;
            }
            foreach ($addressCompanyFields as $companyField => $addressField) {
                $companyField = $addressType . $companyField;
                $this->{$companyField} = $address->{$addressField};
            }
        }
    }

    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Company::class,
                    'title' => Yii::t('app', 'Компании'),
                ],
                [
                    'class' => CompanyAddress::class,
                    'title' => Yii::t('app', 'Адреса компании'),
                ],
                [
                    'class' => CompanyUsers::class,
                    'title' => Yii::t('app', 'Пользователи компании'),
                ],
                [
                    'class' => CompanyUserAccess::class,
                    'title' => Yii::t('app', 'Права доступа к проектам для пользователей компании'),
                ],
            ];
        }

        return self::$list;
    }

    /**
    * Возвращает список пунктов меню.
    *
    * @return array[]
    */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/company/company/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/company/company/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/company/company/' . $item['class']::classNameShort('id') . '_edit'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find()->andFilterWhere(['LIKE', 'title', $this->title]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->updateCompanyAddress();

            return true;
        }
        return false;
    }

    /**
     * Обновляет данные о юредическом и фактическом адресах
     */
    public function updateCompanyAddress()
    {
        $needRollBack = false;
        $transaction = CompanyAddress::getDb()->beginTransaction();
        try {
            // Фактический аддресс
            $modelActualAddress = empty($this->actualAddress) ? new CompanyAddress() : $this->actualAddress;
            $modelActualAddress->country_id = $this->actualAddressCountryId;
            $modelActualAddress->region_id = $this->actualAddressRegionId;
            $modelActualAddress->city_id = $this->actualAddressCityId;
            $modelActualAddress->street = $this->actualAddressStreet;
            $modelActualAddress->house = $this->actualAddressHouse;
            $modelActualAddress->structure = $this->actualAddressStructure;
            $modelActualAddress->index = $this->actualAddressIndex;
            if (!$modelActualAddress->save()) {
                $needRollBack = true;
            }

            // Юредический аддресс
            $modelLegalAddress = empty($this->legalAddress) ? new CompanyAddress() : $this->legalAddress;
            $modelLegalAddress->country_id = $this->legalAddressCountryId;
            $modelLegalAddress->region_id = $this->legalAddressRegionId;
            $modelLegalAddress->city_id = $this->legalAddressCityId;
            $modelLegalAddress->street = $this->legalAddressStreet;
            $modelLegalAddress->house = $this->legalAddressHouse;
            $modelLegalAddress->structure = $this->legalAddressStructure;
            $modelLegalAddress->index = $this->legalAddressIndex;
            if (!$modelLegalAddress->save()) {
                $needRollBack = true;
            }

            if (!$needRollBack) {
                $this->actual_address_id = $modelActualAddress->id;
                $this->legal_address_id = $modelLegalAddress->id;
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }

        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при сохранении адреса для компании ID = ' . $this->id . '. Сообщение валидатора: ' . $e->getMessage());
        }
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => Yii::t('app', 'Компании')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => Yii::t('app', 'Компании'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? Yii::t('app', 'Создать компанию') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getDeleteLink($action = 'company_delete', array $params = [])
    {
        return $this->generateUrl($action, $params, [static::$paramIdent => $this->{static::$paramColumn}]);
    }
}
