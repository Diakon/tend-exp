<?php
namespace common\modules\company\models\common;

use common\components\ClassMap;
use common\models\UserIdentity;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Доступы к проектам / тендерам компании.
 *
 * @property integer $id          Идентификатор.
 * @property integer $user_id     Пользователь в компании.
 * @property string $class_name   Модель.
 * @property string $class_id     ID модели.
 * @property integer $type_access Тип доступа.
 * @property integer $company_id  Компания к которой относится пользователь.
 * @property integer $created_at  Дата создания записи.
 * @property integer $updated_at  Дата изменения записи.
 */
class CompanyUserAccess extends ActiveRecord
{
    // const TYPE_ACCESS_READ = 1;
    const TYPE_ACCESS_ALLOW = 1;
    // const TYPE_ACCESS_EDIT = 2;

    /**
     * Список типов доступа
     *
     * @return array
     */
    public static function getTypeAccessList($id = null)
    {
        $data = [
            self::TYPE_ACCESS_ALLOW => Yii::t('app', 'Доступ разрешен'),
        ];

        return !empty($id) ? $data[$id] : $data;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_user_access}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['user_id', 'class_name', 'class_id', 'company_id'], 'unique',
                'targetAttribute' => ['user_id', 'class_name', 'class_id'],
                'comboNotUnique' => Yii::t('app', 'Указанный пользователь уже был назначен для этого проекта')
            ],
            [['user_id', 'class_name', 'class_id', 'type_access', 'company_id'], 'required'],
            [['user_id', 'class_id', 'type_access', 'company_id', 'created_at', 'updated_at'], 'integer'],
            [['class_name'], 'integer'],
            [['user_id'], 'validateUserCompany'],
        ]);
    }

    /**
     * Проверяю, что пользвователь назначен организации
     *
     * @inheritdoc
     */
    public function validateUserCompany($attribute, $params)
    {
        if (!empty($this->user_id) && !empty($this->company_id) && !CompanyUsers::inCompany($this->user_id, $this->company_id)) {
            $this->addError($attribute, 'Пользователь не закреплен за указанной компанией');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Пользователь в компании'),
            'company_id' => Yii::t('app', 'Компания'),
            'class_name' => Yii::t('app', 'Модель'),
            'class_id' => Yii::t('app', 'ID модели'),
            'type_access' => Yii::t('app', 'Тип доступа'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с пользователями
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'user_id']);
    }

    /**
     * Возвращает модель к которой указан доступ
     *
     * @return null
     */
    public function getModel()
    {
        if (!empty($this->class_name) && !empty($this->class_id)) {
            $model = ClassMap::getClassName($this->class_name);
            return $model::find()
                ->where(['id' => $this->class_id])
                ->andWhere(['status' => self::STATUS_ACTIVE]);

        }

        return null;
    }
}
