<?php
namespace common\modules\company\models\common;

use common\modules\directories\models\common\TypesWork;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Группы работ компании.
 *
 * @property integer     $id            Идентификатор.
 * @property integer     $company_id    Компания.
 * @property TypesWork[] $typeWork      Вид работы компании.
 * @property integer     $type_work_id  Вид работы компании.
 * @property integer     $created_at    Дата создания записи.
 * @property integer     $updated_at    Дата изменения записи.
 */
class CompanyCharsWorks extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_chars_works}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'type_work_id'], 'required'],
            [['company_id', 'type_work_id', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'type_work_id' => Yii::t('app', 'Вид работы компании'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с типом работ
     *
     * @return ActiveQuery
     */
    public function getTypeWork()
    {
        return $this->hasMany(TypesWork::class, ['id' => 'type_work_id']);
    }
}
