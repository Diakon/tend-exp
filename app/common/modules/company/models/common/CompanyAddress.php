<?php
namespace common\modules\company\models\common;

use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use common\models\GeoRegion;
use common\models\GeoCountry;
use common\models\GeoCity;

/**
 * Адреса компании.
 *
 * @property integer   $id          Идентификатор.
 * @property integer   $country_id  Страна.
 * @property integer   $region_id   Регион.
 * @property GeoRegion $region   Регион.
 * @property integer   $city_id     Город.
 * @property integer   $countryId  Страна.
 * @property integer   $regionId   Регион.
 * @property integer   $cityId     Город.
 * @property GeoCity   $city        Город.
 * @property string    $street       Улица.
 * @property string    $house        Дом.
 * @property string    $structure    Корпус / строение.
 * @property string    $index        Индекс
 * @property integer $created_at Дата создания записи.
 * @property integer $updated_at Дата изменения записи.
 */
class CompanyAddress extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['street', 'house'], 'string', 'max' => 255],
            [['structure', 'index'], 'string', 'max' => 20],
            [['street', 'house', 'structure', 'index'], 'trim'],
            [['country_id', 'region_id', 'city_id', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Страна'),
            'region_id' => Yii::t('app', 'Регион'),
            'city_id' => Yii::t('app', 'Город'),
            'street' => Yii::t('app', 'Улица'),
            'house' => Yii::t('app', 'Дом'),
            'structure' => Yii::t('app', 'Корпус / строение'),
            'index' => Yii::t('app', 'Индекс'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь со страной
     *
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::class, ['id' => 'country_id']);
    }

    /**
     * Связь с регионом
     *
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::class, ['id' => 'region_id']);
    }

    /**
     * Связь с городом
     *
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(GeoCity::class, ['id' => 'city_id']);
    }

    /**
     * Возвращает формализованый полный адрес
     *
     * @param bool $addRegion Если TRUE - в ответ добавляем вывод региона (нужен вывод региона не везде)
     * @return string
     */
    public function getFullAddress($addRegion = false)
    {
        $fullAddress = !empty($this->country) ? ($this->country->name . ', ') : '';
        if ($addRegion) {
            $regionName = $this->city->region->name ?? '';
            // У москвы регион называется г. Москва - не выводим такой регион
            if (!empty($regionName) && !in_array($this->city->id ?? 0, [77000000])) {
                $fullAddress .= $regionName . ', ';
            }
        }
        $fullAddress .= !empty($this->city) ? ('г. ' . $this->city->name) : '';
        $fullAddress .= !empty($this->street) ? (', ' . $this->street) : '';
        $fullAddress .= !empty($this->house) ? (', ' . $this->house) : '';
        $fullAddress .= !empty($this->structure) ? (', ' . $this->structure) : '';

        return trim($fullAddress);
    }
}
