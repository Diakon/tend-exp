<?php
namespace common\modules\company\models\common;

use modules\crud\behaviors\FormConfigBehavior;
use common\models\UserIdentity;
use common\modules\tender\models\frontend\Objects;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;
use yii\web\Request;
use \common\modules\company\models\frontend\Company;

/**
 * Комменты компании.
 *
 * @property integer       $id         Идентификатор.
 * @property string        $comment    Комментарий
 * @property integer       $company_id
 * @property Objects       $object     Объект
 * @property UserIdentity  $author     Автор
 *
 */
class CommentCompany extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return  [
            [['comment', 'company_id', 'object_id'], 'required'],
            [[ 'company_id', 'object_id'], 'integer'],
            [['comment'], 'string'],
           ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'comment' => Yii::t('app', 'Отзыв'),
            'object_id' => Yii::t('app', 'Объект'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return  [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'forms'       => FormConfigBehavior::className(),
            'blame' => BlameableBehavior::class
        ];
    }

    /**
     * Связь с компанией
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с автором комманта
     *
     * @return ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'updated_by']);
    }

    /**
     * Связь с объектом
     *
     * @return ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(\common\modules\tender\models\common\Objects::class, ['id' => 'object_id']);
    }

    /**
     * Список компаний
     */
    public static function getObjectList($params = [])
    {
        $company = self::find()
            ->select(['id', 'title'])
            ->where('status =:status', [':status' => ActiveRecord::STATUS_ACTIVE])
            ->andFilterWhere($params)
            ->orderBy(['title' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::map($company, 'id', 'title');
    }

    /**
     * Возвращает список комментариев компании
     *
     * @param Request $request
     *
     * @return ActiveDataProvider
     */
    public function searchItems($request)
    {

        /** @var \yii\db\ActiveQuery $query */
        $query = static::find()->with(['author', 'object']);
        $query->joinWith(['company'])->andWhere( Company::tableName() . '.status = :status AND '  .Company::tableName() . '.url =:url',
            [':status' => self::STATUS_ACTIVE, ':url' => $request->get('url')]); // связь с функциями

        $sort = new Sort([
            'attributes' => [
                'created_at' => [
                    'asc' => [
                        self::tableName() . '.created_at' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.created_at' => SORT_DESC,
                    ],

                ],
            ],
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);

       // $query->andWhere(self::tableName() . '.status = :status', [':status' => self::STATUS_ACTIVE]);

        $query->orderBy($sort->orders);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' =>  YII_ENV == 'dev' ? 10 : 10,
                'pageSizeParam' => false,
            ],
            'sort' => $sort,
        ]);
    }

}
