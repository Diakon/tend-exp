<?php
namespace common\modules\company\models\common;

use common\modules\directories\models\common\MainWork;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Направления строительства компании.
 *
 * @property integer  $id           Идентификатор.
 * @property integer   $company_id   Компания.
 * @property integer   $main_work_id Тип работы компании.
 * @property MainWork[] $mainWorkList Тип работы компании.
 * @property integer  $created_at   Дата создания записи.
 * @property integer  $updated_at   Дата изменения записи.
 */
class CompanyCharsBuilds extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_chars_builds}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'main_work_id'], 'required'],
            [['company_id', 'main_work_id', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'main_work_id' => Yii::t('app', 'Тип работы'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с типом работы
     *
     * @return ActiveQuery
     */
    public function getMainWorkList()
    {
        return $this->hasMany(MainWork::class, ['id' => 'main_work_id']);
    }
}
