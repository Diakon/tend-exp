<?php
namespace common\modules\company\models\common;

use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;
use common\models\GeoRegion;
use common\models\GeoCountry;

/**
 * Регионы действия компании.
 *
 * @property integer $id          Идентификатор.
 * @property integer $company_id  Компания.
 * @property integer $country_id  Страна.
 * @property integer $region_id   Регион.
 * @property integer $created_at  Дата создания записи.
 * @property integer $updated_at  Дата изменения записи.
 */
class CompanyCharsRegions extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_chars_regions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['region_id'], 'required'],
            [['country_id', 'region_id', 'company_id', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->country_id) && !empty($this->region_id)) {
                $region = GeoRegion::find()->where(['id' => $this->region_id])->one();
                $this->country_id = !empty($region->country_id) ? $region->country_id : $this->country_id;
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Страна'),
            'region_id' => Yii::t('app', 'Регион'),
            'company_id' => Yii::t('app', 'Компания'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь со страной
     *
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::class, ['id' => 'country_id']);
    }

    /**
     * Связь с регионом
     *
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::class, ['id' => 'region_id']);
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }
}
