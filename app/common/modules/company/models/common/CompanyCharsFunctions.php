<?php
namespace common\modules\company\models\common;

use common\modules\directories\models\common\FunctionsCompany;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Функции компании
 *
 * @property integer $id          Идентификатор.
 * @property integer $company_id  Страна.
 * @property integer $function_id Функйия компании.
 * @property integer $created_at  Дата создания записи.
 * @property integer $updated_at  Дата изменения записи.
 */
class CompanyCharsFunctions extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_chars_functions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'function_id'], 'required'],
            [['company_id', 'function_id', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'function_id' => Yii::t('app', 'Функйия компании'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с функцией
     *
     * @return ActiveQuery
     */
    public function getFunction()
    {
        return $this->hasMany(FunctionsCompany::class, ['id' => 'function_id']);
    }
}
