<?php
namespace common\modules\company\models\common;

use common\modules\directories\models\common\Directories;
use common\modules\directories\models\common\Works;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Виды выполненных работ компании.
 *
 * @property integer $id         Идентификатор.
 * @property integer $company_id  Объект.
 * @property integer $work_id    Вид работы.
 * @property integer $count      Количество.
 * @property integer $unit_id    Единица измерения.
 * @property integer $created_at Дата создания записи.
 * @property integer $updated_at Дата изменения записи.
 */
class CompanyCompletedWorks extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_completed_works}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'work_id', 'count', 'unit_id'], 'required'],
            [['company_id', 'work_id', 'count', 'unit_id'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Объект'),
            'work_id' => Yii::t('app', 'Вид работы'),
            'unit_id' => Yii::t('app', 'Единица измерения'),
            'count' => Yii::t('app', 'Количество'),
        ];
    }

    /**
     * Связь с видом работ
     *
     * @return ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Works::class, ['id' => 'work_id']);
    }

    /**
     * Связь с объектом
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(\common\modules\company\models\frontend\Company::class, ['id' => 'company_id']);
    }

    /**
     * Возвращает список единиц измерения, или значение выбранной единицы измерения
     *
     * @param null $unitId
     * @return array
     */
    public static function getUnits($unitId = null)
    {
        $units = Works::$units;
        return isset($units[$unitId]) ? $units[$unitId] : $units;
    }
}
