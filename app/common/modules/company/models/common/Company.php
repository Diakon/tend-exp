<?php
namespace common\modules\company\models\common;

use modules\crud\behaviors\FormConfigBehavior;
use modules\upload\behaviors\UploadBehavior;
use modules\upload\models\Upload;
use common\models\GeoRegion;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\directories\models\common\MainWork;
use common\modules\directories\models\common\TypesWork;
use common\modules\tariff\models\common\TariffPlans;
use common\modules\tariff\models\common\TariffHistory;
use common\modules\tender\models\common\Tenders;
use common\modules\tender\models\frontend\Objects;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;
use yii2tech\ar\linkmany\LinkManyBehavior;

/**
 * Компании.
 *
 * @property integer                 $id                Идентификатор.
 * @property string                  $url                Ссылка на страницу.
 * @property string                  $title              Название.
 * @property string                  $ogrn               ОГРН.
 * @property string                  $inn                ИНН.
 * @property string                  $kpp                КПП.
 * @property string                  $phone              Телефон.
 * @property Objects[]               $objects            Объекты компании
 * @property integer                 $year               Год основания.
 * @property integer                 $employeesNow       Количество сотрудников.
 * @property integer                 $employeesMax       Максимальное количество сотрудников.
 * @property Upload                  $avatar
 * @property string                  $general_manage     Генеральный директор
 * @property string                  $generalManage      Генеральный директор
 * @property integer                 $legal_address_id  Юридический адрес
 * @property CompanyCharsBuilds      $charsBuilds       Направления строительства
 * @property CompanyCharsWorks       $charsWorks        Группы работ
 * @property CompanyAddress          $actualAddress     Фактический адрес
 * @property CompanyAddress          $legalAddress      Юридический адрес
 * @property integer                 $actual_address_id Фактический адрес
 * @property integer                 $address_same      Совпадает с юредическим
 * @property string                  $fax                Факс.
 * @property CompanyCompletedWorks[] $completedWorks     Выполненные работы компании
 * @property CompanyUsers[]          $users              Пользователи
 * @property CompanyUsers            $companyUser        Пользователь
 * @property CommentCompany          $comments           Комменты компании
 * @property integer                 $status            Статус
 * @property TariffPlans             $tariff            Тарифный план
 * @property integer                 $tariff_plan_id    Id Тарифного план
 * @property integer                 $tariff_plan_start Дата начала плана
 * @property integer                 $tariff_plan_end   Дата окончания плана
 * @property integer                 $count_objects     Количество объектов.
 * @property integer                 $publish_catalog   Статус публикации в каталоге компаний
 * @property integer                 $created_at        Дата создания записи.
 * @property integer                 $updated_at        Дата изменения записи.
 * @property CompanyUsers            $companyAdmin     Админ компании
 * @property FunctionsCompany $functionsCompany
 */
class Company extends ActiveRecord
{
    const ADDRESS_SAME_YES = 1;
    const ADDRESS_SAME_NO = 0;

    const STATUS_PUBLISH_CATALOG_YES = 1;

    /**
     * Флаг, что для компании выставлен новый тариф
     *
     * @var bool
     */
    private $isNewTarriff = false;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return  [
            [['title', 'phone'], 'trim'],
            ['url', 'default', 'value' => function ($model, $attribute) {
                return str_replace('--', '-', Text::cyr2lat(!empty($model->{$attribute})
                    ? $model->{$attribute}
                    : $this->title . '-' . time(), Yii::$app->params['pattern']['alias']));
            }],
            [['charsRegionsIds', 'charsFunctionsIds', 'charsBuildsIds', 'charsWorksIds'], 'each', 'rule' => ['integer']],
            [['year', 'employees_now', 'employees_max', 'legal_address_id', 'actual_address_id', 'address_same', 'status', 'tariff_plan_id', 'tariff_plan_start', 'tariff_plan_end', 'count_objects', 'publish_catalog', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Компания'),
            'inn' => Yii::t('app', 'ИНН'),
            'status' => Yii::t('app', 'Статус'),
            'url' => Yii::t('app', 'ЧПУ-ссылка'),
            'ogrn' => Yii::t('app', 'ОГРН'),
            'kpp' => Yii::t('app', 'КПП'),
            'phone' => Yii::t('app', 'Телефон'),
            'year' => Yii::t('app', 'Год основания'),
            'legal_address_id' => Yii::t('app', 'Юридический адрес'),
            'actual_address_id' => Yii::t('app', 'Фактический адрес'),
            'address_same' => Yii::t('app', 'Совпадает с юридическим'),
            'general_manage' => Yii::t('app', 'Генеральный директор'),
            'fax' => Yii::t('app', 'Факс'),
            'tariff_plan_id' => Yii::t('app', 'Тарифный план'),
            'tariff_plan_start' => Yii::t('app', 'Дата начала тарифного плана'),
            'tariff_plan_end' => Yii::t('app', 'Дата окончания тарифного плана'),
            'employees_now' => Yii::t('app', 'Количество сотрудников сейчас'),
            'employees_max' => Yii::t('app', 'Количество сотрудников максимум'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
            'charsRegionsIds' => Yii::t('app', 'Регионы деятельности'),
            'charsFunctionsIds' => Yii::t('app', 'Функции'),
            'charsBuildsIds' => Yii::t('app', 'Направления строительства'),
            'charsWorksIds' => Yii::t('app', 'Группы работ'),
            'count_objects' => Yii::t('app', 'Количество объектов'),
            'publish_catalog' => Yii::t('app', 'Публиковать в каталоге компаний'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return  [
                'timestamp'   => TimestampBehavior::class,
                'forms'       => FormConfigBehavior::class,
                'linkCharsRegionsBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'regions',
                    'relationReferenceAttribute' => 'charsRegionsIds'
                ],
                'linkCharsFunctionsBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'functions',
                    'relationReferenceAttribute' => 'charsFunctionsIds'
                ],
                'linkCharsBuildsBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'builds',
                    'relationReferenceAttribute' => 'charsBuildsIds'
                ],
                'linkCharsWorksBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'works',
                    'relationReferenceAttribute' => 'charsWorksIds'
                ],
                'image' => [
                    'class' => UploadBehavior::class,
                    'attributes' => [
                        'avatar' => [
                            'singleFile' => true,
                            'rules' => [
                                'image' => [
                                    'extensions' => 'png,jpeg,jpg,bmp',
                                    'minWidth' => 10,
                                    'maxWidth' => 1600,
                                    'minHeight' => 100,
                                    'maxHeight' => 1200,
                                    'maxSize' => 3 * 1024 * 1024,
                                ],
                            ],
                        ],
                    ]
                ],
            ];
    }

    /**
     * Связь с регионами
     *
     * @return ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(GeoRegion::class, ['id' => 'region_id'])->via('charsRegions');
    }

    /**
     * Связь с Функциями
     *
     * @return ActiveQuery
     */
    public function getFunctions()
    {
        return $this->hasMany(FunctionsCompany::class, ['id' => 'function_id'])->via('charsFunctions');
    }

    /**
     * Связь с направлениями строительства
     *
     * @return ActiveQuery
     */
    public function getBuilds()
    {
        return $this->hasMany(MainWork::class, ['id' => 'main_work_id'])->via('charsBuilds');
    }

    /**
     * Связь с работами
     *
     * @return ActiveQuery
     */
    public function getWorks()
    {
        return $this->hasMany(TypesWork::class, ['id' => 'type_work_id'])->via('charsWorks');
    }

    /**
     * Связь с фактическим адресом
     *
     * @return ActiveQuery
     */
    public function getActualAddress()
    {
        return $this->hasOne(CompanyAddress::class, ['id' => 'actual_address_id']);
    }

    /**
     * Связь с юридическим адресом
     *
     * @return ActiveQuery
     */
    public function getLegalAddress()
    {
        return $this->hasOne(CompanyAddress::class, ['id' => 'legal_address_id']);
    }

    /**
     * Связь с справочной таблицей характеристик (регионы)
     *
     * @return ActiveQuery
     */
    public function getCharsRegions()
    {
        return $this->hasMany(CompanyCharsRegions::class, ['company_id' => 'id']);
    }

    /**
     * Связь с справочной таблицей характеристик (функции)
     *
     * @return ActiveQuery
     */
    public function getCharsFunctions()
    {
        return $this->hasMany(CompanyCharsFunctions::class, ['company_id' => 'id']);
    }

    /**
     * связь с функциями
     *
     * @return ActiveQuery
     */
    public function getFunctionsCompany() {
        return $this->hasMany(FunctionsCompany::class, ['id' => 'function_id'])->via('charsFunctions');
    }

    /**
     * Связь с справочной таблицей характеристик (направления строительства)
     *
     * @return ActiveQuery
     */
    public function getCharsBuilds()
    {
        return $this->hasMany(CompanyCharsBuilds::class, ['company_id' => 'id']);
    }

    public function getTypeBuilds() {
        return $this->hasMany(MainWork::class, ['id' => 'main_work_id'])->via('charsBuilds');
    }

    /**
     * Связь с справочной таблицей характеристик (вид работы)
     *
     * @return ActiveQuery
     */
    public function getCharsWorks()
    {
        return $this->hasMany(CompanyCharsWorks::class, ['company_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTypeWorks() {
        return $this->hasMany(TypesWork::class, ['id' => 'type_work_id'])->via('charsWorks');
    }

    /**
     * @return ActiveQuery
     */
    public function getTenders() {
        return $this->hasMany(Tenders::class, ['company_id' => 'id']);
    }

    /**
     * Комменты компании
     *
     * @return ActiveQuery
     */
    public function getComments() {
        return $this->hasMany(CommentCompany::class, ['company_id' => 'id']);
    }

    /**
     * связь с объектами
     *
     * @return ActiveQuery
     */
    public function getObjects() {
        return $this->hasMany(Objects::class, ['company_id' => 'id'])->onCondition('status=:status', [':status' => ActiveRecord::STATUS_ACTIVE]);
    }

    /**
     * Связь с клиентами компании
     *
     * @return ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(CompanyUsers::class, ['company_id' => 'id']);
    }


    /**
     * Получение клиента компании который является текущий аккаунт
     *
     * @return ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(CompanyUsers::class, ['company_id' => 'id'])->onCondition(['user_id' => Yii::$app->user->identity->id]);
    }

    /**
     * Получение клиента компании по id
     *
     * @param $userId
     *
     * @return CompanyUsers
     */
    public function getCompanyUserById($userId)
    {
        return $this->hasOne(CompanyUsers::class, ['company_id' => 'id'])->onCondition(['user_id' => $userId])->one();
    }


    /**
     * Получение клиента компании который является ее админом
     *
     * @return ActiveQuery
     */
    public function getCompanyAdmin()
    {
        return $this->hasOne(CompanyUsers::class, ['company_id' => 'id'])
            ->onCondition(['type_role' => \common\modules\company\models\frontend\CompanyUsers::TYPE_ROLE_ADMIN]);
    }


    /**
     * Связь с тарфиным планом
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TariffPlans::class, ['id' => 'tariff_plan_id']);
    }

    /**
     * Список компаний
     *
     * @param array $params
     * @param bool $addInn
     * @return array
     */
    public static function getCompanyList($params = [], $addInn = false)
    {
        $company = self::find()
            ->select(['id', 'title', 'inn'])
            ->where('status =:status', [':status' => ActiveRecord::STATUS_ACTIVE])
            ->andFilterWhere($params)
            ->orderBy(['title' => SORT_ASC])
            ->asArray()
            ->all();

        $list = [];
        foreach ($company as $value) {
            $id = $value['id'];
            $name = $value['title'];
            if ($addInn && !empty($value['inn'])) {
                $name .= ' (ИНН  ' . $value['inn'] . ')';
            }
            $list[$id] = $name;
        }

        return $list;
    }

    /**
     * Связь с завершенными работами
     *
     * @return ActiveQuery
     */
    public function getCompletedWorks()
    {
        return $this->hasMany(CompanyCompletedWorks::class, ['company_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->isNewRecord || $this->tariff_plan_id != $this->oldAttributes['tariff_plan_id']) {
                $this->isNewTarriff = true;
                TariffPlans::setNewTariffPlan($this);
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        if ($this->isNewTarriff) {
            TariffHistory::addToHistory($this);
        }
    }
}
