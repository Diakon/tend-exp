<?php
namespace common\modules\company\models\common;

use common\components\AuthDbManager;
use common\components\ClassMap;
use common\models\UserIdentity;
use common\modules\company\models\frontend\CompanyUserAccess;
use common\modules\tender\models\frontend\Objects;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Компании.
 *
 * @property integer                                         $id                 Идентификатор.
 * @property integer                                         $company_id         Компания.
 * @property integer                                         $user_id            Пользователь.
 * @property integer                                         $type_role          Тип роли пользователя в компании.
 * @property integer                                         $verification       Флаг подтверждения
 * @property UserIdentity                                    $user               Пользователь
 * @property string                                          $experience          Стаж работы в компании.
 * @property string                                          $position            Должность.
 * @property \common\modules\company\models\frontend\Company $company
 * @property integer                                         $status      Статус.
 * @property integer                                         $created_at  Дата создания записи.
 * @property integer                                         $updated_at  Дата изменения записи.
 * @property array                                           $objectsIds
 */
class CompanyUsers extends ActiveRecord
{

    const TYPE_ROLE_ADMIN = 1;
    const TYPE_ROLE_USER_INVIT_ADMIN = 2;
    const TYPE_ROLE_USER_INVIT_USER = 3;
    const TYPE_ROLE_USER_INVIT_BUILDER = 4;

    public static $typeRoleList = [
        self::TYPE_ROLE_ADMIN => 'Администратор проекта',
        self::TYPE_ROLE_USER_INVIT_ADMIN => 'Клиент приглашенный администратором проекта',
        self::TYPE_ROLE_USER_INVIT_USER => 'Клиент приглашенный коллегой',
        self::TYPE_ROLE_USER_INVIT_BUILDER => 'Клиент приглашенный застройщиком',
    ];

    const EXPERIENCE_1_YEARS = 1;
    const EXPERIENCE_3_YEARS = 3;
    const EXPERIENCE_5_YEARS = 5;
    const EXPERIENCE_MORE_5_YEARS = 20;

    public $objectsIds;
    /**
     * Список лет отработанных в компании
     *
     * @return array
     */
    public static function getExperienceList() {
        return [
            self::EXPERIENCE_1_YEARS => Yii::t('app', '1 год'),
            self::EXPERIENCE_3_YEARS => Yii::t('app', '3 года'),
            self::EXPERIENCE_5_YEARS => Yii::t('app', '5 лет'),
            self::EXPERIENCE_MORE_5_YEARS => Yii::t('app', 'более 5 лет'),
        ];
    }


    public static $roleAcces = [
        'canDelete' => [self::TYPE_ROLE_ADMIN, self::TYPE_ROLE_USER_INVIT_ADMIN],
        'canEdit' => [self::TYPE_ROLE_ADMIN, self::TYPE_ROLE_USER_INVIT_ADMIN, self::TYPE_ROLE_USER_INVIT_USER],
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'user_id'], 'unique', 'targetAttribute' => ['company_id', 'user_id'],
                'comboNotUnique' => Yii::t('app', 'Для этой компании уже назначен указанный пользователь')
            ],
            [['company_id', 'user_id', 'type_role', 'status'], 'required'],
            [['company_id', 'user_id', 'type_role', 'experience', 'status', 'created_at', 'updated_at'], 'integer'],
            [['position'], 'string', 'max' => 255],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'status' => Yii::t('app', 'Статус'),
            'type_role' => Yii::t('app', 'Роль'),
            'experience' => Yii::t('app', 'Стаж работы в компании'),
            'position' => Yii::t('app', 'Должность'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с пользователями
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'user_id']);
    }

    /**
     * Получение пользователя компании по id
     *
     * @param $id
     *
     * @return CompanyUsers|null
     */
    public static function getUserCompany($id) {
        return self::findOne(['user_id' => $id]);
    }

    /**
     * Доступы к проектам / тендерам в компании
     *
     * @return ActiveQuery
     */
    public function getCompanyUserAccess()
    {
        return $this->hasMany(CompanyUserAccess::class, ['user_id' => 'user_id'])->onCondition('user_id=:user_id', [':user_id' => $this->user_id]);
    }

    /**
     * Проверяет, что пользователь закреплен за компанией
     *
     * @param integer $companyId
     * @param integer $userId
     * @return bool
     */
    public static function inCompany($userId, $companyId)
    {
        $checkUserCompany = self::find()->where(['user_id' => $userId])
            ->andWhere(['company_id' => $companyId])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->count();

        return !empty($checkUserCompany) ? true : false;
    }

    /**
     * Может ли пользователь с указанной ролью просматривать компанию
     *
     * @return bool
     */
    public function canShow()
    {
        return in_array($this->type_role, [
            self::TYPE_ROLE_ADMIN,
            self::TYPE_ROLE_USER_INVIT_ADMIN,
            self::TYPE_ROLE_USER_INVIT_BUILDER,
            self::TYPE_ROLE_USER_INVIT_USER
        ]);
    }

    /**
     * Может ли пользователь с указанной ролью приглашать пользователя в компанию
     *
     * @return bool
     */
    public function canAddClient()
    {
        if(\Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return true;
        }
        return in_array($this->type_role, [
            self::TYPE_ROLE_ADMIN
        ]);
    }

    /**
     * Может ли пользователь с указанной ролью редактировать компанию
     *
     * @return bool
     */
    public function canEdit()
    {
        if(\Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return true;
        }
        return in_array($this->type_role, [
            self::TYPE_ROLE_ADMIN,
            self::TYPE_ROLE_USER_INVIT_ADMIN,
            self::TYPE_ROLE_USER_INVIT_BUILDER,
            self::TYPE_ROLE_USER_INVIT_USER
        ]);
    }

    /**
     * Может ли пользователь с указанной ролью редактировать компанию  в которой состоит
     *
     * @return bool
     */
    public function canEditMyCompany()
    {
        if(\Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return true;
        }
        return in_array($this->type_role, [
            self::TYPE_ROLE_ADMIN,
        ]);
    }

    /**
     * Может ли пользователь смотреть объекты компании
     *
     * @return bool
     */
    public function canShowObject()
    {
        // Если пользователь не админ компании, то проверяю, предостовлял ли админ компании ему доступ к проектам
        if ($this->type_role == self::TYPE_ROLE_ADMIN) {
            return true;
        }

        if ($this->checkObjectAccess()) {
            return true;
        }

        return false;
    }

    /**
     * Может ли пользователь создавать новые / редактировать существующие объекты компании
     *
     * @return bool
     */
    public function canEditObject()
    {
        // Если пользователь не админ компании, то проверяю, предостовлял ли админ компании ему доступ к проектам
        if ($this->type_role == self::TYPE_ROLE_ADMIN) {
            return true;
        }

        if ($this->checkObjectAccess([CompanyUserAccess::TYPE_ACCESS_ALLOW])) {
            return true;
        }

        return false;
    }

    /**
     * Проверяет, есть ли у пользователя доступ к объекту.
     *
     * @param array $typeAccess
     * @return bool
     */
    private function checkObjectAccess($typeAccess = [])
    {
        $objectAccess = CompanyUserAccess::find()
            ->where(['user_id' => $this->user_id])
            ->andWhere(['class_name' => ClassMap::getId(Objects::class)]);
        if (!empty($typeAccess)) {
            $objectAccess->andWhere(['in', 'type_access', $typeAccess]);
        }
        return !empty($objectAccess->count()) ? true : false;
    }

    /**
     * Может ли пользователь с указанной ролью создать новый объект
     *
     * @return bool
     */
    public function canAddObject()
    {
        return in_array($this->type_role, [
            self::TYPE_ROLE_ADMIN
        ]);
    }

    /**
     * Может ли пользователь смотреть тендеры компании
     *
     * @return bool
     */
    public function canShowTender()
    {
        // Если пользователь не админ компании, то смотрю, есть ли проекты, тендеры которых, может видеть поьзователь
        if ($this->type_role == self::TYPE_ROLE_ADMIN) {
            return true;
        }

        if ($this->checkObjectAccess()) {
            return true;
        }

        return false;
    }

    /**
     * Может ли пользователь с указанной ролью создать новый тендер в объекте
     *
     * @param null|integer $objectId
     * @return bool
     */
    public function canAddTender($objectId = null)
    {
        // Если админ - можно все в компании
        if ($this->type_role == self::TYPE_ROLE_ADMIN) {
           return true;
        }
        // Смотрю, был ли дан доступ к проекту
        if (empty($objectId)) {
            $objectAccess = CompanyUserAccess::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['class_name' => ClassMap::CLASS_OBJECTS_ID])->one();
            $objectId = $objectAccess->class_id ?? null;
        }
        if (!empty($objectId)) {
            $object = Objects::getObjectsUser($this, CompanyUserAccess::TYPE_ACCESS_ALLOW)->andWhere(['id' => (int)$objectId])->count();
            if (!empty($object)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Может ли пользователь создавать новые тендеры компании
     *
     * @param null|integer $id
     * @param integer $companyId
     * @return bool
     */
    public function canEditTender($id = null, $companyId)
    {
        $tender = \common\modules\tender\models\frontend\Tenders::find()->where(['id' => (int)$id])->one();

        // Проверяю, что тендер существует и закреплен за компанией пользователя
        if (empty($tender) || $tender->object->company_id != $companyId) {
            return false;
        }


        // Если админ - можно все в компании
        if ($this->type_role == self::TYPE_ROLE_ADMIN) {
            return true;
        }

        // Смотрю, был ли дан доступ к объекту
        $project = Objects::getObjectsUser($this, CompanyUserAccess::TYPE_ACCESS_ALLOW)->andWhere(['id' => $tender->object_id])->count();
        if (!empty($project)) {
            return true;
        }

        return false;
    }

    /**
     * Может ли пользователь с указанной ролью участвовать в тендере (оставить свое предложение для чужого тендера)
     *
     * @return bool
     */
    public function canAddOffer()
    {
        // Участвовать в тендере (при наличии тарифного плана) может любой пользователь компании
        return in_array($this->type_role, array_keys(self::$typeRoleList));
    }
}
