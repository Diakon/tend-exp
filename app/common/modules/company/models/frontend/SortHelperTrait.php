<?php

namespace common\modules\company\models\frontend;


/**
 * Trait SortHelperTrait
 * @package common\modules\company\models\frontend
 */
trait SortHelperTrait
{

    private $sortData;

    public function setSortData(array $data)
    {

        $this->sortData = $data;

    }


    public function getCurrentLabel($attribute = '')
    {

        $sort = $attribute ? $attribute : \Yii::$app->request->get('sort');

        return $this->sortData[$sort] ?? $this->sortData['-year'];
    }

}
