<?php

namespace common\modules\company\models\frontend;

use common\modules\company\models\common\CompanyAddress;
use common\modules\company\models\common\CompanyCharsBuilds;
use common\modules\company\models\common\CompanyCharsFunctions;
use common\modules\company\models\common\CompanyCharsRegions;
use common\modules\company\models\common\CompanyCharsWorks;
use common\modules\directories\models\common\Directories;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\tender\models\common\Objects;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\data\Sort;
use yii\db\Query;
use yii\sphinx\MatchExpression;
use yii\web\Request;
use yii\sphinx\Query as SphinxQuery;
/**
 * Class SearchFilterTrait.
 *
 * @package frontend\models
 */
trait SearchFilterTrait
{

    /**
     * @var CompanyAddress $addressFilter Для формы поиска компаний
     */
    public $addressFilter;

    /**
     * @var array $functionsFilter функции компании
     */
    public $functionsFilter;

    /**
     * @var array $typeWorksFilter Группы работ
     */
    public $typeWorksFilter;

    /**
     * @var array $builds направления строительства
     */
    public $buildsFilter;

    /**
     * @var array $typeObjectsFilter Типы проектов
     */
    public $typeObjectsFilter;

    /**
     * @var array $objectTypeFilter Типы объектов
     */
    public $objectTypeFilter;

    /**
     * @var integer Опыт работы
     */
    public $experience;

    /**
     * @var array $typeWorkFilter Группы работ
     */
    public $typeWorkFilter;

    /**
     * @var array $builds Направления работ
     */
    public $worksFilter;

    /**
     * @var integer $activityCountry Страна региона деятельности
     */
    public $activityCountry;

    /**
     * @var array $activityRegions Регион деятельности
     */
    public $activityRegions;

    /**
     * @var array
     */
    public $columns;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->columns = [
            'price' => [
                'attribute' => 'price',
            ],
            'run' => [
                'attribute' => 'run',
            ],
            'year' => [
                'attribute' => 'year',
            ],
        ];

        parent::init();
    }

    /**
     * Возвращает список элементов
     *
     * @param Request $request   Параметры фильтрации.
     * @param boolean $onlyQuery возвратить query или ActiveDataProvider
     *
     * @return ActiveDataProvider| ActiveQuery
     */
    public function searchItems($request, $onlyQuery = false)
    {

        $this->setSortData([
            'year' => Yii::t('app', 'По возрастанию опыта'),
            '-year' => Yii::t('app', 'По убыванию опыта'),

            'employees_now' => Yii::t('app', 'По увеличению количества сотрудников'),
            '-employees_now' => Yii::t('app', 'По уменьшению количества сотрудников'),

            'count_objects' => Yii::t('app', 'По увеличению количества объектов'),
            '-count_objects' => Yii::t('app', 'По уменьшению количества объектов'),

        ]);
        $this->scenario = self::SCENARIO_FRONTEND_FILTER;

        /** @var \yii\db\ActiveQuery $query */
        $query = static::find()->with('actualAddress');
       // $query->with(['actualAddress.city', 'charsWorks.typeWork']);
        $query->leftJoin(CompanyAddress::tableName() . ' `address` ', Company::tableName() . '.actual_address_id=' . '`address`.`id`'); // связь с адресом

        // связь с функциями
        $query->leftJoin(CompanyCharsFunctions::tableName(), self::tableName() . '.id=' . CompanyCharsFunctions::tableName() . '.company_id');
        $query->leftJoin(Directories::tableName() . ' functions_company', CompanyCharsFunctions::tableName() . '.function_id=' . 'functions_company.id');

        // связь с видами работ
        $query->leftJoin(CompanyCharsWorks::tableName(), self::tableName() . '.id=' . CompanyCharsWorks::tableName() . '.company_id');
        $query->leftJoin(Directories::tableName() . ' types_work', CompanyCharsWorks::tableName() . '.type_work_id=' . 'types_work.id');

        // связь с направления строительства
        $query->leftJoin(CompanyCharsBuilds::tableName(), self::tableName() . '.id=' . CompanyCharsBuilds::tableName() . '.company_id');
        $query->leftJoin(Directories::tableName() . ' main_work', CompanyCharsBuilds::tableName() . '.main_work_id=' . 'main_work.id');

        // типы проектов
        $query->leftJoin(Objects::tableName(), self::tableName() . '.id=' . Objects::tableName() . '.company_id');
        $query->leftJoin('{{%objects_type}}', Objects::tableName() . '.id=' . '{{%objects_type}}.object_id');

        // связь с регионами деятельности
        $query->leftJoin(CompanyCharsRegions::tableName(), self::tableName() . '.id=' . CompanyCharsRegions::tableName() . '.company_id');

        $this->getFilterValues();

        // Адрес
        $this->addressFilter = new CompanyAddress();
        $this->addressFilter->load($request->get());
        $this->addressFilter->validate();

        // Загружаем функции компании
        $this->functionsFilter = $request->get(self::classNameShort())['functionsFilter'] ?? [];
        // Загружаем группы работ
        $this->typeWorksFilter = $request->get(self::classNameShort())['typeWorksFilter'] ?? [];
        // направление строительства
        $this->buildsFilter = $request->get(self::classNameShort())['buildsFilter'] ?? [];
        // Типы объектов
        $this->objectTypeFilter = $request->get(self::classNameShort())['objectTypeFilter'] ?? [];
        // Опыт работы
        $this->experience = $request->get(self::classNameShort())['experience'] ?? null;
        // Группы робот
        $this->typeWorkFilter = $request->get(self::classNameShort())['typeWorkFilter'] ?? [];
        // Направления работ
        $this->worksFilter = $request->get(self::classNameShort())['worksFilter'] ?? [];
        // Страна региона деятельности
        $this->activityCountry = $request->get(self::classNameShort())['activityCountry'] ?? null;
        // Регион деятельности
        $this->activityRegions = $request->get(self::classNameShort())['activityRegions'] ?? [];

        if ($this->load($request->get()) && $this->validate()) {

            // ToDO позже, когда будет реализована привязка Вида работ (на форме раздел "Работы") к компаниям сделать фильтр по $this->typeWorkFilter
            // ToDo $this->worksFilter - направление работ нужно похже запрос фильтрование поставить, когда для компании будет возможность выставлять

            $yearExperience = null;
            if (!empty($this->experience)) {
                $yearExperience = (int)date('Y', time()) - $this->experience;
            }

            // Если оплаченый тариф - расширенный поиск
            if (!Yii::$app->user->isGuest && Yii::$app->user->canShowAdditionalFilter()) {
                $idsFromSphinx = $this->searchBySphinx($request);
                $query->andFilterWhere([
                    'and',
                    ['in', self::tableName().'.id', $idsFromSphinx],
                ]);
            } else {
                // Просто ищем по ИНН и названию компании
                $query->andFilterWhere([
                    'or',
                    ['like', self::tableName() . '.inn', $this->inn],
                    ['like', self::tableName() . '.title', $this->inn]
                ]);
            }

            $query->andFilterWhere([
                'and',
                // ['like', 'inn', $this->inn],
                ['=', 'address.city_id', $this->addressFilter->cityId > 0 ? intval($this->addressFilter->cityId) : null],
                ['=', 'address.region_id', $this->addressFilter->regionId > 0 ? intval($this->addressFilter->regionId) : null],
                ['=', 'address.country_id', $this->addressFilter->countryId > 0 ? intval($this->addressFilter->countryId) : null],
                ['=', 'employees_now', $this->employeesNow > 0 ? intval($this->employeesNow) : null],
                ['=', 'employees_max', $this->employeesMax > 0 ? intval($this->employeesMax) : null],
                ['in', 'functions_company.id', $this->functionsFilter],
                ['in', 'types_work.id', $this->typeWorksFilter],
                ['in', Objects::tableName() . '.id', $this->objectTypeFilter],
                ['>=', self::tableName().'.year', $yearExperience],
                ['=', self::tableName().'.count_objects', $this->count_objects],
                ['in', 'main_work.id', $this->buildsFilter],

                ['=', CompanyCharsRegions::tableName() . '.country_id', $this->activityCountry],
                ['in', CompanyCharsRegions::tableName() . '.region_id', $this->activityRegions],





            ]);
        }

        $sort = new Sort([
            'attributes' => [
                'year' => [
                    'asc' => [
                        self::tableName() . '.year' => SORT_DESC,
                    ],
                    'desc' => [
                        self::tableName() . '.year' => SORT_ASC,
                    ],

                ],

                'title' => [
                    'asc' => [
                        self::tableName() . '.title' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.title' => SORT_DESC,
                    ],
                ],
                'employees_now' => [
                    'asc' => [
                        self::tableName() . '.employees_now' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.employees_now' => SORT_DESC,
                    ],
                ],
                'count_objects' => [
                    'asc' => [
                        self::tableName() . '.count_objects' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.count_objects' => SORT_DESC,
                    ],
                ],

            ],
            'defaultOrder' => [
                'year' => SORT_DESC,
            ],
        ]);

        $query->andWhere(self::tableName() . '.status = :status', [':status' => self::STATUS_ACTIVE]);

        $query->orderBy($sort->orders);

        if($onlyQuery) {
            return $query->distinct();
        }
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->distinct()->count(),
                'pageSize' =>  YII_ENV == 'dev' ? 10 : 10,
                'pageSizeParam' => false,
            ],
            'sort' => $sort,
        ]);
    }

    /**
     * Поиск id помпаний по сфинксу
     *
     * @param Request $request
     *
     * @return array
     */
    private function searchBySphinx($request)
    {

        $text = $request->get('Company')['inn'];
        $sphinxSearch = new SphinxQuery();
        // Ищем по индексу компаний
        $companyIdxData = $sphinxSearch->from(\Yii::$app->params['sphinx']['sphinx_source_company'])
            ->match((new MatchExpression())->match('@title :title', ['title' => $text])
                ->orMatch('@inn :inn', ['inn' => $text])
                ->orMatch('@ogrn :ogrn', ['ogrn' => $text])
                ->orMatch('@kpp :kpp', ['kpp' => $text])
                ->orMatch('@general_manage :general_manage', ['general_manage' => $text])
            )->all();

        // Ищем по индексу объектов
        $objectsIdxData = $sphinxSearch->from(\Yii::$app->params['sphinx']['sphinx_source_objects'])
            ->match((new MatchExpression())->match('@title :title', ['title' => $text])
                ->orMatch('@description :description', ['description' => $text])
                ->orMatch('@employerName :employerName', ['employerName' => $text])
            )->all();

        // Dev::dump(ArrayHelper::map($objectsIdxData, 'id', 'company_id'));
        // Dev::dump(ArrayHelper::map($companyIdxData, 'id', 'id'));

        $result = array_unique(array_merge(ArrayHelper::map($objectsIdxData, 'id', 'company_id'), ArrayHelper::map($companyIdxData, 'id', 'id')));
        // Dev::dump($result);
        // die();
        return $result;
    }

    /**
     * Выбирает все данные для атрибутов учавствующих в фильтрации.
     *
     * @return void
     */
    public function getFilterValues()
    {

    }

    /**
     * Активные товары
     *
     * @return ActiveQuery
     */
    public static function frontendActive()
    {
        return self::find()->andWhere('status=:status', [':status' => self::STATUS_ACTIVE]);
    }

    /**
     * Возвращает запрос на выборку из текущей таблицы товаров.
     *
     * @return Query
     */
    public function getQuery()
    {
        return (new Query())->distinct()->from(self::tableName())->select('*');
    }
}
