<?php
namespace common\modules\company\models\frontend;

use helpers\Dev;
use common\modules\company\models\common\CompanyCompletedWorks;
use yii\db\ActiveQuery;
use common\models\GeoRegion;
use common\modules\company\models\common\CompanyAddress;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\tender\models\frontend\Objects;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\helpers\BaseHtml;
use yii\helpers\ArrayHelper;

/**
 * Class Company.
 * @property FunctionsCompany $functionsCompany
 * @package common\modules\company\models\frontend
 */
class Company extends \common\modules\company\models\common\Company
{

    use SearchFilterTrait;

    use SortHelperTrait;

    // сценарий для страницы списка компаний
    const  SCENARIO_FRONTEND_FILTER = 'frontend-filter';
    // сценарий для страницы редактирование информации компании
    const  SCENARIO_EDIT_COMPANY = 'edit-company';

    // сценарий для страницы редактирование информации компании
    const  SCENARIO_CREATE_COMPANY = 'create-company';

    /**
     * ID выполненных работ
     *
     * @var array
     */
    public $completedWorkWorkIds;

    /**
     * ID Кол-ва выполненных работ
     *
     * @var array
     */
    public $completedWorkCount;

    /**
     * ID единиц измерения выполненных работ
     *
     * @var array
     */
    public $completedWorkUnitIds;

    /**
     * Юридический адрес - страна
     * @var integer 
     */
    public $legalAddressCountryId;
    
    /**
     * Юридический адрес - регион
     * @var integer
     */
    public $legalAddressRegionId;

    /**
     * Юридический адрес - город
     * @var integer
     */
    public $legalAddressCityId;

    /**
     * Юридический адрес - улица
     * @var string
     */
    public $legalAddressStreet;

    /**
     * Юридический адрес - дом
     * @var string
     */
    public $legalAddressHouse;

    /**
     * Юридический адрес - корпус
     * @var string
     */
    public $legalAddressStructure;

    /**
     * Юридический адрес - индекс
     * @var string
     */
    public $legalAddressIndex;

    /**
     * Фактический адрес - страна
     * @var integer
     */
    public $actualAddressCountryId;

    /**
     * Фактический адрес - регион
     * @var integer
     */
    public $actualAddressRegionId;

    /**
     * Фактический адрес - город
     * @var integer
     */
    public $actualAddressCityId;

    /**
     * Фактический адрес - улица
     * @var string
     */
    public $actualAddressStreet;

    /**
     * Фактический адрес - дом
     * @var string
     */
    public $actualAddressHouse;

    /**
     * Фактический адрес - корпус
     * @var string
     */
    public $actualAddressStructure;

    /**
     * Фактический адрес - индекс
     * @var string
     */
    public $actualAddressIndex;

    /**
     * Страны деятельности (профильные характеристики компани)
     * @var array
     */
    public $charsCountryIds = [];

    /**
     * Регионы деятельности (профильные характеристики компани)
     * @var array
     */
    public $charsRegionIds = [];

    /**
     * Функции (профильные характеристики компани)
     * @var array
     */
    public $charsFunctionsIds = [];

    /**
     * Направления строительства (профильные характеристики компани)
     * @var array
     */
    public $charsBuildsIds = [];

    /**
     * Группы работ (профильные характеристики компани)
     * @var array
     */
    public $charsWorksIds = [];

    /**
     * @inheritdoc
     */
    public static $paramIdent = 'url';

    /**
     * @inheritdoc
     */
    public static $paramColumn = 'url';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [];
        switch ($this->getScenario()) {
            case self::SCENARIO_FRONTEND_FILTER:
                $rules[] = [['inn', 'kpp', 'ogrn'], 'string', 'skipOnEmpty' => true];
                $rules[] = [['functionsFilter', 'typeWorksFilter', 'buildsFilter', 'typeObjectsFilter'],  'each', 'rule' => ['integer'], 'skipOnEmpty' => true];

                break;
            case  self::SCENARIO_EDIT_COMPANY:
                $rules[] = [['url'], 'unique'];
                $rules[] = [['url', 'title', 'general_manage'], 'string', 'max' => 255];
                $rules[] = [['ogrn'], 'string', 'min' => 13, 'max' => 13];
                $rules[] = [['inn'], 'string', 'min' => 10, 'max' => 12];
                $rules[] = [['address_same'], 'integer'];
                $rules[] = [['kpp'], 'string', 'min' => 9, 'max' => 9];

                $rules[] = [['title', 'year', 'inn', 'tariff_plan_id', 'status', 'ogrn', 'kpp', 'general_manage', 'charsFunctionsIds',
                    'charsBuildsIds', 'charsWorksIds', 'legalAddressIndex', 'legalAddressCountryId', 'legalAddressCityId'], 'required', 'when' => function($model) {
                    return $model->publish_catalog == self::STATUS_PUBLISH_CATALOG_YES;
                }];



                $rules[] = [['completedWorkWorkIds', 'completedWorkCount', 'completedWorkUnitIds'], 'each', 'rule' => ['integer']];
                break;
            case self::SCENARIO_CREATE_COMPANY:
                $rules[] = [['url', 'title', 'tariff_plan_id', 'inn', 'status'], 'required'];
                $rules[] = [['url'], 'unique'];
                $rules[] = [['inn'], 'string', 'min' => 10, 'max' => 12];
                break;
            default:
                break;
        }

        $rulesDefault = [
            [['employees_now', 'employees_max', 'legalAddressCountryId', 'legalAddressRegionId', 'legalAddressCityId'], 'integer'],
            [['legalAddressCountryId', 'legalAddressRegionId', 'legalAddressCityId'], 'integer'],
            [['actualAddressCountryId', 'actualAddressRegionId', 'actualAddressCityId'], 'integer'],
            [['legalAddressStreet', 'legalAddressHouse', 'actualAddressStreet', 'actualAddressHouse'], 'string', 'max' => 255],
            [['legalAddressStructure', 'legalAddressIndex', 'actualAddressStructure', 'actualAddressIndex'], 'string', 'max' => 20],
            [['charsCountryIds', 'charsRegionIds', 'charsFunctionsIds', 'charsBuildsIds', 'charsWorksIds'], 'each', 'rule' => ['integer']],
            [['charsCountryIds', 'charsRegionIds'], 'required', 'when' => function ($model) {
                return !empty($model->charsCountryIds) || !empty($model->charsRegionIds) ? true : false;
            }],
        ];

        return ArrayHelper::merge(parent::rules(), $rules+$rulesDefault);
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_FRONTEND_FILTER => ['employees_now', 'employees_max', 'inn', 'kpp', 'ogrn', 'functionsFilter', 'typeWorksFilter', 'buildsFilter', 'count_objects'], // Поля для расширенного поиска компаний
            self::SCENARIO_EDIT_COMPANY => ['completedWorkWorkIds', 'completedWorkCount', 'completedWorkUnitIds', 'employees_now', 'actual_address_id', 'legal_address_id',
                'employees_max', 'title', 'fax', 'phone', 'address_same', 'year', 'inn', 'ogrn', 'kpp', 'general_manage', 'charsFunctionsIds', 'charsRegionIds',
                'charsBuildsIds', 'charsWorksIds', 'legalAddressIndex', 'legalAddressCountryId', 'legalAddressCityId', 'legalAddressStreet', 'legalAddressHouse',
                'legalAddressStructure', 'actualAddressIndex', 'actualAddressCountryId', 'actualAddressCityId', 'actualAddressStreet', 'actualAddressHouse', 'actualAddressStructure', 'publish_catalog'
            ],
            self::SCENARIO_CREATE_COMPANY => ['tariff_plan_id', 'title', 'inn', 'status', 'url'],
        ]);
    }

    /**
     * Получает актуальный и физический адреса компании и назначает значения в параметры модели
     */
    public function setAddressParam()
    {
        $addressTypes = [
            'actual',
            'legal'
        ];
        $addressCompanyFields = [
            'AddressCountryId' => 'country_id',
            'AddressRegionId' => 'region_id',
            'AddressCityId' => 'city_id',
            'AddressStreet' => 'street',
            'AddressHouse' => 'house',
            'AddressStructure' => 'structure',
            'AddressIndex' => 'index'
        ];
        foreach ($addressTypes as $addressType) {
            $address = $addressType . 'Address';
            $address = $this->{$address};
            if (empty($address)) {
                continue;
            }
            foreach ($addressCompanyFields as $companyField => $addressField) {
                $companyField = $addressType . $companyField;
                $this->{$companyField} = $address->{$addressField};
            }
        }
    }

    /**
     * Получает характеристики компании и назначает в переменные
     */
    public function setCharsParam()
    {
        $this->charsCountryIds = ArrayHelper::map($this->charsRegions, 'country_id', 'country_id');
        $this->charsRegionIds = ArrayHelper::map($this->charsRegions, 'region_id', 'region_id');
        $this->charsFunctionsIds = ArrayHelper::map($this->charsFunctions, 'id', 'function_id');
        $this->charsBuildsIds = ArrayHelper::map($this->charsBuilds, 'main_work_id', 'main_work_id');
        $this->charsWorksIds = ArrayHelper::map($this->charsWorks, 'type_work_id', 'type_work_id');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'legalAddressCountryId' => Yii::t('app', 'Страна'),
                'legalAddressRegionId' => Yii::t('app', 'Регион'),
                'legalAddressCityId' => Yii::t('app', 'Город'),
                'legalAddressStreet' => Yii::t('app', 'Улица'),
                'legalAddressHouse' => Yii::t('app', 'Дом'),
                'legalAddressStructure' => Yii::t('app', 'Корпус / строение'),
                'legalAddressIndex' => Yii::t('app', 'Индекс'),
                'actualAddressCountryId' => Yii::t('app', 'Страна'),
                'actualAddressRegionId' => Yii::t('app', 'Регион'),
                'actualAddressCityId' => Yii::t('app', 'Город'),
                'actualAddressStreet' => Yii::t('app', 'Улица'),
                'actualAddressHouse' => Yii::t('app', 'Дом'),
                'actualAddressStructure' => Yii::t('app', 'Корпус / строение'),
                'actualAddressIndex' => Yii::t('app', 'Индекс'),
                'charsCountryIds' => Yii::t('app', 'Страна'),
                'charsRegionIds' => Yii::t('app', 'Область / край'),
                'charsFunctionsIds' => Yii::t('app', 'Функции компании'),
                'charsBuildsIds' => Yii::t('app', 'Направления строительства'),
                'charsWorksIds' => Yii::t('app', 'Вид работ'),
                'completedWorkWorkIds' => Yii::t('app', 'Выполненные работы'),
                'completedWorkCount' => Yii::t('app', 'Количество'),
                'completedWorkUnitIds' => Yii::t('app', 'Ед. измерения'),
            ]
        );
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        // Обновляем выполненные работы
        $this->updateCompletedWorks();
    }

    /**
     * Обновляет записи о выполненных работах для объекта
     */
    private function updateCompletedWorks()
    {
        if (!empty($this->completedWorkWorkIds)) {
            $transaction = CompanyCompletedWorks::getDb()->beginTransaction();
            try {
                CompanyCompletedWorks::deleteAll(['company_id' => $this->id]);
                foreach ($this->completedWorkWorkIds as $key => $workId) {

                    if ($this->completedWorkCount[$key] > 0) {
                        $model = new CompanyCompletedWorks();
                        $model->company_id = $this->id;
                        $model->work_id = $workId;
                        $model->count = (int)$this->completedWorkCount[$key];
                        $model->unit_id = (int)$this->completedWorkUnitIds[$key];
                        if (!$model->save()) {
                            /*
                            Dev::dump($model->errors());
                            die();
                            */
                            throw new \yii\web\HttpException(500, BaseHtml::errorSummary($model));
                        }
                    }
                }
                $transaction->commit();
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::error('Ошибка при сохранении выполненных работ для объекта ID = ' . $this->id . '. Сообщение валидатора: ' . $e->getMessage());
            }
        }
    }

    /**
     * Возвращает фото
     *
     * @return string
     */
    public function getPhoto()
    {
        return !empty($this->image) ? $this->image->getFile(true) : null;
    }

    /**
     * @inheritdoc
     */
    public function getViewLink($action = 'company_edit', array $params = [])
    {
        return parent::getViewLink($action, $params);
    }

    /**
     * Активные компании
     *
     * @return ActiveQuery
     */
    public static function frontendActive()
    {
        return self::find()
            ->andWhere(['status' => self::STATUS_ACTIVE]);
    }

    /**
     * Ищем портфолио компании
     *
     * @return ActiveDataProvider | ActiveQuery
     */
    public function searchPortfolio($params = [], $queryOnly = false) {
        $query = $this->getObjects()
            ->where(['show_portfolio' => \modules\crud\models\ActiveRecord::STATUS_ACTIVE])
            ->andFilterWhere($params);

        if($queryOnly) {
            return $query;
        }

        $sort = new Sort([
            'attributes' => [
                'created_at' => [
                    'asc' => [
                        Objects::tableName() . '.created_at' => SORT_ASC,
                    ],
                    'desc' => [
                        Objects::tableName() . '.created_at' => SORT_DESC,
                    ],

                ],
            ],
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' => YII_ENV == 'dev' ? 10 : 10,
                'pageSizeParam' => false,
            ],
            'sort' => $sort,
        ]);
    }


    /**
     * Возвращает компании пользователя
     *
     * @param mixed $request Параметры фильтрации.
     *
     * @return ActiveDataProvider
     */
    public function getUserCompanies($request = null)
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = Yii::$app->user->getAllCompanies();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' => 10,
                'pageSizeParam' => false,
            ],
        ]);
    }

    /**
     * Обновляет дочерние записи информации о компании
     */
    public function updateCompanyChildRecords()
    {
        // Обновляем адреса
        $this->updateCompanyAddress();
        // Обновляем профильные характеристики
        $this->updateCompanyChars();
        return true;
    }

    /**
     * Обновляет данные о профильных характеристиках компании.
     */
    private function updateCompanyChars()
    {
        $companyChars = [
            '\common\modules\company\models\common\CompanyCharsRegions' => [
                'charsRegionIds' => 'region_id'
            ],
            '\common\modules\company\models\common\CompanyCharsFunctions' => [
                'charsFunctionsIds' => 'function_id'
            ],
            '\common\modules\company\models\common\CompanyCharsWorks' => [
                'charsWorksIds' => 'type_work_id'
            ],
            '\common\modules\company\models\common\CompanyCharsBuilds' => [
                'charsBuildsIds' => 'main_work_id'
            ],
        ];

        foreach ($companyChars as $modelChar => $modelData) {

            $transaction = $modelChar::getDb()->beginTransaction();
            try {
                $modelChar::deleteAll([
                    'company_id' => $this->id
                ]);
                $charsIdsField = key($modelData);
                $charFieldName = current($modelData);
                foreach ($this->{$charsIdsField} as $key => $value) {
                    $model = new $modelChar;
                    $model->company_id = $this->id;
                    $model->{$charFieldName} = $value;
                    // Если обновлям записи для характеристик регионов / стран компании - пишем данные из массива регионов
                    if ((new \ReflectionClass($modelChar))->getShortName() == 'CompanyCharsRegions') {
                        //Если не указали страну на форме, получаю страну по региону
                        if (!empty($this->charsCountryIds[$key])) {
                            $countryId = $this->charsCountryIds[$key];
                        } else {
                            $region = GeoRegion::find()->where(['id' => $value])->asArray()->one();
                            $countryId = $region['country_id'] ?? null;
                        }
                        $model->country_id = $countryId;
                    }

                    if (!$model->save()) {
                        throw new \yii\web\HttpException(500, BaseHtml::errorSummary($model));
                    }
                }
                $transaction->commit();
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::error('Ошибка при сохранении характеристик компании ID = ' . $this->id . '. Сообщение валидатора: ' . $e->getMessage());
            }
        }
    }

    /**
     * Обновляет данные о юридическом и фактическом адресах
     */
    private function updateCompanyAddress()
    {
        $transaction = CompanyAddress::getDb()->beginTransaction();
        try {

            // Юридический адрес
            $modelLegalAddress = empty($this->legalAddress) ? new CompanyAddress() : $this->legalAddress;
            $modelLegalAddress->country_id = $this->legalAddressCountryId;
            $modelLegalAddress->region_id = $this->legalAddressRegionId;
            $modelLegalAddress->city_id = $this->legalAddressCityId;
            $modelLegalAddress->street = $this->legalAddressStreet;
            $modelLegalAddress->house = $this->legalAddressHouse;
            $modelLegalAddress->structure = $this->legalAddressStructure;
            $modelLegalAddress->index = $this->legalAddressIndex;
            if (!$modelLegalAddress->save()) {
                Yii::$app->session->addFlash('error-address', Yii::t('app', 'Ошибка сохранения юридического адреса'), false);
            }

            // Фактический адрес
            $modelActualAddress = empty($this->actualAddress) ? new CompanyAddress() : $this->actualAddress;
            $modelActualAddress->country_id = $this->actualAddressCountryId;
            $modelActualAddress->region_id = $this->actualAddressRegionId;
            $modelActualAddress->city_id = $this->actualAddressCityId;
            $modelActualAddress->street = $this->actualAddressStreet;
            $modelActualAddress->house = $this->actualAddressHouse;
            $modelActualAddress->structure = $this->actualAddressStructure;
            $modelActualAddress->index = $this->actualAddressIndex;
            if (!$modelActualAddress->save()) {
                Yii::$app->session->addFlash('error-address', Yii::t('app', 'Ошибка сохранения фактического адреса'), false);
            }

            $this->actual_address_id = $modelActualAddress->id;
            $this->legal_address_id = $modelLegalAddress->id;

            $this->save(false, ['actual_address_id', 'legal_address_id']);


            $transaction->commit();
        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения данных'), false);
            Yii::error('Ошибка при сохранении адреса для компании ID = ' . $this->id . '. Сообщение валидатора: ' . $e->getMessage());
        }
    }
}
