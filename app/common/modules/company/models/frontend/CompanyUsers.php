<?php
namespace common\modules\company\models\frontend;

use common\modules\company\models\common\CompanyUsers as CommonCompanyUsers;

/**
 * Пользователи компании
 */
class CompanyUsers extends CommonCompanyUsers
{
    /**
     * Добавляет пользователя к копании выполняя проверку,
     * если пользователь у этой компании есть, то обновляю данные
     *
     * @return bool
     */
    public function addUser()
    {
        $user = self::find()
            ->where(['company_id' => $this->company_id])
            ->andWhere(['user_id' => $this->user_id])
            ->one();

        if (empty($user)) {
            $user = new self();
            $user->attributes = $this->attributes;
        }

        $user->status = self::STATUS_ACTIVE;
        $user->type_role = $this->type_role;
        return $user->save();
    }


}
