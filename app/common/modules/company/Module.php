<?php
namespace common\modules\company;
use common\modules\company\controllers\CompanyAdminController;
use common\modules\company\controllers\CompanyController;


/**
 * Class Module.
 * @package common\modules\tender
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\company\controllers';

    public $controllerMap = [
        'company' => CompanyAdminController::class,
        'companies' => CompanyController::class,
    ];

}
