<?php
namespace common\modules\company\controllers;


use common\modules\company\controllers\actions\CommentsAction;
use common\modules\company\controllers\actions\CompaniesAction;
use common\modules\company\controllers\actions\CompanyInfo;
use common\modules\company\controllers\actions\DeleteCompanyAvatar;
use common\modules\company\controllers\actions\ObjectInfo;
use common\modules\company\controllers\actions\UserPortfolioList;
use common\modules\tender\models\common\Objects;
use frontend\controllers\FrontendController;

/**
 * Основной контроллер компаний
 */
class CompanyController extends FrontendController
{

    public function init() {

        parent::init();
        $this->layout = '@root/themes/frontend/views/layouts/inner';
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {

        return [
            // Список компаний
            'company_list' => [
                'class' => CompaniesAction::class,
                'params' => [
                    'template' => '/frontend/companies_list',
                ],
            ],
            // Список комментариев компании
            'comment_list' => [
                'class' => CommentsAction::class,
                'params' => [
                    'template' => '/frontend/comment_list',
                ],
            ],
            'company_info' => [
                'class' => CompanyInfo::class,
                'params' => [
                    'template' => '/frontend/company_info',
                ],
            ],
            'portfolio_list' => [
                'class' => UserPortfolioList::class,
                'params' => [
                    'template' => '/frontend/portfolio_list',
                ],
            ],
            'object_info' => [
                'class' => ObjectInfo::class,
                'params' => [
                    'template' => '/frontend/_ajax_object_info',
                ],
            ],
        ];
    }


}
