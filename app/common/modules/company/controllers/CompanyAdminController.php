<?php
namespace common\modules\company\controllers;

use modules\crud\actions\DeleteAction;
use modules\crud\actions\ListAction;
use backend\controllers\BackendController;
use modules\crud\actions\CreateAction;
use common\modules\company\models\backend\CompanyUserAccess;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\Json;
use common\modules\company\models\backend\Company;
use common\modules\company\models\backend\CompanyAddress;
use common\modules\company\models\backend\CompanyUsers;


/**
 * Основной контроллер.
 */
class CompanyAdminController extends BackendController
{
    /**
     * @var string|boolean the name of the layout to be applied to this controller's views.
     */
    public $layout = '@root/themes/backend/views/layouts/main';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'company_create',
                            'company_list',
                            'company_update',
                            'company_delete',
                            'company_address_create',
                            'company_address_list',
                            'company_address_update',
                            'company_users_create',
                            'company_users_list',
                            'company_users_update',
                            'company_chars_works_delete',
                            'company_user_access_list',
                            'company_user_access_update',
                            'company_user_access_delete',
                        ],
                        'roles' => ['administrator'],
                    ],

                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'company_list' => [
                'class' => ListAction::class,
                'template' => '/backend/company_list',
                'model' => Company::class,
            ],
            'company_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Company::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Company::classNameShort('id') . '_edit',
                ],
            ],
            'company_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Company::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Company::classNameShort('id') . '_create',
                ],
            ],
            'company_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => Company::class,
                    'returnUrl' => ['backend/company'],
                ],
            ],
            'company_address_list' => [
                'class' => ListAction::class,
                'template' => '/backend/company_address_list',
                'model' => CompanyAddress::class,
            ],
            'company_address_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => CompanyAddress::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . CompanyAddress::classNameShort('id') . '_edit',
                ],
            ],
            'company_address_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => CompanyAddress::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . CompanyAddress::classNameShort('id') . '_create',
                ],
            ],
            'company_address_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => CompanyAddress::class,
                    'returnUrl' => ['backend/company_address_list'],
                ],
            ],
            'company_users_list' => [
                'class' => ListAction::class,
                'template' => '/backend/company_users_list',
                'model' => CompanyUsers::class,
            ],
            'company_users_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => CompanyUsers::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . CompanyUsers::classNameShort('id') . '_edit',
                ],
            ],
            'company_users_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => CompanyUsers::class,
                    'returnUrl' => ['backend/company_users_list'],
                ],
            ],
            'company_users_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => CompanyUsers::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . CompanyUsers::classNameShort('id') . '_create',
                ],
            ],
            'company_user_access_list' => [
                'class' => ListAction::class,
                'template' => '/backend/company_user_access_list',
                'model' => CompanyUserAccess::class,
            ],
            'company_user_access_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => CompanyUserAccess::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . CompanyUserAccess::classNameShort('id') . '_edit',
                ],
            ],
            'company_user_access_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => CompanyUserAccess::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . CompanyUserAccess::classNameShort('id') . '_create',
                ],
            ],
            'company_user_access_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => CompanyUserAccess::class,
                    'returnUrl' => ['backend/sliders'],
                ],
            ],
        ];
    }

    /**
     * Обработка системных ошибок.
     *
     * @return string
     */
    public function actionError()
    {
        $this->layout = '@cms/themes/backend/views/layouts/base';
        /** @var \yii\base\Exception $exception */
        $exception = Yii::$app->errorHandler->exception;
        $handler = Yii::$app->errorHandler;

        $data = [];
        $data['name'] = $exception->getName();
        $data['message'] = $exception->getMessage();
        $data['line'] = $exception->getLine();
        $data['file'] = $exception->getFile();
        $data['trace'] = $exception->getTraceAsString();
        $data['code'] = $exception->getCode();

        $code = $exception instanceof \yii\web\HttpException ? $exception->statusCode : $exception->getCode();
        $message = $exception->getMessage();


        if (Yii::$app->request->isAjax) {
            return print_r($data, true);
        } else {
            $this->view->title = $code . ' ' . $exception->getMessage();
            $params = compact('exception', 'code', 'message','handler');
            Yii::error(Json::encode($data), 'error');

            return $this->render('@cms/themes/backend/views/backend/error', $params);
        }
    }
}
