<?php
namespace common\modules\company\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\Response;
use common\modules\company\models\frontend\Company;
use frontend\modules\upload\models\Upload;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class DeleteCompanyAvatar.
 * @package common\modules\company\controllers\actions
 */
class DeleteCompanyAvatar extends BaseAction
{
    /**
     * Удаление авы
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function run()
    {
        if (!Yii::$app->user->isGuest && YII::$app->request->isAjax) {

            if (Yii::$app->request->post('avatarRemove') == true) {
                $model = Company::findOne(Yii::$app->user->getActiveCompanyData()['id']);

                /** @var Upload $avatar */
                $avatarModel = $model->avatar;
                $avatar = $avatarModel ?? new Upload();

                if (!$avatar->isNewRecord && $avatar->delete()) {
                    $avatar->deleteFiles();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    echo Json::encode(['data' => 'true', 'src' => Yii::getAlias('@static'). '/../content/images/helment.svg']);
                    Yii::$app->end();
                }
            }
        }

        echo Json::encode(['data' => 'false']);
        Yii::$app->end();
    }
}
