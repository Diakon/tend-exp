<?php
namespace common\modules\company\controllers\actions;

use modules\crud\actions\BaseAction;
use common\modules\company\models\frontend\Company;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class UserPortfolioList
 * @package common\modules\company\controllers\actions
 */
class UserPortfolioList extends BaseAction
{

    /**
     * Вывод портфолио компании.
     *
     * @return string
     */
    public function run($url)
    {

        $company = Company::findOne(['url' => $url]);
        if (empty($company)) {
            throw new NotFoundHttpException();
        }

        $portfolioDataProvider = $company->searchPortfolio();

        $isAjax = \Yii::$app->request->isAjax;
        $page = intval(\Yii::$app->request->post('loadPage', null));
        if ($isAjax) {
            $portfolioDataProvider->setPagination(['pageSize' => YII_ENV == 'dev' ? 10 : 10, 'pageSizeLimit' => [1, $portfolioDataProvider->totalCount], 'totalCount' => $portfolioDataProvider->totalCount]);
            $portfolioDataProvider->pagination->setPage($page);

            if ($page >= $portfolioDataProvider->pagination->pageCount) { // Если нет данных
                $portfolioDataProvider->pagination->setPage($page-1);
                return Json::encode(['data' => '',
                    'pagination' => $this->controller->renderAjax('/frontend/_companies_pagination', ['dataProvider' => $portfolioDataProvider ]),
                    'page' => $page,]);
            }
        }


        $this->controller->view->params['breadcrumbs'][] = ['label' => 'Каталог компаний', 'url' => ['/company/companies/company_list']];
        $title = $company->title;
        $this->controller->view->params['breadcrumbs'][] = ['encode' => false, 'label' => '<span class="breadcrumbs__label">' . $title . '</span>',];
        $this->controller->view->title = $title;

        $view = $isAjax ? '/frontend/_companies_portfolio_inner' : $this->params['template'];
        if ($isAjax) {
            return Json::encode([
                'data' => $this->controller->renderAjax($view,
                    [
                        'model' => $company,
                        'isAjax' => $isAjax,
                        'address' => $company->addressFilter,
                        'portfolioDataProvider' => $portfolioDataProvider,
                        'page' => $page,
                    ]),
                'pageCount' => $portfolioDataProvider->pagination->page+1,
                'pagination' => $this->controller->renderAjax('/frontend/_companies_pagination',
                    ['dataProvider' => $portfolioDataProvider,]),
            ]);
        }

        return $this->controller->render($this->params['template'], [ 'isAjax' => $isAjax,'portfolioDataProvider' => $portfolioDataProvider, 'company' => $company]);

    }
}

