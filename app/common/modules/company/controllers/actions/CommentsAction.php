<?php

namespace common\modules\company\controllers\actions;

use modules\crud\actions\ListAction;
use common\modules\company\models\common\CommentCompany;
use common\modules\company\models\frontend\Company;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class CompaniesAction
 * @package common\modules\company\controllers\actions
 */
class CommentsAction extends ListAction
{
    /**
     * @var string
     */
    public $layout = '@root/themes/frontend/views/layouts/inner';

    /**
     * Список комментариев компании
     *
     * @param string $urlCompany
     *
     * @return string
     */
    public function run()
    {
        $company = Company::findOne(['url' => Yii::$app->request->get('url')]);
        if (empty($company)) {
            throw new NotFoundHttpException();
        }
        $request = Yii::$app->request;
        $model = new CommentCompany(['company_id' => $company->id]);
        if ($model->load($request->post()) &&  $model->save()) {
            Yii::$app->session->addFlash('success', 'Отзыв успешно добавлен');
        }
        $this->controller->layout = $this->layout;
        $dataProvider = $model->searchItems($request);

        $isAjax = Yii::$app->request->isAjax;
        $page = intval(Yii::$app->request->post('loadPage', null));
        if ($isAjax) {
            $dataProvider->setPagination(['pageSize' => YII_ENV == 'dev' ? 10 : 10, 'pageSizeLimit' => [1, $dataProvider->totalCount], 'totalCount' => $dataProvider->totalCount]);
            $dataProvider->pagination->setPage($page);

            if ($page >= $dataProvider->pagination->pageCount) { // Если нет данных
                $dataProvider->pagination->setPage($page - 1);

                return Json::encode(['data' => '',
                    'pagination' => $this->controller->renderAjax('/frontend/_companies_pagination', ['dataProvider' => $dataProvider]),
                    'page' => $page,]);
            }
        }
        $title = Yii::t('app', 'Коментарии');
        $this->controller->view->params['breadcrumbs'][] = ['encode' => false, 'label' => '<span class="breadcrumbs__label">' . $title . '</span>'];
        $this->controller->view->title = $title;

        $view = $isAjax ? '/frontend/_comment_list_inner' : $this->params['template'];
        if ($isAjax) {
            return Json::encode([
                'data' => $this->controller->renderAjax($view,
                    [
                        'model' => $model,
                        'company' => $company,
                        'isAjax' => $isAjax,
                        'dataProvider' => $dataProvider,
                        'page' => $page,
                    ]),
                'pageCount' => $dataProvider->pagination->page+1,
                'pagination' => $this->controller->renderAjax('/frontend/_companies_pagination',
                    ['dataProvider' => $dataProvider,]),
            ]);
        }

        return $this->controller->render(
            $view, [
                'model' => $model,
                'company' => $company,
                'isAjax' => $isAjax,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
