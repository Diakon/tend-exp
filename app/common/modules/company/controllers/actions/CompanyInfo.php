<?php
namespace common\modules\company\controllers\actions;

use helpers\Dev;
use modules\crud\actions\BaseAction;
use common\modules\company\models\frontend\Company;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class CompanyDetailAction
 * @package common\modules\company\controllers\actions
 */
class CompanyInfo extends BaseAction
{
    /**
     * Вывод простой карточки элемента.
     * @param $url
     * @return string
     * @throws NotFoundHttpException
     */
    public function run($url)
    {

        $company = Company::findOne(['url' => $url]);
        if (empty($company)) {
            throw new NotFoundHttpException();
        }
        $company->setCharsParam();

        $objects = $company->getObjects()->with('functions', 'activities')->asArray()->all();
        $objectData['functions'] = $this->getGroupData($objects, 'functions');
        $objectData['activities'] = $this->getGroupData($objects, 'activities');

        $this->controller->view->params['breadcrumbs'][] = ['class' => 'breadcrumbs__link', 'label' => \Yii::t('app', 'Каталог компаний'), 'url' => ['/company/companies/company_list']];
        $title = $company->title;
        $this->controller->view->params['breadcrumbs'][] = ['encode' => false, 'label' => '<span class="breadcrumbs__label">' . $title . '</span>',];
        $this->controller->view->title = $title;

        return $this->controller->render($this->params['template'], ['company' => $company, 'objectData' => $objectData]);

    }

    /**
     * Считаем количество сгруппированных характеристик объекта
     *
     * @param $objects
     * @param $attribute
     *
     * @return array
     */
    public function getGroupData ($objects, $attribute) {

        $dataFunctions = ArrayHelper::getColumn($objects, $attribute);
        $result = [];
        foreach ($dataFunctions as $function) {
            foreach ($function as $key => $item) {
                $result[] =  $item['title'];
            }

        }

        return array_count_values($result);
    }
}
