<?php
namespace common\modules\company\controllers\actions;

use modules\crud\actions\BaseAction;
use common\modules\tender\models\frontend\Objects;

/**
 * Class ObjectInfo.
 * @package common\modules\company\controllers\actions
 */
class ObjectInfo extends BaseAction
{

    /**
     * Вывод простой карточки объекта.
     *
     * @return string
     */
    public function run($id)
    {

        $object = Objects::find()->where(['id' => $id, 'status' => Objects::STATUS_ACTIVE, 'show_portfolio' => Objects::STATUS_ACTIVE])->one();

        if (empty($object)) {
            throw new NotFoundHttpException();
        }

        return $this->controller->renderAjax($this->params['template'], ['object' => $object]);

    }
}
