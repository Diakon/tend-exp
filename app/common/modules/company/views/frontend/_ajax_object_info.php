<?php
/**
 * @var \common\modules\tender\models\frontend\Objects $object
 */

$types = $object->types;
$active = $object->activities;
$functions = $object->functions;
$typeWorks = $object->works;
?>

<div class="popup popup--portfolio" data-popup-id="portfolio-item" id="portfolio-item">
    <div class="popup__box">
        <div class="container">
            <div class="popup__box-inner">
                <button class="popup__close js-close-wnd" type="button"></button>
                <div class="popup__body">
                    <div class="popup-portfolio">
                        <div class="popup-portfolio__nav">
                            <button class="popup-portfolio__nav-prev popup-portfolio__nav-btn">
                <span class="_icon"><svg class="icon icon-arrow-pag-prev ">
    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= \Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#arrow-pag-prev"/>
</svg></span><span class="_text"><?= Yii::t('app', 'ПРЕДЫДУЩИЙ ПРОЕКТ')?></span>
                            </button>
                            <button class="popup-portfolio__nav-next popup-portfolio__nav-btn" data-popup-ajax="ajax/portfolio-item-2.html">
                                <span class="_text"><?= Yii::t('app', 'СЛЕДУЮЩИЙ ПРОЕКТ') ?></span>
                                <span class="_icon"><svg class="icon icon-arrow-pag-next ">
    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= \Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#arrow-pag-next"/>
</svg>
                                </span>
                            </button>
                        </div>
                        <div class="popup-portfolio__section">
                            <div class="popup-portfolio__section-body">
                                <div class="popup-portfolio__main">
                                    <div class="popup-portfolio__main-left">
                                        <div class="popup-portfolio__main-status"><span class="_text"><?= $object->getStatusComplete($object->statusCompleteId) ?? '' ?></span></div>
                                        <div class="popup-portfolio__main-title popup-portfolio__section-title"><?= $object->title ?></div>
                                    </div>
                                    <div class="popup-portfolio__main-right">
<!--                                        <div class="popup-portfolio__main-confirm">ПОДТВЕРЖДЕН ЗАСТРОЙЩИКОМ</div>-->
                                        <div class="popup-portfolio__main-option">
                                            <ul class="_list">
                                                <li class="_item">
                                                    <div class="_label"><?= Yii::t('app', 'Направление строительства')?></div>
                                                    <div class="_value"><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($active, 'title')) ?></div>
                                                </li>
                                                <li class="_item">
                                                    <div class="_label"><?= Yii::t('app', 'Тип объекта')?></div>
                                                    <div class="_value">
                                                        <?= implode(', ', \yii\helpers\ArrayHelper::getColumn($types, 'title')) ?>
                                                        </div>
                                                </li>
                                                <li class="_item">
                                                    <div class="_label"><?= Yii::t('app', 'Адрес')?></div>
                                                    <div class="_value"><?= $object->address->getFullAddress() ?? '----' ?></div>
                                                </li>
                                                <li class="_item">
                                                    <div class="_label"><?= Yii::t('app', 'Период выполнения')?></div>
                                                    <div class="_value">с <?= Yii::$app->formatter->asDate($object->date_start) ?? '----' ?>
                                                    <?php if($object->date_end) { ?>
                                                        по <?= \Yii::$app->formatter->asDate($object->date_end) ?>
                                                    <?php } ?>
                                                    </div>
                                                </li>
                                                <li class="_item">
                                                    <div class="_label">Заказчик</div>
                                                    <div class="_value"><?= $object->employerName ? $object->employerName :
                                                            ($object->employer ? \yii\helpers\Html::a($object->employer->title, ['/company/companies/company_info', 'url' => $object->employer->url]) : '----') ?></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="popup-portfolio__section">
                            <div class="popup-portfolio__section-head">
                                <div class="popup-portfolio__section-title"><?= Yii::t('app', 'Информация об объекте') ?></div>
                            </div>
                            <div class="popup-portfolio__section-body">
                                <div class="popup-portfolio__info">
                                    <ul class="popup-portfolio__info-list">
                                        <li class="_item">
                                            <div class="_label"><?= Yii::t('app', 'Описание') ?></div>
                                            <div class="_value">
                                                <div class="popup-portfolio__more js-company-more-box">
                                                    <div class="_desc">
                                                        <?= \yii\helpers\Html::decode($object->description)?>
                                                    </div>
                                                    <div class="_action">
                                                        <button class="_btn js-company-more-btn" data-open="Раскрыть полное описание" data-close="Скрыть полное описание"></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_item">
                                            <div class="_label"><?= Yii::t('app', 'Функции компании')?></div>
                                            <div class="_value"><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($functions, 'title')) ?></div>
                                        </li>

                                        <li class="_item">
                                            <div class="_label"><?= Yii::t('app', 'Виды работ')?></div>
                                            <div class="_value"><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($typeWorks, 'title')) ?></div>
                                        </li>
                                    </ul>

                                    <?php if ($object->completedWorks) {?>
                                        <div class="popup-portfolio__info-option">
                                            <div class="_title"><?= Yii::t('app', 'Выполненные работы')?></div>
                                            <ul class="_list">

                                                <?php foreach ($object->completedWorks as $work ) { ?>
                                                    <li class="_item">
                                                        <div class="_label"><?= $work->type->title ?></div>
                                                        <div class="_value"><?= $work->count ?>
                                                            <?= \common\modules\directories\models\common\Directories::getUnitData($work->unitId)?></div>
                                                    </li>
                                                <?php  } ?>

                                            </ul>
                                        </div>

                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <div class="popup-portfolio__section">
                            <div class="popup-portfolio__section-head">
                                <div class="popup-portfolio__section-title"><?= Yii::t('app', 'Фотографии'); ?></div>
                            </div>
                            <div class="popup-portfolio__section-body">
                                <div class="gallery-block js-gallery">
                                    <div class="gallery-block__inner">
                                        <div class="gallery-block__view">
                                            <ul class="gallery-block__view-list js-gallery-view">
                                            <?php foreach ($object->files as $file) {?>
                                                <li class="gallery-block__view-item">
                                                    <div class="_item"
                                                         style="background-image: url('<?= \common\components\Thumbnail::thumbnailFileUrl($file->getFile(true), 1078, 600); ?>');"></div>
                                                </li>

                                            <?php } ?>

                                            </ul>
                                        </div>
                                        <div class="gallery-block__nav">
                                            <ul class="gallery-block__nav-list js-gallery-nav">

                                                <?php foreach ($object->files as $file) {?>
                                                    <li class="gallery-block__nav-item">
                                                        <div class="_item"
                                                             style="background-image: url('<?= \common\components\Thumbnail::thumbnailFileUrl($file->getFile(true), 200, 114); ?>');"></div>
                                                    </li>

                                                <?php } ?>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--
                        <div class="company-feedback">
                            <div class="company-feedback__inner">
                                <div class="company-feedback__desc static">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nib euismod tincidunt ut laoreet dolore
                                    magn aliquam erat volutpat. Ut wisi enim a minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                                    nisl ut
                                    aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod
                                    tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
                                    ullamcorper
                                    suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                </div>
                                <div class="company-feedback__title">Юрий Алексеев — Генеральный директор ООО «Первая строительная компания»</div>
                                <div class="company-feedback__option">
                                    <ul class="company-feedback__option-list">
                                        <li class="company-feedback__option-item">
                                            <div class="_label">Объект:</div>
                                            <div class="_value">Бизнес-центр Art Street</div>
                                        </li>
                                        <li class="company-feedback__option-item">
                                            <div class="_label">Оценка: </div>
                                            <div class="_value"><span class="_qty orange">4.0</span></div>
                                        </li>
                                        <li class="company-feedback__option-item">
                                            <div class="_label">Дата отзыва: </div>
                                            <div class="_value">12.05.2018</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
