<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */

$loadPage = $dataProvider->pagination->page;

?>

<?php
$button = '';
if($dataProvider->pagination->pageCount != $dataProvider->pagination->page+1) {
    $button = '<button class="btn btn-more">'. Yii::t("app", "ПОКАЗАТЬ ЕЩЕ") .'</button>';
 } ?>

<?php

$template = $isAjax ? '{items}' : '{items}
<div class="section__action load-more"  
data-page-count="' . $dataProvider->pagination->pageCount . '"  data-ajax-url="' . \yii\helpers\Url::current() . '" data-load-page="' . $loadPage  . '">
' . $button . '
</div>';

echo \modules\crud\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $template,
    'options' => ['class' => 'section__body'],
    'itemOptions' => [
        'tag' => 'div',
        //  'class' => 'section__inner company-item',
        'class' => 'company-item',
    ],
    'itemView' => '_companies_list_item',
    // 'emptyText' => '<div class="filter-result__empty"><div class="_title">' . Yii::t('app', $emptyMessage) . '</div></div>',
    'pager' => false,
])
?>
