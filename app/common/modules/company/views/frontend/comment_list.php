<?php
/**
 * @var \common\modules\company\models\frontend\Company $company
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$loadPage = $dataProvider->pagination->page;
?>

<div class="default">
    <div class="default__inner">
        <div class="breadcrumbs ">
            <div class="container">
                <div class="breadcrumbs__inner">
                    <?= \yii\widgets\Breadcrumbs::widget(['itemTemplate' => '<li class="breadcrumbs__item">{link}</li>',
                        'homeLink' => ['label' => 'Главная страница', 'url' => '/', 'class' => 'breadcrumbs__link'],
                        'options' => ['class' => 'breadcrumbs__list'], 'links' => $this->params['breadcrumbs'] ?? [],
                        'activeItemTemplate' => '<li class="active breadcrumbs__item">{link}</li>']); ?>
                </div>
            </div>
        </div>
        <?= \common\modules\company\widgets\CompanyInfo::widget(['company' => $company, 'comment' => true]); ?>

        <div class="tabs-block tabs-block--link">
            <div class="container">
                <div class="tabs-block__inner">
                    <div class="tabs-block__head">
                        <?= $this->render('_tab_menu', ['company' => $company]) ?>
                    </div>

                    <div class="tabs-block__body">
                        <div class="section">
                            <div class="section__inner">
                                <div class="section__body">

                                    <?= $this->render('_comment_list_inner', ['dataProvider' => $dataProvider, 'isAjax' => $isAjax]) ?>

                                    <?php if ($dataProvider->totalCount > 0) { ?>
                                        <div class="section__action load-more" data-ajax-url="<?= \yii\helpers\Url::current() ?>"
                                             data-page-count="<?= $dataProvider->pagination->pageCount ?>"
                                             data-type="comment"
                                             data-load-page="<?= $loadPage ?>">

                                            <?php if($dataProvider->pagination->pageCount != $dataProvider->pagination->page+1) { ?>
                                                <button class="btn btn-more"><?= Yii::t('app', 'ПОКАЗАТЬ ЕЩЕ') ?></button>
                                           <?php } ?>

                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <div class="pagination__inner">
                                <?= $this->render('_companies_pagination', ['dataProvider' => $dataProvider]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!Yii::$app->user->isGuest) { ?>
    <div class="popup popup--feedback" data-popup-id="feedback-step1" id="feedback-step1" style="display: none;">
        <div class="popup__box">
            <div class="popup__box-inner">
                <button class="popup__close js-close-wnd" type="button"></button>

                <div class="popup__title"><?= Yii::t('app', 'Оценить компанию') ?></div>
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'validateOnBlur' => false,
                    //  'enableClientValidation' => false,
                    'action' => \yii\helpers\Url::current(),
                    'fieldConfig' => [
                        'template' => '{label}{input}{hint}<div class="help-block__head">{error}</div>',
                    ],
                ]); ?>
                <div class="popup__body">

                    <div class="popup-feedback">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="" class="control-label "><?= Yii::t('app', 'Ваше имя')?></label>

                                    <input type="text" class="form-control " placeholder="<?= Yii::$app->user->identity->username ?>" disabled>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="" class="control-label "><?= Yii::t('app', 'Фамилия')?></label>

                                    <input type="text" class="form-control " placeholder="<?= Yii::$app->user->identity->lastname ?>" disabled>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="" class="control-label "><?= Yii::t('app', 'Организация') ?></label>

                                    <input type="text" class="form-control " placeholder="<?= Yii::$app->user->getActiveCompanyData()['title'] ?>" disabled>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="" class="control-label "><?= Yii::t('app', 'Должность в организации') ?></label>

                                    <input type="text" class="form-control "
                                           placeholder="<?= Yii::$app->user->identity->companyUser->position ?? '' ?>" disabled>


                                </div>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'object_id')
                                    ->dropDownList( \yii\helpers\ArrayHelper::map($company->searchPortfolio([], true)->asArray()->all(), 'id', 'title'),
                                        ['class' => 'js-dropdown-box'])
                                    ->error(['tag' => 'div']) ?>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">

                                    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="popup__footer">
                    <div class="popup__action popup__action--right">
                        <button class="btn btn-outline-secondary js-close-wnd" type="reset"><?= Yii::t('app', 'ОТМЕНИТЬ')?></button>
                        <input class="btn btn-primary" type="submit" value="ОТПРАВИТЬ">
                    </div>
                </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>

            </div>
        </div>
    </div>
<?php } ?>
