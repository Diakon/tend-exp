<?php
/**
 * @var \yii\data\ActiveDataProvider $portfolioDataProvider
 * @var \yii\web\View $this
 */

?>

<?php

echo \modules\crud\widgets\ListView::widget([
    'dataProvider' => $portfolioDataProvider,
    'layout' => '{items}',
    'options' => ['class' => 'company-portfolio__row'],
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'company-portfolio__col',
    ],
    'itemView' => '_portfolio_list_item',
     'emptyText' => '',
    'pager' => false,
])
?>
