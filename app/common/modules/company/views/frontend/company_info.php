<?php
/**
 * @var \common\modules\company\models\frontend\Company $company
 */

$diff = date('Y', time()) - $company->year;
$employeesNowLabel = \common\modules\directories\models\common\EmployeesNow::findActiveList();
$employeesMaxLabel = \common\modules\directories\models\common\EmployeesMax::findActiveList();
?>
<div class="default">
                <div class="default__inner">

                    <div class="breadcrumbs ">
                        <div class="container">

                            <div class="breadcrumbs__inner">

                                <?= \yii\widgets\Breadcrumbs::widget(['itemTemplate' => '<li class="breadcrumbs__item">{link}</li>',
                                    'homeLink' => ['label' => 'Главная страница', 'url' => '/', 'class' => 'breadcrumbs__link'],
                                    'options' => ['class' => 'breadcrumbs__list'], 'links' => $this->params['breadcrumbs'] ?? [],
                                    'activeItemTemplate' => '<li class="active breadcrumbs__item">{link}</li>']); ?>

                            </div>
                        </div>
                    </div>

                    <?= \common\modules\company\widgets\CompanyInfo::widget(['company' => $company]); ?>

                    <div class="tabs-block tabs-block--link">
                        <div class="container">
                            <div class="tabs-block__inner">
                                <div class="tabs-block__head">

                                    <?= $this->render('_tab_menu', ['company' => $company ]) ?>

                                </div>

                                <div class="tabs-block__body">
                                    <div class="section">
                                        <div class="section__inner">
                                            <div class="section__body">
                                                <div class="company-common">
                                                    <div class="company-common__inner">
                                                        <div class="company-common__head">
                                                            <div class="company-common__title"><?= Yii::t('app', 'Общая информация') ?></div>
                                                        </div>
                                                        <div class="company-common__body">
                                                            <ul class="company-common__list">
                                                                <li class="company-common__item">

                                                                    <div class="_label"><?= Yii::t('app', 'Год основания') ?></div>
                                                                    <div class="_value"><?= $company->year  ?> <?php if ($diff) {?>
                                                                            год (<?= $diff ?> <?= \helpers\Text::plural($diff, ['год', 'года', 'лет']) ?> в отрасли)
                                                                        <?php }?></div>
                                                                </li>
                                                                <li class="company-common__item">
                                                                    <div class="_label"><?= Yii::t('app', 'Генеральный директор') ?></div>
                                                                    <div class="_value"><?= $company->generalManage ?></div>
                                                                </li>
                                                                <li class="company-common__item">
                                                                    <div class="_label"><?= Yii::t('app', 'Регион деятельности') ?></div>
                                                                    <div class="_value">
                                                                        <?php foreach ($company->charsCountryIds as $countryId) {
                                                                            $country = \common\models\GeoCountry::find()->where(['id' => $countryId])->one();
                                                                            $regions = \yii\helpers\ArrayHelper::map(\common\models\GeoRegion::find()
                                                                                ->where(['in', 'id', $company->charsRegionIds])
                                                                                ->orderBy(['name' => SORT_ASC])
                                                                                ->all(), 'id', 'name');
                                                                            echo (!empty($country->name) ? ($country->name . ': ') : '') . implode(", ", $regions);
                                                                        } ?>
                                                                    </div>
                                                                </li>
                                                                <li class="company-common__item">
                                                                    <div class="_label"><?= Yii::t('app', 'Сотрудников в штате') ?></div>
                                                                    <div class="_value"><?= $employeesNowLabel[$company->employeesNow] ?? 0  ?> человек</div>
                                                                </li>
                                                                <li class="company-common__item">
                                                                    <div class="_label"><?= Yii::t('app', 'Сотрудников максимум')?></div>
                                                                    <div class="_value"><?= $employeesMaxLabel[$company->employeesMax] ?? 0  ?> человек</div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="company-common">
                                                    <div class="company-common__inner">
                                                        <div class="company-common__head">
                                                            <div class="company-common__title"><?= Yii::t('app', 'Функции компании') ?></div>
                                                        </div>
                                                        <div class="company-common__body">
                                                            <ul class="company-common__list">

                                                                <?php foreach ($objectData['functions'] as $key => $count) { ?>

                                                                    <li class="company-common__item">
                                                                        <div class="_label"><?= Yii::t('app', $key) ?></div>
                                                                        <div class="_value"><?= $count ?>
                                                                            <?= \helpers\Text::plural($count, ['проект', 'проекта', 'проектов']) ?>
                                                                        </div>
                                                                    </li>
                                                                <?php } ?>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="company-common">
                                                    <div class="company-common__inner">
                                                        <div class="company-common__head">
                                                            <div class="company-common__title"><?= Yii::t('app', 'Направления строительства') ?></div>
                                                        </div>
                                                        <div class="company-common__body">
                                                            <ul class="company-common__list">

                                                                <?php foreach ($objectData['activities'] as $key => $count) { ?>
                                                                    <?php
                                                                    $key = str_replace(Yii::t('app', 'строительство'), "", $key);
                                                                    ?>
                                                                    <li class="company-common__item">
                                                                        <div class="_label"><?= Yii::t('app', trim($key)) ?></div>
                                                                        <div class="_value"><?= $count ?>
                                                                            <?= \helpers\Text::plural($count, ['проект', 'проекта', 'проектов']) ?>
                                                                        </div>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="company-common">
                                                    <div class="company-common__inner">
                                                        <div class="company-common__head">
                                                            <div class="company-common__title"><?= Yii::t('app', 'Виды работ') ?></div>
                                                        </div>
                                                        <div class="company-common__body">
                                                            <ul class="company-common__list">
                                                                <?php foreach ($company->completedWorks as $work ) { ?>
                                                                    <li class="company-common__item">
                                                                        <div class="_label"><?= $work->type->title ?></div>
                                                                        <div class="_value"><?= $work->count ?>
                                                                            <?= \common\modules\directories\models\common\Directories::getUnitData($work->unitId)?></div>
                                                                    </li>
                                                                <?php  } ?>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


