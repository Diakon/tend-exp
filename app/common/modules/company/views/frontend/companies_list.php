<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */

use yii\helpers\Url;
$sort = $dataProvider->sort;

$isGuest = Yii::$app->user->isGuest;
$ShowAdditionalFilter = Yii::$app->user->canShowAdditionalFilter();
?>
<div class="default">
    <div class="default__inner">
        <div class="breadcrumbs ">
            <div class="container">
                <div class="breadcrumbs__inner">

                    <?= \yii\widgets\Breadcrumbs::widget(['itemTemplate' => '<li class="breadcrumbs__item">{link}</li>',
                        'homeLink' => ['label' => 'Главная страница', 'url' => '/', 'class' => 'breadcrumbs__link'],
                        'options' => ['class' => 'breadcrumbs__list'], 'links' => $this->params['breadcrumbs'] ?? [],
                        'activeItemTemplate' => '<li class="active breadcrumbs__item">{link}</li>']); ?>
                </div>
            </div>
        </div>

        <div class="page-title">
            <div class="container">
                <div class="page-title__inner">
                    <h1 class="page-title__title"><span class="_text"><?= $this->title ?></span> <span class="page-title__label">ВСЕГО <?= $dataProvider->totalCount ?></span>
                    </h1>
                    <div class="page-title__desc">Ниже представлен список строительных компаний, специализирующихся в различных
                        отраслях строительства.
                        С помощью сервиса TENDEROS вы сможете найти компанию, отвечающую любым вашим потребностям.
                    </div>
                </div>
            </div>
        </div>

        <div class="filter">
            <div class="container">
                <div class="filter__inner">
                    <div class="filter__head">
                        <div class="filter__item">
                            <div class="filter__label"><?= Yii::t('app', 'Сортировать по:') ?></div>
                            <div class="filter__select">
                                <div class="drop-list js-sort-list">

                                    <div class="drop-list__header js-sort-list-open">
                                        <span class="drop-list__arrow"></span>
                                        <span class="drop-list__text"><?= $model->getCurrentLabel() ?></span>
                                    </div>

                                    <div class="drop-list__box">
                                        <ul class="drop-list__list">
                                            <li>
                                                <?= \yii\helpers\Html::a($model->getCurrentLabel('year'), Url::current(['sort' => 'year'])) ?>
                                            </li>
                                            <li>
                                                <?= \yii\helpers\Html::a($model->getCurrentLabel('-year'), Url::current(['sort' => '-year'])) ?>
                                            </li>
                                            <li>
                                                <?= \yii\helpers\Html::a($model->getCurrentLabel('employees_now'), Url::current(['sort' => 'employees_now'])) ?>
                                            </li>
                                            <li>
                                                <?= \yii\helpers\Html::a($model->getCurrentLabel('-employees_now'), Url::current(['sort' => '-employees_now'])) ?>
                                            </li>
                                            <li>
                                                <?= \yii\helpers\Html::a($model->getCurrentLabel('count_objects'), Url::current(['sort' => 'count_objects'])) ?>
                                            </li>
                                            <li>
                                                <?= \yii\helpers\Html::a($model->getCurrentLabel('-count_objects'), Url::current(['sort' => '-count_objects'])) ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="filter__body">
                        <div class="filter__action">

                            <button class="btn btn-outline-secondary hidden-md hidden-xs js-advanced-search-btn"
                                    data-popup="<?= !$isGuest && $ShowAdditionalFilter ? 'advanced-search' : 'improve' ?>">
                                <svg class="icon icon-filter">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#filter"/>
                                </svg>
                                <span>РАСШИРЕННЫЙ ПОИСК</span>
                            </button>

                            <button class="btn btn-primary js-open-sidebar-search">УТОЧНИТЬ ПОИСК</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="section__inner">
                    <?= $this->render('_companies_list_inner', ['dataProvider' => $dataProvider, 'isAjax' => $isAjax]) ?>
                </div>
            </div>
        </div>

        <div class="pagination">
            <div class="pagination__inner">
                <?= $this->render('_companies_pagination', ['dataProvider' => $dataProvider]) ?>
            </div>
        </div>
    </div>
</div>


<?php if (!$isGuest && $ShowAdditionalFilter) { ?>
    <?= $this->render('_filter_company', ['model' => $model, 'address' => $address]) ?>
<?php } else { ?>
    <?php echo $this->render('@themes/frontend/views/frontend/notice_need_tariff') ?>
<?php } ?>

<?= $this->render('_companies_base_filter', ['model'=> $model, 'address' => $address]) ?>

<?php //echo \frontend\modules\uwm\widget\UwmWidget::widget([
//    'template' => 'compare_list_widget',
//    'model' => \common\modules\company\models\frontend\Company::class,
//    'type' => Uwm::TYPE_COMPARE,
//]); ?>
