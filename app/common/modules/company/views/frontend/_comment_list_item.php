<?php
/**
 * @var \common\modules\company\models\common\CommentCompany $model
 */
use \common\modules\company\models\common\CompanyUsers;

if($model->author) {
    $companyUser = CompanyUsers::getUserCompany($model->author->id);
} else {
    $companyUser = new CompanyUsers();
}

?>

<div class="company-feedback__inner">
    <div class="company-feedback__desc static">
        <?= \yii\helpers\Html::decode($model->comment) ?>
    </div>
    <div class="company-feedback__title"><?= isset($model->author) ? $model->author->getFullName() : '' ?> — <?= $companyUser->position ?? ''; ?>
        <?= isset($companyUser->company) ? $companyUser->company->title : '' ?></div>
    <div class="company-feedback__option">
        <ul class="company-feedback__option-list">
            <li class="company-feedback__option-item">
                <div class="_label"><?= Yii::t('app', 'Объект') ?>:</div>
                <div class="_value"><?= $model->object ? $model->object->title : '-----' ?></div>
            </li>
<!--            <li class="company-feedback__option-item">-->
<!--                <div class="_label">Оценка:</div>-->
<!--                <div class="_value"><span class="_qty green">5.0</span></div>-->
<!--            </li>-->
            <li class="company-feedback__option-item">
                <div class="_label"><?= Yii::t('app', 'Дата отзыва') ?>:</div>
                <div class="_value"><?= Yii::$app->formatter->asDate($model->created_at, Yii::$app->params['dateFormat'])?></div>
            </li>
        </ul>
    </div>
</div>

