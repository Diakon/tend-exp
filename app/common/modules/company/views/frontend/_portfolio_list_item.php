<?php

/**
 * @var \common\modules\tender\models\frontend\Objects $model
 */
$charsWorks = $model->company->charsWorks ?? [];

use yii\helpers\Html;


?>


<div class="company-portfolio__item">

    <a href="#" class="company-portfolio__link" data-popup-ajax="<?= \yii\helpers\Url::to(['/company/companies/object_info', 'id' => $model->id]) ?>"></a>
    <div class="company-portfolio__head">
<!--        <div class="company-portfolio__confirm ">ПОДТВЕРЖДЕН ЗАКАЗЧИКОМ</div>-->
        <div class="company-portfolio__status"><span class="_text"><?= $model->getStatusComplete($model->statusCompleteId) ?? '----' ?></span></div>
    </div>

    <div class="company-portfolio__body">
        <div class="company-portfolio__title">
            <?= $model->title ?>
        </div>
        <div class="company-portfolio__label"><?= $model->address->getFullAddress() ?? '----' ?></div>
        <div class="company-portfolio__option">
            <ul class="company-portfolio__option-list">
                <li class="company-portfolio__option-item">
                    <div class="_label"><?= Yii::t('app', 'Функции компании')?></div>
                    <div class="_value">Генеральный подрядчик</div>
                </li>
                <li class="company-portfolio__option-item">
                    <div class="_label"><?= Yii::t('app', 'Вид деятельности')?></div>
                    <div class="_value">Гражданское строительство</div>
                </li>
                <li class="company-portfolio__option-item">
                    <div class="_label"><?= Yii::t('app', 'Тип объекта')?></div>
                    <div class="_value">Торговый центр</div>
                </li>
                <li class="company-portfolio__option-item">
                    <div class="_label"><?= Yii::t('app', 'Начало работ') ?></div>
                    <div class="_value"><?= Yii::$app->formatter->asDate($model->date_start) ?? '----' ?></div>
                </li>
                <li class="company-portfolio__option-item">
                    <div class="_label"><?= Yii::t('app', 'Заказчик') ?></div>
                    <div class="_value"><?= $model->employerName ? $model->employerName :
                            ($model->employer ? Html::a($model->employer->title, ['/company/companies/company_info', 'url' => $model->employer->url]) : '----') ?></div>
                </li>
            </ul>
        </div>
    </div>
</div>

