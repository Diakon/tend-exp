<?php
/**
 * @var \yii\data\ActiveDataProvider $portfolioDataProvider
 * @var \yii\web\View                $this
 */

$loadPage = $portfolioDataProvider->pagination->page;
?>


<div class="default">
    <div class="default__inner">
        <div class="breadcrumbs ">
            <div class="container">
                <div class="breadcrumbs__inner">
                    <?= \yii\widgets\Breadcrumbs::widget(['itemTemplate' => '<li class="breadcrumbs__item">{link}</li>',
                        'homeLink' => ['label' => 'Главная страница', 'url' => '/', 'class' => 'breadcrumbs__link'],
                        'options' => ['class' => 'breadcrumbs__list'], 'links' => $this->params['breadcrumbs'] ?? [],
                        'activeItemTemplate' => '<li class="active breadcrumbs__item">{link}</li>']); ?>
                </div>
            </div>
        </div>

        <?= \common\modules\company\widgets\CompanyInfo::widget(['company' => $company]); ?>

        <div class="tabs-block tabs-block--link">
            <div class="container">
                <div class="tabs-block__inner">
                    <div class="tabs-block__head">
                        <?= $this->render('_tab_menu', ['company' => $company ]) ?>
                    </div>
                    <div class="tabs-block__body">
                        <div class="section">
                            <div class="section__inner">
                                <div class="section__body">
                                    <div class="company-portfolio">

                                        <div class="company-portfolio__inner">
                                            <?= $this->render('_companies_portfolio_inner', ['portfolioDataProvider' => $portfolioDataProvider, 'isAjax' => $isAjax]) ?>
                                        </div>

                                        <div style="<?= empty($portfolioDataProvider->getTotalCount()) ? 'display:none;' : '' ?>"
                                             class="section__action load-more"
                                             data-ajax-url="<?= \yii\helpers\Url::current() ?>"
                                             data-type="portfolio"
                                             data-load-page="<?= $loadPage ?>"
                                             data-page-count="<?= $portfolioDataProvider->pagination->pageCount ?>"
                                        >

                                            <?php if($portfolioDataProvider->pagination->pageCount != $portfolioDataProvider->pagination->page+1) { ?>
                                                <button class="btn btn-more"><?= Yii::t('app', 'ПОКАЗАТЬ ЕЩЕ')?></button>
                                            <?php } ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <div class="pagination__inner">
                                <?= $this->render('_companies_pagination', ['dataProvider' => $portfolioDataProvider]) ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
