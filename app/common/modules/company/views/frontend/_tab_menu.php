<?php
/**
 * @var \common\modules\company\models\frontend\Company $company
 */

?>


<?= \yii\widgets\Menu::widget([
    'encodeLabels' => false,
    'items' => [
        [
            'label' => Yii::t('app', 'ОБЩЕЕ'),
            'url' => ['/company/companies/company_info', 'url' => $company->url],
        ],
        [
            'label' => Yii::t('app', 'ПОРТФОЛИО') . '<span class="_qty">' . $company->searchPortfolio([], true)->asArray()->count() . '</span>',
            'url' => ['/company/companies/portfolio_list', 'url' => $company->url],
        ],
        [
            'label' => Yii::t('app', 'ОТЗЫВЫ') . '<span class="_qty">' . $company->getComments()->asArray()->count() . '</span>',
            'url' => ['/company/companies/comment_list', 'url' => $company->url],
        ],

    ],
    'options' => ['class' => 'tabs-block__list', 'tag' => 'div'],
    'itemOptions' => ['class' => 'tabs-block__btn'],
]);
?>
