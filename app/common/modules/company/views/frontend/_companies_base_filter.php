<div class="search-sidebar js-search-sidebar">
    <?php
    $inputStyle = ['horizontalCssClasses' => ['wrapper' => '', 'label' => '']];

    $form = \yii\bootstrap\ActiveForm::begin(
        [
            // 'layout' => 'horizontal',
            'action' => \yii\helpers\Url::to(['/company/companies/company_list']),
            'method' => 'GET', 'options' => ['id' => 'catalog-form-lite', 'class' => 'search-sidebar__inner'],
        ]); ?>

    <div class="search-sidebar__head">
        <button class="search-sidebar__close js-open-sidebar-search" type="button">
            <svg class="icon icon-close ">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#close"/>
            </svg>
        </button>
        <div class="search-sidebar__title"><?= Yii::t('app', 'Уточнить поиск') ?></div>
        <div class="search-sidebar__input">

            <?php $searchButton = '<button class="_btn" type="submit">
                                            <svg class="icon icon-search">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" 
                                                     xlink:href=" ' . Yii::getAlias('@static') . '/../static/images/svg/spriteInline.svg#search"/>
                                            </svg>
                                        </button>';


            echo $form->field($model, 'inn', ['template' => $searchButton . '{input}',
                'options' => ['class' => 'form-group  form-group--search']])
                ->textInput(['placeholder' => Yii::t('app', 'ИНН')])
                ->label('false')

            ?>


        </div>
    </div>

    <div class="search-sidebar__body js-custom-scroll">
        <div class="search-sidebar__body-inner">
            <div class="search-sidebar__section js-acco">
                <div class="search-sidebar__section-head js-acco-title">
                    <label class="control-label advanced-search__caption"><?= Yii::t('app', 'Адрес компании') ?></label>
                </div>
                <div class="search-sidebar__section-body">
                    <div class="form-group form-group--autocomplete">
                        <?= $form->field($address, 'country_id', $inputStyle)->dropDownList(\common\models\GeoCountry::getCountryList(),
                            ['class' => '_select js-autocomplete-select', 'data-result-hidden-input-name' => 'result-input', 'data-empty-text' => 'Ничего не найдено', 'placeholder' => '', 'prompt' => '']) ?>
                    </div>
                    <div class="form-group form-group--autocomplete">
                        <?= $form->field($address, 'region_id', $inputStyle)
                            ->dropDownList([Yii::t('app', 'Выберите регион')] + \common\models\GeoRegion::getRegionsList(),
                                ['class' => '_select js-autocomplete-select', 'data-result-hidden-input-name' => 'result-input', 'data-empty-text' => 'Ничего не найдено', 'placeholder' => '', 'prompt' => '']) ?>
                    </div>
                    <div class="form-group form-group--autocomplete">
                        <?= $form->field($address, 'city_id', $inputStyle)
                            ->dropDownList([Yii::t('app', 'Выберите город')] + \common\models\GeoCity::getCityList(),
                                ['class' => '_select js-autocomplete-select', 'data-result-hidden-input-name' => 'result-input', 'data-empty-text' => 'Ничего не найдено', 'placeholder' => '', 'prompt' => '']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="search-sidebar__body-inner">
            <div class="search-sidebar__section js-acco">
                <div class="search-sidebar__section-head js-acco-title">
                    <label class="control-label advanced-search__caption"><?= Yii::t('app', 'Регион деятельности') ?></label>
                </div>
                <div class="search-sidebar__section-body">
                    <div class="form-group form-group--autocomplete">
                        <?= $form->field($model, 'activityCountry', ['template' => '{label}{input}{error}'])
                            ->dropDownList(\common\models\GeoCountry::getCountryList(),
                            [/*'class' => '_select js-autocomplete-select',*/ 'data-empty-text' => 'Ничего не найдено', 'placeholder' => '', 'prompt' => ''])
                            ->label('Страна');
                        ?>
                    </div>
                    <div class="form-group form-group--autocomplete">
                        <?= $form->field($model, 'activityRegions', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                            ->dropDownList(\common\models\GeoRegion::getRegionsList(),
                                [
                                    'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать', 'prompt' => '']
                            )->label('Регион деятельности');
                        ?>
                    </div>
                </div>
            </div>
        </div>

    <div class="search-sidebar__body js-custom-scroll">
        <div class="search-sidebar__body-inner">

            <div class="search-sidebar__section js-acco">
                <div class="search-sidebar__section-head js-acco-title">
                    <?= Yii::t('app', ' Функции компании')?>
                </div>
                <div class="search-sidebar__section-body">


                    <?= $form->field($model, 'functionsFilter',
                        [
                            'template' => '{beginWrapper}{input}{endWrapper}',
                            'wrapperOptions' => ['class' => '_list']])
                        ->checkboxList(
                            \common\modules\directories\models\common\FunctionsCompany::findActiveList(),
                            [
                                'tag' => false,
                                'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                    $checked = $checked == 1 ? 'checked="checked"' : '';

                                    return "<label class='check-box'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> 
                                                <span class='check-label'>{$label}</span></label>";
                                },
                            ])->label(false);
                    ?>

                </div>
            </div>

            <div class="search-sidebar__section js-acco">
                <div class="search-sidebar__section-head js-acco-title">
                    <?= Yii::t('app', 'Направления строительства')?>
                </div>
                <div class="search-sidebar__section-body">
                    <?= $form->field($model, 'buildsFilter',
                        [
                            'template' => '{beginWrapper}{input}{endWrapper}',
                            'wrapperOptions' => ['class' => '_list']])
                        ->checkboxList(
                            \common\modules\directories\models\common\Activities::findActiveList(),
                            [
                                'tag' => false,
                                'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                    $checked = $checked == 1 ? 'checked="checked"' : '';

                                    return "<label class='check-box'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> 
                                                <span class='check-label'>{$label}</span></label>";
                                },
                            ])->label(false);
                    ?>
                </div>
            </div>

            <?php /*
            <div class="search-sidebar__section js-acco">
                <div class="search-sidebar__section-head js-acco-title">
                    <?= Yii::t('app', 'Направления строительства')?>
                </div>
                <div class="search-sidebar__section-body">
                    <?= $form->field($model, 'buildsFilter',
                        ['horizontalCssClasses' => ['wrapper' => 'advanced-search__fields advanced-search__fields--type-3']])
                        ->checkboxList(
                            \common\modules\directories\models\common\MainWork::findActiveList(),
                            [
                                'tag' => false,
                                'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                    $checked = $checked == 1 ? 'checked="checked"' : '';

                                    return "<label class='check-box'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> 
                                                <span class='check-label'>{$label}</span></label>";
                                },
                            ])->label(false);
                    ?>
                </div>
            </div>
            */ ?>
        </div>
    </div>

    <div class="search-sidebar__foot">
        <button class="btn btn-primary btn-small" type="submit"><?= Yii::t('app', 'ПРИМЕНИТЬ') ?></button>
        <a href="<?= \yii\helpers\Url::to(['/companies/']) ?>"
           class="btn _btn btn-reset">
        <?= Yii::t('app', 'СБРОСИТЬ ВСЕ ФИЛЬТРЫ') ?>
        </a>
    </div>
    <?php $form::end(); ?>
</div></div>