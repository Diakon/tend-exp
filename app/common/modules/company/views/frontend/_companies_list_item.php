<?php
/**
 * @var \common\modules\company\models\frontend\Company $model
 * @var \common\modules\company\models\common\CompanyCharsBuilds[] $charsBuilds
 * @var \common\modules\company\models\common\CompanyCharsWorks[]  $charsWorks
 */

use common\modules\directories\models\common\Activities;
use frontend\modules\uwm\Module as Uwm;

$model->setCharsParam();

$charsBuilds = $model->charsBuilds ?? [];
$charsWorks = $model->charsWorks ?? [];
$functionsCompany = $model->functionsCompany ?? [];

$compareSelected = Uwm::isSelected($model->id, Uwm::TYPE_COMPARE, $model::classNameShort());
$compareButtonOptions['class'] = $compareSelected ? 'car__compare js-compare car__compare--del' : 'car__compare js-compare';
$compareButtonOptions['data-id'] = $model->id;
$compareButtonOptions['data-url'] = \yii\helpers\Url::to(['/uwm/compare/index', 'id' => $model->id]);
$compareButtonOptions['data-model'] = $model::classNameShort();
$diff = date('Y', time()) - $model->year;
?>

<?php // \yii\helpers\Html::tag('button', $compareSelected ? 'Удалить из сравнения' : 'Добавить к сравнению', $compareButtonOptions); ?>

<div class="company-item">
    <div class="company-item__inner">
        <div class="company-item__head">
            <div class="company-item__type"><?= Yii::t('app', 'Направление строительства')?>:
                <span class="_inline"> <?php foreach ($charsBuilds as $item) { ?>
                        <?= implode(', ', \yii\helpers\ArrayHelper::map(Activities::findActive()->andWhere(['in', 'id', $model->charsBuildsIds])->asArray()->all(), 'title', 'title')) ?>
                    <?php } ?>
            </span>
            </div>
            <div class="company-item__year">
                <?= implode(', ', \yii\helpers\ArrayHelper::getColumn($functionsCompany, 'title')) ?>:
                <span class="_inline"><?= $diff ?> <?= \helpers\Text::plural($diff, ['год', 'года', 'лет']) ?> <?= Yii::t('app', 'опыта')?></span>
            </div>
        </div>
        <div class="company-item__body">
            <div class="company-item__left">
                <div class="company-item__logo">
                    <?php if ($model->avatar) { ?>
                        <img class="_img" src="<?php echo \common\components\Thumbnail::thumbnailFile($model->avatar->getFile(true)); ?>">
                    <?php } else { ?>
                        <img src="<?= Yii::getAlias('@static') ?>/../content/images/avatar2.svg" alt="" class="_img">
                    <?php } ?>
                </div>
                <!--<div class="company-item__status company-item__status--red"><span class="_text">offline</span></div>-->
            </div>

            <div class="company-item__main">
                <div class="company-item__title">
                    <?= \yii\helpers\Html::a($model->title, \yii\helpers\Url::to(['/company/companies/company_info', 'url' => $model->url], ['class' => '_link'])) ?>
                </div>
                <div class="company-item__address">
                    <?=  $model->actualAddress ? $model->actualAddress->getFullAddress(true) : '' ?>
                </div>
                <!--
                <div class="company-item__rating">
                    <span class="_value">5.0</span>
                    <span class="_qty">83 отзыва</span>
                </div>
                -->
                <?php //ToDo удрать услови 1==2 когда Настя скажет как передать виды работ ?>
                <?php if (1==2 && !empty($model->completedWorks)) { ?>
                    <div class="company-item__table js-company-more-box opened">
                        <div class="company-item__table-head">
                            <div class="_title"><?= Yii::t('app', 'Виды работ') ?></div>
                            <button class="_btn js-company-more-btn" data-open="Раскрыть все виды работ" data-close="Скрыть все виды работ"></button>
                        </div>
                        <div class="company-item__table-body">
                            <div class="company-item__option">
                                <?php foreach ($model->completedWorks as $work ) { ?>
                                    <div class="company-item__option-item">
                                        <div class="_label"><span><?= $work->type->title ?></span></div>
                                        <div class="_value"><?= $work->count ?> <?= \common\modules\directories\models\common\Directories::getUnitData($work->unitId)?></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!--
            <div class="company-item__action">
                <button class="btn-add">
                    <svg class="icon icon-add ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../static/images/svg/spriteInline.svg#add"/>
                    </svg>        </button>
            </div>
            -->
        </div>
    </div>
</div>

