<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */

?>

<?php

echo \modules\crud\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'options' => ['class' => 'company-feedback-margin'],
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'company-feedback',
    ],
    'itemView' => '_comment_list_item',
    'emptyText' => '<div class="result__empty">' . Yii::t('app', 'У компании еще нет отзывов') . '</div>',
    'pager' => false,
])
?>
