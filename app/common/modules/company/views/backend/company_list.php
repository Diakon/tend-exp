<?php
/**
 * @var View               $this
 * @var Company            $model
 * @var ActiveDataProvider $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'title',
                        'ogrn',
                        'inn',
                        'kpp',
                        'general_manage',
                        'tariff.title',
                        [
                            'attribute' => 'tariff_plan_start',
                            'format' => 'html',
                            'value' => function ($data) {
                                return !empty($data->tariff_plan_start)
                                    ? Yii::$app->formatter->asDate($data->tariff_plan_start, 'php:d-m-Y')
                                    : '';
                            }
                        ],
                        [
                            'attribute' => 'tariff_plan_end',
                            'format' => 'html',
                            'value' => function ($data) {
                                return !empty($data->tariff_plan_end)
                                    ? Yii::$app->formatter->asDate($data->tariff_plan_end, 'php:d-m-Y')
                                    : '';
                            }
                        ],
                        'status:status',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
