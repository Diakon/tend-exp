<?php
/**
 * @var View               $this
 * @var Company            $model
 * @var ActiveDataProvider $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'title',
                        'ogrn',
                        'inn',
                        'kpp',
                        'general_manage',
                        'status:status',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
