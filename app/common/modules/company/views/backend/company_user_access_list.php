<?php
/**
 * @var View               $this
 * @var CompanyUserAccess  $model
 * @var ActiveDataProvider $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>
<?= $this->render('_filter_company_user_access', ['model' => $model]) ?>
<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'company.title',
                        [
                            'attribute' => 'class_name',
                            'format' => 'html',
                            'value' => function ($data) {
                                return $data->model->title ?? '';
                            }
                        ],
                        [
                            'attribute' => 'type_access',
                            'format' => 'html',
                            'value' => function ($data) {
                                return \common\modules\company\models\backend\CompanyUserAccess::getTypeAccessList($data->typeAccess) ?? '';
                            }
                        ],
                        'user.email',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
