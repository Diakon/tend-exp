<?php
/**
 * @var View               $this
 * @var CompanyUsers       $model
 * @var ActiveDataProvider $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>
<?= $this->render('_filter_company_user', ['model' => $model]) ?>
<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'company.title',
                        'user.email',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
