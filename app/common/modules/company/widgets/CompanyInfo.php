<?php
namespace common\modules\company\widgets;

use common\modules\company\models\frontend\Company;
use common\modules\tender\models\common\Objects;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class CompanyInfo extends Widget {

    /**
     * @var Company $company
     */
    public $company;

    public $comment = false;


    public function run()
    {

        //$objects = $this->company->getObjects()->where(['show_portfolio' => \modules\crud\models\ActiveRecord::STATUS_ACTIVE])->all();

        $countObjects = $this->company->getObjects()->count();
        $statusComplete = $this->company->getObjects()->where(['status_complete_id' => Objects::STATUS_COMPLETE])->count();

        return $this->render('company_info_widget', [
            'company' => $this->company,
            'comment' => $this->comment,
            'countObjects' => $countObjects,
            'statusComplete' => $statusComplete,
            'statusNonComplete' => $countObjects - $statusComplete
        ]);

    }
}
