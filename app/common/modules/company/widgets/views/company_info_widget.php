<?php
/**
 * @var integer $countObjects
 * @var integer $statusComplete
 * @var integer $statusNonComplete
 * @var \common\modules\company\models\frontend\Company $company
 * @var integer $totalCountObjects
 */

$canEdit = isset($company->companyUser) ? $company->companyUser->canEdit() : false;
?>

<div class="company-main">
    <div class="container">
        <div class="company-main__inner">
            <div class="company-main__head">
                <div class="company-main__aside">
                    <div class="company-main__logo">

                        <?php if($company->avatar) { ?>
                            <img src="<?= \common\components\Thumbnail::thumbnailFile($company->avatar->getFile(true)) ;?>">
                        <?php } else {?>

                            <img src="<?= Yii::getAlias('@static') ?>/../content/images/avatar2.svg" alt="" class="_img">
                        <?php  }  ?>

                    </div>
                    <!--                                        <div class="company-main__rating">-->
                    <!--                                            <span class="_value">5.0</span>-->
                    <!--                                        </div>-->
                </div>
                <div class="company-main__main">
                    <h1 class="company-main__title"><?= $company->title ?></h1>
                </div>
            </div>
            <div class="company-main__body">
                <div class="company-main__option">
                    <ul class="company-main__option-list">
                        <li class="company-main__option-item">
                            <div class="_value"><?= $countObjects ?></div>
                            <div class="_label">
                                <?= \helpers\Text::plural($countObjects, ['объект', 'объекта', 'объектов']) ?>
                            </div>
                        </li>
                        <li class="company-main__option-item">
                            <div class="_value"><?= $statusComplete ?></div>
                            <div class="_label"><?= Yii::t('app', 'завершено') ?></div>
                        </li>
                        <li class="company-main__option-item">
                            <div class="_value"><?= $statusNonComplete ?></div>
                            <div class="_label"><?= Yii::t('app', 'строится') ?></div>
                        </li>
                    </ul>
                </div>

                <?php if ($canEdit) { ?>
                    <div class="company-main__action">
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/company_edit', 'url' => $company->url])?>"
                       class="btn btn-outline-secondary"><?= Yii::t('app', 'РЕДАКТИРОВАТЬ ИНФОРМАЦИЮ') ?></a>
                </div>
                <?php } ?>
                <?php if ($comment && !Yii::$app->user->isGuest && !empty($countObjects)) { ?>
                    <div class="company-main__action">
                        <button class="btn btn-primary" data-popup="feedback-step1"><?= Yii::t('app', 'ОЦЕНИТЬ КОМПАНИЮ')?></button>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
