<?php
namespace common\modules\tender;

use common\modules\tender\controllers\TenderController;
use common\modules\tender\controllers\TenderFrontendController;

/**
 * Class Module.
 * @package common\modules\tender
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\tender\controllers';

    public $controllerMap = [
        'tender' => TenderController::class,
        'tenders' => TenderFrontendController::class,
    ];
}
