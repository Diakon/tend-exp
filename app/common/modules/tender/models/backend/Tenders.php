<?php
namespace common\modules\tender\models\backend;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Tenders
 */
class Tenders extends \common\modules\tender\models\common\Tenders
{
    /**
     * @var string
     */
    public $dateStartCalendar;

    /**
     * @var string
     */
    public $dateEndCalendar;

    const DATE_FORMAT_CALENDAR = 'php:d-m-Y';

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['dateStartCalendar', 'dateEndCalendar'], 'required'],
                [['dateStartCalendar', 'dateEndCalendar'], 'date', 'format' => self::DATE_FORMAT_CALENDAR]
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->dateStartCalendar = !empty($this->date_start)  ?
            Yii::$app->formatter->asDate($this->date_start, self::DATE_FORMAT_CALENDAR) :
            '';
        $this->dateEndCalendar = !empty($this->date_end)  ?
            Yii::$app->formatter->asDate($this->date_end, self::DATE_FORMAT_CALENDAR) :
            '';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'dateStartCalendar' => 'Дата начала тендера',
                'dateEndCalendar' => 'Дата окончания тендера'
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->date_start = !empty($this->dateStartCalendar) ? Yii::$app->formatter->asDate($this->dateStartCalendar . ' 00:00:00', self::DATE_FORMAT) : null;
        $this->date_end = !empty($this->dateEndCalendar) ? Yii::$app->formatter->asDate($this->dateEndCalendar . ' 23:59:59', self::DATE_FORMAT) : null;

        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает dataProvider для списка пользователей организации
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['id' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Objects::class,
                    'title' => Yii::t('app', 'Объекты компании'),
                ],
                [
                    'class' => Tenders::class,
                    'title' => Yii::t('app', 'Тендеры компании'),
                ],
                [
                    'class' => EstimatesRubrics::class,
                    'title' => Yii::t('app', 'Сметы тендеров компании'),
                ],
                [
                    'class' => EstimatesElements::class,
                    'title' => Yii::t('app', 'Работы сметы тендера компании'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/tender/tender/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/tender/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/tender/' . $item['class']::classNameShort('id') . '_edit'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Тендеры')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Тендеры'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать тендер') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
