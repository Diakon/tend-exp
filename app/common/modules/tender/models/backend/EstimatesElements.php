<?php
namespace common\modules\tender\models\backend;

use common\modules\company\models\common\Company;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * EstimatesElements
 */
class EstimatesElements extends \common\modules\tender\models\common\EstimatesElements
{

    /**
     * Компания
     *
     * @var integer
     */
    public $filterCompanyId;

    /**
     * Тендер
     *
     * @var integer
     */
    public $filterTenderId;

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['filterCompanyId', 'filterTenderId'], 'integer']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'filterCompanyId' => Yii::t('app', 'Компания'),
            'filterTenderId' => Yii::t('app', 'Тендер'),
        ]);
    }

    /**
     * Возвращает dataProvider для списка телефонов
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['id' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Company::class,
                    'title' => Yii::t('app', 'Работы сметы тендера компании'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_update'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();
        $query->leftJoin(Tenders::tableName() . ' as ' . Tenders::classNameShort('id'), Tenders::classNameShort('id') . '.id=' . self::tableName() . '.tender_id');
        $query->leftJoin(Objects::tableName() . ' as ' . Objects::classNameShort('id'), Objects::classNameShort('id') . '.id=' . Tenders::classNameShort('id') . '.object_id');
        $query->leftJoin(Company::tableName() . ' as ' . Company::classNameShort('id'), Company::classNameShort('id') . '.id=' . Objects::classNameShort('id') . '.company_id');


        $query->andFilterWhere(
            [
                'and',
                ['=', Company::classNameShort('id') . '.id', $this->filterCompanyId],
                ['=', self::tableName() . '.tender_id', $this->filterTenderId],
            ]
        );

        if (!empty($this->companyId)) {
            $query->andFilterWhere(
                [
                    'and',
                    ['=', Company::classNameShort('id') . '.id', $this->companyId],
                ]
            );
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }


    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Работы сметы')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Работы сметы'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать работу') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
