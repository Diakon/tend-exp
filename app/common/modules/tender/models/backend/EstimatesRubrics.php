<?php
namespace common\modules\tender\models\backend;

use common\modules\company\models\common\Company;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * EstimatesRubrics
 */
class EstimatesRubrics extends \common\modules\tender\models\common\EstimatesRubrics
{
    /**
     * ID компании
     *
     * @var integer
     */
    public $companyId;

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['companyId'], 'integer'],
        ];

        return ArrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'companyId' => Yii::t('app', 'Компания'),
        ]);
    }

    /**
     * Возвращает dataProvider для списка смет тендера организации
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->where(['>', 'depth', 0]);
        $query->orderBy(['lft' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Company::class,
                    'title' => Yii::t('app', 'Сметы тендеров компаний'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/company/' . $item['class']::classNameShort('id') . '_update'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();
        $query->leftJoin(Tenders::tableName() . ' as ' . Tenders::classNameShort('id'), Tenders::classNameShort('id') . '.id=' . self::tableName() . '.tender_id');
        $query->leftJoin(Objects::tableName() . ' as ' . Objects::classNameShort('id'), Objects::classNameShort('id') . '.id=' . Tenders::classNameShort('id') . '.object_id');
        $query->leftJoin(Company::tableName() . ' as ' . Company::classNameShort('id'), Company::classNameShort('id') . '.id=' . Objects::classNameShort('id') . '.company_id');
        $query->where(['>', self::tableName().'.depth', 0]);

        if (!empty($this->companyId)) {
            $query->andFilterWhere(
                [
                    'and',
                    ['=', Company::classNameShort('id') . '.id', $this->companyId],
                ]
            );
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [self::tableName().'.lft' => SORT_ASC],
                'attributes' => [ self::tableName().'.lft'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * Возвращает массив дерева для выпадающего списка отформатированый в человеко-понятном виде иерархии
     *
     * @return array
     */
    public static function getTreeArray()
    {
        $model = new self();
        $rubrics = $model::getChildrenTree();
        $rubricsItems = [];
        foreach ($rubrics->asArray()->all() as $rubric) {
            $rubricsItems[$rubric['id']] = str_repeat('   ', $rubric['depth']) . $rubric['title'];
        }

        return $rubricsItems;
    }


    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Сметы тендеров')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Сметы тендеров'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать смету') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
