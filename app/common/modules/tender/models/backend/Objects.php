<?php
namespace common\modules\tender\models\backend;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Objects
 */
class Objects extends \common\modules\tender\models\common\Objects
{

    /**
     * @var array
     */
    public static $list = [];

    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * @var string
     */
    public $dateStartCalendar;

    /**
     * @var string
     */
    public $dateEndCalendar;

    const DATE_FORMAT = 'php:Y-m-d H:i:s';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['dateStartCalendar', 'dateEndCalendar'], 'required'],
                [['dateStartCalendar', 'dateEndCalendar'], 'date', 'format' => self::DATE_FORMAT]
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->dateStartCalendar = !empty($this->date_start)  ?
            Yii::$app->formatter->asDate($this->date_start, 'Y-m-d') :
            '';
        $this->dateEndCalendar = !empty($this->date_end)  ?
            Yii::$app->formatter->asDate($this->date_end, 'Y-m-d') :
            '';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'dateStartCalendar' => 'Дата начала реализации',
                'dateEndCalendar' => 'Дата окончания реализации'
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->dateStartCalendar = !empty($this->dateStartCalendar) ? (date('Y-m-d', strtotime($this->dateStartCalendar)) . ' 00:00:00') : null;
        $this->dateEndCalendar = !empty($this->dateEndCalendar) ? (date('Y-m-d', strtotime($this->dateEndCalendar)) . ' 23:59:59') : null;
        $this->date_start = strtotime($this->dateStartCalendar);
        $this->date_end = strtotime($this->dateEndCalendar);

        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает dataProvider для списка пользователей организации
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['id' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Возвращает список меню компании.
     *
     * @return array[]
     */
    private static function getList()
    {
        if (empty($list)) {
            self::$list = [
                [
                    'class' => Objects::class,
                    'title' => Yii::t('app', 'Объекты компании'),
                ],
            ];
        }

        return self::$list;
    }

    /**
     * Возвращает список пунктов меню.
     *
     * @return array[]
     */
    public static function getMenu()
    {
        $menu = [];
        foreach (self::getList() as $item) {
            $menu[] = [
                'label' => $item['title'],
                'url' => ['/tender/tender/' . $item['class']::classNameShort('id') . '_list'],
                'visible' => Yii::$app->user->can('administrator'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/tender/' . $item['class']::classNameShort('id') . '_create'],
                        'visible' => false,
                    ],
                    [
                        'label' => Yii::t('app', 'Company'),
                        'url' => ['/tender/tender/' . $item['class']::classNameShort('id') . '_update'],
                        'visible' => false,
                    ],
                ],
            ];
        }

        return $menu;
    }

    /**
     * @inheritdoc
     */
    public function search()
    {
        $this->pageSize = Yii::$app->request->get('pageSize', $this->pageSize);

        $query = $this::find();

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [ 'id'],
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => 'pageSize',
            ],
        ]);
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Объекты')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Объекты'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать объект') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
