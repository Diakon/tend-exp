<?php
namespace common\modules\tender\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use common\models\UserIdentity;
use common\modules\company\models\backend\CompanyAddress;
use common\modules\company\models\backend\Company;
use common\modules\directories\models\common\Activities;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\directories\models\common\Projects;
use common\modules\directories\models\common\Works;
use yii\helpers\ArrayHelper;

/**
 * Конфигурация формы списка объектов компаний
 *
 * Class CompanyUsers
 *
 * @package common\modules\tender\models\backend\forms
 */
class Objects extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $data = [];
        // Основные
        $data[] = '<h2>Объекты компании ' . $this->owner->isNewRecord ? '' : $this->owner->company->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['company_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => Company::getCompanyList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['user_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(UserIdentity::find()->asArray()->all(), 'id', 'email'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['address_id'] = array(
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => CompanyAddress::getCompanyAddressList(),
            'attributes' => array(
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ),
        );
        $data['title'] = ['type' => self::TYPE_TEXT];
        $data['activitiesIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(Activities::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'multiple' => 'multiple',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['functionsIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(FunctionsCompany::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'multiple' => 'multiple',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['typesIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(Projects::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'multiple' => 'multiple',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['worksIds'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(Works::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'multiple' => 'multiple',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['dateStartCalendar'] = [
            'type' => 'text',
            'attributes' => [
                'class' => 'datepicker',
                'value' => $this->owner->dateStartCalendar,
            ]
        ];
        $data['dateEndCalendar'] = [
            'type' => 'text',
            'attributes' => [
                'class' => 'datepicker',
                'value' => $this->owner->dateEndCalendar,
            ]
        ];
        $data['show_portfolio'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data['status'] = ['type' => self::TYPE_CHECKBOX,
            'attributes' => [
                'class' => 'i-checks',
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';


        return $data;
    }

}
