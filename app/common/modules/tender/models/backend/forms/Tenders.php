<?php
namespace common\modules\tender\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use modules\upload\widgets\UploadStaticWidget;
use common\models\UserIdentity;
use common\modules\company\models\backend\Company;
use common\modules\directories\models\common\Currency;
use yii\helpers\ArrayHelper;


/**
 * Конфигурация формы списка тендеров компаний
 *
 * Class Tenders
 *
 * @package common\modules\tender\models\backend\forms
 */
class Tenders extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $data = [];
        // Основные
        $data[] = '<h2>Тендеры компании ' . $this->owner->isNewRecord ? '' : $this->owner->company->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['object_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(\common\modules\tender\models\backend\Objects::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['company_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => Company::getCompanyList(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['user_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(UserIdentity::find()->asArray()->all(), 'id', 'email'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['currency_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(Currency::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['offer_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(\common\modules\offers\models\backend\Offers::find()->asArray()->all(), 'id', 'id'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['dateStartCalendar'] = [
            'type' => 'text',
            'attributes' => [
                'class' => 'datepicker',
                'value' => $this->owner->dateStartCalendar,
            ]
        ];
        $data['dateEndCalendar'] = [
            'type' => 'text',
            'attributes' => [
                'class' => 'datepicker',
                'value' => $this->owner->dateEndCalendar,
            ]
        ];
        $data['title'] = ['type' => self::TYPE_TEXT];
        $data['type'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tender\models\backend\Tenders::getTypes(),
            'attributes' => [
                'class' => 'select_one form-control',
            ],
        ];
        $data['mode'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tender\models\backend\Tenders::getModes(),
            'attributes' => [
                'class' => 'select_one form-control',
            ],
        ];
        $data['stage'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tender\models\backend\Tenders::getStages(),
            'attributes' => [
                'class' => 'select_one form-control',
            ],
        ];
        $data[] = '<b>Документ</b>';
        $data[] = UploadStaticWidget::widget([
            'model' => $this->owner,
            'attribute' => 'documents',
        ]);
        $data['status'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => \common\modules\tender\models\backend\Tenders::getStatuses(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data[] = '</fieldset>';
        $data[] = '</div>';


        return $data;
    }

}
