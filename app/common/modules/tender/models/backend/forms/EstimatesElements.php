<?php
namespace common\modules\tender\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use yii\helpers\ArrayHelper;
use common\modules\tender\models\backend\EstimatesRubrics;

/**
 * Конфигурация формы списка работ сметы тендеров компаний
 *
 * Class EstimatesElements
 *
 * @package common\modules\tender\models\backend\forms
 */
class EstimatesElements extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Работа сметы ' . $model->isNewRecord ? '' : $model->rubric->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['title'] = ['type' => self::TYPE_TEXT];
        $data[] = 'Укажите рубрику или тендер, если работа не относится какой либо категории тендера';
        $data['rubric_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => $this->getTreeArray(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['tender_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' =>  ArrayHelper::map(\common\modules\tender\models\backend\Tenders::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['unit_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' =>  \common\modules\tender\models\backend\EstimatesElements::getUnits(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];

        $data['count'] = ['type' => self::TYPE_TEXT];
        $data[] = '</fieldset>';
        $data[] = '</div>';

        return $data;
    }

    /**
     * Возвращает массив дерева для выпадающего списка отформатированый в человеко-понятном виде иерархии
     *
     * @return array
     */
    private function getTreeArray()
    {
        $model = new EstimatesRubrics();
        $rubrics = $model::getChildrenTree();
        $rubricsItems = [];
        foreach ($rubrics->asArray()->all() as $rubric) {
            $rubricsItems[$rubric['id']] = str_repeat('   ', $rubric['depth']) . $rubric['title'];
        }

        return $rubricsItems;
    }
}
