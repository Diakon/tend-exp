<?php
namespace common\modules\tender\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use yii\helpers\ArrayHelper;

/**
 * Конфигурация формы списка смет тендеров компаний
 *
 * Class CompanyTenderEstimatesRubrics
 *
 * @package common\modules\tender\models\backend\forms
 */
class EstimatesRubrics extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        $model = $this->owner;

        $data = [];
        // Основные
        $data[] = '<h2>Сметы тендера ' . $model->isNewRecord ? '' : $model->tender->title . '</h2>';
        $data[] = '<div class="panel-body">';
        $data['parent_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => $model::getTreeArray(),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '/'),
            ],
        ];
        $data['tender_id'] = [
            'type' => self::TYPE_DROP_DOWN_LIST,
            'items' => ArrayHelper::map(\common\modules\tender\models\backend\Tenders::find()->asArray()->all(), 'id', 'title'),
            'attributes' => [
                'class' => 'select_one form-control',
                'prompt' => \Yii::t('app', '-----'),
            ],
        ];
        $data['title'] = ['type' => self::TYPE_TEXT];
        $data[] = '</fieldset>';
        $data[] = '</div>';

        return $data;
    }
}
