<?php
namespace common\modules\tender\models\frontend;

use Yii;

/**
 * Тендеры, добавленые в избранное пользователем.
 */
class UserFavoritesTenders extends \common\modules\tender\models\common\UserFavoritesTenders
{
    /**
     * Возврат тендеров, которые выбраны избраными текущим, авторизованым, пользователем
     *
     * @return object
     */
    public function getFavoritesTenders()
    {
        return self::find()->where(['user_id' => Yii::$app->user->id ?? 0]);
    }

}