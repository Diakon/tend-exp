<?php
namespace common\modules\tender\models\frontend;

use modules\crud\behaviors\FieldFormatBehavior;
use modules\crud\models\ActiveRecord;
use common\components\ClassMap;
use common\models\EntityFile;
use common\modules\company\models\common\CompanyAddress;
use common\modules\company\models\frontend\CompanyUserAccess;
use common\modules\company\models\frontend\CompanyUsers;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\common\Objects as CommonCompanyObjects;
use common\modules\tender\models\common\ObjectsCompletedWorks;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtml;
use frontend\modules\upload\components\FileUploadInterface;
use frontend\components\CheckTHashTrait;
use yii\helpers\Url;

/**
 * Проекты компании
 *
 * @property string  $tHash   Временный идентификатора модели
 */
class Objects extends CommonCompanyObjects  implements FileUploadInterface
{

    // Все объекты (в фортфолио и нет)
    const ALL_OBJECTS = 5;

    const MODEL_ATTRIBUTE_FILES = 'files';

    /**
     * Адрес проекта - страна
     * @var integer
     */
    public $addressCountryId;

    /**
     * Адрес проекта - регион
     * @var integer
     */
    public $addressRegionId;

    /**
     * Адрес проекта - город
     * @var integer
     */
    public $addressCityId;

    /**
     * Адрес проекта - улица
     * @var string
     */
    public $addressStreet;

    /**
     * Адрес проекта - дом
     * @var string
     */
    public $addressHouse;

    /**
     * Адрес проекта - корпус
     * @var string
     */
    public $addressStructure;

    /**
     * Адрес проекта - индекс
     * @var string
     */
    public $addressIndex;

    /**
     * Дата начала проекта - календарь
     * @var string
     */
    public $dateStartCalendar;

    /**
     * Дата окончания проекта - календарь
     * @var string
     */
    public $dateEndCalendar;

    /**
     * ID выполненных работ
     *
     * @var array
     */
    public $completedWorkWorkIds;

    /**
     * ID Кол-ва выполненных работ
     *
     * @var array
     */
    public $completedWorkCount;

    /**
     * Ссылки объекта
     *
     * @var array
     */
    public $videoLinks;

    /**
     * ID единиц измерения выполненных работ
     *
     * @var array
     */
    public $completedWorkUnitIds;

    const DATE_FORMAT_SAVE = 'php:Y-m-d H:i:s';
    const DATE_FORMAT_VIEW = 'php:d.m.Y';

    const SCENARIO_CREATE_OBJECT = 'create-object';

    const SCENARIO_CREATE_OBJECT_IN_TENDER = 'create-object-in-tender';

    use CheckTHashTrait;

    /**
     * Событие после коммита в RequestMainAction
     *
     * @see \frontend\modules\request\controllers\actions\RequestMainAction::run
     */
    const EVENT_AFTER_MAIN_ACTION_COMMIT = 'event_after_main_action_commit';

    /**
     * @var string $tHash Свойство для хранения временного идентификатора модели
     */
    private $tHash;

    /**
     * @param string $tHash
     */
    public function setTHash(string $tHash)
    {
        $this->tHash = $tHash;
    }

    /**
     * @return string
     */
    public function getTHash(): string
    {
        if (!empty($this->tHash)) {
            return $this->tHash;
        }

        $this->setTHash($this->isNewRecord ? uniqid('thash_' . substr(md5(static::class), 0, 6) . '_') : (string)$this->id);

        return $this->tHash;
    }

    /**
     * Формируем ссылку на страницу формы создания
     *
     * @param $companyUrl
     *
     * @return string
     */
    public function getCreateLink($companyUrl)
    {
        return Url::to(['/dashboard/dashboard/object_add',  'companyUrl' => $companyUrl, 'tHash' => $this->getTHash()]);
    }



    /**
     * Получаем ajax путь удаления файла
     *
     * @return string
     */
    public function getAjaxDeleteFileUrl(): string
    {
        return Url::to(['/upload/base/delete']);
    }

    /**
     * Получаем ajax путь загрузки файла
     *
     * @return string
     */
    public function getAjaxUploadFileUrl(): string
    {
        return Url::to(['/upload/base/upload']);
    }

    /**
     * Подготовка модели к редактированию
     *
     * @param $tHash
     *
     * @throws \Exception
     */
    public function prepareModelAction($tHash)
    {
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'processTHashEntityFile'], ['tHash' => $tHash]);
        $this->on(self::EVENT_AFTER_MAIN_ACTION_COMMIT, [$this, 'flushTHash'], ['tHash' => $tHash]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['title', 'dateStartCalendar', 'dateEndCalendar', 'addressCountryId', 'addressCityId', 'addressStreet'], 'required'],
                [['addressCountryId', 'addressRegionId', 'addressCityId', 'status_complete_id', 'employer_id'], 'integer'],
                ['employer_id', 'requiredEmployee'],
                [['addressStreet', 'addressHouse', 'employerName'], 'string', 'max' => 255],
                [['addressStructure', 'addressIndex'], 'string', 'max' => 20],
                [['date_start', 'date_end'], 'date', 'format' => 'd.m.Y'],
                [['dateStartCalendar', 'dateEndCalendar'], 'date', 'format' => self::DATE_FORMAT_SAVE],
                [['completedWorkWorkIds', 'completedWorkCount', 'completedWorkUnitIds'], 'each', 'rule' => ['integer']],
                [['videoLinks'], 'each', 'rule' => ['string']],
                [['date_end'], 'statusEndValidate', 'skipOnEmpty' => false],
            ]
        );
    }

    /**
     * Статусы портфолио
     *
     * @param null $id
     *
     * @return array
     */
    public static function portfolioStatusList($id = null)
    {
        $list = [
            self::STATUS_INACTIVE => ['id' => self::STATUS_INACTIVE, 'title' => Yii::t('app', 'Не в портфолио')],
            self::STATUS_ACTIVE => ['id' => self::STATUS_ACTIVE, 'title' => Yii::t('app', 'Портфолио')],
            self::ALL_OBJECTS => ['id' => self::ALL_OBJECTS, 'title' => Yii::t('app', 'Все')],

        ];

        return isset($id) ? ($list[$id]['title'] ?? $list[self::ALL_OBJECTS]['title']) : $list;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_CREATE_OBJECT => ['completedWorkWorkIds', 'employer_id', 'employerName', 'completedWorkCount', 'completedWorkUnitIds', 'addressIndex',
                'addressStructure', 'description', 'show_portfolio', 'status', 'status_complete_id', 'activitiesIds', 'typesIds', 'videoLinks',
                'functionsIds', 'worksIds', 'title', 'date_start', 'date_end', 'addressCountryId', 'addressCityId', 'addressStreet', 'addressHouse'],
            self::SCENARIO_CREATE_OBJECT_IN_TENDER => ['title', 'date_start', 'date_end', 'status', 'status_complete_id', 'company_id', 'user_id'],

        ]);
    }


    /**
     * Если пустой кастомный заказчик до добавляем ошибку
     *
     * @param $attribute
     *
     * @return bool
     */
    public function requiredEmployee($attribute)
    {
        if (empty($this->employerName) && empty($this->{$attribute})) {
            $this->addError($attribute, Yii::t('app', 'Выберите заказчика!'));

            return false;
        }

        return true;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'fieldFormat' => [
                    'class' => FieldFormatBehavior::class,
                    'dateFormat' => self::DATE_FORMAT_VIEW,
                    'dateFields' => ['date_start', 'date_end'],
                ],
                'timestamp' => [
                    'class' => TimestampBehavior::class,
                    'createdAtAttribute' => 'created_at',
                    'updatedAtAttribute' => 'updated_at',
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                    'value' => new Expression('NOW()'),
                ],
            ]
        );
    }

    /**
     * Получает актуальный и физический адреса компании и назначает значения в параметры модели
     */
    public function setAddressParam()
    {
        $addressCompanyFields = [
            'addressCountryId' => 'country_id',
            'addressRegionId' => 'region_id',
            'addressCityId' => 'city_id',
            'addressStreet' => 'street',
            'addressHouse' => 'house',
            'addressStructure' => 'structure',
            'addressIndex' => 'index'
        ];
        $address = $this->getAddress()->one();
        if (!empty($address)) {
            foreach ($addressCompanyFields as $companyField => $addressField) {
                $this->{$companyField} = $address->{$addressField};
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'addressCountryId' => Yii::t('app', 'Страна'),
                'addressRegionId' => Yii::t('app', 'Регион'),
                'addressCityId' => Yii::t('app', 'Город'),
                'addressStreet' => Yii::t('app', 'Улица'),
                'addressHouse' => Yii::t('app', 'Дом'),
                'addressStructure' => Yii::t('app', 'Корпус / строение'),
                'legalAddressIndex' => Yii::t('app', 'Индекс'),
                'dateStartCalendar' => Yii::t('app', 'Дата начала реализации'),
                'dateEndCalendar' => Yii::t('app', 'Дата окончания реализации'),
                'completedWorkWorkIds' => Yii::t('app', 'Выполненные работы'),
                'completedWorkCount' => Yii::t('app', 'Количество'),
                'completedWorkUnitIds' => Yii::t('app', 'Ед. измерения'),
            ]
        );
    }


    /**
     * Возвращает сисок проектов к которым есть доступ у пользователя
     *
     * @param CompanyUsers $companyUser Модель пользователя CompanyUsers
     * @param array        $params
     * @return ActiveDataProvider
     */
    public function getUserProjects($companyUser, array $params = [])
    {

    }

    /**
     * @inheritdoc
     */
    public function getViewLink($action = 'object_edit', array $params = [])
    {
        return parent::getViewLink($action, $params);
    }

    /**
     * Обновляет данные о юредическом и фактическом адресах
     */
    private function updateAddress()
    {
        $transaction = CompanyAddress::getDb()->beginTransaction();
        try {
            // Фактический аддресс
            $modelAddress = empty($this->address) ? new CompanyAddress() : $this->address;
            $modelAddress->country_id = $this->addressCountryId;
            $modelAddress->region_id = $this->addressRegionId;
            $modelAddress->city_id = $this->addressCityId;
            $modelAddress->street = $this->addressStreet;
            $modelAddress->house = $this->addressHouse;
            $modelAddress->structure = $this->addressStructure;
            $modelAddress->index = $this->addressIndex;
            if (!$modelAddress->save()) {
                throw new \yii\web\HttpException(500, BaseHtml::errorSummary($modelAddress));
            }
            $this->address_id = $modelAddress->id;

            $transaction->commit();
        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при сохранении адреса для объекта ID = ' . $this->id . '. Сообщение валидатора: ' . $e->getMessage());
        }
    }

    /**
     * Обновляет записи о выполненных работах для объекта
     */
    private function updateCompletedWorks()
    {
        if (!empty($this->completedWorkWorkIds)) {
            $transaction = ObjectsCompletedWorks::getDb()->beginTransaction();
            try {
                ObjectsCompletedWorks::deleteAll(['object_id' => $this->id]);
                foreach ($this->completedWorkWorkIds as $key => $workId) {

                    if ($this->completedWorkCount[$key] > 0) {
                        $model = new ObjectsCompletedWorks();
                        $model->object_id = $this->id;
                        $model->work_id = $workId;
                        $model->count = (int)$this->completedWorkCount[$key];
                        $model->unit_id = (int)$this->completedWorkUnitIds[$key];
                        if (!$model->save()) {
                            throw new \yii\web\HttpException(500, BaseHtml::errorSummary($model));
                        }
                    }
                }
                $transaction->commit();
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::error('Ошибка при сохранении выполненных работ для компании ID = ' . $this->id . '. Сообщение валидатора: ' . $e->getTraceAsString());
            }
        }
    }

    /**
     * Обновляет дочерние записи информации о проекте
     */
    public function updateObjectChildRecords()
    {
        // Обновляем адреса
        $this->updateAddress();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        // Обновляем выполненные работы
        $this->updateCompletedWorks();
        // Обновляем ссылки на видео
        $this->updateVideoLinks();
        // Обновляем счетчик количества объектов в окмпании (для фильтра)
        $this->setObjectCount();
    }

    /**
     * Сохраняет ссылки на видео для объекта
     */
    private function updateVideoLinks()
    {
        if (isset($this->videoLinks['param-links-input'])) {
            unset($this->videoLinks['param-links-input']);
        }
        if (!empty($this->videoLinks)) {
            ObjectsLinks::deleteAll(['object_id' => $this->id]);
            foreach ($this->videoLinks as $link) {
                $model = new ObjectsLinks();
                $model->object_id = $this->id;
                $model->link = $link;
                $model->save();
            }
        }
    }

    /**
     * бновляет счетчик количества объектов в окмпании
     */
    private function setObjectCount()
    {
        $company = $this->company;
        $company->count_objects = $company->getObjects()->count();
        $company->save(false);
    }

    /**
     * Возвращает проекты компании к которым есть доступ у пользователя
     *
     * @param CompanyUsers $companyUser
     * @param null|integer $typeAccess
     * @return \modules\crud\models\ActiveQuery|\yii\db\ActiveQuery
     */
    public static function getObjectsUser($companyUser, $typeAccess = null)
    {
        $query = self::find();
        // Если пользователь не админ компании - вернуть только те проекты, к которым был дан ему доступ
        if ($companyUser->type_role != CompanyUsers::TYPE_ROLE_ADMIN) {
            $objectsAccess = CompanyUserAccess::find()
                ->where(['user_id' => $companyUser->user_id])
                ->andWhere(['class_name' => ClassMap::getId(Objects::class)]);
            if (!empty($typeAccess)) {
                $objectsAccess->andWhere(['type_access' => $typeAccess]);
            }
            $userAccessProjects = ArrayHelper::map($objectsAccess->asArray()->all(), 'class_id', 'class_id');
            $userAccessProjects = !empty($userAccessProjects) ? $userAccessProjects : [0];
            $query->where(['in', 'id', $userAccessProjects]);
        }

        return $query;
    }


    /**
     * Возвращает проекты компании к которым есть доступ у пользователя
     *
     * @param CompanyUsers $companyUser
     * @param null|integer $typeAccess
     * @return \modules\crud\models\ActiveQuery|\yii\db\ActiveQuery
     */
    public static function getObjectsUserEmployee($companyUser, $typeAccess = null)
    {
        $query = self::find();
        // Если пользователь не админ компании - вернуть только те проекты, к которым был дан ему доступ
        //if ($companyUser->type_role != CompanyUsers::TYPE_ROLE_ADMIN) {
            $objectsAccess = CompanyUserAccess::find()
                ->where(['user_id' => $companyUser->user_id])
                ->andWhere(['class_name' => ClassMap::getId(Objects::class)]);
            if (!empty($typeAccess)) {
                $objectsAccess->andWhere(['type_access' => $typeAccess]);
            }
            $userAccessProjects = ArrayHelper::map($objectsAccess->asArray()->all(), 'class_id', 'class_id');
            $userAccessProjects = !empty($userAccessProjects) ? $userAccessProjects : [0];
            // Выбираем объекты заказчика только те,  в тендерах которых мы победили.
            $query->leftJoin(Tenders::tableName(), Objects::tableName().'.id = {{%tenders}}.object_id')->
            leftJoin(Offers::tableName(), Tenders::tableName().'.id = {{%offers}}.tender_id');
            $query->where(['in', Objects::tableName().'.id', $userAccessProjects]);
            $query->andWhere(Offers::tableName().'.company_id = :company_id AND ' . Offers::tableName() . '.status=:status ',
                [':company_id' => $companyUser->companyId, ':status' => Offers::STATUS_APPROVED_YES ]);

        //}

        return $query;
    }

    /**
     * Получаем реляцию модели
     * @return ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getFiles()
    {
        return $this->hasMany(EntityFile::class, ['modelId' => 'thash'])->onCondition(['modelName' => ClassMap::getId(self::class), 'modelAttribute' => 'files']);
    }

    /**
     * Возвращает активные проекты
     *
     * @return \modules\crud\models\ActiveQuery|\yii\db\ActiveQuery
     */
    public static function getActive()
    {
        $query = self::find();
        $query->where(['status' => self::STATUS_ACTIVE]);

        return $query;
    }
}
