<?php
namespace common\modules\tender\models\frontend;

use common\modules\offers\models\common\OffersElements;
use common\modules\tender\models\common\EstimatesElements as CommonCompanyTenderEstimatesElements;
use Yii;
use yii\helpers\BaseHtml;
use yii\helpers\Json;

/**
 * Работы смет тендера компании
 */
class EstimatesElements extends CommonCompanyTenderEstimatesElements
{
    const DEFAULT_NAME = 'Новая работа';

    /**
     * Сохраняет запись, если ошибка сохранения - пишет в лог и откатывает изменения
     *
     * @return bool
     */
    public function saveWork()
    {
        $transaction = self::getDb()->beginTransaction();
        try {
            if ($this->save()) {
                $transaction->commit();
                return true;
            }
        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при создании новой работы сметы. Сообщение валидатора: ' .
                $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderEstimates');

            return false;
        }

        return false;
    }
}
