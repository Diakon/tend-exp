<?php

namespace common\modules\tender\models\frontend;

use common\components\ClassMap;
use common\modules\company\models\common\CompanyAddress;
use common\modules\company\models\common\CompanyCharsBuilds;
use common\modules\company\models\common\CompanyCharsFunctions;
use common\modules\company\models\common\CompanyCharsWorks;
use common\modules\company\models\frontend\Company;
use common\modules\company\models\frontend\CompanyUsers;
use common\modules\directories\models\common\Directories;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\directories\models\common\MainWork;
use common\modules\directories\models\common\Projects;
use common\modules\directories\models\common\TypesWork;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\common\TendersCharsWorks;
use common\modules\tender\models\frontend\Objects;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\sphinx\Query as SphinxQuery;
use yii\sphinx\MatchExpression;

/**
 * Class SearchFilterTrait.
 *
 * @package frontend\models
 */
trait SearchFilterTrait
{
    /**
     * @var CompanyAddress $addressFilter Для формы поиска компаний
     */
    public $addressFilter;

    /**
     * @var array $functionsFilter функции компании
     */
    public $functionsFilter;

    /**
     * @var array $typeWorksFilter Группы работ
     */
    public $typeWorksFilter;

    /**
     * @var array $builds направления строительства
     */
    public $buildsFilter;

    /**
     * @var array $builds Направления работ
     */
    public $worksFilter;

    /**
     * @var array $typeObjectsFilter Типы проектов
     */
    public $typeObjectsFilter;

    /**
     * @var array $typeWorkFilter Группы работ
     */
    public $typeWorkFilter;

    /**
     * @var array
     */
    public $columns;

    /**
     * @var integer $inn ИНН
     */
    public $inn;

    /**
     * @var array $objectTypeFilter Типы объектов
     */
    public $objectTypeFilter;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->columns = [
            'date_start' => [
                'attribute' => 'date_start',
            ],
        ];

        parent::init();
    }

    /**
     * @var string дата фильтра "с"
     */
    public $dateFilterStart;

    /**
     * @var string дата фильтра "по"
     */
    public $dateFilterEnd;


    /**
     * Возвращает список тендеров
     *
     * @param Request $request               Параметры фильтрации.
     * @param boolean $onlyQuery             Возвратить query или ActiveDataProvider
     * @param integer $companyId             Фильтровать по компании
     * @param null|string $typeTenderList    Тип списка тендеров
     * @param null|CompanyUsers $companyUser Пользователь
     *
     * @return ActiveDataProvider| ActiveQuery
     */
    public function searchItems($request, $onlyQuery = false, $companyId = null, $typeTenderList = null, $companyUser = null)
    {
        $this->scenario = self::SCENARIO_FRONTEND_FILTER;

        /** @var \yii\db\ActiveQuery $query */
        $query = static::find();
        $query->joinWith(['object']); //  связь с объектами
        $query->joinWith(['company']); //  связь с компаниями
        $query->joinWith(['object.address']); //  связь с адресом объекта
        //$query->joinWith(['object.activities']); //  связь с направлениями строительства объекта

        //  связь с направлениями строительства объекта
        $query->leftJoin('tender_objects_activities', Objects::tableName() . '.id=tender_objects_activities.object_id');
        $query->leftJoin(Directories::tableName() . ' `activities` ', 'tender_objects_activities.activity_id=' . 'activities.id');

        //  связь с функциями
        $query->leftJoin(CompanyCharsFunctions::tableName(), Company::tableName() . '.id=' . CompanyCharsFunctions::tableName() . '.company_id');
        $query->leftJoin(Directories::tableName() . ' `functions_company` ', CompanyCharsFunctions::tableName() . '.function_id=' . 'functions_company.id');
        // видами работ
        $query->leftJoin(CompanyCharsWorks::tableName(), Company::tableName() . '.id=' . CompanyCharsWorks::tableName() . '.company_id');
        $query->leftJoin(Directories::tableName() . ' `types_work` ', CompanyCharsWorks::tableName() . '.type_work_id=' . 'types_work.id');
        // направления работ
        $query->leftJoin(CompanyCharsBuilds::tableName(), Company::tableName() . '.id=' . CompanyCharsBuilds::tableName() . '.company_id');
        $query->leftJoin(Directories::tableName() . ' `main_work` ', CompanyCharsBuilds::tableName() . '.main_work_id=' . 'main_work.id');
        // работы тендера
        $query->leftJoin(TendersCharsWorks::tableName(), self::tableName() . '.id=' . TendersCharsWorks::tableName() . '.tender_id');

        /*
        $query->joinWith(['company.functionsCompany']); //  связь с функциями
        $query->joinWith(['company.typeWorks']); // видами работ
        $query->joinWith(['company.typeBuilds']); // направления строительства
        */
        $query->joinWith(['object.types']); // типы проектов
        $this->getFilterValues();

        // Адрес
        $this->addressFilter = new CompanyAddress();
        $this->addressFilter->load($request->get());
        $this->addressFilter->validate();
        // Загружаем функции компании
        $this->functionsFilter = $request->get(self::classNameShort())['functionsFilter'] ?? [];
        // Загружаем Группы работ
        $this->typeWorksFilter = $request->get(self::classNameShort())['typeWorksFilter'] ?? [];
        // направление строительства
        $this->buildsFilter = $request->get(self::classNameShort())['buildsFilter'] ?? [];
        // Направления работ
        $this->worksFilter = $request->get(self::classNameShort())['worksFilter'] ?? [];
        // Типы объектов
        $this->objectTypeFilter = $request->get(self::classNameShort())['objectTypeFilter'] ?? [];
        // Группы робот
        $this->typeWorkFilter = $request->get(self::classNameShort())['typeWorkFilter'] ?? [];


        switch ($typeTenderList) {
            // Список тендеров в которых участвует моя компания
            case 'partners':
                // Получаю тендеры, в которых участвует компания
                $tendersIds = ArrayHelper::map(Offers::find()->where(['company_id' => $companyId])->groupBy(['tender_id'])->asArray()->all(), 'tender_id', 'tender_id');
                $query->andWhere([
                    'and',
                    ['in', Tenders::tableName() . '.id', $tendersIds],
                ]);
                $query->andWhere(['in', self::tableName() . '.status', [self::STATUS_ACTIVE]]);
                break;
            // Список моих избранных тендеров
            case 'favorites':
                $query->joinWith(['favorites']); // избранные тендеры
                $query->andFilterWhere([
                    'and',
                    ['=', UserFavoritesTenders::tableName() . '.user_id', Yii::$app->user->id],
                ]);
                $query->andWhere(['in', self::tableName() . '.status', [self::STATUS_ACTIVE]]);
                break;
            // Список тендеров в которыя учатвовал, но они уже завершились
            case 'completed':
                $tendersIds = ArrayHelper::map(Offers::find()->where(['company_id' => $companyId])->groupBy(['tender_id'])->asArray()->all(), 'tender_id', 'tender_id');
                $query->andWhere([
                    'and',
                    ['in', Tenders::tableName() . '.id', $tendersIds],
                ]);
                $query->andWhere(['in', self::tableName() . '.status', [self::STATUS_CLOSE]]);
                break;
            // Список моих тендеров (моей клмпании)
            case 'myTenders':
                $query->andFilterWhere([
                    'and',
                    ['=', Objects::tableName() . '.company_id', $companyId],
                ]);
                if (!empty($companyUser) && $companyUser->type_role != CompanyUsers::TYPE_ROLE_ADMIN) {
                    // Пользователь не админ, поэтому возвращаю только те тендеры, к объектам которых был предоставлен доступ админом компании
                    $objectsIds = ArrayHelper::map(
                        Objects::getObjectsUser($companyUser)->andWhere('employerName IS NOT NULL OR employerName !=""')->andWhere([Objects::tableName() . '.company_id' => $companyId])->asArray()->all(),
                        'id', 'id');
                    $query->andWhere(['in', Objects::tableName() . '.id', $objectsIds]);
                }

                break;
            // Список тендеров на сайте
            case 'tendersList':
                $query->andWhere(['in', self::tableName() . '.status', [self::STATUS_ACTIVE]]);

                break;
        }


        if ($this->load($request->get())) {
            $companyIdsFromSphinx = $this->searchBySphinxCompany($request);
            $tendersIdsFromSphinx = $this->searchBySphinxTenders($request);

            $dateStart = !empty($this->dateFilterStart)
                ? Yii::$app->formatter->asDate($this->dateFilterStart, 'php:Y-m-d') . ' 00:00:00'
                : $this->dateFilterStart;
            $dateEnd = !empty($this->dateFilterEnd)
                ? Yii::$app->formatter->asDate($this->dateFilterEnd, 'php:Y-m-d') . ' 23:59:59'
                : $this->dateFilterEnd;

            $query->andFilterWhere([
                'and',
                ['=', CompanyAddress::tableName() . '.city_id', $this->addressFilter->cityId > 0 ? intval($this->addressFilter->cityId) : null],
                ['=', CompanyAddress::tableName() . '.region_id', $this->addressFilter->regionId > 0 ? intval($this->addressFilter->regionId) : null],
                ['=', CompanyAddress::tableName() . '.country_id', $this->addressFilter->countryId > 0 ? intval($this->addressFilter->countryId) : null],
                ['in', 'functions_company.id', $this->functionsFilter],
                ['in', 'tender_objects_activities.activity_id', $this->buildsFilter],

                ['in', TendersCharsWorks::tableName() . '.main_work_id', $this->typeWorkFilter],
                ['in', TendersCharsWorks::tableName() . '.works_id', $this->worksFilter],
                ['<=', Tenders::tableName() . '.date_start', $dateStart],
                ['>=', Tenders::tableName() . '.date_end', $dateEnd],
                ['in', Tenders::tableName() . '.type', $this->type],
                ['in', Tenders::tableName() . '.mode', $this->mode],
                ['in', 'projects.id', $this->objectTypeFilter],
            ]);


            $query->andFilterWhere([
                'or',
                ['in', Company::tableName().'.id', $companyIdsFromSphinx],
                ['in', Tenders::tableName().'.id', $tendersIdsFromSphinx],
            ]);

        }


        $sort = new Sort([
            'attributes' => [
                'date_start' => [
                    'asc' => [
                        self::tableName() . '.id' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.id' => SORT_DESC,
                    ],

                ],
            ],
            'defaultOrder' => [
                'date_start' => SORT_DESC,
            ],
        ]);

        $query->orderBy($sort->orders);


//       // Dev::dump($query->all());
//        foreach ($query->all() as $item) {
//            Dev::dump($query->distinct()->count());
//            Dev::dump($item->title);
//        }
//        die();

        if($onlyQuery) {
            return $query->distinct();
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->distinct()->count(),
                'pageSize' => YII_ENV == 'dev' ? 10 : 10,
                'pageSizeParam' => false,
            ],
            'sort' => $sort,
        ]);
    }

    /**
     * Выбирает все данные для атрибутов учавствующих в фильтрации.
     *
     * @return void
     */
    public function getFilterValues()
    {

    }

    /**
     * Активные товары
     *
     * @return ActiveQuery
     */
    public static function frontendActive()
    {
        return self::find();
     //   return self::find()->andWhere('status=:status', [':status' => self::STATUS_ACTIVE]);
    }

    /**
     * Возвращает запрос на выборку из текущей таблицы товаров.
     *
     * @return Query
     */
    public function getQuery()
    {
        return (new Query())->distinct()->from(self::tableName())->select('*');
    }


    /**
     * Возвращает предложения для тендера от компании в виде массива и сортировки для отбова предложений - страница списка тендеров
     *
     * @return array
     */
    public function getTendersListOffers()
    {
        $returnOffersTender = [];

        $offers = $this->getOffers()
            ->orderBy(['updated_at' => SORT_ASC])
            ->all();

        // Получаем все предложения к тендеру
        foreach ($offers as $key => $offer) {
            /**
             * @var Offers $offer
             */
            $companyId = $offer->company_id;

            $offersTender[$companyId][$key]['id'] = $offer->id;
            $offersTender[$companyId][$key]['companyName'] = $offer->company->title;
            $offersTender[$companyId][$key]['sum'] = $offer->sum;
            $offersTender[$companyId][$key]['sumDiscount'] = $offer->sum_discount;
            $offersTender[$companyId][$key]['date'] = $offer->created_at;
        }

        // Форматируем вывод как 1ое преложение, ответ, последнее предложение
        if (!empty($offersTender)) {
            foreach ($offersTender as $companyId => $offers) {
                $returnOffersTender[$companyId]['companyName'] = current($offers)['companyName'];
                $returnOffersTender[$companyId]['firstOffer'] = current($offers);
                $returnOffersTender[$companyId]['answerOffer'] = array_pop($offers);
                $returnOffersTender[$companyId]['lastOffer'] = end($offers);

            }
        }

        return $returnOffersTender;
    }

    /**
     * Поиск id компаний по сфинксу
     *
     * @param Request $request
     *
     * @return array
     */
    private function searchBySphinxCompany($request)
    {
        $text = $request->get('Company')['inn'];
        $sphinxSearch = new SphinxQuery();
        // Ищем по индексу компаний
        $companyIdxData = $sphinxSearch->from(\Yii::$app->params['sphinx']['sphinx_source_company'])
            ->match((new MatchExpression())->match('@title :title', ['title' => $text])
                ->orMatch('@inn :inn', ['inn' => $text])
                ->orMatch('@ogrn :ogrn', ['ogrn' => $text])
                ->orMatch('@kpp :kpp', ['kpp' => $text])
                ->orMatch('@general_manage :general_manage', ['general_manage' => $text])
            )->all();

        // Ищем по индексу объектов
        $objectsIdxData = $sphinxSearch->from(\Yii::$app->params['sphinx']['sphinx_source_objects'])
            ->match((new MatchExpression())->match('@title :title', ['title' => $text])
                ->orMatch('@description :description', ['description' => $text])
                ->orMatch('@employerName :employerName', ['employerName' => $text])
            )->all();

        $result = array_unique(array_merge(ArrayHelper::map($objectsIdxData, 'id', 'company_id'), ArrayHelper::map($companyIdxData, 'id', 'id')));
        return $result;
    }

    /**
     * Поиск id тендеров по сфинксу
     *
     * @param Request $request
     *
     * @return array
     */
    private function searchBySphinxTenders($request)
    {
        $text = $request->get('Company')['inn'];
        $sphinxSearch = new SphinxQuery();
        // Ищем по индексу тендеров
        $tendersIdxData = $sphinxSearch->from(\Yii::$app->params['sphinx']['sphinx_source_tenders'])
            ->match((new MatchExpression())->match('@title :title', ['title' => $text])
                ->orMatch('@description :description', ['description' => $text])
            )->all();

        // Dev::dump(ArrayHelper::map($objectsIdxData, 'id', 'company_id'));
        // Dev::dump(ArrayHelper::map($companyIdxData, 'id', 'id'));

        $result = array_unique(ArrayHelper::map($tendersIdxData, 'id', 'id'));
        // Dev::dump($result);
        // die();
        return $result;
    }
}
