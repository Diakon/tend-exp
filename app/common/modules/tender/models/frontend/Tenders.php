<?php
namespace common\modules\tender\models\frontend;

use modules\crud\models\ActiveQuery;
use common\components\ClassMap;
use common\components\SendMail;
use common\models\EntityFile;
use common\modules\company\models\frontend\Company;
use common\modules\company\models\frontend\CompanyUsers;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\common\Tenders as CommonCompanyTenders;
use common\modules\tender\models\common\TendersCharsWorks;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use frontend\modules\upload\components\FileUploadInterface;
use frontend\components\CheckTHashTrait;

/**
 * Тендеры компании
 *
 * @property string  $tHash   Временный идентификатора модели
 */
class Tenders extends CommonCompanyTenders implements FileUploadInterface
{
    use CheckTHashTrait;

    const MODEL_ATTRIBUTE_FILES = 'files';

    /**
     * @var integer Флаг, что необходимо создание объекта в тендере
     */
    public $isCreateObject;
    /**
     * @var string Создание объекта в тендере - заголовок объекта
     */
    public $createObjectTitle;
    /**
     * @var integer Создание объекта в тендере - статус объекта
     */
    public $createObjectStatus;
    /**
     * @var array Создание объекта в тендере - тип объекта
     */
    public $createObjectType = [];
    /**
     * @var array Создание объекта в тендере - функция объекта
     */
    public $createObjectFunction = [];

    /**
     * ID работ тендера
     * @var array
     */
    public $charsWorksIds = [];

    use SearchFilterTrait;

    // сценарий для страницы списка тенеров
    const  SCENARIO_FRONTEND_FILTER = 'frontend-filter';

    // сценарий создания тендера
    const SCENARIO_CREATE_TENDER = 'create-tender';

    /**
     * Событие после коммита в RequestMainAction
     *
     * @see \frontend\modules\request\controllers\actions\RequestMainAction::run
     */
    const EVENT_AFTER_MAIN_ACTION_COMMIT = 'event_after_main_action_commit';

    /**
     * @var string $tHash Свойство для хранения временного идентификатора модели
     */
    private $tHash;

    /**
     * @param string $tHash
     */
    public function setTHash(string $tHash)
    {
        $this->tHash = $tHash;
    }

    /**
     * @return string
     */
    public function getTHash(): string
    {
        if (!empty($this->tHash)) {
            return $this->tHash;
        }

        $this->setTHash($this->isNewRecord ? uniqid('thash_' . substr(md5(static::class), 0, 6) . '_') : (string)$this->id);

        return $this->tHash;
    }

    /**
     * Получаем ajax путь удаления файла
     *
     * @return string
     */
    public function getAjaxDeleteFileUrl(): string
    {
        return Url::to(['/upload/base/delete']);
    }

    /**
     * Получаем ajax путь загрузки файла
     *
     * @return string
     */
    public function getAjaxUploadFileUrl(): string
    {
        return Url::to(['/upload/base/upload']);
    }

    /**
     * Подготовка модели к редактированию
     *
     * @param $tHash
     *
     * @throws \Exception
     */
    public function prepareModelAction($tHash)
    {
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'processTHashEntityFile'], ['tHash' => $tHash]);
        $this->on(self::EVENT_AFTER_MAIN_ACTION_COMMIT, [$this, 'flushTHash'], ['tHash' => $tHash]);
    }

    /**
     * Получаем реляцию модели
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getFiles()
    {
        return $this->hasMany(EntityFile::class, ['modelId' => 'thash'])->onCondition(['modelName' => ClassMap::getId(self::class), 'modelAttribute' => 'files']);
    }

    public function rules()
    {
        $rules = [];
        switch ($this->getScenario()) {
            case self::SCENARIO_CREATE_TENDER:
                $rules[] = [['title', 'company_id', 'user_id', 'status', 'currency_id', 'date_end', 'function_contractor_id'], 'required'];
                $rules[] = ['createObjectTitle', 'createObjectTitleValidation', 'skipOnEmpty' => false];
                $rules[] = ['createObjectStatus', 'createObjectStatusValidation', 'skipOnEmpty' => false];
                $rules[] = ['object_id', 'objectIdValidation', 'skipOnEmpty' => false];
                $rules[] = ['title', 'titleValidation', 'skipOnEmpty' => false];
                $rules[] = ['date_end', 'dateValidation', 'skipOnEmpty' => false];
                $rules[] = [['charsWorksIds'], 'each', 'rule' => ['integer']];

                $rules[] = [['isCreateObject', 'createObjectStatus'], 'integer'];
                $rules[] = [['createObjectTitle'], 'string', 'max' => 255];
                $rules[] = [['createObjectType', 'createObjectFunction'], 'each', 'rule' => ['integer']];

                break;
        }

        return ArrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * Валидация названия тендера
     *
     * @param $attribute
     * @return bool
     */
    public function titleValidation($attribute)
    {
        if (empty($this->title)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Вы не указали название тендера'));
            $this->addError('title', Yii::t('app', 'Вы не указали название тендера'));
            return false;
        }

        return true;
    }

    /**
     * Валидация даты окончания тендера
     *
     * @param $attribute
     * @return bool
     */
    public function dateValidation($attribute)
    {
        if (empty($this->date_end)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Вы не указали срок завершения тендера'));
            $this->addError('date_end', Yii::t('app', 'Вы не указали срок завершения тендера'));
            return false;
        }

        return true;
    }

    /**
     * Валидация создания объекта - название
     *
     * @param $attribute
     * @return bool
     */
    public function createObjectTitleValidation($attribute)
    {
        if (empty($this->createObjectTitle) && empty($this->object_id)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Вы не указали название объекта'));
            $this->addError('createObjectTitle', Yii::t('app', 'Вы не указали название объекта'));
            return false;
        }

        return true;
    }

    /**
     * Валидация создания объекта - статус завершения объекта
     *
     * @param $attribute
     * @return bool
     */
    public function createObjectStatusValidation($attribute)
    {
        if (empty($this->createObjectStatus) && empty($this->object_id)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Вы не указали статус объекта'));
            $this->addError('createObjectStatus', Yii::t('app', 'Вы не указали статус объекта'));
            return false;
        }

        return true;
    }

    /**
     * Валидация выбора объекта из списка созданых ранее
     *
     * @param $attribute
     * @return bool
     */
    public function objectIdValidation($attribute)
    {
        if (empty($this->createObjectTitle) && empty($this->object_id)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Выберите объект тендера или создайте новый объект'));
            $this->addError('object_id', Yii::t('app', 'Выберите объект тендера или создайте новый объект'));
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_FRONTEND_FILTER => ['date_start', 'date_end', 'inn', 'type', 'mode', 'dateFilterStart', 'dateFilterEnd'],
            self::SCENARIO_CREATE_TENDER => [
                'createObjectTitle', 'createObjectStatus', 'isCreateObject', 'createObjectType', 'createObjectFunction', 'object_id',
                'company_id', 'user_id', 'status', 'type', 'mode', 'stage', 'currency_id', 'created_at', 'updated_at', 'charsWorksIds',
                'title', 'files_store_link', 'description', 'date_start', 'date_end', 'offer_id', 'function_contractor_id'
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'createObjectTitle' => Yii::t('app', 'Заголовок объекта'),
            'createObjectStatus' => Yii::t('app', 'Тип объекта'),
            'createObjectType' => Yii::t('app', 'Функция объекта'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->date_start = !empty($this->date_start) ? (date('Y-m-d', strtotime($this->date_start)) . ' 00:00:00') : null;
        $this->date_end = !empty($this->date_end) ? (date('Y-m-d', strtotime($this->date_end)) . ' 23:59:59') : null;
        $this->user_id = Yii::$app->user->id;
        $this->type = self::TYPE_REQUEST;
        $this->mode = self::MODE_OPEN;
        $this->stage = self::STAGE_LEVEL_1;

        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает сисок тендеров к которым есть доступ у пользователя
     *
     * @param null $request Параметры фильтрации.
     * @param CompanyUsers $companyUser Модель пользователя CompanyUsers
     * @return ActiveDataProvider
     */
    public function getUserTenders($request = null, $companyUser)
    {
        // Получаем Id объектов компании к которым у пользователя есть доступ
        $objectsIds = ArrayHelper::map(Objects::getObjectsUser($companyUser)->asArray()->all(), 'id', 'id');

        /** @var \yii\db\ActiveQuery $query */
        $query = self::find();
        $query->where(['in', 'object_id', $objectsIds]);


        if (!empty($request)) {

        };

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' => 10,
                'pageSizeParam' => false,
            ],
        ]);
    }

    /**
     * Возвращает сисок тендеров в которых участвует компания
     *
     * @param array $request Параметры фильтрации.
     * @param Company $company Модель компании
     * @return ActiveDataProvider
     */
    public function getPartnersTenders($request = null, $company)
    {
        // Получаем Id тендеров, к которым от имени моей компании были оставлены предложения
        $tendersIds = ArrayHelper::map(Offers::find()->where(['company_id' => $company->id])->groupBy(['tender_id'])->asArray()->all(), 'tender_id', 'tender_id');

        /** @var \yii\db\ActiveQuery $query */
        $query = self::find();
        $query->where(['in', 'id', $tendersIds ?? []]);


        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' => 10,
                'pageSizeParam' => false,
            ],
        ]);
    }

    /**
     * Вычисляет и возвращает суммму стоимости тендера с учетои и без учета запроса скидки по предложению $offerId
     *
     * @param integer $offerId
     * @return array
     */
    public function getTotalSum($offerId)
    {
        $totalSum = [];
        // Получаю сумму стоимости работ для тех работ, которые есть в рубриках
        foreach ($this->getEstimatesRubrics()->andWhere(['depth' => 1])->all() as $data) {
            $sum = $data->getSum($offerId);
            $totalSum['noDiscount'] += $sum['noDiscount'];
            $totalSum['withDiscount'] += $sum['withDiscount'];
        }
        //ToDo добавить получение стоимости доп. работ из оффера
        // Высчитываю размер общей скидки
        if (!empty($totalSum)) {
            $totalSum['totalDiscountPercent'] = ($totalSum['noDiscount'] - $totalSum['withDiscount']) / $totalSum['noDiscount'] * 100;
        }

        return $totalSum;
    }

    /**
     * Возвращает сисок активных для участия тендеров
     *
     * @param null $request Параметры фильтрации.
     * @return ActiveDataProvider
     */
    public function getActiveTenders($request = null)
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = self::find();
        $query->where(['in', 'status', [self::STATUS_ACTIVE]]);

        if (!empty($request)) {

        };

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' => 10,
                'pageSizeParam' => false,
            ],
        ]);
    }

    /**
     * Связь с предложениями поступившими к тендеру
     *
     * @return object
     */
    public function getOffersList()
    {
        return Offers::find()->where(['tender_id' => $this->id]);
    }

    /**
     * Возвращает документы приложенные к тендеру
     *
     * @return array
     */
    public function getDocumentsFiles()
    {
        $documents = [];
        foreach ($this->documents as $key => $document) {
            $documents[$key]['name'] = $document->file_name;
            $documents[$key]['url'] = $document->getFile(true);
        }

        return $documents;
    }

    /**
     * Ссылка на список предложений для тендера.
     * @param array  $params Массив параметров.
     *
     * @return string
     */
    public function getOffersListLink(array $params = [])
    {
        return $this->getListLink('tender_offers_list', $params);
    }

    /**
     * Возвращает общеее количество и из них новых предложений для тендера
     * @return array
     */
    public function getOffersCount()
    {
        $counts = [];
        $query = $this->getOffersList()->andWhere(['is_archive' => Offers::IS_ARCHIVE_NO])->groupBy(['company_id']);
        $counts['totalCount'] = $query->count();
        $counts['newCount'] = $query->andWhere(['is_read' => Offers::IS_READ_NO])->count();

        return $counts;
    }

    /**
     * Связь с активными предложениями для тендера. Возможна конкретизация по ID компании автора предложения.
     *
     * @param null|integer $companyId
     * @return ActiveQuery
     */
    public function getActiveOffer($companyId = null)
    {
        $query = Offers::find()->where(['tender_id' => $this->id])->andWhere(['is_archive' => Offers::IS_READ_NO]);
        if (!empty($companyId)) {
            $query->andWhere(['company_id' => $companyId]);
        }

        return $query;
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        // Если выбрали победителя тендера, то для всех предложений к этому тендеру ставлю статус "откланено", а для победителя "одобрен"
        if (!empty($this->offer_id)) {
            Offers::updateAll(['status' => Offers::STATUS_APPROVED_NO], ['tender_id' => $this->id]);  // Всем предложениям для тендера ставим статус "откланено"
            Offers::updateAll(['status' => Offers::STATUS_APPROVED_YES], ['id' => $this->offer_id]);  // Для предложения - победителя - ставлю статус "одобрен"
        }
    }

    /**
     * Создает тендер (и если надо то и объект) на фронте
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    public function saveFrontTender()
    {
        $errorMsg = [];
        $needRollBack = false;
        $transactionOffer = Tenders::getDb()->beginTransaction();
        try {
            // Если тендер в статусе публиукации - проверяю, что для него сделана смета
            if ($this->status == Tenders::STATUS_ACTIVE) {
                $countEstimates = EstimatesRubrics::find()->where(['tender_id' => $this->id])->count();
                if (empty($countEstimates)) {
                    $errorMsg[] =  Yii::t('app', 'Вы не заполнили смету тендера.');
                    $needRollBack = true;
                    $this->status = 0;
                }
            }

            $dateStart = time();
            $dateEnd = strtotime($this->date_end);
            // Если не указали объект - создаю новый объект для тендера
            if (empty($this->object_id)) {
                $modelObject = new Objects();
                $modelObject->setScenario(Objects::SCENARIO_CREATE_OBJECT_IN_TENDER);
                $modelObject->title = $this->createObjectTitle;
                $modelObject->date_start = Yii::$app->formatter->asDate($dateStart, 'php:d.m.Y');
                $modelObject->date_end = Yii::$app->formatter->asDate($dateEnd, 'php:d.m.Y');
                $modelObject->status = Objects::STATUS_ACTIVE;
                $modelObject->status_complete_id = $this->createObjectStatus;
                $modelObject->company_id = $this->company_id;
                $modelObject->user_id = $this->user_id;
                $modelObject->functionsIds = $this->createObjectFunction;
                $modelObject->typesIds = $this->createObjectType;
                if ($modelObject->save()) {
                    $this->object_id = $modelObject->id;
                } else {
                    $needRollBack = true;
                    Yii::error($modelObject->errors);
                    $errorMsg[] = implode('<br>', $modelObject->errors);
                }
            }

            // Сохраняю тендер
            $this->setScenario(self::SCENARIO_CREATE_TENDER);
            if (!$needRollBack && $this->save()) {
                $this->saveCharsWorks();
                $transactionOffer->commit();
                return true;
            } else {
                Yii::error($this->errors);
                $errorMsg[] = implode(',', $this->errors ?? []);
                Yii::$app->session->setFlash('error', implode('<br>', $errorMsg));
            }
            $transactionOffer->rollBack();
            return false;
        } catch(\Throwable $e) {
            $transactionOffer->rollBack();
            Yii::error('Ошибка при ' . (!empty($this->id) ? ('редактировании тендера с ID = ' . $this->id) : 'создании нового тендера' ) . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenders');
            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения тендера3.'));

            return false;
        }
    }

    /**
     * Обновляет записи о работаз тендера
     */
    private function saveCharsWorks()
    {
        TendersCharsWorks::deleteAll(['tender_id' => $this->id]);
        foreach (\common\modules\directories\models\common\Works::find()->where(['in', 'id', $this->charsWorksIds])->all() as $work) {
            $model = new TendersCharsWorks();
            $model->tender_id = $this->id;
            $model->main_work_id = $work->parent->parent->id;
            $model->types_work_id = $work->parent->id;
            $model->works_id = $work->id;
            $model->save();
        }
    }

    /**
     * Формируем ссылку на страницу формы создания
     *
     * @param $companyUrl
     *
     * @return string
     */
    public function getCreateLink($companyUrl)
    {
        return Url::to(['/dashboard/dashboard/tender_add',  'companyUrl' => $companyUrl, 'tHash' => $this->getTHash()]);
    }

    /**
     * Выбор победителя тендера
     *
     * @param integer $offerId
     * @return bool
     */
    public function setWinnerTender($offerId)
    {
        $offer = Offers::find()
            ->where(['id' => $offerId])
            ->andWhere(['status' => Offers::STATUS_ACTIVE])
            ->andWhere(['is_archive' => Offers::IS_ARCHIVE_NO])
            ->andWhere(['tender_id' => $this->id])
            ->one();

        if (!empty($offer) && $this->status != self::STATUS_CLOSE && empty($this->offer_id)) {
            $this->offer_id = $offer->id;
            $this->status = Tenders::STATUS_CLOSE;
            if ($this->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Для тендера №' . $this->id . ' выбран победитель ' , $offer->company->title));
                // Отпрака письма о выборе победителя тендера
                $email = $offer->user->email;
                $viewEmail = 'tender-winner';
                $subjectEmail = 'TENDEROS. Вы победили в тендере № ' . $this->id;
                $paramView = [];
                $paramView['userName'] = $offer->user->username;
                $paramView['email'] = $email;
                $paramView['numberTender'] = $this->id;
                $paramView['nameTender'] = $this->title;
                $paramView['companyUrl'] = $this->company->url;
                $paramView['companyName'] = $this->company->title;
                $paramView['offerLink'] = Yii::$app->urlManager->createAbsoluteUrl([
                    'dashboard/tender_offer_view',
                    'id' => $offer->id,
                    'companyUrl' => $offer->company->url,
                    'tenderId' => $this->id
                ]);
                SendMail::send(
                    $viewEmail,
                    $paramView,
                    Yii::$app->params['infoEmailFrom'],
                    [$email => $offer->user->username],
                    $subjectEmail
                );

                return true;
            }
        }
        Yii::$app->session->setFlash('info', Yii::t('app', 'Ошибка выбора победителя для тендера №' . $this->id));


        return false;
    }
}
