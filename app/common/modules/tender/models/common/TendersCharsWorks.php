<?php
namespace common\modules\tender\models\common;

use common\modules\directories\models\common\MainWork;
use common\modules\directories\models\common\TypesWork;
use common\modules\directories\models\common\Works;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Работы тендера.
 *
 * @property integer  $id            Идентификатор.
 * @property integer  $tender_id     Тендер.
 * @property integer  $main_work_id  Направление работы.
 * @property integer  $types_work_id Группа работы.
 * @property integer  $works_id      Работы.
 * @property integer  $created_at    Дата создания записи.
 * @property integer  $updated_at    Дата изменения записи.
 */
class TendersCharsWorks extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenders_chars_works}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['tender_id', 'main_work_id', 'types_work_id', 'works_id'], 'required'],
            [['tender_id', 'main_work_id', 'types_work_id', 'works_id'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tender_id' => Yii::t('app', 'Тендер'),
            'main_work_id' => Yii::t('app', 'Направление работы'),
            'types_work_id' => Yii::t('app', 'Группа работы'),
            'works_id' => Yii::t('app', 'Работа'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с тендером
     *
     * @return ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tenders::class, ['id' => 'tender_id']);
    }

    /**
     * Связь с направлением работы
     *
     * @return ActiveQuery
     */
    public function getMainWork()
    {
        return $this->hasOne(MainWork::class, ['id' => 'main_work_id']);
    }

    /**
     * Связь с группами работ
     *
     * @return ActiveQuery
     */
    public function getTypesWork()
    {
        return $this->hasOne(TypesWork::class, ['id' => 'types_work_id']);
    }

    /**
     * Связь с работой
     *
     * @return ActiveQuery
     */
    public function getWork()
    {
        return $this->hasOne(Works::class, ['id' => 'works_id']);
    }
}
