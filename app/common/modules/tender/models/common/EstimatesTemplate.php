<?php
namespace common\modules\tender\models\common;

use common\models\UserIdentity;
use common\modules\company\models\common\Company;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Шаблоны смет компании.
 *
 * @property integer $id             Идентификатор.
 * @property integer $company_id     Компания.
 * @property integer $user_id        Пользователь.
 * @property string $title           Название.
 * @property string $estimates_data  Содержимое шаблона.
 * @property integer $created_at     Дата создания записи.
 * @property integer $updated_at     Дата изменения записи.
 */
class EstimatesTemplate extends ActiveRecord
{
    /**
     * Массив смет для созранения в estimates_data
     *
     * @var array
     */
    public $estimatesDataArray = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%estimates_template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'user_id', 'title', 'estimates_data'], 'required'],
            [['company_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['estimates_data'], 'string'],
            ['estimatesDataArray', 'validateEstimatesDateArray'],
        ]);
    }


    /**
     * Валидирует массив шаблона сметы
     *
     * @param string $attribute
     * @param mixed $params
     * @param \yii\validators\InlineValidator $validator
     * @return bool
     */
    public function validateEstimatesDateArray($attribute, $params, $validator)
    {
        // Проверяю, что указаны заголовки
        foreach ($this->{$attribute} as $item) {
            if (empty('title')) {
                $this->addError($attribute, 'Не указан заголовок сметы ');
                return false;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->estimatesDataArray = !empty($this->estimates_data) ? json_decode($this->estimates_data, true) : [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'user_id' => Yii::t('app', 'Автор щаблона'),
            'title' => Yii::t('app', 'Название шаблона'),
            'estimates_data' => Yii::t('app', 'Содержимое щаблона'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate() {
        if (parent::beforeValidate()) {
            $this->estimates_data = !empty($this->estimatesDataArray) ? json_encode($this->estimatesDataArray) : null;

            return true;
        }

        return false;
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с пользователями
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'user_id']);
    }
}
