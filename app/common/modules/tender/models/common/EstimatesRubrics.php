<?php
namespace common\modules\tender\models\common;

use common\components\ClassMap;
use common\modules\offers\models\common\OffersElements;
use Yii;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtml;
use yii\helpers\Json;
use frontend\models\EntityComment;

/**
 * Рубрики смет тендера компани.
 *
 * @property integer $id         Иднтификатор
 * @property integer $parent_id  Родительская запись
 * @property integer $lft        Левый ключ
 * @property integer $rgt        Правый ключ
 * @property integer $depth      Уровень
 * @property string $title       Название сметы
 * @property integer $tender_id  Тендер
 * @property integer $created_at Дата создания записи.
 * @property integer $updated_at Дата изменения записи.
 */
class EstimatesRubrics extends \modules\crud\models\ActiveRecord
{
    /**
     * Максимальный уровень вложенности
     */
    const LEVEL_MAX = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%estimates_rubrics}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['parent_id', 'tender_id', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
        $rules[] = [['title', 'parent_id'], 'required'];

        return ArrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название сметы'),
            'tender_id' => Yii::t('app', 'Тендер'),
            'parent_id' => Yii::t('app', 'Родительская запись'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с тендером проекта
     *
     * @return ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tenders::class, ['id' => 'tender_id']);
    }

    /**
     * Связь с работами
     *
     * @return ActiveQuery
     */
    public function getElements()
    {
        return $this->hasMany(EstimatesElements::class, ['rubric_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'tree' => [
                    'class' => NestedSetsBehavior::class
                ],
            ]);
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new TreeQuery(get_called_class());
    }


    /**
     * Связь с предложениями по данной рубрике сметы
     *
     * @param null $offerId
     * @return object
     */
    public function getOffersElements($offerId = null)
    {
        $offersElements = OffersElements::find()
            ->where(['class_name' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID])
            ->andWhere(['class_id' => $this->id]);
        if (!empty($offerId)) {
            $offersElements->andWhere(['offer_id' => $offerId]);
        }

        return $offersElements;
    }

    /**
     * Создает новый раздел
     *
     * @return bool
     */
    public function saveNode()
    {
        $root = self::find()->where(['id' => $this->parent_id])->one();
        if (empty($root)) {
            $root = self::getRoot($root);
        }
        // Проверка, что не превышаем разрешенный уровень вложености для проекта
        if ($root->depth >= self::LEVEL_MAX) {
            Yii::error('Попытка создать запись больше максимально разрешенной вложенности. Данные:' . Json::encode($this), 'tenderEstimates');
            return false;
        }
        $this->setDefaultTitleRubric();
        $this->parent_id = $root->id;

        $transaction = self::getDb()->beginTransaction();
        try {
            if ($this->appendTo($root)) {
                $transaction->commit();
                return true;
            }
        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при создании новой рубрики сметы. Родительская запись ID=' . $this->parent_id . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderEstimates');

            return false;
        }

        return false;
    }

    /**
     * Обновляет существующий раздел
     *
     * @return bool
     */
    public function updateNode()
    {
        if ($this->depth > self::LEVEL_MAX) {
            Yii::error('Попытка создать запись больше максимально разрешенной вложенности. Данные:' . Json::encode($this), 'tenderEstimates');
            return false;
        }
        $this->setDefaultTitleRubric();
        $transaction = self::getDb()->beginTransaction();
        try {
            if ($this->update()) {
                $transaction->commit();
                return true;
            }
        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при обновлении существующей рубрики сметы с ID=' . $this->id . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderEstimates');

            return false;
        }

        return false;
    }

    /**
     * Удаляет раздел
     *
     * @return bool
     */
    public function deleteNode()
    {
        $transaction = self::getDb()->beginTransaction();
        try {
            if (!$this->deleteWithChildren()) {
                throw new \yii\web\HttpException(500, BaseHtml::errorSummary($this));
            }
            $transaction->commit();

            return true;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при удалении рубрики сметы с ID=' . $this->id . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderEstimates');

            return false;
        }
    }

    /**
     * Вставляет запись перед указанной записью (на том же уровне)
     *
     * @param $rubricId
     * @return bool
     */
    public function insertNodeBefore($rubricId)
    {
        $rubric = self::find()->where(['id' => $rubricId])->one();
        // Проверка, что не превышаем разрешенный уровень вложености для проекта
        if ($rubric->depth > self::LEVEL_MAX) {
            Yii::error('Попытка создать запись больше максимально разрешенной вложенности. Данные:' . Json::encode($this), 'tenderEstimates');
            return false;
        }
        $transaction = self::getDb()->beginTransaction();
        try {
            $this->setDefaultTitleRubric($rubric->title);
            $this->parent_id = $rubric->parent_id;
            if ($this->insertBefore($rubric)) {
                $transaction->commit();
                return true;
            }

        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при добавлении рубрики сметы "выше" рубрики с ID=' . $rubricId . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderEstimates');

            return false;
        }

        return false;
    }

    /**
     * Вставляет запись после указанной записью (на том же уровне)
     *
     * @param $rubricId
     * @return bool
     */
    public function insertNodeAfter($rubricId)
    {
        $rubric = self::find()->where(['id' => $rubricId])->one();
        if ($rubric->depth > self::LEVEL_MAX) {
            Yii::error('Попытка создать запись больше максимально разрешенной вложенности. Данные:' . Json::encode($this), 'tenderEstimates');
            return false;
        }
        $transaction = self::getDb()->beginTransaction();
        try {
            $this->setDefaultTitleRubric($rubric->title);
            $this->parent_id = $rubric->parent_id;
            if (!$this->insertAfter($rubric)) {
                throw new \yii\web\HttpException(500, BaseHtml::errorSummary($this));
            }
            $transaction->commit();

            return true;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при добавлении рубрики сметы "ниже" рубрики с ID=' . $rubricId . '. 
            Сообщение валидатора: ' . $e->getMessage() . '. Данные: ' . Json::encode($this), 'tenderEstimates');

            return false;
        }
    }

    /**
     * Устанавливает дефолтное имя рубрики, если название не указанно.
     *
     * @param string $defaultTitle
     */
    private function setDefaultTitleRubric($defaultTitle = 'Новый раздел')
    {
        $this->title = !empty($this->title) ? $this->title : $defaultTitle;
    }

    /**
     * Возвращает родительский узел, если его нет - создает
     *
     * @param EstimatesRubrics|null $model
     *
     * @return mixed
     */
    public static function getRoot(self $model = null)
    {
        if (is_null($model)) {
            $model = new self;
            $model->title = 'RootTree';
            $model->parent_id = 0;
        }
        $root = $model::find()->roots()->one();
        if (!$root) {
            $model->makeRoot();
            $root = $model::find()->roots()->one();
        }

        return $root;
    }

    /**
     * Возвращает потомков узла
     *
     * @param EstimatesRubrics|null $model
     *
     * @return mixed
     */
    public static function getChildrenTree(self $model = null)
    {
        $root = self::getRoot($model);

        return $root->children();
    }

    /**
     * Связь с коментариями. Если передан ID предложения - отдаю так же
     * коментарии оставленные в предложении $offerId для этой сметы
     *
     * @param integer $offerId
     * @return ActiveQuery
     */
    public function getComments($offerId = null)
    {
        return EntityComment::find()->where([
            'OR',
            ['AND',['className' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID], ['entityId' => $this->id], ['offerId' => null]],
            ['AND',['className' => ClassMap::CLASS_ESTIMATES_RUBRICS_ID], ['entityId' => $this->id], ['offerId' => $offerId]]
        ]);
    }
}
