<?php
namespace common\modules\tender\models\common;

use modules\crud\models\ActiveRecord;
use common\components\ClassMap;
use common\modules\directories\models\common\Works;
use common\modules\offers\models\common\OffersElements;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Элементы рубрики сметы тендера компани.
 *
 * @property integer $id             Иднтификатор
 * @property integer $rubric_id      Раздел сметы
 * @property integer $tender_id      Тендер
 * @property string $title           Работа
 * @property integer $unit_id        Единица измерения
 * @property integer $count          Количество
 * @property integer $created_at     Дата создания записи.
 * @property integer $updated_at     Дата изменения записи.
 */
class EstimatesElements extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%estimates_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['tender_id', 'unit_id', 'title', 'count'], 'required'],
            [['rubric_id', 'tender_id', 'unit_id', 'count', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 555],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rubric_id' => Yii::t('app', 'Раздел сметы'),
            'tender_id' => Yii::t('app', 'Тендер'),
            'title' => Yii::t('app', 'Работа'),
            'unit_id' => Yii::t('app', 'Единица измерения'),
            'count' => Yii::t('app', 'Количество'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->tender_id = !empty($this->rubric) ? $this->rubric->tender_id :  $this->tender_id;

            return true;
        }
        return false;
    }

    /**
     * Связь с рубрикой сметы тендера
     *
     * @return ActiveQuery
     */
    public function getRubric()
    {
        return $this->hasOne(EstimatesRubrics::class, ['id' => 'rubric_id']);
    }

    /**
     * Связь с тендером
     *
     * @return ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tenders::class, ['id' => 'tender_id']);
    }

    /**
     * Возвращает список единиц измерения, или значение выбранной единицы измерения
     *
     * @param null $unitId
     * @return array
     */
    public static function getUnits($unitId = null)
    {
        $units = Works::$units;
        return isset($units[$unitId]) ? $units[$unitId] : $units;
    }

    /**
     * Возвращает стоимость работы из предложения к тендеру
     *
     * @param integer $offerId
     * @return float
     */
    public function getOfferPriceWork($offerId)
    {
        $offerElement = OffersElements::find()
            ->where(['offer_id' => (int)$offerId])
            ->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])
            ->andWhere(['class_id' => $this->id])
            ->one();

        return !empty($offerElement) ? $offerElement->price_work : 0;
    }

    /**
     * Возвращает стоимость материала из предложения к тендеру
     *
     * @param integer $offerId
     * @return float
     */
    public function getOfferPriceMaterial($offerId)
    {
        $offerElement = OffersElements::find()
            ->where(['offer_id' => (int)$offerId])
            ->andWhere(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])
            ->andWhere(['class_id' => $this->id])
            ->one();

        return !empty($offerElement) ? $offerElement->price_material : 0;
    }

    /**
     * Связь с предложениями по данной работе сметы
     *
     * @param null $offerId
     * @return object
     */
    public function getOffersElements($offerId = null)
    {
        $offersElements = OffersElements::find()
            ->where(['class_name' => ClassMap::CLASS_ESTIMATES_ELEMENTS_ID])
            ->andWhere(['class_id' => $this->id]);
        if (!empty($offerId)) {
            $offersElements->andWhere(['offer_id' => $offerId]);
        }

        return $offersElements;
    }
}
