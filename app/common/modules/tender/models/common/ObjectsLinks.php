<?php
namespace common\modules\tender\models\common;

use modules\crud\models\ActiveQuery;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;

/**
 * Объекты компании.
 *
 * @property integer                 $id                Идентификатор.
 * @property integer                 $object_id         Объект.
 * @property string                  $link              Ссылка
 * @property integer                 $created_at        Дата создания записи.
 * @property integer                 $updated_at        Дата изменения записи.
 */
class ObjectsLinks extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%objects_links}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['object_id', 'link'], 'required'],
            [['object_id', 'created_at', 'updated_at'], 'integer'],
            [['link'], 'string', 'max' => 255],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'object_id' => Yii::t('app', 'Объект'),
            'link' => Yii::t('app', 'Ссылка'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors();
    }

    /**
     * Связь с компаниями
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::class, ['id' => 'object_id']);
    }
}