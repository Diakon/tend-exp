<?php
namespace common\modules\tender\models\common;

use modules\crud\models\ActiveRecord;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\models\UserIdentity;

/**
 * Тендеры, добавленые в избранное пользователем.
 *
 * @property integer $id                        Иднтификатор
 * @property integer $tender_id                 Тендер
 * @property integer $user_id                   Пользователь
 * @property integer $created_at                Дата создания записи.
 * @property integer $updated_at                Дата изменения записи.
 */
class UserFavoritesTenders extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_favorites_tenders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['tender_id', 'user_id'], 'required'],
            [['tender_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), []);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tender_id' => Yii::t('app', 'Тендер'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Связь с тендером
     *
     * @return ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tenders::class, ['id' => 'tender_id']);
    }

    /**
     * Связь с пользователем
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'user_id']);
    }
}
