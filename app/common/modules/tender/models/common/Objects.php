<?php
namespace common\modules\tender\models\common;

use common\models\UserIdentity;
use common\modules\company\models\common\Company;
use common\modules\company\models\common\CompanyAddress;
use common\modules\directories\models\common\Activities;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\directories\models\common\Projects;
use common\modules\directories\models\common\Works;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;
use yii2tech\ar\linkmany\LinkManyBehavior;

/**
 * Объекты компании.
 *
 * @property integer                 $id                Идентификатор.
 * @property integer                 $company_id        Компания.
 * @property integer                 $user_id           Пользователь
 * @property UserIdentity            $user              Пользователь.
 * @property integer                 $address_id        Адрес объекта.
 * @property CompanyAddress          $address           Адрес объекта.
 * @property string                  $title             Название.
 * @property string                  $employerName      Заказчик (текстом)
 * @property Company                 $employer          Заказчик
 * @property string                  $employerId        Заказчик
 * @property integer                 $date_start        Дата начала реализации.
 * @property integer                 $date_end          Дата окончания реализации.
 * @property integer                 $status            Статус.
 * @property integer                 $statusCompleteId  Статус готовности.
 * @property integer                 $show_portfolio    Показывать в портфолио.
 * @property string                  $description       Описание.
 * @property FunctionsCompany[]      $functions         Функции объекта
 * @property Works[]                 $works             Группы работ
 * @property ObjectsCompletedWorks[] $completedWorks
 * @property Activities[]            $activities        Направление строительства
 * @property integer                 $created_at        Дата создания записи.
 * @property integer                 $updated_at        Дата изменения записи.
 */
class Objects extends ActiveRecord
{
    // Статусы готовности
    const STATUS_COMPLETE = 1;
    const STATUS_NON_COMPLETE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%objects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'user_id', 'status', 'date_start'], 'required'],
            [['company_id', 'user_id', 'status', 'status_complete_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['activitiesIds', 'functionsIds', 'typesIds', 'worksIds'], 'each', 'rule' => ['integer']],
            [['description'], 'safe']
        ]);
    }


    public function statusEndValidate()
    {
        if ($this->statusCompleteId == self::STATUS_COMPLETE && empty($this->dateEnd)) {
            $this->addError('date_end', 'Поле обязательно для заполнения');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'user_id' => Yii::t('app', 'Заказчик'),
            'address_id' => Yii::t('app', 'Адрес объекта'),
            'title' => Yii::t('app', 'Название'),
            'date_start' => Yii::t('app', 'Начало работ'),
            'date_end' => Yii::t('app', 'Окончание работ'),
            'show_portfolio' => Yii::t('app', 'Показывать в портфолио'),
            'status' => Yii::t('app', 'Статус'),
            'activitiesIds' => Yii::t('app', 'Направления строительства'),
            'status_complete_id' => Yii::t('app', 'Статус строительства'),
            'addressIndex' => Yii::t('app', 'Индекс'),
            'functionsIds' => Yii::t('app', 'Функция'),
            'typesIds' => Yii::t('app', 'Тип'),
            'worksIds' => Yii::t('app', 'Вид работ'),
            'description' => Yii::t('app', 'Описание работ'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * Список статусов
     *
     * @param null $id
     *
     * @return array
     */
    public static function getStatusComplete($id = null)
    {

        $list = [
            self::STATUS_COMPLETE => ['id' => self::STATUS_COMPLETE, 'title' => Yii::t('app', 'Завершен')],
            self::STATUS_NON_COMPLETE => ['id' => self::STATUS_NON_COMPLETE, 'title' => Yii::t('app', 'Строится')],

        ];
        return $id ? $list[$id]['title'] : $list;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'linkActivitiesBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'activities',
                    'relationReferenceAttribute' => 'activitiesIds',
                ],
                'linkFunctionsBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'functions',
                    'relationReferenceAttribute' => 'functionsIds',
                ],
                'linkTypesBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'types',
                    'relationReferenceAttribute' => 'typesIds',
                ],
                'linkWorksBehavior' => [
                    'class' => LinkManyBehavior::class,
                    'relation' => 'works',
                    'relationReferenceAttribute' => 'worksIds',
                ],
            ]
        );
    }

    /**
     * Связь с адресом
     *
     * @return ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(CompanyAddress::class, ['id' => 'address_id']);
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с пользователями
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'user_id']);
    }

    /**
     * Связь с завершенными работами
     *
     * @return ActiveQuery
     */
    public function getCompletedWorks()
    {
        return $this->hasMany(ObjectsCompletedWorks::class, ['object_id' => 'id']);
    }

    /**
     * Возврашает список направлений строительства
     *
     * @return ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activities::class, ['id' => 'activity_id'])->viaTable('{{%objects_activities}}', ['object_id' => 'id']);
    }

    /**
     * Возврашает список функций
     *
     * @return ActiveQuery
     */
    public function getFunctions()
    {
        return $this->hasMany(FunctionsCompany::class, ['id' => 'function_id'])->viaTable('{{%objects_functions}}', ['object_id' => 'id']);
    }

    /**
     * Возврашает список типов
     *
     * @return ActiveQuery
     */
    public function getTypes()
    {
        return $this->hasMany(Projects::class, ['id' => 'type_id'])->viaTable('{{%objects_type}}', ['object_id' => 'id']);
    }

    /**
     * Возврашает список работ
     *
     * @return ActiveQuery
     */
    public function getWorks()
    {
        return $this->hasMany(Works::class, ['id' => 'work_id'])->viaTable('{{%objects_works}}', ['object_id' => 'id']);
    }

    /**
     * связь с тендерами
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getTenders()
    {
        return $this->hasMany($this->getNeededClass('tenders'), ['object_id' => 'id']);
    }

    /**
     * Связь с ссылками объекта
     *
     * @return ActiveQuery
     */
    public function getLinks()
    {
        return $this->hasMany(ObjectsLinks::class, ['object_id' => 'id']);
    }

    /**
     * Выбор нужного типа, возвращаемых объектов.
     *
     * @param $relationName
     *
     * @return string
     * @throws InvalidConfigException
     */
    public function getNeededClass($relationName)
    {

        $currentClass = get_class($this); // имя текущего класса
        $backend = \common\modules\tender\models\backend\Objects::class;
        $frontend = \common\modules\tender\models\frontend\Objects::class;

        $map = [
            $backend => [
                'tenders' => \common\modules\tender\models\backend\Tenders::class,
            ],
            $frontend => [
                'tenders' => \common\modules\tender\models\frontend\Tenders::class,
            ],
        ];
        if (isset($map[$currentClass][$relationName]) ? false : true) {
            throw new InvalidConfigException('Неверное имя связи - ' . $relationName);
        }

        return $map[$currentClass][$relationName];
    }

    /**
     * Связь с тендерами
     *
     * @return ActiveQuery
     */
    public function getEmployer()
    {
        return $this->hasOne(Company::class, ['id' => 'employer_id']);
    }
}
