<?php
namespace common\modules\tender\models\common;

use modules\upload\behaviors\UploadBehavior;
use common\models\UserIdentity;
use common\modules\company\models\common\Company;
use common\modules\directories\models\common\FunctionsCompany;
use common\modules\offers\models\common\Offers;
use common\modules\tender\models\common\EstimatesElements;
use common\modules\directories\models\common\Currency;
use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Тендеры компании.
 *
 * @property integer $id                     Идентификатор.
 * @property integer $object_id              Объект.
 * @property integer $offer_id               Предложение подрядчика (победитель тендера).
 * @property integer $user_id                Пользователь.
 * @property string $title                   Название.
 * @property string $description             Краткое описание работ.
 * @property integer $type                   Тип тендера.
 * @property integer $mode                   Вид тендера.
 * @property integer $stage                  Этапность тендера.
 * @property integer $currency_id            Валюта.
 * @property integer $status                 Статус.
 * @property string $date_start              Дата начала тендера.
 * @property string $date_end                Дата окончания тендера.
 * @property string $date_offer              Дата выбора победителя.
 * @property integer $created_at             Дата создания записи.
 * @property integer $updated_at             Дата изменения записи.
 * @property string $files_store_link        Ссылка на внешнее файловое хранилище.
 * @property integer $function_contractor_id Функция подрядчика
 */
class Tenders extends ActiveRecord
{
    /**
     * Черновик
     */
    const STATUS_DRAFT = 0;
    /**
     * Сбор предложений
     */
    const STATUS_COLLECT_OFFERS = 1;
    /**
     * Выбор победителя
     */
    const STATUS_CHOICE_WINNER = 2;
    /**
     * Завершен
     */
    const STATUS_CLOSE = 3;

    /**
     * Возвращает список статусов тендера
     *
     * @param null|integer $statusId
     * @return array|mixed
     */
    public static function getStatuses($statusId = null)
    {
        $statusList = [
            self::STATUS_DRAFT => Yii::t('app', 'Черновик'),
            self::STATUS_COLLECT_OFFERS => Yii::t('app', 'Сбор предложений'),
            self::STATUS_CHOICE_WINNER => Yii::t('app', 'Выбор победителя'),
            self::STATUS_CLOSE => Yii::t('app', 'Завершен'),
        ];

        return isset($statusList[$statusId]) ? $statusList[$statusId] : $statusList;
    }

    /**
     * Типы тендера
     */
    const TYPE_REQUEST = 1;

    /**
     * Возвращает список типов тендеров
     *
     * @param null $typeId
     * @return array|mixed
     */
    public static function getTypes($typeId = null)
    {
        $types = [
            self::TYPE_REQUEST => Yii::t('app', 'Запрос предложений'),
        ];

        return !empty($typeId) && isset($types[$typeId]) ? $types[$typeId] : $types;
    }


    /**
     * Виды тендера
     */
    const MODE_OPEN = 1;

    /**
     * Возвращаеь списов видов тендеров
     *
     * @param null|integer $modeId
     * @return array|mixed
     */
    public static function getModes($modeId = null)
    {
        $modes = [
            self::MODE_OPEN => Yii::t('app', 'Открытый'),
        ];

        return !empty($modeId) && isset($modes[$modeId]) ? $modes[$modeId] : $modes;
    }

    /**
     * Этапность тендера
     */
    const STAGE_LEVEL_1 = 1;

    /**
     * Возвращает список этапностей тендера
     *
     * @param null|integer $stageId
     * @return array|mixed
     */
    public static function getStages($stageId = null)
    {
        $stages = [
            self::STAGE_LEVEL_1 => Yii::t('app', 'Одноэтапные тендеры'),
        ];

        return !empty($stageId) && isset($stages[$stageId]) ? $stages[$stageId] : $stages;
    }


    const DATE_FORMAT = 'php:Y-m-d H:i:s';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['object_id', 'company_id', 'user_id', 'status', 'type', 'mode', 'stage', 'currency_id', 'function_contractor_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'files_store_link'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['date_start', 'date_end'], 'date', 'format' => self::DATE_FORMAT],
            ['offer_id', 'required', 'when' => function($model) {
                return $model->status == self::STATUS_CLOSE;
            }],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Компания'),
            'object_id' => Yii::t('app', 'Объект'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'title' => Yii::t('app', 'Тендер'),
            'description' => Yii::t('app', 'Краткое описание работ'),
            'type' => Yii::t('app', 'Тип тендера'),
            'mode' => Yii::t('app', 'Вид тендера'),
            'stage' => Yii::t('app', 'Этапность тендера'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'status' => Yii::t('app', 'Статус'),
            'date_start' => Yii::t('app', 'Дата начала тендера'),
            'date_end' => Yii::t('app', 'Дата окончания тендера'),
            'offer_id' => Yii::t('app', 'Предложение подрядчика (победитель тендера)'),
            'date_offer' => Yii::t('app', 'Дата выбора победителя'),
            'files_store_link' => Yii::t('app', 'Ссылка на внешнее файловое хранилище'),
            'function_contractor_id' => Yii::t('app', 'Функция подрядчика'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'documents' => [
                    'class' => UploadBehavior::class,
                    'attributes' => [
                        'documents' => [
                            'singleFile' => false,
                            'rules' => [
                                'file' => [
                                    'extensions' => 'dwg,jpg,jpeg,png,doc,docx,xls,xlsx,ppt,pptx,pdf,3ds,ai,c4d,rar,zip,7z,gzip',
                                    'maxSize' => 10 * 1024 * 1024
                                ],
                            ],
                        ],
                    ]
                ],
            ]);
    }

    /**
     * Связь с объектом
     *
     * @return ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::class, ['id' => 'object_id']);
    }

    /**
     * Связь с функцией подрядчика (справочник "Функции компании")
     *
     * @return ActiveQuery
     */
    public function getFunctionContractor()
    {
        return $this->hasOne(FunctionsCompany::class, ['id' => 'function_contractor_id']);
    }

    /**
     * Связь с компаниями
     *
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Связь с пользователями
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'user_id']);
    }

    /**
     * Связь с валютой
     *
     * @return ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    /**
     * Связь с предлжениями подрядчика
     *
     *
     * @return ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offers::class, ['tender_id' => 'id']);
    }

    /**
     * Сметы тендера
     *
     * @return ActiveQuery
     */
    public function getEstimatesRubrics()
    {
        return $this->hasMany(EstimatesRubrics::class, ['tender_id' => 'id'])->where(['>', 'depth', 0])->orderBy(['lft' => SORT_ASC]);
    }

    /**
     * Все работы сметы тендера
     *
     * @return ActiveQuery
     */
    public function getWorks()
    {
        return $this->hasMany(EstimatesElements::class, ['tender_id' => 'id']);
    }

    /**
     * Связь с избранными тендерами
     *
     * @return ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasMany(UserFavoritesTenders::class, ['tender_id' => 'id']);
    }

    /**
     * Связь с работами
     *
     * @return ActiveQuery
     */
    public function getCharsWorks()
    {
        return $this->hasMany(TendersCharsWorks::class, ['tender_id' => 'id']);
    }

    /**
     * Работы тендера без групп
     *
     * @return ActiveQuery
     */
    public function getWorksNoRubrics()
    {
        return $this->getWorks()->where(['rubric_id' => null]);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->offer_id) && empty($this->date_offer)) {
            $this->date_offer = Yii::$app->formatter->asDate(date('Y-m-d H:i:s', time()), self::DATE_FORMAT);
        }

        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает имя и ссылку на файл архива документов тендера
     *
     * @return string
     */
    public function getZipFileArchiveLink()
    {
        return '/upload/tenders/files/' . $this->id . '/' . $this->id . '.zip';
    }
}
