<?php
namespace common\modules\tender\models\common;

use creocoder\nestedsets\NestedSetsQueryBehavior;

class TreeQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::class
        ];
    }
}