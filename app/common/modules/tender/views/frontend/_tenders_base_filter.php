<div class="search-sidebar js-search-sidebar">
    <?php
    $inputStyle = ['horizontalCssClasses' => ['wrapper' => '', 'label' => '']];
    $form = \yii\bootstrap\ActiveForm::begin([
        'action' => \yii\helpers\Url::to(['/tender/tenders/tender_list']),
        'method' => 'GET',
        'options' => [
            'id' => 'catalog-base-filter-form',
            'class' => 'search-sidebar__inner'
        ],
    ]);

    use kartik\depdrop\DepDrop;
    use yii\helpers\Url; ?>
        <div class="search-sidebar__head">
            <button class="search-sidebar__close js-open-sidebar-search" type="button">
                <svg class="icon icon-close ">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="../static/images/svg/spriteInline.svg#close"/>
                </svg>
            </button>
            <div class="search-sidebar__title">Уточнить поиск</div>
            <div class="search-sidebar__input">
                <div class="form-group  form-group--search ">

                    <?php $searchButton = '<button class="_btn" type="submit">
                                            <svg class="icon icon-search">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" 
                                                     xlink:href=" ' . Yii::getAlias('@static') .'/../static/images/svg/spriteInline.svg#search"/>
                                            </svg>
                                        </button>';

                    echo $form->field($model, 'inn', ['template' => $searchButton . '{input}',
                        'options' => ['class' => 'form-group  form-group--search']])
                        ->textInput(['placeholder' => Yii::t('app', 'Название компании, ИНН, ключевое слово')])
                        ->label('false')

                    ?>

                </div>
            </div>
        </div>

        <div class="search-sidebar__body js-custom-scroll">
            <div class="search-sidebar__body-inner">
                <div class="search-sidebar__section js-acco">
                    <div class="search-sidebar__section-head js-acco-title">
                        <label class="control-label advanced-search__caption"><?= Yii::t('app', 'Адрес компании') ?></label>
                    </div>
                    <div class="search-sidebar__section-body depdrop">
                        <div class="form-group form-group--autocomplete">
                            <?= $form->field($address, 'country_id', $inputStyle)->dropDownList(\common\models\GeoCountry::getCountryList(),
                                [
                                    'class' => '_select ',
                                    'data-result-hidden-input-name' => 'result-input',
                                    'data-empty-text' => 'Ничего не найдено',
                                    'placeholder' => '',
                                    'prompt' => 'Выберите страну'
                                ]) ?>
                        </div>
                        <div class="form-group form-group--autocomplete">
                            <?= $form->field($address, 'region_id', $inputStyle)

                                ->widget(DepDrop::class, [
                                    'data' => \common\models\GeoRegion::getRegionsList(),
                                    'options' => [
                                        'class' => '_select ',
                                        'data-result-hidden-input-name' => 'result-input',
                                        'data-empty-text' => 'Ничего не найдено',
                                        'prompt' => 'Выберите регион',
                                        'placeholder' => '',
                                    ],
                                    'pluginOptions' => [
                                        'depends' => ['companyaddress-country_id'],
                                        'placeholder' => 'Выберите регион',
                                        'url' => Url::to(['/tender/tenders/region-list']),
                                        'initialize' => true,
                                    ],
//                                    'pluginEvents' => [
//                                        "depdrop:afterChange" => "function(event, id, value) { $('select#companyaddress-region_id')[0].sumo.reload(); }",
//                                    ],
                        ])
//                                ->dropDownList([Yii::t('app', 'Выберите регион')] + \common\models\GeoRegion::getRegionsList(),
//                                    ['class' => '_select js-autocomplete-select',
//                                        'data-result-hidden-input-name' => 'result-input', 'data-empty-text' => 'Ничего не найдено', 'placeholder' => ''])
                            ?>
                        </div>
                        <div class="form-group form-group--autocomplete">
                            <?= $form->field($address, 'city_id', $inputStyle)
                                ->widget(DepDrop::class, [
                                    'data' => \common\models\GeoCity::getCityList(),
                                    'options' => [
                                        'class' => '_select ',
                                        'data-result-hidden-input-name' => 'result-input',
                                        'data-empty-text' => 'Ничего не найдено',
                                        'prompt' => 'Выберите город',
                                        'placeholder' => '',
                                        'data-search' => 'data-search',

                                    ],
                                    'pluginOptions' => [
                                        'depends' => ['companyaddress-region_id'],
                                        'placeholder' => 'Выберите город',
                                        'url' => Url::to(['/tender/tenders/city-list']),
                                        'initialize' => true,
                                    ],
//                                    'pluginEvents' => [
//                                        "depdrop:afterChange" => "function(event, id, value) { $('select#companyaddress-city_id')[0].sumo.reload(); }",
//                                    ],
                                ])
                            ?>
                        </div>
                    </div>
                </div>
        </div>

        <div class="search-sidebar__body js-custom-scroll">
            <div class="search-sidebar__body-inner">

                <div class="search-sidebar__section js-acco">
                    <div class="search-sidebar__section-head js-acco-title">
                        <?= Yii::t('app', 'Функции компании')?>
                    </div>
                    <div class="search-sidebar__section-body">
                        <?= $form->field($model, 'functionsFilter',
                            [
                                'template' => '{beginWrapper}{input}{endWrapper}',
                                'wrapperOptions' => ['class' => '_list']])
                            ->checkboxList(
                                \common\modules\directories\models\common\FunctionsCompany::findActiveList(),
                                [
                                    'tag' => false,
                                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                        $checked = $checked == 1 ? 'checked="checked"' : '';

                                        return "<label class='check-box'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> 
                                                <span class='check-label'>{$label}</span></label>";
                                    },
                                ])->label(false);
                        ?>
                    </div>
                </div>

                <div class="search-sidebar__section js-acco">
                    <div class="search-sidebar__section-head js-acco-title">
                        <?= Yii::t('app', 'Направления работ')?>
                    </div>
                    <div class="search-sidebar__section-body js-works-filter-tender">
                        <?= $form->field($model, 'typeWorkFilter', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                            ->dropDownList(\common\modules\directories\models\common\MainWork::findActiveList(),
                                [
                                    'multiple' => 'multiple', 'size' => 1, 'class' => 'js-works-select-filter-tender', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать', 'placeholder' => 'Выбрать']
                            )->label(false)->error(['tag' => 'div']) ?>
                    </div>
                </div>

                <div class="search-sidebar__section js-acco">
                    <div class="search-sidebar__section-head js-acco-title">
                        <?= Yii::t('app', 'Период тендера')?>
                    </div>
                    <div class="search-sidebar__section-body">
                        <div class="date-period">
                            <div class="date-period__item">
                                <div class="form-group form-group--date">
                                    <?= $form->field($model, 'dateFilterStart')
                                        ->textInput([
                                            'placeholder' => Yii::t('app', 'Выберите дату от'),
                                            'class' => 'form-control js-datepicker'
                                        ])
                                        ->label(false) ?>
                                </div>
                            </div>

                            <div class="date-period__item">
                                <div class="fform-group form-group--date">
                                    <?= $form->field($model, 'dateFilterEnd')
                                        ->textInput([
                                            'placeholder' => Yii::t('app', 'Выберите дату до'),
                                            'class' => 'form-control js-datepicker'
                                        ])
                                        ->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="search-sidebar__foot">
            <?= \yii\helpers\Html::submitInput(Yii::t('app', 'ПРИМЕНИТЬ'), ['class' => 'btn btn-primary btn-small']) ?>
            <a href="<?= \yii\helpers\Url::to(['/tenders/']) ?>" class="btn _btn btn-reset"><?= Yii::t('app', 'СБРОСИТЬ ВСЕ ФИЛЬТРЫ') ?></a>
        </div>
    <?php $form::end(); ?>
</div></div>
