<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 * @var string $tHash
 */

use common\modules\directories\models\common\FunctionsCompany;
use yii\helpers\ArrayHelper;

$loadPage = $dataProvider->pagination->page;

?>

<?php
$button = '';
if($dataProvider->pagination->pageCount != $dataProvider->pagination->page+1) {
    $button = '<button class="btn btn-more">'. Yii::t("app", "ПОКАЗАТЬ ЕЩЕ") .'</button>';
} ?>

<?php
$template = $isAjax ? '{items}' : '{items}
<div class="section__action load-more"  
data-page-count="' . $dataProvider->pagination->pageCount . '"  data-ajax-url="' . \yii\helpers\Url::current() . '" data-load-page="' . $loadPage  . '">
' . $button . '
</div>';

echo \modules\crud\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $template,
    'options' => ['class' => 'section__body'],
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'company-item',
    ],
    'itemView' => '_tenders_list_item',
    'viewParams' => [
        'tenderFavoritesId' => $tenderFavoritesId,
        'tHash' => $tHash
    ],
    // 'emptyText' => '<div class="filter-result__empty"><div class="_title">' . Yii::t('app', $emptyMessage) . '</div></div>',
    'pager' => false,
])
?>