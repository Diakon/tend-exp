<?php
use common\modules\tender\models\frontend\Tenders;
/**
 * @var $tenderFavoritesId array
 * @var Tenders $model
 * @var string $tHash
 */
$company = $model->object->company;
$canGetTender = Yii::$app->user->canGetTender($model);
$canGetFavorite = Yii::$app->user->canGetFavorite();
$builds = [];
foreach (\yii\helpers\ArrayHelper::map($company->getBuilds()->asArray()->all(), 'title', 'title') as $build) {
    $builds[] = trim($build);
}
?>

<div class="tender-item  tender-item--adaptiv">
    <div class="tender-item__inner">
        <div class="tender-item__head">
            <div class="tender-item__options">
                <div class="tender-item__option"><?= Yii::t('app', Tenders::getModes($model->mode) . ' тендер') ?> № <?= $model->id ?> до <?= Yii::$app->formatter->asDate($model->date_end, 'php:d.m.Y') ?></div>
                <div class="tender-item__option"><?= Yii::t('app', 'Тендер для') ?>: <span class="_inline"><?= $model->functionContractor->title ?? '' ?></span></div>
                <div class="tender-item__option"><?= Yii::t('app', 'Тип') ?>: <span class="_inline"><?= Yii::t('app', Tenders::getTypes($model->type)) ?></span></div>
            </div>

            <div class="tender-item__status">
                <div class="status-block ">
                    <div class="status-block__text">
                        <?= Tenders::getStatuses($model->status) ?>
                    </div>
                </div>
            </div>

            <div class="tender-item__head-action hidden-md hidden-lg">
                <button class="btn-add">
                    <svg class="icon icon-favorite ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#favorite"></use>
                    </svg>
                </button>
            </div>
        </div>

        <div class="tender-item__body">
            <div class="tender-item__body-left">
                <h2 class="tender-item__title">
                    <?php
                    $titleTender = $model->title;
                    $titleTender = \yii\helpers\Html::a($titleTender,
                        ['/dashboard/tender_offers_info/', 'tenderId' => $model->id]);
                    ?>
                    <?= $titleTender ?>
                </h2>
                <div class="tender-item__desc">
                    <?= $model->object->address->fullAddress ?? '' ?>

                    <span class="tenders-list-company-tender-title hidden-md hidden-xs"><?= Yii::t('app','Заказчик') ?>:</span> <span class="hidden-md hidden-xs"><?= $company->title ?? '' ?></span>
                </div>

            </div>

        </div>

        <div class="tender-item__foot">
            <div class="tender-item__options">
                <div class="tender-item__option hidden-lg">
                    <div class="_label"><?= Yii::t('app','Заказчик') ?>:</div>
                    <div class="_value"><?= $company->title ?? '' ?></div>
                </div>

                <div class="tender-item__option" style="min-width: 90%">
                    <div class="_label" style="min-width: 200px"><?= Yii::t('app','Направления строительства') ?>:</div>
                    <div class="_value"><?= implode('; ', \yii\helpers\ArrayHelper::map($model->object->getActivities()->asArray()->all(), 'title', 'title')) ?></div>
                </div>
                <!--
                <div class="tender-item__option">
                    <div class="_label"><?= Yii::t('app','Краткое описание работ') ?>:</div>
                    <div class="_value"><?= Yii::t('app', $model->description) ?></div>
                </div>
                -->
                <div class="tender-item__option">
                    <div class="_label"><?= Yii::t('app','Название проекта') ?>:</div>
                    <div class="_value"><?= Yii::t('app', $model->object->title) ?></div>
                </div>
            </div>


            <div class="tender-item__action">
                <?php if ($canGetTender) { ?>
                    <a href="<?= \yii\helpers\Url::to(['/dashboard/offer_add', 'tenderId' => $model->id,  'companyUrl' => Yii::$app->user->getActiveCompanyData()['url'] ?? '', 'tHash' => $tHash ?? '']) ?>" class="btn-add hidden-xs hidden-md">
                        <svg class="icon icon-check ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#check"></use>
                        </svg>
                    </a>
                <?php } ?>
                <!--
                <button class="btn-add hidden-xs hidden-md">
                    <svg class="icon icon-print ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#print"></use>
                    </svg>
                </button>
                -->
                <?php if ($canGetFavorite) { ?>
                    <button class="btn-add js-add-tender-to-favorites <?= in_array($model->id, $tenderFavoritesId) ? 'btn-add--active' : '' ?>" data-url="<?= \yii\helpers\Url::to() ?>" data-tender="<?= $model->id ?>">
                        <svg class="icon icon-favorite ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#favorite"></use>
                        </svg>
                    </button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>