<?php
/**
 * @var \common\modules\company\models\frontend\Company $model
 */
?>

<div class="popup popup--fill" data-popup-id="advanced-search" id="advanced-search" style="display: none;">
    <div class="popup__box">
        <?php
        $inputStyle = ['horizontalCssClasses' => ['wrapper' => '', 'label' => '']];
        $form = \yii\bootstrap\ActiveForm::begin([
                'action' => \yii\helpers\Url::to(['/tender/tenders/tender_list']),
                'method' => 'GET',
                'options' => ['id' => 'catalog-form'],
            ]); ?>
        <div class="popup__box-inner">
            <button class="popup__close js-close-wnd" type="button"></button>

            <div class="popup__title"><?= Yii::t('app', 'Расширенный поиск')?> </div>

            <div class="popup__body">
                <div class="advanced-search">
                    <div class="advanced-search__inner">
                        <div class="advanced-search__section">
                            <div class="advanced-search__body">

                                <?php $searchButton = '<button class="_btn" type="submit">
                                            <svg class="icon icon-search">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" 
                                                     xlink:href=" ' . Yii::getAlias('@static') .'/../static/images/svg/spriteInline.svg#search"/>
                                            </svg>
                                        </button>';

                                echo $form->field($model, 'inn', ['template' => $searchButton . '{input}',
                                    'options' => ['class' => 'form-group  form-group--search']])
                                    ->textInput(['placeholder' => Yii::t('app', 'Название компании, ИНН, название тендера')])
                                    ->label('false')

                                ?>

                            </div>
                        </div>

                        <div class="advanced-search__section">
                            <div class="advanced-search__head">
                                <div class="advanced-search__title"><?= Yii::t('app', 'Место проведения')?></div>
                            </div>
                            <div class="advanced-search__body">
                                <div class="advanced-search__row">
                                    <div class="advanced-search__col">
                                        <?= $form->field($address, 'country_id', $inputStyle)->dropDownList(\common\models\GeoCountry::getCountryList(),
                                            ['class' => 'js-dropdown-box', 'prompt' => 'Страна']) ?>
                                    </div>
                                    <div class="advanced-search__col form-group form-group--autocomplete">
                                        <?= $form->field($address, 'region_id', $inputStyle)
                                            ->dropDownList([Yii::t('app', 'Выберите регион')] + \common\models\GeoRegion::getRegionsList(),
                                                ['class' => '_select js-autocomplete-select', 'data-result-hidden-input-name' => 'result-input', 'data-empty-text' => 'Ничего не найдено', 'placeholder' => '']) ?>
                                    </div>
                                    <div class="advanced-search__col form-group form-group--autocomplete">
                                        <?= $form->field($address, 'city_id', $inputStyle)
                                            ->dropDownList([Yii::t('app', 'Выберите город')] + \common\models\GeoCity::getCityList(),
                                                ['class' => '_select js-autocomplete-select', 'data-result-hidden-input-name' => 'result-input', 'data-empty-text' => 'Ничего не найдено', 'placeholder' => '']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?= $form->field($model, 'functionsFilter', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                            ->dropDownList(\common\modules\directories\models\common\FunctionsCompany::findActiveList(),
                                [
                                    'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать', 'placeholder' => 'Выбрать']
                            )->label(Yii::t('app', 'Функции компании'))->error(['tag' => 'div']) ?>

                        <?= $form->field($model, 'buildsFilter', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                            ->dropDownList(\common\modules\directories\models\common\Activities::findActiveList(),
                                [
                                    'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать', 'placeholder' => 'Выбрать']
                            )->label(Yii::t('app', 'Направления строительства'))->error(['tag' => 'div']) ?>


                        <div class="js-works-filter-tender">
                            <?= $form->field($model, 'typeWorkFilter', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                                ->dropDownList(\common\modules\directories\models\common\MainWork::findActiveList(),
                                    [
                                        'multiple' => 'multiple', 'size' => 1, 'class' => 'js-works-select-filter-tender', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать', 'placeholder' => 'Выбрать']
                                )->label(Yii::t('app', 'Направления работ'))->error(['tag' => 'div']) ?>
                        </div>


                        <div class="form-group field-tenders-mode" style="border-bottom: 0px solid; margin-top: 20px;">
                            <label class="control-label advanced-search__caption js-acco-title" style="cursor: pointer;">
                                <?= Yii::t('app', 'Работы')?>
                            </label>
                            <div class="search-sidebar__section-body">
                                <?php
                                $limit = 15;
                                $i = 0;
                                ?>
                                <?php foreach (\common\modules\directories\models\common\MainWork::findActiveList() as $mainWorkId => $mainWorkTitle) { ?>
                                    <?php
                                    $listTypesWorks = \common\modules\directories\models\common\TypesWork::findActiveList('id', 'title', $mainWorkId);
                                    if (empty($listTypesWorks)) {
                                        continue;
                                    }
                                    ?>
                                    <div class="js-type-works-filter-tender js-type-works-filter-tender-id-<?= $mainWorkId ?>" style="padding-bottom: 10px;">
                                        <h4 style="padding-bottom: 10px"><?= Yii::t('app', $mainWorkTitle) ?></h4>
                                        <?php foreach ($listTypesWorks as $typeWorkId => $typeWorkTitle) { ?>
                                            <?php
                                            $listWorks = \common\modules\directories\models\common\Works::findActiveList('id', 'title', $typeWorkId);
                                            if (empty($listWorks)) {
                                                continue;
                                            }
                                            ?>
                                            <?= $form->field($model, 'worksFilter',
                                                [
                                                    'template' => '{beginWrapper}{input}{endWrapper}',
                                                    'wrapperOptions' => ['class' => '_list']])
                                                ->checkboxList(
                                                    $listWorks,
                                                    [
                                                        'tag' => false,
                                                        'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                                            $checked = $checked == 1 ? 'checked="checked"' : '';

                                                            return "<label class='check-box'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> 
                                                        <span class='check-label'>{$label}</span></label>";
                                                        },
                                                    ])->label(false);
                                            ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>


                        <?= $form->field($model, 'objectTypeFilter', ['template' => '{label}<div data-placeholder="Выбрать" class="js-multi-select">{input}{error}</div>'])
                            ->dropDownList(\common\modules\directories\models\common\Projects::findActiveList(),
                                [
                                    'multiple' => 'multiple', 'size' => 1, 'class' => '', 'hidden' => 'hidden', 'data-placeholder' => 'Выбрать', 'placeholder' => 'Выбрать']
                            )->label(Yii::t('app', 'Тип объекта'))->error(['tag' => 'div']) ?>

                        <div class="advanced-search__section">
                            <div class="advanced-search__head">
                                <div class="advanced-search__title"><?= Yii::t('app', 'Период тендера') ?></div>
                            </div>
                            <div class="advanced-search__body">
                                <div class="date-period">
                                    <div class="date-period__item">
                                        <div class="form-group form-group--date">
                                            <?= $form->field($model, 'dateFilterStart')
                                                ->textInput([
                                                    'placeholder' => Yii::t('app', 'Выберите дату от'),
                                                    'class' => 'form-control js-datepicker'
                                                ])
                                                ->label(false) ?>
                                        </div>
                                    </div>

                                    <div class="date-period__item">
                                        <div class="fform-group form-group--date">
                                            <?= $form->field($model, 'dateFilterEnd')
                                                ->textInput([
                                                    'placeholder' => Yii::t('app', 'Выберите дату до'),
                                                    'class' => 'form-control js-datepicker'
                                                ])
                                                ->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <?= $form->field($model, 'type',
                            ['horizontalCssClasses' => ['wrapper' => 'advanced-search__fields advanced-search__fields--type-3']])
                            ->checkboxList(
                                \common\modules\tender\models\frontend\Tenders::getTypes(),
                                [
                                    'tag' => false,
                                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                        $checked = $checked == 1 ? 'checked="checked"' : '';

                                        return "<label class='check-box'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> 
                                                <span class='check-label'>{$label}</span></label>";
                                    },
                                ])->label(Yii::t('app', 'Тип тендера'), ['class' => 'control-label advanced-search__caption']);
                        ?>

                        <?= $form->field($model, 'mode',
                            ['horizontalCssClasses' => ['wrapper' => 'advanced-search__fields advanced-search__fields--type-2']])
                            ->checkboxList(
                                \common\modules\tender\models\frontend\Tenders::getModes(),
                                [
                                    'tag' => false,
                                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                        $checked = $checked == 1 ? 'checked="checked"' : '';

                                        return "<label class='check-box'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> 
                                                <span class='check-label'>{$label}</span></label>";
                                    },
                                ])->label(Yii::t('app', 'Вид тендера'), ['class' => 'control-label advanced-search__caption']);
                        ?>
                    </div>
                </div>
            </div>

            <div class="popup__footer">
                <div class="popup__action">
                    <?= \yii\helpers\Html::submitInput(Yii::t('app', 'ПРИМЕНИТЬ'), ['class' => 'btn btn-primary ']) ?>
                    <a href="<?= \yii\helpers\Url::to(['/tenders/']) ?>"
                       class="btn _btn btn-reset"
                    >
                        <?= Yii::t('app', 'СБРОСИТЬ ВСЕ ФИЛЬТРЫ') ?>
                    </a>
                </div>
            </div>
        </div>

        <?php $form::end(); ?>
    </div>