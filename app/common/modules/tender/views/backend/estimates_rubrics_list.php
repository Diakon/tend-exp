<?php
/**
 * @var View                                                    $this
 * @var \common\modules\tender\models\backend\EstimatesRubrics  $model
 * @var ActiveDataProvider                                      $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>
<?= $this->render('_filter_rubrics_form', ['model' => $model]) ?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        [
                            'label' => 'Смета',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $title = $data->title . ' [ компания: ' . $data->tender->object->company->title . ', тендер: ' . $data->tender->title . ']';
                                return str_repeat('&nbsp;&nbsp;&nbsp;', $data->depth) . $title;
                            }
                        ],
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
