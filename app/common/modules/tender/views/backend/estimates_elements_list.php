<?php
/**
 * @var View                                                    $this
 * @var \common\modules\tender\models\backend\EstimatesElements $model
 * @var ActiveDataProvider                                      $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;
use common\modules\tender\models\backend\EstimatesElements;

?>
<?= $this->render('_filter_elements_form', ['model' => $model]) ?>
<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'title',
                        [
                            'label' => 'Единица измерения',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return EstimatesElements::getUnits($data->unit_id) ?? '';
                            }
                        ],
                        'count',
                        'rubric.title',
                        'tender.object.company.title',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
