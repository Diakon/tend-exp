<?php
/**
 * @var View                                            $this
 * @var \common\modules\tender\models\backend\Objects   $model
 * @var ActiveDataProvider                              $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'title',
                        'company.title',
                        'user.email',
                        'date_start:date',
                        'date_end:date',
                        'status:status',
                        'show_portfolio:status',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
