<?php
namespace common\modules\tender\controllers\actions;

use modules\crud\actions\CreateAction;;
use common\modules\tender\widgets\FormView;

/**
 * Class EstimatesRubricsEdit
 * @package common\modules\tender\controllers\actions
 */
class EstimatesRubricsEdit extends CreateAction
{
    /**
     * @param bool $partial
     * @return string
     */
    public function run($partial = false)
    {
        $data = FormView::widget(['params' => $this->params, 'isNestedTree' => true]);
        return $this->renderAction('//content', ['content' => $data], $partial);
    }
}
