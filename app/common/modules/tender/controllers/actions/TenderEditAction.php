<?php
namespace common\modules\tender\controllers\actions;

use modules\crud\actions\BaseAction;
use common\components\ClassMap;
use common\modules\company\models\frontend\CompanyUserAccess;
use common\modules\company\models\frontend\CompanyUsers;
use common\modules\tender\models\frontend\EstimatesElements;
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\Objects;
use common\modules\tender\models\frontend\Tenders;
use frontend\models\EntityComment;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class TenderEditAction
 * @package common\modules\tender\controllers\actions
 */
class TenderEditAction extends BaseAction
{
    /**
     * @var integer
     */
    private $tenderId;

    /**
     * @var integer
     */
    private $objectId;

    /**
     * Редактирование тендер.
     */
    public function run()
    {
        $this->controller->layout = $this->layout;

        $this->tenderId = Yii::$app->request->get('id');
        $this->objectId = Yii::$app->request->get('objectId');
        $companyUrl = Yii::$app->request->get('companyUrl');
        $tHash = Yii::$app->request->get('tHash');
        $company = Yii::$app->user->getCompany($companyUrl)->one();

        /**
         * @var Tenders $model
         */
        if (!empty($this->tenderId)) {
            $model = Tenders::find()->where(['id' => (int)$this->tenderId])->one();
        } else {
            $model = new $this->params['model'];
            $model->date_start = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
            $model->company_id = $company->id;
            $model->user_id = Yii::$app->user->id;
            $model->status = Tenders::STATUS_INACTIVE;
        }

        $model->setScenario(Tenders::SCENARIO_CREATE_TENDER);

        $user = $company->companyUser;
        if (empty($model) || empty($company)) {
            throw new NotFoundHttpException();
        }

        $model->prepareModelAction($tHash);
        // Установка временного хеша модели

        $model->tHash = $model->isNewRecord && !empty($tHash) && empty($model->id) ? $tHash : $model->id;
        $isNewRecord = $model->isNewRecord;

        $model->createObjectStatus = !empty($model->createObjectStatus) ? $model->createObjectStatus : Objects::STATUS_NON_COMPLETE;

        $objects = Objects::getActive()->where(['company_id' => $model->company->id])->andWhere(['status' => Objects::STATUS_ACTIVE]);
        if (!in_array($user->type_role, [CompanyUsers::TYPE_ROLE_ADMIN])) {
            $objectsIds = ArrayHelper::map(CompanyUserAccess::find()->where(['user_id' => $user->user_id])->andWhere(['class_name' => ClassMap::CLASS_OBJECTS_ID])->asArray()->all(), 'class_id', 'class_id');
            $objects->andWhere(['in', 'id', $objectsIds]);
        }
        $objects = ArrayHelper::map($objects->asArray()->all(), 'id', 'title');

        if (Yii::$app->request->isAjax) {
            if (!empty(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                // Операции с рубриками смет
                if (!empty(Yii::$app->request->post('EstimatesRubrics'))) {
                    $estimatesRubricsParams = Yii::$app->request->post('EstimatesRubrics');
                    $result = $this->processEstimatesRubrics($estimatesRubricsParams);
                }
                // Операции с элементами (работами) рубрик смет
                if (!empty(Yii::$app->request->post('EstimatesElements'))) {
                    $estimatesElementsParams = Yii::$app->request->post('EstimatesElements');
                    $result = $this->processEstimatesElements($estimatesElementsParams);
                }
                // Комантарий для рубрик смет
                if (!empty(Yii::$app->request->post('EstimatesRubricsComment'))) {
                    $commentParams = Yii::$app->request->post('EstimatesRubricsComment');
                    $commentParams['companyId'] = $company->id;
                    $result = $this->commentRubrics($commentParams);
                }
                Yii::$app->response->statusCode = $statusCode = !empty($result) ? 200 : 500;
                echo Json::encode(['status' => $statusCode, 'result' => $result ?? []]);

                exit();
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->saveFrontTender()) {
                // Событие после сохранения модели
                $event = new Event();
                $event->sender = $model;
                $model->trigger($model::EVENT_AFTER_MAIN_ACTION_COMMIT, $event);
                Yii::$app->getResponse()->redirect(['/dashboard/tenders', 'companyUrl' => $companyUrl]);
            }
        }

        $canEditTender = $isNewRecord || ($model->company->id == $company->id && $model->status != Tenders::STATUS_ACTIVE) ? true : false;

        return $this->controller->render($this->params['template'], [
            'model' => $model,
            'user' => $user,
            'tHash' => $tHash,
            'company' => $company,
            'isNewRecord' => $isNewRecord,
            'objects' => $objects,
            'canEditTender' => $canEditTender,
            'typesWorkList' => \common\modules\directories\models\common\TypesWork::findActive(true)->asArray()->all(),
            'workList' => \common\modules\directories\models\common\Works::findActive(true)->asArray()->all(),
        ]);
    }

    /**
     * Создает коментарий к смете тендера
     *
     * @param array $commentParams
     * @return bool
     */
    private function commentRubrics($commentParams)
    {
        $model = EstimatesRubrics::find()->where(['id' => $commentParams['id']])->one();
        return EntityComment::addComment($model, $commentParams['comment']);
    }

    /**
     * Выполняет операции с работами смет (создание, обновление, перемещнение, удаление)
     *
     * @param array $estimatesElementsParams
     * @return bool
     */
    private function  processEstimatesElements($estimatesElementsParams)
    {
        /**
         * @var $model EstimatesElements;
         */
        switch ($estimatesElementsParams['type']) {
            case 'addWork':
                // Создаем работу
                $paramModel = $estimatesElementsParams['model'] ?? [];
                unset($paramModel['id']);
                $rubricId = !empty($estimatesElementsParams['rubricId']) ? (int)$estimatesElementsParams['rubricId'] : null;
                $model = new EstimatesElements();
                foreach ($paramModel as $key => $value) {
                    $model->{$key} = $value;
                }
                $model->rubric_id = $rubricId;
                $model->tender_id = (int)$this->tenderId;

                $result = $model->saveWork();
                break;
            case 'saveWork':
                // Обновить работу
                $paramModel = $estimatesElementsParams['model'] ?? [];
                $rubricId = !empty($estimatesElementsParams['rubricId']) ? (int)$estimatesElementsParams['rubricId'] : null;
                $model = EstimatesElements::find()->where(['id' => (int)$paramModel['id']])->andWhere(['tender_id' => (int)$this->tenderId])->one();
                if (!empty($model)) {
                    unset($paramModel['id']);
                    foreach ($paramModel as $key => $value) {
                        $model->{$key} = $value;
                    }
                    $model->rubric_id = $rubricId;
                    $result = $model->saveWork();
                }

                break;
            case 'deleteWork':
                // Удалить работу
                $paramModel = $estimatesElementsParams['model'] ?? [];
                $rubricId = !empty($estimatesElementsParams['rubricId']) ? (int)$estimatesElementsParams['rubricId'] : null;
                $model = EstimatesElements::find()->where(['id' => (int)$paramModel['id']])->andWhere(['tender_id' => (int)$this->tenderId])->one();
                if (!empty($model)) {
                    $result = $model->delete();
                }

                break;
        }

        return !empty($result) ? true : false;
    }

    /**
     * Выполняет операции с рубриками смет (создание, обновление, перемещнение, удаление)
     *
     * @param array $estimatesRubricsParams
     * @return bool|object
     */
    private function  processEstimatesRubrics($estimatesRubricsParams)
    {
        $estimatesRubric = null;
        switch ($estimatesRubricsParams['type']) {
            case 'addParent':
                // Добавляем новую родительскую рубрику
                $estimatesRubric = new EstimatesRubrics();
                $estimatesRubric->tender_id = (int)$this->tenderId;
                $estimatesRubric->parent_id = null;
                $result = $estimatesRubric->saveNode();

                break;
            case 'up':
                // Добавляем рубрику выше записи с указанным ID на том же уровне
                $estimatesRubric = new EstimatesRubrics();
                $estimatesRubric->tender_id = (int)$this->tenderId;
                $result = $estimatesRubric->insertNodeBefore($estimatesRubricsParams['rubricId']);

                break;
            case 'down':
                // Добавляем рубрику ниже записи с указанным ID на том же уровне
                $estimatesRubric = new EstimatesRubrics();
                $estimatesRubric->tender_id = (int)$this->tenderId;
                $result = $estimatesRubric->insertNodeAfter($estimatesRubricsParams['rubricId']);

                break;
            case 'delete':
                // Удаляем рубрику с указанным ID
                $estimatesRubric = EstimatesRubrics::find()
                    ->where(['id' => (int)$estimatesRubricsParams['rubricId']])
                    ->one();
                if (empty($estimatesRubric)) {
                    $result = false;
                } else {
                    $result = $estimatesRubric->deleteNode();
                }

                break;
            case 'addChildren':
                // Добавляем дочернюю рубрику для записи с указанным ID (если ID не указан - значит это запись 1ого уровня для тендера - нет родительских записей)
                $estimatesRubricId = !empty($estimatesRubricsParams['rubricId']) ? (int)$estimatesRubricsParams['rubricId'] : null;
                $estimatesRubric = new EstimatesRubrics();
                $estimatesRubric->tender_id = (int)$this->tenderId;
                $estimatesRubric->parent_id = $estimatesRubricId;
                $result = $estimatesRubric->saveNode();

                break;
            case 'edit':
                // Редактирование рубрки сметы с указанным ID
                $result = 500;
                $estimatesRubricId = !empty($estimatesRubricsParams['rubricId']) ? (int)$estimatesRubricsParams['rubricId'] : null;
                $estimatesRubric = EstimatesRubrics::find()
                    ->where(['id' => $estimatesRubricId])
                    ->andWhere(['tender_id' => (int)$this->tenderId])
                    ->one();
                if (!empty($estimatesRubric)) {
                    if (!empty($estimatesRubricsParams['params']) && is_array($estimatesRubricsParams['params'])) {
                        foreach ($estimatesRubricsParams['params'] as $key => $value) {
                            $estimatesRubric->{$key} = $value;
                        }
                    }
                    $estimatesRubric->tender_id = (int)$this->tenderId;
                    $result = $estimatesRubric->updateNode();
                }

                break;
        }

        return $estimatesRubric;
    }

}
