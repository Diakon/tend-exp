<?php
namespace common\modules\tender\controllers\actions;

use helpers\Dev;
use modules\crud\actions\ListAction;
use common\modules\company\models\frontend\Company;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class TendersAction
 * @package common\modules\tender\controllers\actions
 */
class TendersAction extends ListAction
{
    /**
     * @var string
     */
    public $layout = '@root/themes/frontend/views/layouts/inner';

    /**
     * @inheritdoc
     *
     * @throws NotFoundHttpException Not found http exception
     */
    public function run()
    {
        $model = new Company();
        $this->controller->layout = $this->layout;
        $dataProvider = $model->searchItems(Yii::$app->request);

        $isAjax = Yii::$app->request->isAjax;
        $page = intval(Yii::$app->request->post('loadPage', null));
        if ($isAjax) {
            $dataProvider->setPagination(['pageSize' => 1, 'pageSizeLimit' => [1, $dataProvider->totalCount], 'totalCount' => $dataProvider->totalCount]);
            $dataProvider->pagination->setPage($page);

            if ($page >= $dataProvider->pagination->pageCount) { // Если нет данных
                $dataProvider->pagination->setPage($page-1);
                return Json::encode(['data' => '',
                    'pagination' => $this->controller->renderAjax('/frontend/_companies_pagination', ['dataProvider' => $dataProvider ]),
                    'page' => $page,]);
            }
        }
        $title = Yii::t('app', 'Каталог компаний');
        $this->controller->view->params['breadcrumbs'][] = ['encode' => false, 'label' => '<span class="breadcrumbs__label">' . $title . '</span>'];
        $this->controller->view->title = $title;

        $view = $isAjax ? '/frontend/_companies_list_inner' : $this->params['template'];
        if ($isAjax) {
            return Json::encode([
                'data' => $this->controller->renderAjax($view,
                    [
                        'model' => $model,
                        'isAjax' => $isAjax,
                        'address' => $model->addressFilter,
                        'dataProvider' => $dataProvider,
                        'page' => $page,
                    ]),
                'pagination' => $this->controller->renderAjax('/frontend/_companies_pagination',
                    ['dataProvider' => $dataProvider,]),
            ]);
        }
        return $this->controller->render(
            $view, [
                'model' => $model,
                'isAjax' => $isAjax,
                'address' => $model->addressFilter,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
