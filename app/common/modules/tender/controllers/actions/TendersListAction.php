<?php
namespace common\modules\tender\controllers\actions;


use modules\crud\actions\ListAction;
use common\modules\tender\models\common\UserFavoritesTenders;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;


/**
 * Class ObjectsList.
 * Выводит список объектов размещеных на портале
 *
 * @package frontend\controllers\actions
 */
class TendersListAction extends ListAction
{
    /**
     * @var string
     */
    public $layout = '@root/themes/frontend/views/layouts/inner';

    /**
     * @inheritdoc
     *
     * @throws NotFoundHttpException Not found http exception
     */
    public function run()
    {
        // Получаю ID тендеров, которые были добавлены в избранное пользователем
        $tenderFavoritesId = Yii::$app->user->isGuest
            ? []
            : ArrayHelper::map(UserFavoritesTenders::find()->where(['user_id' => Yii::$app->user->id])->asArray()->all(), 'tender_id', 'tender_id');

        $model = new Tenders();
        $this->controller->layout = $this->layout;
        $dataProvider = $model->searchItems(Yii::$app->request, false, null, 'tendersList');

        // Для возможности сделать предложение к тендеру генерирую хеш
        $offer = new \common\modules\offers\models\frontend\Offers();
        $tHash = $offer->getTHash();

        $isAjax = Yii::$app->request->isAjax;
        $page = intval(Yii::$app->request->post('loadPage', null));
        if ($isAjax) {
            // Если пришел запрос добавления тендера в избранное - добавляем и выходим
            if (!empty(Yii::$app->request->post('tenderFavorites')) && !Yii::$app->user->isGuest) {
                $this->tenderFavorites(Yii::$app->request->post('tenderFavorites'));
                return Json::encode([
                    'status' => 200
                ]);
            }

            $dataProvider->setPagination(['pageSize' => YII_ENV == 'dev' ? 10 : 10, 'pageSizeLimit' => [1, $dataProvider->totalCount], 'totalCount' => $dataProvider->totalCount]);
            $dataProvider->pagination->setPage($page);

            if ($page >= $dataProvider->pagination->pageCount) { // Если нет данных
                $dataProvider->pagination->setPage($page-1);
                return Json::encode(['data' => '',
                    'pagination' => $this->controller->renderAjax('/frontend/_tenders_pagination', ['dataProvider' => $dataProvider ]),
                    'page' => $page,
                    ]);
            }
        }
        $title = Yii::t('app', 'Каталог тендеров');
        $this->controller->view->params['breadcrumbs'][] = ['encode' => false, 'label' => '<span class="breadcrumbs__label">' . $title . '</span>'];
        $this->controller->view->title = $title;

        $view = $isAjax ? '/frontend/_tenders_list_inner' : $this->template;

        if ($isAjax) {
            return Json::encode([
                'data' => $this->controller->renderAjax($view,
                    [
                        'model' => $model,
                        'isAjax' => $isAjax,
                        'dataProvider' => $dataProvider,
                        'page' => $page,
                        'address' => $model->addressFilter,
                        'tenderFavoritesId' => $tenderFavoritesId,
                        'tHash' => $tHash
                    ]),
                'pageCount' => $dataProvider->pagination->page+1,
                'pagination' => $this->controller->renderAjax('/frontend/_tenders_pagination',
                    ['dataProvider' => $dataProvider,]),
            ]);
        }

        return $this->controller->render(
            $view, [
                'model' => $model,
                'isAjax' => $isAjax,
                'dataProvider' => $dataProvider,
                'address' => $model->addressFilter,
                'tenderFavoritesId' => $tenderFavoritesId,
                'tHash' => $tHash
            ]
        );
    }

    /**
     * Добавляет / убирает тендер из избранного
     *
     * @param $tenderId
     */
    private function tenderFavorites($tenderId)
    {
        $model = UserFavoritesTenders::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['tender_id' => $tenderId])->one();
        // Тендера нет в избраном - добавляю
        if (empty($model)) {
            $model = new UserFavoritesTenders();
            $model->user_id = Yii::$app->user->id;
            $model->tender_id = $tenderId;
            $model->save();
        } else {
            // Тендер есть в избранном - убираю, т.к. повторно кликнули на кнопку "добавить в избранное"
            $model->delete();
        }
    }
}
