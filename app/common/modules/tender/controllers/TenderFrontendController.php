<?php
namespace common\modules\tender\controllers;

use common\modules\geo\models\common\GeoCity;
use common\modules\geo\models\common\GeoRegion;
use common\modules\tender\controllers\actions\TendersListAction;
use frontend\controllers\FrontendController;
use kartik\depdrop\DepDropAction;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


/**
 * Основной контроллер.
 */
class TenderFrontendController extends FrontendController
{
    public function init() {

        parent::init();
        $this->layout = '@root/themes/frontend/views/layouts/inner';
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'tender_list' => [
                'class' => TendersListAction::class,
                'template' => '/frontend/tenders_list'
            ],
            'region-list' => [
                'class' => DepDropAction::class,
                'outputCallback' => function ($selectedId, $params) {
                    $data = $selectedId == 1 ? \common\models\GeoRegion::find()->select(['id', 'name'])->select(['id', 'name'])
                        ->orderBy(['name' => SORT_ASC])->asArray()->all() : null;
                    return $data;
                },
            ],
            'city-list' => [
                'class' => DepDropAction::class,
                'outputCallback' => function ($selectedId, $params) {
                    return \common\models\GeoCity::find()->select(['id', 'name'])->select(['id', 'name'])
                        ->orderBy(['name' => SORT_ASC])->where(['region_id' => $selectedId])->asArray()->all();
                },
            ],
        ]);
    }

}
