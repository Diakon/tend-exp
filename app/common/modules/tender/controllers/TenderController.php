<?php
namespace common\modules\tender\controllers;

use modules\crud\actions\DeleteAction;
use modules\crud\actions\ListAction;
use backend\controllers\BackendController;
use modules\crud\actions\CreateAction;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\Json;
use common\modules\tender\controllers\actions\EstimatesRubricsEdit;
use common\modules\tender\models\backend\Objects;
use common\modules\tender\models\backend\Tenders;
use common\modules\tender\models\backend\EstimatesElements;
use common\modules\tender\models\backend\EstimatesRubrics;


/**
 * Основной контроллер.
 */
class TenderController extends BackendController
{
    /**
     * @var string|boolean the name of the layout to be applied to this controller's views.
     */
    public $layout = '@root/themes/backend/views/layouts/main';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'objects_list',
                            'objects_update',
                            'tenders_list',
                            'tenders_update',
                            'estimates_rubrics_list',
                            'estimates_rubrics_update',
                            'estimates_elements_list',
                            'estimates_elements_update',
                        ],
                        'roles' => ['administrator'],
                    ],

                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'objects_list' => [
                'class' => ListAction::class,
                'template' => '/backend/objects_list',
                'model' => Objects::class,
            ],
            'objects_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Objects::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Objects::classNameShort('id') . '_edit',
                ],
            ],
            'objects_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Objects::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Objects::classNameShort('id') . '_create',
                ],
            ],
            'objects_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => Objects::class,
                    'returnUrl' => ['backend/objects'],
                ],
            ],
            'tenders_list' => [
                'class' => ListAction::class,
                'template' => '/backend/tenders_list',
                'model' => Tenders::class,
            ],
            'tenders_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Tenders::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Tenders::classNameShort('id') . '_edit',
                ],
            ],
            'tenders_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Tenders::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . Tenders::classNameShort('id') . '_create',
                ],
            ],
            'tenders_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => Tenders::class,
                    'returnUrl' => ['backend/tenders_list'],
                ],
            ],
            'estimates_rubrics_list' => [
                'class' => ListAction::class,
                'template' => '/backend/estimates_rubrics_list',
                'model' => EstimatesRubrics::class,
            ],
            'estimates_rubrics_update' => [
                'class' => EstimatesRubricsEdit::class,
                'params' => [
                    'model' => EstimatesRubrics::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . EstimatesRubrics::classNameShort('id') . '_edit',
                ],
            ],
            'estimates_rubrics_create' => [
                'class' => EstimatesRubricsEdit::class,
                'params' => [
                    'model' => EstimatesRubrics::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . EstimatesRubrics::classNameShort('id') . '_create',
                ],
            ],
            'estimates_rubrics_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => EstimatesRubrics::class,
                    'returnUrl' => ['backend/estimates_rubrics_list'],
                ],
            ],
            'estimates_elements_list' => [
                'class' => ListAction::class,
                'template' => '/backend/estimates_elements_list',
                'model' => EstimatesElements::class,
            ],
            'estimates_elements_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => EstimatesElements::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . EstimatesElements::classNameShort('id') . '_edit',
                ],
            ],
            'estimates_elements_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => EstimatesElements::class,
                    'template' => '@views/../modules/crud/create',
                    'applyUrl' => '/' . EstimatesElements::classNameShort('id') . '_create',
                ],
            ],
            'estimates_elements_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => EstimatesElements::class,
                    'returnUrl' => ['backend/estimates_elements_list'],
                ],
            ],
        ];
    }

    /**
     * Обработка системных ошибок.
     *
     * @return string
     */
    public function actionError()
    {
        $this->layout = '@cms/themes/backend/views/layouts/base';
        /** @var \yii\base\Exception $exception */
        $exception = Yii::$app->errorHandler->exception;
        $handler = Yii::$app->errorHandler;

        $data = [];
        $data['name'] = $exception->getName();
        $data['message'] = $exception->getMessage();
        $data['line'] = $exception->getLine();
        $data['file'] = $exception->getFile();
        $data['trace'] = $exception->getTraceAsString();
        $data['code'] = $exception->getCode();

        $code = $exception instanceof \yii\web\HttpException ? $exception->statusCode : $exception->getCode();
        $message = $exception->getMessage();


        if (Yii::$app->request->isAjax) {
            return print_r($data, true);
        } else {
            $this->view->title = $code . ' ' . $exception->getMessage();
            $params = compact('exception', 'code', 'message','handler');
            Yii::error(Json::encode($data), 'error');

            return $this->render('@cms/themes/backend/views/backend/error', $params);
        }
    }
}
