<?php
namespace common\modules\tender\widgets;

use modules\crud\models\ActiveRecord;
use Yii;

class FormView extends \modules\crud\widgets\FormView
{
    public $isNestedTree = false;

    /**
     * Виджет вывода / сохранения записи в админке.
     * Отличается от стандартного биновского тем, что может сохранять записи дерева
     *
     * @return string
     */
    public function run()
    {
        /** @var ActiveRecord $className */
        $className = (new \ReflectionClass($this->params['model']))->getShortName();
        /** @var ActiveRecord $className */
        $classNameFull = $this->params['model'];

        $view = $this->getView();

        if ($classNameFull instanceof ActiveRecord) {
            $model = $classNameFull;
        } else {
            $modelId = !empty($this->params['id']) ? $this->params['id'] : Yii::$app->request->get($classNameFull::$paramIdent, 0);
            /** @var ActiveRecord $model */
            $model = empty($modelId) ? new $this->params['model'] : $classNameFull::findOne([$classNameFull::$paramColumn => $modelId]);

            // Если модель не найдена создаем новую
            if (is_null($model)) {
                $model = new $this->params['model'];
            }
        }

        if ($model->isNewRecord) {
            $model->loadDefaultValues();
        }

        // Устанавливаем scenario если он существует
        if (isset($this->params['scenario']) && !empty($this->params['scenario'])) {
            $model->setScenario($this->params['scenario']);
        }

        // Вызываем параметр callbackModel если он существует
        if (isset($this->params['callbackModel']) && $this->params['callbackModel'] instanceof \Closure) {
            $model = call_user_func($this->params['callbackModel'], $model);
        }

        $this->checkModel($model);

        if (!empty($_GET[$className])) {
            foreach ($_GET[$className] as $key => $value) {
                $model->{$key} = $value;
            }
        }

        // Устанавливаем attributes если они существует
        if (!empty($this->params['attributes'])) {
            $model->attributes = $this->params['attributes'];
        }
        // Устанавливаем заголовок
        $view->title = $model->isNewRecord ? Yii::t('cms', 'Create new element') : Yii::t('cms', 'Update - {title}', ['title' => (!empty($model->title) ? $model->title : $model::classNameShort() . ' - ' . $model->id)]);

        // Устанавливаем добавочный класс для wrapper container
        $view->params['wrapperContentAdditionalClass'] = 'animated fadeInRight ecommerce';
        // Устанавливаем добавочный класс для main container
        $view->params['mainContainerClass'] = 'tabs-container';

        // Устанавливаем breadcrumbs
        $view->params['breadcrumbs'] = $model->getAdminBreadcrumbsData();

        $isNewRecord = $model->isNewRecord;
        if ($model->load(Yii::$app->request->post()) && ($this->isNestedTree === true ? $model->saveNode() : $model->save())) {
            // определяем системные сообщения
            if ($isNewRecord) {
                $message = $this->params['messageAdd'] ?? Yii::t('cms', 'Item created');
            } else {
                $message = $this->params['messageUpdate'] ?? Yii::t('cms', 'Item updated');
            }
            Yii::$app->session->addFlash('success', $message, false);
            // при аякс-запросах не выполняем редирект
            if (!Yii::$app->request->isAjax) {

                $pKey = $isNewRecord ? Yii::$app->db->lastInsertID : $model->id;

                if (isset($this->params['returnUrl'])) {

                    // если returnUrl задан в виде массива с макросами, то заменяем соответствующие
                    // значения атрибутами модели
                    if (is_array($this->params['returnUrl'])) {
                        // переопределяем параметры на которые нужно возвращать
                        foreach ($this->params['returnUrl'][1] as $key => $value) {
                            if (!empty($model[substr($value, 1)])) {
                                $this->params['returnUrl'][$key] = $model[substr($value, 1)];
                            } else {
                                $this->params['returnUrl'][$key] = $value;
                            }
                        }
                        unset($this->params['returnUrl'][1]);
                        $url = $this->params['returnUrl'];
                    } else {
                        $url = [$this->params['returnUrl'], 'id' => $pKey];
                    }
                } else {
                    switch (Yii::$app->request->post('action')) {
                        case self::ACTION_APPLY:
                            $url = $model->getUpdateLink();
                            break;
                        case self::ACTION_SAVE:
                        default:
                            $url = $model->getListLink();
                            break;
                    }
                }

                Yii::$app->controller->redirect($url);
            } else {
                if (empty($_POST['apply'])) {
                    $data = [
                        'status' => 'success',
                        'message' => Yii::$app->session->getFlash('success'),
                        'id' => $model->id,
                    ];

                    return $this->render('//content', ['content' => json_encode($data)]);
                }

                return $this->render($this->params['template'], ['model' => $model,]);
            }
        }

        return $this->render($this->params['template'], ['model' => $model,]);
    }
}