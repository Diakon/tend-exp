<?php
namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Регион.
 *
 * @property integer $city_id         Идентификатор.
 * @property integer $cityId         Идентификатор.
 * @property integer $region_id       Идентификатор региона.
 * @property string $name             Наименование.
 * @property integer $popul_all       Насиление всего.
 * @property integer $popul_municipal Муниципальное насиление.
 * @property integer $popul_regional  Региона насиление.
 */
class GeoCity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'region_id'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
            [['region_id', 'popul_all', 'popul_municipal', 'popul_regional'], 'integer'],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Город'),
            'region_id' => Yii::t('app', 'Регион'),
            'popul_municipal' => Yii::t('app', 'Насиление муниципалитета'),
            'popul_all' => Yii::t('app', 'Насиление всего'),
            'popul_regional' => Yii::t('app', 'Насиление региона'),
        ];
    }

    /**
     * Связь с регионом
     *
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::class, ['id' => 'region_id']);
    }

    /**
     * Возвращает список городов
     *
     * @return array
     */
    public static function getCityList()
    {
        $regions = self::find()
            ->select(['id', 'name'])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::map($regions, 'id', 'name');
    }
}
