<?php
namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Регион.
 *
 * @property integer $id             Идентификатор.
 * @property integer $area_id        Идентификатор округа.
 * @property string $name            Наименование.
 * @property double $coord_lon       Долгота центра.
 * @property double $coord_lat       Широта центра.
 * @property integer $popul_all      Насиление всего.
 * @property integer $popul_regional Насиление региона.
 */
class GeoRegion extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'country_id'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
            [['country_id', 'area_id', 'popul_all', 'popul_regional'], 'integer'],
            [['coord_lon', 'coord_lat'], 'number'],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Страна'),
            'name' => Yii::t('app', 'Регион'),
            'area_id' => Yii::t('app', 'Округ'),
            'coord_lon' => Yii::t('app', 'Долгота центра'),
            'coord_lat' => Yii::t('app', 'Широта центра'),
            'popul_all' => Yii::t('app', 'Насиление всего'),
            'popul_regional' => Yii::t('app', 'Насиление региона'),
        ];
    }

    /**
     * Связь с областью
     *
     * @return ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(GeoArea::class, ['id' => 'area_id']);
    }

    /**
     * Связь с страной
     *
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::class, ['id' => 'country_id']);
    }

    /**
     * Возвращает список регионов
     * @return array
     */
    public static function getRegionsList($countryId = null)
    {
        $query = self::find()->select(['id', 'name']);
        if (!empty($countryId)) {
            $query->where(['country_id' => $countryId]);
        }
        $query->orderBy(['name' => SORT_ASC]);

        $regions = $query->orderBy(['name' => SORT_ASC])->asArray()->all();

        return ArrayHelper::map($regions, 'id', 'name');
    }
}