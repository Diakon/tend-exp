<?php
namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Муниципалететы.
 *
 * @property integer $mun_id          Идентификатор.
 * @property integer $region_id       Идентификатор региона.
 * @property string $name             Наименование.
 * @property integer $coord_lon       Долгота центра.
 * @property integer $coord_lat       Широта центра.
 * @property integer $popul_all       Насиление всего.
 * @property integer $popul_municipal Муниципальное насиление.
 * @property integer $popul_regional  Региона насиление.
 */
class GeoMun extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_mun}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'region_id'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
            [['region_id', 'popul_all', 'popul_municipal', 'popul_regional'], 'integer'],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mun_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'region_id' => Yii::t('app', 'Регион'),
            'popul_municipal' => Yii::t('app', 'Насиление муниципалитета'),
            'popul_all' => Yii::t('app', 'Насиление всего'),
            'popul_regional' => Yii::t('app', 'Насиление региона'),
        ];
    }

    /**
     * Связь с регионом
     *
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::class, ['id' => 'region_id']);
    }
}