<?php

namespace common\models;


/**
 * Класс активных запросов для пользователей
 */
class UsersQuery extends ActiveQuery
{

    /**
     * Выбираем пользователей по роли
     *
     * @param string $role Роль пользователя
     *
     * @return $this
     */
    public function byRole($role)
    {
        return $this->joinWith('assignments')->andWhere(AuthAssignment::tableName() . '.item_name=:role', [':role' => $role]);
    }

    /**
     * Выбираем пользователей по ролям
     *
     * @param array $role Роли пользователя
     *
     * @return $this
     */
    public function byRoles(array $role)
    {
        return $this->joinWith('assignments')->andWhere(['in', AuthAssignment::tableName() . '.item_name', $role]);
    }

    /**
     * Активные пользователи
     *
     * @param array $status
     *
     * @return $this
     */
    public function softDeleteStatus($status)
    {
        $this->andFilterWhere(['in', 'soft_delete_status', $status]);

        return $this;
    }
}
