<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Округа.
 *
 * @property integer $id   Идентификатор.
 * @property string  $name Наименование.
 * @property string  $abbr Абревиатура.
 */
class GeoArea extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_area}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'abbr'], 'required'],
            [['name', 'abbr'], 'unique'],
            [['name'], 'string', 'max' => 255],
            [['abbr'], 'string', 'max' => 10],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'abbr' => Yii::t('app', 'Аббревиатура')
        ];
    }

    /**
     * Возвращает список округов
     * @return array
     */
    public static function getAreaList()
    {
        $regions = self::find()
            ->select(['id', 'name'])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::map($regions, 'id', 'name');
    }
}