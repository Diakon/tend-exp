<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * Class Pages.
 */
class Pages extends modules\pages\models\common\Pages

{

    // Шаблоны страниц
    const MAIN_TEMPLATE = 1;
    const TARIF_TEMPLATE = 2;

    /**
     * @var string Имя REQUEST-параметра для поиска модели в подробном выводе.
     */
    public static $paramIdent = self::C_URL;
    /**
     * @var string Имя колонки в БД для поиска модели в подробном выводе и редактировании/удалении элементов.
     */
    public static $paramColumn = self::C_URL;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::className(),
            'systemData' => SystemDataBehavior::className(),
            'forms' => FormConfigBehavior::className(),
            'image' => [
                'class' => UploadBehavior::class,
                'attributes' => [
                    'img' => [
                        'singleFile' => true,
                        'rules' => [
                            'image' => [
                                'extensions' => 'png,jpeg,jpg,bmp',
                                'minWidth' => 10,
                                'maxWidth' => 1600,
                                'minHeight' => 100,
                                'maxHeight' => 1200,
                                'maxSize' => 3 * 1024 * 1024,
                            ],
                        ],
                    ],
                ],
            ],
            'fieldFormat' => [
                'class' => FieldFormatBehavior::className(),
                'dateFormat' => \Yii::$app->params['datePagesFormat'],
                'dateFields' => ['date_published'],
            ],
        ];
    }
}
