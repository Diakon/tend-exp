<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use modules\crud\models\ActiveRecord;

/**
 * Округа.
 *
 * @property integer $id   Идентификатор.
 * @property string  $name Наименование.
 */
class GeoCountry extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Страна'),
        ];
    }

    /**
     * Возвращает список стран
     * @return array
     */
    public static function getCountryList()
    {
        $regions = self::find()
            ->select(['id', 'name'])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::map($regions, 'id', 'name');
    }
}