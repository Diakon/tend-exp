<?php

namespace common\models;

use common\modules\company\models\frontend\CompanyUsers;
use Yii;

use yii\helpers\ArrayHelper;

/**
 * Class UserIdentity.
 *
 * @property CompanyUsers $companyUser
 * @package common\components
 */
class UserIdentity extends User
{
    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        // Установка нового пароля
        if (!empty($this->newPassword)) {
            $this->setPassword($this->newPassword);
            Yii::$app->session->addFlash('success', Yii::t('users', 'Password successfully stored'));
        }

        parent::afterValidate();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'image' => [
                    'class' => UploadBehavior::class,
                    'attributes' => [
                        'avatar' => [
                            'singleFile' => true,
                            'rules' => [
                                'image' => [
                                    'extensions' => 'png,jpeg,jpg,bmp',
                                    'minWidth' => 10,
                                    'maxWidth' => 1600,
                                    'minHeight' => 100,
                                    'maxHeight' => 1200,
                                    'maxSize' => 3 * 1024 * 1024,
                                ],
                            ],
                        ],
                    ]
                ],
            ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['middlename'], 'string', 'max' => 255],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PROFILE_EDIT] = ['email', 'phone', 'lastname', 'username', 'middlename'];

        return $scenarios;
    }

    /**
     * Связь с пользователем компании
     *
     * @return void
     */
    public function getCompanyUser() {

        $companyData = Yii::$app->user->getActiveCompanyData();

        return $this->hasOne(\common\modules\company\models\frontend\CompanyUsers::class, ['user_id' => 'id'])
            ->onCondition(['company_id' => $companyData['id'] ]);
    }

    /**
     * Список ролей пользователя.
     *
     * @return string | array
     */
    public function getRoles($asArray = true)
    {
        $rolesUser = ($this->getAssignedRules()->asArray()->all());
        $roles = [];
        foreach ($rolesUser as $key => $value) {
            $roles[] = $value['name'];
        }

        if ($asArray) {
            return $roles;
        }

        return implode(',', $roles);
    }

    /**
     *
     *
     * @param $view
     * @param $toAttribute
     * @param \common\modules\company\models\frontend\Company $company
     * @param bool $invite
     * @param $params
     *
     * @return bool
     */
    public function sendEmailConfirmationEmail($view, $toAttribute, \common\modules\company\models\frontend\Company $company, $invite = false, $params = [])
    {
        return \Yii::$app->mailer->compose(['html' => $view . '-html', 'text' => $view . '-text'],
            [ 'company' => $company, 'user' => $this, 'token' => $this->emailConfirmToken, 'invite' => $invite, 'params' => $params])
            ->setFrom(\Yii::$app->params['adminEmailFrom'])
            ->setTo($this->email)
            ->setSubject('Email подтверждение ' . \Yii::$app->name)
            ->send();
    }

}
