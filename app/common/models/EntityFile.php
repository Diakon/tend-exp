<?php

namespace common\models;

use common\components\ClassMap;

use frontend\modules\upload\models\Upload;
use Yii;
use yii\base\DynamicModel;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Модель загрузок
 *
 * @property string       $id             Идентификатор
 * @property string       $guid           GUID
 * @property string       $status         Статус
 * @property string       $title          Заголовок
 * @property string       $modelName      Наименование модели
 * @property string       $modelId        Идентификатор модели
 * @property string       $modelAttribute Атрибут модели
 * @property string       $fileName       Наименование файла
 * @property string       $fileAlt        Описание файла
 * @property string       $fileSize       Размер файла
 * @property string       $fileExt        Расширение файла
 * @property string       $order          Сортировка
 * @property EntityFile[] $messages
 * @property string       $createdAt      Дата создания
 * @property string       $updatedAt      Дата обновления
 * @property boolean      $fileExists     Флаг наличия файла
 */
class EntityFile extends Upload
{

    /**
     * @var UploadedFile[] $file
     */
    public $files;


    const  SCENARIO_SYNC_STATUS = 'sync-status';

    /**
     * Статус: Не принят
     */
    const STATUS_REFUSED = 10;

    /**
     * Статус: Принят частично
     */
    const STATUS_ACCEPTED_WITH_COMMENTS = 20;

    /**
     * Статус: Принят
     */
    const STATUS_ACCEPTED = 30;

    /**
     * @var array $file
     */
    public $uploadedFiles;

    public static $ajaxRules = [
        'extensions' => [],
        'mimeTypes' => ['application/vnd.ms-excel', 'application/vnd.ms-office', 'image/jpeg', 'image/jpg', 'image/png', 'application/vnd.ms-excel', 'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],

        'maxSize' => 20 * 1024 * 1024,
        'maxFiles' => 10,
        'wrongMimeType' => 'Разрешена загрузка файлов только со следующими расширениями: doc, docx, pdf, jpg, png, xls, xlsx.'
    ];

    /**
     * Сценарий загрузки файла по ajax
     */
    const SCENARIO_AJAX_UPLOAD_FILE = 'upload_ajax_file';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entity_file}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'timestamp' => [
                    'class' => TimestampBehavior::className(),
                    'createdAtAttribute' => 'created_at',
                    'updatedAtAttribute' => 'updated_at',
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
                    ],
                    'value' => new Expression('NOW()'),
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // SCENARIO_UPLOAD_FILE
            [['modelAttribute', 'modelName', 'modelId', 'fileExt', 'fileName', 'fileSize'], 'required', 'on' => self::SCENARIO_UPLOAD_FILE],
            // SCENARIO_AJAX_UPLOAD_FILE
            [['modelName', 'modelId', 'fileExt', 'fileName', 'fileSize', 'title'], 'required', 'on' => self::SCENARIO_AJAX_UPLOAD_FILE],
            // DEFAULT
            [[ 'order', 'fileSize', 'createdAt', 'updatedAt'], 'integer'],
            [['modelName'], 'integer'],
            [['modelAttribute', 'title', 'fileName', 'fileAlt'], 'string', 'max' => 512],
            [['fileExt', 'modelAttribute'], 'string', 'max' => 64],
            [['modelId'], 'string', 'max' => 50],
            [['delete'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_AJAX_UPLOAD_FILE => ['modelName', 'modelId', 'fileExt', 'fileName', 'fileSize'],
        ]);
    }

    /**
     * Возвращаем путь к файлу
     *
     * @param boolean $web     Выводим полный путь к файлу, либо путь относительно корня сайта
     * @param null    $noImage Файл заглушки в случае отсутствия запрашиваемого файла
     *
     * @return boolean|mixed|null|string
     */
    public function getFile($web = false, $noImage = null)
    {
        $className = ClassMap::getClassName($this->modelName);
        $path = explode('\\', $className);
        $className = end($path);

        $path = Yii::getAlias('@root/upload/' . Inflector::camel2id($className, '_') . DIRECTORY_SEPARATOR . $this->modelAttribute . DIRECTORY_SEPARATOR . $this->modelId . DIRECTORY_SEPARATOR . $this->fileName);

        return is_file($path) ? ($web ? str_replace(Yii::getAlias('@root'), '', $path) : $path) : $noImage;
    }

    /**
     * Базовый путь для текущей сущности (заявки)
     *
     * @return bool|string
     */
    public function getBaseDir()
    {
        $className = ClassMap::getClassName($this->modelName);
        $path = explode('\\', $className);
        $className = end($path);

        return Yii::getAlias('@root/upload/' . Inflector::camel2id($className, '_') . DIRECTORY_SEPARATOR . $this->modelAttribute);

    }

    /**
     * Удаление файла
     *
     * @return boolean
     */
    public function deleteFiles()
    {

        $className = ClassMap::getClassName($this->modelName);
        $path = Yii::getAlias('@root/upload/' . Inflector::camel2id($className::classNameShort(), '_') . DIRECTORY_SEPARATOR . $this->modelAttribute . DIRECTORY_SEPARATOR . $this->modelId . DIRECTORY_SEPARATOR);

        if (is_dir($path)) {
            $fileName = $this->fileName;
            // Ищем файлы которые оканчиваются на $file_name
            $files = FileHelper::findFiles($path, [
                'filter' => function ($path) use ($fileName) {
                    return substr($path, strlen($path) - strlen($fileName)) == $fileName;
                },
            ]);
            // Удаляем их
            if (!empty($files) && is_array($files)) {
                foreach ($files as $file) {
                    unlink($file);
                }
            }
        }

        return true;
    }

    /**
     * Загружает файл в директорию на сервере и сохраняет запись в базе
     *
     * @param UploadedFile[] $files Инстансы загруженных файлов
     *
     * @return boolean Результат загрузки файла и сохранения модели
     */
    public function ajaxUpload($files)
    {

        $this->scenario = static::SCENARIO_AJAX_UPLOAD_FILE;
        $this->files = $files;

        $dynamicModel = new DynamicModel(['files' => $this->files]);

        $validator = Validator::createValidator(
            'file',
            $dynamicModel,
            'files',
            static::$ajaxRules
        );

        $validator->validateAttribute($dynamicModel, 'files');

        // Устанавливаем ошибку если модель не прошла валидацию
        if ($dynamicModel->hasErrors('files')) {
            $this->addErrors($dynamicModel->getErrors());
            return false;
        }

        foreach ($this->files as $file) {

            $newPath = $this->getUploadSavePath($this->modelId);

            $prepareName = preg_replace('/[^a-zA-Z0-9_]/', '', Inflector::transliterate($file->baseName, Inflector::TRANSLITERATE_LOOSE));
            $fileName = $prepareName . '_' . time() . '.' . $file->extension;

            $upload = new self();
            $upload->fileName = $fileName;
            $upload->title = $file->baseName;
            $upload->modelName = $this->modelName;
            $upload->modelId = $this->modelId;
            $upload->fileExt = $file->extension;
            $upload->modelAttribute = 'files';
            $upload->fileSize = $file->size;
            $upload->order = 0;

            if (!$upload->validate()) {
                return false;
            }

            try {
                if (!$file->saveAs($newPath . DIRECTORY_SEPARATOR . $fileName)) {


                    $this->addError('fileName', $file->baseName . ': Ошибка сохранения загруженного файла');
                    continue;
                }
                $upload->save(false);

                $this->uploadedFiles[] = [
                    'id' => $upload->id,
                    'title' => $upload->title,
                    'fileExt' => $upload->fileExt,
                    'fileSize' => $upload->fileSize,
                    'fileName' => $upload->fileName,
                    'downloadUrl' => Url::to($upload->getDownloadRoute()),
                ];
            } catch (\Exception $exception) {
                var_dump($exception->getMessage() . '***' . $exception->getTraceAsString());
                $this->addError('fileName', $file->baseName . ': Ошибка сохранения загруженного файла');
            }

        }
        return !$this->hasErrors();

    }

    /**
     * Возвращаем путь сохранения файла
     *
     * @param integer $id Идентификатор модели
     *
     * @return string
     */
    private function getUploadSavePath($id = null)
    {
        $className = ClassMap::getClassName($this->modelName);

        $alias = '@root/upload/' . Inflector::camel2id($className::classNameShort(), '_') . DIRECTORY_SEPARATOR;
        $path = Yii::getAlias($alias . DIRECTORY_SEPARATOR . $this->modelAttribute . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR);

        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        return $path;
    }

    /**
     * Возвращаем роут к функционалу загрузки файлов
     *
     * @return array
     */
    public function getDownloadRoute(): array
    {
        return ['/upload/base/download', 'id' => $this->id,'notOrg'=>true];
    }

    /**
     * Возвращаем путь к функционалу зашщищеной загрузки файлов
     *
     * @return array
     */
    public function getProtectedDownloadRoute(): array
    {
        return ['/upload/base/protected-download', 'id' => $this->id,'notOrg'=>true];
    }

}
