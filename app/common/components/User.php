<?php
namespace common\components;

use common\models\UserIdentity;
use common\modules\company\models\frontend\Company;
use common\modules\company\models\frontend\CompanyUsers;
use common\modules\offers\models\frontend\Offers;
use common\modules\tariff\models\common\TariffPlans;
use frontend\modules\upload\models\Upload;
use Yii;
use common\modules\tender\models\common\Tenders;
use yii\caching\DbDependency;

/**
 * Пользователь.
 * @property $avatar Upload
 * @property UserIdentity $identity
 */
class User extends \yii\web\User
{
    /**
     * Возвращает данные об организации
     * @param string $companyUrl
     *
     * @return \modules\crud\models\ActiveQuery|object|\yii\db\ActiveQuery
     */
    public function getCompany($companyUrl = '')
    {
        $companyUrl = str_replace("/", "", $companyUrl);
        // Если URL компании не указан, то возвращаю все организации к которым закреплен пользователь
        if (empty($companyUrl)) {
            return $this->getAllCompanies();
        }

        if(\Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return  Company::find()->where(['url' => $companyUrl]);
        }
        return $this->getAllCompanies()->andWhere([Company::tableName() . '.url' => $companyUrl]);
    }

    /**
     * Возвращает список всех компаний, к которым относится пользователь
     *
     * @return object
     */
    public function getAllCompanies()
    {
        return Company::find()
            ->with('users')
            ->leftJoin(CompanyUsers::tableName() . ' as ' . CompanyUsers::classNameShort('id'), CompanyUsers::classNameShort('id') . '.company_id=' . Company::tableName() . '.id')
            ->where([CompanyUsers::classNameShort('id') . '.user_id' => $this->id])
            ->andWhere([CompanyUsers::classNameShort('id') . '.status' => CompanyUsers::STATUS_ACTIVE])
            ->andWhere([Company::tableName() . '.status' => Company::STATUS_ACTIVE]);
    }

    /**
     * Проверка можно ли показывать расширенный фильтр
     *
     * @return bool|mixed
     */
    public function canShowAdditionalFilter()
    {
        if (Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return true;
        }
        if (!\Yii::$app->user->isGuest) {

            $activeCompanyData = $this->getActiveCompanyData();

            /**
             * @var Company $myCompany
             */
            $myCompany = Company::findOne(['id' => $activeCompanyData['id']]); //Yii::$app->user->getCompany()->one();
            $tariff = $myCompany->tariff ?? new TariffPlans();

            return $tariff->type > TariffPlans::CONST_TYPE_MIN ? true : false;
        }

        return false;
    }

    /**
     * Получение активной организации
     *
     * @return array|mixed|null
     */
    public function getActiveCompanyData()
    {

        $dataCompany = Yii::$app->cache->get('activeCompany-' . Yii::$app->user->id);

        if ($dataCompany !== false) {
            return $dataCompany;
        }

        $myCompany = Yii::$app->user->getCompany()->asArray()->one();
        $dependency = new DbDependency(['sql' => 'SELECT MAX(updated_at) from {{%company}}']);
        Yii::$app->cache->set('activeCompany-' . Yii::$app->user->id, $myCompany, null, $dependency);

        return $myCompany;
    }

    /**
     * Проверяет, может ли пользователь участвовать в тендере
     *
     * @param \common\modules\tender\models\frontend\Tenders $tender Тендер
     * @param bool $ignoreOfferCompany Флаг игнорирования факта, что для этого тендера было уже оставлено предложение компанией
     * @return bool
     */
    public function canGetTender($tender, $ignoreOfferCompany = false)
    {
        // Проверяю, что дата тендера еще не истекла
        if (Yii::$app->formatter->asTimestamp($tender->date_end) < time()) {
            return false;
        }
        $statusTender = $tender->status;
        if (!in_array($statusTender, [Tenders::STATUS_COLLECT_OFFERS])) {
            return false;
        }
        if(\Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return true;
        }
        if (!\Yii::$app->user->isGuest) {
            $activeCompanyData = $this->getActiveCompanyData();
            /**
             * @var Company $myCompany
             */
            $myCompany = Company::findOne(['id' => $activeCompanyData['id']]); //Yii::$app->user->getCompany()->one();

            // Проверяю, что тендер не относится к компании пользователя (нельзя брать тендер, который делала твоя же компания)
            if ($myCompany->id == $tender->company->id) {
                return false;
            }

            if (!$ignoreOfferCompany) {
                // Проверка, что для этого тендера уже сделали предложение в компании пользователя
                $countOfferTender = Offers::find()->where(['company_id' => $myCompany->id])->andWhere(['tender_id' => $tender->id])->count();
                if (!empty($countOfferTender)) {
                    return false;
                }
            }

            // Проверка на такриф компании
            $tariff = $myCompany->tariff ?? new TariffPlans();
            return in_array($tariff->type, [TariffPlans::CONST_TYPE_CORP, TariffPlans::CONST_TYPE_PROF]) ? true : false;

        }
        return false;
    }

    /**
     * Проверяет, может ли пользователь взять тендер в избранное
     *
     * @return bool
     */
    public function canGetFavorite()
    {
        if(\Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return true;
        }
        if (!\Yii::$app->user->isGuest) {
            $activeCompanyData = $this->getActiveCompanyData();
            /**
             * @var Company $myCompany
             */
            // Проверка на такриф компании
            $myCompany = Company::findOne(['id' => $activeCompanyData['id']]); //Yii::$app->user->getCompany()->one();
            // Проверка на такриф компании
            $tariff = $myCompany->tariff ?? new TariffPlans();
            return in_array($tariff->type, [TariffPlans::CONST_TYPE_CORP, TariffPlans::CONST_TYPE_PROF]) ? true : false;
        }
        return false;
    }
}
