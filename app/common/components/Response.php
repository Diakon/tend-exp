<?php

namespace common\components;

/**
 * Class Response.
 * @package common\components
 */
class Response extends \yii\web\Response
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!YII_DEBUG) {
            $dataHeaders = [
                'X-XSS-Protection' => '1; mode=block', // Зачита от xss
                'X-Frame-Options' => 'ALLOW-FROM http://www.ratingruneta.ru/', // Запрет открытия во фрейме
                'X-Content-Type-Options' => 'nosniff', // Запрет неправльной интерпритации mime-типов.
                'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains', // для https - защита от атаки man-in-the-middle - подробности https://habrahabr.ru/post/216751/
            ];
            $this->addHeaders($dataHeaders);
        }
    }

    /**
     * Множественная загрузка заголовков
     *
     * @param array $headers Заголовки
     *
     * @return void
     */
    private function addHeaders(array $headers) {
        foreach ($headers as $name => $value) {
            $this->headers->add($name, $value);
        }
    }
}
