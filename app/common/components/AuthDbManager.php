<?php
namespace common\components;


use yii\db\Query;
use yii\rbac\Assignment;
use yii\rbac\DbManager;

/**
 * Class AuthDbManager
 *
 * @package common\components
 */
class AuthDbManager extends DbManager
{
    /**
     * Роль клиента
     */
    const ROLE_CLIENT = 'client';

    /**
     * Роль Администратор
     */
    const ROLE_ADMIN = 'administrator';

    /**
     * Роль Модератов
     */
    const ROLE_MODERATOR = 'moderator';

    /**
     * Статическое свойство, для хранения результата вызова функции getAssignments() и сокращения запросов в б/д
     *
     * @var array
     */
    private static $RESULT_USER_ASSIGNMENTS = [];

    /**
     * @inheritdoc
     */
    public function getAssignments($userId)
    {
        if (empty($userId)) {
            return [];
        }

        // Если ранее проверялось данное правило для данного уровня, то выдаем результат проверки
        if (isset(self::$RESULT_USER_ASSIGNMENTS[$userId])) {
            return self::$RESULT_USER_ASSIGNMENTS[$userId];
        }

        $query = (new Query)
            ->from($this->assignmentTable)
            ->where(['user_id' => (string)$userId]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['item_name']] = new Assignment([
                'userId' => $row['user_id'],
                'roleName' => $row['item_name'],
                'createdAt' => $row['created_at'],
            ]);
        }
        self::$RESULT_USER_ASSIGNMENTS[$userId] = $assignments;

        return $assignments;
    }
}
