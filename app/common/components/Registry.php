<?php
namespace common\components;

use backend\modules\settings\models\backend\Settings;
use yii\base\Component;
use yii;

/**
 * Класс-контейнер который содержит настройки для использования в различных модулях системы. Yii::$app->registry->getSetting('twit');
 */
class Registry extends Component
{
    /**
     * @var string Cache
     */
    protected static $cacheData = null;

    /**
     * Настройки Системы
     *
     * @param string  $key       Название поля в базе.
     * @param boolean $cacheMode Использование кеша.
     *
     * @return string
     */
    public function getSetting($key, $cacheMode = true) : string
    {
        // Сохраняем в статическую переменную данные из кеша
        if (self::$cacheData === null) {
            $cacheData = Yii::$app->cache->get('siteSettings');
            self::$cacheData = $cacheData; // Для последующих запросов настроек
        } else {
            $cacheData = self::$cacheData;
        }

        if ($cacheData && $cacheMode) {
            return !empty($cacheData->{$key}) ? $cacheData->{$key} : '';
        }
        $setting = Settings::findOne(['id' => 1]);

        $dependency = new yii\caching\DbDependency([
            'sql' => 'SELECT updated_at FROM ' . Settings::tableName() . ' WHERE id=1',
        ]);

        self::$cacheData = $setting;

        Yii::$app->cache->set('siteSettings', $setting, Yii::$app->params['cache']['time']['siteSettings'], $dependency);

        return !empty($setting->{$key}) ? $setting->{$key} : '';
    }
}
