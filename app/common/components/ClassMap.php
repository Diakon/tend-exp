<?php

namespace common\components;

use common\models\EntityFile;
use common\modules\company\models\frontend\Company;
use common\modules\offers\models\frontend\Offers;
use common\modules\offers\models\frontend\OffersElements;
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\EstimatesElements;
use common\modules\tender\models\frontend\Objects;
use common\modules\tender\models\frontend\Tenders;
use frontend\models\EntityComment;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class ClassMap. Соответствие классов, их ID которые будут храниться в базе
 *
 * @package common\components
 */
class ClassMap extends Component
{

    /**
     * Константы внутренних ID классов
     */
    const CLASS_COMMENT_ID = 1;
    const CLASS_OBJECTS_ID = 2;
    const CLASS_TENDER_ID = 3;
    const CLASS_ESTIMATES_RUBRICS_ID = 4;
    const CLASS_ESTIMATES_ELEMENTS_ID = 5;
    const CLASS_OFFERS_ELEMENTS = 6;
    const CLASS_ENTITY_FILE = 7;
    const CLASS_COMPANY = 8;
    const CLASS_OFFER = 9;


    /**
     * Массив классов и их Id в базе
     *
     * @return array
     */
    private static function classMap(): array
    {
        return [
            [
                'class' => EntityComment::class,
                'id' => self::CLASS_COMMENT_ID,
            ],
            [
                'class' => Objects::class,
                'id' => self::CLASS_OBJECTS_ID,
            ],
            [
                'class' => Tenders::class,
                'id' => self::CLASS_TENDER_ID,
            ],
            [
                'class' => EstimatesRubrics::class,
                'id' => self::CLASS_ESTIMATES_RUBRICS_ID,
            ],
            [
                'class' => EstimatesElements::class,
                'id' => self::CLASS_ESTIMATES_ELEMENTS_ID,
            ],
            [
                'class' => OffersElements::class,
                'id' => self::CLASS_OFFERS_ELEMENTS,
            ],
            [
                'class' => EntityFile::class,
                'id' => self::CLASS_ENTITY_FILE,
            ],
            [
                'class' => Company::class,
                'id' => self::CLASS_COMPANY,
            ],
            [
                'class' => Offers::class,
                'id' => self::CLASS_OFFER,
            ],
        ];
    }

    /**
     * Получаем массив классов и их Id в базе
     *
     * @return array
     */
    public static function getClassMap(): array
    {
        return ArrayHelper::map(self::classMap(), 'id', 'class');
    }

    /**
     * Получение названия имени класса по ID
     *
     * @param       $classId
     *
     * @param array $default Значение по умолчанию
     * @return array|string
     */
    public static function getClassName($classId, $default = [])
    {
        return ArrayHelper::getValue(self::getClassMap(), $classId, $default);
    }


    /**
     * Получение ID по имени класса
     *
     * @param object|string $class
     *
     * @return false|int|string
     * @throws InvalidConfigException
     */
    public static function getId($class)
    {
        if (empty($class)) {
            throw new InvalidConfigException('Запрашивается идентификатор связанного класса для пустого значения');
        }

        $className = is_object($class) ? get_class($class) : $class;

        $result = array_search($className, self::getClassMap());
        if ($result === false) {
            throw new InvalidConfigException('Запрашивается неизвестный класс: ' . (is_object($class) ? get_class($class) : $class));
        }
        return $result;
    }

    /**
     * Маппинг идентификаторов
     *
     * @param   string|int $id         Идентификатор
     *
     * @return false|int|string
     * @throws InvalidConfigException
     */
    public static function mapId($id)
    {
        return self::getId(self::getClassName($id, null));
    }
}
