<?php
namespace common\components;
use helpers\Dev;
use Exception;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\imagine\Image;
/**
 * Class Thumbnail
 *
 * @package common\components
 */
class Thumbnail
{
    const DEFAULT_MODE = self::THUMBNAIL_OUTBOUND;
    const THUMBNAIL_OUTBOUND = ManipulatorInterface::THUMBNAIL_OUTBOUND;
    const THUMBNAIL_INSET = ManipulatorInterface::THUMBNAIL_INSET;
    const QUALITY = 100;
    const DEFAULT_WIDTH = 300;
    const DEFAULT_HEIGHT = 300;
    const MKDIR_MODE = 0755;
    const NO_FILE = 'no-file';
    /** @var string $cacheAlias path alias relative with @web where the cache files are kept */
    public static $cacheAlias = 'assets/thumbnails';
    /** @var int $cacheExpire */
    public static $cacheExpire = 86400;
    /**
     * Creates and caches the image thumbnail and returns ImageInterface.
     *
     * @param string  $filename the image file path or path alias or URL
     * @param integer $width    the width in pixels to create the thumbnail
     * @param integer $height   the height in pixels to create the thumbnail
     * @param string  $mode     self::THUMBNAIL_INSET, the original image
     * @param integer $quality
     *                          is scaled down so it is fully contained within the thumbnail dimensions.
     *                          The specified $width and $height (supplied via $size) will be considered
     *                          maximum limits. Unless the given dimensions are equal to the original image’s
     *                          aspect ratio, one dimension in the resulting thumbnail will be smaller than
     *                          the given limit. If self::THUMBNAIL_OUTBOUND mode is chosen, then
     *                          the thumbnail is scaled so that its smallest side equals the length of the
     *                          corresponding side in the original image. Any excess outside of the scaled
     *                          thumbnail’s area will be cropped, and the returned thumbnail will have
     *                          the exact $width and $height specified
     *
     * @throws \Imagine\Exception\RuntimeException
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws Exception
     * @return \Imagine\Image\ImageInterface
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public static function thumbnail($filename, $width = self::DEFAULT_WIDTH, $height = self::DEFAULT_HEIGHT, $mode = self::THUMBNAIL_OUTBOUND, $quality = null)
    {
        return Image::getImagine()
            ->open(self::thumbnailFile($filename, $width, $height, $mode, $quality));
    }
    /**
     * Creates and caches the image thumbnail and returns full path from thumbnail file.
     *
     * @param string  $filename The image file path or path alias
     * @param integer $width    The width in pixels to create the thumbnail
     * @param integer $height   The height in pixels to create the thumbnail
     * @param string  $mode     self::THUMBNAIL_INSET, The original image
     * @param integer $quality
     *
     * @return string
     * @throws Exception
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     * @throws \yii\base\InvalidParamException
     */
    public static function thumbnailFile($filename, $width = self::DEFAULT_WIDTH, $height = self::DEFAULT_HEIGHT, $mode = self::THUMBNAIL_OUTBOUND, $quality = null)
    {
        $filename = FileHelper::normalizePath(Yii::getAlias($filename));
        $root = '@webroot';
        $filenameWithRoot = Yii::getAlias($root . $filename);
        if (!is_file($filename) && is_file($filenameWithRoot)) {
            $filename = $filenameWithRoot;
        }
        if (!is_file($filename)) {
            throw new Exception(self::NO_FILE);
        }
        $cachePath = Yii::getAlias('@webroot/' . self::$cacheAlias);
        $thumbnailFileExt = strrchr($filename, '.');
        $thumbnailFileName = md5($filename . $width . $height . $mode . filemtime($filename));
        $thumbnailFilePath = $cachePath . DIRECTORY_SEPARATOR . substr($thumbnailFileName, 0, 2);
        $thumbnailFilePathLink = Yii::getAlias('@web/' . self::$cacheAlias) . DIRECTORY_SEPARATOR . substr($thumbnailFileName, 0, 2);
        $thumbnailFile = $thumbnailFilePath . DIRECTORY_SEPARATOR . $thumbnailFileName . $thumbnailFileExt;
        $thumbnailFileLink = $thumbnailFilePathLink . DIRECTORY_SEPARATOR . $thumbnailFileName . $thumbnailFileExt;
        if (file_exists($thumbnailFile)) {
            if (self::$cacheExpire !== 0 && (time() - filemtime($thumbnailFile)) > self::$cacheExpire) {
                unlink($thumbnailFile);
            } else {
                return $thumbnailFileLink;
            }
        }
        if (!is_dir($thumbnailFilePath)) {
            mkdir($thumbnailFilePath, self::MKDIR_MODE, true);
        }
        $options = [
            'quality' => $quality === null ? self::QUALITY : $quality
        ];
        $imagine = Image::getImagine();
        $size = new Box($width, $height);
        $resizeimg = $imagine->open($filename)
            ->thumbnail($size, $mode);
        $sizeR = $resizeimg->getSize();
        $widthR = $sizeR->getWidth();
        $heightR = $sizeR->getHeight();
        $preserve = $imagine->create($size);
        $startX = $startY = 0;
        if ($widthR < $width) {
            $startX = ($width - $widthR) / 2;
        }
        if ($heightR < $height) {
            $startY = ($height - $heightR) / 2;
        }
        $preserve->paste($resizeimg, new Point($startX, $startY))
            ->save($thumbnailFile, $options);
        return $thumbnailFileLink;
    }
    /**
     * Creates and caches the image thumbnail and returns URL from thumbnail file.
     *
     * @param string  $filename the image file path or path alias
     * @param integer $width    the width in pixels to create the thumbnail
     * @param integer $height   the height in pixels to create the thumbnail
     * @param string  $mode     self::THUMBNAIL_INSET, the original image
     * @param integer $quality
     *
     * @return string
     * @throws Exception
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     * @throws \yii\base\InvalidParamException
     */
    public static function thumbnailFileUrl($filename, $width = self::DEFAULT_WIDTH, $height = self::DEFAULT_HEIGHT, $mode = self::DEFAULT_MODE, $quality = null)
    {
        $filename = FileHelper::normalizePath(Yii::getAlias($filename));
        $cacheUrl =str_replace($_SERVER['DOCUMENT_ROOT'], '',  Yii::getAlias('@webroot/' . self::$cacheAlias));
        try {
            $thumbnailFilePath = static::thumbnailFile($filename, $width, $height, $mode, $quality);
        } catch (\Exception $e) {
            return static::errorHandler($e, $filename);
        }
        preg_match('#[^\\' . DIRECTORY_SEPARATOR . ']+$#', $thumbnailFilePath, $matches);
        $fileName = $matches[0] ?? '';
        return $cacheUrl . '/' . substr($fileName, 0, 2) . '/' . $fileName;
    }
    /**
     * Creates and caches the image thumbnail and returns <img> tag.
     *
     * @param array $config options similarly with \yii\helpers\Html::img()
     *
     * @return string
     * @throws Exception
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public static function img($config)
    {
        $filename = isset($config['image']) ? $config['image'] : null;
        $width = isset($config['width']) ? $config['width'] : self::DEFAULT_HEIGHT;
        $height = isset($config['height']) ? $config['height'] : self::DEFAULT_HEIGHT;
        $mode = isset($config['mode']) ? $config['mode'] : self::DEFAULT_MODE;
        $quality = isset($config['quality']) ? $config['quality'] : self::QUALITY;
        $options = isset($config['options']) ? $config['options'] : [];
        $noImage = isset($config['noImage']) ? $config['noImage'] : null;
        try {
            try {
                $thumbnailFileUrl = self::thumbnailFileUrl($filename, $width, $height, $mode, $quality);
            } catch (Exception $e) {
                if (!is_null($noImage)) {
                    $thumbnailFileUrl = self::thumbnailFileUrl($noImage, $width, $height, $mode, $quality);
                } else {
                    throw $e;
                }
            }
        } catch (\Exception $e) {
            return static::errorHandler($e, $filename);
        }
        if($thumbnailFileUrl == self::NO_FILE) {
            $thumbnailFileUrl = $noImage;
        }
        return Html::img(
            $thumbnailFileUrl,
            $options
        );
    }
    /**
     * Clear cache directory.
     *
     * @throws \yii\base\InvalidParamException
     * @return bool
     */
    public static function clearCache()
    {
        $cacheDir = Yii::getAlias('@webroot/' . self::$cacheAlias);
        self::removeDir($cacheDir);
        return @mkdir($cacheDir, self::MKDIR_MODE, true);
    }
    /**
     * Remove directory.
     *
     * @return void
     */
    protected static function removeDir($path)
    {
        if (is_file($path)) {
            @unlink($path);
        } else {
            array_map('self::removeDir', glob($path . DIRECTORY_SEPARATOR . '*'));
            @rmdir($path);
        }
    }
    /**
     * @param \Exception $error
     * @param string     $filename
     *
     * @return string
     */
    protected static function errorHandler($error, $filename)
    {
        if ($error instanceof Exception) {
            return $error->getMessage();
        }
        Yii::warning("{$error->getCode()}\n{$error->getMessage()}\n{$error->getFile()}");
        return 'Error ' . $error->getCode();
    }
}
