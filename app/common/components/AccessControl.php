<?php

namespace common\components;

use Yii;
use yii\di\Instance;
use function ksort;

/**
 * Class AccessControl
 *
 * @package common\components
 */
class AccessControl extends \yii\filters\AccessControl
{
    /**
     * Initializes the [[rules]] array by instantiating rule objects from configurations.
     */
    public function init()
    {
        if ($this->user !== false) {
            $this->user = Instance::ensure($this->user, \yii\web\User::className());
        }
        $rules = [];
        foreach ($this->rules as $i => $rule) {
            if (is_array($rule)) {
                $rules[$i] = Yii::createObject(array_merge($this->ruleConfig, $rule));
            }
        }
        ksort($rules);

        $this->rules = $rules;
    }
}