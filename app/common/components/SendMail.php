<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class SendMail
 */
class SendMail extends Component
{
    /**
     * Отправляет письмо адресату используя шаблоны
     *
     * @param string $view Шаблон письма
     * @param array $param Массив данных передаваемых в шаблон
     * @param array $from От кого
     * @param array $to Кому
     * @param string $subject Тема письма
     * @param string $layout Кастомный шаблон
     * @return bool
     */
    public static function send($view, $param = [], $from, $to, $subject, $layout = null)
    {
        $imageUrl = Yii::getAlias('@mailer') . '/images';

        // Кастомный шаблон
        if (!empty($layout)) {
            Yii::$app->mailer->htmlLayout = $layout;
        }

        if (!empty(Yii::$app->params['server']['isDev'])) {
            $to = [];
            foreach (Yii::$app->params['adminEmailTo'] ?? [] as $adminEmail) {
                $to[$adminEmail] = $adminEmail;
            }
        }

        // Добавляю картинки стандартного шаблона (в кастомном не используется)
       // $param = ArrayHelper::merge(['headerDefaultTemplateImage' => $imageUrl . '/selekt-banner.jpg',], $param);

        return Yii::$app->mailer->compose($view, $param)->setFrom($from)->setTo($to)->setSubject($subject)->send();
    }
}
