<?php
namespace common\components;

use modules\crud\widgets\GridView as BaseGridView;
use yii\helpers\Html;
use base\BaseActiveRecord;

/**
 * The GridView widget is used to display data in a grid.
 */
class GridView extends BaseGridView
{
    /**
     * Секция шаблона элементов управления: Клонирование.
     */
    const SECTION_CONTROL_CLONE = '{clone}';

    /**
     * @var boolean Определяет необходимость добавления столбца с элементами управления.
     */
    public $footerAsArray = false;

    /**
     * Формирование элементов управления.
     *
     * @param BaseActiveRecord $data Данные модели.
     *
     * @return string HTML-представление элементов управления.
     */
    protected function renderControlItems(BaseActiveRecord $data)
    {
        $output = $this->layoutControl;
        if (mb_strpos($this->layoutControl, self::SECTION_CONTROL_VIEW) !== false) {
            $output = str_replace(self::SECTION_CONTROL_VIEW, Html::a('<i class="glyphicon glyphicon-eye-open"></i>', !empty($data->viewLink) ? $data->viewLink : '#', ['class' => 'btn btn-default', 'data-pjax' => 0, 'data-title' => !empty($data->title) ? $data->title : \Yii::t('cms/crud', 'View')]), $output);
        }
        if (strpos($this->layoutControl, self::SECTION_CONTROL_EDIT) !== false) {
            $output = str_replace(self::SECTION_CONTROL_EDIT, Html::a('<i class="glyphicon glyphicon-edit"></i>', !empty($data->updateLink) ? $data->updateLink : '#', ['class' => 'btn btn-default', 'data-pjax' => 0, 'data-title' => \Yii::t('cms/crud', 'Editing')]), $output);
        }
        if (strpos($this->layoutControl, self::SECTION_CONTROL_DELETE) !== false) {
            $output = str_replace(self::SECTION_CONTROL_DELETE, Html::a('<i class="glyphicon glyphicon-remove text-danger"></i>', !empty($data->deleteLink) ? $data->deleteLink : '#', ['class' => 'btn btn-default btn-confirm-open', 'data-pjax' => 0, 'data-title' => \Yii::t('cms/crud', 'Deleting'), 'data-confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?')]), $output);
        }
        if (strpos($this->layoutControl, self::SECTION_CONTROL_CLONE) !== false) {
            $output = str_replace(self::SECTION_CONTROL_CLONE, Html::a('<i class="fa fa-copy"></i>', '#', ['class' => 'btn btn-default product-clone btn-confirm-reload', 'data-pjax' => 0, 'data-url' => !empty($data->cloneLink) ? $data->cloneLink : '#', 'data-pjax' => 0, 'data-product-id' => $data->id, 'data-title' => \Yii::t('cms/crud', 'Cloning'), 'data-confirm' => \Yii::t('yii', 'Are you sure you want to clone this item?')]), $output);
        }

        return mb_strpos($this->layoutControlWrapper, self::SECTION_CONTROL_WRAP_CONTENT) !== false ? str_replace(self::SECTION_CONTROL_WRAP_CONTENT, $output, $this->layoutControlWrapper) : $output;
    }

    /**
     * Renders the table footer.
     *
     * @return string the rendering result.
     */
    public function renderTableFooter()
    {
        if ($this->footerAsArray) {
            $cells = [];
            $content = '';
            $maxItems = 0;

            foreach ($this->columns as $column) {
                $cells[] = !empty($column->footer) ? $column->footer : $this->emptyCell;
                $maxItems = (is_array($column->footer) && count($column->footer) > $maxItems) ? count($column->footer) : $maxItems;
            }

            for ($i = 0; $i < $maxItems; $i++) {
                $trContent = [];
                foreach ($cells as $cell) {
                    $trContent[] = Html::tag('td', is_array($cell) ? (!empty($cell[$i]) ? $cell[$i] : $this->emptyCell) : $cell);
                }
                $content .= Html::tag('tr', implode('', $trContent), $this->footerRowOptions);
            }

            if ($this->filterPosition === self::FILTER_POS_FOOTER) {
                $content .= $this->renderFilters();
            }

            return "<tfoot>\n" . $content . "\n</tfoot>";
        } else {
            parent::renderTableFooter();
        }
    }
}
