<?php
namespace common\components;

use common\models\UserIdentity;
use yii\i18n\Formatter as BaseFormatter;
use yii;
use yii\helpers\Html;

/**
 * Formatter provides a set of commonly used data formatting methods.
 */
class Formatter extends BaseFormatter
{
    /**
     * Возвращает число в формате "сумма денег".
     *
     * @param float $value Число для форматирования.
     *
     * @return string
     */
    public function asMoney($value)
    {
        $value = $this->asDecimal($value, 2);
        $value = str_replace(",", ".", $value);

        return trim($value);
    }

    /**
     * Возвращает число в формате "процент скидки".
     *
     * @param float $value Число для форматирования.
     * @param null|integer $decimals Количество знаков после запятой
     * @return string
     */
    public function asDiscount($value, $decimals = null)
    {
        return number_format($value, $decimals) . '%';
    }

    /**
     * @inheritdoc
     */
    public function asDatetime($value, $format = null)
    {
        $format = empty($format) ? Yii::$app->params['dateFormat'] : $format;

        return parent::asDatetime($value, $format);
    }

    /**
     * Возвращает универсальную метку статуса.
     *
     * @param integer $value Значение статуса.
     * @param string  $type  Тип поля.
     *
     * @return string
     */
    public function asStatus($value, $type = '')
    {
        $type = !empty($type) ? ' (' . $type . ')' : '';

        return !empty($value) ? '<h5><span class="label label-success"><i class="glyphicon glyphicon-eye-open"></i> ' . $type . '</span></h5>' : '<h5><span class="label label-default"> <i class="glyphicon glyphicon-eye-close">  ' . $type . '</span></h5>';
    }

    /**
     * Возвращает универсальную текстовую метку статуса.
     *
     * @param integer $value Значение статуса.
     *
     * @return string
     */
    public function asStatusText($value)
    {
        return !empty($value) ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
    }

    /**
     * Возвращает метку статуса пользователя.
     *
     * @param integer $value Идентификатор статуса.
     *
     * @return string
     */
    public function asUserStatusLabel($value)
    {
        switch ($value) {
            case UserIdentity::STATUS_NEW:
                return '<span class="label label-warning">' . UserIdentity::getStatusArray()[UserIdentity::STATUS_NEW] . '</span>';
            case UserIdentity::STATUS_ACTIVE:
                return '<span class="label label-success">' . UserIdentity::getStatusArray()[UserIdentity::STATUS_ACTIVE] . '</span>';
            case UserIdentity::STATUS_BLOCKED:
                return '<span class="label label-warning">' . UserIdentity::getStatusArray()[UserIdentity::STATUS_BLOCKED] . '</span>';
        }

        return false;
    }

    /**
     * Formats the value as a phone link.
     *
     * @param string $value   The value to be formatted.
     * @param array  $options The tag options in terms of name-value pairs. See [[Html::mailto()]].
     *
     * @return string the formatted result.
     */
    public function asPhone($value, array $options = [])
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        return Html::a(Html::encode($value), 'tel:' . $value, $options);
    }
}
