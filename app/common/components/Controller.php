<?php
namespace common\components;

use yii;
use yii\web\ForbiddenHttpException;
use base\BaseController as MainController;
use yii\filters\AccessControl;

/**
 * Базовый контроллер.
 */
abstract class Controller extends MainController
{
    /**
     * @inheritdoc
     */
    public $layout = 'main';

    /**
     * @var array Классы html-тега body.
     */
    public $classBody = [];

    /**
     * @inheritdoc
     */
    public function init()
    {

        parent::init();
        Yii::$app->language = 'ru';

        $this->view->title = html_entity_decode(\Yii::$app->name, null, \Yii::$app->charset);
    }

    /**
     * Возвращает class-атрибут html-тега body.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function classBody()
    {
        return !empty($this->classBody) ? ' class="' . implode(' ', $this->classBody) . '"' : '';
    }

    /**
     * @inheritdoc
     * @throws ForbiddenHttpException
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }
}
