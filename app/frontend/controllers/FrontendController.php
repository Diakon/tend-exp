<?php
namespace frontend\controllers;

use modules\crud\actions\DetailAction;
use common\models\Pages;
use frontend\controllers\actions\NewList;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use frontend\controllers\actions\Index;
use frontend\controllers\actions\TenderView;

/**
 * Основной контроллер.
 */
class FrontendController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => Index::class,
                'template' => 'index'
            ],
            'tender' => [
                'class' => TenderView::class,
                'template' => 'tender_view'
            ],
            'new_list' => [
                'class' => NewList::class,
                'params' => [ 'template' => 'new_list',]

            ],
            'new_view' => [
                'class' => DetailAction::class,
                'layout' => '@root/themes/frontend/views/layouts/news',
                'params' => [
                    'model' => Pages::class,
                    'template' => '@themes/frontend/views/frontend/new_view',
                ],
            ],
            'page_view' => [
                'class' => DetailAction::class,
                'layout' => '@root/themes/frontend/views/layouts/news',
                'params' => [
                    'model' => Pages::class,
                    'template' => '@themes/frontend/views/frontend/new_view',
                ],
            ],
        ];
    }

    /**
     * Обработка системных ошибок.
     *
     * @return string
     */
    public function actionError()
    {
        $this->layout = '404';
        /** @var \yii\base\Exception $exception */
        $exception = Yii::$app->errorHandler->exception;
        $handler = Yii::$app->errorHandler;

        $data['name'] = $exception->getName();
        $data['message'] = $exception->getMessage();
        $data['line'] = $exception->getLine();
        $data['file'] = $exception->getFile();
        $data['trace'] = $exception->getTraceAsString();
        $data['code'] = $exception->getCode();

        $code = $exception instanceof \yii\web\HttpException ? $exception->statusCode : $exception->getCode();

        if (Yii::$app->request->isAjax) {
            return print_r($data, true);
        } else {
            $this->view->title = $code . ' ' . $exception->getMessage();
            Yii::error(Json::encode($data), 'error');

            return $this->render('/exception/' . $code, ['data' => Json::encode($data)]);
        }
    }
}
