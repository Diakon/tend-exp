<?php
namespace frontend\controllers\actions;
use modules\crud\actions\BaseAction;
use Yii;


/**
 * Class Index.
 * @package frontend\controllers\actions
 */
class Index extends BaseAction
{
    public $template = '';

    public function run($partial = false)
    {
        return $this->renderAction($this->template);
    }

}
