<?php
namespace frontend\controllers\actions;
use modules\crud\actions\BaseAction;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use yii\web\NotFoundHttpException;


/**
 * Class TenderView.
 * Выводит тендер и его сметы
 *
 * @package frontend\controllers\actions
 */
class TenderView extends BaseAction
{
    public $template = '';

    public function run()
    {
        $this->controller->layout = $this->layout;
        $tenderId = Yii::$app->request->get('tenderId', 0);
        $model = Tenders::find()->where(['id' => (int)$tenderId])->andWhere(['status' => Tenders::STATUS_ACTIVE])->one();

        if (empty($model)) {
            throw new NotFoundHttpException('Tender not found');
        }


        return $this->renderAction($this->template, ['model' => $model]);
    }

}
