<?php

namespace frontend\controllers\actions;

use helpers\Dev;
use modules\crud\actions\ListAction;
use common\models\Pages;
use common\modules\company\models\frontend\Company;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class NewList.
 * @package frontend\controllers\actions
 */
class NewList extends ListAction
{
    /**
     * @var string
     */
    public $layout = '@root/themes/frontend/views/layouts/news';

    /**
     * @inheritdoc
     *
     * @throws NotFoundHttpException Not found http exception
     */
    public function run()
    {

        $model = new Pages();
        $this->controller->layout = $this->layout;
        $query = $model->filterShow(['type'=> Pages::TYPE_NEWS, 'is_important' => Pages::STATUS_INACTIVE]); // все неважные новости

        $importantNews = $model->filterShow(['type'=> Pages::TYPE_NEWS, 'is_important' => Pages::STATUS_ACTIVE])->orderBy(['date_published' => SORT_DESC])->limit(2)->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' =>  YII_ENV == 'dev' ? 10 : 10,
                'pageSizeParam' => false,
            ],
            'sort' => new Sort([
                'attributes' => [
                    'date_published' => [
                        'asc' => [Pages::tableName() . '.date_published' => SORT_ASC,],
                        'desc' => [Pages::tableName() . '.date_published' => SORT_DESC,],
                    ],

                ],
                'defaultOrder' => [
                    'date_published' => SORT_DESC,
                ],
            ]),
        ]);

        $isAjax = Yii::$app->request->isAjax;
        $page = intval(Yii::$app->request->post('loadPage', null));
        if ($isAjax) {
            $dataProvider->setPagination(['pageSize' =>  YII_ENV == 'dev' ? 10 : 10, 'pageSizeLimit' => [1, $dataProvider->totalCount], 'totalCount' => $dataProvider->totalCount]);
            $dataProvider->pagination->setPage($page);

            if ($page >= $dataProvider->pagination->pageCount) { // Если нет данных
                $dataProvider->pagination->setPage($page - 1);

                return Json::encode(['data' => '',
                    'pagination' => $this->controller->renderAjax('_new_pagination', ['dataProvider' => $dataProvider]),
                    'page' => $page,]);
            }
        }
        $title = Yii::t('app', 'Новости');
        $this->controller->view->params['breadcrumbs'][] = ['encode' => false, 'label' => '<span class="breadcrumbs__label">' . $title . '</span>'];
        $this->controller->view->title = $title;

        $view = $isAjax ? '_new_list_inner' : $this->params['template'];
        if ($isAjax) {
            return Json::encode([
                'data' => $this->controller->renderAjax($view,
                    [
                        'model' => $model,
                        'isAjax' => $isAjax,
                        'dataProvider' => $dataProvider,
                        'page' => $page,
                    ]),
                'pageCount' => $dataProvider->pagination->page+1,
                'pagination' => $this->controller->renderAjax('_new_pagination',
                    ['dataProvider' => $dataProvider,]),
            ]);
        }

        return $this->controller->render(
            $view, [
                'model' => $model,
                'isAjax' => $isAjax,
                'importantNews' => $importantNews ?? [],
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
