var LOGIN = {
    init: function (settings) {
        //Выношу конфиги
        LOGIN.config = {
            tokenMeta: 'meta[name=csrf-param]',
        };

        // Передаем сsrf токен в yii по ajax
        var csrfToken = $(LOGIN.config.tokenMeta).attr("content");
        var csrfValue = $(LOGIN.config.tokenMeta).attr("content");
        $.ajaxSetup({
            headers: {csrfToken: csrfValue}
        });

        $.extend(LOGIN.config, settings);

        LOGIN.loginPage();
    },

    /**
     * Действия на странице авторизации
     */
    loginPage: function () {
        $(document).on('click', '.js-login-change-form', function () {

        });
    }
};
LOGIN.init();
