var PROJECT = {
    init: function (settings) {
        //Выношу конфиги
        PROJECT.config = {
            tokenMeta: 'meta[name=csrf-param]'
        };

        // Передаем сsrf токен в yii по ajax
        var csrfToken = $(PROJECT.config.tokenMeta).attr("content");
        var csrfValue = $(PROJECT.config.tokenMeta).attr("content");
        $.ajaxSetup({
            headers: {csrfToken: csrfValue}
        });

        $.extend(PROJECT.config, settings);
        if( $('.js-address_same').prop('checked') == true) {
            $('.js-show-actual-address').css({'display': 'none'});
        } else {
            $('.js-show-actual-address').css({'display': 'block'});

        }
        PROJECT.tenderObject();
        PROJECT.tenders();
        PROJECT.showAddress();
        PROJECT.fileName();
        PROJECT.estimatesRubrics();
        PROJECT.offers();
        PROJECT.loadMore();
        PROJECT.removeFiles();
        PROJECT.tariff();
        PROJECT.company();
        PROJECT.filters();
    },


    fileName: function() {

        $(document).on('change', '.js-file-name', function(e) {
            $('.js-file-delete-text').hide();
            const file = e.target.files[0];
            $('.js-file-name-text').html(['name'].map(n => {
                return `<div>${file[n]}</div>`
            }).join(''));
        });

    },
    /**
     * Действия с фильтрами
     */
    filters: function() {
        // Скрывает / отображает работы в соответствии с выбраным в фильтре направлением работ
        $(document).on('click', '.js-works-filter-tender .form-group .js-multi-select', function () {
            var ids = {};
            $(this).find('.multi-select__item').each(function(index) {
                if ($(this).hasClass('selected')) {
                    ids[index] = parseInt($(this).data('value'));
                }
            });

            if (!jQuery.isEmptyObject(ids)) {
                $('.js-type-works-filter-tender').hide();
                var idsArray = $.map(ids, function(value, index) {
                    return [value];
                });
                $(idsArray).each(function(index, value) {
                    $('.js-type-works-filter-tender-id-' + value).show();
                });
            } else {
                $('.js-type-works-filter-tender').show();
            }
        });
    },
    
    /**
     * Действия на странице компаний
     */
    company: function() {
        $(document).on('click', '.js-add-new-user-in-team', function () {
            var count = parseInt($('.js-add-users-in-team-hidden-input').val());
            count = count + 1;
            $('.js-add-users-in-team-hidden-input').val(count);
            var pattern = $('.js-add-users-in-team-input-block').html();
            pattern = pattern.replace(/count-input/g, count);
            $('.js-add-user-popup-block').append(pattern);

            return false;
        });

        $(document).on('click', '.js-btn-remove-avatar', function () {
            $('.js-file-name-text').html('');
            $('.js-file-delete-text').show();
        });
    },

    tariff: function() {
        $(document).on('click', '.js-tariff-plan-month-input, .js-tariff-plan-user-input', function () {
           var typeTariff = $(this).data('type');
           var countMonth = $("#js-tariff-plan-form-" + typeTariff +" input[class='_input js-tariff-plan-month-input']:checked").val();
           var countUser = $("#js-tariff-plan-form-" + typeTariff +" input[class='_input js-tariff-plan-user-input']:checked").val();
           var price = $('.js-tariff-plan-price-' + typeTariff + '-' + countUser + '-' + countMonth).data('price');
           $('.js-selected-tariff-price-' + typeTariff).empty().append(price);
        });

    },

    removeFiles: function () {
        $('body').on('click', '.js-btn-remove-avatar', function () {

            $url = $(this).data('url');
            $.ajax({
                type: "POST",
                url: $url,
                dataType: 'json',
                data: 'avatarRemove=true',
                success: function (result) {

                    if (result.data == 'true') {
                        console.log(result);
                        $('.js-avatar').attr('src', result.src);
                    }
                },
            });
        });

        // Удаление документа со страницы /редактироания / добавления тендера
        $(document).on('click', '.js-btn-remove-document', function () {
            var url = '/upload/base/delete/';
            var block = $(this).closest('.js-upload-file-block');
            var id = block.find('.js-hidden-input-file-container-data').val();
       
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {'id': id},
                success: function (result) {
                    block.remove();
                },
            });
        });
    },

    /**
     * Действия с объектами тендера
     */
    tenderObject: function () {
        // Добавление ссылки на видео
        $(document).on('click', '.js-object-links-bnt-add', function () {
            var count = $('.js-object-links-count-input').val();
            var count = parseInt(count);
            count = count +1;
            $('.js-object-links-count-input').val(count);
            var pattern = $('.js-object-links-input').html();
            pattern = pattern.replace(/param-links-input/g, count);
            $('.js-links-block').append(pattern);

            return false;
        });

        // Добавление выполненой работы в объект тендера
        $(document).on('click', '.js-completed-works-add-btn', function () {
            var count = $('.js-count-completed-works-hidden-input').val();
            ++count;
            var completedWorksText = $('.js-completed-works-block-hidden-block').html();
            completedWorksText = completedWorksText.replace(/keyCloneId/g, count);
            completedWorksText = completedWorksText.replace(/keycloneid/g, count);
            completedWorksText = completedWorksText.replace(/js-drop-down-param-class/g, 'js-dropdown-box');
            $('.js-count-completed-works-hidden-input').val(count);
            $('.js-completed-works-block').append(completedWorksText);
            $('#objects-completedworkunitids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });
            $('#objects-completedworkworkids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });
            $('#company-completedworkworkids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });
            $('#company-completedworkunitids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });

            return false;
        });

        $(document).on('change', '.js-object-form-status-complete', function () {
            var id = parseInt($(this).val());
            if (id == 2) {
                $('.js-object-form-date-end').val('');
                $('.js-object-form-date-end').prop('disabled', 'disabled');
            } else {
                $('.js-object-form-date-end').prop('disabled', false);
            }
        });
    },

    showAddress: function() {
        $(document).on('change', '.js-address_same', function () {
            if( $('.js-address_same').prop('checked') == true) {
                $('.js-show-actual-address').css({'display': 'none'});
            } else {
                $('.js-show-actual-address').css({'display': 'block'});

            }

        });
    },

    loadMore: function () {
        $(document).on('click', '.load-more', function () {
            var $that = $(this);
            $loadPage = $that.data('loadPage');
            $type = $that.data('type');

            var $url = $that.data('ajax-url');
            $.ajax({
                type: 'POST',
                url: $url,
                dataType: 'json',
                data: {loadPage: $loadPage+1},
                success: function(result) {
                    $('.load-more').data('load-page', $loadPage+1);

                    if($type =='news') {
                        $('.js-news-block__row').append(result.data)
                    } else {
                        $that.before(result.data);
                    }

                    $('.pagination__inner').html(result.pagination);
                    console.log($that.data('loadPage'));

                    if (result.data=='' || result.pageCount == $that.data('pageCount')) {
                        $('.load-more').remove();
                    }
                },
                error:  function(xhr, str){
                    alert(str);
                }
            });

            return false;
        });
    },

    /**
     * Предложения для тендера
     */
    offers: function () {
        // Подтвердить / отклонить запрос скидки - вывод модалки
        $(document).on('click', '.js-cancel-discount-request, .js-accept-discount-request', function () {
            var id = $(this).data('id');
            var title = $(this).data('title');
            var type = $(this).data('type');
            var block = $('.js-tender-estimates-discount-request-block');
            block.find('.popup__title').empty().append(title);
            block.find('.js-estimates-discount-request-id-input').val(id);
            block.find('.js-estimates-discount-request-type-input').val(type);
            $('.js-estimates-discount-request-comment-input').val('');
            $('.js-estimates-discount-request-comment-input').removeClass('comment-error-request-border');

            $('.js-discountrequest-discount-request-btn-' + id).click();
        });

        // Подтвердить / отклонить запрос скидки - подтверждение действия на экране модалки
        $(document).on('click', '.js-estimates-discount-request-resolution-btn',function () {
            var id = parseInt($('.js-estimates-discount-request-id-input').val());
            var type = $('.js-estimates-discount-request-type-input').val();
            var comment = $('.js-estimates-discount-request-comment-input').val();

            if (type == 'cancel' && comment.length == 0) {
                $('.js-estimates-discount-request-comment-input').addClass('comment-error-request-border');
            } else {
                $('.js-estimates-discount-request-comment-input').removeClass('comment-error-request-border');
                $('.js-discount-request-id-' + id).val(id);
                $('.js-discount-request-comment-' + id).val(comment);
                $('.js-discount-request-type-' + id).val(type == 'cancel' ? 0 : 1);
                $('.js-estimates-discount-request-close-btn').click();
            }
        });

        // Подтвердить / отклонить запрос скидки -закрытие окна экрана модалки
        $(document).on('click', '.js-estimates-discount-request-close-btn', function () {
            $('.js-tender-estimates-discount-request-block').hide();
        });


        // Скрыть - раскрыть условия
        $(document).on('click', '.js-offer-conditions-title', function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
            $(this).parent().find('.section-lk__acco-body').slideToggle();
        });


        // Скрыть - раскрыть список контактов
        $(document).on('click', '.js-compare-contact-info-btn', function () {
            $(this).parent().find('.js-compare-contact-info-block').slideToggle();
            return false;
        });

        // Добавить / убрать из сравнения
        $(document).on('click', '.js-compare', function () {
            var elementId = $(this).data('id');
            var element = $('.js-compare[data-id=' + elementId + ']');
            var url = $(this).data('url');
            var model = $(this).data('model');
            var id = $(this).data('id');
            var tenderId = $(this).data('tender');
            var className = 'btn-add--active';

            if ($(this).hasClass('js-compare-item-remove')) {
                $(document).find('.js-offer-compare-data-' + elementId).remove();
            } else {
                var countCompare = parseInt($('.js-count-compare-offers-input').val());

                if(element.hasClass(className)) {
                    --countCompare;
                    element.removeClass(className);
                } else {
                    ++countCompare;
                    element.addClass(className);
                }
                $('.js-count-compare-label').html(countCompare);
            }
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {id: id, model: model, tenderId: tenderId},
                success: function(result) {
                    $('.js-count-compare-offers-input').val(countCompare);
                    if (countCompare > 0) {
                        $('.js-count-compare-block').show();
                    } else {
                        $('.js-count-compare-block').hide();
                    }
                },
                error:  function(xhr, str){
                    alert(str);
                }
            });


            return false;
        });

        // Удаляет из сравнения предложение (страница подробного сравнения предложеий)
        $(document).on('click', '.js-compare-offer-remove', function () {
            var url = $(this).data('url');
            var model = $(this).data('model');
            var id = $(this).data('id');
            var tenderId = $(this).data('tender');
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {id: id, model: model, tenderId: tenderId},
                success: function(result) {
                    // Убрали из сравнения - убираем с экрана

                },
                error:  function(xhr, str){
                    alert(str);
                }
            });

            return false;
        });

        // Получить данные о предложених к тендеру
        $(document).on('click', '.js-show-more-info-tender-offers-btn', function () {
            var tenderId = $(this).data('tender');
            $.ajax({
                type: 'POST',
                url: location.href,
                dataType: 'json',
                data: {'OffersTenderInfo[tenderId]': tenderId},
                error: function (data) {
                    alert('Ошибка получения данных.');
                }
            }).done(function (data) {
                var json = $.parseJSON(data);
                $('.js-show-more-info-tender-offers-' + tenderId).empty().append(json.result).slideToggle();
            });

            return false;
        });

        // Пересчет суммы предложения
        $(document).on('click', '.js-offer-recount-btn', function () {
            var url = $(this).data('url');
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: $('#js-add-offer-form').serialize(),
                success: function(result) {
                    // Обновляю сумму в ИТОГО
                    $('.js-offer-total-sum-no-discount-text').empty().append(result.sum.totalOfferSum.noDiscount);
                    // Обновляю сумму в рубриках
                    $.each(result.sum.rubrics, function(index, value) {
                        $('.js-offer-sum-with-discount-' + index).empty().append(value.formatterWithDiscount);
                        $('.js-offer-sum-no-discount-' + index).empty().append(value.formatterNoDiscount);
                    });
                    // Обновляю сумму в работах
                    $.each(result.sum.elements, function(index, value) {
                        $('.js-offer-sum-element-with-discount-' + index).empty().append(value.formatterWithDiscount);
                        $('.js-offer-sum-element-no-discount-' + index).empty().append(value.formatterNoDiscount);
                        // Высчитываю стоимость за 1 штуку - делим сумму на кол-во
                        if (parseInt(value.noDiscount) > 0) {
                            var divBlock = $('.js-offer-sum-element-one-no-discount-' + index);
                            var count = parseInt(divBlock.data('count'));
                            var priceOneElement = value.noDiscount / count;

                            // Формализую вид цены
                            var sum = priceOneElement.toFixed(2);
                            sum = String(sum).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
                            sum = sum.replace(',', '.');

                            divBlock.empty().append(sum);
                        }
                    });
                    // Обновляю сумму в доп. работах
                    $.each(result.sum.addWorks, function(index, value) {
                        $('.js-offer-sum-add-work-no-discount-' + index).empty().append(value.formatterNoDiscount);
                        // Высчитываю стоимость за 1 штуку - делим сумму на кол-во
                        if (parseInt(value.noDiscount) > 0) {
                            var count = parseInt($('.js-add-work-count-input-' + index).val());
                            //var count = $('#offerform-addworks-' + index + '-count').val();
                            count = parseInt(count);
                            var priceOneElement = value.noDiscount / count;
                            $('.js-offer-sum-element-with-discount-' + index).empty();
                            if (priceOneElement.toFixed(2) != 'Infinity') {
                                // Формализую вид цены
                                var sum = priceOneElement.toFixed(2)
                                sum = String(sum).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
                                sum = sum.replace(',', '.');

                                $('.js-offer-sum-add-work-one-no-discount-' + index).empty().append(sum);
                            }
                        }
                    });
                }
            });

            return false;
        });

        // Кнопка "Добавить доп. работу" для добавления дополнительной работы на страние предложения к тендеру
        $(document).on('click', '.js-offer-add-work-btn', function () {
            var count = $('.js-offer-add-additional-work-hidden-count').val();
            ++count;
            var addWorksText = $('.js-offer-additional-work-hidden-block').html();
            addWorksText = addWorksText.replace(/keyCloneId/g, count);
            addWorksText = addWorksText.replace(/keycloneid/g, count);
            addWorksText = addWorksText.replace(/js-drop-down-param-class/g, 'js-dropdown-box');
            $('.js-offer-add-additional-work-hidden-count').val(count);
            $('.js-offer-add-work-main-block').append(addWorksText);
            $('#offerform-addworks-' + count +'-unit_id').dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });

            return false;
        });
        // Кнопка "Удалить доп. работу" для удаления дополнительной работы на странице предложения к тендеру
        $(document).on('click', '.js-offer-add-work-delete', function () {
            var id = $(this).data('id');
            $('.js-offer-add-work-block-' + id).remove();

            return false;
        });
        // Добавить коментарий в предложении
        $(document).on('click', '.js-offer-add-estimates-comment', function () {
            var rubricId = $(this).data('entity-id');
            var comment = $('.js-comment-block').val();
            $('.js-add-estimates-comment-' + rubricId).val(comment);

            return false;
        });
        //Назначаю выставленое значение запроса скидки
        $(document).on('click', '.js-estimates-discount-btn', function () {
            var rubricId = $('.js-discount-hidden-input').val();
            var discount = $('.js-discount-input').val();
            $('.js-add-estimates-discount-' + rubricId).val(discount);
            // Получаю ID всех подчененных категорий - выставляю там нулевой запрос скидки
            $.each(getRubrics(rubricId + 'q'), function(index, value) {
                var id = parseInt(value.replace(/\D+/g,""));
                if (id != rubricId) {
                    $('.js-add-estimates-discount-' + id).val(null);
                }
            });
            $('form').submit();
        });

        // Возвращает ID подчиненных категорий
        function getRubrics(id) {
            let $box = $('.js-acco-work');
            let arrId = [id];

            function findChild(id) {
                let $item = $box.find(`.js-acco-work-item[data-id=${id}]`);
                let $children = $box.find(`.js-acco-work-item[data-parent-id=${id}]`);

                $children.each(function (index, elem) {
                    arrId.push($(elem).data('id'));

                    if($children.length !== 0){
                        findChild($(elem).data('id'));
                    }
                });
            }

            findChild(id);

            return arrId;
        }
    },

    /**
     * Действия с тендерами
     */
    tenders: function () {
        // Добавить / убрать тендер из избраного
        $(document).on('click', '.js-add-tender-to-favorites', function () {
            var url = $(this).data('url');
            var tenderId = $(this).data('tender');
            var className = 'btn-add--active';
            if ($(this).hasClass(className)) {
                $(this).removeClass(className);
            } else {
                $(this).addClass(className);
            }

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {'tenderFavorites': tenderId},
                error: function (data) {
                    alert('Ошибка добавления тендера в избранное.');
                }
            }).done(function (data) {
            });

            return false;
        });

        // Отправляет запрос на сервер с выбором победителя тендера
        $(document).on('click', '.js-select-tender-offer-winner', function () {
            var url = $(this).data('url');
            var offerId = $('.js-offer-id-hidden-input').val();
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {'winnerOfferId': offerId},
                error: function (data) {
                    Toast.error('Ошибка сервера!');
                    $('.js-close-wnd').click();
                }
            }).done(function (data) {
                $('.js-close-wnd').click();
                if (data.status == 'success') {
                    Toast.success(data.msg);
                    $('.js-winner-btn-icon').hide();
                } else {
                    Toast.error(data.msg);
                }
            });

            return false;
        });
        // Переключение вида добавления объекта в тендере
        $(document).on('click', '.js-object-in-tender-btn', function () {
            $(this).hide();
            $('.js-create-object-in-tender-label').hide();
            $('.js-create-object-in-tender-block').hide();
            var type = parseInt($(this).data('type'));
            // Создают объект в тендере
            if (type == 1) {
                $('.js-cancel-create-object-in-tender').show();
                $('.js-create-object-in-tender-block').show();
            } else {
                $('.js-create-object-in-tender').show();
                $('.js-create-object-in-tender-label').show();
            }
            $('.js-create-object-in-tender-hidden-input').val(type);

            return false;
        });

        // Добавить работы в создании тендера
        $(document).on('click', '.js-chars-works-add-btn', function () {
            var count = $('.js-count-chars-works-hidden-input').val();
            ++count;
            var worksText = $('.js-chars-works-block-hidden-block').html();
            worksText = worksText.replace(/keyCloneId/g, count);
            worksText = worksText.replace(/keycloneid/g, count);
            worksText = worksText.replace(/js-drop-down-param-class/g, 'js-dropdown-box');
            $('.js-count-chars-works-hidden-input').val(count);
            $('.js-chars-works-block').append(worksText);
            $('#objects-completedworkunitids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });
            $('#objects-completedworkworkids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });
            $('#company-completedworkworkids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });
            $('#company-completedworkunitids-' + count).dropdown({
                prefix: $(this).data('prefix'),
                inline: $(this).hasClass('inline')
            });

            return false;
        });

        // Убирает с формы блок работ
        $(document).on('click', '.js-tender-chars-works-delete-btn', function () {
            var key = $(this).data('key');
            $('.js-tender-chars-works-block-' + key).remove();

            return false;
        });

        // Выбрали направление работ - показать / скрыть 2 нижних пункта в зависимости от выбранного значения
        $(document).on('change', '.js-tender-chars-works-main-work-input', function () {
            var key = parseInt($(this).data('key'));
            var val = parseInt($(this).val());
            if (val > 0) {
                var selector = tenderGetCharSelect('typeWork', val);
                $('.js-tender-chars-works-type-work-input-' + key).empty().append(selector);
                $('.js-tender-chars-works-type-work-block-' + key).show();
            } else {
                $('.js-tender-chars-works-work-block-' + key).hide();
                $('.js-tender-chars-works-type-work-block-' + key).hide();
                $('.js-tender-chars-works-work-input-' + key).val('');
                $('.js-tender-chars-works-type-work-input-' + key).val('');
            }
        });

        // Выбрали группы работ - показать / скрыть 1 нижних пункт в зависимости от выбранного значения
        $(document).on('change', '.js-tender-works-type-work-input', function () {
            var key = parseInt($(this).data('key'));
            var val = parseInt($(this).val());
            if (val > 0) {
                var selector = tenderGetCharSelect('work', val);
                $('.js-tender-chars-works-work-input-' + key).empty().append(selector);
                $('.js-tender-chars-works-work-block-' + key).show();
            } else {
                $('.js-tender-chars-works-work-block-' + key).hide();
                $('.js-tender-chars-works-work-input-' + key).val('');
            }
        });

        // Возращает HTML код для селектора выбора работ при создании тендера
        function tenderGetCharSelect(type, parentId) {
            var block = type == 'work' ? $('.js-tender-chars-select-data-work-' + parentId) : $('.js-tender-chars-select-data-type-work-' + parentId);
            var selectOption = '<option value=""></option>';
            block.each(function() {
                var id = parseInt($(this).data('id'));
                var title = $(this).data('title');
                selectOption += '<option value="' + id + '">' + title + '</option>';
            });

            return selectOption;
        }
    },

    /**
     * Действия с рубриками смет
     */
    estimatesRubrics: function () {
        // Функция отправляет тип действия над рубрикой сметы
        function editRubric(type, rubricId, params, $this) {
            var urlLink = $('.js-estimates-url-link-ajax').val();
            $.ajax({
                type: 'POST',
                url: urlLink,
                dataType: 'json',
                data: {'EstimatesRubrics[type]': type, 'EstimatesRubrics[rubricId]': rubricId, 'EstimatesRubrics[params]': params},
                error: function (data) {
                    alert('Ошибка сохранения сметы. Изменения не внесены.');
                },
                success: function (data) {
                    var pattern = $('.js-estimates-clone-block').html();
                    pattern = pattern.replace(/idParam/g, data.result.id);
                    pattern = pattern.replace(/parentParam/g, data.result.parent_id);
                    pattern = pattern.replace(/titleParam/g, data.result.title);
                    pattern = pattern.replace(/depthParam/g, data.result.depth);
                    switch(type) {
                        // Удалить
                        case 'delete':
                            accoWorkRemoveCategory(rubricId + 'q');
                            break;
                        // Добавить пункт снизу
                        case 'down':
                            var css = 'padding-left: ' + ((parseInt(data.result.depth) - 1) * 20) + 'px;';
                            if (!$($this).closest('.js-acco-work-item').hasClass('active')) {
                                css += 'display: none;';
                            }
                            pattern = pattern.replace(/styleParam/g, css);
                            pattern = pattern.replace(/display: none/g, '');
                            pattern = pattern.replace(/showStyleCss/g, 'display: none');
                            $($this).closest('.js-acco-work-item').after(pattern);
                            break;
                        // Добавить пункт сверху
                        case 'up':
                            var css = 'padding-left: ' + ((parseInt(data.result.depth) - 1) * 20) + 'px;';
                            if (!$($this).closest('.js-acco-work-item').hasClass('active')) {
                                css += 'display: none;';
                            }
                            pattern = pattern.replace(/styleParam/g, css);
                            pattern = pattern.replace(/display: none/g, '');
                            pattern = pattern.replace(/showStyleCss/g, 'display: none');
                            $($this).closest('.js-acco-work-item').before(pattern);
                            break;
                        // Новый раздел
                        case 'addParent':
                            pattern = pattern.replace(/display: none/g, '');
                            pattern = pattern.replace(/showStyleCss/g, 'display: none');
                            $('.js-estimates-work-block').append(pattern);
                            break;
                        // Добавить дочерний пункт
                        case 'addChildren':
                            var css = 'padding-left: ' + ((parseInt(data.result.depth) - 1) * 20) + 'px;';
                            if (!$($this).closest('.js-acco-work-item').hasClass('active')) {
                                css += 'display: none;';
                            }
                            pattern = pattern.replace(/styleParam/g, css);
                            pattern = pattern.replace(/showStyleCss/g, 'display: none');
                            $($this).closest('.js-acco-work-item').after(pattern);
                            break;
                    }
                }
            });

        }

        // Функция для добавдения / редактирования / удаления работы сметы
        function editElement(type, rubricId, elementId, model, divBlock) {
            var urlLink = $('.js-estimates-url-link-ajax').val();
            $.ajax({
                type: 'POST',
                url: urlLink,
                dataType: 'json',
                data: {'EstimatesElements[type]': type, 'EstimatesElements[rubricId]': rubricId, 'EstimatesElements[model]': model},
                error: function (data) {
                    alert('Ошибка сохранения сметы. Изменения не внесены.');
                }
            }).done(function (data) {
                switch (type) {
                    case "deleteWork":
                        $(divBlock).remove();
                        break;
                    case 'saveWork':
                        break;
                    case 'addWork':
                        var blockRubric = $('#js-work-rows-block-' + rubricId);
                        blockRubric.load(document.location.href + ' #js-work-rows-block-' + rubricId);
                        // Если рубрика свернута - выдаем сообщение, что операция прошла успешно в виде тоста
                        if (!blockRubric.closest('.js-acco-work-item').hasClass('active')) {
                            blockRubric.closest('.js-acco-work-item').click();
                        }

                        break;
                    default:
                        $(divBlock).load(document.location.href + ' ' + divBlock);
                        break;
                }
            });
        }

        // Изменение названия  рубрики сметы
        $(document).on('change', '.js-estimates-rubric-edit', function () {
            var actionType = 'edit';
            var rubricId = $(this).data('id');
            var params = {};
            // Если редактирование - добавляем параметры
            if (actionType == 'edit') {
                params['title'] = $('.js-estimates-rubric-edit-title-' + rubricId).val();
                params['discount'] = $('.js-estimates-rubric-edit-discount-' + rubricId).val();
            }

            editRubric(actionType, rubricId, params, this);
        });

        // Удаление рубрики работы
        $(document).on('click', '.js-remove-heading', function () {
            var rubricId = $('.js-estimates-rubric-delete-id').val();
            rubricId =  parseInt(rubricId.replace(/\D+/g));
            var  params = {};
            var actionType = 'delete';
            editRubric(actionType, rubricId, params, this);

            return false;
        });

        // Обработка кнопок добавления выше, добавления ниже, редактирования, удаления рубрики сметы
        $(document).on('click', '.js-edit-estimates-rubric', function () {
            var actionType = $(this).data('action');
            var rubricId = $(this).data('id');
            var  params = {};
            // Если редактирование - показываем импут ввода
            if (actionType == 'edit') {
                $('.js-estimates-rubric-edit-title-' + rubricId).show();
                $('.js-estimates-rubric-title-' + rubricId).hide();
                return false;
            }

            editRubric(actionType, rubricId, params, this);

            return false;
        });

        // Возвращает массив с данными о работе
        function getEstimatesWorkModelData(divBlock) {
            var fields = ['id', 'title', 'count', 'unit_id', 'price_work', 'price_material', 'discount'];

            var model = {};
            $(fields).each(function(index, element){
                model[element] = $(divBlock).find('.js-estimates-elements-' + element).val();
            });

            return model;
        }

        // Изменение данных работы сметы
        $(document).on('change', '.js-edit-estimates-element', function () {
            var rubricId = $(this).data('rubric');
            var actionType = 'saveWork';
            var id = $(this).data('id');
            var divBlock = '#js-estimates-elements-block-id-' + id;
            var model = getEstimatesWorkModelData(divBlock);
            editElement(actionType, rubricId, id, model, divBlock);

            return false;
        });

        // Удаление работы сметы
        $(document).on('click', '.js-delete-estimates-element', function () {
            var rubricId = $(this).data('rubric');
            var actionType = 'deleteWork';
            var id = $(this).data('id');
            var divBlock = '#js-estimates-elements-block-id-' + id;
            var model = getEstimatesWorkModelData(divBlock);
            editElement(actionType, rubricId, id, model, divBlock);

            return false;
        });

        // Добавление новой работы сметы
        $(document).on('click', '.js-add-new-estimates-element', function () {
            var rubricId = $(this).data('rubric');
            var actionType = $(this).data('action');
            var id = $(this).data('id');
            // Если кнопка добавления  новой работы
            var divBlock = '#js-estimates-elements-hidden-block';
            var model = getEstimatesWorkModelData(divBlock);
            editElement(actionType, rubricId, id, model, divBlock);

            return false;
        });

        // Добавить новый коментарий
        $(document).on('click', '.js-add-estimates-comment-btn', function () {
            var rubricId = $('.js-comment-rubric-id-hidden-input').val();
            var comment = $('.js-estimates-comment-input').val();
            $('.js-add-estimates-comment-close-btn').click();

            // Если коментарий для доп. работы, то не отправляем на сервер, а вставляем в скрытый инпут на форме (доп. работа может еще не быть сохранена на момент создания коментария)
            if ( (rubricId.indexOf("addWork")) > 0) {
                $('.js-offer-add-work-comment-' + rubricId).val(comment);

                return false;
            }

            $.ajax({
                type: 'POST',
                url: $(this).data('url'),
                dataType: 'json',
                data: {'EstimatesRubricsComment[id]': rubricId, 'EstimatesRubricsComment[comment]': comment},
                error: function (data) {
                    alert('Ошибка сохранения коментария.');
                }
            }).done(function (data) {
                $('.js-tender-estimates-add-comment-block').hide();
                $('#js-estimates-rubric-block-item-' + rubricId).load(document.location.href + ' #js-estimates-rubric-block-item-' + rubricId);
            });

            return false;
        });
        // Кнопка "отменить" в добавлении коментария
        $(document).on('click', '.js-add-estimates-comment-close-btn', function () {
            $('.js-tender-estimates-add-comment-block').hide();
        });

        // Прочитать оставленные ранее коментарии
        $(document).on('click', '.js-estimates-comments-show', function () {
            var text = '';
            $($(this).parent().find('.js-estimates-comment-text')).each(function(index) {
                var user = $(this).data('user');
                var comment = $(this).data('comment');

                text += (user + ': ' + comment + "\n");
            });

            alert(text);

            return false;
        });


    }
};
PROJECT.init();
