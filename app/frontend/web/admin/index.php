<?php
const APP_NAME = 'backend';
const APP_TYPE = 'yii\web\Application';

$root = dirname(dirname(dirname(dirname(__DIR__))));

require $root . '/config/check_debug.php';
require $root . '/vendor/autoload.php';
require $root . '/vendor/yiisoft/yii2/Yii.php';

$config = require $root . 'config/init.php';
unset($config['assetManager']);
(new ReflectionClass(APP_TYPE))->newInstance($config)->run();
