<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Ресурсы приложения.
 */
class AppAsset extends AssetBundle
{

    /**
     * @var string the Web-accessible directory that contains the asset files in this bundle.
     */
    public $basePath = '@webroot';

    /**
     * @var string the Web-accessible directory that contains the asset files in this bundle.
     */
    public $baseUrl = '/html';


    /**
     * @var array list of CSS files that this bundle contains.
     */
    public $css = [
        '../css/style.css',
        'dist/static/css/main.css',
        '../css/toast.css',
    ];

    /**
     * @var array list of JS files that this bundle contains.
     */
    public $js = [
       // 'dist/static/js/all.min.js',
        '../js/script.js',
        'dist/static/js/toast.js',
        'dist/static/js/rangeslider.js',
    ];

    /**
     * @var array list of JavaScript files that this bundle contains.
     */
    public $depends = [
        'frontend\assets\StaticAsset',
    ];

    /**
     * @var array the options to be passed to [[AssetManager::publish()]] when the asset bundle is being published.
     */
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
