<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Ресурсы для страницы 404.
 */
class ErrorAsset extends AssetBundle
{

    /**
     * @var string the Web-accessible directory that contains the asset files in this bundle.
     */
    public $basePath = '@webroot';

    /**
     * @var string the Web-accessible directory that contains the asset files in this bundle.
     */
    public $baseUrl = '/html';


    /**
     * @var array list of CSS files that this bundle contains.
     */
    public $css = [
        'dist/static/css/404.css',
    ];

    /**
     * @var array list of JS files that this bundle contains.
     */
    public $js = [
        // 'dist/static/js/all.min.js',
    ];

    /**
     * @var array list of JavaScript files that this bundle contains.
     */
    public $depends = [
        'frontend\assets\StaticAsset',
    ];

    /**
     * @var array the options to be passed to [[AssetManager::publish()]] when the asset bundle is being published.
     */
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
