<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Ресурсы приложения.
 */
class StaticAsset extends AssetBundle
{
    /**
     * @var string the Web-accessible directory that contains the asset files in this bundle.
     */
    public $sourcePath = '@root/themes/frontend';

    /**
     * @var array list of CSS files that this bundle contains.
     */
    public $css = [
        //'static/css/example.css',
    ];

    /**
     * @var array list of JavaScript files that this bundle contains.
     */
    public $js = [
//        'static/js/example.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
       // 'assets\CmsHelperAsset',
    ];

    /**
     * @var array the options to be passed to [[AssetManager::publish()]] when the asset bundle is being published.
     */
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
