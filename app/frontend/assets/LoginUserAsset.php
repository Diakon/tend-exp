<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Ресурсы приложения.
 */
class LoginUserAsset extends AssetBundle
{
    /**
     * @var string the Web-accessible directory that contains the asset files in this bundle.
     */
    public $basePath = '@root/themes/frontend';

    /**
     * @var string the base URL that will be prefixed to the asset files for them to be accessed via Web server.
     */
    public $baseUrl = '@static';

    /**
     * @var array list of CSS files that this bundle contains.
     */
    public $css = [
        //'../../../css/style.css'
    ];

    /**
     * @var array list of JavaScript files that this bundle contains.
     */
    public $js = [
        'js/script-login.js',
    ];

    /**
     * @var array list of JavaScript files that this bundle contains.
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    /**
     * @var array the options to be passed to [[AssetManager::publish()]] when the asset bundle is being published.
     */
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
