<?php

namespace frontend\components;

use common\components\ClassMap;
use DomainException;
use common\models\EntityFile;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Trait CheckTHashTrait
 * @package frontend\modules\request\components
 */
trait CheckTHashTrait
{


    /**
     * Проверяем наличие загруженных временных файлов
     *
     * @param Event $event
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function processTHashEntityFile(Event $event)
    {
        /** @var self $model */
        $model = $event->sender;
        $tHash = ArrayHelper::getValue($event->data, 'tHash');
        $update = EntityFile::updateAll(['modelId' => $model->id], ['modelName' => ClassMap::getId(get_class($model)), 'modelId' => $tHash, 'modelAttribute' => 'files']);

        /**
         * @var $fileModel EntityFile
         */
        $fileModel = EntityFile::findOne(['modelName' => ClassMap::getId(get_class($model)), 'modelId' => $model->id, 'modelAttribute' => $model::MODEL_ATTRIBUTE_FILES]);

        if ($fileModel) {
            $rename = true;
            if (is_dir($fileModel->getBaseDir() . '/' . $tHash) && $update > 0) {
                $rename = @rename($fileModel->getBaseDir() . '/' . $tHash, $fileModel->getBaseDir() . '/' . $model->id);
            }
            if (!$rename) {
                Yii::error('Ошибка переименования временной папки с ' . $fileModel->getBaseDir() . '/' . $tHash . ' на ' .
                    $fileModel->getBaseDir() . '/' . $model->id, 'request');
            }
        }
    }

    /**
     * Проверяем наличие временных сообщений
     *
     * @param Event $event
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function processTHashSessionMessageData(Event $event)
    {
        $model = $event->sender;
        $tHash = ArrayHelper::getValue($event->data, 'tHash');
        $data = self::getFromSession($tHash, Dialog::SESSION_KEY);
        $countData = count($data);

        if ($countData > 0) {
            $dialog = Dialog::find()->where(['typeItem' => ClassMap::getId($model), 'typeItemId' => $model->id])->one();

            if (!$dialog instanceof Dialog) {
                $dialog = new Dialog();
                $dialog->setScenario(Dialog::SCENARIO_SEND_MESSAGE);
                $dialog->setAttribute('status', DialogMsg::STATUS_NEW);
                $dialog->setAttribute('typeItem', ClassMap::getId($model));
                $dialog->setAttribute('typeItemId', $model->id);

                if (!$dialog->save()) {
                    throw new DomainException(Html::errorSummary($dialog, ['header' => false]));
                }
            }

            for ($i = 0; $i < $countData; $i++) {
                $dataItem = $data[$i];

                $dialogMsg = new DialogMsg();
                $dialogMsg->setScenario(DialogMsg::SCENARIO_SEND_MESSAGE);
                $dialogMsg->setAttributes($dataItem);
                $dialogMsg->setAttribute('dialogId', $dialog->id);
                $dialogMsg->setAttribute('status', DialogMsg::STATUS_NEW);

                if (!$dialogMsg->save()) {
                    throw new DomainException(Html::errorSummary($dialogMsg, ['header' => false]));
                }
            }
        }
    }

    /**
     * Очистка сессии
     *
     * @param Event $event
     */
    public function flushTHash(Event $event)
    {
        $tHash = ArrayHelper::getValue($event->data, 'tHash');

        if (!$this->hasErrors()) {
            self::flushSession($tHash);
        }
    }

    /**
     * Запись данных в сессию
     *
     * @param      $tHash
     * @param      $sessionKey
     * @param      $data
     * @param bool $replace Замена элемента
     */
    public static function setToSession($tHash, $sessionKey, $data, $replace = false)
    {
        $session = Yii::$app->session;
        $existsSessionThash = $session->get(Yii::$app->params['session_key_thash'], []);

        $existsSessionElement = ArrayHelper::getValue($existsSessionThash, $tHash, []);
        $existsSessionElementItem = ArrayHelper::getValue($existsSessionElement, $sessionKey, []);

        $existsToSessionElementItem = $replace ? $data : ArrayHelper::merge($existsSessionElementItem, $data);
        ArrayHelper::setValue($existsSessionElement, $sessionKey, $existsToSessionElementItem);
        ArrayHelper::setValue($existsSessionThash, $tHash, $existsSessionElement);
        $session->set(Yii::$app->params['session_key_thash'], $existsSessionThash);
    }

    /**
     * Получение данных из сессии
     *
     * @param       $tHash
     * @param       $sessionKey
     * @param array $default
     *
     * @return array
     */
    public static function getFromSession($tHash, $sessionKey, $default = []): array
    {
        $session = Yii::$app->session;
        $existsSessionThash = $session->get(Yii::$app->params['session_key_thash'], []);
        $existsSessionElement = ArrayHelper::getValue($existsSessionThash, $tHash, []);

        return ArrayHelper::getValue($existsSessionElement, $sessionKey, $default);
    }

    /**
     * Удаление данных из сесии
     *
     * @param $tHash
     * @param $sessionKey
     */
    public static function flushSession($tHash, string $sessionKey = null)
    {
        $session = Yii::$app->session;
        if (!is_null($sessionKey)) {
            $existsSessionThash = $session->get(Yii::$app->params['session_key_thash'], []);

            $existsSessionElement = ArrayHelper::getValue($existsSessionThash, $tHash, []);
            ArrayHelper::remove($existsSessionElement, $sessionKey);
            $session->set(Yii::$app->params['session_key_thash'], [$tHash => $existsSessionElement]);
        } else {
            $existsSessionThash = $session->get(Yii::$app->params['session_key_thash'], []);
            ArrayHelper::remove($existsSessionThash, $tHash, []);
            $session->set(Yii::$app->params['session_key_thash'], $existsSessionThash);
        }

    }
}
