<?php
namespace frontend\widgets;

use helpers\Dev;
use common\modules\company\models\frontend\Company;
use common\modules\tender\models\frontend\Tenders;
use common\modules\company\models\frontend\CompanyUsers;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class TenderosInfo.
 * Возвращает информацию о количестве компаний в каталоге, тендеров, заказчиков
 *
 * @package frontend\widgets
 */
class TenderosInfo extends Widget
{
    public $view;

    public function run()
    {
        $param = [];
        $param['countCompany'] = Company::find()->where(['status' => Company::STATUS_ACTIVE])->count();
        $param['countActiveTenders'] = Tenders::find()->where(['in', 'status', [Tenders::STATUS_COLLECT_OFFERS, Tenders::STATUS_CHOICE_WINNER]])->count();
        $param['countCompanyUsers'] = CompanyUsers::find()->where(['status' => CompanyUsers::STATUS_ACTIVE])->groupBy(['user_id'])->count();

        return $this->render($this->view, $param);
    }
}
