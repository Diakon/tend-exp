<?php
namespace frontend\widgets;

use yii\base\Widget;
use backend\modules\slider\models\frontend\UniqueFeatures as BaseUniqueFeatures;

/**
 * Class UniqueFeatures.
 *
 * @package frontend\widgets
 */
class UniqueFeatures extends Widget
{
    public $view;

    public function run()
    {
        $uniqueFeatures = BaseUniqueFeatures::find()->where(['publish' => BaseUniqueFeatures::STATUS_ACTIVE])->all();

        return $this->render($this->view,
            [
                'uniqueFeatures' => $uniqueFeatures
            ]
        );
    }

}
