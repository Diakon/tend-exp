<?php
namespace frontend\widgets;

use modules\pages\models\common\Pages;
use yii\base\Widget;
use modules\slider\models\backend\Slider as Slider;

/**
 * Class News.
 *
 * @package frontend\widgets
 */
class News extends Widget
{
    public $view;

    public function run()
    {
        $newsModel = new \common\models\Pages();
        $news = $newsModel->filterShow(['type'=> Pages::TYPE_NEWS, 'is_important' => Pages::STATUS_INACTIVE])->limit(9)->all(); // неважные новости
        $newsImportant = $newsModel->filterShow(['type'=> Pages::TYPE_NEWS, 'is_important' => Pages::STATUS_ACTIVE])->limit(1)->all(); // неважные новости
        return $this->render($this->view,
            [
                'news' => $news,
                'newsImportant' => $newsImportant,
            ]
        );
    }

}
