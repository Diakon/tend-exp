<?php
namespace frontend\widgets;

use yii\base\Widget;
use modules\slider\models\backend\Slider as Slider;

/**
 * Class Slider.
 *
 * @package frontend\widgets
 */
class Slider extends Widget
{
    public $view;

    public function run()
    {
        $sliders = Slider::find()->where(['publish' => Slider::STATUS_ACTIVE])->all();

        return $this->render($this->view,
            [
                'sliders' => $sliders
            ]
        );
    }

}