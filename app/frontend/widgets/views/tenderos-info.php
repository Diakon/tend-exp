<div class="home-slider__digits">
    <div class="home-slider__digit">
        <span class="home-slider__digit_title"><?= $countCompany ?></span>
        <span class="home-slider__digit_text">компаний в каталоге</span>
    </div>
    <div class="home-slider__digit">
        <span class="home-slider__digit_title"><?= $countActiveTenders ?></span>
        <span class="home-slider__digit_text">активных тендеров</span>
    </div>
    <div class="home-slider__digit">
        <span class="home-slider__digit_title"><?= $countCompanyUsers ?></span>
        <span class="home-slider__digit_text">довольных заказчиков</span>
    </div>
</div>

