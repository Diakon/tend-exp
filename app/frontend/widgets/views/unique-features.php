<?php

?>
<div class="home-section__inner">
    <header class="home-section__header">
        <h2 class="home-section__header_title"><?= Yii::t('app', 'Уникальные возможности Tenderos')?></h2>
    </header>

    <div class="home-demo">
        <div class="home-demo__tabs">
            <div class="home-demo__grid js-tabs">

                <div class="home-demo__col home-demo__col--left">
                    <div class="home-demo__navigation">

                        <?php if (!empty($uniqueFeatures)) { ?>
                            <?php foreach ($uniqueFeatures as $key =>  $uniqueFeature ) { ?>

                                <div class="home-demo__navigation-item">
                                    <a href="#" class="<?= $key == 0 ? 'active' : '' ?>" data-tabs="tab_<?= $key?>"><?= $uniqueFeature->titleSecond ?></a>
                                </div>

                            <?php } ?>
                        <?php } ?>
                    </div>

                </div>

                <div class="home-demo__col home-demo__col--right">

                    <?php if (!empty($uniqueFeatures)) { ?>
                        <?php foreach ($uniqueFeatures as $key =>  $uniqueFeature ) { ?>
                            <div id="tab_<?= $key?>">
                                <p class="home-demo__text"> <?= $uniqueFeature->text ?></p>

                                <img src="<?= \common\components\Thumbnail::thumbnailFileUrl($uniqueFeature->image, 765, 545) ?>">
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
            </div>
        </div>

        <div class="home-demo__content">

            <?php if (!empty($uniqueFeatures)) { ?>
                <?php foreach ($uniqueFeatures as $key =>  $uniqueFeature ) { ?>

                    <h3><?= $uniqueFeature->titleSecond ?></h3>
                    <p><?= $uniqueFeature->text ?></p>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
