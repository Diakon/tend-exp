<?= $this->render('_tenders_list_inner', ['dataProvider' => $dataProvider, 'isAjax' => $isAjax]) ?>

<div class="pagination">
    <div class="pagination__inner">
        <?= $this->render('_tenders_pagination', ['dataProvider' => $dataProvider]) ?>
    </div>
</div>