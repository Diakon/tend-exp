<?php
/**
 * @var common\modules\tender\models\frontend\Tenders $model
 */
use common\modules\tender\models\frontend\Tenders;
use common\modules\offers\models\frontend\Offers;

// Получаю активное предложение от моей компании для этого тендера
$activeOffer = $model->getActiveOffer($this->context->companyId)->one();
$sum = $activeOffer->sumPrice;
?>
<p><b><?= $model->title ?></b></p>

<a href="<?= $activeOffer->getOfferAddLink(['companyUrl' => $this->context->params['companyUrl'], 'tenderId' => $model->id, 'offerId' => $activeOffer->id]) ?>" class="btn">
    <p><?= Yii::t('app', 'Статус предложения по тендеру') ?>: <?= Offers::$statusList[$activeOffer->status] ?></p>
</a>


<p><?= Yii::t('app', 'Адрес') ?>: <?= $model->object->address->fullAddress ?? '' ?></p>
<p><?= Yii::t('app', 'Закрытый тендер №') ?>: <?= $model->id ?></p>
<p><?= Yii::t('app', 'Статус') ?>: <?= Tenders::getStatuses($model->status) ?></p>
<p><?= Yii::t('app', 'Дата начала') ?>: <?= Yii::$app->formatter->asDate($model->date_start, 'php:d.m.Y') ?></p>
<p><?= Yii::t('app', 'Дата завершения') ?>: <?= Yii::$app->formatter->asDate($model->date_end, 'php:d.m.Y') ?></p>


<p><?= Yii::t('app', 'Ваше предложение') ?>: <?= Yii::$app->formatter->asMoney($activeOffer->sum) ?></p>
<p><?= Yii::t('app', 'Запрош. скидка') ?>: <?= $sum['noDiscount'] != $sum['withDiscount']
        ? Yii::$app->formatter->asMoney($sum['withDiscount'])
        : Yii::t('app', 'отсутствует')?></p>