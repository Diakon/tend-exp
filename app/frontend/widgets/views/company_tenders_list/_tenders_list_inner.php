<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */

$loadPage = $dataProvider->pagination->page;

?>

<?php
$template = $isAjax ? '{items}' : '{items}
<div class="section__action load-more" data-ajax-url="' . \yii\helpers\Url::current() . '" data-load-page="' . $loadPage  . '">
    <!--<button class="btn btn-more">ПОКАЗАТЬ ЕЩЕ</button>-->
</div>';

echo \modules\crud\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $template,
    'options' => ['class' => 'main__body'],
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'tender-item js-tender-acco',
    ],
    'itemView' => $this->context->typeTenderList == 1 ? '_tenders_list_item' : '_tenders_partners_list_item',
    // 'emptyText' => '<div class="filter-result__empty"><div class="_title">' . Yii::t('app', $emptyMessage) . '</div></div>',
    'pager' => false,
])
?>
