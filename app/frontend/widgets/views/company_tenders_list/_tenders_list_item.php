<?php
use common\modules\tender\models\frontend\Tenders;
/**
 * @var common\modules\tender\models\frontend\Tenders $model
 */
$company = $model->object->company;
$canGetTender = Yii::$app->user->canGetTender($model);
$offersCount = $model->getOffersCount();
$offers = !empty($offersCount) ? $model->getTendersListOffers() : null;
?>

<div class="tender-item js-tender-acco">
    <div class="tender-item__inner">
        <div class="tender-item__head">
            <div class="tender-item__options">
                <div class="tender-item__option"><?= Yii::t('app', Tenders::getModes($model->mode) . ' тендер') ?> № <?= $model->id ?></div>
                <div class="tender-item__option"><?= Yii::t('app', 'Тип') ?>: <span class="_inline"><?= Yii::t('app', Tenders::getTypes($model->type)) ?></span></div>
                <div class="tender-item__option"><?= Yii::t('app', 'Дата завершения тендера') ?>: <span class="_inline"><?= Yii::$app->formatter->asDate($model->date_end, 'php:d.m.Y') ?></span>
                </div>
            </div>

            <div class="tender-item__status">
                <div class="status-block ">
                    <div class="status-block__text">
                        <?= Tenders::getStatuses($model->status) ?>
                    </div>
                </div>
            </div>

            <div class="tender-item__head-action hidden-md hidden-lg">
                <button class="btn-add">
                    <svg class="icon icon-favorite ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/images/svg/spriteInline.svg#favorite"></use>
                    </svg>
                </button>
            </div>
        </div>

        <div class="tender-item__body">
            <div class="tender-item__body-left">
                <h2 class="tender-item__title">
                    <a href="<?= $model->getViewLink('tender_edit', ['companyUrl' => Yii::$app->request->get('companyUrl')]) ?>">
                        <?= $model->title ?>
                    </a>
                </h2>
                <div class="tender-item__desc"><?= $model->object->address->fullAddress ?? '' ?></div>
            </div>
            <div class="tender-item__body-right">
                <div class="tender-item__offer">
                    <span class="_qty">Предложения: <a href="<?= $model->getOffersListLink(['companyUrl' => $this->context->params['companyUrl'], 'id' => $model->id]) ?>"><?= $offersCount['totalCount'] ?></a></span>
                    <a href="<?= $model->getOffersListLink(['companyUrl' => $this->context->params['companyUrl'], 'id' => $model->id]) ?>" class="_new"><?= $offersCount['newCount'] ?> <?= Yii::t('app', 'новых') ?></a>
                </div>
            </div>
        </div>

        <div class="tender-item__foot">
            <div class="tender-item__options">
                <div class="tender-item__option hidden-lg">
                    <div class="_label"><?= Yii::t('app','Заказчик') ?>:</div>
                    <div class="_value"><?= $company->title ?? '' ?></div>
                </div>

                <div class="tender-item__option">
                    <div class="_label"><?= Yii::t('app','Вид деятельности') ?>:</div>
                    <div class="_value"><?= implode('; ', \yii\helpers\ArrayHelper::map($company->getBuilds()->asArray()->all(), 'title', 'title')) ?></div>
                </div>
                <!--
                <div class="tender-item__option">
                    <div class="_label"><?= Yii::t('app','Краткое описание работ') ?>:</div>
                    <div class="_value"><?= Yii::t('app', $model->description) ?></div>
                </div>
                -->
                <div class="tender-item__option">
                    <div class="_label"><?= Yii::t('app','Название проекта') ?>:</div>
                    <div class="_value"><?= Yii::t('app', $model->object->title) ?></div>
                </div>
            </div>


            <?php if (!empty($offers)) { ?>
                <div class="tender-item__action">
                    <button class="tender-item__acco js-tender-acco-btn" data-text-open="<?= Yii::t('app','Подробнее о тендере') ?>" data-text-close="<?= Yii::t('app','Скрыть подробности') ?>"></button>
                </div>
            <?php } ?>

        </div>

        <?php if (!empty($offers)) { ?>
            <div class="tender-item__content js-tender-acco-content">
                <!--<div class="tender-item__content-title">По приглашениям</div>-->
                <div class="tender-item__table">
                    <div class="tender-item__table-head">
                        <div class="tender-item__table-row">
                            <div class="tender-item__table-col _col-1">Название компании</div>
                            <div class="tender-item__table-col _col-2">Первое предложение</div>
                            <div class="tender-item__table-col _col-3">Ваш ответ</div>
                            <div class="tender-item__table-col _col-4">Последнее предложение</div>
                            <div class="tender-item__table-col _col-5"></div>
                        </div>
                    </div>

                    <div class="tender-item__table-body">
                        <?php foreach ($offers as $offer) { ?>
                            <div class="tender-item__table-row">
                                <?php $companyName = $offer['companyName']; ?>
                                <div class="tender-item__table-col _col-1"><?= $companyName ?></div>
                                <div class="tender-item__table-col _col-2"><?= Yii::$app->formatter->asMoney($offer['firstOffer']['sum']) ?> руб.</div>
                                <div class="tender-item__table-col _col-3"><?= $offer['answerOffer']['sumDiscount'] > 0 ? (Yii::$app->formatter->asMoney($offer['answerOffer']['sumDiscount']) . ' руб.') : '' ?></div>
                                <div class="tender-item__table-col _col-4"><?= !empty($offer['lastOffer']) ? (Yii::$app->formatter->asMoney($offer['lastOffer']['sum']) . ' руб.') : '' ?></div>
                                <!--
                                <div class="tender-item__table-col _col-5">
                                    <button class="btn-comment">
                                        <svg class="icon icon-comment ">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#comment"/>
                                        </svg>
                                        <span class="_qty">7</span>
                                    </button>
                                </div>
                                -->
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>