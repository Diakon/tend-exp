
<?php $info = \frontend\widgets\TenderosInfo::widget([
    'view'   => '@app/widgets/views/tenderos-info',
]);
?>

<?php if (!empty($sliders)) { ?>
        <section class="home-slider" style="background-image: url('<?= \common\components\Thumbnail::thumbnailFileUrl($sliders[0]->image, 1366, 635) ?>')">
            <div class="container">
                <div class="home-slider__inner">
                    <h1 class="home-slider__title"><?= $sliders[0]->text ?></h1>
                    <?= $info ?>
                </div>
            </div>
        </section>
<?php } ?>
