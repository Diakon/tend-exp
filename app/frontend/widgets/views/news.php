<?php
/**
 * @var $news          \common\models\Pages[]
 * @var $newsImportant \common\models\Pages[]
 */
?>

<section class="home-section home-news">
    <div class="container">
        <div class="home-section__inner">
            <header class="home-section__header">
                <h2 class="home-section__header_title"><?= Yii::t('app', 'Новости')?></h2>
            </header>
            <div class="home-news__grid">


                <div class="home-news__main">
                    <div class="news-item news-item--big">

                        <?php if (!empty($newsImportant)) { ?>
                            <?php foreach ($newsImportant as $newImportant) {

                                $date = \Yii::$app->formatter->asDate($newImportant->datePublished);
                                ?>
                                <div class="news-item__inner">
                                    <span class="news-item__badge">Важное</span>

                                    <a href="<?= $newImportant->getViewLink('new_view') ?>" class="news-item__link">
                                        <div class="news-item__img">
                                            <img src="<?= Yii::getAlias('@static') ?>/../content/images/news-big.jpg" class="_img">
                                        </div>
                                    </a>

                                    <div class="news-item__content">
                                        <a href="<?= $newImportant->getViewLink('new_view') ?>" class="news-item__title"><?= $newImportant->title ?></a>
                                        <time datetime="<?= $date ?>" class="news-item__date"><?= $date ?></time>
                                    </div>
                                </div>

                            <?php } ?>
                        <?php } ?>

                    </div>
                </div>


                <div class="home-news__right">
                    <div class="home-news__slider">


                        <?php if (!empty($news)) { ?>
                            <?php foreach ($news as $new) {

                                $date = \Yii::$app->formatter->asDate($new->datePublished);
                                ?>

                                <div class="home-news__slide">
                                    <div class="news-item news-item--vertical">
                                        <div class="news-item__inner">

                                            <a href="<?= $new->getViewLink('new_view') ?>" class="news-item__link">
                                                <div class="news-item__img">

                                                    <?php if ($new->img) { ?>
                                                        <img class="_img" src="<?php echo \common\components\Thumbnail::thumbnailFile($new->img->getFile(true)); ?>">
                                                    <?php } else { ?>
                                                        <img src="<?= Yii::getAlias('@static') ?>/../content/images/news-empty.jpg" alt="" class="_img">
                                                    <?php } ?>
                                                </div>
                                            </a>

                                            <div class="news-item__content">
                                                <a href="<?= $new->getViewLink('new_view') ?>" class="news-item__title"><?= $new->title ?></a>
                                                <time datetime="<?= $date ?>" class="news-item__date">
                                                    <?= $date ?></time>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        <?php } ?>

                    </div>
                </div>
            </div>
            <div class="section__action">
                <a href="<?= \yii\helpers\Url::to(['/frontend/new_list']) ?>" class="btn btn-outline-dark"><?= Yii::t('app', 'Перейти ко всем новостям')?></a>
            </div>
        </div>
    </div>
</section>
