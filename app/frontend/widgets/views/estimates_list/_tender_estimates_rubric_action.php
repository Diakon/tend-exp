<?php

use common\modules\tender\models\frontend\EstimatesRubrics;
use frontend\widgets\EstimatesList;
use common\components\ClassMap;
use yii\helpers\Html;

/**
 * @var integer $typeEstimates
 * @var EstimatesRubrics $estimatesRubric
 * @var \common\modules\offers\models\forms\OfferForm $offerForm
 */
?>

<ul class="option-menu__list">
    <?php if ($typeEstimates == EstimatesList::TYPE_ADD_TENDER) { ?>
        <li class="option-menu__item">
            <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="down" data-id="<?= $estimatesRubric->isNewRecord ? 'idParam' : $estimatesRubric->id ?>">
                <?= Yii::t('app', 'Добавить пункт снизу') ?>
            </button>
        </li>
        <li class="option-menu__item">
            <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="up" data-id="<?= $estimatesRubric->isNewRecord ? 'idParam' : $estimatesRubric->id ?>">
                <?= Yii::t('app', 'Добавить пункт сверху') ?>
            </button>
        </li>
        <?php if ($estimatesRubric->depth < EstimatesRubrics::LEVEL_MAX) { ?>
            <li class="option-menu__item">
                <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="addChildren" data-id="<?= $estimatesRubric->isNewRecord ? 'idParam' : $estimatesRubric->id ?>">
                    <?= Yii::t('app', 'Добавить дочерний пункт') ?>
                </button>
            </li>
        <?php } ?>
        <li class="option-menu__item">
            <button class="option-menu__link js-add-new-estimates-element" type="button" data-action="addWork" data-rubric="<?= $estimatesRubric->isNewRecord ? 'idParam' : $estimatesRubric->id ?>">
                <?= Yii::t('app', 'Добавить работу') ?>
            </button>
        </li>
        <li class="option-menu__item">
            <button class="option-menu__link js-edit-estimates-rubric" type="button" data-action="edit" data-id="<?= $estimatesRubric->isNewRecord ? 'idParam' : $estimatesRubric->id ?>">
                <?= Yii::t('app', 'Изменить') ?>
            </button>
        </li>
    <?php } ?>

    <?php if (in_array($typeEstimates, [EstimatesList::TYPE_ADD_TENDER, EstimatesList::TYPE_ADD_OFFER, EstimatesList::TYPE_CORRECTION_OFFER])) { ?>
        <li class="option-menu__item">
            <button class="option-menu__link" type="button" data-popup="add-comment" data-id="<?= $estimatesRubric->isNewRecord ? 'idParam' : $estimatesRubric->id ?>">
                <?= Yii::t('app', 'Добавить комментарий') ?>
            </button>
        </li>
    <?php } ?>

    <?php if ($typeEstimates == EstimatesList::TYPE_ADD_OFFER && !empty($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['discountRequest'])) { ?>
        <li class="option-menu__item">
            <button class="option-menu__link js-accept-discount-request"
                    type="button"
                    data-id="<?= $estimatesRubric->id ?>"
                    data-title="<?= Yii::t('app', 'Подтвердить скидку') ?>"
                    data-type="accept"
            >
                <?= Yii::t('app', 'Подтвердить скидку') ?>
            </button>
        </li>
        <li class="option-menu__item">
            <button class="option-menu__link js-cancel-discount-request"
                    type="button"
                    data-id="<?= $estimatesRubric->id ?>"
                    data-title="<?= Yii::t('app', 'Отклонить скидку') ?>"
                    data-type="cancel"
            >
                <?= Yii::t('app', 'Отклонить скидку') ?>
            </button>
        </li>
        <?= Html::button('', ['class' => 'js-discountrequest-discount-request-btn-' . $estimatesRubric->id, 'style' => 'display: none', 'data-popup' => 'add-discountrequest', 'type' => "button"]) ?>
        <?=  Html::activeHiddenInput($offerForm, 'offerDiscountRequest[' . ClassMap::CLASS_ESTIMATES_RUBRICS_ID . ']['. $estimatesRubric->id . '][id]',
            ['value' => $estimatesRubric->id, 'class' => 'js-discount-request-id-' . $estimatesRubric->id]); ?>
        <?=  Html::activeHiddenInput($offerForm, 'offerDiscountRequest[' . ClassMap::CLASS_ESTIMATES_RUBRICS_ID . ']['. $estimatesRubric->id . '][comment]',
            ['class' => 'js-discount-request-comment-' . $estimatesRubric->id]); ?>
        <?=  Html::activeHiddenInput($offerForm, 'offerDiscountRequest[' . ClassMap::CLASS_ESTIMATES_RUBRICS_ID . ']['. $estimatesRubric->id . '][type]',
            ['class' => 'js-discount-request-type-' . $estimatesRubric->id]); ?>
    <?php } ?>

    <?php if (in_array($typeEstimates, [EstimatesList::TYPE_CORRECTION_OFFER])) { ?>
        <li class="option-menu__item">
            <button
                    class="option-menu__link js-request-discount-btn"
                    type="button"
                    data-id="<?= $estimatesRubric->id ?>"
                    data-popup="sale"
                    onclick="$('.js-discount-hidden-input').val(<?= $estimatesRubric->id ?>);"
            >
                <?= Yii::t('app', 'Запросить скидку') ?>
            </button>
        </li>
    <?php } ?>

    <?php if ($typeEstimates == \frontend\widgets\EstimatesList::TYPE_ADD_TENDER) { ?>
        <li class="option-menu__item">
            <button class="option-menu__link" type="button" data-popup="remove-heading" data-work-name="<?= $estimatesRubric->title ?>" data-action="delete" data-id="<?= $estimatesRubric->isNewRecord ? 'idParam' : $estimatesRubric->id ?>q">
                <?= Yii::t('app', 'Удалить рубрику') ?>
            </button>
        </li>
    <?php } ?>
</ul>