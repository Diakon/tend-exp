<?php
use frontend\widgets\EstimatesList;
use yii\helpers\Html;
/**
 * @var integer $typeEstimates
 * @var \common\modules\offers\models\forms\OfferForm $offerForm
 * @var \common\modules\tender\models\frontend\EstimatesRubrics $model
 */

$discount = null;
if (!empty($offerForm->offerId)) {
    $discount = $model->getOffersElements($offerForm->offerId)->one();
    $discount = !empty($discount['discount']) ? $discount['discount'] : null;
}
?>
<?php if ($typeEstimates == EstimatesList::TYPE_VIEW_OFFER) { ?>
    <?= Html::activeInput('text', $offerForm, 'tenderRubrics[' . $model->id . '][discount]', [
        'value' => $discount,
        'class' => 'js-add-estimates-discount-' . $model->id,
        'data-id' => $model->id,
    ]) ?>
    <a href="#" class="js-add-estimates-discount-btn" data-id="<?= $model->id ?>" data-name="<?= $model::classNameShort() ?>">Запросить скидку</a>
<?php } else { ?>

    <?= Yii::t('app', 'Запрос скидки') ?>: <?= $discount ?>

<?php } ?>

