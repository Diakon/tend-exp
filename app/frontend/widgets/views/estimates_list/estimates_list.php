<?php
use frontend\widgets\EstimatesList;
use yii\helpers\Html;
use common\modules\tender\models\frontend\EstimatesElements;
use yii\helpers\Url;

/**
 * @var array $currency
 * @var integer $typeEstimates
 * @var $company \common\modules\company\models\frontend\Company
 * @var $model \common\modules\tender\models\frontend\EstimatesElements
 * @var $offerForm \common\modules\offers\models\forms\OfferForm
 * @var boolean $canEdit
 */

$estimatesRubrics = $model->estimatesRubrics ?? null;
echo Html::hiddenInput('urlLink', Url::to(), ['class' => 'js-estimates-url-link-ajax']);
?>

<div class="js-estimates-clone-block" style="display: none">
    <div class="acco-work__item js-acco-work-item" data-id="idParamq" data-parent-id="parentParamq" data-depth="depthParam" style="styleParam">
        <div class="acco-work__head">
            <div class="acco-work__title js-estimates-rubric-title-idParam">titleParam</div>
            <div class="acco-work__title acco-work__form">
                <input style="showStyleCss" type="text" class="js-estimates-rubric-edit js-estimates-rubric-edit-title-idParam form-control" data-id="idParam" value="titleParam">
            </div>
            <div class="acco-work__action">
                <div class="option-menu js-option">
                    <div class="option-menu__head">
                        <button class="option-menu__action js-option-open" type="button">
                            <svg class="icon icon-more-set ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#more-set"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="option-menu__body">
                        <?= $this->render('_tender_estimates_rubric_action', ['estimatesRubric' => new \common\modules\tender\models\frontend\EstimatesRubrics(), 'typeEstimates' => $typeEstimates]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="acco-work__body">
            <div class="acco-work__form" style="padding: 0px;">
                <div class="table-work">
                    <div class="table-work__inner">
                        <div class="table-work__body js-work-rows-block-idParam" id="js-work-rows-block-idParam">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($typeEstimates == \frontend\widgets\EstimatesList::TYPE_ADD_TENDER) { ?>
    <div id="js-estimates-elements-hidden-block" style="display:none;">
        <?= $this->render('_tender_estimates_elements', ['element' => new EstimatesElements(), 'typeEstimates' => $typeEstimates, 'canEdit' => $canEdit]);?>
    </div>

<?php } ?>

<div class="section-lk">
    <div class="section-lk__head">
        <h1 class="section-lk__title"><?= Yii::t('app', 'Смета работ') ?></h1>
        <?php if ($typeEstimates == \frontend\widgets\EstimatesList::TYPE_ADD_TENDER) { ?>
            <div class="section-lk__info">
                <div class="section-lk__label">
                    <p>
                        <?= $model->isNewRecord
                            ? Yii::t('app', 'Вы сможите создать сметы тендера, после того как сохраните этот новый тендер.')
                            : Yii::t('app', '
                        Перечислите работы, которые должны быть проведены подрядчиком.
                        Воспользуйтесь группировкой для структурирования сметы.')?>
                    </p>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php if (!$model->isNewRecord) { ?>
        <div class="_body">
            <div class="acco-work js-acco-work"
                 data-remove-heading-url="<?= Url::to(['/dashboard/tender_edit', 'id' => $model->id]) ?>"
                 data-remove-work-url="<?= Url::to(['/dashboard/tender_edit', 'id' => $model->id]) ?>">
                <div class="acco-work__main js-estimates-work-block">
                    <?php if (in_array($typeEstimates, [EstimatesList::TYPE_ADD_OFFER])) { ?>
                        <div class="acco-work__nds">
                            <?= Yii::t('app', 'Сумма без НДС') ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($estimatesRubrics)) { ?>
                        <?php foreach ($estimatesRubrics as $estimatesRubric) { ?>
                            <div class="acco-work__item js-acco-work-item"
                                 data-id="<?= $estimatesRubric->id ?>q"
                                <?php if ($estimatesRubric->depth > 1) { ?>
                                    data-parent-id="<?= $estimatesRubric->parent_id ?>q"
                                <?php } ?>
                                 data-depth="<?= $estimatesRubric->depth ?>"
                            >
                                <div class="acco-work__head">
                                    <div class="acco-work__title">
                                        <span class="js-estimates-rubric-title-<?= $estimatesRubric->id ?>"><?= $estimatesRubric->title ?></span>
                                        <input style="display: none" type="text" class="js-estimates-rubric-edit js-estimates-rubric-edit-title-<?= $estimatesRubric->id ?> form-control" data-id="<?= $estimatesRubric->id ?>" value="<?= $estimatesRubric->title ?>">
                                    </div>

                                    <?= $this->render('_tender_estimates_comments', ['model' => $estimatesRubric, 'id' => $estimatesRubric->id, 'offerId' => $offerForm->offerId ?? null]);?>
                                    <?php if ($canEdit) { ?>
                                        <div class="acco-work__action">
                                            <div class="option-menu js-option">
                                                <div class="option-menu__head">
                                                    <button class="option-menu__action js-option-open" type="button">
                                                        <svg class="icon icon-more-set ">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#more-set"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="option-menu__body">
                                                    <?= $this->render('_tender_estimates_rubric_action', ['estimatesRubric' => $estimatesRubric, 'typeEstimates' => $typeEstimates, 'offerForm' => $offerForm]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (in_array($typeEstimates, [EstimatesList::TYPE_ADD_OFFER, EstimatesList::TYPE_CORRECTION_OFFER])) { ?>
                                        <div class="acco-work__sale">
                                            <?php if (!empty($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['withDiscount']) && $offerForm->offerPrices['rubrics'][$estimatesRubric->id]['withDiscount'] > 0) { ?>
                                                <span class="_old-price">
                                                    <span class="js-offer-sum-with-discount-<?= $estimatesRubric->id ?>">
                                                        <?= Yii::$app->formatter->asMoney($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['noDiscount']) ?>
                                                    </span>
                                                    <?= \common\modules\directories\models\common\Currency::getCurrencyList($model->currency_id) ?>
                                                </span>
                                            <?php } ?>
                                            <?php if (!empty($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['noDiscount']) && $offerForm->offerPrices['rubrics'][$estimatesRubric->id]['noDiscount'] > 0) { ?>
                                                <span class="_current-price js-offer-sum-no-discount-<?= $estimatesRubric->id ?>">
                                                    <?= Yii::$app->formatter->asMoney($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['withDiscount'] > 0
                                                        ? $offerForm->offerPrices['rubrics'][$estimatesRubric->id]['withDiscount']
                                                        : $offerForm->offerPrices['rubrics'][$estimatesRubric->id]['noDiscount'])
                                                    ?>
                                                </span>
                                                <?= \common\modules\directories\models\common\Currency::getCurrencyList($model->currency_id) ?>
                                            <?php } ?>
                                            <?php if (!empty($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['discountRequest'])) { ?>
                                                <span class="_sale _sale--orange">
                                                    <?= floor($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['discountRequest']) ?>%
                                                </span>
                                            <?php } elseif (!empty($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['discount'])) { ?>
                                                <span class="_sale">
                                                    <?= floor($offerForm->offerPrices['rubrics'][$estimatesRubric->id]['discount']) ?>%
                                                </span>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (in_array($typeEstimates, [EstimatesList::TYPE_CORRECTION_OFFER])) { ?>
                                        <?= Html::activeHiddenInput($offerForm, 'tenderRubrics[' . $estimatesRubric->id . '][discount]', [
                                            'class' => 'js-add-estimates-discount-' . $estimatesRubric->id,
                                        ]) ?>
                                    <?php } ?>
                                </div>

                                <div class="acco-work__body">
                                    <div class="acco-work__form" style="padding: 0px;">
                                        <div class="table-work">
                                            <div class="table-work__inner">
                                                <div class="table-work__body js-work-rows-block-<?= $estimatesRubric->id ?>" id="js-work-rows-block-<?= $estimatesRubric->id ?>">
                                                    <?php if (!empty($estimatesRubric->elements)) { ?>
                                                    <div class="table-work">
                                                        <div class="table-work__head">
                                                            <div class="table-work__row">
                                                                <div class="table-work__col _col-1"><?= Yii::t('app', 'Наименование работ') ?></div>
                                                                <div class="table-work__col _col-2"><?= Yii::t('app', 'Кол-во') ?></div>
                                                                <div class="table-work__col _col-3"><?= Yii::t('app', 'Ед. изм.') ?></div>
                                                                <?php if (in_array($typeEstimates, [EstimatesList::TYPE_ADD_OFFER, EstimatesList::TYPE_CORRECTION_OFFER])) { ?>
                                                                    <div class="table-work__col _col-4"><?= Yii::t('app', 'Стоимость работ за ед.') ?></div>
                                                                    <div class="table-work__col _col-5"><?= Yii::t('app', 'Стоимость материала за ед.') ?></div>
                                                                    <div class="table-work__col _col-6"><?= Yii::t('app', 'Общая стоимость за ед.') ?></div>
                                                                    <div class="table-work__col _col-7"><?= Yii::t('app', 'Сумма, без НДС') ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php foreach ($estimatesRubric->elements as $element) { ?>
                                                            <div class="table-work__row " id="js-estimates-elements-block-id-<?= $element->id ?>" data-work-row="work-<?= $element->id ?>e">
                                                                <?= $this->render('_tender_estimates_elements', [
                                                                    'element' => $element,
                                                                    'typeEstimates' => $typeEstimates,
                                                                    'offerForm' => $offerForm,
                                                                    'currency' => $currency,
                                                                    'canEdit' => $canEdit
                                                                ]); ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <?php if ($typeEstimates == EstimatesList::TYPE_ADD_TENDER && $model->status != \common\modules\tender\models\frontend\Tenders::STATUS_ACTIVE) { ?>
                <div class="acco-work__footer">
                    <div class="acco-work__footer-add">
                        <button type="button" class="btn btn-primary js-edit-estimates-rubric" data-is-new-tender="<?= $model->isNewRecord ? 'yes' : 'no' ?>" data-action="addParent">
                            <?= Yii::t('app', 'ДОБАВИТЬ НАЗВАНИЕ РАБОТ') ?>
                        </button>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php if (in_array($typeEstimates, [EstimatesList::TYPE_ADD_OFFER, EstimatesList::TYPE_CORRECTION_OFFER])) { ?>
            <?= $this->render('_tender_estimates_offer_add_works', [
                'typeEstimates' => $typeEstimates,
                'model' => $offerForm,
                'currency' => $currency,
            ]); ?>
        <?php } ?>
        <?php if (!empty($offerForm)) { ?>
            <div class="acco-work__footer">
                <div></div>
                <div class="acco-work__total">
                    <?php if (!empty($offerForm->offerPrices['totalOfferSum']['noDiscount'])) { ?>
                        <div class="acco-work__total-item">
                            <b class="_label"><?= Yii::t('app', 'Нач. стоимость') ?>:</b>
                            <b class="_value">
                                <?= Yii::$app->formatter->asMoney($offerForm->sumInitialPrice) ?>
                                <?= \common\modules\directories\models\common\Currency::getCurrencyList($model->currency_id) ?>
                            </b>
                        </div>
                    <?php } ?>
                    <?php if (!empty($offerForm->offerPrices['totalOfferSum']['withDiscount'])) { ?>
                        <div class="acco-work__total-item acco-work__total-item--green">
                            <b class="_label"><?= Yii::t('app', 'Утв. скидка') ?>:</b>
                            <b class="_value">
                                <?= Yii::$app->formatter->asMoney($offerForm->calcSumWithDiscount) ?>
                                <?= \common\modules\directories\models\common\Currency::getCurrencyList($model->currency_id) ?>
                            </b>
                        </div>
                    <?php } ?>
                    <?php if(!empty($offerForm->offerPrices['totalOfferSum']['withRequestDiscount'])) { ?>
                        <div class="acco-work__total-item acco-work__total-item--orange">
                            <b class="_label"><?= Yii::t('app', 'Запрош. скидка') ?>:</b>
                            <b class="_value">
                                <?= Yii::$app->formatter->asMoney($offerForm->calcSumDiscountRequest) ?>
                                <?= \common\modules\directories\models\common\Currency::getCurrencyList($model->currency_id) ?>
                            </b>
                        </div>
                    <?php } ?>
                    <div class="acco-work__total-item">
                        <span class="_label"><?= Yii::t('app', 'Итоговая стоимость') ?>:</span>
                        <span class="_value">
                            <span class="js-offer-total-sum-no-discount-text">
                                <?php if (!empty($offerForm->totalSum)) { ?>
                                    <?= Yii::$app->formatter->asMoney($offerForm->totalSum) ?>
                                <?php } ?>
                            </span>
                            <?= \common\modules\directories\models\common\Currency::getCurrencyList($model->currency_id) ?>
                        </span>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>

<div class="popup popup--comment js-tender-estimates-add-comment-block" data-popup-id="add-comment" id="add-comment" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title"><?= Yii::t('app', 'Оставить комментарий') ?></div>
            <div class="popup__body">
                <div class="popup-comment">
                    <div class="popup-comment__message">
                        <div class="form-group   ">
                            <textarea class="form-control js-estimates-comment-input"></textarea>
                        </div>
                        <?= Html::hiddenInput('commentRubricId', null, ['class' => '_hidden-input-id js-comment-rubric-id-hidden-input']) ?>
                    </div>
                </div>
            </div>

            <div class="popup__footer">
                <div class="popup__action">
                    <button class="btn btn-primary js-add-estimates-comment-btn" type="submit" data-url="<?= \yii\helpers\Url::to() ?>"><?= Yii::t('app', 'ОТПРАВИТЬ') ?></button>
                    <button class="btn btn-outline-secondary js-add-estimates-comment-close-btn js-close-wnd" type="reset"><?= Yii::t('app', 'ОТМЕНИТЬ') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup popup--winner" data-popup-id="remove-work" id="remove-work" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title popup__title--margin-b-small"><?= Yii::t('app', 'Удаение работы') ?></div>
            <div class="popup__body">
                <p>
                    Вы действительно хотите удалить <span class="_work-name">выбранную работу</span> из сметы
                    работ?
                </p>
                <input type="hidden" name="input-id" class="_hidden-input-id">
            </div>
            <div class="popup__footer">
                <div class="popup__action">
                    <button type="submit" class="btn btn-primary js-remove-work">ДА</button>
                    <button type="button" class="btn btn-outline-secondary js-close-wnd">Отменить</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup popup--winner" data-popup-id="remove-heading" id="remove-heading" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title popup__title--margin-b-small"><?= Yii::t('app', 'Удаение рубрики') ?></div>
            <div class="popup__body">
                <p>
                    Вы действительно хотите удалить <span class="_work-name">выбранную рубрику</span> из сметы
                    работ?
                </p>
                <input type="hidden" name="input-id" class="_hidden-input-id js-estimates-rubric-delete-id">
            </div>
            <div class="popup__footer">
                <div class="popup__action">
                    <button type="button" class="btn btn-primary js-remove-heading">ДА</button>
                    <button type="button" class="btn btn-outline-secondary js-close-wnd">Отменить</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="popup popup--sale js-sale" data-popup-id="sale" id="sale" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title popup__title--margin-b-small"><?= Yii::t('app', 'Запросить скидку') ?></div>
            <div class="popup__body">
                <p>
                    <?= Yii::t('app', 'Если вы запросите общую скидку для рубрики, то все остальные действующие скидки, находящиеся внутри рубрики, исчезнут.') ?>
                </p>
            </div>
            <div class="popup__footer">
                <div class="popup__action">
                    <button type="button" class="btn btn-primary js-sale-open"><?= Yii::t('app', 'ЗАПРОСИТЬ СКИДКУ') ?></button>
                    <button type="button" class="btn btn-outline-secondary js-close-wnd"><?= Yii::t('app', 'Отменить') ?></button>
                </div>
            </div>
            <div class="popup-sale js-sale-box">
                <div class="popup-sale__inner">
                    <div class="popup-sale__field">
                        <div class="form-group">
                            <?= Html::input('text', 'sale', 0, ['class' => 'form-control js-sale-musk js-discount-input']) ?>
                            <?= Html::hiddenInput('rubricId', null, ['class' => 'js-discount-hidden-input']) ?>
                        </div>
                    </div>
                    <div class="popup-sale__action">
                        <a href="#" class="btn btn-primary js-estimates-discount-btn"><?= Yii::t('app', 'отправить') ?></a>
                    </div>
                    <div class="popup-sale__label">
                        <?= Yii::t('app', 'Введите запрашиваемую скидку')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup popup--discountrequest js-tender-estimates-discount-request-block" data-popup-id="add-discountrequest" id="add-discountrequest" style="display: none;">
    <div class="popup__box">
        <div class="popup__box-inner">
            <div class="popup__title"></div>
            <div class="popup__body">
                <div class="popup-comment">
                    <div class="popup-comment__message">
                        <div class="form-group   ">
                            <textarea class="form-control js-estimates-discount-request-comment-input"></textarea>
                            <input type="hidden" class="js-estimates-discount-request-id-input">
                            <input type="hidden" class="js-estimates-discount-request-type-input">
                        </div>
                        <?= Html::hiddenInput('commentRubricId', null, ['class' => '_hidden-input-id js-comment-rubric-id-hidden-input']) ?>
                    </div>
                </div>
            </div>

            <div class="popup__footer">
                <div class="popup__action">
                    <button class="btn btn-primary js-estimates-discount-request-resolution-btn" type="button" data-url="<?= \yii\helpers\Url::to() ?>">
                        <?= Yii::t('app', 'Сохранить') ?>
                    </button>
                    <button class="btn btn-outline-secondary js-estimates-discount-request-close-btn js-close-wnd" type="reset"><?= Yii::t('app', 'ОТМЕНИТЬ') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
