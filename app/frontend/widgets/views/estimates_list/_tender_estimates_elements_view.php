<?php
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\EstimatesElements;
use yii\helpers\Html;
/**
 * @var integer $typeEstimates
 * @var $rubric EstimatesRubrics
 * @var $element EstimatesElements
 */
$units = EstimatesElements::getUnits();
?>

<div class="table-work__col _col-1">
    <?= $element->title ?>
</div>
<div class="table-work__col _col-2">
    <?= $element->count ?>
</div>
<div class="table-work__col _col-3">
    <?= $units[$element->unit_id] ?? '' ?>
</div>