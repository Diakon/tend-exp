<?php
/**
 * @var \common\modules\tender\models\frontend\EstimatesRubrics $model
 * @var \common\modules\offers\models\forms\OfferForm $offerForm
 */
$comments = $model->getComments($offerForm->offerId ?? null)->orderBy(['created_at' => SORT_DESC])->all();

?>
<?php if (!empty($comments)) { ?>
    <div>
        <a href="#" data- class="js-estimates-comments-show"><?= count($comments) ?></a>
        <?php foreach ($comments as $comment) { ?>
            <div style="display: none" class="js-estimates-comment-text" data-user="<?= $comment->user->fullName ?>" data-comment="<?= $comment->text ?>"></div>
        <?php } ?>
    </div>
<?php } ?>