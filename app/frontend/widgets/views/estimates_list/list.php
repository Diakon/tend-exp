<?php
/**
 * @var integer $typeEstimates
 * @var \common\modules\offers\models\forms\OfferForm $offerForm
 * @var \common\modules\tender\models\frontend\Tenders $model
 * @var \common\modules\offers\models\frontend\Offers $offer
 */
?>
<?php if (!empty($model->estimatesRubrics)) { ?>
    <div id="js-estimates-rubrics-block-list">
        <?php foreach ($model->estimatesRubrics as $estimatesRubric) { ?>
            <div style="background-color: gainsboro; padding: 10px; margin-bottom: 5px;">
                <?= $this->render('_list_rubrics', ['model' => $estimatesRubric, 'typeEstimates' => $typeEstimates, 'offerForm' => $offerForm, 'offer' => $offer]);?>
                <br>
                <?php if (!empty($estimatesRubric->elements)) { ?>
                    <div class="elements">
                        <?php foreach ($estimatesRubric->elements as $element) { ?>
                            <?= $this->render('_list_elements', ['element' => $element, 'typeEstimates' => $typeEstimates, 'offerForm' => $offerForm, 'offer' => $offer]);?>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
        <?php } ?>
    </div>
<?php } ?>