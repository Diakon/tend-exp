<?php
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\EstimatesElements;
use frontend\widgets\EstimatesList;
use yii\helpers\Html;
/**
 * @var array $currency
 * @var integer $typeEstimates
 * @var $rubric EstimatesRubrics
 * @var $element EstimatesElements
 * @var $offerForm \common\modules\offers\models\forms\OfferForm
 * @var $offer \common\modules\offers\models\frontend\Offers
 */
$units = EstimatesElements::getUnits();
?>

<div class="table-work__col _col-1">
    <?= $element->title ?>
</div>
<div class="table-work__col _col-2">
    <?= $element->count ?>
</div>
<div class="table-work__col _col-3">
    <?= $units[$element->unit_id] ?? '' ?>
</div>

<div class="table-work__col _col-4">
    <?php if ($typeEstimates == EstimatesList::TYPE_CORRECTION_OFFER) { ?>
        <?= Yii::$app->formatter->asMoney($offerForm->getOfferElementPriceWork($element)) ?>
    <?php } else { ?>
        <?= Html::activeInput('text', $offerForm, 'tenderWorks[' . $element->id . '][price_work]', [
            'value' => $offerForm->getOfferElementPriceWork($element),
            'class' => 'js-estimates-offer-price-work form-control',
        ]) ?>
    <?php } ?>
</div>
<div class="table-work__col _col-5">
    <?php if ($typeEstimates == EstimatesList::TYPE_CORRECTION_OFFER) { ?>
        <?= Yii::$app->formatter->asMoney($offerForm->getOfferElementPriceMaterial($element)) ?>
    <?php } else { ?>
        <?= Html::activeInput('text', $offerForm, 'tenderWorks[' . $element->id . '][price_material]', [
            'value' => $offerForm->getOfferElementPriceMaterial($element),
            'class' => 'js-estimates-offer-price-_material form-control',
        ]) ?>
    <?php } ?>
</div>
<div class="table-work__col _col-6">
    <span class="js-offer-sum-element-one-no-discount-<?= $element->id ?>" data-count="<?= $element->count ?>">
        <?= !empty($offerForm->offerPrices['elements'][$element->id]['noDiscount']) && !empty($element->count)
            ? (Yii::$app->formatter->asMoney(($offerForm->offerPrices['elements'][$element->id]['noDiscount'] / $element->count)))
            : ''
        ?>
    </span>
</div>
<div class="table-work__col _col-7">
    <div class="table-work__sale">
        <?php if (!empty($offerForm->offerPrices['elements'][$element->id]['withDiscount']) && $offerForm->offerPrices['elements'][$element->id]['withDiscount'] > 0) { ?>
            <div class="_old-price">
                <span class="js-offer-sum-element-with-discount-<?= $element->id ?>">
                    <?= Yii::$app->formatter->asMoney($offerForm->offerPrices['elements'][$element->id]['noDiscount']) ?>
                </span>
            </div>
        <?php } ?>
        <div class="_current-price">
            <span class="js-offer-sum-element-no-discount-<?= $element->id ?>">
                <?php if (!empty($offerForm->offerPrices['elements'][$element->id]['noDiscount'])) { ?>
                    <?= Yii::$app->formatter->asMoney($offerForm->offerPrices['elements'][$element->id]['withDiscount'] > 0
                    ? $offerForm->offerPrices['elements'][$element->id]['withDiscount']
                    : $offerForm->offerPrices['elements'][$element->id]['noDiscount']) ?>
                <?php } ?>
            </span>
        </div>
        <?php if (!empty($offerForm->offerPrices['elements'][$element->id]['discount'])) { ?>
            <div class="_sale">
                <?= floor($offerForm->offerPrices['elements'][$element->id]['discount']) ?>%
            </div>
        <?php } ?>
    </div>
</div>