<?php
use frontend\widgets\EstimatesList;
use yii\helpers\Html;
use common\modules\tender\models\frontend\EstimatesElements;
/**
 * @var \common\modules\offers\models\forms\OfferForm $offerForm
 * @var integer $typeEstimates
 * @var \common\modules\tender\models\common\EstimatesElements $element
 */
$sum = [];
if (!empty($offer)) {
    $sum = $offer->getOfferElementSumPrice($element);
}
?>
<ul>
    <li><?= Yii::t('app', 'Наименование работ') ?>: <?= $element->title ?></li>
    <li><?= Yii::t('app', 'Количество') ?>: <?= $element->count ?></li>
    <li><?= Yii::t('app', 'Ед. изм.') ?>: <?= EstimatesElements::getUnits($element->unit_id) ?></li>
    <?php if (in_array($typeEstimates, [EstimatesList::TYPE_ADD_OFFER])) { ?>
        <li>
            <?= Yii::t('app', 'Стоимость работ за ед.') ?>:
            <?php if ($typeEstimates == EstimatesList::TYPE_VIEW_OFFER) { ?>
                <?= $offerForm->getOfferElementPriceWork($element) ?>
            <?php } else { ?>
                <?= Html::activeInput('text', $offerForm, 'tenderWorks[' . $element->id . '][price_work]', [
                    'value' => $offerForm->getOfferElementPriceWork($element)
                ]) ?>
            <?php } ?>
        </li>
        <li>
            <?= Yii::t('app', 'Стоимость материала за ед.') ?>:
            <?php if ($typeEstimates == EstimatesList::TYPE_VIEW_OFFER) { ?>
                <?= $offerForm->getOfferElementPriceMaterial($element) ?>
            <?php } else { ?>
                <?= Html::activeInput('text', $offerForm, 'tenderWorks[' . $element->id . '][price_material]', [
                    'value' => $offerForm->getOfferElementPriceMaterial($element)
                ]) ?>
            <?php } ?>
        </li>
    <?php } ?>

    <?php if (!empty($sum)) { ?>
        <li>Сумма, без учета НДС: <?= Yii::$app->formatter->asMoney($sum['noDiscount']) ?></li>
    <?php } ?>
</ul>
