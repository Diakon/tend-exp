<?php
use frontend\widgets\EstimatesList;
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\EstimatesElements;
/**
 * @var array $currency
 * @var integer $typeEstimates
 * @var $rubric EstimatesRubrics
 * @var $element EstimatesElements
 * @var $offerForm \common\modules\offers\models\forms\OfferForm
 * @var boolean $canEdit
 */

switch ($typeEstimates) {
    // Редактирование / создание работ сметы
    case (EstimatesList::TYPE_ADD_TENDER):
        echo $this->render('_tender_estimates_elements_edit', [
            'element' => $element,
            'currency' => $currency ?? [],
            'typeEstimates' => $typeEstimates,
            'canEdit' => $canEdit
        ]);
        break;
    // Редактирование / создание / корректировка предлажения
    case (EstimatesList::TYPE_CORRECTION_OFFER):
    case (EstimatesList::TYPE_ADD_OFFER):
        echo $this->render('_tender_estimates_elements_edit_offer', [
            'element' => $element,
            'typeEstimates' => $typeEstimates,
            'currency' => $currency ?? [],
            'offerForm' => $offerForm
        ]);
        break;
    // Просмотр работ сметы
    case (EstimatesList::TYPE_LIST):
    case (EstimatesList::TYPE_VIEW_OFFER):
        echo $this->render('_tender_estimates_elements_view', [
            'element' => $element,
            'currency' => $currency ?? [],
            'typeEstimates' => $typeEstimates
        ]);
        break;
}
?>