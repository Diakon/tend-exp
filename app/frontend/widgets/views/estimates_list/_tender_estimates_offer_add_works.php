<?php
use frontend\widgets\EstimatesList;
use yii\helpers\Html;

/**
 * @var $model \common\modules\offers\models\forms\OfferForm
 * @var $typeEstimates integer
 */
?>
<?php if ($typeEstimates == EstimatesList::TYPE_ADD_OFFER) { ?>
    <!-- Скрытый блок для JS клонирования -->
    <div class="js-offer-additional-work-hidden-block" style="display:none;">
        <?= $this->render('_offer_add_additional_work', [
            'addWorks' => [\common\modules\offers\models\forms\OfferForm::ADD_WORK_HIDDEN_KEY => []],
            'typeEstimates' => $typeEstimates
        ]); ?>
    </div>
<?php } ?>

<div class="section-lk__head" style="margin-top: 60px;">
    <h1 class="section-lk__title"><?= Yii::t('app', 'Дополнительные работы') ?></h1>
    <?php if ($typeEstimates == EstimatesList::TYPE_ADD_OFFER) { ?>
        <div class="section-lk__label">
            <?= Yii::t('app',
                'Если вы считаете необходимым проведение дополнительных работ, не указанных в смете Заказчика, укажите их ниже:') ?>
        </div>
    <?php } ?>
</div>

<div class="section-lk__body">
    <div class="add-work">
        <div class="add-work__list js-offer-add-work-main-block">
            <?php if (!empty($model->addWorks)) { ?>
                <?= $this->render('_offer_add_additional_work', [
                    'model' => $model,
                    'addWorks' => $model->addWorks,
                    'typeEstimates' => $typeEstimates
                ]); ?>
            <?php } ?>
        </div>
    </div>
</div>

<?php if ($typeEstimates == EstimatesList::TYPE_ADD_OFFER) { ?>
    <?= Html::hiddenInput(null, count($model->addWorks), ['class' => 'js-offer-add-additional-work-hidden-count']) ?>

    <div class="acco-work__footer">
        <?= Html::hiddenInput(null, count($model->addWorks), ['class' => 'js-offer-add-additional-work-hidden-count']) ?>
        <a href="#" class="btn btn-primary js-offer-add-work-btn"><?= Yii::t('app', 'ДОБАВИТЬ РАБОТУ') ?></a>
        <a href="#"
           class="btn btn-outline-secondary btn-white-bg btn-ml js-offer-recount-btn"
           data-url="<?= \yii\helpers\Url::to(['/dashboard/dashboard/offer_add', 'type' => 'recount', 'offerId' => $model->offerId, 'tenderId' => $model->tender->id, 'tHash' => $model->offerTHash]) ?>"
        ><?= Yii::t('app', 'ПЕРЕСЧИТАТЬ') ?></a>
    </div>
<?php } ?>