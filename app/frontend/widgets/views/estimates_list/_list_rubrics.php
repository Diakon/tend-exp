<?php
use frontend\widgets\EstimatesList;
use yii\helpers\Html;
/**
 * @var integer $typeEstimates
 * @var \common\modules\offers\models\forms\OfferForm $offerForm
 * @var \common\modules\tender\models\frontend\EstimatesRubrics $model
 * @var \common\modules\offers\models\frontend\Offers $offer
 */

$sum = [];
if (!empty($offer)) {
    $sum = $offer->getOfferElementSumPrice($model);;
}

?>
<?= str_repeat('&nbsp;&nbsp;&nbsp;', $model->depth)?>
<?= $model->title ?>
<?= $this->render('_list_comments', ['model' => $model, 'offerForm' => $offerForm]);?>
<?php if (in_array($typeEstimates , [EstimatesList::TYPE_VIEW_OFFER, EstimatesList::TYPE_ADD_OFFER])) { ?>
    <?= $this->render('_list_estimates_discount', ['model' => $model, 'offerForm' => $offerForm, 'typeEstimates' => $typeEstimates]);?>
<?php } ?>
<?php if (in_array($typeEstimates , [EstimatesList::TYPE_VIEW_OFFER, EstimatesList::TYPE_ADD_OFFER])) { ?>
    <?= Html::activeHiddenInput($offerForm, 'tenderComments[' . $model::classNameShort() . '][' . $model->id . ']', ['class' => 'js-add-estimates-comment-' . $model->id]) ?>
    <a href="#" class="js-offer-add-estimates-comment" data-entity-id="<?= $model->id ?>">Добавить комнтарий</a>
    <?php if (!empty($sum)) { ?>
        <p>С скидкой: <?= $sum['withDiscount'] ?></p>
        <p>Без скидки: <?= $sum['noDiscount'] ?></p>
    <?php } ?>
<?php } ?>
<?php if (in_array($typeEstimates, [EstimatesList::TYPE_ADD_OFFER])) { ?>
    <?= Html::activeHiddenInput($offerForm, 'tenderRubrics[' . $model->id . '][title]', [
        'value' => $model->title
    ]) ?>
<?php } ?>
