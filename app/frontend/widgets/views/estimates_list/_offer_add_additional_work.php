<?php
use common\modules\offers\models\common\OffersElements;
use frontend\widgets\EstimatesList;
use yii\helpers\Html;
use common\modules\offers\models\forms\OfferForm;
/**
 * @var $model \common\modules\offers\models\forms\OfferForm
 * @var $addWorks array
 * @var $typeEstimates integer
 */

?>
<?php if (!empty($addWorks)) { ?>
    <?php foreach ($addWorks as $key => $data) { ?>
        <?php
        $id = $data['id'] ?? $key;
        ?>
        <div class="add-work__item js-offer-add-work-block-<?= $key ?>">
            <div class="add-work__row">
                <?php if ($typeEstimates == EstimatesList::TYPE_ADD_OFFER) { ?>
                    <div class="add-work__col _col-1">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Название работ') ?></label>
                            <?= Html::activeInput('text', new OfferForm(), 'addWorks[' . $key . '][title]', [
                                'class' => 'form-control',
                                'value' => $data['title'] ?? null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-2">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Количество') ?></label>
                            <?= Html::activeInput('text', new OfferForm(), 'addWorks[' . $key . '][count]', [
                                'class' => 'form-control js-add-work-count-input-field js-add-work-count-input-' . $id,
                                'value' => $data['count'] ?? 0
                            ]) ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-3">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Ед. измерения') ?></label>
                            <?= Html::activeDropDownList(new OfferForm(), 'addWorks[' . $key . '][unit_id]', OffersElements::getUnits(), [
                                'class' => 'js-drop-down-param-class',
                                'value' => $data['unit_id'] ?? null
                            ]) ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-4">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Стоимость работ за ед.') ?></label>
                            <?= Html::activeInput('text', new OfferForm(), 'addWorks[' . $key . '][price_work]', [
                                'class' => 'form-control',
                                'value' => !empty($data['price_work']) ? Yii::$app->formatter->asMoney($data['price_work']) : 0
                            ]) ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-5">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Стоимость материала за ед.') ?></label>
                            <?= Html::activeInput('text', new OfferForm(), 'addWorks[' . $key . '][price_material]', [
                                'class' => 'form-control',
                                'value' => !empty($data['price_material']) ? Yii::$app->formatter->asMoney($data['price_material']) : 0
                            ]) ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-6">
                        <div class="form-group">
                            <div class="control-label"><?= Yii::t('app', 'Общая стоимость за единицу') ?></div>
                            <div class="add-work__value">
                                       <span class="js-offer-sum-add-work-one-no-discount-<?= $id ?>">
                                            <?= !empty($model->offerPrices['addWorks'][$id]['noDiscount']) && $model->offerPrices['addWorks'][$id]['noDiscount'] > 0
                                                ? (Yii::$app->formatter->asMoney(($model->offerPrices['addWorks'][$id]['noDiscount'] / $data['count'])))
                                                : ''
                                            ?>
                                       </span>
                            </div>
                        </div>
                    </div>
                    <div class="add-work__col _col-7">
                        <div class="form-group">
                            <div class="control-label"><?= Yii::t('app', 'Сумма без НДС, руб.') ?></div>
                            <div class="add-work__value">
                                    <span class="js-offer-sum-add-work-no-discount-<?= $id ?>">
                                        <?= !empty($model->offerPrices['addWorks'][$id]['noDiscount'])
                                            ? Yii::$app->formatter->asMoney($model->offerPrices['addWorks'][$id]['noDiscount'])
                                            : ''
                                        ?>
                                    </span>
                            </div>
                        </div>
                    </div>
                <?php }
                else { ?>
                    <div class="add-work__col _col-1">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Название работ') ?></label>
                            <?= $data['title'] ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-2">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Количество') ?></label>
                            <?= $data['count'] ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-3">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Ед. измерения') ?></label>
                            <?= $data['unit_id'] ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-4">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Стоимость работ за ед.') ?></label>
                            <?= Yii::$app->formatter->asMoney($data['price_work']) ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-5">
                        <div class="form-group">
                            <label for="" class="control-label "><?= Yii::t('app', 'Стоимость материала за ед.') ?></label>
                            <?= Yii::$app->formatter->asMoney($data['price_material']) ?>
                        </div>
                    </div>
                    <div class="add-work__col _col-6">
                        <div class="form-group">
                            <div class="control-label"><?= Yii::t('app', 'Общая стоимость за единицу') ?></div>
                            <div class="add-work__value">
                                       <span class="js-offer-sum-add-work-one-no-discount-<?= $id ?>">
                                            <?= !empty($model->offerPrices['addWorks'][$id]['noDiscount']) && $model->offerPrices['addWorks'][$id]['noDiscount'] > 0
                                                ? (Yii::$app->formatter->asMoney(($model->offerPrices['addWorks'][$id]['noDiscount'] / $data['count'])))
                                                : ''
                                            ?>
                                       </span>
                            </div>
                        </div>
                    </div>
                    <div class="add-work__col _col-7">
                        <div class="form-group">
                            <div class="control-label"><?= Yii::t('app', 'Сумма без НДС, руб.') ?></div>
                            <div class="add-work__value">
                                    <span class="js-offer-sum-add-work-no-discount-<?= $id ?>">
                                        <?= !empty($model->offerPrices['addWorks'][$id]['noDiscount'])
                                            ? Yii::$app->formatter->asMoney($model->offerPrices['addWorks'][$id]['noDiscount'])
                                            : ''
                                        ?>
                                    </span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="add-work__action">
                <?= Html::hiddenInput('OfferForm[addWorks][' . $key . '][comment]', null, ['class' => 'js-offer-add-work-comment-' . $key . '-addWork']) ?>
                <button class="_btn _add-comment" data-popup="add-comment" data-id="<?= $key ?>-addWork"><span><?= Yii::t('app', 'ДОБАВИТЬ КОММЕНТАРИЙ') ?></span></button>
                <?php if ($typeEstimates == EstimatesList::TYPE_ADD_OFFER) { ?>
                    <button class="_btn _remove js-offer-add-work-delete" data-id="<?= $key ?>"><span><?= Yii::t('app', 'УДАЛИТЬ') ?></span></button>
                <?php } ?>
            </div>
            <?= Html::activeHiddenInput(new OfferForm(), 'addWorks[' . $key . '][id]', ['value' => $id]) ?>
        </div>
    <?php } ?>

<?php } ?>
