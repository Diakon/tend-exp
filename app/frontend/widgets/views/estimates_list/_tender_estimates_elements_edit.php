<?php
use common\modules\tender\models\frontend\EstimatesRubrics;
use common\modules\tender\models\frontend\EstimatesElements;
use yii\helpers\Html;

/**
 * @var integer $typeEstimates
 * @var $rubric EstimatesRubrics
 * @var $element EstimatesElements
 */
?>

<?= Html::activeHiddenInput($element, 'id', [
    'class' => 'js-estimates-elements-id'
]) ?>


<div class="table-work__col _col-1">
    <?php if ($canEdit) { ?>
        <?= Html::activeInput('text', $element, 'title', [
            'class' => 'js-edit-estimates-element js-estimates-elements-title form-control',
            'placeholder' => Yii::t('app', 'Название работы'),
            'data-id' => $element->id,
            'data-rubric' => $element->rubric_id,
            'value' => $element->isNewRecord ? EstimatesElements::DEFAULT_NAME : $element->title
        ]) ?>
    <?php } else { ?>
        <?= $element->title ?>
    <?php } ?>
</div>
<div class="table-work__col _col-2">
    <?php if ($canEdit) { ?>
        <?= Html::activeInput('text', $element, 'count', [
            'class' => 'js-edit-estimates-element js-estimates-elements-count form-control',
            'data-id' => $element->id,
            'data-rubric' => $element->rubric_id,
            'placeholder' => Yii::t('app', 'Количество'),
            'value' => (int)$element->count ])
        ?>
    <?php } else { ?>
        <?= $element->count ?>
    <?php } ?>
</div>
<div class="table-work__col _col-3">
    <?php if ($canEdit) { ?>
        <?= Html::activeDropDownList($element, 'unit_id', EstimatesElements::getUnits(), [
            'class' => 'js-edit-estimates-element js-estimates-elements-unit_id',
            'data-id' => $element->id,
            'data-rubric' => $element->rubric_id,
            'placeholder' => Yii::t('app', 'Единица измерения')
        ]) ?>
    <?php } else { ?>
        <?= EstimatesElements::getUnits()[$element->unit_id] ?? '' ?>
    <?php } ?>
</div>

<div class="option-menu js-option">
    <?php if ($canEdit) { ?>
        <div class="option-menu__head">
            <button class="option-menu__action js-option-open" type="button">
                <svg class="icon icon-more-set ">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#more-set"/>
                </svg>
            </button>
        </div>
        <div class="option-menu__body">
            <ul class="option-menu__list">
                <li class="option-menu__item">
                    <button class="option-menu__link js-acco-remove-work-btn" type="button" data-id="work-1e"  data-work-name="<?= $element->title ?>" data-popup="remove-work">
                        Удалить работу
                    </button>
                </li>
            </ul>
        </div>
    <?php } ?>
</div>