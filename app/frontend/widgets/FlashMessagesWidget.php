<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use function is_array;
use function is_null;
use function sprintf;

/**
 * Виджет отображения флеш сообщений
 *
 * @package frontend\widgets
 */
class FlashMessagesWidget extends Widget
{
    /**
     * @var null|string Ключ flash сообщений для показа
     */
    public $key = null;

    /**
     * @return void
     */
    public function run()
    {
        $js = '';

        foreach (Yii::$app->session->getAllFlashes() as $type => $messages) {
            if (!is_null($this->key) && $this->key != $type) {
                continue;
            }
            switch (true) {
                case strpos($type, 'success') !== false:
                    $type = 'success';
                    break;
                case strpos($type, 'info') !== false:
                    $type = 'info';
                    break;
                case strpos($type, 'error') !== false:
                    $type = 'error';
                    break;
                case strpos($type, 'warning') !== false:
                default:
                    $type = 'warning';
                    break;
            }

            if (is_array($messages)) {
                $message = '';

                array_walk_recursive($messages, function ($item, $key) use (&$message) {
                    $message .= $item . '<br>';
                });

                if (!empty($message)) {
                    $js .= sprintf('Toast.%s(\'%s\');', $type, $message);
                }

            } else {
                $js .= sprintf('Toast.%s(\'%s\');', $type, $messages);
            }
        }

        Yii::$app->view->registerJs($js);
    }
}