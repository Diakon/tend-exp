<?php
namespace frontend\widgets;

use common\modules\directories\models\common\Currency;
use common\modules\offers\models\forms\OfferForm;
use common\modules\offers\models\frontend\Offers;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Выводит смету
 * Class EstimatesList.
 *
 * @package frontend\widgets
 */
class EstimatesList extends Widget
{
    /**
     * Простой вывод сметы на сайте для просмотра
     */
    const TYPE_LIST = 1;

    /**
     * Создание предложения по смете
     */
    const TYPE_ADD_OFFER = 2;

    /**
     * Просмотр созданного предложения
     */
    const TYPE_VIEW_OFFER = 3;

    /**
     * Создание тендера
     */
    const TYPE_ADD_TENDER = 4;

    /**
     * Корректировка созданного предложения автором тендера
     */
    const TYPE_CORRECTION_OFFER = 5;

    /**
     * Тип вывода списка
     *
     * @var integer
     */
    public $typeEstimates;

    /**
     * Модель тендера
     *
     * @var Tenders
     */
    public $tender;

    /**
     * Модель формы создания / редактирвоания предложения для сметы
     *
     * @var OfferForm
     */
    public $offerForm;

    /**
     * Может редактировать смету тендкра
     *
     * @var bool
     */
    public $canEdit = true;

    /**
     * Вьюшка виджета
     *
     * @var string
     */
    public $view;

    public function run()
    {
        $model = $this->tender;
        $form = $this->offerForm;
        $offer = !empty($form->offerId) ? Offers::find()->where(['id' => $form->offerId])->one() : null;
        if (empty($form) && !empty($offer)) {
            $form = OfferForm::setOfferFormByOffer($offer);
        }
        $currency = Currency::getCurrencyList();

        return $this->render(!empty($this->view) ? $this->view : 'estimates_list/estimates_list', [
            'model' => $model,
            'offerForm' => $form,
            'offer' => $offer,
            'typeEstimates' => !empty($this->typeEstimates) ? $this->typeEstimates : self::TYPE_LIST,
            'currency' => $currency,
            'canEdit' => $this->canEdit,
        ]);
    }

}