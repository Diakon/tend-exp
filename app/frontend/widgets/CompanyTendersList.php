<?php
namespace frontend\widgets;

use modules\crud\widgets\BaseWidget;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;


/**
 * Виджет для вывода списка тендеров компании (мои, участвую)
 */
class CompanyTendersList extends BaseWidget
{
    /**
     * Тип списка тендеров - тендеры компании (мои)
     */
    const TYPE_TENDERS_LIST_MY = 1;
    /**
     * Тип списка тендеров - тендеры в которым участвует компания (участвую)
     */
    const TYPE_TENDERS_LIST_PARTNER = 2;

    /**
     * @var integer
     */
    public $typeTenderList = self::TYPE_TENDERS_LIST_MY;

    /**
     * ID компании
     *
     * @var integer
     */
    public $companyId;

    /**
     * Запуск действия.
     *
     * @throws NotFoundHttpException Модель не найдена.
     *
     * @return string
     */
    public function run()
    {
        $company = Yii::$app->user->getCompany($this->params['companyUrl'])->one();
        $this->companyId = $company->id;

        $model = new Tenders();

        switch ($this->typeTenderList) {
            // Список моих тендеров (млей клмпании)
            case self::TYPE_TENDERS_LIST_MY:
                $dataProvider = $model->searchItems(Yii::$app->request, false, $this->companyId);
                break;
            // Список тендеров в которых участвует моя компания
            case self::TYPE_TENDERS_LIST_PARTNER:
                $dataProvider = $model->getPartnersTenders(Yii::$app->request, $company);
                break;
            default:
                $dataProvider = $model->searchItems(Yii::$app->request, false, $this->companyId);
        }

        $isAjax = Yii::$app->request->isAjax;
        $page = intval(Yii::$app->request->post('loadPage', null));
        if ($isAjax) {
            $dataProvider->setPagination(['pageSize' => 10, 'pageSizeLimit' => [1, $dataProvider->totalCount], 'totalCount' => $dataProvider->totalCount]);
            $dataProvider->pagination->setPage($page);

            if ($page >= $dataProvider->pagination->pageCount) { // Если нет данных
                $dataProvider->pagination->setPage($page-1);
                return Json::encode(['data' => '',
                    'pagination' => Yii::$app->controller->renderAjax( '@frontend/widgets/views/company_tenders_list/_tenders_pagination', ['dataProvider' => $dataProvider ]),
                    'page' => $page,]);
            }
        }

        $view = $isAjax ? '@frontend/widgets/views/company_tenders_list/_tenders_pagination' : $this->params['template'];

        if ($isAjax) {
            return Json::encode([
                'data' => Yii::$app->controller->renderAjax($view,
                    [
                        'model' => $model,
                        'isAjax' => $isAjax,
                        'dataProvider' => $dataProvider,
                        'page' => $page,
                        'address' => $model->addressFilter,
                    ]),
                'pagination' => Yii::$app->controller->renderAjax('@frontend/widgets/views/company_tenders_list/_tenders_pagination',
                    ['dataProvider' => $dataProvider,]),
            ]);

        }

        return $this->render(
            $view, [
                'model' => $model,
                'isAjax' => $isAjax,
                'dataProvider' => $dataProvider,
                'address' => $model->addressFilter,
            ]
        );
    }
}