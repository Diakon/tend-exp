<?php
namespace frontend\widgets;

use helpers\Dev;
use modules\crud\widgets\BaseWidget;
use common\models\UserIdentity;
use common\modules\users\models\forms\UpdateUserForm;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Виджет для редактирования карточки пользователя.
 */
class UserProfileEdit extends BaseWidget
{
    /**
     * @var integer
     */
    public $userId;

    /**
     * Запуск действия.
     *
     * @throws NotFoundHttpException Модель не найдена.
     *
     * @return string
     */
    public function run()
    {
        $modelForm = new UpdateUserForm();
        $modelForm->userId = $this->userId;
        $modelForm->setUserData();

        if ($modelForm->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            if ($modelForm->updateUser()) {
                $transaction->commit();
                Yii::$app->session->setFlash('success-profile-form', Yii::t('app', 'Данные успешно сохранены'));
            } else {
                Yii::$app->session->setFlash('error-profile-form', Yii::t('app', 'Ошибка сохранения данных'));
                $transaction->rollBack();
            }
        }

        return $this->render($this->params['template'], [
            'model' => $modelForm
        ]);
    }
}
