<?php

namespace frontend\behaviors;

use frontend\models\EntityComment;
use yii\base\DynamicModel;
use yii\base\Event;
use yii\base\InvalidConfigException;
use modules\crud\models\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\validators\Validator;

/**
 * Class CommentBehavior.
 *
 * @package frontend\behaviors
 */
class CommentBehavior extends \yii\base\Behavior {

    /**
     * @var string $comment Комментарий
     */
    public $comment;

    /**
     * @var array Массив названий атрибутов
     */
    public $attributes = [];

    /**
     * @var array Правила валидации по умолчанию
     */
    public $defaultRules = ['skipOnEmpty' => true];

    /**
     * @var string Наименвание класса модели
     */
    private $ownerClassName = null;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    private $user;

    /**
     * @var EntityComment[] $uploadComments Загруженные комментарии
     */
    private $uploadComments = [];

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $this->ownerClassName = $owner::className();
        // Проверка существования атрибута 'attributes'
        if (empty($this->attributes)) {
            throw new InvalidConfigException('The "attributes" property must be set');
        }
    }

    /**
     * Действия перед валидацией модели.
     *
     * @param Event $event Объект события.
     *
     * @return void
     */
    public function beforeValidate(Event $event)
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        $comment = Yii::$app->request->post($model::classNameShort())['comment'];

        // Обходим атрибуты и валидируем возможно загруженные файлы
        foreach ($this->attributes as $attribute => $data) {

            // Валидируем новые файлы
            if (!empty($comment)) {

                $dynamicModel = new DynamicModel([$attribute => $comment]);

                $rules = array_key_exists('comment', $data['rules']) ? $data['rules']['comment'] : [];
                $validator = Validator::createValidator(
                    'string',
                    $dynamicModel,
                    $attribute,
                    ArrayHelper::merge($this->defaultRules, $rules)
                );

                $validator->validateAttribute($dynamicModel, $attribute);

                // Устанавливаем ошибку если модель не прошла валидацию
                if ($dynamicModel->hasErrors($attribute)) {
                    $model->addErrors($dynamicModel->getErrors());
                } else {
                    $model->comment = $comment;
                }

                $this->attributes[$attribute]['dynamicModel'] = $dynamicModel;
            }
        }
    }

    /**
     * Действия после сохранениея модели.
     *
     * @param Event $event Объект события.
     *
     * @return void
     */
    public function afterSave(Event $event)
    {

        /** @var ActiveRecord $model */
        $model = $this->owner;

        foreach ($this->attributes as $attribute => $data) {

            try {
                $newComment = new EntityComment();
                $newComment->text = Html::encode($model->comment);
                $newComment->userId = Yii::$app->user->id;
                $newComment->className = $this->ownerClassName;
                $newComment->entityId = $model->id;
                $newComment->save();
            } catch (\Exception $exception) {
                Yii::error('Ошибка сохранения коментария в тендере - ' . $this->owner , 'request');
            }

        }

    }

    /**
     * @param string $attribute Аттрибут модели
     *
     * @return EntityComment|EntityComment[]
     */
    public function getUploadComments($attribute)
    {
        if (empty($this->uploadComments[$attribute])) {

            $relation = $this->getRelationQuery();

            $this->uploadComments[$attribute] = $relation->all();
        }

        return $this->uploadComments[$attribute];
    }

    /**
     * @param string $name Запрашиваемый атрибут
     *
     * @return Upload|Upload[]
     * @throws InvalidConfigException
     */
    public function __get($name)
    {
        if ($this->checkIssetAttribute($name)) {
            $commentModel = $this->getRelationQuery();

            return $commentModel;
        }

        throw new InvalidConfigException('Call undefined attribute by CommentBehavior');
    }

    /**
     * @param string $name Запрашиваемый метод
     *
     * @param array  $params
     *
     * @return EntityComment|EntityComment[]
     * @throws InvalidConfigException
     */
    public function __call($name, $params)
    {
        $attributeName = $this->getAttributeByMethod($name);
        if ($this->checkIssetAttribute($attributeName)) {
            return $this->getRelationQuery();
        }

        throw new InvalidConfigException('Call undefined method by CommentBehavior');
    }

    /**
     * Получаем реляцию модели
     *
     * @return mixed
     */
    public function getRelationQuery()
    {

        $owner = $this->owner;
        $query = $owner->hasMany(EntityComment::class, ['entityId' => 'id'])->onCondition(['className' => $this->ownerClassName]);

        return $query;
    }

    /**
     * Проверяем указан ли конфиг запрашиваемый атрибута
     *
     * @param string $name
     *
     * @return bool
     */
    private function checkIssetAttribute($name)
    {
        return in_array($name, array_keys($this->attributes));
    }

    /**
     * Получаем наименование атрибута по возможному геттеру
     *
     * @param string $name
     *
     * @return string
     */
    private function getAttributeByMethod($name)
    {
        preg_match('/^get([a-zA-Z_]*)$/', $name, $matches);

        return isset($matches[1]) ? lcfirst($matches[1]) : null;
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return $this->checkIssetAttribute($name) ? true : parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function hasMethod($name)
    {
        $attributeName = $this->getAttributeByMethod($name);

        return !empty($attributeName) && $this->checkIssetAttribute($attributeName) ? true : parent::hasMethod($name);
    }

}
