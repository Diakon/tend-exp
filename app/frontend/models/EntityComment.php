<?php

namespace frontend\models;

use common\components\ClassMap;
use common\components\SendMail;
use modules\crud\models\ActiveRecord;
use common\models\UserIdentity;
use common\modules\company\models\frontend\Company;
use common\modules\offers\models\frontend\Offers;
use Yii;

/**
 * This is the model class for table "tender_comment".
 *
 * @property string         $id Id
 * @property string         $userId     Id пользователя
 * @property integer        $isRead     прочитанно или нет
 * @property string         $className  Название класса сущности
 * @property string         $entityId   Id сущности к которой прикреплен коммент
 * @property string         $text текст комментария
 * @property string         $createdAt  Дата создания
 * @property string         $updatedAt  Дата изменения
 * @property string         $offerId    ID предложения, если коментарий для сметы предложения
 * @property UserIdentity   $user       Автор коммента
 *
 */
class EntityComment extends ActiveRecord
{
    /**
     * Коментарий от заказчика по предложению к тендеру
     */
    const TYPE_COMMENT_CUSTOMER_OFFER = 1;

    const IS_READ_TRUE = 1;
    const IS_READ_FALSE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'className', 'entityId', 'text'], 'required'],
            [['userId', 'entityId', 'isRead'], 'integer'],
            [['text', 'offerId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'userId' => 'Id пользователя',
            'isRead' => 'прочитанно или нет',
            'offerId' => 'ID предложения',
            'className' => 'Название класса сущности',
            'entityId' => 'Id сущности к которой прикреплен коммент',
            'text' => 'текст комментария',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата изменения',
        ];
    }

    /**
     * Связь с пользователями
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserIdentity::class, ['id' => 'userId']);
    }

    /**
     * Связь с компаниями
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(Offers::class, ['id' => 'offerId']);
    }

    /**
     * Добавляет коментарий
     *
     * @param ActiveRecord|\yii\db\ActiveRecord $model
     * @param string $text
     * @param null|integer $offerId
     * @return bool
     */
    public static function addComment($model, $text = '', $offerId = null)
    {
        $classId = ClassMap::getId($model);
        if (empty($text) || empty($classId)) {
            return false;
        }

        $comment = new self();
        $comment->userId = Yii::$app->user->id;
        $comment->isRead = self::IS_READ_FALSE;
        $comment->className = $classId;
        $comment->entityId = $model->id;
        $comment->text = $text;
        if (!empty($offerId)) {
            $comment->offerId = $offerId;
        }

        return $comment->save();
    }
}
