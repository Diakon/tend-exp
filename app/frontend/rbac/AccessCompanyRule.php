<?php namespace frontend\rbac;

use common\modules\company\models\common\Company;
use common\modules\company\models\frontend\CompanyUsers;
use yii\rbac\Rule;
use common\components\AuthDbManager;
use Yii;

/**  * Проверяем на соответствие с пользователем  */
class AccessCompanyRule extends Rule
{
    public $name = AuthDbManager::ROLE_CLIENT . '.accessProfileRule';

    /**
     * Выполение проверки, что пользователь имеет нужные права в организации
     *
     * @param int|string $userId Id пользователя
     * @param \yii\rbac\Item $permission Разрешение для правила
     * @param array $params Дополнительные параметры
     *
     * @return boolean
     */
    public function execute($userId, $permission, $params)
    {

        if(Yii::$app->user->can(AuthDbManager::ROLE_ADMIN)) {
            return true;
        }

        /**
         * @var Company[] $company
         * @var CompanyUsers $company->users
         * Компания пользователя
         */
        $companyUrl = $params['companyUrl'] ?? Yii::$app->request->get('companyUrl');
        $company = Yii::$app->user->getCompany($companyUrl)->one();

        if (!empty($company)) {
            /**
             * @var \common\modules\company\models\frontend\CompanyUsers $user
             */
           $user = $company->companyUser;

            switch ($permission->name) {
                case 'clienteditCompany':
                    return $user->canEdit();
                    break;
                case 'addClientCompany':
                    return $user->canAddClient();
                    break;
                case 'accessProject':
                    return $user->canShowObject();
                    break;
                case 'editProject':
                    return $user->canEditObject();
                    break;
                case 'addProject':
                    return $user->canAddObject();
                    break;
                case 'accessTender':
                    return $user->canShowTender();
                    break;
                case 'addTender':
                    return $user->canAddTender(Yii::$app->request->get('objectId'));
                    break;
                case 'editTender':
                    $tenderId = $params['tenderId'] ?? Yii::$app->request->get('id');
                    return $user->canEditTender($tenderId, $company->id);
                    break;
                case 'addOffer':
                    return $user->canAddOffer();
                    break;
                default:
                    return $user->canShow();
            }
        }

        return false;
    }
}

