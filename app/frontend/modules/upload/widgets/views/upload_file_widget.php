<?php
/**
 * @var FileUploadInterface $model
 * @var integer|string+ $modelId
 */

use frontend\modules\upload\components\FileUploadInterface;
use yii\helpers\Html;

?>

<div class="section-lk">
    <div class="section-lk__head">
        <h1 class="section-lk__title"><?= Yii::t('app', 'Фотографии')?></h1>
        <div class="section-lk__label">
            <?= $this->render('_notice_limit_files') ?>
        </div>
    </div>
    <div class="section-lk__body">
        <div class="doc-load">
            <div class="doc-load__inner">

                    <div class="doc-load__head">
                        <div class="doc-load__field js-error-after-container">
                            <input type="file"
                                   id="upload-files"
                                   name="<?= Html::getInputName($model, 'ajaxUpload') ?>"
                                   class="_input js-file-upload"
                                   data-drop-area="js-drop-area"

                                   data-class-name="<?= \common\components\ClassMap::getId(get_class($model)) ?>"
                                   data-model-id="<?= $modelId ?>"
                                   data-url="<?= $model->getAjaxUploadFileUrl() ?>"
                                   data-error-after="js-error-after-container"
                                   data-files-type="jpg,jpeg,png"
                                   data-files="js-files-container"
                                   data-upload-type="photo"
                                   data-progress="js-progress-container"
                                   multiple>
                            <!--<input type="file" id="load-photo" class="_input">-->
                            <label for="upload-files" class="doc-load__label js-drop-area">
                                                        <span class="btn btn-outline-secondary">
                                                                <?= Yii::t('app', 'ЗАГРУЗИТЕ ФОТОГРАФИИ') ?>
                                                        </span>
                                <span class="_label"> <?= Yii::t('app', 'или перетащите их в эту область')?></span>
                            </label>
                        </div>
                        <div class="doc-load__progress js-progress-container">

                            <!--<div class="_line" style="width: 45%"></div>-->
                            <div class="_line" style="width: 0%"></div>
                        </div>
                    </div>

                    <div class="doc-load__body">
                        <div class="doc-load__row js-files-container">

                            <?php
                            $files = $model->files;
                            if (!empty($files)) {
                                /** @var \common\models\EntityFile[] $files */
                                foreach ($files as $file) {


                                   // \helpers\Dev::dump($file);
                                    ?>

                                    <div class="doc-load__col" id="_lzyje19y8">
                                        <div class="doc-load__item js-delete-file-url">

                                            <img class="_img"
                                                 src="<?= \common\components\Thumbnail::thumbnailFileUrl($file->getFile(true)); ?>">


                                            <button class="_btn _remove" type="button">
                                                <svg class="icon icon-remove">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#remove">
                                                    </use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>

                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>





