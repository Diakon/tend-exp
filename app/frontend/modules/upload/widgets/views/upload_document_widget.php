<?php
/**
 * @var FileUploadInterface $model
 * @var integer|string+ $modelId
 * @var boolean $canEdit
 */

use frontend\modules\upload\components\FileUploadInterface;
use yii\helpers\Html;

?>

<div class="section-lk">
    <div class="section-lk__head">
        <h1 class="section-lk__title"><?= Yii::t('app', 'Документы')?></h1>
        <div class="section-lk__label">
            <?= $this->render('_notice_limit_files') ?>
        </div>
    </div>
    <div class="section-lk__body">
        <div class="doc-load">
            <div class="doc-load__inner">

                <?php if ($canEdit) { ?>
                    <div class="doc-load__head">
                        <div class="doc-load__field js-error-after-container">
                            <input type="file"
                                   id="upload-files"
                                   name="<?= Html::getInputName($model, 'ajaxUpload') ?>"
                                   class="_input js-file-upload"
                                   data-drop-area="js-drop-area"

                                   data-class-name="<?= \common\components\ClassMap::getId(get_class($model)) ?>"
                                   data-model-id="<?= $modelId ?>"
                                   data-url="<?= $model->getAjaxUploadFileUrl() ?>"
                                   data-error-after="js-error-after-container"
                                   data-files-type="dwg,jpg,jpeg,png,doc,docx,xls,xlsx,ppt,pptx,pdf,3ds,ai,c4d,rar,zip,7z,gzip"
                                   data-files="js-files-container"
                                   data-upload-type="documents"
                                   data-progress="js-progress-container"
                                   multiple>
                            <!--<input type="file" id="load-photo" class="_input">-->
                            <label for="upload-files" class="doc-load__label js-drop-area">
                            <span class="btn btn-outline-secondary">
                                <?= Yii::t('app', 'ЗАГРУЗИТЕ ДОКУМЕНТЫ') ?>
                            </span>
                                <span class="_label">
                                <?= Yii::t('app', 'или перетащите их в эту область')?>
                            </span>
                            </label>
                        </div>
                        <div class="doc-load__progress js-progress-container">

                            <!--<div class="_line" style="width: 45%"></div>-->
                            <div class="_line" style="width: 0%"></div>
                        </div>
                    </div>
                <?php } ?>

                <div class="doc-load__body">
                    <div class="doc-load__row js-files-container">
                        <?php
                        $files = $model->files;
                        if (!empty($files)) {
                            /** @var \common\models\EntityFile[] $files */
                            foreach ($files as $file) { ?>
                                <div class="col-6 js-delete-file-url js-delete-file-block-<?= $file->id ?> js-upload-file-block">
                                    <div class="doc-item">
                                        <div class="<?= $canEdit ? "doc-item__inner" : "" ?>">
                                            <div class="doc-item__title">
                                                <a href="/upload/tenders/files/<?= $file->modelId ?>/<?= $file->fileName ?>" class="doc-item__link"><?= $file->title ?>.<?= $file->fileExt ?></a>
                                            </div>
                                            <div class="doc-item__action">
                                                <button class="_remove js-btn-remove-document" type="button"></button>
                                            </div>
                                            <div class="doc-item__size"><?=  number_format($file->fileSize / 1000000, 2, '.', ''); ?> мб</div>
                                        </div>
                                        <?= Html::hiddenInput('PrimeDocumentsRequest[files][' . $file->id . ']',$file->id, ['class' => 'js-hidden-input-file-container-data']) ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





