<?php

namespace frontend\modules\upload\widgets;

use frontend\modules\upload\components\FileUploadInterface;
use yii\base\InvalidConfigException;
use yii\base\Widget;

/**
 * Виджет: Отображение загруженных файлов и формы загрузки
 * @package frontend\modules\request\widgets
 */
class UploadFileFormWidget extends Widget
{
    /**
     * тип загрузки "документы"
     */
    const TYPE_DOCUMENTS = 1;

    /**
     * @var FileUploadInterface
     */
    public $model;

    /**
     * @var integer|string
     */
    public $modelId;

    /**
     * @var integer
     */
    public $type;

    /**
     * @var bool
     */
    public $canEdit = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->model instanceof FileUploadInterface) {
            throw new InvalidConfigException('Неверная конфигурация виджета');
        }

        if (empty($this->modelId)) {
            throw new InvalidConfigException('Неверный идентификатор модели виджета');
        }

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        switch ($this->type) {
            case self::TYPE_DOCUMENTS:
                $view = 'upload_document_widget';
                break;
            default:
                $view = 'upload_file_widget';
        }

        return $this->render($view, [
            'model' => $this->model,
            'modelId' => $this->modelId,
            'canEdit' => $this->canEdit
        ]);
    }
}
