<?php

namespace frontend\modules\upload\behaviors;

use frontend\modules\upload\models\Upload;
use Yii;
use yii\base\Behavior;
use yii\base\DynamicModel;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Поведение загрузки файлов
 *
 * ```php
 * use modules\upload\behaviors\UploadBehavior;
 *
 * function behaviors()
 * {
 *     return [
 *         [
 *             'class' => UploadBehavior::className(),
 *             'attributes' => [
 *                  'image'=>[
 *                      'singleFile' => true,
 *                      'rules'=>[],
 *                  ],
 *                  'doc'=>[
 *                      'rules'=>[],
 *                  ],
 *              ]
 *         ],
 *     ];
 * }
 * ```
 *
 * @package modules\upload\behaviors
 */
class UploadBehavior extends Behavior
{
    /**
     * @var array Массив названий атрибутов
     */
    public $attributes = [];

    /**
     * @var array Правила валидации по умолчанию
     */
    public $defaultRules = ['skipOnEmpty' => true, 'maxFiles' => 10];

    /**
     * @var string $ownerClassName Наименвание класса модели
     */
    protected $ownerClassName = null;

    /**
     * @var string Префикс оригинального изображения
     */
    private $originPrefix = 'origin';

    /**
     * @var Upload[] Загруженные файлы
     */
    private $uploadFiles = [];

    static $existsUploadTable = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // Проверка на существование таблицы для хранения информации о файлах
        if (is_null(self::$existsUploadTable)) {
            self::$existsUploadTable = is_null(Yii::$app->db->schema->getTableSchema(Upload::tableName(), true)) ? false : true;
        }

        if (!self::$existsUploadTable) {
            throw new InvalidConfigException('Not found ' . Upload::tableName() . ' database table');
        }
        // Проверка существования атрибута 'attributes'
        if (empty($this->attributes)) {
            throw new InvalidConfigException('The "attributes" property must be set');
        }
    }

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        parent::attach($owner);

        // Проверяем на наличие атрибутов у модели
        foreach ($this->attributes as $attribute => $data) {
            if (empty($data['rules'])) {
                throw new InvalidConfigException('The "' . $attribute . '" validation rules must be set on UploadBehavior initialisation');
            }
        }

        $this->ownerClassName = (new \ReflectionClass($owner->className()))->getShortName();
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * Действия перед валидацией модели.
     *
     * @param Event $event Объект события.
     *
     * @return void
     */
    public function beforeValidate(Event $event)
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        $uploadPost = Yii::$app->request->post('Upload');

        // Обходим атрибуты и валидируем возможно загруженные файлы
        foreach ($this->attributes as $attribute => $data) {
            $newFiles = UploadedFile::getInstances($model, $attribute);

            // Fix for module dynamicRelation
            if (!$newFiles) {
                $path = explode('\\', $model::className());
                $className = end($path);

                $dynamicRelationName = !empty($model->id) && !empty($className) ? $className . '[' . $model->id . ']' : null;
                $dynamicRelationNewFiles = UploadedFile::getInstancesByName($dynamicRelationName);
                if (!empty($dynamicRelationNewFiles)) {
                    $newFiles = $dynamicRelationNewFiles;
                }
            }

            // Валидируем новые файлы
            if (!empty($newFiles)) {

                $dynamicModel = new DynamicModel([$attribute => $newFiles]);

                $isImage = array_key_exists('image', $data['rules']);
                $rules = array_key_exists('file', $data['rules']) ? $data['rules']['file'] : ($isImage ? $data['rules']['image'] : $data['rules']);
                $validator = Validator::createValidator(
                    $isImage ? 'image' : 'file',
                    $dynamicModel,
                    $attribute,
                    ArrayHelper::merge($this->defaultRules, $rules)
                );

                $validator->validateAttribute($dynamicModel, $attribute);

                // Устанавливаем ошибку если модель не прошла валидацию
                if ($dynamicModel->hasErrors($attribute)) {
                    $model->addErrors($dynamicModel->getErrors());
                }

                $this->attributes[$attribute]['dynamicModel'] = $dynamicModel;
            }
            // Валидируем ранее прикрепленные файлы
            if (!empty($uploadPost)) {
                $files = $this->getUploadFiles($attribute);
                if ($files instanceof Upload) {
                    $files = [$files];
                }
                if (is_array($files) && !empty($files)) {
                    foreach ($files as $key => $file) {
                        if (isset($uploadPost[$file->id])) {
                            $file->setScenario(Upload::SCENARIO_UPDATE_INFO);
                            $file->setAttributes($uploadPost[$file->id]);
                            $file->validate();
                        }
                    }
                }
            }

        }
    }

    /**
     * Действия после сохранением модели.
     *
     * @param Event $event Объект события.
     *
     * @return void
     */
    public function afterSave(Event $event)
    {

        /** @var ActiveRecord $model */
        $model = $this->owner;

        foreach ($this->attributes as $attribute => $data) {

            $files = $this->getUploadFiles($attribute);

            // Если это единичный файл, и при этом идет загрузка дополнительного файла, то старый удаляем
            if ($files instanceof Upload) {
                if ($this->checkSingleFile($attribute) && isset($data['dynamicModel']) && $data['dynamicModel'] instanceof DynamicModel && empty($data['dynamicModel']->getErrors())) {
                    $files->delete = true;
                }
                $files = [$files];
            }

            if (!empty($files) && is_array($files)) {
                foreach ($files as $file) {
                    // Если отмечен checkbox на удаление.
                    if ($file->delete) {
                        $file->on($file::EVENT_BEFORE_DELETE, [$file, 'deleteFiles']);
                        $file->delete();
                    } else {
                        if (!$file->save()) {
                            Yii::error('UploadBehavior error saving file exist model:' . implode(', ', $file->attributes));
                        }
                    }
                }
            }

            // Загрузка новых фотографий
            $dynamicModel = isset($data['dynamicModel']) ? $data['dynamicModel'] : [];

            if (!empty($dynamicModel->$attribute) && is_array($dynamicModel->$attribute)) {
                foreach ($dynamicModel->$attribute as $file) {
                    if ($file instanceof UploadedFile) {
                        $prepareName = preg_replace('/[^a-zA-Z0-9_]/', '', Inflector::transliterate($file->baseName, Inflector::TRANSLITERATE_LOOSE));
                        $fileName = time() . '_' . $prepareName . '.' . $file->extension;

                        if ($file->saveAs($this->getUploadSavePath($attribute, $model->id) . $this->originPrefix . '_' . $fileName)) {
                            $fileModel = new Upload();
                            $fileModel->setScenario(Upload::SCENARIO_UPLOAD_FILE);
                            $fileModel->modelName = $this->ownerClassName;
                            $fileModel->modelId = $model->id;
                            $fileModel->modelAttribute = $attribute;
                            $fileModel->fileExt = $file->extension;
                            $fileModel->fileName = $fileName;
                            $fileModel->fileSize = $file->size;

                            if (!$fileModel->save()) {
                                Yii::error('UploadBehavior error saving file:' . implode(', ', $fileModel->attributes));
                            } else {
                                $fileModel->trigger(Upload::EVENT_AFTER_FILE_DOWNLOAD);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Возвращаем путь сохранения файла
     *
     * @param string  $attribute Атрибут модели
     * @param integer $id        Идентификатор модели
     *
     * @return string
     */
    private function getUploadSavePath($attribute, $id = null)
    {

        $alias = '@root/upload/' . Inflector::camel2id($this->ownerClassName, '_') . DIRECTORY_SEPARATOR . $attribute . DIRECTORY_SEPARATOR;
        $path = Yii::getAlias(is_null($id) ? $alias : $alias . $id . DIRECTORY_SEPARATOR);

        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        return $path;
    }

    /**
     * Проверка на singleFile
     *
     * @param $attribute
     *
     * @return boolean
     */
    public function checkSingleFile($attribute)
    {
        return isset($this->attributes[$attribute]['singleFile']) ? (bool)$this->attributes[$attribute]['singleFile'] : false;
    }

    /**
     * @param string $attribute Аттрибут модели
     *
     * @return Upload|Upload[]
     */
    public function getUploadFiles($attribute)
    {
        if (empty($this->uploadFiles[$attribute])) {
            $isSingleFile = $this->checkSingleFile($attribute);

            $relation = $this->getRelationQuery($attribute);

            $this->uploadFiles[$attribute] = $isSingleFile ? $relation->one() : $relation->all();
        }

        return $this->uploadFiles[$attribute];
    }

    /**
     * @param string $name Запрашиваемый атрибут
     *
     * @return Upload|Upload[]
     * @throws InvalidConfigException
     */
    public function __get($name)
    {
        if ($this->checkIssetAttribute($name)) {
            $uploadModel = $this->getRelationQuery($name);

            return $uploadModel;
        }

        throw new InvalidConfigException('Call undefined attribute by UploadBehavior');
    }

    /**
     * @param string $name Запрашиваемый метод
     *
     * @param array  $params
     *
     * @return Upload|Upload[]
     * @throws InvalidConfigException
     */
    public function __call($name, $params)
    {
        $attributeName = $this->getAttributeByMethod($name);
        if ($this->checkIssetAttribute($attributeName)) {
            return $this->getRelationQuery($attributeName);
        }

        throw new InvalidConfigException('Call undefined method by UploadBehavior');
    }

    /**
     * Получаем реляцию модели
     * @param $attribute
     *
     * @return mixed
     */
    public function getRelationQuery($attribute)
    {
        $isSingleFile = $this->checkSingleFile($attribute);

        $relationMethod = $isSingleFile ? 'hasOne' : 'hasMany';
        $owner = $this->owner;
        $query = $owner->$relationMethod(Upload::class, ['modelId' => 'id'])->onCondition(['modelName' => $this->ownerClassName, 'modelAttribute' => $attribute]);

        return $query;
    }

    /**
     * Проверяем указан ли конфиг запрашиваемый атрибута
     *
     * @param string $name
     *
     * @return bool
     */
    private function checkIssetAttribute($name)
    {
        return in_array($name, array_keys($this->attributes));
    }

    /**
     * Получаем наименование атрибута по возможному геттеру
     *
     * @param string $name
     *
     * @return string
     */
    private function getAttributeByMethod($name)
    {
        preg_match('/^get([a-zA-Z_]*)$/', $name, $matches);

        return isset($matches[1]) ? lcfirst($matches[1]) : null;
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return $this->checkIssetAttribute($name) ? true : parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function hasMethod($name)
    {
        $attributeName = $this->getAttributeByMethod($name);

        return !empty($attributeName) && $this->checkIssetAttribute($attributeName) ? true : parent::hasMethod($name);
    }
}
