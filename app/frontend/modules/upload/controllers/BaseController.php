<?php

namespace frontend\modules\upload\controllers;

use common\components\AccessControl;
use frontend\controllers\FrontendController;
use frontend\modules\upload\controllers\actions\AjaxDeleteFileAction;
use frontend\modules\upload\controllers\actions\AjaxUploadFileAction;
use frontend\modules\upload\controllers\actions\DownloadFileAction;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use function explode;


/**
 * Class BaseController
 *
 * @package frontend\modules\upload\controllers
 */
class BaseController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'upload' => AjaxUploadFileAction::class,
            'delete' => AjaxDeleteFileAction::class,
            'download' => DownloadFileAction::class,
            'protected-download' => [
                'class' => DownloadFileAction::class,
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {


        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    100 => [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new ForbiddenHttpException('Вам запрещен доступ к данному разделу сайта');
                },
            ],
        ]);
    }
}
