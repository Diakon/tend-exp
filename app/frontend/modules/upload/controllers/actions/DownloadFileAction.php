<?php

namespace frontend\modules\upload\controllers\actions;

use frontend\modules\request\models\common\EntityFile;
use yii\base\Action;
use yii\web\NotFoundHttpException;

/**
 * Class DownloadFileAction Загрузка файла с проверкой прав
 *
 * @philintodo Реализовать проверку прав на доступ к файлу
 *
 * @package    frontend\modules\request\controllers\actions
 */
class DownloadFileAction extends Action
{
    /**
     * @var bool Флаг проверки прав доступа к файлу пользователем
     */
    public $checkPermissions = true;

    public function run($id)
    {
        $model = EntityFile::findOne($id);

        if (!$model instanceof EntityFile) {
            throw new NotFoundHttpException('Не найдена запрашиваемая модель файла');
        }

        $file = $model->getFile();

        if (!file_exists($file)) {
            throw new NotFoundHttpException('Не найден запрашиваемый файл');
        }
        $file = $model->getFile(true);

        header('X-Accel-Redirect: ' . $file);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        exit;
    }
}
