<?php
namespace frontend\modules\upload\controllers\actions;

use common\components\ClassMap;
use common\modules\tender\models\frontend\Objects;
use common\modules\tender\models\frontend\Tenders;
use common\models\EntityFile;
use Yii;
use yii\base\Action;
use yii\helpers\Json;
use yii\web\Response;

/**
 * Class AjaxUploadFileAction Загрузка файлов по ajax
 * @package frontend\modules\request\controllers\actions
 */
class AjaxDeleteFileAction extends Action
{

    public function run()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = intval(Yii::$app->request->post('id'));

        $file = EntityFile::findOne($id);
        try {
            if ($file->deleteFiles()) {
                $file->delete();

                return [
                    'success' => Json::encode('Файл успешно удален'),
                ];
            }

        } catch (\Exception $exception) {
            Yii::error('Ошибка удаления файла', 'request');

            return [
                'error' => empty($file->errors)
                    ? 'Ошибка удаления файла'
                    : Json::encode($file->errors),
            ];
        }

        return [
            'error' => Json::encode('Ошибка удаления файла'),
        ];
    }
}
