<?php
namespace frontend\modules\upload\controllers\actions;

use common\components\ClassMap;
use common\models\EntityFile;
use common\modules\tender\models\frontend\Objects;
use common\modules\tender\models\frontend\Tenders;
use Yii;
use yii\base\Action;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\UploadedFile;
use function strval;

/**
 * Class AjaxUploadFileAction Загрузка файлов по ajax
 * @package frontend\modules\request\controllers\actions
 */
class AjaxUploadFileAction extends Action
{

    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $files = [];
        foreach ($_FILES as $key => $file) {
            $files[] = UploadedFile::getInstanceByName($key);
        }

        $model = new EntityFile([
            'modelName' => strval(Yii::$app->request->post('className')),
            'modelId' => strval(Yii::$app->request->post('modelId')),
            'modelAttribute' => 'files'
        ]);

        $result = $model->ajaxUpload($files);

        if ($result) {

            return [
                'success' => Json::encode($model->uploadedFiles),
            ];

        } else {
            return [
                'error' => empty($model->errors)
                    ? 'Неизвестная ошибка сохранения документа'
                    : Json::encode($model->errors),
            ];
        }

    }

}
