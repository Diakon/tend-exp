<?php
namespace frontend\modules\upload\models;

use helpers\Dev;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

/**
 * Модель загрузок
 *
 * @property string $id             Идентификатор
 * @property string $status         Статус
 * @property string $title          Заголовок
 * @property string $modelName      Наименование модели
 * @property string $modelId        Идентификатор модели
 * @property string $modelAttribute Атрибут модели
 * @property string $fileName       Наименование файла
 * @property string $fileAlt        Описание файла
 * @property string $fileSize       Размер файла
 * @property string $fileExt        Расширение файла
 * @property string $order          Сортировка
 * @property string $createdAt      Дата создания
 * @property string $updatedAt      Дата обновления
 * @property boolean $fileExists    Флаг наличия файла
 */
class Upload extends \base\BaseActiveRecord
{
    /**
     * Сценарий загрузки файла
     */
    const SCENARIO_UPLOAD_FILE = 'upload_file';


    /**
     * Событие срабатываемое после успешной загрузки файла
     */
    const EVENT_AFTER_FILE_DOWNLOAD = 'event_after_file_download';

    /**
     * @var boolean Атрибут удаления модели
     */
    public $delete = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%upload}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // SCENARIO_UPLOAD_FILE
            [['modelAttribute', 'modelName', 'modelId', 'fileExt', 'fileName', 'fileSize'], 'required', 'on' => self::SCENARIO_UPLOAD_FILE],
            // DEFAULT
            [['status', 'modelId', 'order', 'fileSize', 'createdAt', 'updatedAt'], 'integer'],
            [['modelAttribute', 'modelName', 'title', 'fileName', 'fileAlt'], 'string', 'max' => 512],
            [['fileExt', 'modelAttribute'], 'string', 'max' => 64],
            [['delete'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPLOAD_FILE => ['modelAttribute', 'modelName', 'modelId', 'fileExt', 'fileName', 'fileSize'],
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'id' => 'Идентификатор',
            'status' => 'Статус',
            'title' => 'Заголовок',
            'modelName' => 'Наименование модели',
            'modelId' => 'Идентификатор модели',
            'modelAttribute' => 'Атрибут модели',
            'fileName' => 'Наименование файла',
            'fileAlt' => 'Описание изображения',
            'fileSize' => 'Размер файла',
            'fileExt' => 'Расширение файла',
            'order' => 'Сортировка',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата обновления',
            'delete' => 'Удалить',
        ]);
    }

    /**
     * Возвращаем путь к файлу
     *
     * @param boolean $web     Выводим полный путь к файлу, либо путь относительно корня сайта
     * @param null    $noImage Файл заглушки в случае отсутствия запрашиваемого файла
     *
     * @return boolean|mixed|null|string
     */
    public function getFile($web = false, $noImage = null)
    {
        $path = Yii::getAlias('@root/upload/' . Inflector::camel2id($this->modelName, '_') . DIRECTORY_SEPARATOR . $this->modelAttribute . DIRECTORY_SEPARATOR . $this->modelId . DIRECTORY_SEPARATOR . 'origin_' . $this->fileName);


        return is_file($path) ? ($web ? str_replace(Yii::getAlias('@root'), '', $path) : $path) : $noImage;
    }

    /**
     * Удаление файла
     *
     * @return boolean
     */
    public function deleteFiles()
    {
        $path = Yii::getAlias('@root/upload/' . Inflector::camel2id($this->modelName, '_') . DIRECTORY_SEPARATOR . $this->modelAttribute . DIRECTORY_SEPARATOR . $this->modelId . DIRECTORY_SEPARATOR);

        if (is_dir($path)) {
            $fileName = $this->fileName;
            // Ищем файлы которые оканчиваются на $file_name
            $files = FileHelper::findFiles($path, [
                'filter' => function ($path) use ($fileName) {
                    return substr($path, strlen($path) - strlen($fileName)) == $fileName;
                },
            ]);
            // Удаляем их
            if (!empty($files) && is_array($files)) {
                foreach ($files as $file) {
                    unlink($file);
                }
            }
        }

        return true;
    }

    /**
     * Проверяем наличие файла
     * @return   boolean
     */
    public function getFileExists()
    {
        $file = $this->getFile();

        return is_null($file) ? false : file_exists($file) && is_readable($file);
    }
}
