<?php

namespace frontend\modules\upload\components;

use common\models\EntityFile;
use yii\db\ActiveQuery;

/**
 * Interface FileUploadInterface
 *
 * @property EntityFile[] $files
 *
 * @package frontend\modules\upload\components
 */
interface FileUploadInterface
{
    /**
     * Получаем файлы сущности
     * @return array|ActiveQuery
     */
    public function getFiles();

    /**
     * Получаем ajax путь удаления файла
     * @return string
     */
    public function getAjaxDeleteFileUrl(): string;


    /**
     * Получаем ajax путь загрузки файла
     * @return string
     */
    public function getAjaxUploadFileUrl(): string;
}
