<?php
namespace frontend\modules\uwm\helpers;

/**
 * Interface WishListInterface
 *
 * @package frontend\modules\uwm\helpers
 */
interface WishListInterface
{
    /**
     * Функция возвращающая данные для виджета избранного
     */
    public function getWishWidgetListData();
}
