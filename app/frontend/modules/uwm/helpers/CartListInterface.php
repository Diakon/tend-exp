<?php
namespace frontend\modules\uwm\helpers;

/**
 * Interface CartListInterface
 *
 * @package frontend\modules\uwm\helpers
 */
interface CartListInterface
{
    /**
     * Функция возвращающая данные для виджета корзины
     */
    public function getCartWidgetListData();
}
