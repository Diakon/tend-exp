<?php
namespace frontend\modules\uwm\helpers;

use common\models\Product;
use common\models\properties\Available;
use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRuleInterface;
use yii\base\Object;

/**
 * UwmUrlRule класс правил для логики путей для Uwm
 */
class UwmUrlRule extends Object implements UrlRuleInterface
{

    /**
     * @inheritdoc
     */
    public function createUrl($manager, $route, $params)
    {
        switch ($route) {
            case 'uwm/cart':
            case 'uwm/compare':
            case 'uwm/accessories':
            case 'uwm/wish':
                return $route . '/' . $params['id'] . '/';
                break;
        }

        return false;   // данное правило не применимо
    }

    /**
     * @inheritdoc
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        $params = $request->getQueryParams();

        // Если формат урла uwm/<action>/<id>
        if (preg_match('%^uwm/(([a-zA-Z0-9_-]+))/(\d+)%', $pathInfo, $matches)) {
            switch ($matches[1]) {
                case 'cart':
                case 'compare':
                case 'wish':
                    $params['id'] = $matches[3];
                    return ['uwm/' . $matches[1] . '/index', $params];
                    break;
            }
        }

        return false;  // данное правило не применимо
    }
}
