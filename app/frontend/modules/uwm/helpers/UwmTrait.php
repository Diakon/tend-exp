<?php
namespace frontend\modules\uwm\helpers;

use Yii;

/**
 * Трейт основных фунций Uwm модуля
 */
trait UwmTrait
{

    /**
     * Добавляет/удаляет элемент в выбранный $type
     *
     * @param integer    $id    Идентификатор элемента
     * @param string     $type  Тип списка
     * @param string     $model Тип модели элемента
     * @param array|null $data  Данные для сохранения
     *
     * @return integer 0-Error 1-Added 2-Removed
     */
    protected static function setSelected($id, $type, $model, $data = null)
    {
        try {
            $selected = !empty(Yii::$app->session['uwm'][$type][$model]) ? Yii::$app->session['uwm'][$type][$model] : [];

            if (empty($selected[$id])) {
                $selected[$id] = $data;
                $result = 1;
            } else {
                unset($selected[$id]);
                $result = 2;
            }

            $_SESSION['uwm'][$type][$model] = !empty($selected) ? $selected : null;

            return $result;
        } catch (\Exception $e) {
            return 0;
        }

        return 0;
    }


    /**
     *  Возвращает признак того что элемент находится в $type.
     *
     * @param integer $id    Идентификатор элемента
     * @param string  $type  Тип списка
     * @param string  $model Тип модели элемента
     *
     * @return boolean
     */
    public static function isSelected($id, $type, $model)
    {
        return isset(Yii::$app->session['uwm'][$type][$model][$id]) && !empty(Yii::$app->session['uwm'][$type][$model][$id]);
    }

    /**
     * Удаляет все элементы из $type
     *
     * @param string  $type  Тип списка
     * @param string  $model Тип модели элемента
     * @param integer $id    Идентификатор элемента
     *
     * @return void
     */
    public static function unsetList($type, $model = null, $id = null)
    {
        $session = Yii::$app->session;
        $vars = $session['uwm'];

        if (!is_null($model) && !is_null($id) && !empty($vars[$type][$model][$id])) {
            unset($vars[$type][$model][$id]);
        } elseif (!is_null($model) && is_null($id) && !empty($vars[$type][$model])) {
            unset(Yii::$app->session['uwm'][$type][$model]);
        } elseif (is_null($model) && is_null($id) && !empty($vars[$type])) {
            unset($vars[$type]);
        }
        $session['uwm'] = $vars;
    }

    /**
     * Получаем все элементы из $type
     *
     * @param string  $type    Тип списка
     * @param string  $model   Тип модели элемента
     * @param boolean $prepare Обработка результата функцией prepareData
     *
     * @return array
     */
    public static function getList($type, $model = null, $prepare = false): array
    {
        if (!is_null($model)) {
            $result = !empty(Yii::$app->session['uwm'][$type][$model]) ? Yii::$app->session['uwm'][$type][$model] : [];
        } else {
            $result = !empty(Yii::$app->session['uwm'][$type]) ? Yii::$app->session['uwm'][$type] : [];
        }

        return !empty($result) ? ($prepare ? self::prepareData($result) : $result) : [];
    }

    /**
     * Подготавливаем $data к виду $modelClass => $ids
     *
     * @param array $data Массив с исходными данными
     *
     * @return array
     */
    public static function prepareData(array $data = [])
    {
        $prepareElements = [];
        foreach ($data as $typeName => $type) {
            if (is_array($type)) {
                foreach ($type as $typeItem) {
                    $prepareElements[$typeItem['model']][] = $typeItem['id'];
                }
            }
        }

        return
            $prepareElements;
    }
}
