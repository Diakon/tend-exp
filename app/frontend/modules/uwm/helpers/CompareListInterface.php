<?php
namespace frontend\modules\uwm\helpers;

/**
 * Interface CompareListInterface
 *
 * @package frontend\modules\ufam\helpers
 */
interface CompareListInterface
{
    /**
     * Функция возвращающая данные для виджета списка сравнения
     */
    public function getCompareWidgetListData();
}
