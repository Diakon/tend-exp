<?php
/**
 * @var \common\modules\company\models\frontend\Company[] $data
 */

$staticPath = Yii::getAlias('@static');
use common\components\Thumbnail;
use yii\helpers\Html;
?>

<div class="compare-list js-compare-update" data-url="<?= \yii\helpers\Url::to(['/car/catalog/list/']) ?>">
    <button class="compare-list__close"></button>
    <div class="compare-list__title"><?= empty($data) ? 'Список сравнения пуст' : 'Список сравнения' ?></div>

    <div class="compare-list__body">
        <?php foreach ($data as $model) { ?>
            <div class="body__item js-remove-compare-<?= $model->id ?>">
                <div class="car">

                    <div class="car__inner">

                        <?php
                        $compareButtonOptions['class'] = 'car__remove js-compare js-compare-remove-list';
                        $compareButtonOptions['data-id'] = $model->id;
                        $compareButtonOptions['data-url'] = \yii\helpers\Url::to(['/uwm/compare/index', 'id' => $model->id]);
                        $compareButtonOptions['data-model'] = $model::classNameShort();

                        echo Html::tag('button', '', $compareButtonOptions); ?>

                        <div class="car__img">
                            <a href="<?= $model->getViewLink() ?>" class="select-img">
                                <?php if ($model->volvoSelekt) { ?>
                                    <div class="select-img__logo">
                                        <img src="<?= $staticPath ?>/../content/images/select/s-logo.svg" alt="">
                                    </div>
                                <?php } ?>

                                <div class="select-img__slider">

                                   <?php $attrs = $model::getCountRequirePhoto(3);

                                    foreach ($attrs as $attr) { ?>

                                            <div class="select-img__item">
                                                <div class="_img">
                                                    <?php


                                                    $img = $model->{$attr} ?? new \modules\upload\models\Upload(); ?>

                                                   <?= Thumbnail::img(['image' => $img->getFile(true), 'width' => 265, 'height' => 199,
                                                        'noImage' => Yii::$app->params['no_photo_url'], 'options' => ['alt' => $img->file_alt]]); ?>
                                                </div>
                                                <span class="_btn"></span>
                                            </div>
                                        <?php } ?>

                                    <div class="select-img__item">
                                        <?php if (!empty($model->image_require_four)) { ?>
                                            <div class="_img">
                                                <?= Thumbnail::img(['image' => $model->image_require_four->getFile(true), 'width' => 530, 'height' => 398,
                                                    'noImage' => Yii::$app->params['no_photo_url'], 'options' => ['alt' => $model->image_require_four->file_alt]]); ?>
                                            </div>
                                        <?php } ?>
                                        <div class="_more">
                                            <span>Ещё фото</span>
                                        </div>
                                        <span class="_btn"></span>
                                    </div>


                                </div>

                            </a>
                        </div>
                        <div class="car__info">
                            <div class="_title">
                                <a href="<?= $model->getViewLink() ?>">
                                    <?= $model->getFullTitle() ?>
                                </a>
                            </div>


                            <div class="location">
                                <div class="location__city"><?= $model->dealer->city->title ?? '' ?></div>
                            </div>

                            <div class="_price">
                                <?= Yii::$app->formatter->asMoney($model->price) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <?php if(!empty($data)) { ?>
    <div class="compare-list__footer">
        <a onclick="gtag('event', 'compare', {'event_category': 'compare','event_action': 'click_compare_detail'});" href="<?= \yii\helpers\Url::to(['/car/catalog/compare_detail']) ?>" class="btn btn-blue btn-arrow w100">
            <span>Сравнить</span>
        </a>
    </div>
    <?php } ?>
</div>
