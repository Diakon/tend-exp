<?php
namespace frontend\modules\uwm\widget;

use common\modules\car\models\frontend\Car;
use frontend\modules\uwm\Module as Uwm;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class CompareList Виджет вывода списка элементов
 */
class UwmWidget extends Widget
{
    /**
     * @var string Тип модели элемента
     */
    public $model;

    /**
     * @var string Шаблон вывода
     */
    public $template;


    /**
     * @var string Тип списка
     */
    public $type;

    /**
     * @var array Данные виджета
     */
    public $data = [];


    /**
     * @var string Флаг показа виджета
     */
    public $display = true;


    /**
     * Initializes the widget. This renders the form open tag.
     *
     * @return void
     */
    public function init()
    {
        if (!$this->display) {
            return;
        }

        $this->data = Uwm::getList($this->type, ($this->model)::classNameShort());
        $data = ArrayHelper::getColumn($this->data, 'id');

        /**
         * @var Car $product
         */
        $product = new $this->model;
        $this->data = $product->searchCompare(['with' => 'body'], $data);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (!empty($this->template)) {
            return $this->render($this->template, [
                'data' => $this->data ?? [],
            ]);
        }
    }
}
