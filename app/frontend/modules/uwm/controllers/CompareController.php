<?php
namespace frontend\modules\uwm\controllers;

use common\modules\offers\models\frontend\Offers;
use frontend\modules\uwm\helpers\CompareListInterface;
use frontend\modules\uwm\Module;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\Response;

/**
 * Class CompareController
 */
class CompareController extends UwmController
{

    /**
     * Добавляет/удаляет элемент для сравнения.
     *
     * @param integer $id Идентификатор элемента
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionIndex($id)
    {
        $data = null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Module $module */
        $module = $this->module;

        $module->modelMap = [Offers::classNameShort() => Offers::class];

        $modelType = Yii::$app->request->post('model');

        // Проверка на наличия модели в modelMap конфига модуля
        if (empty($module->modelMap[$modelType])) {
            throw new InvalidConfigException('Not set model in modelMap config module parameter');
        }

        $modelClass = $module->modelMap[$modelType];

        if (!array_search(CompareListInterface::class, class_implements($modelClass))) {
            throw new InvalidConfigException('Not set interface ' . CompareListInterface::class . ' in model');
        }

        if (!self::isSelected($id, $module::TYPE_COMPARE, $modelType)) {
            $model = $modelClass::findOne($id);
            $data = !empty($model) ? $model->getCompareWidgetListData() : null;
        }

        $selectResult = self::setSelected($id, $module::TYPE_COMPARE, $modelType, $data);

        $result['result'] = $selectResult;
        $result['data'] = $module::getList($module::TYPE_COMPARE, $modelType);

        return $result;
    }
}
