<?php
namespace frontend\modules\uwm\controllers;

use frontend\modules\uwm\helpers\UwmTrait;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * Class UwmController User Widget Module controller
 *
 * @inheritdoc
 */
class UwmController extends Controller
{
    use UwmTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['post'],
                ],
            ],
        ];
    }
}
