<?php
namespace frontend\modules\uwm\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use frontend\modules\uwm\Module;
use yii\base\InvalidConfigException;
use \frontend\modules\uwm\widget\UwmWidget;
use frontend\modules\uwm\helpers\WishListInterface;

/**
 * Class WishController
 */
class WishController extends UwmController
{

    /**
     * Добавляет/удаляет элемент в wishlist.
     *
     * @param integer $id Идентификатор элемента
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionIndex($id = null)
    {
        $data = null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Module $module */
        $module = $this->module;

        $modelType = Yii::$app->request->post('model');


        // Проверка на наличия модели в modelMap конфига модуля
        if (empty($module->modelMap[$modelType])) {
            throw new InvalidConfigException('Not set model in modelMap config module parameter');
        }

        $modelClass = $module->modelMap[$modelType];

        if (!array_search(WishListInterface::class, class_implements($modelClass))) {
            throw new InvalidConfigException('Not set interface ' . WishListInterface::class . ' in model');
        }

        if (!self::isSelected($id, $module::TYPE_WISH, $modelType)) {
            $model = $modelClass::findOne($id);
            $data = !empty($model) ? $model->getWishWidgetListData() : null;
        }

        $selectResult = self::setSelected($id, $module::TYPE_WISH, $modelType, $data);

        $html = UwmWidget::widget([
            'template' => '@frontend/views/site/wish/wish_list_widget',
            'type' => $module::TYPE_WISH,
        ]);
        $list = $module::getList($module::TYPE_WISH, $modelType);


        $result['result'] = $selectResult;
        $result['data'] = $html;
        $result['count'] = count($list);

        return $result;
    }
}
