<?php
namespace frontend\modules\uwm\controllers;

use frontend\models\Clock;
use frontend\modules\uwm\helpers\CartListInterface;
use frontend\modules\uwm\widget\UwmWidget;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\Response;

/**
 * Class CompareController
 */
class CartController extends UwmController
{

    /**
     * Добавляет/удаляет элемент для сравнения.
     *
     * @param integer $id Идентификатор элемента
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionIndex($id)
    {
        $data = null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $module = $this->module;

        $modelType = Yii::$app->request->post('model');


        // Проверка на наличия модели в modelMap конфига модуля
        if (empty($module->modelMap[$modelType])) {
            throw new InvalidConfigException('Not set model in modelMap config module parameter');
        }

        $modelClass = $module->modelMap[$modelType];

        if (!array_search(CartListInterface::class, class_implements($modelClass))) {
            throw new InvalidConfigException('Not set interface ' . CartListInterface::class . ' in model');
        }

        if (!self::isSelected($id, $module::TYPE_CART, $modelType)) {
            $model = $modelClass::findOne($id);
            $data = !empty($model) ? $model->getCartWidgetListData() : null;
        }

        $selectResult = self::setSelected($id, $module::TYPE_CART, $modelType, $data);


        $html = UwmWidget::widget([
            'template' => '@frontend/views/site/cart/cart_list_widget',
            'type' => $module::TYPE_CART,
        ]);

        $result['id'] = $id;
        $result['result'] = $selectResult;
        $result['data'] = $html;

        return $result;
    }
}
