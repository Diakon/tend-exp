<?php
namespace frontend\modules\uwm\controllers;

use common\modules\car\models\frontend\Car;
use Yii;
use yii\web\Response;
use frontend\modules\uwm\Module;
use yii\base\InvalidConfigException;
use common\modules\car\models\common\CarAccessories;

/**
 * Class AccessoriesController
 */
class AccessoriesController extends UwmController
{
    /**
     * Добавляет/удаляет аксесуары авто
     *
     * @param integer $id Идентификатор элемента
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionIndex($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 400;

        /** @var Module $module */
        $module = $this->module;

        $carId = (int)Yii::$app->request->post('carId');

        // Проверка на то, что есть авто
        $car = Car::find()
            ->where(['id' => $carId])
            ->andWhere(['status' => Car::STATUS_ACTIVE])
            ->one();
        if (empty($car)) {
            throw new InvalidConfigException('Car not found');
        }

        //Проверка на то, что для этого авто назначен верный аксессуар
        //(в списке аксесуаров доступных для авто, есть переданный в контроллер аксесуар)
        $carAccessories = CarAccessories::getAccessoriesCar($car);
        if (!isset($carAccessories[$id])) {
            throw new InvalidConfigException('Accessories not found');
        }


        //Вставляем / удаляем аксесуар для этого авто
        self::setSelected($id, $module::TYPE_ACCESSORIES, CarAccessories::classNameShort() . $carId, $id);
        //Получаем стоимость всех выбранных для авто аксесуаров
        $accessoriesPrice = CarAccessories::getAccessoriesSumPrice(self::getList($module::TYPE_ACCESSORIES, CarAccessories::classNameShort() . $carId));

        Yii::$app->response->statusCode = 200;
        $result = [];
        $result['id'] = $id;
        $result['totalPrice'] = Yii::$app->formatter->asMoney($car->price + $accessoriesPrice);

        return $result;
    }
}