<?php
namespace frontend\modules\uwm;

use frontend\modules\uwm\helpers\UwmTrait;

/**
 * Class Module.
 */
class Module extends \yii\base\Module
{
    use UwmTrait;

    /**
     * Тип: сравнение
     */
    const TYPE_COMPARE = 'compare';

    /**
     * Тип: корзина
     */
    const TYPE_CART = 'cart';

    /**
     * Тип: избранное
     */
    const TYPE_WISH = 'wish';

    /**
     * @var array Модели используемые в модуле
     */
    public $modelMap = [];
}
