<?php
namespace backend\modules\pages\models\backend;

use modules\crud\behaviors\FieldFormatBehavior;
use modules\crud\behaviors\FormConfigBehavior;
use modules\crud\behaviors\SystemDataBehavior;
use modules\upload\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use \Yii;
use modules\pages\models\backend\Pages as BasePages;

class Pages extends BasePages
{



    /**
     * Шаблоны страниц
     *
     *
     * @return array
     */
    public  static function getTemplateList() {
        return [
            \common\models\Pages::MAIN_TEMPLATE => Yii::t('app', 'Стандартный шаблон'),
            \common\models\Pages::TARIF_TEMPLATE => Yii::t('app', 'Шаблон с тарифами'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getListLink($action = 'pages_list', array $params = [])
    {
        return $this->generateUrl($action, $params);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),  [
            [[ 'type_templates'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'type_templates' => Yii::t('app', 'Шаблон страницы'),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return  [
            'timestamp'   => TimestampBehavior::className(),
            'systemData'  => SystemDataBehavior::className(),
            'forms'       => FormConfigBehavior::className(),
            'mainPhoto' => [
                'class' => UploadBehavior::class,
                'attributes' => [
                    'img' => [
                        'singleFile' => true,
                        'rules' => [
                            'image' => [
                                'extensions' => 'png,jpeg,jpg,bmp',
                                'minWidth' => 10,
                                'maxWidth' => 1600,
                                'minHeight' => 100,
                                'maxHeight' => 1200,
                                'maxSize' => 3 * 1024 * 1024,
                            ],
                        ],
                    ],
                ]
            ],
            'fieldFormat' => [
                'class' => FieldFormatBehavior::className(),
                'dateFormat' => \Yii::$app->params['datePagesFormat'],
                'dateFields' => ['date_published'],
            ],
        ];
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => Yii::t('app', 'Новости')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => Yii::t('app', 'Новости'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? Yii::t('app', 'Создать новость') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
