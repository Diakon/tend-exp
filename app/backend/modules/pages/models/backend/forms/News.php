<?php
namespace backend\modules\pages\models\backend\forms;

use common\components\Thumbnail;
use \Yii;
use yii\helpers\Url;
use yii\jui\DatePicker;
use modules\pages\models\common\Pages as BasePages;

/**
 * Конфигурация формы добавления/редактирования статических страниц.
 */
class News extends \modules\pages\models\backend\forms\Pages
{
    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return $this->owner->isNewRecord ? Yii::t('app', 'Добавить страницу') : $this->owner->title;
    }

    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        /** @var News $model */
        $model = $this->owner;

        return [
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            'title' => ['type' => 'text'],
            'url' => ['type' => 'text'],
            'is_important' => ['type' => 'checkbox'],
            'status' => ['type' => 'checkbox'],
            '<div class="form-group">',
            '<div class="col-sm-2 control-label"><label for="pages-date_published">' . Yii::t('app', 'Дата публикации') .'</label></div>',
            '<div class="col-sm-10">',
            DatePicker::widget(
                [
                    'name' => 'Pages[date_published]',
                    'value' => $model->datePublished,
                    'dateFormat' => Yii::$app->params['datePickerFormat'],
                    'options' => ['class' => 'form-control datepicker'],
                ]
            ),
            '</div>',
            '</div>',
            'type' => ['type' => 'hidden'],
            '</fieldset>',
            '</div>',
            ['type' => 'tab', 'label' => Yii::t('app', 'Дополнительно')],
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            'short' => [
                'type' => 'textarea',
                'attributes' => [
                    'class' => 'form-control wysiwyg',
                    'rows' => 10,
                    'data-url-image-upload' => Url::to(
                        [Yii::$app->params['uploadRedactorImageHandler'], 'id' => $this->owner->id, 'model' => BasePages::className(), 'attribute' => 'redactor_image']
                    ),

                ],
            ],
            'full' => [
                'type' => 'textarea',
                'attributes' => [
                    'class' => 'form-control wysiwyg',
                    'rows' => 10,
                    'data-url-image-upload' => Url::to(
                        [Yii::$app->params['uploadRedactorImageHandler'], 'id' => $this->owner->id, 'model' => BasePages::className(), 'attribute' => 'redactor_image']
                    ),
                ],
            ],
            '</fieldset>',
            '</div>',
            ['type' => 'tab', 'label' => Yii::t('app', 'Галерея')],
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            'img' => ['type' => 'file'],
            (!empty($model->img) ?
                '<div class="file-thumbnail js-file-thumbnail"><div>' . Thumbnail::img($model->img) . '</div><button type="button" class="btn btn-default btn-xs js-file-delete" data-url="' . Url::to(['control/ajax_delete_file', 'model' => $model::className(), 'id' => $model->id, 'attribute' => 'photo']) . '"><i class="glyphicon glyphicon-trash"></i> ' . Yii::t('app', 'Delete file') . '</button></div>' :
                '') . '<hr/>',
            '<div><h3>' . Yii::t('app', 'Video') . '</h3></div>',
            'video' => ['type' => 'text'],
            '</fieldset>',
            '</div>',
            ['type' => 'tab', 'label' => Yii::t('app', 'SEO-настройки')],
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            'title_page' => ['type' => 'text'],
            'meta_keywords' => ['type' => 'text'],
            'meta_description' => ['type' => 'text'],
            '</fieldset>',
            '</div>',
        ];
    }
}
