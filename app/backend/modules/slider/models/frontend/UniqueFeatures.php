<?php
namespace backend\modules\slider\models\frontend;

use yii\helpers\ArrayHelper;

class UniqueFeatures extends \modules\slider\models\backend\Slider
{


    /**
     * @inheritdoc
     */
    public function rules()
    {

        return ArrayHelper::merge(parent::rules(), [
            [['titleSecond'], 'required'],
            [['titleSecond'], 'string', 'max' => 255]
        ]);
    }

    public static function tableName()
    {
        return '{{%unique_features}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return  ArrayHelper::merge(parent::attributeLabels(),[
            'titleSecond' => \Yii::t('app', 'Второй заголовок'),
        ]);
    }
}
