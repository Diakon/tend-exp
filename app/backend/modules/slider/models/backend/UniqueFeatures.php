<?php
namespace backend\modules\slider\models\backend;

use yii\helpers\ArrayHelper;

class UniqueFeatures extends \modules\slider\models\backend\Slider
{

    /**
     * @inheritdoc
     */
    public function getListLink($action = 'unique_features_list', array $params = [])
    {
        return $this->generateUrl($action, $params);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return ArrayHelper::merge(parent::rules(), [
            [['titleSecond'], 'required'],
            [['titleSecond'], 'string', 'max' => 255]
        ]);
    }

    public static function tableName()
    {
        return '{{%unique_features}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return  ArrayHelper::merge(parent::attributeLabels(),[
            'titleSecond' => \Yii::t('app', 'Второй заголовок'),
        ]);
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Уникальные предложения')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Уникальные предложения'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать предложение') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
