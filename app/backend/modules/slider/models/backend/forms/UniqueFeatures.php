<?php
namespace backend\modules\slider\models\backend\forms;

use modules\crud\behaviors\FormConfigBehavior;
use components\Thumbnail;

/**
 * Конфигурация формы
 *
 * @package modules\slider\models\backend\forms
 */
class UniqueFeatures extends FormConfigBehavior
{

    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        return [
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            'title' => ['type' => self::TYPE_TEXT],
            'titleSecond' => ['type' => self::TYPE_TEXT],
            'image' => ['type' => self::TYPE_FILE],
            (!empty($this->owner->image) ? '<div class="form-group"><div class="col-sm-10 col-sm-offset-2">' . Thumbnail::img($this->owner->image) . '</div></div>' : '') . '<hr/>',

            'text' => [
                'type' => self::TYPE_TEXT_AREA,
                'attributes' => ['class' => 'form-control wysiwyg', 'rows' => 10],
                'options' => ['template' => '{input}{error}'],
            ],
            'url' => ['type' => self::TYPE_TEXT],
            'publish' => ['type' => self::TYPE_CHECKBOX],
            'sort' => ['type' => self::TYPE_TEXT],
            '</fieldset>',
            '</div>',
        ];
    }
}
