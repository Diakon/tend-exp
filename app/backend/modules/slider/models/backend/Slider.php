<?php
namespace backend\modules\slider\models\backend;
use Yii;

class Slider extends \modules\slider\models\backend\Slider
{

    /**
     * @inheritdoc
     */
    public function getListLink($action = 'slider_list', array $params = [])
    {
        return $this->generateUrl($action, $params);
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => Yii::t('app', 'Слайдер')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => Yii::t('app', 'Слайдер'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? Yii::t('app', 'Создать слайдер') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
