<?php
namespace backend\controllers;

use backend\modules\slider\models\backend\UniqueFeatures;
use common\modules\settings\controllers\AdminController;
use yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\components\Controller;
use common\modules\users\controllers\actions\Login;
use zxbodya\yii2\galleryManager\GalleryManagerAction;
use backend\modules\settings\models\backend\Settings;
use backend\modules\pages\models\backend\Pages;
use backend\modules\pages\models\backend\News;
use backend\modules\slider\models\backend\Slider;

use helpers\Dev;
use modules\crud\actions\CreateAction;
use modules\crud\actions\DeleteAction;
use modules\crud\actions\ListAction;
use modules\admin\controllers\actions\IndexAction;

use backend\models\GeoCity;
use backend\models\GeoRegion;
use backend\models\GeoArea;
use backend\models\GeoMun;
use backend\models\GeoCountry;
use backend\controllers\actions\GeoAction;

use backend\controllers\actions\CompanyAction;
use backend\models\Company;

use backend\models\Activities;


/**
 * Основной контроллер для админки приложения.
 */
class BackendController extends Controller
{
    /**
     * @var string|boolean $layout The name of the layout to be applied to this controller's views.
     */
    public $layout = '@root/themes/backend/views/layouts/main';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Dev::flush();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post', 'get'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['administrator', 'moderator'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->response->redirect(Yii::$app->request->hostInfo);
                    Yii::$app->end();
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = [
            'index' => [
                'class' => IndexAction::class,
                'params' => [
                    'template' => 'index',
                ],
            ],
            'login' => [
                'class' => Login::class,
            ],

            'slider_list' => [
                'class' => ListAction::class,
                'model' => Slider::class,
                'template' => 'slider/slider_list',
            ],
            'slider_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Slider::class,
                    'messageAdd' => Yii::t('cms', 'Item created'),
                ],
            ],
            'slider_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Slider::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'slider_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => Slider::class,
                    'returnUrl' => ['backend/sliders'],
                ],
            ],
            'unique_features_list' => [
                'class' => ListAction::class,
                'model' => UniqueFeatures::class,
                'template' => 'slider/unique_features_list',
            ],
            'unique_features_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => UniqueFeatures::class,
                    'messageAdd' => Yii::t('cms', 'Item created'),
                ],
            ],
            'unique_features_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => UniqueFeatures::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'unique_features_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => UniqueFeatures::class,
                    'returnUrl' => ['backend/unique_features'],
                ],
            ],

            'pages_list' => [
                'class' => ListAction::class,
                'model' => Pages::class,
                'attributes' => [
                    'type' => Pages::TYPE_STATIC,
                ],
                'template' => 'pages/list',
            ],
            'pages_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Pages::class,
                    'attributes' => [
                        'type' => Pages::TYPE_STATIC,
                    ],
                    'messageAdd' => Yii::t('cms', 'Item created'),
                ],
            ],
            'pages_edit' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Pages::class,
                    'attributes' => [
                        'type' => Pages::TYPE_STATIC,
                    ],
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'pages_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => Pages::class,
                    'returnUrl' => ['backend/pages'],
                ],
            ],

            'news_list' => [
                'class' => ListAction::class,
                'model' => News::class,
                'attributes' => [
                    'type' => News::TYPE_NEWS
                ],
                'template' => 'pages/list',
            ],
            'news_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => News::class,
                    'attributes' => [
                        'type' => News::TYPE_NEWS,
                    ],
                    'messageAdd' => Yii::t('cms', 'Item created'),
                ],
            ],
            'news_edit' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => News::class,
                    'attributes' => [
                        'type' => News::TYPE_NEWS,
                    ],
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'news_delete' => [
                'class' => DeleteAction::class,
                'params' => [
                    'model' => News::class,
                    'returnUrl' => ['backend/news'],
                ],
            ],
            'gallery_api_pages' => [
                'class' => GalleryManagerAction::class,
                'types' => [
                    'news' => Pages::class,
                ],
            ],
            'settings_list' => [
              'class' => AdminController::class
            ],
            'settings_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Settings::class,
                ],
            ],
            'setting_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => Settings::class,
                    'returnUrl' => 'backend/setting_update',
                ],
            ],

            'geo_city_list' => [
                'class' => GeoAction::class,
                'model' => GeoCity::class,
                'template' => 'geo/city/list',
            ],
            'geo_city_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoCity::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_city_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoCity::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_region_list' => [
                'class' => GeoAction::class,
                'model' => GeoRegion::class,
                'template' => 'geo/region/list',
            ],
            'geo_region_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoRegion::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_region_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoRegion::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_area_list' => [
                'class' => GeoAction::class,
                'model' => GeoArea::class,
                'template' => 'geo/area/list',
            ],
            'geo_area_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoArea::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_area_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoArea::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_mun_list' => [
                'class' => GeoAction::class,
                'model' => GeoMun::class,
                'template' => 'geo/mun/list',
            ],
            'geo_mun_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoMun::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_mun_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoMun::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_country_list' => [
                'class' => GeoAction::class,
                'model' => GeoCountry::class,
                'template' => 'geo/country/list',
            ],
            'geo_country_update' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoCountry::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
            'geo_country_create' => [
                'class' => CreateAction::class,
                'params' => [
                    'model' => GeoCountry::class,
                    'messageUpdate' => Yii::t('cms', 'Item updated'),
                ],
            ],
        ];

        return $actions;
    }
}
