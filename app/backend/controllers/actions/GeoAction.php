<?php
namespace backend\controllers\actions;

use modules\crud\actions\ListAction;
use Yii;

/**
 * Class GeoAction
 * @package backend\controllers\actions
 */
class GeoAction extends ListAction
{
    /**
     * @var GeoAction Модель гео.
     */
    public $model;

    public function run()
    {
        $this->model = new $this->model($this->attributes);
        $view = $this->controller->getView();
        $class = $this->model::classNameShort();

        // Устанавливаем breadcrumbs
        $view->params['breadcrumbs'] = $this->model->getAdminBreadcrumbsData($this->model::BREADCRUMBS_TYPE_LIST);
        $view->params['breadcrumbs_buttons'] = [
            ['url' => $this->model->addLink, 'title' => '<i class="glyphicon glyphicon-plus-sign"></i> ' . Yii::t('cms/crud', 'Add new'), 'options' => ['class' => 'btn btn-w-m btn-success']],
        ];

        // Устанавливаем добавочный класс для wrapper container
        $view->params['wrapperContentAdditionalClass'] = 'animated fadeInRight';
        // Устанавливаем добавочный класс для main container
        $view->params['mainContainerClass'] = ' ';

        $page = Yii::$app->request->getQueryParam('page');
        $this->model->pageSize = (is_numeric(Yii::$app->request->get('pageSize', $this->model->pageSize))) ? Yii::$app->request->get('pageSize', $this->model->pageSize) : $this->model->pageSize;
        $this->applyPagination($class, $page);

        if ($this->model->validate()) {
            $this->model->load(Yii::$app->request->get());
        }

        return $this->controller->render(
            $this->template,
            [
                'model'        => $this->model,
                'dataProvider' => $this->model->setDataProviderList(),
            ]
        );
    }
}