<?php
namespace backend\components;

use modules\navigation\components\Menu as BaseMenu;
use Yii;

/**
 * Class Menu Виджет меню администратора
 *
 * @package backend\components
 */
class Menu extends BaseMenu
{
    /**
     * Функция проверки активного пункта меню
     *
     * @param mixed $item Пункт меню
     *
     * @return boolean
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = Yii::getAlias($item['url'][0]);
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }

            // Разбиваем роуты на составляющие
            $explodeRoute = explode('/', ltrim($route, '/'));
            $explodeThisRoute = explode('/', $this->route);
            // Получаем первую часть роута, прим: clock_
            preg_match('/^([a-zA-z]*_)/', $explodeRoute[1], $prefixRoute);
            preg_match('/^([a-zA-z]*_)/', $explodeThisRoute[1], $prefixThisRoute);

            // Проверяем на совпадение первые части роутов пример: clock_list && clock_view
            // Проверяем на совпадение части роутов пример: user/admin/index && user/admin/update
            if (!empty($prefixRoute[0]) && !empty($prefixThisRoute[0]) && ($prefixRoute[0] == $prefixThisRoute[0])) {
                return true;
            } elseif (!empty($explodeRoute[0]) && !empty($explodeThisRoute[0]) && !empty($explodeRoute[1]) && !empty($explodeThisRoute[1]) && $explodeRoute[0] == $explodeThisRoute[0] && $explodeRoute[1] == $explodeThisRoute[1]) {
                return true;
            }

            if (ltrim($route, '/') !== $this->route) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                $params = $item['url'];
                unset($params[0]);
                foreach ($params as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }
}
