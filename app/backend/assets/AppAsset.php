<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Ресурсы приложения.
 */
class AppAsset extends AssetBundle
{

    /**
     * @inheritdoc
     */
    public $depends = [
        'assets\CmsAsset',
    ];

    /**
     * @inheritdoc
     */
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
