<?php

$name = Yii::$app->user->identity->username ?? '';
$roles = Yii::$app->user->identity->getRoles(false) ?? '';

return [
    [
        'label' => '',
        'url' => ['/control/pages_list'],
        'template' => '<div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">' . $name . '</strong>
                             </span> <span class="text-muted text-xs block">' . $roles . '</span> </span> </a>
                    </div>
                    <div class="logo-element"></div>',
        'visible' => true,
        'options' => ['class' => 'nav-header'],
    ],
    [
        'label' => Yii::t('cms', 'Dashboard'),
        'url' => ['/backend/index'],
        'template' => '<a href="{url}"><i class="fa fa-file-text-o"></i> <span class="nav-label">{label}</span></a>',
        'visible' => true,
    ],
    [
        'label' => Yii::t('cms', 'Слайдер'),
        'url' => ['/backend/slider_list'],
        'template' => '<a href="{url}"><i class="fa fa-picture-o"></i><span class="nav-label">{label}</span></a>',
        'visible' => true,
    ],
    [
        'label' => Yii::t('cms', 'Уникальные предложения'),
        'url' => ['/backend/unique_features_list'],
        'template' => '<a href="{url}"><i class="fa fa-picture-o"></i><span class="nav-label">{label}</span></a>',
        'visible' => true,
    ],
    [
        'label' => Yii::t('cms', 'Страницы'),
        'url' => ['/backend/pages_list'],
        'template' => '<a href="{url}"><i class="fa fa-file-text-o"></i> <span class="nav-label">{label}</span></a>',
        'visible' => true,
    ],
    [
        'label' => Yii::t('cms', 'Новости'),
        'url' => ['/backend/news_list'],
        'template' => '<a href="{url}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">{label}</span></a>',
        'visible' => true,
    ],
    [
        'label' => Yii::t('app', 'Справочники'),
        'url' => '#',
        'visible' => Yii::$app->user->can('administrator'),
        'template' => '<a href="#"><i class="fa fa-book"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => \yii\helpers\ArrayHelper::merge(\common\modules\directories\models\common\Directories::getMenu(),[
        ] )
    ],
    [
        'label' => Yii::t('app', 'География'),
        'url' => '#',
        'visible' => true,
        'template' => '<a href="#"><i class="fa fa-map-marker"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => [
            [
                'label' => Yii::t('app', 'Страны'),
                'url' => ['/backend/geo_country_list'],
                'visible' => true,
            ],
            [
                'label' => Yii::t('app', 'Округа'),
                'url' => ['/backend/geo_area_list'],
                'visible' => true,
            ],
            [
                'label' => Yii::t('app', 'Регионы'),
                'url' => ['/backend/geo_region_list'],
                'visible' => true,
            ],
            [
                'label' => Yii::t('app', 'Города'),
                'url' => ['/backend/geo_city_list'],
                'visible' => true,
            ],
            [
                'label' => Yii::t('app', 'Муниципалитеты'),
                'url' => ['/backend/geo_mun_list'],
                'visible' => true,
            ],
        ],
    ],
    [
        'label' => Yii::t('app', 'Компании'),
        'url' => '#',
        'visible' => Yii::$app->user->can('administrator'),
        'template' => '<a href="#"><i class="fa fa-university"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => \common\modules\company\models\backend\Company::getMenu()
    ],
    [
        'label' => Yii::t('app', 'Тендеры'),
        'url' => '#',
        'visible' => Yii::$app->user->can('administrator'),
        'template' => '<a href="#"><i class="fa fa-gavel"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => \common\modules\tender\models\backend\Tenders::getMenu()
    ],
    [
        'label' => Yii::t('app', 'Предложения'),
        'url' => '#',
        'visible' => Yii::$app->user->can('administrator'),
        'template' => '<a href="#"><i class="fa fa-pie-chart"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => \common\modules\offers\models\backend\Offers::getMenu()
    ],
    [
        'label' => Yii::t('app', 'Тарифные планы'),
        'url' => '#',
        'visible' => Yii::$app->user->can('administrator'),
        'template' => '<a href="#"><i class="fa fa-money"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => \common\modules\tariff\models\backend\TariffPlans::getMenu(),
    ],
    [
        'label' => Yii::t('cms', 'Users'),
        'url' => '#',
        'visible' => true,
        'template' => '<a href="#"><i class="fa fa-users"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => [
            [
                'label' => Yii::t('cms', 'Пользователи'),
                'url' => ['/user/admin/index'],
                'visible' => true,
            ],
            [
                'label' => Yii::t('cms', 'User groups'),
                'url' => ['/user/rbac/index'],
                'visible' => \helpers\Dev::check() ? true : false,
            ],
        ],
    ],
    [
        'label' => Yii::t('cms', 'Настройки сайта'),
        'url' => '#',
        'visible' => true,
        'template' => '<a href="#"><i class="fa fa-gears"></i> <span class="nav-label">{label}</span><span class="fa arrow"></span></a>',
        'items' => [
            [
                'label' => Yii::t('cms', 'Настройки'),
                'url' => ['/settings/admin/list'],
                'visible' => true,
            ],
            [
                'label' => Yii::t('cms', 'Меню'),
                'url' => ['/menu/dashboard/index'],
                'visible' => true,
                'template' => '<a href="{url}"><i class="fa fa-list"></i> <span class="nav-label">{label}</span></a>',
            ],
        ],
    ],
];
