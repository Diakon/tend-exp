<?php
namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\GeoRegion as CommonGeoRegion;

/**
 * Class GeoRegion
 * @package backend\models
 */
class GeoRegion extends CommonGeoRegion
{
    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * Возвращает dataProvider для списка регионов
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['name' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }

    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Регионы')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Регионы'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать регион') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

}
