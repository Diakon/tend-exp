<?php
namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\GeoMun as CommonGeoMun;

/**
 * Class GeoMun
 * @package backend\models
 */
class GeoMun extends CommonGeoMun
{
    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * Возвращает dataProvider для списка орегионов
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['name' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Муниципалитеты')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Муниципалитеты'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать муниципалитет') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }
}
