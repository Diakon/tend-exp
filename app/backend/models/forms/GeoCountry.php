<?php
namespace backend\models\forms;

use yii;
use modules\crud\behaviors\FormConfigBehavior;

/**
 * Конфигурация формы списка стран
 */
class GeoCountry extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        return [
            '<h2>Страна ' . $this->owner->name . '</h2>',
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            '<div class="clear">',
            '<div class="list-group col-xs-6">',
            'name' => ['type' => self::TYPE_TEXT],
            '</div>',
            '</div>',

            '</fieldset>',
            '</div>',
        ];
    }
}
