<?php
namespace backend\models\forms;

use yii;
use backend\models\GeoRegion;
use modules\crud\behaviors\FormConfigBehavior;

/**
 * Конфигурация формы списка муниципалитетов
 */
class GeoMun extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        return [
            '<h2>Муниципалитет ' . $this->owner->name . '</h2>',
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            '<div class="clear">',
            '<div class="list-group col-xs-6">',
            'name' => ['type' => self::TYPE_TEXT],
            'region_id' => [
                'type' => self::TYPE_DROP_DOWN_LIST,
                'items' => GeoRegion::getRegionsList(),
                'attributes' => [
                    'class' => 'select_one form-control',
                    'prompt' => \Yii::t('app', '-----'),
                ],
            ],
            'popul_all' => ['type' => self::TYPE_TEXT],
            'popul_municipal' => ['type' => self::TYPE_TEXT],
            'popul_regional' => ['type' => self::TYPE_TEXT],
            '</div>',
            '</div>',

            '</fieldset>',
            '</div>',
        ];
    }
}
