<?php
namespace backend\models\forms;

use common\models\GeoArea;
use yii;
use common\models\GeoCountry;
use modules\crud\behaviors\FormConfigBehavior;

/**
 * Конфигурация формы списка регионов
 */
class GeoRegion extends FormConfigBehavior
{
    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        return [
            '<h2>Регион ' . $this->owner->name . '</h2>',
            '<div class="panel-body">',
            '<fieldset class="form-horizontal">',
            '<div class="clear">',
            '<div class="list-group col-xs-6">',
            'name' => ['type' => self::TYPE_TEXT],
            'area_id' => [
                'type' => self::TYPE_DROP_DOWN_LIST,
                'items' => GeoArea::getAreaList(),
                'attributes' => [
                    'class' => 'select_one form-control',
                    'prompt' => \Yii::t('app', '-----'),
                ],
            ],
            'country_id' => [
                'type' => self::TYPE_DROP_DOWN_LIST,
                'items' => GeoCountry::getCountryList(),
                'attributes' => [
                    'class' => 'select_one form-control',
                    'prompt' => \Yii::t('app', '-----'),
                ],
            ],
            'coord_lon' => ['type' => self::TYPE_TEXT],
            'coord_lat' => ['type' => self::TYPE_TEXT],
            'popul_all' => ['type' => self::TYPE_TEXT],
            'popul_regional' => ['type' => self::TYPE_TEXT],
            '</div>',
            '</div>',

            '</fieldset>',
            '</div>',
        ];
    }
}
