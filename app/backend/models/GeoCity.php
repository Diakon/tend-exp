<?php
namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\GeoCity as CommonGeoCity;

/**
 * Class GeoCity
 * @package backend\models
 */
class GeoCity extends CommonGeoCity
{
    /**
     * @var integer
     */
    public $pageSize = 15;

    /**
     * Возвращает dataProvider для списка орегионов
     */
    public function setDataProviderList()
    {
        $query = static::find();
        $query->orderBy(['name' => SORT_ASC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount'    => $query->count(),
                'pageSize'      => $this->pageSize ?? 15,
                'pageSizeParam' => false,
            ],
        ]);
    }


    /**
     * Хлебные крошки для админки
     *
     * @param string $type
     *
     * @return array
     */
    public function getAdminBreadcrumbsData($type = self::BREADCRUMBS_TYPE_ITEM): array
    {
        switch ($type) {
            case self::BREADCRUMBS_TYPE_LIST:
                $result[] = ['label' => \Yii::t('app', 'Города')];
                break;
            case self::BREADCRUMBS_TYPE_ITEM:
                $result[] = ['label' => \Yii::t('app', 'Города'), 'url' => $this->getListLink()];
                $result[] = ['label' => $this->isNewRecord ? \Yii::t('app', 'Создать город') : $this->title];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

}
