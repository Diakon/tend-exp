<?php
/**
 * @var \base\BaseActiveRecord $model Модель.
 */

use components\ActiveForm;
use \yii\helpers\Html;

/** @var ActiveForm $form */
$form = Yii::$app->activeForm;
$form::begin();
$fields = $model->getFormsFields();
?>
    <div class="form-page clearfix">
        <?php
        echo $form->errorSummary($model, ['class' => 'alert alert-danger']);
        ?>
        <div class="row-fluid form-wrapper">
            <?php
            $additionalElementOption = [
                'labelOptions' => ['class' => 'col-sm-2 control-label'],
                'template' => '{label}<div class="col-sm-10" >{input}{hint}{error}</div>',
                'checkboxTemplate' => '<div class="col-sm-2 control-label" >{beginLabel}{labelTitle}{endLabel}</div><div class="col-sm-10" >{input}{hint}{error}</div>',
            ];

            foreach ($fields['elements'] as &$element) {
                if (isset($element['type'])) {
                    $element['options'] = isset($element['options']) ? $element['options'] : [];
                    $element['options'] = \yii\helpers\ArrayHelper::merge($element['options'], $additionalElementOption);
                }
            }
            $content = $form->renderFields($model, $fields['elements']);
            if (count($content) > 1) {
                echo \yii\bootstrap\Tabs::widget([
                    'items' => $content,
                ]);
            } else {
                echo $content[0]['content'];
            }
            ?>
        </div>
    </div>

    <div class="modal-footer">
        <input type="hidden" name="apply" id="apply-field" value="0">
        <?php
        foreach ($fields['buttons'] as $key => $value) {
            switch ($key) {
                case 'cancel':
                    echo $value ? Html::a($value[0], $model->getListLink(), $value[1] ?? []) : '';
                    break;
                default:
                    echo $value ? Html::button($value[0], $value[1] ?? []) : '';
                    break;
            }
        }

        ?>
    </div>
<?php
$form::end();

