<?php
/**
 * @var yii\web\View $this
 * @var string       $content
 */

use backend\assets\AppAsset;

use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <title><?= Html::encode($this->title) ?></title>
</head>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody(); ?>
</html>
<?php $this->endPage(); ?>
