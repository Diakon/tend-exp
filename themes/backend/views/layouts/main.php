<?php

use modules\admin\Module;
use components\AdminMenu;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Alert;
use yii\helpers\Html;

$this->beginContent('@cms/themes/backend/views/layouts/base.php');
?>
<body class="pace-done">
<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?= AdminMenu::widget([
                'encodeLabels' => false,
                'menuFile' => Yii::getAlias('@backend/helpers/_main_menu.php'),
                'activeCssClass' => 'active',
                'activateParents' => true,
                'controller' => $this->context,
                'showFrom' => 1,
                'showTo' => 2,
                'options' => ['class' => 'nav metismenu', 'id' => 'side-menu'],
                'submenuTemplate' => "\n<ul class='nav nav-second-level collapse'>\n{items}\n</ul>\n",
            ]); ?>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                    <div class="admin-title-wrap">
                        <span class="h3 vert-middle admin-title"><?= Yii::$app->settings->get('siteName', 'site') ?? Yii::$app->name ?>
                            <span
                                    class="h4"><?= Yii::t('cms', 'Admin panel') ?></span></span>
                    </div>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a target="_blank" href="<?= Yii::$app->urlManager->createAbsoluteUrl('..') ?>">
                            <i class="fa fa-home"></i><?= Yii::t('cms', 'Frontend') ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/user/user/logout']); ?>">
                            <i class="fa fa-sign-out"></i><?= Yii::t('cms', 'Log out') ?>
                        </a>
                    </li>
                </ul>

            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-8">
                <?php
                $breadcrumbs = isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [];
                $popBreadcrumbsElement = !empty($this->params['breadcrumbs']) ? array_pop($this->params['breadcrumbs']) : [];
                $popBreadcrumbsElementLabel = !empty($popBreadcrumbsElement['label']) ? $popBreadcrumbsElement['label'] : null;
                ?>
                <h2><?= !is_null($popBreadcrumbsElementLabel) ? $popBreadcrumbsElementLabel : $this->title ?></h2>
                <?php
                echo Breadcrumbs::widget([
                    'homeLink' => ['label' => Yii::t('cms', 'Home'), 'url' => Yii::$app->homeUrl],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
            <?php if (!empty($this->params['breadcrumbs_buttons']) && is_array($this->params['breadcrumbs_buttons'])) { ?>
                <div class="col-lg-4">
                    <div class="ibox-content float-right">
                        <?php foreach ($this->params['breadcrumbs_buttons'] as $button) {
                            if (!empty($button['title']) && !empty($button['url'])) {
                                echo Html::a($button['title'], $button['url'], $button['options']) . ' &nbsp;';
                            } else {
                                echo $button;
                            }
                        } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php
                $wrapperContentAdditionalClass = !empty($this->params['wrapperContentAdditionalClass']) ? $this->params['wrapperContentAdditionalClass'] : '';
                $mainContainerClass = !empty($this->params['mainContainerClass']) ? $this->params['mainContainerClass'] : 'ibox float-e-margins ibox-content';
                ?>
                <div class="wrapper wrapper-content <?= $wrapperContentAdditionalClass ?>">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="<?= $mainContainerClass ?>">
                                <?php
                                $flash = \Yii::$app->session->getAllFlashes();
                                foreach ($flash as $key => $mess) {
                                    $flashOptions = ['class' => 'alert-' . ($key == 'error' ? 'danger' : $key)];
                                    if (is_array($mess)) {
                                        foreach ($mess as $k => $m) {
                                            echo Alert::widget(['options' => $flashOptions, 'body' => $m]);
                                        }
                                    } else {
                                        echo Alert::widget(['options' => $flashOptions, 'body' => $mess]);
                                    }
                                }
                                ?>
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div>
                        <?= Module::getCopyright(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php $this->endContent(); ?>
