<?php
/**
 * @var View               $this
 * @var GeoRegion          $model
 * @var ActiveDataProvider $dataProvider
 */

use backend\models\GeoRegion;
use yii\data\ActiveDataProvider;
use common\components\GridView;
use common\models\GeoCountry;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'name',
                        [
                            'attribute' => 'area_id',
                            'format' => 'raw',
                            'value' => function (GeoRegion $data) {
                                return $data->area->name;
                            },
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'country_id',
                            'format' => 'raw',
                            'value' => function (GeoRegion $data) {
                                return $data->country->name ?? 'Страна не указана';
                            },
                            'filter' => false,
                        ],
                        'coord_lon',
                        'coord_lat',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
