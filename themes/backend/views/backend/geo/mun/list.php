<?php
/**
 * @var View               $this
 * @var GeoMun             $model
 * @var ActiveDataProvider $dataProvider
 */

use backend\models\GeoMun;
use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'name',
                        [
                            'attribute' => 'region_id',
                            'format' => 'raw',
                            'value' => function (GeoMun $data) {
                                return $data->region->name ?? '-----';
                            },
                            'filter' => false,
                        ],
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
