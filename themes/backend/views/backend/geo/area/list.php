<?php
/**
 * @var View               $this
 * @var GeoArea            $model
 * @var ActiveDataProvider $dataProvider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        'name',
                        'abbr',
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
