<?php
/**
 * @var View               $this
 * @var Pages              $model
 * @var ActiveDataProvider $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use common\components\Thumbnail;
use modules\pages\models\backend\Pages;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        [
                            'attribute' => 'date_published',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'photo',
                            'format' => 'raw',
                            'options' => ['class' => 'column-photo'],
                            'contentOptions' => ['class' => 'text-center'],
                            'value' => function (Pages $data) {
                                return Thumbnail::img(
                                    [
                                        'image' => $data->photo,
                                        'width' => 50,
                                        'height' => 50,
                                    ]
                                );
                            },
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'title',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'status',
                            'options' => ['class' => 'column-status'],
                            'filter' => false,
                        ],
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
