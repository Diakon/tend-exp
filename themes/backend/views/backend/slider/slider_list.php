<?php
/**
 * @var yii\web\View                 $this
 * @var Slider                       $model
 * @var \yii\data\ActiveDataProvider $provider
 */

use backend\modules\slider\models\backend\Slider;
use common\components\GridView;
use common\components\Thumbnail;

?>
<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget([
                'dataProvider' => $provider,
                'filterModel' => $model,
                'layoutControl' => '{edit}{delete}',
                'options' => ['class' => 'grid-view slider-list disable-modal', 'data-pjax' => 0],
                'tableOptions' => ['class' => 'table table-bordered table-hover'],
                'columns' => [
                    'title',
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'options' => ['class' => 'column-photo'],
                        'contentOptions' => ['class' => 'text-center'],
                        'value' => function (Slider $data) {
                            return Thumbnail::img([
                                'image' => $data->image,
                                'width' => 100,
                                'height' => 100,
                            ]);
                        },
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'sort',
                        'filter' => false,
                    ],

                ],
            ]) ?>
        </div>
    </div>
</div>
