<?php
/**
 * @var View               $this
 * @var Activities         $model
 * @var ActiveDataProvider $provider
 */

use yii\data\ActiveDataProvider;
use common\components\GridView;
use common\components\Thumbnail;
use backend\modules\pages\models\backend\Pages;
use yii\web\View;

?>

<div class="row">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <?= GridView::widget(
                [
                    'dataProvider' => $provider,
                    'filterModel' => $model,
                    'layoutControl' => '{edit}{delete}',
                    'options' => ['class' => 'grid-view news-list disable-modal'],
                    'columns' => [
                        [
                            'attribute' => 'title',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'status',
                            'options' => ['class' => 'column-status'],
                            'filter' => false,
                        ],
                    ],
                ]
            ) ?>
        </div>
    </div>
</div>
