<?php
/**
 * @var $this    \yii\web\View view component instance
 * @var $message \yii\mail\MessageInterface the message being composed
 * @var $content string main view render result
 */

?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=640, initial-scale=1.0">

    <style>
        body {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
            background-color: #f3f3f3;
        }

        td {
            font-family: 'Arial', sans-serif;
            color: #000;
        }

        @media only screen and (max-width: 641px) {
            .email-wrap {
                min-width: 640px !important;
                width: 640px !important;
            }
        }
    </style>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
