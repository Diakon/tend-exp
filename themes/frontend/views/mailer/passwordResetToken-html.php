<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/password-reset', 'token' => $token->token]);

?>
<tr>
    <td>
        <h2 style="color: #39434d;font-size: 18px;line-height: 24px;font-weight: 700;font-family: 'Arial', sans-serif;margin: 0 0 20px;">
            Уважаемый (-ая), <?= Html::encode($user->username) ?>
        </h2>
        <p style="color: #39434d;font-size: 15px;line-height: 18px;font-family: 'Arial', sans-serif;margin: 0;">
            Пройдите по ссылке, чтобы сменить пароль:
            <?= Html::a(Html::encode($resetLink), $resetLink, ['target' => '_blank', 'style' => 'text-decoration: none;color: #0055c4;']) ?>
        </p>
        <p style="color: #39434d;font-size: 15px;line-height: 18px;font-family: 'Arial', sans-serif;margin: 0;">
            Если вы не запрашивали восстановление пароля, просто проигнорируйте это письмо.
        </p>
    </td>
</tr>

<tr>
    <td style="height: 40px;"></td>
</tr>
