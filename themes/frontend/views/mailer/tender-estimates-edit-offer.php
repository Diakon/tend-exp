<?php
$logo = $message->embed(Yii::getAlias('@root') . '/app/frontend/web/html/dist/content/images/email/logo.png');
?>

<table class="email-wrap" cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f3f3f3">
    <tr>
        <td>
            <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
        </td>
    </tr>


    <tr>
        <td align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="640" bgcolor="white" style="border: 1px solid #D3D8E0;border-radius: 5px">
                <tr>
                    <td>
                        <div style="font-size: 50px; line-height: 50px; height: 50px;"></div>
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" border="0" width="352">
                            <tr>
                                <td style="text-align: center;">
                                    <a href="<?= Yii::$app->request->hostName ?>">
                                        <img src="<?= $logo ?>" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 26px;line-height: 1;color: #303030;font-family: 'Arial', sans-serif; font-weight: bold;">
                                    Компания <?= $companyName ?> скорректировала свое предложение по тендеру № <?= $numberTender ?> <?= $nameTender ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 25px; line-height: 25px; height: 25px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;color: #474a4f;">
                                    Здраствуйте <?= $userName ?>!
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 15px; line-height: 15px; height: 15px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;color: #474a4f;font-weight: bold;">
                                    По вашему тендеру № <?= $numberTender ?> <?= $nameTender ?> компания <?= $companyOfferName ?> скорректировала свое предложение на сайте Tenderos.ru
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 25px; line-height: 25px; height: 25px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;padding: 20px;background-color: #F5F5F5;border-radius: 5px;color: #A1A1A1;">
                                    Для просмотра скорректированого предложения <a href="<?= $offerLink ?>" style="display: block;
                                    background-color: #FDA219;
                                    border-radius: 2px;line-height: 40px;
                                    text-align: center;font-size: 14px;
                                    font-family: 'Arial', sans-serif;color: #ffffff;font-weight: bold;text-decoration: none;">перейдите по ссылке</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 25px; line-height: 25px; height: 25px;"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div style="font-size: 50px; line-height: 50px; height: 50px;"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
        </td>
    </tr>
</table>
