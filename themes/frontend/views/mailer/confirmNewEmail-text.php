<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/user/confirm-email', 'token' => $token->new_email_token, 'company' => $company->id]);
?>
Уважаемый (-ая), <?= $user->username ?>,

Спасибо за регистрацию на портале!
Перейдите по указанной ссылке для подтверждения вашего e-mail адреса - <?= $confirmLink ?>.
