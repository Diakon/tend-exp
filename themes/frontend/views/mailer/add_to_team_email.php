<?php
$logo = $message->embed(Yii::getAlias('@root') . '/app/frontend/web/html/dist/content/images/email/logo.png');
?>

<table class="email-wrap" cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f3f3f3">
    <tr>
        <td>
            <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
        </td>
    </tr>


    <tr>
        <td align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="640" bgcolor="white" style="border: 1px solid #D3D8E0;border-radius: 5px">
                <tr>
                    <td>
                        <div style="font-size: 50px; line-height: 50px; height: 50px;"></div>
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" border="0" width="352">
                            <tr>
                                <td style="text-align: center;">
                                    <a href="<?= Yii::$app->request->hostName ?>">
                                        <img src="<?= $logo ?>" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
                                </td>
                            </tr>



                            <tr>
                                <td>
                                    <div style="font-size: 25px; line-height: 25px; height: 25px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;color: #474a4f;">
                                    Здраствуйте <?= $userName ?>!
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 15px; line-height: 15px; height: 15px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;color: #474a4f;font-weight: bold;">
                                    Мы рады приветствовать вас на сайте первого многофункционального строительного сервиса!
                                    Вы получили приглащение на портал TENDEROS от компании <?= $companyName ?>. Перейдите в <a href="<?= $profileUrl ?>">профиль вашей компании</a> и ознакомиться с многочисленными возможностями TENDEROS.
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;">
                                    <p>Данные для авторизации на сайте:</p>
                                    <p>Логин: <?= $email ?></p>
                                    <p>Пароль: <?= $password ?></p>
                                </td>
                            </tr>

                            <?php if (!empty($msgCompanyAdmin)) { ?>
                                <tr>
                                    <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;">
                                        <p>Коментарий от администратора компании:</p>
                                        <p><?= $msgCompanyAdmin ?></p>
                                    </td>
                                </tr>
                            <?php } ?>

                            <tr>
                                <td>
                                    <div style="font-size: 25px; line-height: 25px; height: 25px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="font-size: 25px; line-height: 25px; height: 25px;"></div>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div style="font-size: 50px; line-height: 50px; height: 50px;"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
        </td>
    </tr>
</table>

