<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/password-reset', 'token' => $token->token]);
?>
Уважаемый (-ая), <?= $user->username ?>,

Пройдите по ссылке, чтобы сменить пароль:

<?= $resetLink ?>


Если вы не запрашивали восстановление пароля, просто проигнорируйте это письмо.
