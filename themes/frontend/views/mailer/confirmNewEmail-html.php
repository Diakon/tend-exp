<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/user/confirm-email', 'token' => $token->new_email_token, 'companyId' => $company->id, 'invite' => $invite]);
$logo = $message->embed(Yii::getAlias('@root') . '/app/frontend/web/html/dist/content/images/email/logo.png');
?>


<?php if (!$invite) { ?>
    <tr>
        <td style="color: #0055c4;font-weight: 700;font-size: 24px;line-height: 30px;font-family: 'Arial', sans-serif;">
            <br/>
            Спасибо за регистрацию на портале!
        </td>
    </tr>

    <tr>
        <td style="height: 40px;"></td>
    </tr>

    <tr>
        <td>
            <h2 style="color: #39434d;font-size: 18px;line-height: 24px;font-weight: 700;font-family: 'Arial', sans-serif;margin: 0 0 20px;">
                Подтверждение e-mail
            </h2>

            <p style="color: #39434d;font-size: 15px;line-height: 18px;font-family: 'Arial', sans-serif;margin: 0;">
                Перейдите по указанной ссылке для подтверждения вашего e-mail
                адреса:
                <?= Html::a(Html::encode($confirmLink), $confirmLink, ['target' => '_blank', 'style' => 'text-decoration: none;color: #0055c4;' ]) ?>
            </p>
        </td>
    </tr>

    <tr>
        <td style="height: 40px;"></td>
    </tr>
<?php } else { ?>

    <table class="email-wrap" cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f3f3f3">
        <tr>
            <td>
                <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
            </td>
        </tr>


        <tr>
            <td align="center">
                <table cellpadding="0" cellspacing="0" border="0" width="640" bgcolor="white" style="border: 1px solid #D3D8E0;border-radius: 5px">
                    <tr>
                        <td>
                            <div style="font-size: 50px; line-height: 50px; height: 50px;"></div>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table cellpadding="0" cellspacing="0" border="0" width="352">
                                <tr>
                                    <td style="text-align: center;">
                                        <a href="<?= Yii::$app->request->hostName ?>">
                                            <img src="<?= $logo ?>" alt="">
                                        </a>
                                    </td>
                                </tr>


                                <tr>
                                    <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;color: #474a4f;font-weight: bold;">
                                        Мы рады приветствовать вас на сайте первого многофункционального строительного сервиса!
                                        Вы получили приглащение на портал TENDEROS от компании <?= $company->title ?>.
                                        <br/><br/>
                                        <p style="color: #39434d;font-size: 15px;line-height: 18px;font-family: 'Arial', sans-serif;margin: 0;">
                                            Перейдите по указанной ссылке для подтверждения вашего e-mail
                                            адреса:
                                            <?= Html::a(Html::encode($confirmLink), $confirmLink,
                                                ['target' => '_blank', 'style' => 'text-decoration: none;color: #0055c4;' ]) ?>
                                        </p>
                                    </td>
                                </tr>

                                <?php if (!empty($params['adminText'])) { ?>
                                    <tr>
                                        <td style="font-size: 14px;line-height: 20px;font-family: 'Arial', sans-serif;">
                                            <p>Коментарий от администратора компании:</p>
                                            <p><?= $params['adminText'] ?></p>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <tr>
                                    <td>
                                        <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div style="font-size: 50px; line-height: 50px; height: 50px;"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <div style="font-size: 30px; line-height: 30px; height: 30px;"></div>
            </td>
        </tr>
    </table>

<?php } ?>
