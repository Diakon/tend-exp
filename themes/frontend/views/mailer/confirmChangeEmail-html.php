<?php
use yii\helpers\Html;

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/user/confirm-email', 'token' => $token->old_email_token]);
?>

<tr>
    <td>
        <h2 style="color: #39434d;font-size: 18px;line-height: 24px;font-weight: 700;font-family: 'Arial', sans-serif;margin: 0 0 20px;">
            Подтверждение e-mail
        </h2>

        <p style="color: #39434d;font-size: 15px;line-height: 18px;font-family: 'Arial', sans-serif;margin: 0;">
            Перейдите по указанной ссылке для подтверждения вашего e-mail
            адреса:
            <?= Html::a(Html::encode($confirmLink), $confirmLink, ['target' => '_blank', 'style' => 'text-decoration: none;color: #0055c4']) ?>
        </p>
    </td>
</tr>

<tr>
    <td style="height: 40px;"></td>
</tr>
