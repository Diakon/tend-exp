<?php


/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <?php $this->head() ?>
        <style type="text/css">
            body {
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                margin: 0;
                padding: 0;
                background-color: #f3f3f3;
            }

            td {
                font-family: 'Arial', sans-serif;
                color: #000;
            }

            @media only screen and (max-width: 641px) {
                .email-wrap {
                    min-width: 640px !important;
                    width: 640px !important;
                }
            }
        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <?= $content ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
