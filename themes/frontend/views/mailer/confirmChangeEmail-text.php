<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/user/confirm-email', 'token' => $token->old_email_token]);
?>
Уважаемый (-ая), <?= $user->username ?>,

Пройдите по ссылке чтобы подтвердить смену емайл адреса:

<?= $confirmLink ?>
