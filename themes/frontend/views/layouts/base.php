<?php

use frontend\assets\AppAsset;
use yii\helpers\Html;

/**
 * @var $this    \yii\web\View
 * @var $content string
 */
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php if (!Yii::$app->request->isAjax) { // Chrome icon fix (при запросе $('document').load() -  пропадает иконка) ?>
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon.png">
    <?php } ?>

    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<?php
$this->beginBody();
echo Html::beginTag('body', ['class' => $bodyClass ?? '']);
// Начало отображения контента
echo $content;
?>
<script>
window.onload = () => {
    const $btn = $('.aside__nav').find('._link');

        $btn.on('click', function () {
            const id = $(this).data('id');

            $('html, body').animate({
                scrollTop: $(`#${id}`).offset().top
            }, 600);
        });
    };
</script>
<?php
$this->endBody();
echo Html::endTag('body');
echo Html::endTag('html');
$this->endPage();
