<?php
/**
 * @var $this    \yii\web\View
 * @var $content string
 */
?>

<?php $this->beginContent('@views/layouts/base.php', ['bodyClass' => 'landing']);?>

<div class="wrapper">
    <div class="wrapper__wrap">
        <header class="header">
            <?= $this->render('_header') ?>
        </header>

        <div class="wrapper__inner">
            <?= $content; ?>
        </div>

        <?= $this->render('_footer', ['footerClass' => 'footer--black']) ?>

    </div>
</div>

<?php $this->endContent(); ?>
