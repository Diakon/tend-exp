<?php
/**
 * @var boolean $showMenu
 */
use \common\components\Thumbnail;
$roles = Yii::$app->user->identity ? Yii::$app->user->identity->getRoles() : [];
$avatar= Yii::$app->user->identity ? Yii::$app->user->identity->avatar : false;


?>
<div class="container">
    <div class="header__inner">
        <a href="<?= Yii::$app->homeUrl ?>" class="header__logo"></a>
        <?php if($showMenu ?? true) { ?>
        <div class="header__links">
            <a href="<?= \yii\helpers\Url::to(['/company/companies/company_list']) ?>"><?= Yii::t('app', 'Каталог компаний')?></a>
            <a href="<?= \yii\helpers\Url::to(['/tender/tenders/tender_list']) ?>"><?= Yii::t('app', 'Каталог тендеров')?></a>
        </div>
        <?php } ?>
        <?php if(Yii::$app->user->isGuest) { ?>

           <div class="header__login">
               <a href="<?= \yii\helpers\Url::to(['/user/user/login']) ?>" class="btn btn-small btn-primary"><?= Yii::t('app', 'ВОЙТИ')?></a>
               <a href="<?= \yii\helpers\Url::to(['/user/user/signup']) ?>" class="btn btn-small btn-outline-secondary"><?= Yii::t('app', 'ЗАРЕГИСТРИРОВАТЬСЯ')?></a>
           </div>

      <?php } else { ?>

            <div class="header__profile">
                <div class="header-profile">
                    <div class="header-profile__inner">
                        <div class="header-profile__head">
                            <div class="header-profile__logo">

                                <?php if ($avatar) { ?>
                                    <img class="_img" src="<?=Thumbnail::thumbnailFileUrl($avatar->getFile(true, Yii::getAlias('@static') . '/../content/images/avatar.svg')); ?>">
                                <?php } else { ?>
                                    <img src="<?= Yii::getAlias('@static') ?>/../content/images/avatar.svg" alt="" class="_img">
                                <?php } ?>


                            </div>
                            <div class="header-profile__name"><?= Yii::$app->user->identity->getFullName()?></div>

                        </div>
                        <div class="header-profile__body">
                            <ul class="header-profile__list">
                                <li class="header-profile__item">
                                    <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/company_info']) ?>" class="header-profile__link"><?= Yii::t('app', 'Личный кабинет')?></a>
                                </li>

                                <?php if (!in_array(\common\components\AuthDbManager::ROLE_ADMIN, $roles) &&
                                    !in_array(\common\components\AuthDbManager::ROLE_MODERATOR, $roles)) { ?>
                                    <li class="header-profile__item">
                                        <a href="<?= \yii\helpers\Url::to(['/dashboard/dashboard/profile_edit']) ?>" class="header-profile__link"><?= Yii::t('app', 'Настройки профиля') ?></a>
                                    </li>
                                <?php } ?>

<!--                                <li class="header-profile__item">-->
<!--                                    <a href="" class="header-profile__link">Настройки организации</a>-->
<!--                                </li>-->
                            </ul>
                            <div class="header-profile__action">
                                <a href="<?= \yii\helpers\Url::to(['/user/user/logout']) ?>" class="header-profile__link"><?= Yii::t('app', 'Выход')?></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        <?php } ?>

        <div class="header__action">
            <button class="_open"></button>
        </div>
    </div>
</div>
