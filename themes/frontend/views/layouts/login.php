<?php
/**
 * @var View $this
 * @var string $content
 */

use yii\web\View;

\frontend\assets\LoginUserAsset::register($this);
$path = Yii::getAlias('@static');

$this->beginPage();
?>

<?php
/**
 * @var $this    \yii\web\View
 * @var $content string
 */
?>
<?php $this->beginContent('@views/layouts/base.php'); ?>
<div class="wrapper">
    <div class="wrapper__wrap">

        <?= $this->render('_header', ['showMenu' => false]) ?>

        <div class="wrapper__inner">
            <div class="auth-page">
                <?= $content; ?>
            </div>

        </div>
    </div>
</div>
<?php $this->endContent(); ?>


