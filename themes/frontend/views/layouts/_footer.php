<?php
$footerMenu = \modules\menu\widget\MenuRenderer::widget([
    'id' => 1,
    'menuConfig' => [
        'options' => ['class' => 'footer__nav-list'],
        'itemOptions' => ['class' => 'footer__nav-item'],
        'linkTemplate' => '<a href="{url}" class="footer__nav-link">{label}</a>'
    ],
]);
?>
<footer class="footer <?= $footerClass ?? '' ?>">
    <div class="container">
        <div class="footer__inner">
            <div class="footer__top">
                <a href="<?= Yii::$app->homeUrl ?>" class="footer__logo"></a>
                <div class="footer__nav">

                    <?= $footerMenu ?>

                </div>
                <div class="footer__contact">
                    <ul class="footer__contact-list">
                        <li class="footer__contact-item">
                            <a href="tel:<?= Yii::$app->settings->get('phone', 'site') ?>" class="footer__contact-link"><?= Yii::$app->settings->get('phone', 'site') ?></a>
                        </li>
                        <li class="footer__contact-item">
                            <a href="mailto:<?= Yii::$app->settings->get('email', 'site') ?>" class="footer__contact-link"><?= Yii::$app->settings->get('email', 'site') ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="footer__copy">&copy; <?= date('Y', time()) ?>, Tenderos</div>
                <div class="footer__contact">
                    <ul class="footer__contact-list">
                        <li class="footer__contact-item">
                            ИНН: <?= Yii::$app->settings->get('inn', 'site') ?>
                        </li>
                        <li class="footer__contact-item">
                            ОГРН: <?= Yii::$app->settings->get('ogrn', 'site') ?>
                        </li>
                        <li class="footer__contact-item">
                            <a href="<?= Yii::$app->settings->get('terms_use', 'site') ?>" class="footer__contact-link" target="_blank">Пользовательское соглашение</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
