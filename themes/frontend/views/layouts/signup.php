<?php
/**
 * @var View $this
 * @var string $content
 */

use yii\web\View;

$this->beginPage();
?>

<?php
/**
 * @var $this    \yii\web\View
 * @var $content string
 */
?>
<?php $this->beginContent('@views/layouts/base.php', ['bodyClass' => 'auth']); ?>

<header class="header">
        <?= $this->render('_header', ['showMenu' => false]) ?>
</header>



<div class="signup-page">
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
