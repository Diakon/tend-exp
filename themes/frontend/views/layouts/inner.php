<?php
/**
 * @var $this    \yii\web\View
 * @var $content string
 */
?>
<?php $this->beginContent('@views/layouts/base.php'); ?>

<div class="wrapper">
    <div class="wrapper__wrap">
        <header class="header">
            <?= $this->render('_header') ?>
        </header>

        <div class="wrapper__inner">
            <?= $content; ?>
        </div>
        <footer class="footer">
            <?= $this->render('_footer') ?>
        </footer>

    </div>
</div>

<?php $this->endContent(); ?>
