<?php

use yii\helpers\Html;

/**
 * @var $this    \yii\web\View
 * @var $content string
 */
\frontend\assets\ErrorAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<?php
$this->beginBody();
echo Html::beginTag('body', ['class' => $bodyClass ?? '']);
// Начало отображения контента
echo $content;
?>
<?php
$this->endBody();
echo Html::endTag('body');
echo Html::endTag('html');
$this->endPage();
