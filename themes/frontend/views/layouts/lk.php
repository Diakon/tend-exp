<?php
/**
 * @var $this    \yii\web\View
 * @var $content string
 */

use yii\helpers\Url;
$static = Yii::getAlias('@static');
$roles = Yii::$app->user->identity?  Yii::$app->user->identity->getRoles() : [];
$avatar= Yii::$app->user->identity->avatar;
?>
<?php $this->beginContent('@views/layouts/base.php'); ?>

<div class="wrapper wrapper--lk">
    <div class="wrapper__wrap">
        <div class="wrapper__inner">
            <?php if (!empty($this->params['isLongView'])) { ?>
                <?= $content; ?>
            <?php } else { ?>
                <main class="main">
                    <div class="main__head">
                        <h1 class="main__title"><?= Yii::t('app', $this->title)?></h1>
                    </div>
                    <div class="main__body">
                        <?= $content; ?>
                    </div>
                </main>
            <?php } ?>
        </div>
    </div>
</div>
<div class="side-nav">
    <div class="side-nav__inner">
        <div class="side-nav__main">
            <div class="side-nav__section">
                <ul class="side-nav__list">
                    <li class="side-nav__item side-nav__item--main">
                        <a href="<?= Yii::$app->homeUrl ?>"  target="_blank" class="side-nav__link">T</a>
                    </li>
                </ul>
            </div>
            <div class="side-nav__section">

                <?= \yii\widgets\Menu::widget(['items' => [
                        ['label' => '<svg class="icon icon-nav-1 ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $static . '/../static/images/svg/spriteInline.svg#nav-1"/> </svg>',
                            'url' => ['/dashboard/dashboard/company_info'],
                            'options' => ['title' => Yii::t('app', 'О компании')]
                        ],
                        ['label' => '<svg class="icon icon-nav-1 ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $static . '/../static/images/svg/spriteInline.svg#nav-2"/> </svg>',
                            'url' => ['/dashboard/dashboard/tenders', 'companyUrl' => Yii::$app->user->getActiveCompanyData()['url']],
                            'options' => ['title' => Yii::t('app', 'Тендеры')]
                        ],
                        ['label' => '<svg class="icon icon-nav-1 ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $static . '/../static/images/svg/spriteInline.svg#nav-3"/> </svg>',
                            'url' => ['/dashboard/dashboard/objects', 'companyUrl' => Yii::$app->user->getActiveCompanyData()['url']],
                            'options' => ['title' => Yii::t('app', 'Объекты')]
                        ],
                    ],
                        'options' => [
                            'class' => 'side-nav__list',
                        ],
                        'encodeLabels' => false,
                        'itemOptions' => [
                            'class' => 'side-nav__item',
                        ],
                        'linkTemplate' => '<a class="side-nav__link" href="{url}">{label}</a>',
                    ]
                ) ?>

            </div>
            <div class="side-nav__section">
                <ul class="side-nav__list">
                    <li class="side-nav__item side-nav__item--link">
                        <a href="<?= Url::to(['/tenders']) ?>" target="_blank" class="side-nav__link" title="<?= Yii::t('app', 'Каталог тендеров') ?>">
                            <svg class="icon icon-nav-2 ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#nav-2"/>
                            </svg>
                        </a>
                    </li>
                    <li class="side-nav__item side-nav__item--link">
                        <a href="<?= Url::to(['/companies']) ?>" target="_blank" class="side-nav__link" title="<?= Yii::t('app', 'Каталог компаний') ?>">
                            <svg class="icon icon-nav-1 ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?= Yii::getAlias('@static') ?>/../static/images/svg/spriteInline.svg#nav-1"/>
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="side-nav__section">
                <ul class="side-nav__list">
                    <li class="side-nav__item side-nav__item--profile">
                        <a href="<?= Url::to(['/dashboard/dashboard/profile_edit']) ?>" class="side-nav__link" title="<?= Yii::t('app', 'Настройки профиля') ?>">

                            <?php if ($avatar) { ?>
                                <img class="_img" src="<?= \common\components\Thumbnail::thumbnailFileUrl($avatar->getFile(true, Yii::getAlias('@static') . '/../content/images/avatar3.png')); ?>">
                            <?php } else { ?>
                                <img src="<?= Yii::getAlias('@static') ?>/../content/images/avatar3.png" alt="" class="_img">
                            <?php } ?>
                        </a>
                    </li>
<!--                    <li class="side-nav__item">-->
<!--                        <a href="#" class="side-nav__link">-->
<!--                            <svg class="icon icon-settings ">-->
<!--                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../static/images/svg/spriteInline.svg#settings"/>-->
<!--                            </svg>-->
<!--                        </a>-->
<!--                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>
