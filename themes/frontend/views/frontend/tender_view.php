<?php
use yii\helpers\ArrayHelper;
use frontend\widgets\EstimatesList;
/**
 * @var \common\modules\tender\models\frontend\Tenders $model
 */
$documents = $model->getDocumentsFiles();
?>
<?= Yii::t('app', 'Имя') ?>: <?= $model->title ?>
<br>
<b><?= Yii::t('app', 'Условия') ?></b>
<br>
<?= Yii::t('app', 'Информация об объекте') ?>:
<ul>
    <li><?= Yii::t('app', 'Название объекта') ?>: <?= $model->object->title ?></li>
    <li><?= Yii::t('app', 'Тип объекта') ?>: <?= implode(', ', ArrayHelper::map($model->object->types, 'id', 'title')) ?></li>
    <li><?= Yii::t('app', 'Адрес') ?>: <?= $model->object->address->fullAddress ?? '' ?></li>
    <li><?= Yii::t('app', 'Вид работ') ?>: <?= implode(', ', ArrayHelper::map($model->object->works, 'id', 'title')) ?></li>
    <li><?= Yii::t('app', 'Название объекта') ?>: <?= Yii::$app->formatter->asDate($model->object->date_start, 'php:d.m.Y') ?></li>
    <li><?= Yii::t('app', 'Краткое описание') ?>: <?= $model->object->description ?></li>
</ul>
<a href="<?= \yii\helpers\Url::to(['dashboard/offer_add', 'tenderId' => $model->id]) ?>">Участвовать в тендере</a>
<br>
<?= Yii::t('app', 'Смета работ') ?>:
<?= EstimatesList::widget([
        'tender' => $model,
        'view' => 'estimates_list/list',
]);
?>
<?php if (!empty($documents)) { ?>
    <?= Yii::t('app', 'Документы') ?>:
    <?php foreach ($documents as $document) { ?>
        <a href="<?= $document['url'] ?>"><?= $document['name'] ?></a>
    <?php } ?>
<?php } ?>

