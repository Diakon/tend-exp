<?php
/**
 * @var $data \common\models\Pages
 */
?>
<main class="page-landing">
    <article class="static">
        <div class="container">
            <h1 class="page-title__title page-title__title--middle">
                <span class="_text"><?= $data->title ?></span>
            </h1>
            <figure>
                <?php if ($data->img) { ?>
                    <img class="_img" src="<?php echo \common\components\Thumbnail::thumbnailFile($data->img->getFile(true), 1000, 497); ?>">
                <?php }  ?>
            </figure>
            <?= $data->full ?>

            <?php
            if ($data->typeTemplates == \common\models\Pages::TARIF_TEMPLATE) { ?>

               <?= $this->render('_tarify_template'); ?>
            <?php } ?>
        </div>
    </article>
</main>
