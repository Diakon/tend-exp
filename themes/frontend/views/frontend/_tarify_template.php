<?php
/**
 * Created by PhpStorm.
 * User: mikhail
 * Date: 26.09.18
 * Time: 12:20
 */
use yii\helpers\Url;
$link = Yii::$app->user->isGuest ? Url::to(['/user/user/login']) : Url::to(['/dashboard/dashboard/company_info']);

?>

<div class="change-tariff">
    <div class="change-tariff__inner">
        <!--                        <div class="change-tariff__head">-->
        <!--                            <div class="change-tariff__title">Изменить тариф</div>-->
        <!--                        </div>-->
        <div class="change-tariff__body">
            <div class="change-tariff__row">
                <div class="change-tariff__col">
                    <div class="tariff-item">
                        <div class="tariff-item__inner">
                            <div class="tariff-item__head">
                                <div class="tariff-item__title"><?= Yii::t('app', 'МИНИМАЛЬНЫЙ') ?></div>
                            </div>
                            <div class="tariff-item__body">
                                <ul class="tariff-item__list">
                                    <li>Поиск подрядчиков по основным критериям</li>
                                    <li>Возможность просмотра основной информации в профиле Компании</li>
                                    <li>Регистрация на сайте и создание своей Компании</li>
                                </ul>
                            </div>
                            <div class="tariff-item__foot">
                                <div class="tariff-item__price"><?= Yii::t('app', 'БЕСПЛАТНО') ?></div>
                                <div class="tariff-item__action">
                                    <a href="<?= $link ?>" target="_blank" class="btn btn-primary btn-w100"><?= Yii::t('app', 'ВЫБРАТЬ') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="change-tariff__col">
                    <div class="tariff-item">
                        <div class="tariff-item__inner">
                            <div class="tariff-item__head">
                                <div class="tariff-item__title"><?= Yii::t('app', 'ОСНОВНОЙ') ?></div>
                            </div>
                            <div class="tariff-item__body">
                                <ul class="tariff-item__list">
                                    <li>Поиск подрядчиков по основным критериям
                                    </li>
                                    <li>Возможность просмотра основной информации в профиле Компании
                                    </li>
                                    <li>Регистрация на сайте и создание своей Компании</li>
                                    <li>Возможность расширенного поиска подрядчиков</li>
                                    <li>Доступ ко всей информации в профиле Компании</li>
                                    <li>Возможность опубликовать профиль Компании
                                    </li>
                                </ul>
                            </div>
                            <div class="tariff-item__foot">
                                <div class="tariff-item__price">399 РУБ./МЕСЯЦ</div>
                                <div class="tariff-item__action">
                                    <a href="<?= $link ?>" target="_blank" class="btn btn-primary btn-w100"><?= Yii::t('app', 'ВЫБРАТЬ') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="change-tariff__row">
                <div class="change-tariff__col">
                    <div class="tariff-item active">
                        <div class="tariff-item__inner">
                            <div class="tariff-item__head">
                                <div class="tariff-item__title"><?= Yii::t('app', 'ПРОФЕССИОНАЛЬНЫЙ') ?></div>
                            </div>
                            <div class="tariff-item__body">
                                <ul class="tariff-item__list">
                                    <li>Поиск подрядчиков по основным критериям</li>
                                    <li>Возможность просмотра основной информации в профиле Компании</li>
                                    <li>Регистрация на сайте и создание своей Компании</li>
                                    <li>Возможность расширенного поиска подрядчиков</li>
                                    <li>Доступ ко всей информации в профиле Компании</li>
                                    <li>Возможность опубликовать профиль Компании</li>
                                    <li>Поиск и участие в тендерах</li>
                                </ul>
                            </div>
                            <div class="tariff-item__foot">
                                <div class="tariff-item__price">499 РУБ./МЕСЯЦ</div>
                                <div class="tariff-item__action">
                                    <a href="<?= $link ?>" target="_blank" class="btn btn-primary btn-w100" ><?= Yii::t('app', 'ВЫБРАТЬ') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="change-tariff__col">
                    <div class="tariff-item">
                        <div class="tariff-item__inner">
                            <div class="tariff-item__head">
                                <div class="tariff-item__title"><?= Yii::t('app', 'КОРПОРАТИВНЫЙ') ?></div>
                            </div>
                            <div class="tariff-item__body">
                                <ul class="tariff-item__list">
                                    <li>Поиск подрядчиков по основным критериям</li>
                                    <li>Возможность просмотра основной информации в профиле Компании</li>
                                    <li>Регистрация на сайте и создание своей Компании</li>
                                    <li>Возможность расширенного поиска подрядчиков</li>
                                    <li>Доступ ко всей информации в профиле Компании</li>
                                    <li>Возможность опубликовать профиль Компании</li>
                                    <li>Поиск и участие в тендерах</li>
                                    <li>Создание и проведение тендеров</li>
                                </ul>
                            </div>
                            <div class="tariff-item__foot">
                                <div class="tariff-item__price">599 РУБ./МЕСЯЦ</div>
                                <div class="tariff-item__action">
                                    <a href="<?= $link ?>" target="_blank" class="btn btn-primary btn-w100"><?= Yii::t('app', 'ВЫБРАТЬ') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
