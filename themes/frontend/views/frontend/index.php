<?php
/**
 * @var \yii\web\View $this
 */



?>





<main class="page-landing">

    <div class="home">

        <?= \frontend\widgets\Slider::widget([
            'view'   => '@app/widgets/views/slider',
        ]); ?>

        <section class="home-section">
            <div class="container">


                <?= \frontend\widgets\UniqueFeatures::widget([
                    'view'   => '@app/widgets/views/unique-features',
                ]); ?>
            </div>
        </section>

        <section class="home-section">
            <div class="container">
                <div class="home-section__inner">
                    <header class="home-section__header">
                        <h2 class="home-section__header_title">Как это работает</h2>
                    </header>
                    <div class="home-how">
                        <div class="home-how__list">
                            <div class="home-how__col">
                                <span class="home-how__title">СОЗДАЙТЕ КОМПАНИЮ</span>
                                <ul>
                                    <li>Создайте профиль своей компании</li>
                                    <li>Добавьте работы в портфолио</li>
                                </ul>
                            </div>
                            <div class="home-how__col">
                                <span class="home-how__title">РАЗМЕСТИТЕ ТЕНДЕР</span>
                                <ul>
                                    <li>Создайте и разместите тендер</li>
                                    <li>Сравните предложения</li>
                                    <li>Выберите лучшее</li>
                                </ul>
                            </div>
                            <div class="home-how__col">
                                <span class="home-how__title">ВЫБЕРИТЕ ПОДРЯДЧИКА</span>
                                <ul>
                                    <li>Укажите критерии поиска подрядчика</li>
                                    <li>Ознакомьтесь с компанией и её портфолио</li>
                                    <li>Выберите подходящего подрядчика</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="home-section home-tenders">
            <div class="container">
                <div class="home-section__inner">
                    <div class="home-section__tabs js-tabs">
                        <button class="home-section__tab active" data-tabs="tenders">Тендеры</button>
                        <button class="home-section__tab" data-tabs="companies">Компании</button>
                    </div>

                    <div id="tenders">
                        <header class="home-section__header">
                            <h2 class="home-section__header_title">Найдите тендер уже сейчас</h2>
                        </header>

                        <div class="home-tenders__list">
                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_01.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_01_h.svg">
                                    <div class="home-tenders__name"><span>Жилищно-Гражданское строительство</span></div>
                                </a>
                            </div>
                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_02.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_02_h.svg">
                                    <div class="home-tenders__name"><span>Промышленное строительство</span></div>
                                </a>
                            </div>

                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_03.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_03_h.svg">
                                    <div class="home-tenders__name"><span>Транспортное строительство</span></div>
                                </a>
                            </div>

                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_04.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_04_h.svg">
                                    <div class="home-tenders__name"><span>Гидротехническое строительство</span></div>
                                </a>
                            </div>

                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_05.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_05_h.svg">
                                    <div class="home-tenders__name"><span>Инфраструктура</span></div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div id="companies" style="display: none">
                        <header class="home-section__header">
                            <h2 class="home-section__header_title">Найдите подрядчика уже сейчас</h2>
                        </header>
                        <div class="home-tenders__list">
                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_01.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_01_h.svg">
                                    <div class="home-tenders__name"><span>Жилищно-Гражданское строительство</span></div>
                                </a>
                            </div>
                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_02.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_02_h.svg">
                                    <div class="home-tenders__name"><span>Промышленное строительство</span></div>
                                </a>
                            </div>

                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_03.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_03_h.svg">
                                    <div class="home-tenders__name"><span>Транспортное строительство</span></div>
                                </a>
                            </div>

                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_04.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_04_h.svg">
                                    <div class="home-tenders__name"><span>Гидротехническое строительство</span></div>
                                </a>
                            </div>

                            <div class="home-tenders__item">
                                <a href="#" class="home-tenders__link">
                                    <img class="home-tenders__img" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_05.svg">
                                    <img class="home-tenders__img home-tenders__img--hover" src="<?= Yii::getAlias('@static') ?>/../content/images/home/tender_05_h.svg">
                                    <div class="home-tenders__name"><span>Инфраструктура</span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?= \frontend\widgets\News::widget([
         'view'   => '@app/widgets/views/news',
        ]); ?>

        <section class="home-section home-static">
            <div class="container">
                <div class="home-section__inner">
                    <header class="home-section__header home-section__header--small">
                        <h2 class="home-section__header_title"><?= Yii::t('app', 'Добро пожаловать на портал Tenderos')?></h2>
                    </header>
                    <p><?= Yii::$app->settings->get('seo_text', 'site') ?></p>
                </div>
            </div>
        </section>
    </div>
</main>

