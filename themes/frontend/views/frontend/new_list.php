<?php

/**
 * @var $importantNews \common\models\Pages[]
 */
$loadPage = $dataProvider->pagination->page;
?>
<main class="page-landing">


    <div class="container">
        <h1 class="page-title__title">
            <span class="_text"><?= $this->title ?></span>
        </h1>

        <div class="news-block">
            <div class="news-block__row">
                <?php foreach ($importantNews as $new) { ?>
                    <div class="news-block__col news-block__col--big">
                        <div class="news-item news-item--big">
                            <div class="news-item__inner">
                                <span class="news-item__badge"><?= Yii::t('app', 'Важное')?></span>

                                <a href="<?= $new->getViewLink('new_view') ?>" class="news-item__link">
                                    <div class="news-item__img">
                                        <?php if ($new->img) { ?>
                                            <img class="_img" src="<?php echo \common\components\Thumbnail::thumbnailFile($new->img->getFile(true), 592, 384); ?>">
                                        <?php }  ?>
                                    </div>
                                </a>

                                <div class="news-item__content">
                                    <a href="<?= $new->getViewLink('new_view') ?>" class="news-item__title"><?= $new->title ?></a>
                                    <time datetime="<?= Yii::$app->formatter->asDate($new->datePublished) ?>" class="news-item__date"><?= Yii::$app->formatter->asDate($new->datePublished) ?></time>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php  } ?>
            </div>
        </div>

        <div class="news-block">
            <div class="news-block__row js-news-block__row">
                <?= $this->render('_new_list_inner', ['dataProvider' => $dataProvider, 'isAjax' => $isAjax]) ?>

        </div>
            <div class="section__action load-more"
                 data-page-count="<?= $dataProvider->pagination->pageCount ?>"
                 data-ajax-url="<?= \yii\helpers\Url::current() ?>" data-type="news" data-load-page="<?= $loadPage ?>">
                <?php if($dataProvider->pagination->pageCount != $dataProvider->pagination->page+1) { ?>
                    <button class="btn btn-outline-dark">Показать еще новости</button>
                <?php } ?>
            </div>
        </div>


        <div class="pagination">
            <div class="pagination__inner">

                <?= $this->render('_new_pagination', ['dataProvider' => $dataProvider]) ?>

            </div>
        </div>
    </div>

</main>
