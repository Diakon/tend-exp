<?php
/**
 * @var $model \common\models\Pages
 */

?>

<div class="news-item__inner">

    <a href="<?= $model->getViewLink('new_view') ?>" class="news-item__link">
        <div class="news-item__img">
            <?php if ($model->img) { ?>
                <img class="_img" src="<?php echo \common\components\Thumbnail::thumbnailFile($model->img->getFile(true)); ?>">
            <?php } else { ?>
                <img src="<?= Yii::getAlias('@static') ?>/../content/images/avatar2.svg" alt="" class="_img">
            <?php } ?>
        </div>
    </a>

    <div class="news-item__content">
        <a href="<?= $model->getViewLink('new_view') ?>" class="news-item__title"><?= $model->title ?></a>
        <time datetime="<?= \Yii::$app->formatter->asDate($model->datePublished) ?>" class="news-item__date"><?= \Yii::$app->formatter->asDate($model->datePublished) ?></time>

    </div>
</div>


