<div class="popup " data-popup-id="improve" id="improve" style="display: none;" >
    <div class="popup__box">
        <div class="popup__box-inner">
            <button class="popup__close js-close-wnd" type="button"></button>

            <div class="popup__title">Улучшите аккаунт для расширенного поиска</div>

            <div class="popup__body">
                <div class="popup-improve">
                    <div class="popup-improve__inner">
                        <div class="popup-improve__desc">
                            Ваш текущий уровень аккаунта не позволяет использовать возвожности расширенного поиска.
                            Улучшите аккаунт и наслаждайтесь всеми возможностями сервиса TENDEROS, как сделали уже 2105 пользователей.
                        </div>
                        <div class="popup-improve__action">
                            <a href="#" class="btn btn-primary js-close-wnd">УЛУЧШИТЬ АККАУНТ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>