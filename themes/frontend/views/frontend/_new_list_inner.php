<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\web\View $this
 */

$loadPage = $dataProvider->pagination->page;

?>

<?= \modules\crud\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'options' => ['class' => 'news-block__col'],
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'news-item',
    ],
    'itemView' => '_new_list_item',
    // 'emptyText' => '<div class="filter-result__empty"><div class="_title">' . Yii::t('app', $emptyMessage) . '</div></div>',
    'pager' => false,
])
?>



